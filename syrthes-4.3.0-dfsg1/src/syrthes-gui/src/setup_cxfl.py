import os, shutil, sys

from cx_Freeze import setup, Executable

if os.name == 'nt':
    include_files=[]
else:
    # ATTENTION : le package libqwt5-qt4-dev de Calibre7 n'est pas le bon !!
    #             le bon package est python-qwt5-qt4
    pb_package = os.popen('apt-cache policy python-qwt5-qt4 | grep Install | cut -d":" -f2',"r")
    pb_package = pb_package.readline()
    pb_package = pb_package.strip()
    if pb_package == "(aucun)" or pb_package == "(none)":
        print "ERROR : python-qwt5-qt4 package must be installed !!"  
        sys.exit(1)
    # fichier libqwt-qt4.so.5 embarque par cxfreeze
    include_files=[("/usr/lib/libqwt-qt4.so.5","libqwt-qt4.so.5")]

# deux premiers chiffres de la version python
version_python_long = sys.version_info
version_python = str(version_python_long[0]) + "." +  str(version_python_long[1])
setup(
        name = "IHM_SYRTHES",
        version = "2.0",
        description = "Code_Syrthes GUI",
        options = {"build_exe": { "include_files": include_files } },
        executables = [Executable("SyrthesMain.py", None)]      
)

def copyFic22():
    if os.name == 'nt':
        exe = "exe.win32-"+version_python
    else:
        exe = "exe.linux-x86_64-"+version_python
    
    cwd = os.getcwd()

    # copier ficwhatsthis/*.*
    src = cwd + os.sep + "ficwhatsthis"
    dst = cwd + os.sep + "build" + os.sep + exe + os.sep + "ficwhatsthis"
    try:
    #if 1:
        os.makedirs(dst)
        names = os.listdir(src)
        for name in names:
            if name == '.svn' or name == 'images': continue
            srcname = os.path.join(src, name)
            dstname = os.path.join(dst, name)
            shutil.copy2(srcname, dstname)
        print "copied :", src
        print "to :", dst
    except:
        print "fail to copy", src
        print "to :", dst
        print "Please do this manually"
        print "-----------------------"
        pass
    
    # copier ficwhatsthis/images/*.*
    src = cwd + os.sep + "ficwhatsthis" + os.sep + "images"
    dst = cwd + os.sep + "build" + os.sep + exe + os.sep + "ficwhatsthis" + os.sep + "images"    
    try:
    #if 1:
        os.makedirs(dst)
        names = os.listdir(src)
        for name in names:
            if name == '.svn' : continue
            srcname = os.path.join(src, name)
            dstname = os.path.join(dst, name)
            shutil.copy2(srcname, dstname)
        print "copied :", src
        print "to :", dst
    except:
        print "fail to copy", src
        print "to :", dst
        print "Please do this manually"
        print "-----------------------"
        pass
    
    # copier 22x22/*.*
    src = cwd + os.sep + "22x22"
    dst = cwd + os.sep + "build" + os.sep + exe + os.sep + "22x22"
    try:
    #if 1:
        os.makedirs(dst)
        names = os.listdir(src)
        for name in names:
            if name == '.svn' : continue
            srcname = os.path.join(src, name)
            dstname = os.path.join(dst, name)
            shutil.copy2(srcname, dstname)
        print "copied :", src
        print "to :", dst
    except:
        print "fail to copy", src
        print "to :", dst
        print "Please do this manually"
        print "-----------------------"
        pass
    
def removeFic22():
    if os.name == 'nt':
        exe = "exe.win32-"+version_python
    else:
        exe = "exe.linux-x86_64-"+version_python
    
    try:
    #if 1:
        cwd = os.getcwd()
        src1 = cwd + os.sep + "build" + os.sep + exe + os.sep + "ficwhatsthis"
        src2 = cwd + os.sep + "build" + os.sep + exe + os.sep + "22x22"
        shutil.rmtree(src1)
        shutil.rmtree(src2)
        print "removed :", src1
        print "removed :", src2
    except:
        print "fail to delete", src1, "or", src2
        print "Please do this manually if these folders existed"
        pass

removeFic22()
copyFic22()
print "done"
