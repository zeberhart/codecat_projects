/*
 *  Copyright 2008 Michael Pfeuti <m_pfeuti@students.unibe.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <signal.h>
#include <unistd.h>

#include <gtk/gtk.h>

#include <gdk/gdkkeysyms.h>
#include <gdk/gdk.h>

#include <gio/gio.h>
#include <gio/gdesktopappinfo.h>

#include <libxfcegui4/libxfcegui4.h>
#include <libxfce4panel/libxfce4panel.h>

#include "linelight.h"
#include "defines.h"
#include "linelight-ico.h"

static LineLightData *linelight = NULL;
static GThread* current_search_thread;

static void plugin_create(XfcePanelPlugin *plugin);
static void showProperties_cb(XfcePanelPlugin *plugin, gpointer userdata);
static gboolean tree_mouse_selection (GtkWidget   *widget, GdkEventButton *event, GtkWidget *tree);
static gboolean esc_event (GtkWidget *w, GdkEventKey *event, GtkWidget *entry);
static void start_search(GtkEditable *entry,char* user_data);
static void open_search_window_cb(GtkButton *button, XfcePanelPlugin* win);
static gboolean tree_selection (GtkWidget *widget, GdkEventKey *event, GtkWidget *tree);
static void cell_renderer (GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *tree_model, GtkTreeIter *iter, gpointer data);
static gboolean delete_event(GtkWidget *w, GdkEvent* event,  gpointer   user_data);

/*
 *  GUI Handling
 */

//main window which takes text to search for and displays the result
static GtkWidget* create_windows_content(GtkListStore *list)
{
    GtkWidget *hbox = gtk_vbox_new(FALSE, 5);

    GtkWidget *tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (list));
    gtk_tree_view_set_enable_search(GTK_TREE_VIEW(tree), FALSE);
    gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree), FALSE);


    //ICON col
    GtkCellRenderer *icon_renderer = gtk_cell_renderer_pixbuf_new();
    GtkTreeViewColumn *icon_column = gtk_tree_view_column_new_with_attributes("Icon", icon_renderer,"pixbuf", ICON_COL,NULL);
    //gtk_tree_view_column_set_resizable(icon_column, FALSE);
    //gtk_tree_view_column_set_sizing(icon_column,GTK_TREE_VIEW_COLUMN_AUTOSIZE);
    gtk_tree_view_append_column(GTK_TREE_VIEW (tree), icon_column);

    //Text col
    GtkCellRenderer *text_renderer = gtk_cell_renderer_text_new ();
    /*  g_object_set(renderer,"size-set",TRUE,NULL);
      g_object_set(renderer,"size-points",9.0,NULL);
      g_object_set(renderer,"height", 0,NULL);
    GtkTreeViewColumn *text_column = gtk_tree_view_column_new();
    gtk_tree_view_column_set_title(text_column,"LineLight");
    gtk_tree_view_column_pack_start(text_column, text_renderer,  FALSE);
    */
    GtkTreeViewColumn *text_column = gtk_tree_view_column_new_with_attributes("Text", text_renderer,"text", TEXT_COL,NULL);
    gtk_tree_view_column_set_resizable(text_column, FALSE);
    gtk_tree_view_column_set_sizing(text_column,GTK_TREE_VIEW_COLUMN_AUTOSIZE);
    gtk_tree_view_column_set_cell_data_func(text_column, text_renderer, cell_renderer, NULL, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW (tree), text_column);
    gtk_container_add(GTK_CONTAINER(hbox), tree);

    GtkWidget *entry = gtk_entry_new();

    gtk_container_add(GTK_CONTAINER(hbox), entry);

    g_signal_connect(tree,"key-release-event", G_CALLBACK(tree_selection),entry);
    g_signal_connect(tree,"button-press-event", G_CALLBACK(tree_mouse_selection), entry);
    g_signal_connect(entry,"key-release-event", G_CALLBACK(esc_event),NULL);
    g_signal_connect(entry, "changed", G_CALLBACK(start_search), NULL);

    linelight->text_field = entry;

    return hbox;
}

static void move_action_cb(GtkWidget *w, GtkRequisition *g, XfcePanelPlugin* plugin)
{
    switch (linelight->grow_corner)
    {
    case TOP_RIGHT_CORNER:
        gtk_window_move(GTK_WINDOW(w), linelight->x - g->width, linelight->y);
        break;
    case BOTTOM_LEFT_CORNER:
        gtk_window_move(GTK_WINDOW(w), linelight->x, linelight->y - g->height);
        break;
    case BOTTOM_RIGHT_CORNER:
        gtk_window_move(GTK_WINDOW(w), linelight->x - g->width, linelight->y - g->height);
        break;
    case TOP_LEFT_CORNER:
        gtk_window_move(GTK_WINDOW(w), linelight->x, linelight->y);
        break;
    }
}

static void cell_renderer(GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, gpointer data)
{
    char *path, *name;
    GdkPixbuf *buf;

    gtk_tree_model_get(model, iter, ICON_COL, &buf, TEXT_COL, &path, -1);

    if (path == NULL)
       return;

    name = strrchr(path, '/');

    if (name == NULL) //" " or SECTION Name
    {
        //indicate # of search results
        int i, count = 0;
        for (i =  0;i<SECTION_COUNT; i++)
        {
            if (strcmp(path,sectionNames[i]) == 0)
                count = linelight->all_found_results[i];
        }
        if (count != 0)
        {
            char entry[strlen(path) + 100]; // 100 chars for indicating how many results are found
            sprintf(entry, "%s (%i)", path, count);
            g_object_set(cell,"text", entry,NULL);
        }
        else
        {
            g_object_set(cell,"text", path,NULL);
        }
    }
    else if (strstr(name,".desktop") != NULL) //Application names based on .desktop files
    {
        XfceDesktopEntry *application = xfce_desktop_entry_new(path,categories,3);
        if (XFCE_IS_DESKTOP_ENTRY(application))
        {
            char* app_name;
            xfce_desktop_entry_get_string(application, "Name", TRUE, &app_name);
            g_object_unref(application);
            g_object_set(cell,"text", app_name,NULL);
            free(app_name);
        }
    }
    else
    {
        g_object_set(cell,"text", ++name,NULL);
    }
    g_free(path);
}

static gboolean panel_resize_cb(XfcePanelPlugin *plugin, gint new_size, gpointer but)
{
    GtkWidget *img = gtk_image_new();
    GdkPixbuf *pb = gdk_pixbuf_new_from_inline(-1, linelightico, FALSE, NULL);
    int size = new_size - 5;
    GdkPixbuf *pb_scaled = gdk_pixbuf_scale_simple(pb,size,size, GDK_INTERP_HYPER);
    gtk_image_set_from_pixbuf(GTK_IMAGE(img), pb_scaled);
    g_object_unref(G_OBJECT(pb));
    g_object_unref(G_OBJECT(pb_scaled));
    gtk_button_set_image(GTK_BUTTON(but), GTK_WIDGET(img));
    gtk_widget_set_size_request(GTK_WIDGET(but), new_size, new_size);
    return TRUE;
}

/*
 *  Search Handling
 */

void* search(void *arg)
{
    char *command;
    FILE *search_process;
    char line[MAX_PATH_LENGTH];

    command = (char*) arg;
    search_process = popen(command, "r");

    if (search_process == NULL)
    {
        xfce_err("locate clould not be executed!");
        return NULL;
    }

    gdk_threads_enter();
    linelight_clear_list(linelight);
    gdk_threads_leave();

    while (NULL!=fgets(line, MAX_PATH_LENGTH, search_process) && g_thread_self() == current_search_thread)
    {
        gdk_threads_enter();
        linelight_add_search_result(linelight, line);
        gdk_threads_leave();

    }

    pclose(search_process);
    free(command);
    g_thread_exit(NULL);
    return NULL;
}

static void start_search(GtkEditable *entry, char *user_data)
{
    const char *search_string;
    char *command;

    search_string = gtk_entry_get_text(GTK_ENTRY(entry));

    if (strlen(search_string) < linelight->min_char_for_search)
    {
        current_search_thread = NULL;
        linelight_clear_list(linelight);
        return;
    }
    command = malloc(sizeof(char) *(20 + strlen(search_string)));
    //strcpy(command, "locate -b \"");
    strcpy(command, "locate -b ");
    strcat(command, gtk_entry_get_text(GTK_ENTRY(entry)));
    //strcat(command, "\"");

    current_search_thread = g_thread_create(search, (void *)command, FALSE, NULL);
}

/*
 *  Mouse, Keyboads and Button Event Methods
 */

static void executeFile(char* path)
{
    GFile           *file;
    GFileInfo       *info;
    GAppInfo        *app;
    GDesktopAppInfo *desktop_app;
    GList           *fileList = NULL;

    file = g_file_new_for_path (path);
    if (file == NULL)
         return;

    info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE, G_FILE_QUERY_INFO_NONE, NULL, NULL);
    if (info == NULL)
    {
        g_object_unref (file);
        return;
    }

    if (strstr(path,".desktop") != 0)
    {
        desktop_app = g_desktop_app_info_new_from_filename (path);
        if (desktop_app != NULL)
        {
            g_app_info_launch (G_APP_INFO (desktop_app), NULL, NULL, NULL);
            g_object_unref (desktop_app);
        }
    }
    else if (g_file_query_file_type (file, G_FILE_QUERY_INFO_NONE, NULL) == G_FILE_TYPE_DIRECTORY)
    {
        char command[strlen(path) + 10];
        strcpy(command, "thunar \"");
        strcat(command, path);
        strcat(command,"\"");
        system(command);
    }
    else
    {
        fileList = g_list_append (fileList, file);

	app = g_app_info_get_default_for_type (g_file_info_get_content_type (info), FALSE);

	g_app_info_launch (app, fileList, NULL, NULL);

        g_object_unref(app);
        g_list_free(fileList);
    }
    g_object_unref (info);
    g_object_unref (file);
}

void* show_all_search_results(void* arg)
{
    const char *search_string;
    FILE *search_process;
    search_string = gtk_entry_get_text(GTK_ENTRY(linelight->text_field));
    char command[20 + strlen(search_string)], line[MAX_PATH_LENGTH], link_command[MAX_PATH_LENGTH * 2];
    //strcpy(command, "locate -b \"");
    strcpy(command, "locate -b ");
    strcat(command, gtk_entry_get_text(GTK_ENTRY(linelight->text_field)));
    //strcat(command, "\"");

    search_process = popen(command, "r");

    if (search_process == NULL)
    {
        xfce_err("locate clould not be executed!");
        return NULL;
    }
    system("rm -r /tmp/xfce4-linelight/");
    system("mkdir /tmp/xfce4-linelight/");
    while (NULL!=fgets(line, MAX_PATH_LENGTH, search_process))
    {
        //remove /n at the end
        if (strlen(line) != 0 && line[strlen(line)-1] == '\n')
            line[strlen(line)-1] = '\0' ;

        sprintf(link_command, "ln -s \"%s\" /tmp/xfce4-linelight/", line);
        system(link_command);
    }

    pclose(search_process);
    system("thunar /tmp/xfce4-linelight/");
    return NULL;
}

//TODO: remove dublicated code
void* show_all_search_results_from_section(void* sec)
{
    const char *search_string;
    FILE *search_process;
    int section;
    search_string = gtk_entry_get_text(GTK_ENTRY(linelight->text_field));
    char command[20 + strlen(search_string)], line[MAX_PATH_LENGTH], link_command[MAX_PATH_LENGTH * 2];
    //strcpy(command, "locate -b \"");
    strcpy(command, "locate -b ");
    strcat(command, gtk_entry_get_text(GTK_ENTRY(linelight->text_field)));
    //strcat(command, "\"");

    search_process = popen(command, "r");

    if (search_process == NULL)
    {
        xfce_err("locate clould not be executed!");
        return NULL;
    }

    system("rm -r /tmp/xfce4-linelight/");
    system("mkdir /tmp/xfce4-linelight/");
    while (NULL!=fgets(line, MAX_PATH_LENGTH, search_process))
    {
        section = linelight_get_result_section(linelight,line);
        if (section == *((int*)sec))
        {
            //remove /n at the end
            // if (strlen(line) != 0 && line[strlen(line)-1] == '\n')
            //    line[strlen(line)-1] = '\0' ;

            sprintf(link_command, "ln -s \"%s\" /tmp/xfce4-linelight/", line);
            system(link_command);
        }
    }

    pclose(search_process);
    system("thunar /tmp/xfce4-linelight/");
    free(sec);
    return NULL;
}

void executeAction(char *text)
{
    int i;

    // more... was selected
    if (strcmp(text,"more...") == 0)
    {
        g_thread_create(show_all_search_results, NULL, FALSE, NULL);
    }

    //A section was selected
    for (i =  0;i<SECTION_COUNT; i++)
    {
        if (strstr(text,sectionNames[i]) != NULL)
        {
            int *section = malloc(sizeof(int));
            *section = i;
            g_thread_create(show_all_search_results_from_section, (void*)section, FALSE, NULL);
        }
    }

    //a path was selected
    executeFile(text);
}

static gboolean tree_mouse_selection (GtkWidget *tree,  GdkEventButton *event, GtkWidget *entry)
{
    if (event->type == GDK_2BUTTON_PRESS)
    {
        GtkTreeSelection *selection;
        GtkTreeIter iter;
        GtkTreeModel *model;
        char *path;

        selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));

        if (gtk_tree_selection_get_selected (selection, &model, &iter))
        {
            gtk_tree_model_get(model, &iter, TEXT_COL, &path, -1);

            executeAction(path);
            g_free(path);
        }
        return TRUE;
    }
    return FALSE;
}

static gboolean tree_selection(GtkWidget *tree, GdkEventKey *event, GtkWidget *entry)
{
    if (event->keyval == GDK_Escape || event->keyval == GDK_F4)
    {
        linelight_toggle_show_hide(linelight);
    }
    else if (event->keyval == GDK_Return)
    {
        GtkTreeSelection *selection;
        GtkTreeIter iter;
        GtkTreeModel *model;
        char *path;

        selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));;

        if (gtk_tree_selection_get_selected (selection, &model, &iter))
        {
            gtk_tree_model_get(model, &iter, TEXT_COL, &path, -1);

            executeAction(path);
            g_free(path);
        }
    }
    else if (event->keyval != GDK_Up && event->keyval != GDK_Down)
    {
        gtk_widget_grab_focus(entry);
        gtk_entry_append_text(GTK_ENTRY(entry),event->string);
        const char *text = gtk_entry_get_text(GTK_ENTRY(entry));
        gtk_editable_select_region(GTK_EDITABLE(entry), strlen(text),strlen(text));
    }
    return TRUE;
}

//callback for the panel button (creates or destroys the main window)
static void open_search_window_cb(GtkButton *button, XfcePanelPlugin* plugin)
{
    linelight_toggle_show_hide(linelight);
}

static gboolean esc_event(GtkWidget *w, GdkEventKey *event, GtkWidget *entry)
{
    if (event->keyval == GDK_Escape || event->keyval == GDK_F4)
    {
        linelight_toggle_show_hide(linelight);
        return TRUE;
    }
    return FALSE;
}

static gboolean delete_event(GtkWidget *w, GdkEvent* event,  gpointer   user_data)
{
    linelight_toggle_show_hide(linelight);
    return TRUE;
}

/*
 *  About Dialog
 */

static void show_about_cb(XfcePanelPlugin *plugin, gpointer userdata)
{
    GtkWidget *dialog;
    XfceAboutInfo *info = xfce_about_info_new("xfce4-linelight-plugin", VERSION, _("A Spotlight-like frontend for locate search."), XFCE_COPYRIGHT_TEXT("2008", "Michael Pfeuti"), XFCE_LICENSE_GPL);

    xfce_about_info_set_homepage(info,"http://www.ganymede.ch");

    dialog = xfce_about_dialog_new_with_values(NULL, info, NULL);
    g_signal_connect(G_OBJECT(dialog), "response", G_CALLBACK(gtk_widget_destroy),NULL);
    gtk_window_set_title(GTK_WINDOW(dialog), "xfce4-linelight");
    gtk_dialog_run(GTK_DIALOG(dialog));

    xfce_about_info_free(info);
}

/*
 *  Persistance and Clean-Up
 */

static void load_data(XfcePanelPlugin *plugin, LineLightData *data)
{
    gchar *file;
    XfceRc *rc;
    int i;
    char sec_enable[] = "section_enabled_#";

    if (!(file = xfce_panel_plugin_lookup_rc_file(plugin)))
        return;

    rc = xfce_rc_simple_open(file,TRUE);
    g_free(file);

    if (!rc)
        return;

    data->max_search_results = xfce_rc_read_int_entry(rc,"max_search_results", 5);
    data->grow_corner = xfce_rc_read_int_entry(rc,"grow_corner",TOP_LEFT_CORNER);
    data->decorated = xfce_rc_read_int_entry(rc,"decorated", TRUE);
    data->x = xfce_rc_read_int_entry(rc,"coord_x", gdk_screen_width()/2);
    data->y = xfce_rc_read_int_entry(rc,"coord_y", gdk_screen_height()/2);
    data->min_char_for_search = xfce_rc_read_int_entry(rc,"min_char_for_search",2);
    for(i = 0;i<SECTION_COUNT;i++)
    {
        sprintf(sec_enable, "section_enabled_%i", i);
        data->section_enabled[i] = xfce_rc_read_int_entry(rc,sec_enable, TRUE);
    }

    xfce_rc_close(rc);
}

static void save_data_cb(XfcePanelPlugin *plugin, gpointer userdata)
{
    gchar *file;
    XfceRc *rc;
    int i;
    char sec_enable[] = "section_enabled_#";

    if (!(file = xfce_panel_plugin_save_location(plugin, TRUE)))
        return;

    rc = xfce_rc_simple_open(file,FALSE);
    g_free(file);

    if (!rc)
        return;

    xfce_rc_write_int_entry(rc,"max_search_results", linelight->max_search_results);
    xfce_rc_write_int_entry(rc,"grow_corner", (int) linelight->grow_corner);
    xfce_rc_write_int_entry(rc,"decorated", linelight->decorated);
    xfce_rc_write_int_entry(rc,"coord_x", linelight->x);
    xfce_rc_write_int_entry(rc,"coord_y", linelight->y);
    xfce_rc_write_int_entry(rc,"min_char_for_search",linelight->min_char_for_search);
    for(i = 0;i<SECTION_COUNT;i++)
    {
        sprintf(sec_enable, "section_enabled_%i", i);
        xfce_rc_write_int_entry(rc,sec_enable, linelight->section_enabled[i]);
    }

    xfce_rc_close(rc);
}

static void clean_up_cb(XfcePanelPlugin *plugin, gpointer userdata)
{
    save_data_cb(plugin, userdata);
    linelight_free(linelight);
    system("rm -r /tmp/xfce4-linelight/");
    gdk_threads_leave ();
}

/*
 *  Properties Frame and Callback Methods
 */

static void set_max_results_value_cb(GtkWidget *spinner, gpointer userdata)
{
    linelight->max_search_results = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner));
}

static void min_char_for_search_value_cb(GtkWidget *spinner, gpointer userdata)
{
    linelight->min_char_for_search = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner));
}

static void toggle_window_decoration_cb(GtkWidget *but, gpointer userdata)
{
    linelight->decorated = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(but));
    gtk_window_set_decorated(GTK_WINDOW (linelight->text_window), linelight->decorated);
}

static void section_selection_cb(GtkWidget *but, gpointer userdata)
{
    const char *section = gtk_button_get_label(GTK_BUTTON(but));
    if (strcmp("Music", section) == 0)
        linelight->section_enabled[MUSIC_SECTION] = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(but));
    if (strcmp("Videos", section) == 0)
        linelight->section_enabled[VIDEO_SECTION] = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(but));
    if (strcmp("Pictures", section) == 0)
        linelight->section_enabled[PICTURES_SECTION] = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(but));
    if (strcmp("Applications", section) == 0)
        linelight->section_enabled[BIN_SECTION] = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(but));
    if (strcmp("Files", section) == 0)
        linelight->section_enabled[FILES_SECTION] = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(but));
    if (strcmp("Folders", section) == 0)
        linelight->section_enabled[FOLDER_SECTION] = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(but));
    //if(strcmp("Web", section) == 0)
    //  linelight->section_enabled[WEB_SECTION] = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(but));
}

static void grow_corner_selection_cb(GtkWidget *but, gpointer userdata)
{
    const char *section = gtk_button_get_label(GTK_BUTTON(but));
    if (strcmp("Top Right", section) == 0)
        linelight->grow_corner = TOP_RIGHT_CORNER;
    if (strcmp("Top Left", section) == 0)
        linelight->grow_corner = TOP_LEFT_CORNER;
    if (strcmp("Bottom Right", section) == 0)
        linelight->grow_corner = BOTTOM_RIGHT_CORNER;
    if (strcmp("Bottom Left", section) == 0)
        linelight->grow_corner = BOTTOM_LEFT_CORNER;
}

static void showProperties_cb(XfcePanelPlugin *plugin, gpointer userdata)
{
    linelight_update_coordinates(linelight);
    gtk_widget_hide(linelight->text_window);
    xfce_panel_plugin_block_menu(plugin);

    GtkWidget *dialog = gtk_dialog_new_with_buttons("Properties", GTK_WINDOW(plugin), GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR, GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, NULL);

    gtk_window_set_position(GTK_WINDOW(dialog), GTK_WIN_POS_CENTER);
    g_signal_connect(dialog, "response", G_CALLBACK(gtk_widget_destroy), dialog);

    GtkWidget *table = gtk_table_new(3,4,FALSE);
    gtk_table_set_col_spacing(GTK_TABLE(table),0,5);

    //left side of the dialog
    GtkWidget *section_frame = gtk_frame_new("Section Selection");
    GtkWidget *section_selection = gtk_vbox_new(FALSE,5);

    GtkWidget *box = gtk_check_button_new_with_label("Music");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(box),linelight->section_enabled[MUSIC_SECTION]);
    g_signal_connect(box,"toggled",G_CALLBACK(section_selection_cb),NULL);
    gtk_container_add(GTK_CONTAINER(section_selection), box);

    box = gtk_check_button_new_with_label("Videos");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(box),linelight->section_enabled[VIDEO_SECTION]);
    g_signal_connect(box,"toggled",G_CALLBACK(section_selection_cb),NULL);
    gtk_container_add(GTK_CONTAINER(section_selection), box);

    box = gtk_check_button_new_with_label("Pictures");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(box),linelight->section_enabled[PICTURES_SECTION]);
    g_signal_connect(box,"toggled",G_CALLBACK(section_selection_cb),NULL);
    gtk_container_add(GTK_CONTAINER(section_selection), box);

    //box = gtk_check_button_new_with_label("Web");
    //gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(box),linelight->section_enabled[WEB_SECTION]);
    //g_signal_connect(box,"toggled",G_CALLBACK(section_selection_cb),NULL);
    //gtk_container_add(GTK_CONTAINER(section_selection), box);

    box = gtk_check_button_new_with_label("Applications");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(box),linelight->section_enabled[BIN_SECTION]);
    g_signal_connect(box,"toggled",G_CALLBACK(section_selection_cb),NULL);
    gtk_container_add(GTK_CONTAINER(section_selection), box);

    box = gtk_check_button_new_with_label("Files");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(box),linelight->section_enabled[FILES_SECTION]);
    g_signal_connect(box,"toggled",G_CALLBACK(section_selection_cb),NULL);
    gtk_container_add(GTK_CONTAINER(section_selection), box);

    box = gtk_check_button_new_with_label("Folders");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(box),linelight->section_enabled[FOLDER_SECTION]);
    g_signal_connect(box,"toggled",G_CALLBACK(section_selection_cb),NULL);
    gtk_container_add(GTK_CONTAINER(section_selection), box);

    gtk_container_add(GTK_CONTAINER(section_frame), section_selection);
    gtk_table_attach(GTK_TABLE(table), section_frame,0,1,0,4,GTK_FILL,GTK_FILL,0,0);

    //right side of the dialog
    GtkWidget *decoration_label = gtk_label_new("Show Window Decoration: ");
    gtk_misc_set_alignment(GTK_MISC(decoration_label), 0, 0.5);
    gtk_table_attach(GTK_TABLE(table), decoration_label,1,2,0,1,GTK_FILL,GTK_FILL,0,0);
    GtkWidget *decoration_checkbox = gtk_check_button_new();
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(decoration_checkbox), gtk_window_get_decorated(GTK_WINDOW(linelight->text_window)));
    g_signal_connect(decoration_checkbox,"toggled",G_CALLBACK(toggle_window_decoration_cb),NULL);
    gtk_table_attach(GTK_TABLE(table), decoration_checkbox,2,3,0,1,GTK_FILL,GTK_FILL,0,0);

    GtkWidget *spinner_label = gtk_label_new("Max Results Per Section: ");
    gtk_misc_set_alignment(GTK_MISC(spinner_label), 0, 0.5);
    gtk_table_attach(GTK_TABLE(table), spinner_label,1,2,1,2,GTK_FILL,GTK_FILL,0,0);
    GtkWidget *spinner = gtk_spin_button_new_with_range(1,20,1.0);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinner), (double) linelight->max_search_results);
    g_signal_connect(spinner,"value-changed",G_CALLBACK(set_max_results_value_cb),NULL);
    gtk_table_attach(GTK_TABLE(table), spinner,2,3,1,2,GTK_FILL,GTK_FILL,0,0);

    spinner_label = gtk_label_new("Min Character to Search: ");
    gtk_misc_set_alignment(GTK_MISC(spinner_label), 0, 0.5);
    gtk_table_attach(GTK_TABLE(table), spinner_label,1,2,2,3,GTK_FILL,GTK_FILL,0,0);
    spinner = gtk_spin_button_new_with_range(1,20,1.0);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinner), (double) linelight->min_char_for_search);
    g_signal_connect(spinner,"value-changed",G_CALLBACK(min_char_for_search_value_cb),NULL);
    gtk_table_attach(GTK_TABLE(table), spinner,2,3,2,3,GTK_FILL,GTK_FILL,0,0);

    GtkWidget *corner_frame = gtk_frame_new("Fix Corner When Resizing");
    GtkWidget *corner_selection = gtk_vbox_new(FALSE,5);

    GtkWidget *radio = gtk_radio_button_new_with_label(NULL,"Top Left");
    g_signal_connect(radio,"toggled",G_CALLBACK(grow_corner_selection_cb),NULL);
    if (linelight->grow_corner == TOP_LEFT_CORNER)
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), TRUE);
    gtk_container_add(GTK_CONTAINER(corner_selection), radio);

    radio = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(radio),"Top Right");
    g_signal_connect(radio,"toggled",G_CALLBACK(grow_corner_selection_cb),NULL);
    if (linelight->grow_corner == TOP_RIGHT_CORNER)
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), TRUE);
    gtk_container_add(GTK_CONTAINER(corner_selection), radio);

    radio = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(radio),"Bottom Left");
    g_signal_connect(radio,"toggled",G_CALLBACK(grow_corner_selection_cb),NULL);
    if (linelight->grow_corner == BOTTOM_LEFT_CORNER)
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), TRUE);
    gtk_container_add(GTK_CONTAINER(corner_selection), radio);

    radio = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(radio),"Bottom Right");
    g_signal_connect(radio,"toggled",G_CALLBACK(grow_corner_selection_cb),NULL);
    if (linelight->grow_corner == BOTTOM_RIGHT_CORNER)
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), TRUE);
    gtk_container_add(GTK_CONTAINER(corner_selection), radio);

    gtk_container_add(GTK_CONTAINER(corner_frame), corner_selection);
    gtk_table_attach(GTK_TABLE(table), corner_frame,1,3,3,4,GTK_FILL,GTK_FILL,0,0);


    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), table);
    gtk_widget_show_all(dialog);
    gtk_window_set_resizable(GTK_WINDOW(dialog),FALSE);
    gtk_dialog_run(GTK_DIALOG(dialog));

    linelight_clear_list(linelight);
    xfce_panel_plugin_unblock_menu(plugin);
}

/*
 *  APPLICATION START METHODS
 */

#ifdef __DEBUG__
int main (int argc, char *argv[])
{
    g_type_init ();
    g_thread_init (NULL);
    gdk_threads_init();
    gdk_threads_enter();

    gtk_init (&argc, &argv);


    //main window
    GtkWidget *text_window =  gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_resizable(GTK_WINDOW(text_window),FALSE);
    g_signal_connect(GTK_WIDGET(text_window), "size-request", G_CALLBACK(move_action_cb), NULL);
    GtkListStore *list = gtk_list_store_new(NUM_COLUMS,GDK_TYPE_PIXBUF, G_TYPE_STRING);
    //gtk_window_set_decorated(GTK_WINDOW (text_window), FALSE);
    //gtk_window_set_accept_focus(GTK_WINDOW(text_window), TRUE);
    //g_object_set(text_window, "has-toplevel-focus",TRUE,NULL);
    //g_object_set(text_window, "focus-on-map",TRUE,NULL);
    linelight = linelight_new(text_window, list);
    gtk_container_add(GTK_CONTAINER(text_window), create_windows_content(list));
    showProperties_cb(NULL,NULL);

    gtk_widget_show_all(text_window);
    gtk_widget_grab_focus(text_window);


    gtk_main ();
    gdk_threads_leave ();
    return 0;
}
#else
// callback for xfce panel (executed on plugin addition)
static void plugin_create(XfcePanelPlugin *plugin)
{
    g_type_init ();   
    g_thread_init (NULL);
    gdk_threads_init();
    gdk_threads_enter();

    xfce_panel_plugin_menu_show_configure(plugin);
    xfce_panel_plugin_menu_show_about(plugin);

    GtkWidget *button = xfce_create_panel_button();
    gtk_container_add(GTK_CONTAINER(plugin), button);
    xfce_panel_plugin_add_action_widget(plugin, button);
    panel_resize_cb(plugin,xfce_panel_plugin_get_size(plugin), button);
    gtk_widget_show_all(button);

    g_signal_connect(plugin, "free-data", G_CALLBACK(clean_up_cb), NULL);
    g_signal_connect(plugin, "about", G_CALLBACK(show_about_cb), NULL);
    g_signal_connect(plugin, "configure-plugin", G_CALLBACK(showProperties_cb), NULL);
    g_signal_connect(plugin, "save", G_CALLBACK(save_data_cb), NULL);
    g_signal_connect(button, "clicked", G_CALLBACK(open_search_window_cb), button);
    g_signal_connect (plugin, "size-changed", G_CALLBACK(panel_resize_cb), button);
    gtk_tooltips_set_tip(gtk_tooltips_new(), button, "Linelight", NULL);

    //main window
    GtkWidget *text_window =  gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_resizable(GTK_WINDOW(text_window),FALSE);
    gtk_window_set_deletable(GTK_WINDOW(text_window),FALSE);
    gtk_window_set_keep_above(GTK_WINDOW(text_window),TRUE);
    gtk_window_set_skip_taskbar_hint(GTK_WINDOW(text_window),TRUE);
    g_signal_connect(GTK_WIDGET(text_window), "size-request", G_CALLBACK(move_action_cb), plugin);
    GtkListStore *list = gtk_list_store_new(NUM_COLUMS,GDK_TYPE_PIXBUF, G_TYPE_STRING);
    linelight = linelight_new(text_window, list);
    gtk_container_add(GTK_CONTAINER(text_window), create_windows_content(list));
    g_signal_connect(text_window, "delete-event", G_CALLBACK(delete_event), NULL);

    load_data(plugin,linelight);
    linelight_clear_list(linelight);
    if(linelight->decorated)
        gtk_window_set_decorated(GTK_WINDOW (text_window), TRUE);
    else
        gtk_window_set_decorated(GTK_WINDOW (text_window), FALSE);

}


XFCE_PANEL_PLUGIN_REGISTER_EXTERNAL(plugin_create);

#endif
