/*
     This file is part of GNUnet.
     (C) 2010-2013 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/



/**
 * @file src/conversation/gnunet-conversation-gtk_history.c
 * @brief 
 * @author yids
 * @author hark
 */

#include "gnunet-conversation-gtk_common.h"
#include "gnunet-conversation-gtk.h"


/*******
 * history
 ********/
/**
 * call history liststore
 */
static GtkListStore *history_liststore;

/**
 * call history treestore
 */
static GtkTreeStore *history_treestore;

/**
 * call histore treeview
 */
static GtkTreeView *history_treeview;

/**
 * call history tree model
 */
static GtkTreeModel *history_treemodel;


/*
 *
 * adds a item to the call history
 *
 * @param type type of call: 0: accepted 1: rejected 2: outgoing call
 * @return void
 */

extern void
GNUNET_CONVERSATION_GTK_history_add (int type, char *contactName)
{
  GtkTreeIter iter;
  time_t t;
  char *event;

  switch (type)
  {
  case CH_ACCEPTED:
    event = "Accepted";
    break;
  case CH_REJECTED:
    event = "Rejected";
    break;
  case CH_OUTGOING:
    event = "Outgoing";
    break;
  case CH_HANGUP:
    event = "Hangup";
    break;
  case CH_MISSED:
    event = "Missed";
    break;
  default:
    event = "UNKNOWN";
    break;
  }
  time (&t);
  gtk_list_store_append (history_liststore, &iter);
  gtk_list_store_set (history_liststore, &iter, 1, event, 0, ctime (&t), 2,
                      contactName, -1);
}

extern void
GNUNET_CONVERSATION_GTK_history_init(){
  // call history
  history_liststore =
      GTK_LIST_STORE (get_object ("gnunet_conversation_gtk_history_liststore"));
  history_treeview =
      GTK_TREE_VIEW (get_object ("gnunet_conversation_gtk_history_treeview"));
  history_treestore =
      GTK_TREE_STORE (get_object ("gnunet_conversation_gtk_history_treestore"));
  history_treemodel = GTK_TREE_MODEL (history_treestore);

}
