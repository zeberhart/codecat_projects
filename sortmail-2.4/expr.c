#ifndef lint
static const char sccsid[] = "@(#)expr.c 1.4 02/03/11 falk" ;
static const char rcsid[] = "$Id: expr.c,v 1.4 2003/12/22 06:03:14 efalk Exp $" ;
#endif

#include <stdio.h>
#include <stdlib.h>	/* defines getenv(3) */
#include <pwd.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <sys/param.h>

#include "sortmail.h"
#include "utils.h"



/* This is a recursive-descent parser for the expressions used in
 * sortmail.  I find these easier to write than to learn yacc.
 *
 * The grammar for these expressions is as follows:
 *
 *	expression ::=	<commaExpr>
 *
 *	commaExpr ::=	<orExpr>
 *			<orExpr> , <commaExpr>
 *
 *	orExpr ::=	<andExpr>
 *			<andExpr> || <orExpr>
 *
 *	andExpr ::=	<eqExpr>
 *			<eqExpr> && <andExpr>
 *
 *	eqExpr ::=	<cmpExpr>
 *			<cmpExpr> == <eqExpr>
 *			<cmpExpr> != <eqExpr>
 *
 *	cmpExpr ::=	<addExpr>
 *			<addExpr> <= <cmpExpr>
 *			<addExpr> >= <cmpExpr>
 *			<addExpr> < <cmpExpr>
 *			<addExpr> > <cmpExpr>
 *
 *	addExpr ::=	<mulExpr>
 *			<mulExpr> + <addExpr>
 *			<mulExpr> - <addExpr>
 *
 *	mulExpr ::=	<notExpr>
 *			<notExpr> * <mulExpr>
 *			<notExpr> / <mulExpr>
 *
 *	notExpr ::=	<value>
 *			! <value>
 *
 *	value ::=	<constant>
 *			$<variable>
 *			/<pattern>/<options>
 *			[digits]<options>		ip addr or block
 *			[digits/digits]<options>	ip range
 *			[digits - digits]<options>	ip range
 *			(<expression>)<options>
 *
 *	variable ::=	<varname>
 *			{<varname>}
 *
 *	varname ::=	<alpha><alphanum>
 *
 *	alpha ::=	[A-Za-z]
 *
 *	alphanum ::=	[A-Za-z0-9_]*
 *
 *	constant ::=	[0-9]+
 *
 *	options ::=	<option><options>
 *			(empty)
 *
 *	option ::=	:m
 *			:m <email>
 *			:j
 *			:e <constant>
 *			:E <constant>
 *			:k
 *			:f <folder>
 *			:+<path>
 *			:d <folder>
 *			:a <folder>
 *			:| <command>
 *			:c
 *
 *	email ::=	[A-Za-z!@#$%^();.,]+
 *	command ::=	<anything but : or newline>+
 *
 *	folder ::=	</path>
 *			<~/path>
 *			<~name/path>
 *			<+path>
 */


#define	LE	1	/* <= */
#define	GE	2	/* >= */
#define	OR1	3	/* | */
#define	AND1	4	/* & */


static	Expression	*readExpr( Ictx *in ) ;
static	Expression	*parenExpr( Ictx *in ) ;
static	Expression	*commaExpr( Ictx *in ) ;
static	Expression	*orExpr( Ictx *in ) ;
static	Expression	*andExpr( Ictx *in ) ;
static	Expression	*eqExpr( Ictx *in ) ;
static	Expression	*cmpExpr( Ictx *in ) ;
static	Expression	*addExpr( Ictx *in ) ;
static	Expression	*mulExpr( Ictx *in ) ;
static	Expression	*notExpr( Ictx *in ) ;
static	Expression	*valExpr( Ictx *in ) ;
static	Expression	*constExpr( Ictx *in ) ;
static	Expression	*varExpr( Ictx *in ) ;
static	char		*varName( Ictx *in) ;
static	Expression	*patExpr( Ictx *in ) ;
static	int		patMatch(PatExpr *pat, MailMessage *msg) ;
#ifndef	NOREGCOMP
static	Expression	*ipExpr( Ictx *in ) ;
static	int		ipMatch(PatExpr *pat, MailMessage *msg) ;
#endif

static	char	*get_cmds( Ictx *in ) ;
static	void	reportMemError( Ictx *in ) ;

static	int	parenHandler( ExprOp op, Expression *expr, MailMessage * ) ;
static	int	binHandler( ExprOp op, Expression *expr, MailMessage * ) ;
static	int	notHandler( ExprOp op, Expression *expr, MailMessage * ) ;
static	int	constHandler( ExprOp op, Expression *expr, MailMessage * ) ;
static	int	varHandler( ExprOp op, Expression *expr, MailMessage * ) ;
static	int	patHandler( ExprOp op, Expression *expr, MailMessage * ) ;
#ifndef	NOREGCOMP
static	int	ipHandler( ExprOp op, Expression *expr, MailMessage * ) ;
#endif


void
ExpressionAdd(Ictx *in)
{
	ExprInfo *info ;

	info = MALLOC(ExprInfo, 1) ;
	if( info == NULL ) {
	  logFile("out of memory reading expression at line %d\n",
	  	in->lineno) ;
	  return ;
	}

	info->c.lineno = in->lineno ;

	in->lineno0 = in->lineno ;

	advanceNext(in) ;

	/* Programming note:  The rules of this grammar state that I could
	 * just parse a ',' expression and get the right results.  However,
	 * I choose to look at the input and select a () expression or
	 * /pattern/ accordingly.  There are two reasons:  First, it
	 * will be much more efficient.  Second, I want to handle the
	 * case where a () expression is followed on the next line by
	 * a /pattern/.  Otherwise, the parser would read a division:
	 *	(expression) /pattern
	 *
	 * As you may guess, the sortmail config file is not a proper
	 * LL-1 grammar.
	 */

	if( *in->ptr == '(' ) {
	  info->c.type = EXPRESSION ;
	  info->expr = parenExpr(in) ;
	}
	else if( *in->ptr == '/' ) {
	  info->c.type = PATTERN ;
	  info->expr = patExpr(in) ;
	}
#ifndef	NOREGCOMP
	else if( *in->ptr == '[' ) {
	  info->c.type = IPPATTERN ;
	  info->expr = ipExpr(in) ;
	}
#endif
	else {
	  advanceLine(in) ;
	  return ;
	}

	if( *in->ptr != EOF && in->ptr != in->line ) {
	  /* Look for extraneous characters on the line, this usually
	   * indicates user error.
	   */
	  while( isspace(*in->ptr) )
	    ++in->ptr;
	  if( *in->ptr != '\0' ) {
	    logFile("syntax error line %d, \"%s\" unrecognized\n",
	      in->lineno, in->ptr) ;
	    free(info);
	    return;
	  }
	  advanceLine(in) ;
	}

	if( info->expr == NULL ) {
	  free(info) ;
	  return ;
	}

	append_cmd((Cmd *)info) ;
}


	/* NOTE:  In the functions which follow, it is assumed
	 * (and required) that the input has been advanced to the
	 * next token.
	 */


	/* expression ::= <commaExpr> */

static	Expression *
readExpr(Ictx *in)
{
	assert(*in->ptr != ' ') ;

	if( *in->ptr == '\0' ) {
	  logFile("syntax error line %d\n", in->lineno) ;
	  if( in->lineno != in->lineno0 )
	    logFile("  expression starts at line %d\n", in->lineno0) ;
	  return NULL ;
	}

	return commaExpr(in) ;
}



	/* common code to most binary functions */

static	Expression *
binCommon(Ictx *in, Expression *left, Expression *right, char type)
{
	BinExpr		*new ;

	if( right == NULL ) {
	  left->handler(E_FREE, left, NULL) ;
	  return NULL ;
	}

	new = MALLOC(BinExpr, 1) ;
	if( new == NULL ) {
	  left->handler(E_FREE, left, NULL) ;
	  right->handler(E_FREE, right, NULL) ;
	  reportMemError(in) ;
	  return NULL ;
	}

	new->e.type = type ;
	new->e.handler = binHandler ;
	new->left = left ;
	new->right = right ;

	return (Expression *)new ;
}



	/* comma expression ::= <or-expr>
	 *			<or-expr> , <comma-expr>
	 */

static	Expression *
commaExpr(Ictx *in)
{
	Expression	*left, *right ;

	if( (left = orExpr(in)) == NULL  ||  *in->ptr != ',' )
	  return left ;

	advance(in, 1) ; advanceNext(in) ;

	right = commaExpr(in) ;

	return binCommon(in, left, right, ',') ;
}



	/* or expression ::=	<and-expr>
	 *			<and-expr> || <or-expr>
	 */

static	Expression *
orExpr(Ictx *in)
{
	Expression	*left, *right ;
	char		type ;
	int		len ;

	if( (left = andExpr(in)) == NULL )
	  return NULL ;

	if( strncmp(in->ptr, "||",2) == 0 )
	  { type = '|' ; len = 2 ; }
	else if( strncmp(in->ptr, "|",1) == 0 )
	  { type = OR1 ; len = 1 ; }
	else
	  return left ;

	advance(in, len) ; advanceNext(in) ;

	right = orExpr(in) ;

	return binCommon(in, left, right, type) ;
}



	/* and expression ::=	<eq-expr>
	 *			<eq-expr> && <and-expr>
	 */

static	Expression *
andExpr(Ictx *in)
{
	Expression	*left, *right ;
	char		type ;
	int		len ;

	if( (left = eqExpr(in)) == NULL )
	  return NULL ;

	if( strncmp(in->ptr, "&&",2) == 0 )
	  { type = '&' ; len = 2 ; }
	else if( strncmp(in->ptr, "&",1) == 0 )
	  { type = AND1 ; len = 1 ; }
	else
	  return left ;

	advance(in, len) ; advanceNext(in) ;

	right = andExpr(in) ;

	return binCommon(in, left, right, type) ;
}



	/* eq expression ::=	<cmp-expr>
	 *			<cmp-expr> == <eq-expr>
	 *			<cmp-expr> != <eq-expr>
	 */

static	Expression *
eqExpr(Ictx *in)
{
	Expression	*left, *right ;
	char		type ;

	if( (left = cmpExpr(in)) == NULL )
	  return NULL ;

	if( strncmp(in->ptr, "==", 2) == 0 )
	  type = '=' ;
	else if( strncmp(in->ptr, "!=", 2) == 0 )
	  type = '!' ;
	else
	  return left ;

	advance(in, 2) ; advanceNext(in) ;
	right = eqExpr(in) ;
	return binCommon(in, left, right, type) ;
}



	/* cmp expression ::=	<add-expr>
	 *			<add-expr> <= <cmp-expr>
	 *			<add-expr> >= <cmp-expr>
	 *			<add-expr> < <cmp-expr>
	 *			<add-expr> > <cmp-expr>
	 */

static	Expression *
cmpExpr(Ictx *in)
{
	Expression	*left, *right ;
	char		type ;
	int		adv ;

	if( (left = addExpr(in)) == NULL )
	  return NULL ;

	if( strncmp(in->ptr, "<=", 2) == 0 )
	  adv=2, type = LE ;
	else if( strncmp(in->ptr, ">=", 2) == 0 )
	  adv=2, type = GE ;
	else if( *in->ptr == '<' )
	  adv=1, type = '<' ;
	else if( *in->ptr == '>' )
	  adv=1, type = '>' ;
	else
	  return left ;

	advance(in, adv) ; advanceNext(in) ;
	right = cmpExpr(in) ;
	return binCommon(in, left, right, type) ;
}



	/* HITCH:  All of the binary parsers above build trees which
	 * are heavy on the right.  E.g: ( a || b || c ) becomes
	 * ( a || ( b || c ) ).  This works because the operators in
	 * question are all associative.  It fails for subtraction and
	 * division.  We don't want (a-b+c) to become (a-(b+c)).
	 *
	 * We fix the problem by defining the grammar productions more
	 * properly.
	 */




	/* add expression ::=	<mulExpr><aList>
	 *
	 * aList ::= +<mulExpr><aList>
	 *	     -<mulExpr><alist>
	 *	     (empty)
	 *
	 * Note that these need to be evaluated strictly left -> right
	 */

static	Expression *
addExpr(Ictx *in)
{
	Expression	*left, *right ;
	char		type ;

	if( (left = mulExpr(in)) == NULL )
	  return NULL ;

	while( (type = *in->ptr) == '+' || type == '-' )
	{
	  advance(in, 1) ; advanceNext(in) ;
	  right = mulExpr(in) ;
	  if( (left = binCommon(in, left, right, type)) == NULL )
	    return NULL ;
	}

	return left ;
}



	/* mul expression ::=	<not-expr>
	 *			<not-expr> * <not-expr> ...
	 *			<not-expr> / <not-expr> ...
	 */

static	Expression *
mulExpr(Ictx *in)
{
	Expression	*left, *right ;
	char		type ;

	if( (left = notExpr(in)) == NULL )
	  return NULL ;


	while( (type = *in->ptr) == '*' || type == '/' )
	{
	  advance(in, 1) ; advanceNext(in) ;
	  right = notExpr(in) ;
	  if( (left = binCommon(in, left, right, type)) == NULL )
	    return NULL ;
	}

	return left ;
}



	/* not expression ::=	<val-expr>
	 *			! <not-expr>
	 */

static	Expression *
notExpr(Ictx *in)
{
	UnaryExpr	*not ;

	if( *in->ptr != '!' )
	  return valExpr(in) ;

	advance(in,1) ;
	advanceNext(in) ;

	not = MALLOC(UnaryExpr, 1) ;
	if( not == NULL ) {
	  reportMemError(in) ;
	  return NULL ;
	}

	not->e.type = '~' ;
	not->e.handler = notHandler ;
	not->left = notExpr(in) ;
	if( not->left == NULL ) {
	  free(not) ;
	  return NULL ;
	}

	return (Expression *)not ;
}



	/* val expression ::=	<constant>
	 *			$<variable>
	 *			/<pattern>/
	 *			(<paren-expression>
	 */

static	Expression *
valExpr(Ictx *in)
{
	if( *in->ptr == '-' || isdigit(*in->ptr) )
	  return constExpr(in) ;
	if( *in->ptr == '$' )
	  return varExpr(in) ;
	if( *in->ptr == '/' )
	  return patExpr(in) ;
#ifndef	NOREGCOMP
	if( *in->ptr == '[' )
	  return ipExpr(in) ;
#endif
	if( *in->ptr == '(' )
	  return parenExpr(in) ;

	logFile("syntax error line %d\n", in->lineno) ;
	if( in->lineno != in->lineno0 )
	  logFile("  expression starts at line %d\n", in->lineno0) ;
	return NULL ;
}



	/* constant ::=	[-]<digits> */

static	Expression *
constExpr(Ictx *in)
{
	ConstExpr	*con ;

	con = MALLOC(ConstExpr, 1) ;
	if( con == NULL ) {
	  reportMemError(in) ;
	  return NULL ;
	}

	con->e.type = 'C' ;
	con->e.handler = constHandler ;
	con->value = strtol(in->ptr, &in->ptr, 0) ;

	advanceNext(in) ;

	return (Expression *)con ;
}



	/* variable ::= $<[a-zA-Z0-9_]*> */


static	Expression *
varExpr(Ictx *in)
{
	VarExpr	*var ;
	char	*ptr ;

	assert(*in->ptr == '$') ;

	advance(in,1) ;

	if( *in->ptr == '{' ) {
	  advance(in,1) ;
	  if( (ptr = varName(in)) == NULL )
	    return NULL ;
	  if( *in->ptr != '}' ) {
	    logFile("variable name missing '}', line %d\n", in->lineno) ;
	    if( in->lineno != in->lineno0 )
	      logFile("  expression starts at line %d\n", in->lineno0);
	  }
	  advance(in,1) ;
	}
	else if( isalpha(*in->ptr) ) {
	  if( (ptr = varName(in)) == NULL )
	    return NULL ;
	}
	else {
	  logFile("invalid variable name \"$%s\", line %d\n",
	    in->ptr, in->lineno) ;
	  return NULL ;
	}

	advanceNext(in) ;


	var = MALLOC(VarExpr, 1) ;
	if( var == NULL ) {
	  free(ptr) ;
	  reportMemError(in) ;
	  return NULL ;
	}

	var->e.type = 'V' ;
	var->e.handler = varHandler ;
	var->var = ptr ;

	return (Expression *)var ;
}



static	char *
varName(Ictx *in)
{
	char	*ptr ;
	int	len ;

	for(ptr = in->ptr ; isalnum(*ptr) || *ptr == '_'; ++ptr) ;
	len = ptr - in->ptr ;

	ptr = malloc(len+1) ;
	if( ptr == NULL ) {
	  reportMemError(in) ;
	  return NULL ;
	}

	memcpy(ptr, in->ptr, len) ;
	ptr[len] = '\0' ;

	in->ptr += len ;

	return ptr ;
}



	/* pattern ::= /<regularexpression>/<commands> */

static	void	exprModifiers() ;

static	Expression *
patExpr(Ictx *in)
{
	PatExpr	*pat ;

	char	buffer[2048] ;		/* TODO! should be unlimited */
register char	*optr = buffer ;
register int	done, done2 ;
	int	has_var = 0 ;

	assert(*in->ptr == '/') ;

	pat = MALLOC(PatExpr, 1) ;
	if( pat == NULL ) {
	  reportMemError(in) ;
	  return NULL ;
	}

	pat->e.type = 'P' ;
	pat->e.handler = patHandler ;
	pat->pattern = NULL ;
	pat->compiled = False ;
	pat->flags = 0 ;
	pat->command = NULL ;


	advance(in,1) ;			/* skip leading '/' */

	/* Note: since we require patterns to be contained on one line,
	 * we'll just do pointer arithmatic on in->ptr
	 */

	for(done=0;!done;)
	  switch( *in->ptr ) {
	    case '\0':
	      logFile("line %d: missing '/' in pattern\n", in->lineno) ;
	      free(pat) ;
	      return NULL ;

	    case '\\':			/* '\' quotes anything except '\0' */
	      *optr++ = *in->ptr++ ;
	      if( *optr != '\0' )
		*optr++ = *in->ptr++ ;
	      break ;

	    case '/':			/* '/' terminates */
	      advance(in,1) ;
	      done = 1 ;
	      break ;

	    case '[':			/* '[' starts a set */
	      *optr++ = *in->ptr++ ;
	      for(done2=0; !done2;)
		switch(*in->ptr) {
		  case '\0':		/* premature EOS, reject */
		    logFile("line %d: missing ']' in pattern\n", in->lineno) ;
		    return NULL ;
		  case ']':		/* ']' terminates a set */
		    done2=1 ;
		    *optr++ = *in->ptr++ ;
		    break ;
		  default:
		    *optr++ = *in->ptr++ ;
		    break ;
		}
	      break ;

	    case '$':
	      has_var = 1 ;

	    default:
	      *optr++ = *in->ptr++ ;
	  }
	*optr++ = '\0' ;

	pat->pattern = strdup(buffer) ;
	if( pat->pattern == NULL ) {
	  free(pat) ;
	  reportMemError(in) ;
	  return NULL ;
	}

	exprModifiers(in, pat, has_var) ;
#ifdef	COMMENT
	/* parse modifiers part */

	pat->flags = 0 ;

	for(done=0;!done;)
	  switch( *in->ptr ) {
	    case 't': pat->flags |= P_TO ; ++in->ptr ; break ;
	    case 'f': pat->flags |= P_FROM ; ++in->ptr ; break ;
	    case 's': pat->flags |= P_SUBJECT ; ++in->ptr ; break ;
	    case 'h': pat->flags |= P_HEADER ; ++in->ptr ; break ;
	    case 'r': pat->flags |= P_RECEIVED ; ++in->ptr ; break ;
	    case 'a': pat->flags |= P_ALL ; ++in->ptr ; break ;
	    case 'i': pat->flags |= P_IGNORECASE ; ++in->ptr ; break ;
	    case 'o': pat->flags |= P_ONCE ; ++in->ptr ; break ;
	    default: done=1 ; break ;
	  }

	if( (pat->flags & P_SEARCH) == 0 )
	  pat->flags |= P_SUBJECT ;

	if( pat->flags & P_ALL )
	  pat->flags &= ~(P_HEADER|P_TO|P_FROM|P_SUBJECT|P_RECEIVED) ;
	else if( pat->flags & P_HEADER )
	  pat->flags &= ~(P_TO|P_FROM|P_SUBJECT|P_RECEIVED) ;

	if( has_var && (pat->flags & P_ONCE) ) {
	  char *converted = variable_expand(pat->pattern, True) ;
	  if( converted != NULL ) {
	    free(pat->pattern) ;
	    pat->pattern = converted ;
	  }
	}
	else if( !has_var )
	  pat->flags |= P_ONCE ;

	pat->command = get_cmds(in) ;
	advanceNext(in) ;
#endif	/* COMMENT */

	return (Expression *)pat ;
}


static	void
exprModifiers(Ictx *in, PatExpr *pat, int has_var)
{
	int	done ;

	/* parse modifiers part */

	pat->flags = 0 ;

	for(done=0;!done;)
	  switch( *in->ptr ) {
	    case 't': pat->flags |= P_TO ; ++in->ptr ; break ;
	    case 'f': pat->flags |= P_FROM ; ++in->ptr ; break ;
	    case 's': pat->flags |= P_SUBJECT ; ++in->ptr ; break ;
	    case 'h': pat->flags |= P_HEADER ; ++in->ptr ; break ;
	    case 'r': pat->flags |= P_RECEIVED ; ++in->ptr ; break ;
	    case 'a': pat->flags |= P_ALL ; ++in->ptr ; break ;
	    case 'i': pat->flags |= P_IGNORECASE ; ++in->ptr ; break ;
	    case 'o': pat->flags |= P_ONCE ; ++in->ptr ; break ;
	    default: done=1 ; break ;
	  }

	if( (pat->flags & P_SEARCH) == 0 )
	  pat->flags |= P_SUBJECT ;

	if( pat->flags & P_ALL )
	  pat->flags &= ~(P_HEADER|P_TO|P_FROM|P_SUBJECT|P_RECEIVED) ;
	else if( pat->flags & P_HEADER )
	  pat->flags &= ~(P_TO|P_FROM|P_SUBJECT|P_RECEIVED) ;

	if( has_var && (pat->flags & P_ONCE) ) {
	  char *converted = variable_expand(pat->pattern, True) ;
	  if( converted != NULL ) {
	    free(pat->pattern) ;
	    pat->pattern = converted ;
	  }
	}
	else if( !has_var )
	  pat->flags |= P_ONCE ;

	pat->command = get_cmds(in) ;
	advanceNext(in) ;
}



#ifndef	NOREGCOMP
	/* ip-pattern ::= [<ipaddr>] | [<ipaddr>/number] | [ipaddr - ipaddr] */


static	Expression *
ipExpr(Ictx *in)
{
	PatExpr	*pat ;

	char	buffer[2048] ;		/* TODO! should be unlimited */
register char	*optr = buffer ;
register int	done;
	int	has_var = 0 ;

	assert(*in->ptr == '[') ;

	pat = MALLOC(PatExpr, 1) ;
	if( pat == NULL ) {
	  reportMemError(in) ;
	  return NULL ;
	}

	pat->e.type = 'I' ;
	pat->e.handler = ipHandler ;
	pat->pattern = NULL ;
	pat->compiled = False ;
	pat->flags = 0 ;
	pat->command = NULL ;


	advance(in,1) ;			/* skip leading '[' */

	/* Note: since we require patterns to be contained on one line,
	 * we'll just do pointer arithmatic on in->ptr
	 */

	for(done=0;!done;)
	  switch( *in->ptr ) {
	    case '\0':
	      logFile("line %d: missing ']' in pattern\n", in->lineno) ;
	      free(pat) ;
	      return NULL ;

	    case ']':			/* ']' terminates */
	      advance(in,1) ;
	      done = 1 ;
	      break ;

	    default:
	      *optr++ = *in->ptr++ ;
	  }
	*optr++ = '\0' ;

	pat->pattern = strdup(buffer) ;
	if( pat->pattern == NULL ) {
	  free(pat) ;
	  reportMemError(in) ;
	  return NULL ;
	}

	exprModifiers(in, pat, has_var) ;
#ifdef	COMMENT
	/* parse modifiers part */

	pat->flags = 0 ;

	for(done=0;!done;)
	  switch( *in->ptr ) {
	    case 't': pat->flags |= P_TO ; ++in->ptr ; break ;
	    case 'f': pat->flags |= P_FROM ; ++in->ptr ; break ;
	    case 's': pat->flags |= P_SUBJECT ; ++in->ptr ; break ;
	    case 'h': pat->flags |= P_HEADER ; ++in->ptr ; break ;
	    case 'r': pat->flags |= P_RECEIVED ; ++in->ptr ; break ;
	    case 'a': pat->flags |= P_ALL ; ++in->ptr ; break ;
	    case 'i': pat->flags |= P_IGNORECASE ; ++in->ptr ; break ;
	    case 'o': pat->flags |= P_ONCE ; ++in->ptr ; break ;
	    default: done=1 ; break ;
	  }

	if( (pat->flags & P_SEARCH) == 0 )
	  pat->flags |= P_SUBJECT ;

	if( pat->flags & P_ALL )
	  pat->flags &= ~(P_HEADER|P_TO|P_FROM|P_SUBJECT|P_RECEIVED) ;
	else if( pat->flags & P_HEADER )
	  pat->flags &= ~(P_TO|P_FROM|P_SUBJECT|P_RECEIVED) ;

	if( has_var && (pat->flags & P_ONCE) ) {
	  char *converted = variable_expand(pat->pattern, True) ;
	  if( converted != NULL ) {
	    free(pat->pattern) ;
	    pat->pattern = converted ;
	  }
	}
	else if( !has_var )
	  pat->flags |= P_ONCE ;

	pat->command = get_cmds(in) ;
	advanceNext(in) ;
#endif	/* COMMENT */

	return (Expression *)pat ;
}
#endif



	/* paren expression ::= ( <expression> ) <commands> */

static	Expression *
parenExpr(Ictx *in)
{
	ParenExpr *paren ;

	assert(*in->ptr == '(') ;
	advance(in,1) ; advanceNext(in) ;

	paren = MALLOC(ParenExpr, 1) ;
	if( paren == NULL ) {
	  reportMemError(in) ;
	  return NULL ;
	}

	paren->e.type = '(' ;
	paren->e.handler = parenHandler ;
	paren->expr = readExpr(in) ;
	if( paren->expr == NULL ) {
	  free(paren) ;
	  return NULL ;
	}

	if( *in->ptr != ')' )
	{
	  paren->expr->handler(E_FREE, paren->expr, NULL) ;
	  free(paren) ;
	  logFile("expected ')', found %s, line %d\n", in->ptr,in->lineno);
	  if( in->lineno != in->lineno0 )
	    logFile("  expression starts at line %d\n", in->lineno0) ;
	  return NULL ;
	}

	advance(in,1) ;
	paren->command = get_cmds(in) ;
	advanceNext(in) ;

	return (Expression *)paren ;
}



	/* get a list of ':' commands.  */

static	char *
get_cmds(Ictx *in)
{
	int	len ;
	char	*ptr, *rval ;
	int	i ;
	int	has_variables = 0 ;

	/* Search input buffer for anything that can't be part of a
	 * :command.  Consider that the end of the commands.
	 *
	 * Extremely crude heuristic.  A :command starts with a ':' and
	 * one of the characters [mjkf+da|c].
	 *
	 * Some of the commands [mf+da|] may be followed by optional blanks
	 * and a filename or email address.
	 *
	 * ':' not followed by a valid command is an error.
	 */

	for(len=0, ptr=in->ptr ;;) {
	  if( ptr[0] == ':' ) {
	    if( strchr("eEmjkf+da|c$", ptr[1]) != NULL )
	    {
	      len += 2 ; ptr += 2 ;
	      if( strchr("eEmf+da|", ptr[-1]) != NULL )
	      {
		while(isspace(*ptr)) { ++ptr ; ++len ; }
		i = strcspn(ptr, ": !&|()*=") ;
		len += i ; ptr += i ;
	      }
	      else if( ptr[-1] == '$' )
	      {
		has_variables = 1 ;
		while( *ptr != '\0' &&
		      (isalnum(*ptr) || strchr("{}_", *ptr) != NULL) )
		  { ++len ; ++ptr ; }
	      }
	    }
	    else {
	      logFile("syntax error line %d, command \"%s\" unrecognized\n",
		in->lineno, ptr);
	      break;
	    }
	  }
	  else
	    break ;
	}

	if( (rval = malloc(len+1)) == NULL ) {
	  reportMemError(in) ;
	  return NULL ;
	}
	memcpy(rval, in->ptr, len) ;
	rval[len] = '\0' ;
	if( has_variables ) {
	  if( (ptr = variable_expand(rval, True)) == NULL ) {
	    reportMemError(in) ;
	    return NULL ;
	  }
	  free(rval) ;
	  rval = ptr ;
	}
	in->ptr += len ;
	return rval ;
}



static	void
reportMemError(Ictx *in)
{
	logFile("out of memory reading expression at line %d\n",in->lineno);
}








bool
ExpressionEvaluate(ExprInfo *expr, MailMessage *msg)
{
	return expr->expr->handler(E_EVAL, expr->expr, msg) ;
}


static	bool
parenHandler(ExprOp op, Expression *expr, MailMessage *msg)
{
	ParenExpr	*paren = (ParenExpr *)expr ;
	bool		rval ;

	switch(op) {
	  case E_EVAL:
	    if( (rval = paren->expr->handler(op, paren->expr, msg)) )
	      dispose(paren->command, msg) ;
	    return rval ;

	  case E_FREE:
	    paren->expr->handler(op, paren->expr, msg) ;
	    free(paren->command) ;
	    free(paren) ;
	}
	return False ;
}




	/* common binary operators */

static	bool
binHandler(ExprOp op, Expression *expr, MailMessage *m)
{
	BinExpr	*b = (BinExpr *)expr ;
	Expression *l = b->left ;
	Expression *r = b->right ;
	int	i,j ;

	switch(op) {
	  case E_EVAL:
	    switch(expr->type) {
	      case ',': return l->handler(op, l, m) , r->handler(op, r, m) ;
	      case '|': return l->handler(op, l, m) || r->handler(op, r, m) ;
	      case '&': return l->handler(op, l, m) && r->handler(op, r, m) ;
	      case OR1:
	        i = l->handler(op, l, m) ;
	        j = r->handler(op, r, m) ;
		return i || j ;
	      case AND1:
	        i = l->handler(op, l, m) ;
	        j = r->handler(op, r, m) ;
		return i && j ;
	      case '=': return l->handler(op, l, m) == r->handler(op, r, m) ;
	      case '!': return l->handler(op, l, m) != r->handler(op, r, m) ;
	      case '<': return l->handler(op, l, m) < r->handler(op, r, m) ;
	      case '>': return l->handler(op, l, m) > r->handler(op, r, m) ;
	      case LE: return l->handler(op, l, m) <= r->handler(op, r, m) ;
	      case GE: return l->handler(op, l, m) >= r->handler(op, r, m) ;
	      case '+': return l->handler(op, l, m) + r->handler(op, r, m) ;
	      case '-': return l->handler(op, l, m) - r->handler(op, r, m) ;
	      case '*': return l->handler(op, l, m) * r->handler(op, r, m) ;
	      case '/':
		i = r->handler(op,r, m) ;
		if( i == 0 ) {
		  logFile("divide by zero\n") ;
		  i = 1 ;
		}
		return l->handler(op,l, m) / i ;
	      default: assert(0) ;
	    }

	  case E_FREE:
	    l->handler(op, l, m) ;
	    r->handler(op, r, m) ;
	    free(b) ;
	    return False ;
	}
	return False ;
}



static	bool
notHandler(ExprOp op, Expression *expr, MailMessage *msg)
{
	UnaryExpr	*not = (UnaryExpr *)expr ;

	switch(op) {
	  case E_EVAL: return ! not->left->handler(op, not->left, msg) ;
	  case E_FREE:
	    not->left->handler(op, not->left, NULL) ;
	    free(not) ;
	    return False ;
	}
	return False ;
}


static	bool
constHandler(ExprOp op, Expression *expr, MailMessage *msg)
{
	ConstExpr	*con = (ConstExpr *)expr ;

	switch(op) {
	  case E_EVAL: return con->value ;
	  case E_FREE: free(con) ; return False ;
	}
	return False ;
}


static	bool
varHandler(ExprOp op, Expression *expr, MailMessage *msg)
{
	VarExpr	*var = (VarExpr *)expr ;

	switch(op) {
	  case E_EVAL: return atoi(getenv(var->var)) ;
	  case E_FREE: free(var->var) ; free(var) ; return False ;
	}
	return False ;
}


static	bool
patHandler(ExprOp op, Expression *expr, MailMessage *msg)
{
	PatExpr	*pat = (PatExpr *)expr ;
	bool	rval ;

	switch(op) {
	  case E_EVAL:
	    if( (rval = patMatch(pat,msg)) )
	      dispose(pat->command, msg) ;
	    return rval ;

	  case E_FREE:
	    if( pat->pattern != NULL ) free(pat->pattern) ;
	    if( pat->command != NULL ) free(pat->command) ;
	    free(pat) ;
	}
	return False ;
}


#ifndef	NOREGCOMP

static	bool
ipHandler(ExprOp op, Expression *expr, MailMessage *msg)
{
	PatExpr	*pat = (PatExpr *)expr ;
	bool	rval ;

	switch(op) {
	  case E_EVAL:
	    if( (rval = ipMatch(pat,msg)) )
	      dispose(pat->command, msg) ;
	    return rval ;

	  case E_FREE:
	    if( pat->pattern != NULL ) free(pat->pattern) ;
	    if( pat->command != NULL ) free(pat->command) ;
	    free(pat) ;
	}
	return False ;
}
#endif




	/* TODO: cache compiled patterns? */

static	bool
patMatch(PatExpr *pat, MailMessage *msg)
{
	char	line[MAXLINE] ;
	char	*pat2 = pat->pattern ;

	logFilev(3, "testing pattern \"%s\"\n", pat->pattern) ;

	if( !pat->compiled )
	{
	  int	err, flags ;
	  /* compile it.  If NOREGCOMP, we'll have to do this every time. */
	  /* Likewise, we have to compile every time if contains vars */
	  if( (pat->flags & P_ONCE) == 0 )
	  {
	    pat2 = variable_expand(pat->pattern, True) ;
	    if( pat2 == NULL ) pat2 = pat->pattern ;
	  }
	  flags = (pat->flags & P_IGNORECASE) ?
	  	REG_ICASE | REG_NOSUB : REG_NOSUB ;
	  if( (err = regcomp(&pat->preg, pat2, flags)) != 0 )
	  {
	    char errbuf[256] ;
	    regerror(err, &pat->preg, errbuf, sizeof(errbuf)) ;
	    logFile("Unable to process pattern '%s': %s\n",
	      pat->pattern, errbuf) ;
	    if( pat2 != pat->pattern ) free(pat2) ;
	    return False ;
	  }
#ifndef	NOREGCOMP
	  pat->compiled = (pat->flags & P_ONCE) != 0 ;
#endif
	}

	if( pat->flags & P_ALL )
	  getMessage(msg, HAVE_PARTIAL) ;
	else if( pat->flags & (P_HEADER|P_RECEIVED) )
	  getMessage(msg, HAVE_HEADER) ;

	if( pat->flags & (P_SUBJECT | P_FROM | P_TO ) )
	{
	  if( ((pat->flags & P_SUBJECT) && msg->subjectline != NULL &&
		    regexec(&pat->preg, msg->subjectline, 0,NULL, 0) == 0 ) ||
	      ((pat->flags & P_FROM) && msg->fromline != NULL &&
		    regexec(&pat->preg, msg->fromline, 0,NULL, 0) == 0) ||
	      ((pat->flags & P_TO) && msg->toline != NULL &&
		    regexec(&pat->preg, msg->toline, 0,NULL, 0) == 0) )
	  {
	    /* TODO: if only processing one message, free regex? */
	    logFilev(2, "message matched \"%s\"\n", pat2) ;
	    if( pat2 != pat->pattern ) free(pat2) ;
	    return True ;
	  }
	}

	/* need to search whole header or file? */
	if( pat->flags & (P_HEADER|P_RECEIVED|P_ALL) )
	{
	  int inReceived = 0 ;
	  /* TODO: if POP, or IMAP, may need to go back to server for whole
	   * message.
	   */
	  rewind(msg->file) ;
	  while( fgets(line, sizeof(line), msg->file) != NULL )
	  {
	    if( line[0] == '\n'  &&  !(pat->flags & P_ALL) )
	      break ;
	    if( (pat->flags & P_RECEIVED) &&  strmatch(line, "Received: ") )
	      inReceived = 1 ;
	    else if( inReceived && !isspace(line[0]) )
	      inReceived = 0 ;

	    if( ( (pat->flags & (P_ALL | P_HEADER))  ||
		  ((pat->flags & P_RECEIVED) && inReceived) )  &&
		regexec(&pat->preg, line, 0,NULL, 0) == 0  )
	    {
	      logFilev(2, "message matched \"%s\"\n", pat2) ;
	      if( pat2 != pat->pattern ) free(pat2) ;
	      return True ;
	    }
	  }
	}

	if( pat2 != pat->pattern ) free(pat2) ;
	return False ;
}



#ifndef	NOREGCOMP

static	unsigned long
parseIp(char *str)
{
	int	i1=0,i2=0,i3=0,i4=0 ;
	sscanf(str, "%d.%d.%d.%d", &i1,&i2,&i3,&i4) ;
	return i1<<24 | i2<<16 | i3<<8 | i4 ;
}

static	bool
ipcheck(PatExpr *pat, char *str, regmatch_t *match)
{
	unsigned long	testip ;	/* test IP */
	unsigned long	testip2 ;
	unsigned long	mask ;		/* test mask */
	unsigned long	ip ;		/* IP to be tested */
	char	*ptr ;

	ip = parseIp(str + match->rm_so + 1) ;
	testip = parseIp(pat->pattern) ;
	if( (ptr = strchr(pat->pattern, '/')) != NULL ) {	/* mask */
	  for(++ptr; isspace(*ptr); ++ptr) ;
	  mask = 32 - atoi(ptr) ;
	  mask = 0xffffffff << mask ;
	  return (testip & mask) == (ip & mask) ;
	}
	else if( (ptr = strchr(pat->pattern, '-')) != NULL ) {	/* range */
	  for(++ptr; isspace(*ptr); ++ptr) ;
	  testip2 = parseIp(ptr) ;
	  return ip >= testip && ip <= testip2 ;
	}
	else
	  return testip == ip ;

}

static	bool
ipMatch(PatExpr *pat, MailMessage *msg)
{
	char	line[MAXLINE] ;
	regmatch_t match ;

	logFilev(3, "testing ip pattern \"%s\"\n", pat->pattern) ;

	if( !pat->compiled )
	{
	  int	err;
	  /* compile it.  If NOREGCOMP, we'll have to do this every time. */
	  /* Likewise, we have to compile every time if contains vars */
	  if( (err = regcomp(&pat->preg,
	  	"\\[[0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*\\]", 0)) != 0 )
	  {
	    char errbuf[256] ;
	    regerror(err, &pat->preg, errbuf, sizeof(errbuf)) ;
	    logFile("Unable to process pattern '%s': %s\n",
	      pat->pattern, errbuf) ;
	    return False ;
	  }
	  pat->compiled = (pat->flags & P_ONCE) != 0 ;
	}

	if( pat->flags & P_ALL )
	  getMessage(msg, HAVE_PARTIAL) ;
	else if( pat->flags & (P_HEADER|P_RECEIVED) )
	  getMessage(msg, HAVE_HEADER) ;

	if( pat->flags & (P_SUBJECT | P_FROM | P_TO ) ) {
	  if( ((pat->flags & P_SUBJECT) && msg->subjectline != NULL &&
		regexec(&pat->preg, msg->subjectline, 1,&match, 0) == 0 &&
		ipcheck(pat, msg->subjectline, &match) ) ||
	      ((pat->flags & P_FROM) && msg->fromline != NULL &&
		regexec(&pat->preg, msg->fromline, 1,&match, 0) == 0 &&
		ipcheck(pat, msg->fromline, &match) ) ||
	      ((pat->flags & P_TO) && msg->toline != NULL &&
		regexec(&pat->preg, msg->toline, 1,&match, 0) == 0 &&
		ipcheck(pat, msg->toline, &match))  )
	  {
	    logFilev(2, "message matched ip \"%s\"\n", pat->pattern) ;
	    return True ;
	  }
	}

	/* need to search whole header or file? */
	if( pat->flags & (P_HEADER|P_RECEIVED|P_ALL) )
	{
	  int inReceived = 0 ;
	  /* TODO: if POP, or IMAP, may need to go back to server for whole
	   * message.
	   */
	  rewind(msg->file) ;
	  while( fgets(line, sizeof(line), msg->file) != NULL )
	  {
	    if( line[0] == '\n'  &&  !(pat->flags & P_ALL) )
	      break ;
	    if( (pat->flags & P_RECEIVED) &&  strmatch(line, "Received: ") )
	      inReceived = 1 ;
	    else if( inReceived && !isspace(line[0]) )
	      inReceived = 0 ;

	    if( ( (pat->flags & (P_ALL | P_HEADER))  ||
		  ((pat->flags & P_RECEIVED) && inReceived) )  &&
		regexec(&pat->preg, line, 1,&match, 0) == 0  &&
		ipcheck(pat, line, &match) )
	    {
	      logFilev(2, "message matched ip \"%s\"\n", pat->pattern) ;
	      return True ;
	    }
	  }
	}

	return False ;
}
#endif




#ifdef	NOREGCOMP

/*	If this is an older OS, it does not define the regcomp()
 *	regular expression interface.  We fake it here.  We can do this
 *	because of certain limitations in the way sortmail uses
 *	regcomp().  In particular, there is never more than one
 *	compiled expression in use at a time.
 */

#define	REG_ICASE	0

static	char	*regErr ;

int
regcomp(regex_t	*preg, char *pattern, int flags)
{
	char	*ptr ;
	int	rval ;

	*preg = flags ;

	if( flags & REG_ICASE ) {
	  pattern = strdup(pattern) ;
	  tolowerstr(pattern) ;
	}
	rval = re_comp(pattern) != NULL ;

	if( flags & REG_ICASE )
	  free(pattern) ;

	return rval ;
}


bool
regexec(regex_t	*preg,
	char	*string,
	size_t	nmatch,		/* ignored */
	regmatch_t pmatch[],	/* ignored */
	int	eflags)		/* ignored */
{
	char	*ptr ;
	bool	rval ;

	if( *preg & REG_ICASE ) {
	  string = strdup(string) ;
	  tolowerstr(string) ;
	}
	rval !re_exec(string) ;
	if( flags & REG_ICASE )
	  free(string) ;

	return rval ;
}


void
regfree(regex_t	*preg)
{
	/* do nothing */
}

size_t
regerror(int errcode, regex_t *preg, char *errbuf, int errbuf_size)
{
	strncpy(errbuf, regErr, errbuf_size) ;
	return strlen(regErr)+1 ;
}


#endif	/* NOREGCOMP */
