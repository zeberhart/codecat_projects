/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#ifndef CB2BIB_PARAMETERS_H
#define CB2BIB_PARAMETERS_H

#include <QString>

#ifdef C2B_USE_QMAKE
const QString C2B_DATA_DIR("/usr/share/cb2bib");
#else
#include "cb2bib_conf_parameters.h"
#endif

const int C2B_MESSAGE_TIME(6000);
const QString C2B_APPLICATION("cb2Bib");
const QString C2B_CITE_ID_PATTERN("<<author_first>><<year_abbreviated>><<ppages_first>>");
const QString C2B_DOCUMENT_ID_PATTERN("<<citeid>>");
const QString C2B_FIND_REPLACE_LIST(
    "Corresponding Author Contact Information||ScienceDirect alt tag^e"
    "E-mail The Corresponding Author||ScienceDirect alt tag^e"
    "small pi, Greek|$\\pi$|ScienceDirect alt tag^e"
    "{sect}||PNAS - Author String^e"
    "{dagger}||PNAS - Author String");
const QString C2B_ICON_ABOUT_B_BACK("back_cb2bib_32.png");
const QString C2B_ICON_ABOUT_B("cb2bib.png");
const QString C2B_ICON_CONNECT_B("connect_established.png");
const QString C2B_ICON_DIR(":/icons/icons/");
const QString C2B_ICON_DISCONNECT_B("connect_no.png");
const QString C2B_ICON_VIEWBIB_B("viewbib.png");
const QString C2B_ICON_VIEWC2B_B("viewcb.png");
const QString C2B_ORGANIZATION("MOLspaces");
const QString C2B_VERSION("1.9.2");

// File Manager Client
#ifdef Q_OS_UNIX
const QString C2B_FM_CLIENT_COPY_ARG("copy");
const QString C2B_FM_CLIENT_COPY_BIN("kfmclient");
const QString C2B_FM_CLIENT_MOVE_ARG("move");
const QString C2B_FM_CLIENT_MOVE_BIN("kfmclient");
#endif
#ifdef Q_OS_MACX
const QString C2B_FM_CLIENT_COPY_ARG("");
const QString C2B_FM_CLIENT_COPY_BIN("");
const QString C2B_FM_CLIENT_MOVE_ARG("");
const QString C2B_FM_CLIENT_MOVE_BIN("");
#endif
#ifdef Q_OS_WIN
const QString C2B_FM_CLIENT_COPY_ARG("");
const QString C2B_FM_CLIENT_COPY_BIN("");
const QString C2B_FM_CLIENT_MOVE_ARG("");
const QString C2B_FM_CLIENT_MOVE_BIN("");
#endif

// To text converter
#ifdef Q_OS_UNIX
const QString C2B_METADATAPARSER_EXIFTOOL_BIN("exiftool");
const QString C2B_BIBSEARCHER_PDF2TXT_BIN("pdftotext");
const QString C2B_PDFIMPORT_PDF2TXT_BIN("pdftotext");
#endif
#ifdef Q_OS_MACX
const QString C2B_METADATAPARSER_EXIFTOOL_BIN("exiftool");
const QString C2B_BIBSEARCHER_PDF2TXT_BIN("pdftotext");
const QString C2B_PDFIMPORT_PDF2TXT_BIN("pdftotext");
#endif
#ifdef Q_OS_WIN
const QString C2B_METADATAPARSER_EXIFTOOL_BIN("C:\\Windows\\exiftool.exe");
const QString C2B_PDFIMPORT_PDF2TXT_BIN("C:\\Program Files\\xpdf-3.04-win32\\pdf2cb.exe");
const QString C2B_BIBSEARCHER_PDF2TXT_BIN("C:\\Program Files\\xpdf-3.04-win32\\pdf2cb.exe");
#endif

// BibTeX postprocessing
#ifdef Q_OS_UNIX
const QString C2B_POSTPROCESS_BIBTEX_BIN("c2btools/bib2pdf");
const QString C2B_POSTPROCESS_BIBTEX_ARG("%finput %foutput");
const QString C2B_POSTPROCESS_BIBTEX_EXT("bib.pdf");
#endif
#ifdef Q_OS_MACX
const QString C2B_POSTPROCESS_BIBTEX_BIN("c2btools/bib2pdf");
const QString C2B_POSTPROCESS_BIBTEX_ARG("%finput %foutput");
const QString C2B_POSTPROCESS_BIBTEX_EXT("bib.pdf");
#endif
#ifdef Q_OS_WIN
const QString C2B_POSTPROCESS_BIBTEX_BIN("c2btools\\bib2end.bat");
const QString C2B_POSTPROCESS_BIBTEX_ARG("%finput %foutput");
const QString C2B_POSTPROCESS_BIBTEX_EXT("end");
#endif

#endif
