/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 1988-2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/
/* Conversions de maillages */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "convert2syrthes.h"

/* longueur maximale d'une ligne du maillage a lire */
#define LONGUEUR_LIGNE 1001

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Ouverture du fichier                                                |
  |======================================================================| */
int ouvrir_neu(struct typ_maillage *maillage, /* INOUT structure de maillage */
               char *nomfich) {               /* IN nom de fichier maillage a lire */
   /* ouverture du fichier de maillage .neu en lecture */
   (*maillage).fichier_ext = fopen(nomfich,"r"); /* descripteur de fichier d'entree */
   if ( (*maillage).fichier_ext == NULL ) {
     printf("ERROR : unable to open file (%s)\n",nomfich);
     return 1;
   }
   printf("Gambit Neutral data file opened : %s\n",nomfich); 
   return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Fermeture du fichier                                                |
  |======================================================================| */
int fermer_neu(struct typ_maillage *maillage) {
   /* fermeture du fichier .neu */ 
   if ( (*maillage).fichier_ext == NULL ) {
     printf("ERROR : unable to close the file .neu\n");
     return 1;
   }
   printf("Gambit Neutral file closed\n");
   fclose((*maillage).fichier_ext);
   return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | lecture de maillage Gambit neutral                                   |
  | entete                                                               |
  |======================================================================| */
int lire_entete_neu(struct typ_maillage *maillage) {
  int numnp;                   /* taille dans le maillage : nombre de sommets */
  int nelem;                   /* taille dans le maillage : nombre d'element*/
  int ngrps;                   /* taille dans le maillage : nombre de groupes*/
  int nbsets;                   /* taille dans le maillage */
  int ndfcd;                   /* taille dans le maillage : dimension 2 ou 3*/
  int ndfvl;                   /* taille dans le maillage : dimension 2 ou 3*/
  char chaine[LONGUEUR_LIGNE];  /* une ligne du fichier                */
  char* nomdom;                 /* nom de domaine                      */
  int longueur_mot;

  printf("  Reading the head of the file ...\n");
  
  /* initialisations */
  numnp = 0;
  nelem = 0; 
  ngrps = 0; 
  nbsets = 0;
  ndfcd = 0;
  ndfvl = 0;

  /* nom du domaine */
  while ((*maillage).numligne < 3) {
    fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
    (*maillage).numligne++;
  }
  longueur_mot = strlen(chaine);
  nomdom = (char *) malloc(sizeof(char) * longueur_mot);
  sscanf(chaine, "%s", nomdom);
  printf("Domain name : %s \n",nomdom);
  free(nomdom);

  /* tailles */
  while ((*maillage).numligne < 7) {
    fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
    (*maillage).numligne++;
  }
  if (sscanf(chaine, "%i %i %i %i %i %i", &numnp, &nelem, &ngrps, &nbsets, &ndfcd, &ndfvl) != 6) {
    printf("ERROR while reading line %i of the mesh file\n",(*maillage).numligne);
    return 1;
  }

  printf("NUMNP : %i\n",numnp);
  printf("NELEM : %i\n",nelem);
  printf("NGRPS : %i\n",ngrps);
  printf("NBSETS : %i\n",nbsets);
  printf("NDFCD : %i\n",ndfcd);
  printf("NDFVL : %i\n",ndfvl);
  ((*maillage).dimension) = ndfvl;
  ((*maillage).nbnoeuds)  = numnp;
  ((*maillage).nbelem)  = nelem;
  ((*maillage).nbzones)  = ngrps;
  ((*maillage).nbcl)  = nbsets;
  
  return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | lecture de maillage Gambit neutral                                   |
  | coordonnees                                                          |
  |  ATTENTION : le numero du noeud est considere comme croissant d'un a |
  |un en partant de 1                                                    |
  |======================================================================| */
int lire_coord_neu(struct typ_maillage *maillage) {
  
  char chaine[LONGUEUR_LIGNE];  /* une ligne du fichier                */
  char mot1[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot2[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot3[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  int i; /* indice de boucle */
  int entier1;
  int numnoeud_moinsun;
  
  printf("  Reading coordinates...\n");

  strcpy(mot1,"\0");
  strcpy(mot2,"\0");
  /* recherche du debut de chapitre */
  while (strcmp(mot1,"NODAL") != 0 && strcmp(mot2,"COORDINATES") != 0) {
    if (fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext) == NULL) {
      printf("ERROR : coordinates not found in the file\n");
      return 1;
    }
    (*maillage).numligne++;
    sscanf(chaine, "%s %s %s", mot1, mot2, mot3);
  }

  switch ((*maillage).dimension) {
  case 2 : 
    /* allocations et verifications */
    ((*maillage).xcoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
    if ((*maillage).xcoord == NULL) {
      printf("ERROR : allocation error, x coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
      return 1;
    }
    ((*maillage).ycoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
    if ((*maillage).ycoord == NULL) {
      printf("ERROR : allocation error, y coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
      return 1;
    }   
    numnoeud_moinsun = 0;
    for (numnoeud_moinsun = 0; numnoeud_moinsun < (*maillage).nbnoeuds ; numnoeud_moinsun++) {
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
      (*maillage).numligne++;
      sscanf(chaine, "%i %lf %lf", &entier1, &((*maillage).xcoord[numnoeud_moinsun]), &((*maillage).ycoord[numnoeud_moinsun]));
    }
    break;
  case 3 : 
    /* allocations et verifications */
    ((*maillage).xcoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
    if ((*maillage).xcoord == NULL) {
      printf("ERROR : allocation error, x coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
      return 1;
    }
    ((*maillage).ycoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
    if ((*maillage).ycoord == NULL) {
      printf("ERROR : allocation error, y coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
      return 1;
    }   
    ((*maillage).zcoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
    if ((*maillage).zcoord == NULL) {
      printf("ERROR : allocation error, z coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
      return 1;
    }   
    for (numnoeud_moinsun = 0; numnoeud_moinsun < (*maillage).nbnoeuds ; numnoeud_moinsun++) {
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
      (*maillage).numligne++;
      sscanf(chaine, "%i %lf %lf %lf", &entier1, &((*maillage).xcoord[numnoeud_moinsun]), 
                                                 &((*maillage).ycoord[numnoeud_moinsun]),
					         &((*maillage).zcoord[numnoeud_moinsun]));
    }
    break;
  default :  /* probleme sur dimension */
    printf("ERROR : invalid dimension  : %i\n",(*maillage).dimension);
    return -1;
  } 
  if (entier1 != (*maillage).nbnoeuds) {
    printf("ERROR : the number of the last node is not equal to the node number : %i != %i\n",entier1,(*maillage).nbnoeuds);
    return -1;
  }  
  return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | lecture des noeuds colores : rien a lire                             |
  | allocation et initialisation du tableau des couleurs des noeuds      |
  |======================================================================| */
int lire_noeuds_colores_neu(struct typ_maillage *maillage) {

  int i; /* indice de boucle */
  
  /* allocation */
  ((*maillage).coul_noeud) = (int *) malloc (sizeof(int) * ((*maillage).nbnoeuds));
  if ((*maillage).coul_noeud == NULL) {
    printf("ERROR : allocation error for the node color table, needed size : %zu \n", sizeof(int) * ((*maillage).nbnoeuds));
    return 1;
  }   
  /* initialisation a zero */
  for (i=0;i<(*maillage).nbnoeuds;i++) (*maillage).coul_noeud[i] = 0;
  
  return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | lecture des connectivite                                             |
  | ATTENTION : le numero de l'element est considere croissant un a un   |
  | commencant par 1                                                     |
  | Un seul type d'elements est attendu puisque seul le premier element  |
  | est verifie                                                          |
  |======================================================================| */
int lire_elem_neu(struct typ_maillage *maillage) {

  char chaine[LONGUEUR_LIGNE];  /* une ligne du fichier                */
  char mot1[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot2[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  int i; /* indice de boucle */
  int debut_cpt; 
  int numelem; /* numero de l'element */
  int typeelem; /* type de l'element : segment, triangle ou tetraedre */
  int nbnodes; /* nombre de noeuds par element */
  int entier1, entier2, entier3;
  int numelem_moinsun;

  printf("  Reading elements...\n");

  strcpy(mot1,"\0");
  /* recherche du debut de chapitre */
  while (strcmp(mot1,"ELEMENTS/CELLS") != 0) {
    if (fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext) == NULL) {
      printf("ERROR : nodes not found in the file\n");
      return 1;
    }
    (*maillage).numligne++;
    sscanf(chaine, "%s %s", mot1, mot2);
  }

  /* allocation et verification */
  (*maillage).liste_elem = (int **) malloc (sizeof(int *) * ((*maillage).nbelem));
  if ((*maillage).liste_elem == NULL) {
    printf("ERROR : allocation error for the connectivity, needed size : %zu \n", sizeof(int *) * ((*maillage).nbelem));
    return 1;
  }

  fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
  (*maillage).numligne++;
  
  /* determination du type d'elements et du nombre de noeuds par element */
  if (sscanf(chaine, "%i %i %i", &numelem, &typeelem, &nbnodes) != 3) {
    printf("ERROR : while reading line %i of the mesh file\n",(*maillage).numligne);
    return 1;
  }
  switch (typeelem) {
  case 6 : /* tetraedre => dimension des elements = 3*/
          (*maillage).dim_elem = 3;
	  (*maillage).nbnoeuds_par_elem = nbnodes;
	  switch (nbnodes) {
	  case 4 : /* tetraedre P1 */
	           /* allocation du tableau de connectivite */
		   for (i=0; i<(*maillage).nbelem; i++) (*maillage).liste_elem[i] = (int *) malloc (sizeof(int ) * nbnodes);
		   /* verification de la derniere allocation seulement */
                   if ((*maillage).liste_elem[(*maillage).nbelem-1] == NULL) {
                     printf("ERROR :  allocation error for the connectivity (verification of the last allocation)\n");
		     return 1;
                   }
		   
		   /* lecture du premier element*/
                   sscanf(chaine, "%i %i %i %i %i %i %i", &entier1, &entier2, &entier3, 
	                          &((*maillage).liste_elem[0][0]),
	                          &((*maillage).liste_elem[0][1]),
	                          &((*maillage).liste_elem[0][2]),				  
	                          &((*maillage).liste_elem[0][3]));
		   for (numelem_moinsun=1; numelem_moinsun<(*maillage).nbelem; numelem_moinsun++) {
                     fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
                     (*maillage).numligne++;
		   /* lecture des tetraedres P1*/
                     sscanf(chaine, "%i %i %i %i %i %i %i", &entier1, &entier2, &entier3, 
	                            &((*maillage).liste_elem[numelem_moinsun][0]),
	                            &((*maillage).liste_elem[numelem_moinsun][1]),
	                            &((*maillage).liste_elem[numelem_moinsun][2]),				  
	                            &((*maillage).liste_elem[numelem_moinsun][3]));
		   }	           
	           break;
	  case 10 :/* tetraedre P2 : la connectivite est sur 2 lignes pour un element */ 
	           /* renumerotation pour format Syrthes */
	           /* allocation du tableau de connectivite */
		   /* si format 4.0 demande, arret, P1 attendu */
		   if ((*maillage).version_syr==4) {
                     printf("ERROR : element P2 line %i : not allowed with SYRTHES 4.0, Stop.\n",(*maillage).numligne);
		     return 1;
                   }
		   for (i=0; i<(*maillage).nbelem; i++) (*maillage).liste_elem[i] = (int *) malloc (sizeof(int ) * nbnodes);
		   /* verification de la derniere allocation seulement */
                   if ((*maillage).liste_elem[(*maillage).nbelem-1] == NULL) {
                     printf("ERROR : allocation error for the connectivity (verification of the last allocation)\n");
		     return 1;
                   }
		   
		   /* lecture du premier element sur 2 lignes*/
                   sscanf(chaine, "%i %i %i %i %i %i %i %i %i %i", &entier1, &entier2, &entier3, 
	                          &((*maillage).liste_elem[0][0]),
	                          &((*maillage).liste_elem[0][4]),
	                          &((*maillage).liste_elem[0][1]),				  
	                          &((*maillage).liste_elem[0][6]),
	                          &((*maillage).liste_elem[0][5]),
	                          &((*maillage).liste_elem[0][2]),
	                          &((*maillage).liste_elem[0][7]));				  
                   fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
                   (*maillage).numligne++;
                   sscanf(chaine, "%i %i %i", 
	                          &((*maillage).liste_elem[0][8]),
	                          &((*maillage).liste_elem[0][9]),
	                          &((*maillage).liste_elem[0][3]));				  
		   for (numelem_moinsun=1; numelem_moinsun<(*maillage).nbelem; numelem_moinsun++) {
                     fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
                     (*maillage).numligne++;
		     /* lecture des tetraedres P2 sur 2 lignes*/
                     sscanf(chaine, "%i %i %i %i %i %i %i %i %i %i", &entier1, &entier2, &entier3, 
	                            &((*maillage).liste_elem[numelem_moinsun][0]),
	                            &((*maillage).liste_elem[numelem_moinsun][4]),
	                            &((*maillage).liste_elem[numelem_moinsun][1]),				  
	                            &((*maillage).liste_elem[numelem_moinsun][6]),
	                            &((*maillage).liste_elem[numelem_moinsun][5]),
	                            &((*maillage).liste_elem[numelem_moinsun][2]),
	                            &((*maillage).liste_elem[numelem_moinsun][7]));				  
                     fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
                     (*maillage).numligne++;
                     sscanf(chaine, "%i %i %i", 
	                            &((*maillage).liste_elem[numelem_moinsun][8]),
	                            &((*maillage).liste_elem[numelem_moinsun][9]),
	                            &((*maillage).liste_elem[numelem_moinsun][3]));				  
		   }	           
	           break;
	  default :
	    printf("ERROR : number of nodes per tetrahedron not allowed (%i)\n", nbnodes);
	    return 1;
	  }
	  break;
  case 3 : /* triangle => dimension des elements = 2*/
          (*maillage).dim_elem = 2;
	  (*maillage).nbnoeuds_par_elem = nbnodes;
	  if (nbnodes != 6 &&  nbnodes != 3) {
	    printf("ERROR : wrong element type\n");
	    return 1;
	  }
	  switch (nbnodes) {
	  case 3 : /* triangle P1 */
	           /* allocation du tableau de connectivite */
		   for (i=0; i<(*maillage).nbelem; i++) (*maillage).liste_elem[i] = (int *) malloc (sizeof(int ) * nbnodes);
		   /* verification de la derniere allocation seulement */
                   if ((*maillage).liste_elem[(*maillage).nbelem-1] == NULL) {
                     printf("ERROR : allocation error for the connectivity (verification of the last allocation)\n");
		     return 1;
                   }
		   
		   /* lecture du premier element*/
                   sscanf(chaine, "%i %i %i %i %i %i", &entier1, &entier2, &entier3, 
	                          &((*maillage).liste_elem[0][0]),
	                          &((*maillage).liste_elem[0][1]),
	                          &((*maillage).liste_elem[0][2]));				  
		   for (numelem_moinsun=1; numelem_moinsun<(*maillage).nbelem; numelem_moinsun++) {
                     fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
                     (*maillage).numligne++;
		   /* lecture des triangles P1*/
                     sscanf(chaine, "%i %i %i %i %i %i", &entier1, &entier2, &entier3, 
	                            &((*maillage).liste_elem[numelem_moinsun][0]),
	                            &((*maillage).liste_elem[numelem_moinsun][1]),
	                            &((*maillage).liste_elem[numelem_moinsun][2]));				  
		   }	           
	           break;
	  case 6 : /* triangle P2 */ 
	           /* renumerotation pour format Syrthes */
	           /* allocation du tableau de connectivite */
		   /* si format 4.0 demande, arret, P1 attendu */
		   if ((*maillage).version_syr==4) {
                     printf("ERROR : element P2 line %i : not allowed with SYRTHES 4.0, Stop.\n",(*maillage).numligne);
		     return 1;
                   }
		   for (i=0; i<(*maillage).nbelem; i++) (*maillage).liste_elem[i] = (int *) malloc (sizeof(int ) * nbnodes);
		   /* verification de la derniere allocation seulement */
                   if ((*maillage).liste_elem[(*maillage).nbelem-1] == NULL) {
                     printf("ERROR : allocation error for the connectivity (verification of the last allocation)\n");
		     return 1;
                   }
		   
		   /* lecture du premier element*/
                   sscanf(chaine, "%i %i %i %i %i %i %i %i %i", &entier1, &entier2, &entier3, 
	                          &((*maillage).liste_elem[0][0]),
	                          &((*maillage).liste_elem[0][3]),
	                          &((*maillage).liste_elem[0][1]),				  
	                          &((*maillage).liste_elem[0][4]),
	                          &((*maillage).liste_elem[0][2]),
	                          &((*maillage).liste_elem[0][5]));				  
		   for (numelem_moinsun=1; numelem_moinsun<(*maillage).nbelem; numelem_moinsun++) {
                     fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
                     (*maillage).numligne++;
		   /* lecture des triangles P2 */
                     sscanf(chaine, "%i %i %i %i %i %i %i %i %i", &entier1, &entier2, &entier3, 
	                            &((*maillage).liste_elem[numelem_moinsun][0]),
	                            &((*maillage).liste_elem[numelem_moinsun][3]),
	                            &((*maillage).liste_elem[numelem_moinsun][1]),				  
	                            &((*maillage).liste_elem[numelem_moinsun][4]),
	                            &((*maillage).liste_elem[numelem_moinsun][2]),
	                            &((*maillage).liste_elem[numelem_moinsun][5]));				  
		   }	           
                   break;
	  default :
	    printf("ERROR : number of nodes per triangle not allowed(%i)\n", nbnodes);
	    return 1;
	  }
	  break;
  case 1 : /* segment => dimension des elements = 1*/
          (*maillage).dim_elem = 1;
	  (*maillage).nbnoeuds_par_elem = nbnodes;
	  switch (nbnodes) {
	  case 2 : /* segment P1 */
	           /* allocation du tableau de connectivite */
		   for (i=0; i<(*maillage).nbelem; i++) (*maillage).liste_elem[i] = (int *) malloc (sizeof(int ) * nbnodes);
		   /* verification de la derniere allocation seulement */
                   if ((*maillage).liste_elem[(*maillage).nbelem-1] == NULL) {
                     printf("ERROR : allocation error for the connectivity (verification of the last allocation)\n");
		     return 1;
                   }
		   
		   /* lecture du premier element*/
                   sscanf(chaine, "%i %i %i %i %i", &entier1, &entier2, &entier3, 
	                          &((*maillage).liste_elem[0][0]),
	                          &((*maillage).liste_elem[0][1]));
		   for (numelem_moinsun=1; numelem_moinsun<(*maillage).nbelem; numelem_moinsun++) {
                     fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
                     (*maillage).numligne++;
		   /* lecture des triangles P1 */
                     sscanf(chaine, "%i %i %i %i %i", &entier1, &entier2, &entier3, 
	                            &((*maillage).liste_elem[numelem_moinsun][0]),
	                            &((*maillage).liste_elem[numelem_moinsun][1]));
		   }	           
	           break;
	  case 3 : /* segment P2 */ 
	           /* renumerotation pour format Syrthes */
	           /* allocation du tableau de connectivite */
		   /* si format 4.0 demande, arret, P1 attendu */
		   if ((*maillage).version_syr==4) {
                     printf("ERROR : element P2 line %i : not allowed with SYRTHES 4.0, Stop.\n",(*maillage).numligne);
		     return 1;
                   }
		   for (i=0; i<(*maillage).nbelem; i++) (*maillage).liste_elem[i] = (int *) malloc (sizeof(int ) * nbnodes);
		   /* verification de la derniere allocation seulement */
                   if ((*maillage).liste_elem[(*maillage).nbelem-1] == NULL) {
                     printf("ERROR : allocation error for the connectivity (verification of the last allocation)\n");
		     return 1;
                   }
		   
		   /* lecture du premier element*/
                   sscanf(chaine, "%i %i %i %i %i %i", &entier1, &entier2, &entier3, 
	                          &((*maillage).liste_elem[0][0]),
	                          &((*maillage).liste_elem[0][2]),
	                          &((*maillage).liste_elem[0][1]));				  
		   for (numelem_moinsun=1; numelem_moinsun<(*maillage).nbelem; numelem_moinsun++) {
                     fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
                     (*maillage).numligne++;
		   /* lecture des segments P2 */
                     sscanf(chaine, "%i %i %i %i %i %i", &entier1, &entier2, &entier3, 
	                            &((*maillage).liste_elem[numelem_moinsun][0]),
	                            &((*maillage).liste_elem[numelem_moinsun][2]),
	                            &((*maillage).liste_elem[numelem_moinsun][1]));				  
		   }	           
	           break;
	  default :
	    printf("ERROR : number of nodes per bar not allowed (%i)\n", nbnodes);
	    return 1;
	  }
	  break;
  default : 
  	  printf("ERROR : element type not allowed\n");
	  return 1;
  }

  return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | lecture des zones (groupes)                                          |
  |les numeros d'elements a lire sont disposes avec 10 elements maximum  |
  |par ligne dans le fichier Gambit Neutral                              |
  |======================================================================| */
int lire_zones_neu(struct typ_maillage *maillage) {

/* les "material" et "flags" sont ignores */
/* chaque groupe rencontre sera incremente de un a un a partir de un */
/* ce qui correspondra a la couleur des elements */

  char chaine[LONGUEUR_LIGNE];  /* une ligne du fichier                */
  char mot1[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot2[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot3[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot4[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  int i,j,k; /* indices de boucle */
  int nbelem_du_groupe; /* nombre d'element du groupe */
  int entier1, entier3, entier4;
  int elem[10] ; /* liste de 10 elements servant a la lecture des groupes d'elements */

  printf("  Reading groups...\n");
  
  /* allocation du tableau des couleurs d'elements et verification */
  (*maillage).coul_elem = (int *) malloc (sizeof(int) * ((*maillage).nbelem));
  if ((*maillage).liste_elem == NULL) {
    printf("ERROR : allocation error for the elements color table, needed size : %zu \n", sizeof(int) * ((*maillage).nbelem));
    return 1;
  }
  /* initialisation a 0 des couleurs d'elements */
  for (i=0;i<(*maillage).nbelem;i++) (*maillage).coul_elem[i] = 0;
  
  /* boucle sur le nombre de groupes */
  for (i=0;i<(*maillage).nbzones;i++) {

    strcpy(mot1,"\0");
    strcpy(mot2,"\0");
    /* recherche d'un chapitre correspondant a un groupe */
    while (strcmp(mot1,"ELEMENT") != 0 && strcmp(mot2,"GROUP") != 0) {
      if (fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext) == NULL) {
        printf("ERROR : elements not found in the file\n");
        return 1;
      }
      (*maillage).numligne++;
      sscanf(chaine, "%s %s %s", mot1, mot2, mot3);
    }

    fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
    (*maillage).numligne++;
    /* recuperation des caracteristiques du groupe */
    if (sscanf(chaine, "%s %i %s %i %s %i %s %i",
               mot1, &entier1, mot2, &nbelem_du_groupe,
	       mot3, &entier3, mot4, &entier4) != 8) {
      printf("ERROR : while reading line %i of the mesh file\n",(*maillage).numligne);
      return 1;
    }
    
    fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
    (*maillage).numligne++;
    sscanf(chaine, "%s", mot1);
    /* correspondance nom <=> couleur des elements */
    printf("Group %s : color %i\n", mot1, i+1);
    
    fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
    (*maillage).numligne++;
    /* flags ignores */

    /* lectures des numeros des elements pas groupe */
    /* 10 par ligne sauf eventuellement la derniere */
    for(j=0;j<nbelem_du_groupe / 10;j++) {
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
      (*maillage).numligne++;
      sscanf(chaine, "%i %i %i %i %i %i %i %i %i %i",
		 &elem[0], &elem[1], &elem[2], &elem[3], &elem[4],
		 &elem[5], &elem[6], &elem[7], &elem[8], &elem[9]);
      /* mise a jour du tableau des couleurs avec les elements appartenant au groupe i+1 venant d'etre lus */
      for (k=0;k<10;k++) (*maillage).coul_elem[elem[k]-1] = i+1;
    }
    /* elements sur la derniere ligne (0 a 9 elements possibles) */
    if (nbelem_du_groupe%10 > 0) {
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
      (*maillage).numligne++;
      if (sscanf(chaine, "%i %i %i %i %i %i %i %i %i %i",
		 &elem[0], &elem[1], &elem[2], &elem[3], &elem[4],
		 &elem[5], &elem[6], &elem[7], &elem[8], &elem[9]) != nbelem_du_groupe%10) {
        printf("ERROR : while reading line %i of the mesh file\n",(*maillage).numligne);
        return 1;
      }
        
      /* prise en compte des couleurs de la derniere ligne d'elements */
      for (k=0;k<nbelem_du_groupe%10;k++) (*maillage).coul_elem[elem[k]-1] = i+1;
    }
  }  
  return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | lecture des conditions limites et des noeuds colores dans le fichier |
  | Gambit Neutral                                                       |
  | les valeurs ne sont pas prises en compte                             |
  | Pour Syrthes 4.0 les noeuds non-sommets dans le cas P2 ne sont pas   |
  | indiques pour les elements de bord                                   |
  |======================================================================| */
int lire_cl_neu(struct typ_maillage *maillage) {

  char chaine[LONGUEUR_LIGNE];  /* une ligne du fichier                */
  char mot1[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot2[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot3[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  int type_cl; /* type de condition limite (1 : element de bord, 0 : noeud colore) */
  int i,j,k; /* indices de boucle */
  int *nbelem_de_la_cl; /* nombre d'elements de la condition limite */
  int entier3, entier4;
  int cpt_coul_elembord; /* compteur de la couleur pour les elements de bord */
  int cpt_coul_noeud; /* compteur de la couleur pour les noeuds */
  int numelem; /* numero de l'element lu */
  int typeelem;  /* type de l'element lu */
  int numface; /* numero de la face de l'element o� s'applique la condition limite */
  int numelembord; /* numero des elements de bord */
  int nbfaces_par_elem = 0; /* nombre de faces par element : 4 lorsque tertraedres ou 3 lorsque triangles */
  int ***elembord; /* elements de bord */
  int nbelem_alire; /* elements d'elements a lire */
  
  /* initialisation du numero des elements de bords (compteur)*/
  numelembord = 0;
  /* initialisations des compteurs de couleur */
  cpt_coul_elembord = 0;
  cpt_coul_noeud = 0;
  
  if ( ((*maillage).nbcl > 0) &&  (((*maillage).dimension == 3 && (*maillage).dim_elem == 3)
                                || ((*maillage).dimension == 2 && (*maillage).dim_elem == 2) ) ) {

    printf("  Reading boundary conditions...\n");
  
    switch ((*maillage).nbnoeuds_par_elem) {
    case 10 : 
    case 4 : /* elements tetraedres, 4 faces */
             nbfaces_par_elem = 4;
             break;
    case 6 : 
    case 3 : /* elements triangles, 3 aretes */
             nbfaces_par_elem = 3;
             break;
    default :
             printf("ERROR : number of nodes per element not equal to %i\n", (*maillage).nbnoeuds_par_elem);
	     return 1;
    }
  
    /* allocation du tableau du nombre d'elements par condition limite */
    nbelem_de_la_cl = (int *) malloc (sizeof(int) * ((*maillage).nbcl));
    if (nbelem_de_la_cl == NULL) {
      printf("ERROR : allocation error for nbelem_de_la_cl, needed size : %zu \n", sizeof(int) * ((*maillage).nbcl));
      return 1;
    }
    for (i=0;i<(*maillage).nbcl;i++) nbelem_de_la_cl[i] = 0;
    
    /* allocation de elembord*/
    /* allocation du tableau des listes des elements de bord : une partie ne sert pas si parmi les conditions il y a des conditions noeuds */
    elembord = (int ***) malloc (sizeof(int **) * ((*maillage).nbcl));
    if (elembord == NULL) {
      printf("ERROR : allocation error for the boundary elements, needed size : %zu \n", sizeof(int **) * ((*maillage).nbcl));
      return 1;
    }
    
 
  }
  else /* cas rayonnement */
    (*maillage).nbcl = 0;
  
  /* boucle sur le nombre de conditions limites a lire */
  for (i=0;i<(*maillage).nbcl;i++) {

    strcpy(mot1,"\0");
    strcpy(mot2,"\0");
    /* recherche d'un chapitre correspondant a une condition limite */
    while (strcmp(mot1,"BOUNDARY") != 0 && strcmp(mot2,"CONDITIONS") != 0) {
      if (fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext) == NULL) {
        printf("ERROR : boundary conditions not found in file\n");
        return 1;
      }
      (*maillage).numligne++;
      sscanf(chaine, "%s %s %s", mot1, mot2, mot3);
    }

    fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
    (*maillage).numligne++;
    /* recuperation des caracteristiques de la condition limite */
    if (sscanf(chaine, "%s %i %i %i %i",
               mot1, &type_cl, &nbelem_alire,
	             &entier3, &entier4) != 5) {
      printf("ERROR : while reading line %i of the mesh file\n",(*maillage).numligne);
      return 1;
    }
    
    switch (type_cl) {
    case 1 : /* condition limite sur un bord */
             nbelem_de_la_cl[cpt_coul_elembord] = nbelem_alire;
             cpt_coul_elembord++;
             /* correspondance nom <=> couleur des elements de bord */
             printf("Boundary condition %s : color %i\n", mot1, cpt_coul_elembord);
	     if (nbfaces_par_elem == 4) { /* tetraedres */
	       elembord[cpt_coul_elembord - 1]=malloc(sizeof(int *) * (nbelem_de_la_cl[cpt_coul_elembord - 1]));
	       for(j=0;j<nbelem_de_la_cl[cpt_coul_elembord - 1];j++) {
		 fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
		 (*maillage).numligne++;
		 sscanf(chaine, "%i %i %i", &numelem, &typeelem, &numface);
		 numelembord++; /* numero de l'element de bord */
		 /* allocation : les faces ont 3 sommets */
		 elembord[cpt_coul_elembord - 1][j]= malloc(sizeof(int) * (nbfaces_par_elem - 1));
		 switch (numface) { /* de Gambit Neural a Syrthes les faces 3 et 4 sont a intervertir */
		 case 1 : /* la face numero 1 est un element de bord */
		   elembord[cpt_coul_elembord - 1][j][0] = (*maillage).liste_elem[numelem-1][1];
		   elembord[cpt_coul_elembord - 1][j][1] = (*maillage).liste_elem[numelem-1][0];
		   elembord[cpt_coul_elembord - 1][j][2] = (*maillage).liste_elem[numelem-1][2];
		   break;
		 case 2 : /* la face numero 2 est un element de bord */
		   elembord[cpt_coul_elembord - 1][j][0] = (*maillage).liste_elem[numelem-1][0];
		   elembord[cpt_coul_elembord - 1][j][1] = (*maillage).liste_elem[numelem-1][1];
		   elembord[cpt_coul_elembord - 1][j][2] = (*maillage).liste_elem[numelem-1][3];
		   break;
		 case 3 : /* la face numero 3 est un element de bord intervertie avec la face numero 4*/
		   elembord[cpt_coul_elembord - 1][j][0] = (*maillage).liste_elem[numelem-1][1];
		   elembord[cpt_coul_elembord - 1][j][1] = (*maillage).liste_elem[numelem-1][2];
		   elembord[cpt_coul_elembord - 1][j][2] = (*maillage).liste_elem[numelem-1][3];
		   break;
		 case 4 : /* la face numero 4 est un element de bord intervertie avec la face numero 3*/
		   elembord[cpt_coul_elembord - 1][j][0] = (*maillage).liste_elem[numelem-1][2];
		   elembord[cpt_coul_elembord - 1][j][1] = (*maillage).liste_elem[numelem-1][0];
		   elembord[cpt_coul_elembord - 1][j][2] = (*maillage).liste_elem[numelem-1][3];
		   break;
		 default :
		   printf("ERROR : wrong face number (%i line %i of the file)\n",numface, (*maillage).numligne);
		   return 1;
		 }
	       }
	       
	       
	     }
	     else {                        /* triangles */
	       elembord[cpt_coul_elembord - 1]=malloc(sizeof(int *) * (nbelem_de_la_cl[cpt_coul_elembord - 1]));
	       for(j=0;j<nbelem_de_la_cl[cpt_coul_elembord - 1];j++) {
		 fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
		 (*maillage).numligne++;
		 sscanf(chaine, "%i %i %i", &numelem, &typeelem, &numface);
		 numelembord++; /* numero de l'element de bord */
		 /* allocation : les faces ont 2 sommets */
		 elembord[cpt_coul_elembord - 1][j]= malloc(sizeof(int) * (nbfaces_par_elem - 1));
		 switch (numface) { 
		 case 1 : /* l'arete numero 1 est un element de bord */
		   elembord[cpt_coul_elembord - 1][j][0] = (*maillage).liste_elem[numelem-1][0];
		   elembord[cpt_coul_elembord - 1][j][1] = (*maillage).liste_elem[numelem-1][1];
		   break;
		 case 2 : /* l'arete numero 2 est un element de bord */
		   elembord[cpt_coul_elembord - 1][j][0] = (*maillage).liste_elem[numelem-1][1];
		   elembord[cpt_coul_elembord - 1][j][1] = (*maillage).liste_elem[numelem-1][2];
		   break;
		 case 3 : /* l'arete numero 3 est un element de bord */
		   elembord[cpt_coul_elembord - 1][j][0] = (*maillage).liste_elem[numelem-1][2];
		   elembord[cpt_coul_elembord - 1][j][1] = (*maillage).liste_elem[numelem-1][0];
		   break;
		 default :
		   printf("ERROR : wrong face number (%i line %i of the file)\n",numface, (*maillage).numligne);
		   return 1;
		 }
	       }
	       
	     }	              
	     break;
    case 0 : /* noeud marque */
             cpt_coul_noeud++;
             /* correspondance nom <=> couleur des noeuds */
             printf("Group of marked nodes %s : color %i\n", mot1, cpt_coul_noeud);
             for(j=0;j<nbelem_alire;j++) {
               fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
               (*maillage).numligne++;
               sscanf(chaine, "%i", &numelem);
	       (*maillage).coul_noeud[numelem-1] = cpt_coul_noeud ; 
             }
	     break;
    default : 
             printf("ERROR : while reading line %i of the mesh file\n",(*maillage).numligne);
             printf("        the boundary condition type must be 1 or 0. Value=%i\n",type_cl);
	     return 1;
    } 
  } /* fin de la boucle sur nbcl */
  
  /* nouveau nombre de conditions limites */
  (*maillage).nbcl = cpt_coul_elembord;

  /* liberation de la memoire des elements dans le cas Syrthes 4.0 */
  for (i=0;i<(*maillage).nbelem;i++) free((*maillage).liste_elem[i]);
  free ((*maillage).liste_elem);

  if (numelembord > 0) {
    /* pour version Syrthes 4.0, mise en place dans la structure des elements de bord */
    (*maillage).nbelem_de_bord = numelembord;
    (*maillage).liste_elembord = (int **) malloc (sizeof(int *) * numelembord);
    if ((*maillage).liste_elembord == NULL) {
      printf("ERROR : allocation error for the boundary elements list, needed size : %zu \n", sizeof(int *) * numelembord);
      return 1;
    }
    (*maillage).coul_elembord = (int *) malloc (sizeof(int) * numelembord);
    if ((*maillage).coul_elembord == NULL) {
      printf("ERROR : allocation error for the boundary elements color, needed size : %zu \n", sizeof(int) * numelembord);
      return 1;
    }
    
    numelembord = 0;  
    for  (i=0;i<cpt_coul_elembord;i++) {
      for (j=0;j<nbelem_de_la_cl[i];j++) {
        /* listes des noeuds des elements de bord */
        (*maillage).liste_elembord[numelembord] = (int *) malloc (sizeof(int) * (nbfaces_par_elem - 1));
	for(k=0;k<nbfaces_par_elem-1;k++) (*maillage).liste_elembord[numelembord][k] = elembord[i][j][k];
	/* couleurs des elements de bord */
	(*maillage).coul_elembord[numelembord] = i+1 ;
        numelembord++;
        /* desallocation de elembord */
	free(elembord[i][j]);
      }
      free(elembord[i]);
    }
    free(elembord);
  }
  else
    (*maillage).nbelem_de_bord = 0;
  
  return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |modification de la couleur des noeuds qui ont une couleur             |
  |differente de 0                                                       |
  |valable uniquement pour Gambit Neutral                                |
  |pourra etre ameliore en ne reparcourant que le fichier Syrthes mais   |
  |probleme de saut de ligne pour l'instant                              |
  |======================================================================| */
int modif_noeuds_neusyr(struct typ_maillage *maillage) {

  char chaine[LONGUEUR_LIGNE];  /* une ligne du fichier                */
  char mot1[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot2[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot3[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot4[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  int i; /* indice de boucle */
  int j; /* indice de boucle */
  int numnoeud; /* numero du noeud lu */
  int entier2; /* couleur lue ignoree */
  double xcoord, ycoord, zcoord; /* coordonnees */
  
  /* initialisations */
  strcpy(mot1,"\0");
  strcpy(mot4,"\0");
  /* recherche du debut du chapitre des coordonnees dans le fichier Syrthes */
  while (strcmp(mot1,"C$") != 0 && strcmp(mot4,"NOEUDS") != 0) {
    if (fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_syr) == NULL) {
      printf("ERROR : coordinates not found\n");
      return 1;
    }
    sscanf(chaine, "%s %s %s %s", mot1, mot2, mot3, mot4);
  }
  fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_syr);
  
  /* initialisations */
  strcpy(mot1,"\0");
  strcpy(mot2,"\0");
  /* recherche du debut de chapitre des coordonnes pour le fichier Gambit Neutral */
  while (strcmp(mot1,"NODAL") != 0 && strcmp(mot2,"COORDINATES") != 0) {
    if (fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext) == NULL) {
      printf("ERROR :  coordinates not found\n");
      return 1;
    }
    sscanf(chaine, "%s %s %s", mot1, mot2, mot3);
  }
  /* initialisation de la 3e coordonnee */
  zcoord = 0.0;
    
  
  /* recherche des noeuds qui ont change de couleurs */
  for (i=0;i<(*maillage).nbnoeuds;i++) {
    /* lecture des coordonnes dans le fichier Gambit Neutral */    
    if ((*maillage).coul_noeud[i] == 0) {
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_syr);
    }
    else {
      /* si le noeud a une couleur */
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
      /* lecture des coordonnes dans le fichier Gambit Neutral selon la dimension 2 ou 3 */    
      if ((*maillage).dimension == 2) sscanf(chaine, "%i %lf %lf", &numnoeud, &xcoord, &ycoord);
      else sscanf(chaine, "%i %lf %lf %lf", &numnoeud, &xcoord, &ycoord, &zcoord);
      /* ecriture des coordonnees dans le fichier Syrthes */    
      fprintf((*maillage).fichier_syr,"%10i%4i %14.7E %14.7E %14.7E \n",
	      numnoeud, (*maillage).coul_noeud[i], 
	      xcoord, ycoord, zcoord);
    }
  }       
  

  return 0;
}

