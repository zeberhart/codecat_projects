/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_parall.h"
#include "syr_tree.h"
#include "syr_proto.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
MPI_Status status;
#endif

extern struct Performances perfo;
extern struct Affichages affich;

extern char nomgeomr[CHLONG];
extern char nomfdf[CHLONG];


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_syrthes_ray(struct Maillage *maillnodray)
{
  FILE  *fmaill;
  char ch[CHLONG];
  int ndmats_lu,npoin_lu,nelem_lu,nelemb_lu,ndim_lu;
  int i,j,n,nr;
  int *ne0,*ne1,*ne2;
  double z;

  if ((fmaill=fopen(nomgeomr,"r")) == NULL)
    {
      if (SYRTHES_LANG == FR)
	printf("Impossible d'ouvrir le fichier %s\n",nomgeomr);
      else if (SYRTHES_LANG == EN)
	printf("Impossible to open the file %s\n",nomgeomr);
      syrthes_exit(1) ;
    }

  fgets(ch,90,fmaill);
  fgets(ch,90,fmaill);
  fgets(ch,90,fmaill);
  fscanf(fmaill,"%s%s%s%d\n",ch,ch,ch,&ndim_lu);
  fgets(ch,90,fmaill);
  fscanf(fmaill,"%s%s%s%s%s%d",ch,ch,ch,ch,ch,&npoin_lu);
  fscanf(fmaill,"%s%s%s%s%d",ch,ch,ch,ch,&nelem_lu);
  fscanf(fmaill,"%s%s%s%s%s%s%d",ch,ch,ch,ch,ch,ch,&nelemb_lu);
  fscanf(fmaill,"%s%s%s%s%s%s%s%d",ch,ch,ch,ch,ch,ch,ch,&ndmats_lu);
  
  

  if (ndim_lu==3)
    {
      maillnodray->ndim=ndim_lu;    maillnodray->ndmat=3;         maillnodray->ndiele=2;  
      maillnodray->npoin=npoin_lu;  maillnodray->nelem=nelem_lu;
    }
  else 
    {
      maillnodray->ndim=ndim_lu;    maillnodray->ndmat=2;      maillnodray->ndiele=1;
      maillnodray->npoin=npoin_lu;  maillnodray->nelem=nelem_lu;
    }

  maillnodray->nref=NULL;

  maillnodray->coord=(double**)malloc(maillnodray->ndim*sizeof(double*));
  for (i=0;i<maillnodray->ndim;i++)  maillnodray->coord[i]=(double*)malloc(maillnodray->npoin*sizeof(double)); 
  verif_alloue_double2d(maillnodray->ndim,"lire_syrthes",maillnodray->coord);

  maillnodray->node=(int**)malloc(maillnodray->ndmat*sizeof(int*));
  for (i=0;i<maillnodray->ndmat;i++) maillnodray->node[i]=(int*)malloc(maillnodray->nelem*sizeof(int)); 
  verif_alloue_int2d(maillnodray->ndmat,"lire_syrthes",maillnodray->node);

  maillnodray->nrefe=(int*)malloc(maillnodray->nelem*sizeof(int));
  verif_alloue_int1d("lire_syrthes",maillnodray->nrefe);


  perfo.mem_ray+=maillnodray->npoin*maillnodray->ndim*sizeof(double);
  perfo.mem_ray+=(maillnodray->ndmat+1)*maillnodray->nelem*sizeof(int);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;

  fgets(ch,90,fmaill);
  fgets(ch,90,fmaill);
  fgets(ch,90,fmaill);
  fgets(ch,90,fmaill);
  fgets(ch,90,fmaill);


  /* coordonnees noeuds maillage SYRTHES */
  if (maillnodray->ndim==2) 
    {
      for (i=0;i<maillnodray->npoin;i++)
	fscanf(fmaill,"%d%d%14lf%14lf%14lf",&n,&nr,(maillnodray->coord[0]+i),(maillnodray->coord[1]+i),&z);
    }
  else
    {
      for (i=0;i<maillnodray->npoin;i++)
	fscanf(fmaill,"%d%d%14lf%14lf%14lf",&n,&nr,(maillnodray->coord[0]+i),(maillnodray->coord[1]+i),(maillnodray->coord[2]+i));
    }
  
  
  /* connectivite maillage volumique SYRTHES */
  fgets(ch,90,fmaill);
  fgets(ch,90,fmaill);
  fgets(ch,90,fmaill);
  fgets(ch,90,fmaill);
  
  if (maillnodray->ndmat==3)
    for (i=0,ne0=maillnodray->node[0],ne1=maillnodray->node[1],ne2=maillnodray->node[2];
	 i<maillnodray->nelem;
	 i++,ne0++,ne1++,ne2++)
      fscanf(fmaill,"%d%d%d%d%d",&n,maillnodray->nrefe+i,ne0,ne1,ne2);

  else
    for (i=0,ne0=maillnodray->node[0],ne1=maillnodray->node[1];
	 i<maillnodray->nelem;
	 i++,ne0++,ne1++)
      fscanf(fmaill,"%d%d%d%d",&n,maillnodray->nrefe+i,ne0,ne1);


  /*    imprime_maillage_ref(*maillnodray); */

  
  /* numerotation des noeuds a partir de 0 */
  for (i=0;i<maillnodray->nelem;i++)
    for (j=0;j<maillnodray->ndmat;j++)
      maillnodray->node[j][i]--;


  if (syrglob_nparts==1 || syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      {
	printf("\n\n *** MAILLAGE RAYONNEMENT SYRTHES :\n");
	printf("                           |--------------------|\n");
	printf("                           | Maillage volumique |\n");
	printf("      ---------------------|--------------------|\n");
	printf("      | Dimension          |    %8d        |\n",maillnodray->ndim);
	printf("      | Nombre de noeuds   |    %8d        |\n",maillnodray->npoin);
	printf("      | Nombre d'elements  |    %8d        |\n",maillnodray->nelem);
	printf("      | Nb noeuds par elt  |    %8d        |\n",maillnodray->ndmat);
	printf("      ---------------------|--------------------|\n");
      }
    else if (SYRTHES_LANG == EN)
      {
	printf("\n\n *** SYRTHES RADIATION MESH :\n");
	printf("                           |--------------------|\n");
	printf("                           |   Volumic mesh     |\n");
	printf("      ---------------------|--------------------|\n");
	printf("      | Dimension          |    %8d        |\n",maillnodray->ndim);
	printf("      | Number of nodes    |    %8d        |\n",maillnodray->npoin);
	printf("      | Number of elements |    %8d        |\n",maillnodray->nelem);
	printf("      | Nb nodes per elt   |    %8d        |\n",maillnodray->ndmat);
	printf("      ---------------------|--------------------|\n");
      }
  
  fclose(fmaill);
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_fdf(struct FacForme ***listefdf,double **diagfdf,
	      struct PropInfini *propinf,struct Horizon *horiz,
	      struct SDparall_ray sdparall_ray)
{
  FILE *ffdf;

  int i,ii,j,n,nn,nb,id,nbeleloc;
  struct FacForme *pff,*qff,*ppff;
  int *itrav,*itravf,*itaill1,*nbfdfproc,itaill,maxtaill;
  double *trav,*diagtrav;

  /* en entrant, listefdf est deja alloue au nbre de proc : listefdf[nbproc] */


  if (syrglob_nparts==1 || syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf("\n\n *** FACTEURS DE FORME : lecture sur fichier \n");
    else if (SYRTHES_LANG == EN)
      printf("\n\n *** VIEW FACTORS : Reading on a file \n");


  if (sdparall_ray.npartsray>1)
    {
#ifdef _SYRTHES_MPI_

#ifdef _SYRTHES_MPI_IO_

      MPI_File  ffdfp;
      MPI_File_open(sdparall_ray.syrthes_comm_ray,nomfdf,MPI_MODE_RDONLY,MPI_INFO_NULL,&ffdfp);  

      /* lecture du nombre de facettes locales */
      /* ------------------------------------- */
      MPI_File_read_ordered(ffdfp,&nb,1,MPI_INT,&status);
      
      /* lecture de la diagonale des fdf */
      /* ------------------------------- */
      *diagfdf=(double*)malloc(nb*sizeof(double));
      MPI_File_read_ordered(ffdfp,*diagfdf,nb,MPI_DOUBLE,&status);
      

      /* lecture du nombre total de fdf non nuls sur le proc courant  */
      /* -----------------------------------------------------------  */
      MPI_File_read_ordered(ffdfp,&itaill,1,MPI_INT,&status);
      
      /* lecture des fdf */
      /* --------------- */
      

      /* nombre de fdf nuls nuls par proc */
      nbfdfproc=(int*)malloc(nb*sdparall_ray.npartsray*sizeof(int));

      itravf=(int*)malloc(itaill*sizeof(int));      /* numero de la facette en face */
      trav=(double*)malloc(itaill*sizeof(double));  /* valeur du fdf entre les 2 facettes */


      MPI_File_read_ordered(ffdfp,nbfdfproc,nb*sdparall_ray.npartsray,MPI_INT,&status);   
      MPI_File_read_ordered(ffdfp,itravf,itaill,MPI_INT,&status);
      MPI_File_read_ordered(ffdfp,trav,itaill,MPI_DOUBLE,&status);

      /* rangement des infos dans les listes */
      /* ----------------------------------- */
      for (n=0;n<sdparall_ray.npartsray;n++)
	{
	  listefdf[n]=(struct FacForme**)malloc(nb*sizeof(struct FacForme*));
	  for (i=0;i<nb;i++) listefdf[n][i]=NULL;
	}

      for (ii=0,i=0;i<nb;i++)
	{
	  for (n=0;n<sdparall_ray.npartsray;n++)
	    {
	      /* boucle sur le nbre de fdf non nuls pour la facette i sur le proc n */

	      for (j=0; j<nbfdfproc[i*sdparall_ray.npartsray+n]; j++)
		  {
		    qff=(struct FacForme*)malloc(sizeof(struct FacForme));
		    qff->numenface = itravf[ii]; 
		    qff->fdf       = trav[ii];
		    qff->suivant   = NULL;

		    if (!listefdf[n][i])
		      listefdf[n][i]=qff;
		    else
		      {
			pff=listefdf[n][i];
			while(pff) {ppff=pff; pff=pff->suivant;}
			ppff->suivant=qff;	      
		      }
		    ii++;
		  }
	    }
	}

      free(itravf); free(trav); free(nbfdfproc);
      

      MPI_File_close(&ffdfp); 
#else
/* sans MPI-IO */

      if (syrglob_rang==0)
	{
	  if ((ffdf=fopen(nomfdf,"rb")) == NULL)
	    {
	      if (SYRTHES_LANG == FR)
		printf("Impossible d'ouvrir le fichier des facteur de forme :%s\n",nomfdf);
	      else if (SYRTHES_LANG == EN)
		printf("Impossible to open the view factors file :%s\n",nomfdf);
	      syrthes_exit(1) ;
	    }
	
	  /* lire les donnees pour le proc 0 */
	  /* =============================== */

	  /* nbre de fdf */
	  fread(&nb,sizeof(int),1,ffdf);

	  /* lecture de la diagonale des fdf */
	  *diagfdf=(double*)malloc(nb*sizeof(double));
	  fread((*diagfdf),sizeof(double),nb,ffdf);	  

	  /* lecture de la taille des listes de fdf a lire */
	  fread(&itaill,sizeof(int),1,ffdf);
	  
	  /* lecture des fdf */

	  /* nombre de fdf nuls nuls par proc */
	  nbfdfproc=(int*)malloc(nb*sdparall_ray.npartsray*sizeof(int));

	  itravf=(int*)malloc(itaill*sizeof(int));
	  trav=(double*)malloc(itaill*sizeof(double));
	  
	  fread(nbfdfproc,sizeof(int),nb*sdparall_ray.npartsray,ffdf);
	  fread(itravf,sizeof(int),itaill,ffdf);
	  fread(trav,sizeof(double),itaill,ffdf);

	  /* rangement des infos dans les listes */
	  /* ----------------------------------- */
	  for (n=0;n<sdparall_ray.npartsray;n++)
	    {
	      listefdf[n]=(struct FacForme**)malloc(nb*sizeof(struct FacForme*));
	      for (i=0;i<nb;i++) listefdf[n][i]=NULL;
	    }

	  for (ii=0,i=0;i<nb;i++)
	    {
	      for (n=0;n<sdparall_ray.npartsray;n++)
		{
		  /* boucle sur le nbre de fdf non nuls pour la facette i sur le proc n */
		  
		  for (j=0; j<nbfdfproc[i*sdparall_ray.npartsray+n]; j++)
		    {
		      qff=(struct FacForme*)malloc(sizeof(struct FacForme));
		      qff->numenface = itravf[ii]; 
		      qff->fdf       = trav[ii];
		      qff->suivant   = NULL;
		      
		      if (!listefdf[n][i])
			listefdf[n][i]=qff;
		      else
			{
			  pff=listefdf[n][i];
			  while(pff) {ppff=pff; pff=pff->suivant;}
			  ppff->suivant=qff;	      
			}
		      ii++;
		    }
		}
	    }

	  free(itravf); free(trav); free(nbfdfproc);

	  /* boucler sur les autres proc pour lire et envoyer les donnees */
	  /* ============================================================ */
	  for (n=1;n<sdparall_ray.npartsray;n++)
	    {
	      /* nbre de fdf */
	      fread(&nb,sizeof(int),1,ffdf);
	      MPI_Send(&nb,1,MPI_INT,n,0,sdparall_ray.syrthes_comm_ray);

	      /* lecture de la diagonale des fdf */
	      diagtrav=(double*)malloc(nb*sizeof(double));
	      fread(diagtrav,sizeof(double),nb,ffdf);	  
	      MPI_Send(diagtrav,nb,MPI_DOUBLE,n,0,sdparall_ray.syrthes_comm_ray);

	      /* lecture de la taille des listes de fdf a lire */
	      fread(&itaill,sizeof(int),1,ffdf);
	      MPI_Send(&itaill,1,MPI_INT,n,0,sdparall_ray.syrthes_comm_ray);
	  
	  
	      /* lecture des fdf */

	      /* nombre de fdf nuls nuls par proc */
	      nbfdfproc=(int*)malloc(nb*sdparall_ray.npartsray*sizeof(int));
	      itravf=(int*)malloc(itaill*sizeof(int));
	      trav=(double*)malloc(itaill*sizeof(double));
	  
	      fread(nbfdfproc,sizeof(int),nb*sdparall_ray.npartsray,ffdf);
	      MPI_Send(nbfdfproc,nb*sdparall_ray.npartsray,MPI_INT,n,0,sdparall_ray.syrthes_comm_ray);

	      fread(itravf,sizeof(int),itaill,ffdf);
	      MPI_Send(itravf,itaill,MPI_INT,n,0,sdparall_ray.syrthes_comm_ray);

	      fread(trav,sizeof(double),itaill,ffdf);
	      MPI_Send(trav,itaill,MPI_DOUBLE,n,0,sdparall_ray.syrthes_comm_ray);

	      free(itravf); free(trav); free(nbfdfproc); free(diagtrav);
	      
	    } /* fin boucle envoi vers les autres proc */	      
	  
	  fclose(ffdf);
	}
      else /* sur les autres proc que 0, on recoit les donnees */
	{
	  /* nbre de fdf */
	  MPI_Recv(&nb,1,MPI_INT,0,0,sdparall_ray.syrthes_comm_ray,&status);

	  /* lecture de la diagonale des fdf */
	  *diagfdf=(double*)malloc(nb*sizeof(double));
	  MPI_Recv(*diagfdf,nb,MPI_DOUBLE,0,0,sdparall_ray.syrthes_comm_ray,&status);

	  /* lecture de la taille des listes de fdf a lire */
	  MPI_Recv(&itaill,1,MPI_INT,0,0,sdparall_ray.syrthes_comm_ray,&status);
	  
	  /* lecture des fdf */

	  /* nombre de fdf nuls nuls par proc */
	  nbfdfproc=(int*)malloc(nb*sdparall_ray.npartsray*sizeof(int));

	  itravf=(int*)malloc(itaill*sizeof(int));
	  trav=(double*)malloc(itaill*sizeof(double));
	  
	  MPI_Recv(nbfdfproc,nb*sdparall_ray.npartsray,MPI_INT,0,0,sdparall_ray.syrthes_comm_ray,&status);
	  MPI_Recv(itravf,itaill,MPI_INT,0,0,sdparall_ray.syrthes_comm_ray,&status);
	  MPI_Recv(trav,itaill,MPI_DOUBLE,0,0,sdparall_ray.syrthes_comm_ray,&status);

	  /* rangement des infos dans les listes */
	  /* ----------------------------------- */
	  for (n=0;n<sdparall_ray.npartsray;n++)
	    {
	      listefdf[n]=(struct FacForme**)malloc(nb*sizeof(struct FacForme*));
	      for (i=0;i<nb;i++) listefdf[n][i]=NULL;
	    }
	  
	  for (ii=0,i=0;i<nb;i++)
	    {
	      for (n=0;n<sdparall_ray.npartsray;n++)
		{
		  /* boucle sur le nbre de fdf non nuls pour la facette i sur le proc n */
		  
		  for (j=0; j<nbfdfproc[i*sdparall_ray.npartsray+n]; j++)
		    {
		      qff=(struct FacForme*)malloc(sizeof(struct FacForme));
		      qff->numenface = itravf[ii]; 
		      qff->fdf       = trav[ii];
		      qff->suivant   = NULL;
		      
		      if (!listefdf[n][i])
			listefdf[n][i]=qff;
		      else
			{
			  pff=listefdf[n][i];
			  while(pff) {ppff=pff; pff=pff->suivant;}
			  ppff->suivant=qff;	      
			}
		      ii++;
		    }
		}
	    }
   
	  free(itravf); free(trav); free(nbfdfproc);

	} /* fin de la boucle sur les autres proc */

/* fin du ifdef _SYRTHES_MPI_IO_ */
#endif  
    
/* fin du ifdef _SYRTHES_MPI_ */
#endif      
    }
  else
    {
      if ((ffdf=fopen(nomfdf,"rb")) == NULL)
	{
	  if (SYRTHES_LANG == FR)
	    printf("Impossible d'ouvrir le fichier des facteur de forme :%s\n",nomfdf);
	  else if (SYRTHES_LANG == EN)
	    printf("Impossible to open the view factors file :%s\n",nomfdf);
	  syrthes_exit(1) ;
	}
      /* nbre de fdf */
      fread(&nb,sizeof(int),1,ffdf);
      
      /* lecture de la diagonale des fdf */
      /* ------------------------------- */
      *diagfdf=(double*)malloc(nb*sizeof(double));
      fread((*diagfdf),sizeof(double),nb,ffdf);

      
      /* lecture de la taille des listes de fdf a lire */
      /* --------------------------------------------- */
      itaill1=(int*)malloc(nb*sizeof(int));
      fread(itaill1,sizeof(int),nb,ffdf);

      
      /* lecture des fdf */
      /* --------------- */
      listefdf[0]=(struct FacForme**)malloc(nb*sizeof(struct FacForme*));
      for (i=0;i<nb;i++) listefdf[0][i]=NULL;

      for (maxtaill=0,i=0;i<nb;i++)
	if (itaill1[i]>maxtaill) maxtaill=itaill1[i];

      
      /* on lit dans itrav la liste des facettes en face */
      /* et dans trav les fdf correspondants             */
      itrav=(int*)malloc(maxtaill*sizeof(int));
      trav=(double*)malloc(maxtaill*sizeof(double));
      
      
      for (i=0;i<nb;i++)
	{
	  fread(itrav,sizeof(int),itaill1[i],ffdf);
	  fread(trav,sizeof(double),itaill1[i],ffdf);

	  /* on les range dans listefdf */
	  for (nn=0, j=0;j<itaill1[i];j++)
	    {
	      qff=(struct FacForme*)malloc(sizeof(struct FacForme));
	      qff->numenface=itrav[nn]; 
	      qff->fdf=trav[nn];
	      qff->suivant=NULL;
	      
	      if (!listefdf[0][i])
		listefdf[0][i]=qff;
	      else
		{
		  pff=listefdf[0][i];
		  while(pff) {ppff=pff; pff=pff->suivant;}
		  ppff->suivant=qff;	      
		}
	      nn++;
	    }
	}

      free(itrav); free(trav);
            
      fclose(ffdf);

  }

}

