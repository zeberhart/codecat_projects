# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui  import *
import sys

class OpenFile(object): # Classe de décodage de fichier de donnée
       
    def OpenFile(self, parent=None): 
        # La principe de cette classe consiste à lire une à une les lignes du fichiers de donnée, et de traiter celle ci.
        # L'entête de chaque ligne est ensuite passé comme clé dans un dictionnaire répertoriant les mots clé utilisables par l'interface.
        # Il est à noter que certain mots clé se trouve dans Syrthes mais ne sont pas présent ici car relevant d'une utilisation très expérimentée
        # Ensuite la fonction correspondant à ce mot clé est appelé pour modifier l'interface afin de montré les informations contenus dans celui ci
        # Ces modifications sont toujours les mêmes pour les tableau. On crée un QTableWidgetItem correspondant à la cellule de tableau à renseigner,
        # dans le quel on met la valeur du mot clé correspondant à cette cellule. Ensuite on met ce QTableWidgetItem à là cellule du tableau concerné.
        # Pour ce qui des autres composant il suffit juste de mettre la valeur du mot clé au composant graphique concerné.
        self.KeyWords={'TITRE ETUDE' : self.tke_TE,
                  'DIMENSION DU PROBLEME' : self.tke_DP,
                  '//' : self.tke_DC,
                  'PRISE EN COMPTE DU RAYONNEMENT CONFINE' : self.tke_PR,
                  'MODELISATION DES TRANSFERTS D HUMIDITE' : self.tke_PH,
                  'PRISE EN COMPTE COUPLAGE CFD' : self.tke_PC,
                  'MODELE 1D FLUIDE' : self.tke_FL1D,
                  'MODELE 0D FLUIDE' : self.tke_FL0D,
                  'SUITE DE CALCUL' : self.tke_SC,
                  'SUITE : NOUVEAU TEMPS INITIAL' : self.tke_ST,
                  'PAS DE TEMPS SOLIDE' : self.tke_PTS,
                  'NOMBRE DE PAS DE TEMPS SOLIDES' : self.tke_NPS,
                  'PAS DE TEMPS AUTOMATIQUE' : self.tke_PTA,
                  'PAS DE TEMPS MULTIPLES' : self.tke_PTM,
                  'NOMBRE ITERATIONS SOLVEUR TEMPERATURE' : self.tke_IST,
                  'PRECISION POUR LE SOLVEUR TEMPERATURE' : self.tke_PST,
                  'NOMBRE ITERATIONS SOLVEUR PRESSION VAPEUR' : self.tke_IPV,
                  'PRECISION POUR LE SOLVEUR PRESSION VAPEUR' : self.tke_PPV,
                  'NOMBRE ITERATIONS SOLVEUR PRESSION TOTALE' : self.tke_IPT,
                  'PRECISION POUR LE SOLVEUR PRESSION TOTALE' : self.tke_PPT,
                  'CLIM_T' : self.tke_CLM,
                  'CLIM_T_FCT' : self.tke_CLM,
                  'CLIM_T_PROG' : self.tke_CLM,
                  'CPHY_MAT_ISO' : self.tke_CPYI,
                  'CPHY_MAT_ISO_FCT' : self.tke_CPYI,
                  'CPHY_MAT_ISO_PROG' : self.tke_CPYI,
                  'CPHY_MAT_ORTHO_2D' : self.tke_CPYO,
                  'CPHY_MAT_ORTHO_3D' : self.tke_CPYO,
                  'CPHY_MAT_ORTHO_2D_FCT' : self.tke_CPYO,
                  'CPHY_MAT_ORTHO_3D_FCT' : self.tke_CPYO,
                  'CPHY_MAT_ORTHO_2D_PROG' : self.tke_CPYO,
                  'CPHY_MAT_ORTHO_3D_PROG' : self.tke_CPYO,
                  'CPHY_MAT_ANISO_2D' : self.tke_CPYA,
                  'CPHY_MAT_ANISO_3D' :self.tke_CPYA,
                  'CPHY_MAT_ANISO_2D_FCT' : self.tke_CPYA,
                  'CPHY_MAT_ANISO_3D_FCT' : self.tke_CPYA,
                  'CPHY_MAT_ANISO_2D_PROG' : self.tke_CPYA,
                  'CPHY_MAT_ANISO_3D_PROG' : self.tke_CPYA,
                  'CINI_T' : self.tke_CIN,
                  'CINI_T_FCT' : self.tke_CIN,
                  'CINI_T_PROG' : self.tke_CIN,
                  'CVOL_T' : self.tke_CVO,
                  'CVOL_T_FCT' : self.tke_CVO,
                  'CVOL_T_PROG' : self.tke_CVO,
                  'CLIM' : self.tke_PERCHT,
                  'CLIM_PROG' : self.tke_PERCHT,
                  'PAS DES SORTIES CHRONO SOLIDE SECONDES' : self.tke_OUT,
                  'PAS DES SORTIES CHRONO SOLIDE ITERATIONS' : self.tke_OUT,
                  'INSTANTS SORTIES CHRONO SOLIDE SECONDES' : self.tke_OUT,
                  'CHAMP DE FLUX THERMIQUE' : self.tke_OUT,
                  'CHAMP DE TEMPERATURES MAXIMALES' : self.tke_OUT,
                  'SUPPRESSION DU CHAMP RESULTAT FINAL' : self.tke_OUT,
                  'HIST' : self.tke_HST,
                  'BILAN FLUX SURFACIQUES' : self.tke_BF,
                  'BILAN FLUX VOLUMIQUES' : self.tke_BF,
                  'MAILLAGE CONDUCTION' : self.tke_MC,
                  'MAILLAGE RAYONNEMENT' : self.tke_MR,
                  'MAILLAGE FL1D' : self.tke_MA1D,
# isa                  'PREFIXE DU RESULTAT PRECEDENT POUR SUITE DE CALCUL' : self.tke_RSC,
                  'RESULTAT PRECEDENT POUR SUITE DE CALCUL' : self.tke_RSC,
                  'FICHIER METEO' : self.tke_FM,
                  'PREFIXE DES FICHIERS RESULTATS' : self.tke_PRF,
                  'RAYT': self.tke_RBS,
                  'LECTURE DES FACTEURS DE FORME SUR FICHIER' : self.tke_VFM,
                  '/CC': self.tke_CCC,
                  '/CR': self.tke_CCR,
                  'CLIM_RAYT' : self.tke_RBC,
                  'DOMAINE DE RAYONNEMENT CONFINE OUVERT SUR L EXTERIEUR' : self.tke_RPA,
                  'SOLAIRE' : self.tke_SM,
                  'PRISE EN COMPTE DU RAYONNEMENT SOLAIRE' : self.tke_PRS,
                  'CINI_PV' : self.tke_HIV,
                  'CINI_PV_FCT' : self.tke_HIV,
                  'CINI_PV_PROG' : self.tke_HIV,
                  'CINI_PT' : self.tke_HIV,
                  'CINI_PT_FCT' : self.tke_HIV,
                  'CINI_PT_PROG' : self.tke_HIV,
                  'CVOL_PV' : self.tke_HSV,
                  'CVOL_PV_FCT' : self.tke_HSV,
                  'CVOL_PV_PROG' : self.tke_HSV,
                  'CVOL_PT' : self.tke_HSV,
                  'CVOL_PT_FCT' : self.tke_HSV,
                  'CVOL_PT_PROG' : self.tke_HSV,
                  'HMT_MAT' : self.tke_MATISO,
                  'HMT_MAT_ISO' : self.tke_MATISO,
                  'HMT_MAT_ANISO' : self.tke_MATANISO,
                  'CLIM_HMT' : self.tke_HBC,
                  'CLIM_HMT_FCT' : self.tke_HBC,
                  'CLIM_HMT_PROG' : self.tke_HBC,
                  # fluid1d
                  'PAS DE TEMPS FLUIDE 1D' : self.tke_PTF1D,
                  '/CCFL1D': self.tke_CCFL1D,
                  'GEOM_FL1D' : self.tke_GM1D,
                  'CINI_FL1D_T' : self.tke_CI1D,
                  'CINI_FL1D_T_FCT' : self.tke_CI1D,
                  'CINI_FL1D_T_PROG' : self.tke_CI1D,
                  'CLIM_FL1D_T' : self.tke_CL1D,
                  'CLIM_FL1D_T_FCT' : self.tke_CL1D,
                  'CLIM_FL1D_T_PROG' : self.tke_CL1D,
                  'GRAVITE_FL1D' : self.tke_GR1D,
                  'CPHY_FL1D_T' : self.tke_PHY1D,
                  'CPHY_FL1D_T_FCT' : self.tke_PHY1D,
                  'CPHY_FL1D_T_PROG' : self.tke_PHY1D,
                  'PDC_LIN_FL1D_T' : self.tke_PDCL1D,
                  'PDC_LIN_FL1D_T_FCT' : self.tke_PDCL1D,
                  'PDC_LIN_FL1D_T_PROG' : self.tke_PDCL1D,
                  'PDC_SING_FL1D_T' : self.tke_PDCS1D,
                  'PDC_SING_FL1D_T_FCT' : self.tke_PDCS1D,
                  'PDC_SING_FL1D_T_PROG' : self.tke_PDCS1D,
                  'CVOL_FL1D_T' : self.tke_SOU1D,
                  'CVOL_FL1D_T_FCT' : self.tke_SOU1D,
                  'CVOL_FL1D_T_PROG' : self.tke_SOU1D,
                  'PAS DE TEMPS FLUIDE 1D' : self.tke_PTF1D,
                  # fluid0d
                  'GEOM_FL0D' : self.tke_GM0D,
                  'CPHY_FL0D_T' : self.tke_CPHY0D,
                  'CPHY_FL0D_T_FCT' : self.tke_CPHY0D,
                  'CPHY_FL0D_T_PROG' : self.tke_CPHY0D,
                  'CVOL_FL0D_T' : self.tke_CVOL0D,
                  'CVOL_FL0D_T_FCT' : self.tke_CVOL0D,
                  'CVOL_FL0D_T_PROG' : self.tke_CVOL0D,
                  #
                  '/+' : self.tke_CM,
                  '/#' : self.tke_UC,
                  '/*******/' : self.tke_RO,
                  '' : self.tke_PAS
                  }
        
        self.KeyWordsHum = {'CINI_T' : self.tke_HIV,
                            'CINI_T_FCT' : self.tke_HIV,
                            'CINI_T_PROG' : self.tke_HIV,
                            'CVOL_T' : self.tke_HSV,
                            'CVOL_T_FCT' : self.tke_HSV,
                            'CVOL_T_PROG' : self.tke_HSV
                            } # dico spéciale Humidité
        
        self.chk=True
        self.TabFlag=False
        self.linevol=''
        self.linebil=''
        # Batterie d'itérateur pour les mopt clé se référent à un tableau
        self.BBTi=0
        self.HEi=0
        self.CRi=0
        self.FLi=0
        self.DRi=0
        self.IRi=0
        self.ICi=0
        self.ORi=0
        self.AIi=0
        self.ITi=0
        self.CVi=0
        self.PRi=0
        self.CRDi=0
        self.STBi=0
        self.VTBi=0
        self.RBSi=0
        self.VF2i=0
        self.VF3i=0
        self.VS2i=0
        self.VS3i=0
        self.VP2i=0
        self.VP3i=0
        self.MRPi=0
        self.BITi=0
        self.BIFi=0
        self.CSMi=0
        self.HMi=0
        self.SMi=0
        self.SHBi=0
        self.HVIi=0
        self.HSTi=0
        self.HMTi=0
        self.HMTa=0
        self.HBCi=0
        self.HCRi=0
        self.SCi=0
        self.VCi=0
        self.AdvModei = 0
        filei=0
        # fluid1d
        self.TVi=0
        self.GMi=0
        self.HE1Di=0
        self.FX1Di=0
        self.IN3D1Di=0
        self.IN2D1Di=0
        self.CLi=0
        self.THCLi=0
        self.DPCLi=0
        self.PHYi=0
        self.SOUi=0
        self.PDCi=0
        self.PDCSi=0
        self.CC1Di=0
        # fluid0d
        self.GM0i=0
        self.CP0Di=0
        self.CV0Di=0
        self.CL0Di=0
        #
        self.opstr=self.opfil.readline()
        self.open=True
        d=self.KeyWords.keys()
        self.com=''
        # debut modif KA (ajout)
        self.action_Advanced_mode.setChecked(False) # Par defaut, on n'est pas mode ihm avancé tant que le mot clef n'a pas été trouvé
        # fin modif KA (ajout)
        for line in self.opfil: # boucle de traitement des lignes du fichier de donnée
            self.opstr=line.rstrip().lstrip()
            #self.opstr=self.opfil.readline().rstrip().lstrip()
            if self.opstr == '':
                self.opstr=''
                pass
            # debut modif KA (modif)
            elif self.opstr.partition('= ')[0] in d and not self.action_Advanced_mode.isChecked():
            # fin modif KA (modif)
                ln=self.opstr.partition('= ')
                mc=ln[0]
                if self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked() : # prise en compte Humidité
                    if mc in self.KeyWordsHum.keys(): # dico spéciale Humidité
                        self.KeyWordsHum[mc]()
                    else:
                        self.KeyWords[mc]() # dico normale
                else:
                    self.KeyWords[mc]() # dico normale
                filei=filei+1
            #debut modif KA (modif)
            elif self.opstr.startswith('/') and self.opstr.split()[0] in d and not self.action_Advanced_mode.isChecked():
            #fin modif KA (modif)
                ln=self.opstr.split()
                mc=ln[0]
                self.KeyWords[mc]() # are in this case -> tke_UC, tke_CM, tke_RO
                filei=filei+1
            #debut modif KA (ajout)
            elif not(self.opstr.startswith('/')) and self.action_Advanced_mode.isChecked(): # Cas d'erreur d'un mot clé répertorié mais utilisé avec l'IHM en mode avancé
                self.tke_Advanced_mode()
            #fin modif KA (ajout)
            elif not(self.opstr.startswith('/')) and not (self.opstr.split()[0] in d): # Cas d'erreur d'un mot clé non répertorié
                if self.action_Advanced_mode.isChecked(): # tous mots clés non répertoriés seront considérés comme mots clés uniquement compréhensibles par SYRTHES
                    self.tke_Advanced_mode()
                else:
                    print 'ERROR unknown keyword:', self.opstr.split()[0] , " at line:",filei+2
                    QMessageBox.information(self, 'Message', 'ERROR unknown keyword: '+ self.opstr.split()[0] + " at line: " + str(filei+2), QMessageBox.Ok)
                filei=filei+1
            else:
                filei=filei+1

        self.open=False
        pass

    def tke_PAS(self, parent=None): # Fonction ne faisant rien pour une ligne vide 
        print "pass"
        pass
    
    def tke_UC(self, parent=None): # Fonction d'une ligne non prise en compte
        self.chk=False
        self.opstr=self.opstr.lstrip('/# ')
        ln=self.opstr.partition('= ')
        mc=ln[0]
        
        # after stripping off /#, procede like OpenFile function
        if mc in self.KeyWords.keys() :
            ln=self.opstr.partition('= ')
            mc=ln[0]
            if self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked() : # prise en compte Humidité
                if mc in self.KeyWordsHum.keys(): # dico spéciale Humidité
                    self.KeyWordsHum[mc]()
                else:
                    self.KeyWords[mc]() # dico normale
            else:
                self.KeyWords[mc]() # dico normale
        
        elif not(self.opstr.startswith('/')) and not (self.opstr.split()[0] in self.KeyWords.keys()): # Cas d'erreur d'un mot clé non répertorié
            if self.action_Advanced_mode.isChecked(): # tous mots clés non répertoriés seront considérés comme mots clés uniquement compréhensibles par SYRTHES
                self.tke_Advanced_mode()
            else:
                print 'ERROR unknown keyword:', self.opstr.split()[0]

    def tke_TE(self, parent=None): # Fonction du titre de l'étude 
        line=self.opstr.partition('= ')
        line=line[2]
        if line==None: pass
        self.syrthesIHMCollector.Home_form.lineEdit_7.setText(line)
        
    def tke_DP(self, parent=None): # Fonction de la dimension du problème
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        i=0
        a=self.Dim_comb_dic.values()
        while (i<len(a)):
            b=a[i]==line
            if (a[i]==line) :
                index=i
                i=i+1
            else :
                i=i+1
        i=0
        self.syrthesIHMCollector.Home_form.Dim_Comb.setCurrentIndex(index)
        self.Dimension=self.Dim_comb_dic[index]
        if index == 0 : # 3D
            self.Solar_aspect.setDisabled(False)
        else :
            self.Solar_aspect.setDisabled(True)
        
    def tke_DC(self, parent=None): # fonction du commentaire du cas
        line=self.opstr
        line=line.split(' ', 1)
        try :
            line=str(line[1])
            line=line.decode('Utf-8')
#            if len(line)!=1:
            self.syrthesIHMCollector.Home_form.Ho_Ds_te.append(line)
        except:
            self.syrthesIHMCollector.Home_form.Ho_Ds_te.append("")

    def tke_PR(self, parent=None): # Fonction de la prise en compte du rayonnement confiné
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        if line=='OUI':
            self.syrthesIHMCollector.Home_form.Ho_Tr_ch.setChecked(True)
        else:
            self.syrthesIHMCollector.Home_form.Ho_Tr_ch.setChecked(False)

    def tke_PH(self, parent=None): # Fonction de la modélisation du transfert d'humidité
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line=='0':
            self.syrthesIHMCollector.Home_form.Ho_Hm_ch.setChecked(False)
        elif line=='2':
            self.syrthesIHMCollector.Home_form.Ho_Hm_ch.setChecked(True)
            self.syrthesIHMCollector.Home_form.Hm_cmb.setCurrentIndex(0)      
        else:
            self.syrthesIHMCollector.Home_form.Ho_Hm_ch.setChecked(True)
            self.syrthesIHMCollector.Home_form.Hm_cmb.setCurrentIndex(1)

    def tke_PC(self, parent=None): # Fonction de la prise en compte du couplage conduction
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line=='OUI':
            self.syrthesIHMCollector.Home_form.Ho_Ch_ch.setChecked(True)
        else:
            self.syrthesIHMCollector.Home_form.Ho_Ch_ch.setChecked(False)

    def tke_FL1D(self, parent=None): # Fonction de la prise en compte du modele fluide 1D
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        if line=='OUI':
            self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.setChecked(True)
        else:
            self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.setChecked(False)

    def tke_FL0D(self, parent=None): # Fonction de la prise en compte du modele fluide 0D
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        if line=='OUI':
            self.syrthesIHMCollector.Home_form.Ho_fluid0d_ch.setChecked(True)
        else:
            self.syrthesIHMCollector.Home_form.Ho_fluid0d_ch.setChecked(False)

    def tke_SC(self, parent=None): # Fonction de la suite de calcul (checkbox)
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        if line=='OUI':
            self.syrthesIHMCollector.Control_form.Ch_res_cal.setChecked(True)
        else:
            self.syrthesIHMCollector.Control_form.Ch_res_cal.setChecked(False)

    def tke_ST(self, parent=None): # Fonction du nouveau temps initial pour une suite de calcul
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        self.syrthesIHMCollector.Control_form.Ch_Sr_cb.setChecked(True)
        self.syrthesIHMCollector.Control_form.lineEdit_39.setText(line)

    def tke_PTS(self, parent=None): # Fonction du pas de temps solide
        self.lineps=self.opstr.partition('= ')
        self.lineps=self.lineps[2].rstrip()
        if self.lineps==None: pass
        self.syrthesIHMCollector.Control_form.Le_const_Ts.setText(self.lineps)
        self.syrthesIHMCollector.Control_form.comb_time_st.setCurrentIndex(0)
        self.Time_step_select(0)

    def tke_NPS(self, parent=None): # Fonction du nombre de pas de temps solide
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        self.syrthesIHMCollector.Control_form.Le_Nts.setText(line)
        
    def tke_PTA(self, parent=None): # Fonction du pas de temps automatique
        line=self.opstr.split()
        line1=line[4].rstrip()
        if line1==None: pass
        self.syrthesIHMCollector.Control_form.Le_auto_Mt.setText(line1)

        if len(line) == 6: # delta_temp + delta_t_max
            line4 = line[5].strip()

        elif len(line) == 7: # delta_temp + delta_pv + delta_t_max
            line2=line[5].rstrip()
            if line2==None: pass
            self.syrthesIHMCollector.Control_form.Le_auto_Mpv.setText(line2)
            line4 =line[6].strip()

        elif len(line) == 8: # delta_temp + delta_pv + delta_pt + delta_t_max
            if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==1:
                line3=line[6].rstrip()
                if line3==None: pass
                self.syrthesIHMCollector.Control_form.Le_auto_Mpt.setText(line3)
                line4=line[7].rstrip()
                pass
            pass

        if line4==None: pass
        self.syrthesIHMCollector.Control_form.Le_auto_Mts.setText(line4)
        self.syrthesIHMCollector.Control_form.comb_time_st.setCurrentIndex(1)
        self.Time_step_select(1)
        self.syrthesIHMCollector.Control_form.Le_auto_It.setText(self.lineps)
        self.syrthesIHMCollector.Control_form.Le_const_Ts.setText('')
        pass

    def tke_CM(self, parent=None): # Fonction d'un commentaire utilisateur d'un tableau
        self.com=self.opstr.strip('/+ ')
        self.com=self.com.decode('Utf-8')

    def tke_PTM(self, parent=None): # Fonction du pas de temps multiples
        self.syrthesIHMCollector.Control_form.comb_time_st.setCurrentIndex(2)
        self.Time_step_select(2)
        i=0
        Strliste=QStringList()
        if (self.BBTi==self.syrthesIHMCollector.Control_form.By_Block_table.rowCount()):
            self.syrthesIHMCollector.Control_form.By_Block_table.insertRow(self.BBTi)
            while i<=self.BBTi:
                Strliste.append("")
                i=i+1            
            self.syrthesIHMCollector.Control_form.By_Block_table.setVerticalHeaderLabels(Strliste)
        line=self.opstr.split()
        GTSN=QTableWidgetItem()
        if line[4]==None: pass
        GTSN.setText(line[4].rstrip())
        self.syrthesIHMCollector.Control_form.By_Block_table.setItem(self.BBTi, 0, GTSN)
        TS=QTableWidgetItem()
        if line[5]==None: pass
        TS.setText(line[5].rstrip())
        self.syrthesIHMCollector.Control_form.By_Block_table.setItem(self.BBTi, 1, TS)
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        self.syrthesIHMCollector.Control_form.By_Block_table.setItem(self.BBTi, 2, UC)
        self.BBTi=self.BBTi+1
        i=0

    def tke_IST(self, parent=None): # fonction du nombre d'iteration du solveur temprérature
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        self.syrthesIHMCollector.Control_form.Le_Mni.setText(line)

    def tke_PST(self, parent=None): # fonction de la précision du solveur températures
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        self.syrthesIHMCollector.Control_form.lineEdit_43.setText(line)

    def tke_IPV(self, parent=None): # fonction du nombre d'iteration du solveur pression vapeur
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        self.syrthesIHMCollector.Control_form.Vap_Mn_le.setText(line)

    def tke_PPV(self, parent=None): # fonction de la précision du solveur pression vapeur
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        self.syrthesIHMCollector.Control_form.Vap_Sp_le.setText(line)

    def tke_IPT(self, parent=None): # fonction du nombre d'iteration du solveur pression totale
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        self.syrthesIHMCollector.Control_form.Ap_Mn_le.setText(line)

    def tke_PPT(self, parent=None): # fonction de la précision du solveur pression totale
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        self.syrthesIHMCollector.Control_form.Ap_Sp_le.setText(line)
        
    def tke_CLM(self, parent=None): # fonction des condition aux limites
        line=self.opstr.split()
        line2=self.opstr
        if line[1]=='COEF_ECH': # détection d'un mot clé de type coefficient d'echange
            if (self.HEi==self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table, self.HEi)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table.cellWidget(self.HEi, 0)
                CKB.setChecked(False)
                self.chk=True
                
            CBB=self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table.cellWidget(self.HEi, 1)
            CBB.setFocus()
            if line[0]=='CLIM_T=': # analyse du type de donnée de la ligne (constant) 
#                CBB.setFocus(0)
#                CBB.show()
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setFocus()
#                CBB.show()
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_T_PROG=': # analyse du type de donnée de la ligne (sous-programme) 
                CBB.setCurrentIndex(2)
                self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table, CBB, self.HEi)
            CH=QTableWidgetItem()
            if line[2]==None: pass
            CH.setText(line[2].rstrip())
            if line[0] != 'CLIM_T_PROG=':
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table.setItem(self.HEi, 2, CH)
                ET=QTableWidgetItem()
                ET.setText(line[3].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table.setItem(self.HEi, 3, ET)
                a=line2.split(None, 4) # ref field
                a=a[4]
            else:
                a=line2.split(None, 2) # ref field
                a=a[2]
            RE=QTableWidgetItem()
            RE.setText(a)
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table.setItem(self.HEi, 4, RE) # ref field
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table.setItem(self.HEi, 5, UC)
            self.HEi=self.HEi+1

        elif line[1]=='RES_CONTACT': # détection d'un mot clé de type résistance de contact
            if (self.CRi==self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table, self.CRi)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table.cellWidget(self.CRi, 0)
                CKB.setChecked(False)
                self.chk=True

            CBB=self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table.cellWidget(self.CRi, 1)
            CBB.setFocus()
            if line[0]=='CLIM_T=': # analyse du type de donnée de la ligne (constant) 
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBB.setCurrentIndex(2)
            CG=QTableWidgetItem()
            CG.setText(line[2].rstrip())
            if line[0]!='CLIM_T_PROG=': # line2 = "CLIM_T_PROG= RES_CONTACT  1 2 -1 3"
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table.setItem(self.CRi, 2, CG)
                a=line2.split(None, 3) # a = ['CLIM_T_PROG=', 'RES_CONTACT', '1', '2 -1 3']
                a=a[3] # a = '2 -1 3'
            else: # line2 = "CLIM_T_PROG= RES_CONTACT  2 -1 3"
                a=line2.split(None, 2) # a = ['CLIM_T_PROG=', 'RES_CONTACT', '2 -1 3']
                a=a[2] # a = '2 -1 3'
            a1=(a.partition('-1')[0]).strip() # a = '2'
            a2=(a.partition('-1')[2]).strip() # a = '3'
            RE1=QTableWidgetItem()
            RE2=QTableWidgetItem()
            RE1.setText(a1)
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table.setItem(self.CRi, 3, RE1)
            RE2.setText(a2)
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table.setItem(self.CRi, 4, RE2)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table.setItem(self.CRi, 5, UC)
            if line[0]=='CLIM_T_PROG=' : self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table, CBB, self.CRi)
            self.CRi=self.CRi+1

        elif line[1]=='FLUX': # détection d'un mot clé de type flux
            if (self.FLi==self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table, self.FLi)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table.cellWidget(self.FLi, 0)
                CKB.setChecked(False)
                self.chk=True

            CBB=self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table.cellWidget(self.FLi, 1)
            CBB.setFocus()
            if line[0]=='CLIM_T=': # analyse du type de donnée de la ligne (constant) 
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBB.setCurrentIndex(2)        
            FX=QTableWidgetItem()
            FX.setText(line[2].rstrip())
            if line[0]=='CLIM_T_PROG=': FX.setText('')
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table.setItem(self.FLi, 2, FX)
            if line[0]!='CLIM_T_PROG=': 
                a=line2.split(None, 3)
                a=a[3] 
            else:
                a=line2.split(None, 2)
                a=a[2]
                self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table, CBB, self.FLi)            
            RE=QTableWidgetItem()
            RE.setText(a)
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table.setItem(self.FLi, 3, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table.setItem(self.FLi, 4, UC)
            self.FLi=self.FLi+1

        elif line[1]=='DIRICHLET': # détection d'un mot clé de type dirichlet
            if (self.DRi==self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table, self.DRi)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table.cellWidget(self.DRi, 0)
                CKB.setChecked(False)
                self.chk=True

            CBB=self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table.cellWidget(self.DRi, 1)
            CBB.setFocus()
            if line[0]=='CLIM_T=':# analyse du type de donnée de la ligne (constant)  
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBB.setCurrentIndex(2)
                self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table, CBB, self.DRi)
            DR=QTableWidgetItem()
            DR.setText(line[2].rstrip())
            if line[0]!='CLIM_T_PROG=': self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table.setItem(self.DRi, 2, DR)
            
            RE=QTableWidgetItem()
            if line[0]=='CLIM_T_PROG=':
                a=line2.split(None, 2)
                a=a[2]
                self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table, CBB, self.DRi)
            else:
                a=line2.split(None, 3)
                a=a[3]
            RE.setText(a)
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table.setItem(self.DRi, 3, RE)
            
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table.setItem(self.DRi, 4, UC)
            self.DRi=self.DRi+1

        elif line[1]=='RAY_INFINI': # détection d'un mot clé de type rayonnement infinis
            if (self.IRi==self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table, self.IRi)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table.cellWidget(self.IRi, 0)
                CKB.setChecked(False)
                self.chk=True
                
            CBB=self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table.cellWidget(self.IRi, 1)
            CBB.setFocus()
            if line[0]=='CLIM_T=': # analyse du type de donnée de la ligne (constant)  
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBB.setCurrentIndex(2)
                
            
            EPS=QTableWidgetItem()
            ET=QTableWidgetItem()  
            RE=QTableWidgetItem()
            if line[0]=='CLIM_T_PROG=': 
                a=line2.split(None, 2)
                a=a[2]
            else:
                EPS.setText(line[2].rstrip())
                ET.setText(line[3].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table.setItem(self.IRi, 2, EPS)
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table.setItem(self.IRi, 3, ET)
                a=line2.split(None, 4)
                a=a[4]
                
            RE.setText(a)
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table.setItem(self.IRi, 4, RE)
            
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table.setItem(self.IRi, 5, UC)
            
            if line[0]=='CLIM_T_PROG=' : self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table, CBB, self.IRi)
            self.IRi=self.IRi+1
            
        elif line[1]=='ECH_OD': # détection d'un mot clé de type echange 0D
            if (self.CL0Di==self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table, self.CL0Di)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table.cellWidget(self.CL0Di, 0)
                CKB.setChecked(False)
                self.chk=True

            CBB=self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table.cellWidget(self.CL0Di, 1)
            CBB.setFocus()
            if line[0]=='CLIM_T=': # analyse du type de donnée de la ligne (constant) 
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBB.setCurrentIndex(2)        
            FX=QTableWidgetItem()
            FX.setText(line[2].rstrip())
            if line[0]=='CLIM_T_PROG=': FX.setText('')
            self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table.setItem(self.CL0Di, 2, FX)
            if line[0]!='CLIM_T_PROG=': 
                a=line2.split(None, 3)
                a=a[3] 
            else:
                a=line2.split(None, 2)
                a=a[2]
                self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table, CBB, self.CL0Di)            
            RE=QTableWidgetItem()
            RE.setText(a)
            self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table.setItem(self.CL0Di, 3, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table.setItem(self.CL0Di, 4, UC)
            self.CL0Di=self.CL0Di+1

    def tke_CPYI(self, parent=None): # Fonction des conditions physique en isotropie
        line=self.opstr.split()
        line2=self.opstr
        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
            ICtable=self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table
        else:
            ICtable=self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table
        if (self.ICi==ICtable.rowCount()):
            self.Open_New_Line(ICtable, self.ICi)    
        if self.chk==False:
            CKB=ICtable.cellWidget(self.ICi, 0)
            CKB.setChecked(False)
            self.chk=True
            
        CBB=ICtable.cellWidget(self.ICi, 1)
        CBB.setFocus()
        if line[0]=='CPHY_MAT_ISO=': # analyse du type de donnée de la ligne (constant)  
            CBB.setCurrentIndex(0)
        elif line[0]=='CPHY_MAT_ISO_FCT=': # analyse du type de donnée de la ligne (fonction)
            CBB.setCurrentIndex(1)
        elif line[0]=='CPHY_MAT_ISO_PROG=': # analyse du type de donnée de la ligne (sous-programme)            
            CBB.setCurrentIndex(2)
            self.Table_prog2(ICtable, CBB, self.ICi)
        if line[0]!='CPHY_MAT_ISO_PROG=':
            RHO=QTableWidgetItem()
            RHO.setText(line[1].rstrip())
            ICtable.setItem(self.ICi, 2, RHO)
            CP=QTableWidgetItem()
            CP.setText(line[2].rstrip())
            ICtable.setItem(self.ICi, 3, CP)
            K=QTableWidgetItem()
            K.setText(line[3].rstrip())
            ICtable.setItem(self.ICi, 4, K)
            RE=QTableWidgetItem()
            a=line2.split(None, 4)
            a=a[4]
            RE.setText(a)
            ICtable.setItem(self.ICi, 5, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            ICtable.setItem(self.ICi, 6, UC)

        else:
            RE=QTableWidgetItem()
            a=line2.split(None, 1)
            a=a[1]
            RE.setText(a)
            ICtable.setItem(self.ICi, 5, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            ICtable.setItem(self.ICi, 6, UC)
        self.ICi=self.ICi+1

    def tke_CPYO(self, parent=None): # Fonction des conditions physique en orthotropie
        line=self.opstr.split()
        line2=self.opstr
        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
            OCtable=self.syrthesIHMCollector.Physical_prop_3D_form.Ort_cond_3D_table
        else:
            OCtable=self.syrthesIHMCollector.Physical_prop_2D_form.Ort_cond_2D_table
        if (self.ORi==OCtable.rowCount()):
            self.Open_New_Line(OCtable, self.ORi)
            
        if self.chk==False:
            CKB=OCtable.cellWidget(self.ORi, 0)
            CKB.setChecked(False)
            self.chk=True
            
        CBB=OCtable.cellWidget(self.ORi, 1)
        CBB.setFocus()
        OCtable.setCurrentCell (self.ORi, 1)
        if line[0]=='CPHY_MAT_ORTHO_2D=' or line[0]=='CPHY_MAT_ORTHO_3D=': # analyse du type de donnée de la ligne (constant)  
            CBB.setCurrentIndex(0)
        elif line[0]=='CPHY_MAT_ORTHO_2D_FCT=' or line[0]=='CPHY_MAT_ORTHO_3D_FCT=': # analyse du type de donnée de la ligne (fonction)
            CBB.setCurrentIndex(1)
        elif line[0]=='CPHY_MAT_ORTHO_2D_PROG=' or line[0]=='CPHY_MAT_ORTHO_3D_PROG=': # analyse du type de donnée de la ligne (sous-programme)
            CBB.setCurrentIndex(2)
            self.Table_prog2(OCtable, CBB, self.ORi)
        if line[0]!='CPHY_MAT_ORTHO_2D_PROG=' and line[0]!='CPHY_MAT_ORTHO_3D_PROG=':
            RHO=QTableWidgetItem()
            RHO.setText(line[1].rstrip())
            OCtable.setItem(self.ORi, 2, RHO)
            CP=QTableWidgetItem()
            CP.setText(line[2].rstrip())
            OCtable.setItem(self.ORi, 3, CP)    
            KX=QTableWidgetItem()
            KX.setText(line[3].rstrip())
            OCtable.setItem(self.ORi, 4, KX)
            KY=QTableWidgetItem()
            KY.setText(line[4].rstrip())
            OCtable.setItem(self.ORi, 5, KY)
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                KZ=QTableWidgetItem()
                KZ.setText(line[5].rstrip())
                OCtable.setItem(self.ORi, 6, KZ)
            RE=QTableWidgetItem()
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                a=line2.split(None, 6)
                a=a[6]
                RE.setText(a)
                OCtable.setItem(self.ORi, 7, RE)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                OCtable.setItem(self.ORi, 8, UC)
            else:
                a=line2.split(None, 5)
                a=a[5]
                RE.setText(a)
                OCtable.setItem(self.ORi, 6, RE)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                OCtable.setItem(self.ORi, 7, UC)
        else:
            RE=QTableWidgetItem()
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                a=line2.split(None, 1)
                a=a[1]                
                RE.setText(a)
                OCtable.setItem(self.ORi, 7, RE)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                OCtable.setItem(self.ORi, 8, UC)
            else:
                a=line2.split(None, 1)
                a=a[1]
                RE.setText(a)
                OCtable.setItem(self.ORi, 6, RE)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                OCtable.setItem(self.ORi, 7, UC)
        
        self.ORi=self.ORi+1

    def tke_CPYA(self, parent=None): # Fonction des conditions physique en anisothropie
        line=self.opstr.split()
        line2=self.opstr
        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
            ACtable=self.syrthesIHMCollector.Physical_prop_3D_form.Ani_cond_3D_table
        else:
            ACtable=self.syrthesIHMCollector.Physical_prop_2D_form.Ani_cond_2D_table
        if (self.AIi==ACtable.rowCount()):
            self.Open_New_Line(ACtable, self.AIi)
            
        if self.chk==False:
            CKB=ACtable.cellWidget(self.AIi, 0)
            CKB.setChecked(False)
            self.chk=True
            
        CBB=ACtable.cellWidget(self.AIi, 1)
        CBB.setFocus()
        if line[0]=='CPHY_MAT_ANISO_2D=' or line[0]=='CPHY_MAT_ANISO_3D=': # analyse du type de donnée de la ligne (constant)
            CBB.setCurrentIndex(0)
        elif line[0]=='CPHY_MAT_ANISO_2D_FCT=' or line[0]=='CPHY_MAT_ANISO_3D_FCT=': # analyse du type de donnée de la ligne (fonction)
            CBB.setCurrentIndex(1)
        elif line[0]=='CPHY_MAT_ANISO_2D_PROG=' or line[0]=='CPHY_MAT_ANISO_3D_PROG=': # analyse du type de donnée de la ligne (sous-programme)
            CBB.setCurrentIndex(2)
            self.Table_prog2(ACtable, CBB, self.AIi)
        RE=QTableWidgetItem()
        if line[0]!='CPHY_MAT_ANISO_2D_PROG=' and line[0]!='CPHY_MAT_ANISO_3D_PROG=':
            RHO=QTableWidgetItem()
            RHO.setText(line[1].rstrip())
            ACtable.setItem(self.AIi, 2, RHO)
            CP=QTableWidgetItem()
            CP.setText(line[2].rstrip())
            ACtable.setItem(self.AIi, 3, CP)    
            KX=QTableWidgetItem()
            KX.setText(line[3].rstrip())
            ACtable.setItem(self.AIi, 4, KX)
            KY=QTableWidgetItem()
            KY.setText(line[4].rstrip())
            ACtable.setItem(self.AIi, 5, KY)
            
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                KZ=QTableWidgetItem()
                KZ.setText(line[5].rstrip())
                ACtable.setItem(self.AIi, 6, KZ)
                
                UX=QTableWidgetItem()
                UY=QTableWidgetItem()
                UZ=QTableWidgetItem()
                VX=QTableWidgetItem()
                VY=QTableWidgetItem()
                VZ=QTableWidgetItem()
                WX=QTableWidgetItem()
                WY=QTableWidgetItem()
                WZ=QTableWidgetItem()
                UX.setText(line[6].rstrip())
                UY.setText(line[7].rstrip())
                UZ.setText(line[8].rstrip())
                VX.setText(line[9].rstrip())
                VY.setText(line[10].rstrip())
                VZ.setText(line[11].rstrip())
                WX.setText(line[12].rstrip())
                WY.setText(line[13].rstrip())
                WZ.setText(line[14].rstrip())
                
                ACtable.setItem(self.AIi, 7, UX)
                ACtable.setItem(self.AIi, 8, UY)
                ACtable.setItem(self.AIi, 9, UZ)
                ACtable.setItem(self.AIi, 10, VX)
                ACtable.setItem(self.AIi, 11, VY)
                ACtable.setItem(self.AIi, 12, VZ)
                ACtable.setItem(self.AIi, 13, WX)
                ACtable.setItem(self.AIi, 14, WY)
                ACtable.setItem(self.AIi, 15, WZ)
                
                a=line2.split(None, 16)
                a=a[15]
                RE.setText(a)
                ACtable.setItem(self.AIi, 16, RE)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                ACtable.setItem(self.AIi, 17, UC)
            else:
                AN=QTableWidgetItem()
                AN.setText(line[5].rstrip())
                ACtable.setItem(self.AIi, 6, AN)
                a=line2.split(None, 6)
                a=a[6]
                RE.setText(a)
                ACtable.setItem(self.AIi, 7, RE)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                ACtable.setItem(self.AIi, 8, UC)
        else:
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                a=line2.split(None, 1)
                a=a[1]
                RE.setText(a)
                ACtable.setItem(self.AIi, 16, RE)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                ACtable.setItem(self.AIi, 17, UC)
            else:
                a=line2.split(None, 1)
                a=a[1]
                RE.setText(a)
                ACtable.setItem(self.AIi, 7, RE)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                ACtable.setItem(self.AIi, 8, UC)
        self.AIi=self.AIi+1
        
    def tke_CIN(self, parent=None): # Fonction des conditions initial
        line=self.opstr.split()
        line2=self.opstr
        if self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked()==False:
            if (self.ITi==self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table, self.ITi)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table.cellWidget(self.ITi, 0)
                CKB.setChecked(False)
                self.chk=True

            CBB=self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table.cellWidget(self.ITi, 1)
            CBB.setFocus()
            if line[0]=='CINI_T=': # analyse du type de donnée de la ligne (constant)
                CBB.setCurrentIndex(0)
            elif line[0]=='CINI_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setCurrentIndex(1)
            elif line[0]=='CINI_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBB.setCurrentIndex(2)
                self.Table_prog2(self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table, CBB, self.ITi)
            IT=QTableWidgetItem()
            IT.setText(line[1])
            if line[0]!='CINI_T_PROG=': self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table.setItem(self.ITi, 2, IT)
            RE=QTableWidgetItem()
            if line[0]=='CINI_T_PROG=':
                a=line2.split(None, 1)
                a=a[1]
            else : 
                a=line2.split(None, 2)
                a=a[2]
            RE.setText(a)
            self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table.setItem(self.ITi, 3, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table.setItem(self.ITi, 4, UC)
            self.ITi=self.ITi+1

        else:
            if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==0:
                Table=self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.Vch_Ic_TPv_table
            else:
                Table=self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.Vch_Ic_TPvPt_table
            if self.chk==False:
                CKB=Table.cellWidget(self.HVIi, 0)
                CKB.setChecked(False)
                self.chk=True
            CBBT=Table.cellWidget(self.HVIi, 1)
            CBBT.setFocus()
            if line[0]=='CINI_T=': # analyse du type de donnée de la ligne (constant)
                CBBT.setCurrentIndex(0)
            elif line[0]=='CINI_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBBT.setCurrentIndex(1)
            elif line[0]=='CINI_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBBT.setCurrentIndex(2)
                self.Table_prog2(Table, CBBT, self.HVIi)
            Table.cellWidget(self.HVIi, 2).setCurrentIndex(0)
            VA=QTableWidgetItem()
            VA.setText(line[1])
            Table.setItem(self.HVIi, 3, VA)
            RE=QTableWidgetItem()
            if line[0]=='CINI_T_PROG=':
                a=line2.split(None, 1)
                a=a[1]
            else:
                a=line2.split(None, 2)
                a=a[2]
            RE.setText(a)
            Table.setItem(self.HVIi, 4, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.HVIi, 5, UC)
            self.HVIi=self.HVIi+1

    def tke_CVO(self, parent=None): # Fonction des conditions volumiques
        line=self.opstr.split()
        line2=self.opstr
        if self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked()==False:
            if self.linevol!=line2:
                if (self.CVi==self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table.rowCount()):
                    self.Open_New_Line(self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table, self.CVi)
                    
                if self.chk==False:
                    CKB=self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table.cellWidget(self.CVi, 0)
                    CKB.setChecked(False)
                    self.chk=True

                CBB=self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table.cellWidget(self.CVi, 1)
                CBB.setFocus()
                if line[0]=='CVOL_T=': # analyse du type de donnée de la ligne (constant)
                    CBB.setCurrentIndex(0)
                elif line[0]=='CVOL_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                    CBB.setCurrentIndex(1)
                elif line[0]=='CVOL_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                    CBB.setCurrentIndex(2)
                    self.Table_prog2(self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table, CBB, self.CVi)
                if line[0]!='CVOL_T_PROG=':
                    CoefA=QTableWidgetItem() # line = ['CVOL_T', '1', '2', '3']
                    CoefA.setText(line[1]) # line[1] = '1'
                    CoefB=QTableWidgetItem() # line = ['CVOL_T', '1', '2', '3']
                    CoefB.setText(line[2]) # line[2] = '2'
                    self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table.setItem(self.CVi, 2, CoefA)
                    self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table.setItem(self.CVi, 3, CoefB)
                    RE=QTableWidgetItem()
                    a=line2.split(None, 3) # a = ['CVOL_T', '1', '2', '3']
                    a=a[3] # a = '3'
                    RE.setText(a)
                    self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table.setItem(self.CVi, 4, RE)
                else:
                    RE=QTableWidgetItem()
                    a=line2.split(None, 1) # a = ['CVOL_T', '3 4 5']
                    a=a[1] # a = '3 4 5'
                    RE.setText(a)
                    self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table.setItem(self.CVi, 4, RE)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table.setItem(self.CVi, 5, UC)
                self.CVi=self.CVi+1
                self.linevol=self.opstr
            else:
                pass
        else:
            if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==0:
                Table=self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.Vch_St_TPv_table
            else:
                Table=self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.Vch_St_TPvPt_table
            if self.chk==False:
                CKB=Table.cellWidget(self.HSTi, 0)
                CKB.setChecked(False)
                self.chk=True
            CBBT=Table.cellWidget(self.HSTi, 1)
            CBBT.setFocus()
            if line[0]=='CVOL_T=': # analyse du type de donnée de la ligne (constant)
                CBBT.setCurrentIndex(0)
            elif line[0]=='CVOL_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBBT.setCurrentIndex(1)
            elif line[0]=='CVOL_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBBT.setCurrentIndex(2)
                self.Table_prog2(Table, CBBT, self.HSTi)
            Table.cellWidget(self.HSTi, 2).setCurrentIndex(0)
            if line[0]!='CVOL_T_PROG=':                
                a=line2.split(None, 3) # a = ['CVOL_T', '1', '2', '3 4 5']
                CoefA=QTableWidgetItem()
                CoefA.setText(a[1]) # a[1] = '1'
                Table.setItem(self.HSTi, 3, CoefA)
                CoefB=QTableWidgetItem()
                CoefB.setText(a[2]) # a[2] = '2'
                Table.setItem(self.HSTi, 4, CoefB)
                RE=QTableWidgetItem()
                RE.setText(a[3]) # a[3] = '3 4 5'
                Table.setItem(self.HSTi, 5, RE)
            else :
                RE=QTableWidgetItem()
                a=line2.split(None, 1) # line2 = "CVOL_T= 3 4 5", a = ['CVOL_T', '3 4 5']
                a=a[1] # a = '3 4 5'
                RE.setText(a)
                Table.setItem(self.HSTi, 5, RE)
                                
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.HSTi, 6, UC)
            self.HSTi=self.HSTi+1

        
    def tke_PERCHT(self, parent=None): # Fonction de la périodicité en conduction et du couplage heat transfer
        line=self.opstr.split()
        line2=self.opstr
        if line[1]=='COUPLAGE_RAYONNEMENT': # détection d'un mot clé de type couplage rayonnement. Le traitement de ce mot clé se fait ici car il a le même entête que les périodicités
            a=line2.split(None, 2)
            a=a[2]
            a=a.decode('Utf-8')
            self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne.setText(a)
        elif line[1]=='COUPLAGE_FLUIDE_1D': # détection d'un mot clé de type couplage fluide 1D. Le traitement de ce mot clé se fait ici car il a le même entête que les périodicités

            if (self.CC1Di==self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table, self.CC1Di)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table.cellWidget(self.CC1Di, 0)
                CKB.setChecked(False)
                self.chk=True
                
            CBB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table.cellWidget(self.CC1Di, 1)
            CBB.setFocus()
            if line[2]=='H_COLBURN': # analyse du type de donnée de la ligne (constant) 
                CBB.setCurrentIndex(1)
                self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table, CBB, self.CC1Di)
            elif line[2]=='H_CONSTANT': # analyse du type de donnée de la ligne (fonction)
                CBB.setFocus()
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_PROG=': # analyse du type de donnée de la ligne (sous-programme) 
                CBB.setCurrentIndex(2)
                self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table, CBB, self.CC1Di)
            if line[1]==None: pass
            if line[2]=='H_CONSTANT':
                CB=QTableWidgetItem()
                CB.setText(line[3].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table.setItem(self.CC1Di, 2, CB)
                a=line2.split(None, 4)
                a=a[4]
            if  line[0]=='CLIM_PROG=': 
                a=line2.split(None, 2)
                a=a[2]
            elif line[2]=='H_COLBURN': 
                a=line2.split(None, 3)
                a=a[3]
            RE=QTableWidgetItem()
            RE.setText(a)
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table.setItem(self.CC1Di, 3, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table.setItem(self.CC1Di, 4, UC)
            self.CC1Di=self.CC1Di+1
            
            
        elif line[1]=='COUPLAGE_SURF_FLUIDE' or line[1]=='COUPLAGE_VOL_FLUIDE': #couplage heat transfer
            a=line2.split(None, 3)
            self.syrthesIHMCollector.Home_form.Ho_Ch_ch.setChecked(True)
            NA=QTableWidgetItem()
            NA.setText(a[2])
            RE=QTableWidgetItem()
            RE.setText(a[3])
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            if line[1]=='COUPLAGE_SURF_FLUIDE':
                CHTTab=self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Sc_table
                if self.chk==False:
                    CKB=CHTTab.cellWidget(self.SCi, 0)
                    CKB.setChecked(False)
                    self.chk=True                
                CHTTab.setItem(self.SCi, 1, NA)            
                CHTTab.setItem(self.SCi, 2, RE)
                CHTTab.setItem(self.SCi, 3, UC)
                self.SCi=self.SCi+1                
            else:
                CHTTab=self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Vc_table
                if self.chk==False:
                    CKB=CHTTab.cellWidget(self.VCi, 0)
                    CKB.setChecked(False)
                    self.chk=True                    
                CHTTab.setItem(self.VCi, 1, NA)                
                CHTTab.setItem(self.VCi, 2, RE)
                CHTTab.setItem(self.VCi, 3, UC)
                self.VCi=self.VCi+1   
        else:
            if line[1]=='PERIODICITE_2D' and line[2]=='R': # détection d'un mot clé de type périodicités 2D en rotation
                PerTab=self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_rot_table
                self.TabFlag=True
            elif line[1]=='PERIODICITE_2D' and line[2]=='T': # détection d'un mot clé de type périodicités 2D en translation
                PerTab=self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_tra_table
                if self.TabFlag==True:
                    self.PRi=0
                    self.TabFlag=False
            elif line[1]=='PERIODICITE_3D' and line[2]=='R': # détection d'un mot clé de type périodicités 3D en rotation
                PerTab=self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_rot_table
                self.TabFlag=True
            elif line[1]=='PERIODICITE_3D' and line[2]=='T': # détection d'un mot clé de type périodicités 3D en translation
                PerTab=self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_tra_table
                if self.TabFlag==True:
                    self.PRi=0
                    self.TabFlag=False
                    
            if (self.PRi==PerTab.rowCount()):
                self.Open_New_Line(PerTab, self.PRi)
            if self.chk==False:
                CKB=PerTab.cellWidget(self.PRi, 0)
                CKB.setChecked(False)
                self.chk=True
            PX=QTableWidgetItem()
            PX.setText(line[3].rstrip())
            PerTab.setItem(self.PRi, 1, PX)
            PY=QTableWidgetItem()
            PY.setText(line[4].rstrip())
            PerTab.setItem(self.PRi, 2, PY)
            if line[1]=='PERIODICITE_3D':
                PZ=QTableWidgetItem()
                PZ.setText(line[5].rstrip())
                PerTab.setItem(self.PRi, 3, PZ)
                if line[2]=='R': 
                    AX=QTableWidgetItem()
                    AX.setText(line[6].rstrip())
                    PerTab.setItem(self.PRi, 4, AX)
                    AY=QTableWidgetItem()
                    AY.setText(line[7].rstrip())
                    PerTab.setItem(self.PRi, 5, AY)
                    AZ=QTableWidgetItem()
                    AZ.setText(line[8].rstrip())
                    PerTab.setItem(self.PRi, 6, AZ)
                    AN=QTableWidgetItem()
                    AN.setText(line[9].rstrip())
                    PerTab.setItem(self.PRi, 7, AN)

            else:
                if line[2]=='R':
                    AN=QTableWidgetItem()
                    AN.setText(line[5].rstrip())
                    PerTab.setItem(self.PRi, 3, AN)
            if line[1]=='PERIODICITE_2D' and line[2]=='R': # Détermination du premier group de référence selon le type de périodicité
                i=7
                group1=line[6]
                posg1=4
            elif line[1]=='PERIODICITE_2D' and line[2]=='T':
                i=6
                group1=line[5]
                posg1=3
            elif line[1]=='PERIODICITE_3D' and line[2]=='R':
                i=11
                group1=line[10]
                posg1=8
            else:
                i=7
                group1=line[6]
                posg1=4
            while line[i]!='-1':
                group1=group1+' '+line[i]
                i=i+1
            i=i+1
            F1=QTableWidgetItem()
            F1.setText(group1)
            PerTab.setItem(self.PRi, posg1, F1)
            group2=line[i]
            i=i+1
            while i<len(line):
                group2=group2+' '+line[i]
                i=i+1
            F2=QTableWidgetItem()
            F2.setText(group2)
            PerTab.setItem(self.PRi, posg1+1, F2)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            PerTab.setItem(self.PRi, posg1+2, UC)

            self.PRi=self.PRi+1



    def tke_OUT(self, parent=None): # Fonction des sorties du calcul
        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
            CBB=self.syrthesIHMCollector.Output_3D_form.Cb_3D_Op
            LNE=self.syrthesIHMCollector.Output_3D_form.Le_3D_Op
            TRC=self.syrthesIHMCollector.Output_3D_form.Tf_cb_3D_Op
            CFT=self.syrthesIHMCollector.Output_3D_form.Hf_cb_3D_Op
            CTM=self.syrthesIHMCollector.Output_3D_form.Mt_cb_3D_Op
            S3D=self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op
            but = self.syrthesIHMCollector.Output_3D_form.Ai_but_3D_Op
        else:
            CBB=self.syrthesIHMCollector.Output_2D_form.Cb_2D_Op
            LNE=self.syrthesIHMCollector.Output_2D_form.Le_2D_Op
            TRC=self.syrthesIHMCollector.Output_2D_form.Tf_cb_2D_Op
            CFT=self.syrthesIHMCollector.Output_2D_form.Hf_cb_2D_Op
            CTM=self.syrthesIHMCollector.Output_2D_form.Mt_cb_2D_Op
            S3D=self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op
            but = self.syrthesIHMCollector.Output_2D_form.Ai_but_2D_Op
            
        line=self.opstr.split()
        if line[0]=='INSTANTS': # détection d'un mot clé de type INSTANTS SORTIES CHRONO SOLIDE SECONDES
            CBB.setCurrentIndex(2)
            line=self.opstr.split(' ', 5)
            LNE.setText(line[5])
            TRC.setChecked(True)
            but.setEnabled(True)
        elif line[0] == 'PAS' and line[5]=='SECONDES=': # détection d'un mot clé de type PAS DES SORTIES CHRONO SOLIDE SECONDES 
            CBB.setCurrentIndex(1)
            line=self.opstr.rsplit(' ', 1)
            LNE.setText(line[1])
            TRC.setChecked(True)
        elif line[0] == 'SUPPRESSION':
            if line[5] == 'OUI':
                S3D.setChecked(True)
            else:
                S3D.setChecked(False)
            pass
        elif line[0] == 'CHAMP' and line[2] == 'TEMPERATURES':
            if line[4] == 'OUI':
                CTM.setChecked(True)
            else:
                CTM.setChecked(False)
            pass
        elif line[0] == 'CHAMP' and line[2] == 'FLUX':
            if line[4] == 'OUI':
                CFT.setChecked(True)
                pass
            else:
                CFT.setChecked(False)
                pass
            pass
        else: # détection d'un mot clé de type INSTANTS SORTIES CHRONO SOLIDE SECONDES 
            CBB.setCurrentIndex(0)
            line=self.opstr.partition('= ')
            line=line[2].rstrip()
            LNE.setText(line)
            TRC.setChecked(True)

    def tke_HST(self, parent=None): # Fonction de l'historique de calcul
        line=self.opstr.split()
        
        if line[1]=='FREQ_ITER' or line[1]=='FREQ_SECONDS' or line[1]=='FREQ_LIST_TIMES' : # détection d'un mot clé de type fréquence
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                LNE = self.syrthesIHMCollector.Output_3D_form.Le2_3D_Op
                CB2 = self.syrthesIHMCollector.Output_3D_form.Cb2_3D_Op
            else:
                LNE = self.syrthesIHMCollector.Output_2D_form.Le2_2D_Op
                CB2 = self.syrthesIHMCollector.Output_2D_form.Cb2_2D_Op
            line=self.opstr.split()
            LNE.setText(line[2])
            if line[1]== 'FREQ_ITER' :
                CB2.setCurrentIndex(0)
            elif line[1]== 'FREQ_SECONDS':
                CB2.setCurrentIndex(1)
            else: # 'FREQ_LIST_TIME'
                CB2.setCurrentIndex(2)
        else: # détection d'un mot clé de type coordonnées
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                Table=self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table
                line=self.opstr.rsplit(' ',3)
            else:
                Table=self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table
                line=self.opstr.rsplit(' ',2)
                
            if (self.CRDi==Table.rowCount()):
                self.Open_New_Line(Table, self.CRDi)
            
            if self.chk==False:
                CKB=Table.cellWidget(self.CRDi, 0)
                CKB.setChecked(False)
                self.chk=True
            X=QTableWidgetItem()
            X.setText(line[1])
            Table.setItem(self.CRDi, 1, X)
            Y=QTableWidgetItem()
            Y.setText(line[2])
            Table.setItem(self.CRDi, 2, Y)
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                Z=QTableWidgetItem()
                Z.setText(line[3])
                Table.setItem(self.CRDi, 3, Z)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.CRDi, Table.columnCount()-1, UC)
            self.CRDi=self.CRDi+1

    def tke_BF(self, parent=None): # Fonction des bilans
        line2=self.opstr
        if self.linebil!=line2:
            Mot=self.opstr.partition('= ')
            if Mot[0]=='BILAN FLUX SURFACIQUES': # détection d'un mot clé de type bilan surfacique
                if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                    Table=self.syrthesIHMCollector.Output_3D_form.Op_Sb_3D_table
                else:
                    Table=self.syrthesIHMCollector.Output_2D_form.Op_Sb_2D_table
                if (self.STBi==Table.rowCount()):
                    self.Open_New_Line(Table, self.STBi)
                if self.chk==False:
                    CKB=Table.cellWidget(self.STBi, 0)
                    CKB.setChecked(False)
                    self.chk=True
                line=self.opstr.partition('= ')
                ref=line[2].strip()
                RE=QTableWidgetItem()
                RE.setText(ref)
                Table.setItem(self.STBi, 1, RE)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                Table.setItem(self.STBi, 2, UC)
                self.STBi=self.STBi+1
            else: # détection d'un mot clé de type bilan volumique
                if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                    Table=self.syrthesIHMCollector.Output_3D_form.Op_Vb_3D_table
                else:
                    Table=self.syrthesIHMCollector.Output_2D_form.Op_Vb_2D_table

                if (self.VTBi==Table.rowCount()):
                    self.Open_New_Line(Table, self.VTBi)
                    
                if self.chk==False:
                    CKB=Table.cellWidget(self.VTBi, 0)
                    CKB.setChecked(False)
                    self.chk=True
                    
                line=self.opstr.partition('= ')
                ref=line[2].strip()
                RE=QTableWidgetItem()
                RE.setText(ref)
                Table.setItem(self.VTBi, 1, RE)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                Table.setItem(self.VTBi, 2, UC)
                self.VTBi=self.VTBi+1
        self.linebil=self.opstr

    def tke_MC(self, parent=None): # Fonction des maillages conduction
        line=self.opstr.partition('= ')
        self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.setText(line[2])

    def tke_MR(self, parent=None): # Fonction des maillages rayonnement
        line=self.opstr.partition('= ')
        self.syrthesIHMCollector.Filename_form.Fn_Rm_lne.setText(line[2])

    def tke_MA1D(self, parent=None): # Fonction des maillages fluid1d
        line=self.opstr.partition('= ')
        self.syrthesIHMCollector.Filename_form.Fn_fluid1d_lne.setText(line[2])

    def tke_RSC(self, parent=None): # Fonction de la prise en compte du résultat précédent pour la suite de calcul
        line=self.opstr.partition('= ')
        self.syrthesIHMCollector.Filename_form.Fn_Rs_lne.setText(line[2])
        self.syrthesIHMCollector.Control_form.Ch_res_cal.setChecked(True)

    def tke_FM(self, parent=None): # Fonction des fichiers météo
        line=self.opstr.partition('= ')
        self.syrthesIHMCollector.Filename_form.Fn_Mt_lne.setText(line[2])
         
    def tke_PRF(self, parent=None): # Fonction du préfixe des fichiers résultats
         line=self.opstr.partition('= ')
         self.syrthesIHMCollector.Filename_form.Fn_Rnp_lne.setText(line[2])

    def tke_RBS(self, parent=None): # Fonction du rayonnement
        line=self.opstr.split()
        line2=self.opstr
        if line[1]=="BANDES_SPECTRALES": # détection d'un mot clé de type BANDES_SPECTRALES 
            Table=self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table
            if self.chk==False:
                CKB=Table.cellWidget(self.RBSi, 0)
                CKB.setChecked(False)
                self.chk=True
            BA=QTableWidgetItem()
            BA.setText(line[2].rstrip())
            Table.setItem(self.RBSi, 1, BA)
            LB=QTableWidgetItem()
            LB.setText(line[3].rstrip())
            Table.setItem(self.RBSi, 2, LB)
            UB=QTableWidgetItem()
            UB.setText(line[4].rstrip())
            Table.setItem(self.RBSi, 3, UB)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.RBSi, 4, UC)
            self.RBSi=self.RBSi+1
            
        elif line[1]=="VOLUME_CONNEXE": # détection d'un mot clé de type VOLUME_CONNEXE
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                Table=self.syrthesIHMCollector.View_factor_3D_form.Vf_Ip_3D_table
                if self.chk==False:
                    CKB=Table.cellWidget(self.VF3i, 0)
                    CKB.setChecked(False)
                    self.chk=True
                CX=QTableWidgetItem()
                CX.setText(line[2].rstrip())
                Table.setItem(self.VF3i, 1, CX)
                CY=QTableWidgetItem()
                CY.setText(line[3].rstrip())
                Table.setItem(self.VF3i, 2, CY)
                CZ=QTableWidgetItem()
                CZ.setText(line[4].rstrip())
                Table.setItem(self.VF3i, 3, CZ)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                Table.setItem(self.VF3i, 4, UC)
                self.VF3i=self.VF3i+1
            else:
                Table=self.syrthesIHMCollector.View_factor_2D_form.Vf_Ip_2D_table
                if self.chk==False:
                    CKB=Table.cellWidget(self.VF2i, 0)
                    CKB.setChecked(False)
                    self.chk=True
                CX=QTableWidgetItem()
                CX.setText(line[2].rstrip())
                Table.setItem(self.VF2i, 1, CX)
                CY=QTableWidgetItem()
                CY.setText(line[3].rstrip())
                Table.setItem(self.VF2i, 2, CY)
                UC=QTableWidgetItem()
                UC.setText(self.com)
                self.com=''
                Table.setItem(self.VF2i, 3, UC)
                self.VF2i=self.VF2i+1

        elif line[1]=="SYMETRIE_2D": # détection d'un mot clé de type SYMETRIE_2D
            Table=self.syrthesIHMCollector.View_factor_2D_form.Vf_Sy_2D_table
            if self.chk==False:
                CKB=Table.cellWidget(self.VS2i, 0)
                CKB.setChecked(False)
                self.chk=True
            CA=QTableWidgetItem()
            CA.setText(line[2].rstrip())
            Table.setItem(self.VS2i, 1, CA)
            CB=QTableWidgetItem()
            CB.setText(line[3].rstrip())
            Table.setItem(self.VS2i, 2, CB)
            CC=QTableWidgetItem()
            CC.setText(line[4].rstrip())
            Table.setItem(self.VS2i, 3, CC)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.VS2i, 4, UC)
            self.VS2i=self.VS2i+1

        elif line[1]=="SYMETRIE_3D": # détection d'un mot clé de type SYMETRIE_3D
            Table=self.syrthesIHMCollector.View_factor_3D_form.Vf_Sy_3D_table
            if self.chk==False:
                CKB=Table.cellWidget(self.VS3i, 0)
                CKB.setChecked(False)
                self.chk=True
            CA=QTableWidgetItem()
            CA.setText(line[2].rstrip())
            Table.setItem(self.VS3i, 1, CA)
            CB=QTableWidgetItem()
            CB.setText(line[3].rstrip())
            Table.setItem(self.VS3i, 2, CB)
            CC=QTableWidgetItem()
            CC.setText(line[4].rstrip())
            Table.setItem(self.VS3i, 3, CC)
            CD=QTableWidgetItem()
            CD.setText(line[5].rstrip())
            Table.setItem(self.VS3i, 4, CD)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.VS3i, 5, UC)
            self.VS3i=self.VS3i+1

        elif line[1]=="PERIODICITE_2D": # détection d'un mot clé de type PERIODICITE_2D
            Table=self.syrthesIHMCollector.View_factor_2D_form.Vf_Pe_2D_table
            if self.chk==False:
                CKB=Table.cellWidget(self.VP2i, 0)
                CKB.setChecked(False)
                self.chk=True
            IX=QTableWidgetItem()
            IX.setText(line[2].rstrip())
            Table.setItem(self.VP2i, 1, IX)
            IY=QTableWidgetItem()
            IY.setText(line[3].rstrip())
            Table.setItem(self.VP2i, 2, IY)
            TT=QTableWidgetItem()
            TT.setText(line[4].rstrip())
            Table.setItem(self.VP2i, 3, TT)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.VP2i, 4, UC)
            self.VP2i=self.VP2i+1

        elif line[1]=="PERIODICITE_3D": # détection d'un mot clé de type PERIODICITE_3D 
            Table=self.syrthesIHMCollector.View_factor_3D_form.Vf_Pe_3D_table
            if self.chk==False:
                CKB=Table.cellWidget(self.VP3i, 0)
                CKB.setChecked(False)
                self.chk=True
            IX=QTableWidgetItem()
            IX.setText(line[2].rstrip())
            Table.setItem(self.VP3i, 1, IX)
            IY=QTableWidgetItem()
            IY.setText(line[3].rstrip())
            Table.setItem(self.VP3i, 2, IY)
            IZ=QTableWidgetItem()
            IZ.setText(line[4].rstrip())
            Table.setItem(self.VP3i, 3, IZ)
            VX=QTableWidgetItem()
            VX.setText(line[5].rstrip())
            Table.setItem(self.VP3i, 4, VX)
            VY=QTableWidgetItem()
            VY.setText(line[6].rstrip())
            Table.setItem(self.VP3i, 5, VY)
            VZ=QTableWidgetItem()
            VZ.setText(line[7].rstrip())
            Table.setItem(self.VP3i, 6, VZ)
            TT=QTableWidgetItem()
            TT.setText(line[8].rstrip())
            Table.setItem(self.VP3i, 7, TT)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.VP3i, 8, UC)
            self.VP3i=self.VP3i+1

        elif line[1]=="ETR": # détection d'un mot clé pour les propriété matériaux en rayonnement
            Table=self.syrthesIHMCollector.Material_radiation_properties_form.Mrp_table
            if self.chk==False:
                CKB=Table.cellWidget(self.MRPi, 0)
                CKB.setChecked(False)
                self.chk=True
            BA=QTableWidgetItem()
            BA.setText(line[2].rstrip())
            Table.setItem(self.MRPi, 1, BA)
            EM=QTableWidgetItem()
            EM.setText(line[3].rstrip())
            Table.setItem(self.MRPi, 2, EM)
            RE=QTableWidgetItem()
            a=line2.split(None, 6)
            a=a[6]
            RE.setText(a)
            Table.setItem(self.MRPi, 3, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.MRPi, 4, UC)
            self.MRPi=self.MRPi+1

        elif line[1]=='TEMPERATURE_INFINI': # détection d'un mot clé de type TEMPERATURE_INFINI
            self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rpa_le.setText(line[len(line)-1])

    def tke_VFM(self, parent=None): # Fonction des facteurs de forme sur fichier
        line=self.opstr.partition('= ')
        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
            cmb=self.syrthesIHMCollector.View_factor_3D_form.Vfm_3D_cmb
        else:
            cmb=self.syrthesIHMCollector.View_factor_2D_form.Vfm_2D_cmb
        if line[2]=='OUI':
            cmb.setCurrentIndex(1)
        else:
            cmb.setCurrentIndex(0)
            
    def tke_CCC(self, parent=None): # Fonction du commentaire du couplage conduction
        line=self.opstr.split(' ', 1)
        try :
            line=str(line[1])
            line=line.decode('Utf-8')
            if len(line)!=0:
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_te.append(line)
        except:
            self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_te.append("")

    def tke_CCR(self, parent=None): # Fonction du commentaire du couplage rayonnement
        line=self.opstr.split(' ', 1)
        try :
            line=str(line[1])
            line=line.decode('Utf-8')
            if len(line)!=0:
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_te.append(line)
        except:
            self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_te.append("")

    def tke_RBC(self, parent=None): # Fonction des condition aux limites en rayonnement
        line=self.opstr.split()
        line2=self.opstr
            
        if line[1]=='COUPLAGE_CONDUCTION': # détection d'un mot clé de type COUPLAGE_CONDUCTION
            a=line2.split(None, 2)
            a=a[2]
            a=a.decode('Utf-8')
            self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne.setText(a)

        elif line[1]=='TEMPERATURE_IMPOSEE': # détection d'un mot clé de type TEMPERATURE_IMPOSEE
            Table=self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irt_table
            if self.chk==False:
                CKB=Table.cellWidget(self.BITi, 0)
                CKB.setChecked(False)
                self.chk=True
            TE=QTableWidgetItem()
            TE.setText(line[2].rstrip())
            Table.setItem(self.BITi, 1, TE)
            RE=QTableWidgetItem()
            a=line2.split(None, 3)
            a=a[3]
            RE.setText(a)
            Table.setItem(self.BITi, 2, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.BITi, 3, UC)
            self.BITi=self.BITi+1

        elif line[1]=='FLUX_IMPOSE_PAR_BANDE': # détection d'un mot clé de type FLUX_IMPOSE_PAR_BANDE
            Table=self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irf_table
            if self.chk==False:
                CKB=Table.cellWidget(self.BIFi, 0)
                CKB.setChecked(False)
                self.chk=True
            BA=QTableWidgetItem()
            BA.setText(line[2].rstrip())
            Table.setItem(self.BIFi, 1, BA)
            FB=QTableWidgetItem()
            FB.setText(line[3].rstrip())
            Table.setItem(self.BIFi, 2, FB)
            RE=QTableWidgetItem()
            a=line2.split(None, 4)
            a=a[4]
            RE.setText(a)
            Table.setItem(self.BIFi, 3, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.BIFi, 4, UC)
            self.BIFi=self.BIFi+1

        elif line[1]=='HORIZON': # détection d'un mot clé de type HORIZON
            Table=self.syrthesIHMCollector.Solar_aspect_form.Sa_Hm_table
            if self.chk==False:
                CKB=Table.cellWidget(self.HMi, 0)
                CKB.setChecked(False)
                self.chk=True
            TE=QTableWidgetItem()
            TE.setText(line[2].rstrip())
            Table.setItem(self.HMi, 1, TE)
            EM=QTableWidgetItem()
            EM.setText(line[3].rstrip())
            Table.setItem(self.HMi, 2, EM)
            RE=QTableWidgetItem()
            a=line2.split(None, 4)
            a=a[4]
            RE.setText(a)
            Table.setItem(self.HMi, 3, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.HMi, 4, UC)
            self.HMi=self.HMi+1

        elif line[1]=='OMBRAGE': # détection d'un mot clé de type OMBRAGE
            Table=self.syrthesIHMCollector.Solar_aspect_form.Sa_Sm_table
            if self.chk==False:
                CKB=Table.cellWidget(self.SMi, 0)
                CKB.setChecked(False)
                self.chk=True
            SQ=QTableWidgetItem()
            SQ.setText(line[2].rstrip())
            Table.setItem(self.SMi, 1, SQ)
            RE=QTableWidgetItem()
            a=line2.split(None, 3)
            a=a[3]
            RE.setText(a)
            Table.setItem(self.SMi, 2, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.SMi, 3, UC)
            self.SMi=self.SMi+1

    def tke_RPA(self, parent=None): # Fonction du DOMAINE DE RAYONNEMENT CONFINE OUVERT SUR L EXTERIEUR
        line=self.opstr.split()
        if line[len(line)-1]=='OUI':
            self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rpa_chb.setChecked(True)
        else:
            self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rpa_chb.setChecked(False)

    def tke_SM(self, parent=None): # Fonction de la modélisation solaire
        line=self.opstr.split()
        line2=self.opstr
        if line[1]=='SOURCE_AZIMUTH_H': # détection d'un mot clé de type SOURCE_AZIMUTH_H
            self.syrthesIHMCollector.Solar_aspect_form.Sa_cmb.setCurrentIndex(0)
            self.syrthesIHMCollector.Solar_aspect_form.Csm_frame.show()
            self.syrthesIHMCollector.Solar_aspect_form.Asm_frame.hide()
            self.syrthesIHMCollector.Solar_aspect_form.spinBox.setValue(int(line[2]))
            self.syrthesIHMCollector.Solar_aspect_form.spinBox_2.setValue(int(line[3]))
        elif line[1]=='SOURCE_REPART_AUTO_SUR_LES_BANDES': # détection d'un mot clé de type SOURCE_REPART_AUTO_SUR_LES_BANDES
            if line[2]=='OUI':
                self.syrthesIHMCollector.Solar_aspect_form.Csm_cmb.setCurrentIndex(0)
                self.syrthesIHMCollector.Solar_aspect_form.Csm_At_frame.show()
                self.syrthesIHMCollector.Solar_aspect_form.Csm_Us_frame.hide()
                
            elif line[2]=='NON':
                self.syrthesIHMCollector.Solar_aspect_form.Csm_cmb.setCurrentIndex(1)
                self.syrthesIHMCollector.Solar_aspect_form.Csm_At_frame.hide()
                self.syrthesIHMCollector.Solar_aspect_form.Csm_Us_frame.show()

        elif line[1]=='SOURCE_FLUX_CONSTANT_PAR_BANDE' and self.syrthesIHMCollector.Solar_aspect_form.Csm_cmb.currentIndex()==1: # détection d'un mot clé de type SOURCE_FLUX_CONSTANT_PAR_BANDE
            
            Table=self.syrthesIHMCollector.Solar_aspect_form.Csm_Db_table
            if self.chk==False:
                CKB=Table.cellWidget(self.CSMi, 0)
                CKB.setChecked(False)
                self.chk=True
            BA=QTableWidgetItem()
            BA.setText(line[2].rstrip())
            Table.setItem(self.CSMi, 1, BA)
            FB=QTableWidgetItem()
            FB.setText(line[3].rstrip())
            Table.setItem(self.CSMi, 2, FB)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.CSMi, 3, UC)
            self.CSMi=self.CSMi+1

        elif line[1]=='SOURCE_FLUX_CONSTANT_PAR_BANDE' and self.syrthesIHMCollector.Solar_aspect_form.Csm_cmb.currentIndex()==0: # détection d'un mot clé de type SOURCE_FLUX_CONSTANT_PAR_BANDE
            if len(line) > 3 : # if user have filled the textbox "Flux"
                self.syrthesIHMCollector.Solar_aspect_form.lineEdit.setText(line[3])
        
        elif line[1]=='LATITUDE_DU_LIEU': # détection d'un mot clé de type LATITUDE_DU_LIEU
            self.syrthesIHMCollector.Solar_aspect_form.Sa_cmb.setCurrentIndex(1)
            self.syrthesIHMCollector.Solar_aspect_form.Csm_frame.hide()
            self.syrthesIHMCollector.Solar_aspect_form.Asm_frame.show()
            self.syrthesIHMCollector.Solar_aspect_form.spinBox_3.setValue(int(line[2]))
            self.syrthesIHMCollector.Solar_aspect_form.spinBox_4.setValue(int(line[3]))

        elif line[1]=='LONGITUDE_DU_LIEU': # détection d'un mot clé de type LONGITUDE_DU_LIEU
            self.syrthesIHMCollector.Solar_aspect_form.spinBox_5.setValue(int(line[2]))
            self.syrthesIHMCollector.Solar_aspect_form.spinBox_6.setValue(int(line[3]))
            
        elif line[1]=='MOIS_JOUR_HEURE_MINUTE': # détection d'un mot clé de type MOIS_JOUR_HEURE_MINUTE
            self.syrthesIHMCollector.Solar_aspect_form.Sa_cmb.setCurrentIndex(1)
            self.syrthesIHMCollector.Solar_aspect_form.Csm_frame.hide()
            self.syrthesIHMCollector.Solar_aspect_form.Asm_frame.show()
            Date=QDate()
            Date.setDate(2011,int(line[2]), int(line[3]))
            self.syrthesIHMCollector.Solar_aspect_form.dateTimeEdit.setDate(Date)
            Time=QTime()
            Time.setHMS(int(line[4]), int(line[5]),0)
            self.syrthesIHMCollector.Solar_aspect_form.dateTimeEdit.setTime(Time)    

        elif line[1]=='BILAN': # détection d'un mot clé de type BILAN
            Table=self.syrthesIHMCollector.Solar_aspect_form.Sa_Sht_table
            if self.chk==False:
                CKB=Table.cellWidget(self.SHBi, 0)
                CKB.setChecked(False)
                self.chk=True
            RE=QTableWidgetItem()
            a=line2.split(None, 2)
            a=a[2]
            RE.setText(a)
            Table.setItem(self.SHBi, 1, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            Table.setItem(self.SHBi, 2, UC)
            self.SHBi=self.SHBi+1

    def tke_PRS(self, parent=None): # Fonction de la prise en compte du rayonnement solaire
        line=self.opstr.split()
        if line[6]=='OUI':
            self.syrthesIHMCollector.Solar_aspect_form.Sa_chb.setChecked(True)
        else:
            self.syrthesIHMCollector.Solar_aspect_form.Sa_chb.setChecked(False)

    def tke_HIV(self, parent=None): # Fonction des conditions initiales en humidité
        line=self.opstr.split()
        line2=self.opstr

        if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==0:
            Table=self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.Vch_Ic_TPv_table
        else:
            Table=self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.Vch_Ic_TPvPt_table

        if self.chk==False:
            CKB=Table.cellWidget(self.HVIi, 0)
            CKB.setChecked(False)
            self.chk=True
        
        CBBT=Table.cellWidget(self.HVIi, 1)
        CBBT.setFocus()        
        if line[0]=='CINI_T_FCT=':
            CBBT.setCurrentIndex(1)
        elif line[0]=='CINI_T_PROG=':
            CBBT.setCurrentIndex(2)
            self.Table_prog2(Table, CBBT, self.HVIi)
        elif line[0]=='CINI_PV=': # analyse du type de donnée de la ligne (constant pression vapeur)
            CBBT.setCurrentIndex(0)
            CBBPV=Table.cellWidget(self.HVIi, 2)
            CBBPV.setCurrentIndex(1)
        elif line[0]=='CINI_PV_FCT=': # analyse du type de donnée de la ligne (fonction pression vapeur)
            CBBT.setCurrentIndex(1)
            CBBPV=Table.cellWidget(self.HVIi, 2)
            CBBPV.setCurrentIndex(1)
        elif line[0]=='CINI_PV_PROG=': # analyse du type de donnée de la ligne (sous-programme pression vapeur)
            CBBT.setCurrentIndex(2)
            self.Table_prog2(Table, CBBT, self.HVIi)
            CBBPV=Table.cellWidget(self.HVIi, 2)
            CBBPV.setCurrentIndex(1)
        elif line[0]=='CINI_PT=': # analyse du type de donnée de la ligne (constant pression total)
            CBBT.setCurrentIndex(0)
            CBBPT=Table.cellWidget(self.HVIi, 2)
            CBBPT.setCurrentIndex(2)
        elif line[0]=='CINI_PT_FCT=': # analyse du type de donnée de la ligne (fonction pression total)
            CBBT.setCurrentIndex(1)
            CBBPT=Table.cellWidget(self.HVIi, 2)
            CBBPT.setCurrentIndex(2)
        elif line[0]=='CINI_PT_PROG=': # analyse du type de donnée de la ligne (sous-programme pression total)
            CBBT.setCurrentIndex(2)
            self.Table_prog2(Table, CBBT, self.HVIi)
            CBBPT=Table.cellWidget(self.HVIi, 2)
            CBBPT.setCurrentIndex(2)
        
        if Table.cellWidget(self.HVIi, 1).currentIndex() != 2 : # if it's not PROG
            beginCol = 3
            for i in range(Table.columnCount())[beginCol:-1]: # loop through columns    
                cell = QTableWidgetItem()
                posLine = len(line) - (Table.columnCount() - i) + 1
                cell.setText((line[posLine].rstrip()).lstrip())
                Table.setItem(self.HVIi, i, cell)
        else: # PROG --> load only reference field
            cell = QTableWidgetItem()
            cell.setText((line[len(line)-1].rstrip()).lstrip())
            Table.setItem(self.HVIi, Table.columnCount()-2, cell)
         
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        Table.setItem(self.HVIi, 5, UC)
        self.HVIi=self.HVIi+1

    def tke_HSV(self, parent=None): # Fonction des conditions volumique en humidité
        line=self.opstr.split()
        line2=self.opstr
        if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==0:
            Table=self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.Vch_St_TPv_table
        else:
            Table=self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.Vch_St_TPvPt_table
        if self.chk==False:
            CKB=Table.cellWidget(self.HSTi, 0)
            CKB.setChecked(False)
            self.chk=True
        
        CBBPV=Table.cellWidget(self.HSTi, 1)
        CBBPV.setFocus()        
        if line[0]=='CVOL_T_FCT=':
            CBBPV.setCurrentIndex(1)
        elif line[0]=='CVOL_T_PROG=':
            CBBPV.setCurrentIndex(2)
            self.Table_prog2(Table, CBBPV, self.HSTi)
        elif line[0]=='CVOL_PV=': # analyse du type de donnée de la ligne (constant pression vapeur)
            CBBPV.setCurrentIndex(0)
            Table.cellWidget(self.HSTi, 2).setCurrentIndex(1)
        elif line[0]=='CVOL_PV_FCT=': # analyse du type de donnée de la ligne (fonction pression vapeur) 
            CBBPV.setCurrentIndex(1)
            Table.cellWidget(self.HSTi, 2).setCurrentIndex(1)
        elif line[0]=='CVOL_PV_PROG=': # analyse du type de donnée de la ligne (sous-programme pression vapeur)
            CBBPV.setCurrentIndex(2)
            self.Table_prog2(Table, CBBPV, self.HSTi)
            Table.cellWidget(self.HSTi, 2).setCurrentIndex(1)
        elif line[0]=='CVOL_PT=': # analyse du type de donnée de la ligne (constant pression total)
            CBBPV.setCurrentIndex(0)
            Table.cellWidget(self.HSTi, 2).setCurrentIndex(2)
        elif line[0]=='CVOL_PT_FCT=': # analyse du type de donnée de la ligne (fonction pression total)
            CBBPV.setCurrentIndex(1)
            Table.cellWidget(self.HSTi, 2).setCurrentIndex(2)
        elif line[0]=='CVOL_PT_PROG=': # analyse du type de donnée de la ligne (sous-programme pression total)
            CBBPV.setCurrentIndex(2)
            self.Table_prog2(Table, CBBPV, self.HSTi)
            Table.cellWidget(self.HSTi, 2).setCurrentIndex(2)
        
        
        if Table.cellWidget(self.HSTi, 1).currentIndex() != 2 : # if it's not PROG
            beginCol = 3
            a=line2.split(None, 3) # a = ['CVOL_T', '1', '2', '3 4 5']
            for i in range(Table.columnCount())[beginCol:-1]: # loop through columns    
                cell = QTableWidgetItem()
                posLine = len(a) - (Table.columnCount() - i) + 1
                cell.setText((a[posLine].rstrip()).lstrip())
                Table.setItem(self.HSTi, i, cell)
        else: # PROG --> load only reference field
            a=line2.split(None, 1) # a = ['CVOL_T', '3 4 5']
            cell = QTableWidgetItem()
            cell.setText((a[len(a)-1].rstrip()).lstrip())
            Table.setItem(self.HSTi, Table.columnCount()-2, cell)
         
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        Table.setItem(self.HSTi, 6, UC)
        self.HSTi=self.HSTi+1       
        
#        VA=QTableWidgetItem()
#        VA.setText(line[1])
#        Table.setItem(self.HSTi, 3, VA)
#        RE=QTableWidgetItem()
#        a=line2.split(None, 2)
#        a=a[2]
#        RE.setText(a)
#        Table.setItem(self.HSTi, 4, RE)
#        UC=QTableWidgetItem()
#        UC.setText(self.com)
#        self.com=''
#        Table.setItem(self.HSTi, 5, UC)
#        self.HSTi=self.HSTi+1

    def tke_MATISO(self, parent=None): # Fonction des propriété materiaux en humidité
        line=self.opstr.split()
        line2=self.opstr

        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
            Table=self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_iso_3D_table
        else:
            Table=self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_iso_2D_table

        if self.chk==False:
            CKB=Table.cellWidget(self.HMTi, 0)
            CKB.setChecked(False)
            self.chk=True
        CBB=Table.cellWidget(self.HMTi, 1)
        CBB.setCurrentIndex(CBB.findText(line[1]))
        RE=QTableWidgetItem()
        a=line2.split(None, 2)
        a=a[2]
        RE.setText(a)
        Table.setItem(self.HMTi, 2, RE)
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        Table.setItem(self.HMTi, 3, UC)
        self.HMTi=self.HMTi+1

    def tke_MATANISO(self, parent=None): # Fonction des propriété materiaux en humidité
        line=self.opstr.split()
        line2=self.opstr
        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
            Table=self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_aniso_3D_table
            nv = 9 # coordonnees des 3 vecteurs de la nouvelles base
        else:
            Table=self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_aniso_2D_table
            nv = 1 # angle de rotation

        if self.chk==False:
            CKB=Table.cellWidget(self.HMTa, 0)
            CKB.setChecked(False)
            self.chk=True
        CBB=Table.cellWidget(self.HMTa, 1)
        CBB.setCurrentIndex(CBB.findText(line[1]))

        for j in range(nv):
            AV=QTableWidgetItem()
            a = line2.split(None)
            AV.setText(a[2+j])
            Table.setItem(self.HMTa, 2+j,AV)

        RE=QTableWidgetItem()
        a=line2.split(None)
        a=a[2+nv]
        RE.setText(a)
        Table.setItem(self.HMTa, 2+nv, RE)

        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        Table.setItem(self.HMTa, 3+nv, UC)
        self.HMTa=self.HMTa+1


    def tke_HBC(self, parent=None): # Fonction des conditions aux limites en humidité
        line=self.opstr.split()
        line2=self.opstr
        nbValue = 2
        if line[1]=='HH': # Détection du modèle d'humidité à 2 équations
            Table=self.syrthesIHMCollector.Boundary_conditions_TPv_form.Bc_TPv_table
        elif line[1]=='HHH': # Détection du modèle d'humidité à 3 équations
            Table=self.syrthesIHMCollector.Boundary_conditions_TPvPt_form.Bc_TPvPt_table

        elif line[1] == "RES_CONTACT":
            if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==0:
                Table=self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.Cont_res_hum_TPv_table
                nbValue = 2
            else:
                Table=self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.Cont_res_hum_TPvPt_table
                nbValue = 3
            pass
            
        if line[1] == "RES_CONTACT":
            keyWords = ['CLIM_HMT=', 'CLIM_HMT_FCT=', 'CLIM_HMT_PROG=']
            self.openTable2(line, line2, Table, keyWords, self.HCRi, nbValue)
            self.HCRi = self.HCRi+1
            pass
        else:
            keyWords = ['CLIM_HMT=', 'CLIM_HMT_FCT=', 'CLIM_HMT_PROG=']
            self.openTable1(line, Table, keyWords, self.HBCi)
            self.HBCi=self.HBCi+1
            pass
#        line=self.opstr.split()
#        line2=self.opstr
#        if line[1]=='HH': # Détection du modèle d'humidité à 2 équations
#            Table=self.syrthesIHMCollector.Boundary_conditions_TPv_form.Bc_TPv_table
#        elif line[1]=='HHH': # Détection du modèle d'humidité à 3 équations
#            Table=self.syrthesIHMCollector.Boundary_conditions_TPvPt_form.Bc_TPvPt_table
#        if self.chk==False:
#            CKB=Table.cellWidget(self.HBCi, 0)
#            CKB.setChecked(False)
#            self.chk=True
#        CBB=Table.cellWidget(self.HBCi, 1)
#        if line[0]=='CLIM_HMT=': # analyse du type de donnée de la ligne (constant)
#            CBB.setCurrentIndex(0)
#        elif line[0]=='CLIM_HMT_FCT=': # analyse du type de donnée de la ligne (fonction)
#            CBB.setCurrentIndex(1)
#        elif line[0]=='CLIM_HMT_PROG=': # analyse du type de donnée de la ligne (sous-programme)
#            CBB.setCurrentIndex(2)
#        
#        T1=QTableWidgetItem()
#        T1.setText(line[2])
#        Table.setItem(self.HBCi, 2, T1)
#        T2=QTableWidgetItem()
#        T2.setText(line[3])
#        Table.setItem(self.HBCi, 3, T2)
#        T3=QTableWidgetItem()
#        T3.setText(line[4])
#        Table.setItem(self.HBCi, 4, T3)
#        T4=QTableWidgetItem()
#        T4.setText(line[5])
#        Table.setItem(self.HBCi, 5, T4)
#        T5=QTableWidgetItem()
#        T5.setText(line[6])
#        Table.setItem(self.HBCi, 5, T5)
#        T6=QTableWidgetItem()
#        T6.setText(line[7])
#        Table.setItem(self.HBCi, 6, T6)
#        if line[1]=='HH': # Détection du modèle d'humidité à 2 équations
#            UC=QTableWidgetItem()
#            UC.setText(self.com)
#            self.com=''
#            Table.setItem(self.HBCi, 7, UC)
#            self.HBCi=self.HBCi+1   
#        elif line[1]=='HHH': # Détection du modèle d'humidité à 3 équations
#            T7=QTableWidgetItem()
#            T7.setText(line[8])
#            Table.setItem(self.HBCi, 7, T7)
#            T8=QTableWidgetItem()
#            T8.setText(line[9])
#            Table.setItem(self.HBCi, 8, T8)
#            UC=QTableWidgetItem()
#            UC.setText(self.com)
#            self.com=''
#            Table.setItem(self.HBCi, 9, UC)
#            self.HBCi=self.HBCi+1

    # fluid1d

    def tke_PTF1D(self, parent=None): # Fonction du pas de temps fluide 1D
        line=self.opstr.partition('= ')
        line=line[2].rstrip()
        if line==None: pass
        self.syrthesIHMCollector.Control_fluid1d_form.Le_const_Ts_fluid1d.setText(line)

    def tke_CCFL1D(self, parent=None): # Fonction du commentaire du couplage conduction
        line=self.opstr.split(' ', 1)
        try :
            line=str(line[1])
            line=line.decode('Utf-8')
            if len(line)!=0:
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1dfUc_lne.setText(line)
        except:
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1dfUc_lne.setText("")
            

    def tke_GM1D(self, parent=None): # Fonction de geometrie pour fluid1d
        line=self.opstr.split()
        line2=self.opstr
        if (self.GMi==self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table.rowCount()):
            self.Open_New_Line(self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table, self.GMi)
            
        if self.chk==False:
            CKB=self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table.cellWidget(self.GMi, 0)
            CKB.setChecked(False)
            self.chk=True
        DH=QTableWidgetItem()
        DH.setText(line[1])
        self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table.setItem(self.GMi, 1, DH)
        SN=QTableWidgetItem()
        SN.setText(line[2])
        self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table.setItem(self.GMi, 2, SN)
        RO=QTableWidgetItem()
        RO.setText(line[3])
        self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table.setItem(self.GMi, 3, RO)
        a=line2.split(None, 4)
        a=a[4]
        RE=QTableWidgetItem()
        RE.setText(a)
        self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table.setItem(self.GMi, 4, RE)
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table.setItem(self.GMi, 5, UC)
        self.GMi=self.GMi+1
        
    def tke_GM0D(self, parent=None): # Fonction de geometrie pour fluid0d
        line=self.opstr.split()
        line2=self.opstr
        if (self.GM0i==self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table.rowCount()):
            self.Open_New_Line(self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table, self.GM0i)
            
        if self.chk==False:
            CKB=self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table.cellWidget(self.GM0i, 0)
            CKB.setChecked(False)
            self.chk=True
        NU=QTableWidgetItem()
        NU.setText(line[1])
        self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table.setItem(self.GM0i, 1, NU)
        VO=QTableWidgetItem()
        VO.setText(line[2])
        self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table.setItem(self.GM0i, 2, VO)
        a=line2.split(None, 3)
        a=a[3]
        RE=QTableWidgetItem()
        RE.setText(a)
        self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table.setItem(self.GM0i, 3, RE)
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table.setItem(self.GM0i, 4, UC)
        self.GM0i=self.GM0i+1

    def tke_CI1D(self, parent=None): # Fonction des conditions initiales pour fluid1d
        line=self.opstr.split()
        line2=self.opstr
        if (self.TVi==self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table.rowCount()):
            self.Open_New_Line(self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table, self.TVi)
            
        if self.chk==False:
            CKB=self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table.cellWidget(self.TVi, 0)
            CKB.setChecked(False)
            self.chk=True

        CBB=self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table.cellWidget(self.TVi, 1)
        CBB.setFocus()
        if line[0]=='CINI_FL1D_T=': # analyse du type de donnée de la ligne (constant)
            CBB.setCurrentIndex(0)
        elif line[0]=='CINI_FL1D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
            CBB.setCurrentIndex(1)
        elif line[0]=='CINI_FL1D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
            CBB.setCurrentIndex(2)
            self.Table_prog2(self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table, CBB, self.TVi)
        TT=QTableWidgetItem()
        if line[0]!='CINI_FL1D_T_PROG=':
            TT.setText(line[1])
            self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table.setItem(self.TVi, 2, TT)
            VV=QTableWidgetItem()
            VV.setText(line[2])
            self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table.setItem(self.TVi, 3, VV)
            a=line2.split(None, 3)
            a=a[3]
        else :
            a=line2.split(None, 1)
            a=a[1]
        RE=QTableWidgetItem()
        RE.setText(a)
        self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table.setItem(self.TVi, 4, RE)
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table.setItem(self.TVi, 5, UC)
        self.TVi=self.TVi+1

    def tke_CL1D(self, parent=None): # fonction des condition aux limites pour fluid1d
        line=self.opstr.split()
        line2=self.opstr

        if line[1]=='COUPLAGE_CONDUCTION': # détection d'un mot clé de type COUPLAGE_CONDUCTION 
            a=line2.split(None, 2)
            a=a[2]
            a=a.decode('Utf-8')
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne.setText(a)

        elif line[1]=='ECHANGE': # détection d'un mot clé de type coefficient d'echange
            if (self.HE1Di==self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table, self.HE1Di)
                    
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table.cellWidget(self.HE1Di, 0)
                CKB.setChecked(False)
                self.chk=True
                
            CBB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table.cellWidget(self.HE1Di, 1)

            
            CBB.setFocus()
 
            if line[0]=='CLIM_FL1D_T=': # analyse du type de donnée de la ligne (constant) 
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_FL1D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setFocus()
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_FL1D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme) 
                CBB.setCurrentIndex(2)
                self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table, CBB, self.HE1Di)
            CH=QTableWidgetItem()
            if line[1]==None: pass
            if line[0] != 'CLIM_FL1D_T_PROG=':
                CH.setText(line[2].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table.setItem(self.HE1Di, 2, CH)
                ET=QTableWidgetItem()
                ET.setText(line[3].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table.setItem(self.HE1Di, 3, ET)
                a=line2.split(None, 4) # ref field
                a=a[4]
            else:
                a=line2.split(None, 2) # ref field
                a=a[2]
            RE=QTableWidgetItem()
            RE.setText(a)
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table.setItem(self.HE1Di, 4, RE) # ref field
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table.setItem(self.HE1Di, 5, UC)
            self.HE1Di=self.HE1Di+1

        elif line[1]=='FLUX_IMPOSE': # détection d'un mot clé de type flux impose
            if (self.FX1Di==self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table, self.FX1Di)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table.cellWidget(self.FX1Di, 0)
                CKB.setChecked(False)
                self.chk=True

            CBB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table.cellWidget(self.FX1Di, 1)


            CBB.setFocus()

            if line[0]=='CLIM_FL1D_T=': # analyse du type de donnée de la ligne (constant) 
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_FL1D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_FL1D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBB.setCurrentIndex(2)        
            FX=QTableWidgetItem()
            FX.setText(line[2].rstrip())
            if line[0]=='CLIM_FL1D_T_PROG=': FX.setText('')
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table.setItem(self.FX1Di, 2, FX)
            if line[0]!='CLIM_FL1D_T_PROG=': 
                a=line2.split(None, 3)
                a=a[3] 
            else:
                a=line2.split(None, 2)
                a=a[2]
                self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table, CBB, self.FX1Di)            

            RE=QTableWidgetItem()
            RE.setText(a)
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table.setItem(self.FX1Di, 3, RE)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table.setItem(self.FX1Di, 4, UC)
            self.FX1Di=self.FX1Di+1

        elif line[1]=='ENTREE_3D': # détection d'un mot clé de type inlet 3D
            if (self.IN3D1Di==self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table, self.IN3D1Di)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table.cellWidget(self.IN3D1Di, 0)
                CKB.setChecked(False)
                self.chk=True

            CBB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table.cellWidget(self.IN3D1Di, 1)
            CBB.setFocus()
            if line[0]=='CLIM_FL1D_T=':# analyse du type de donnée de la ligne (constant)  
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_FL1D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_FL1D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBB.setCurrentIndex(2)
                self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table, CBB, self.IN3D1Di)
            PX=QTableWidgetItem()
            PX.setText(line[2].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table.setItem(self.IN3D1Di, 2, PX)
            PY=QTableWidgetItem()
            PY.setText(line[3].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table.setItem(self.IN3D1Di, 3, PY)
            PZ=QTableWidgetItem()
            PZ.setText(line[4].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table.setItem(self.IN3D1Di, 4, PZ)
            if line[0]!='CLIM_FL1D_T_PROG=' :
                QQ=QTableWidgetItem()
                QQ.setText(line[5].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table.setItem(self.IN3D1Di, 5, QQ)
                TT=QTableWidgetItem()
                TT.setText(line[6].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table.setItem(self.IN3D1Di, 6, TT)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table.setItem(self.IN3D1Di, 7, UC)
            self.IN3D1Di=self.IN3D1Di+1
            
        elif line[1]=='LOOP': # détection d'un mot clé de type closed loop
            if (self.CLi==self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl, self.CLi)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.cellWidget(self.CLi, 0)
                CKB.setChecked(False)
                self.chk=True

            CBB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.cellWidget(self.CLi, 1)



            CBB.setFocus()

            if line[0]=='CLIM_FL1D_T=': # analyse du type de donnée de la ligne (constant) 
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_FL1D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_FL1D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBB.setCurrentIndex(2)        
            FX=QTableWidgetItem()
            if line[0]=='CLIM_FL1D_T_PROG=': FX.setText('')
            else :FX.setText(line[2].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.setItem(self.CLi, 2, FX)
            self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl, CBB, self.CLi)

            P0=QTableWidgetItem()
            P0.setText(line[2].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.setItem(self.CLi, 2, P0)
            P1=QTableWidgetItem()
            P1.setText(line[3].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.setItem(self.CLi, 3, P1)
            P2=QTableWidgetItem()
            P2.setText(line[4].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.setItem(self.CLi, 4, P2)
            if line[0]!='CLIM_FL1D_T_PROG=' :
                P3=QTableWidgetItem()
                P3.setText(line[5].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.setItem(self.CLi, 5, P3)
                P4=QTableWidgetItem()
                P4.setText(line[6].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.setItem(self.CLi, 6, P4)
                P5=QTableWidgetItem()
                P5.setText(line[7].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.setItem(self.CLi, 7, P5)
                P6=QTableWidgetItem()
                P6.setText(line[8].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.setItem(self.CLi, 8, P6)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl.setItem(self.CLi, 9, UC)
            self.CLi=self.CLi+1
            
        elif line[1]=='THERMAL_LOOP': # détection d'un mot clé de type closed loop
            if (self.THCLi==self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl, self.THCLi)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl.cellWidget(self.THCLi, 0)
                CKB.setChecked(False)
                self.chk=True

            CBB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl.cellWidget(self.THCLi, 1)


            CBB.setFocus()

            if line[0]=='CLIM_FL1D_T=': # analyse du type de donnée de la ligne (constant) 
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_FL1D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_FL1D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBB.setCurrentIndex(2)        
            FX=QTableWidgetItem()
            if line[0]=='CLIM_FL1D_T_PROG=': FX.setText('')
            else : FX.setText(line[2].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl.setItem(self.THCLi, 2, FX)
            P0=QTableWidgetItem()
            P0.setText(line[2].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl.setItem(self.THCLi, 2, P0)
            P1=QTableWidgetItem()
            P1.setText(line[3].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl.setItem(self.THCLi, 3, P1)
            P2=QTableWidgetItem()
            P2.setText(line[4].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl.setItem(self.THCLi, 4, P2)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl.setItem(self.THCLi, 5, UC)
            self.THCLi=self.THCLi+1
            
        elif line[1]=='DELTA_P': # détection d'un mot clé de type delta pressure
            if (self.DPCLi==self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP.rowCount()):
                self.Open_New_Line(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP, self.DPCLi)
                
            if self.chk==False:
                CKB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP.cellWidget(self.DPCLi, 0)
                CKB.setChecked(False)
                self.chk=True

            CBB=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP.cellWidget(self.DPCLi, 1)


            CBB.setFocus()

            if line[0]=='CLIM_FL1D_T=': # analyse du type de donnée de la ligne (constant) 
                CBB.setCurrentIndex(0)
            elif line[0]=='CLIM_FL1D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
                CBB.setCurrentIndex(1)
            elif line[0]=='CLIM_FL1D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
                CBB.setCurrentIndex(2)        
            FX=QTableWidgetItem()
            if line[0]=='CLIM_FL1D_T_PROG=': FX.setText('')
            else : FX.setText(line[2].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP.setItem(self.DPCLi, 2, FX)
            self.Table_prog2(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP, CBB, self.DPCLi)

            P0=QTableWidgetItem()
            P0.setText(line[2].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP.setItem(self.DPCLi, 2, P0)
            P1=QTableWidgetItem()
            P1.setText(line[3].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP.setItem(self.DPCLi, 3, P1)
            P2=QTableWidgetItem()
            P2.setText(line[4].rstrip())
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP.setItem(self.DPCLi, 4, P2)
            if line[0]!='CLIM_FL1D_T_PROG=' :
                P3=QTableWidgetItem()
                P3.setText(line[5].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP.setItem(self.DPCLi, 5, P3)
                P4=QTableWidgetItem()
                P4.setText(line[6].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP.setItem(self.DPCLi, 6, P4)
                P5=QTableWidgetItem()
                P5.setText(line[7].rstrip())
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP.setItem(self.DPCLi, 7, P5)
            UC=QTableWidgetItem()
            UC.setText(self.com)
            self.com=''
            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP.setItem(self.DPCLi, 8, UC)
            self.DPCLi=self.DPCLi+1

    def tke_GR1D(self, parent=None): # Fonction de la gravite pour fluid1d
        line=self.opstr.partition('= ')
        G=line[2].split()
        if len(G) >= 3:
           GX=G[0].rstrip()
           self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_x.setText(GX)
           GY=G[1].rstrip()
           self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_y.setText(GY)
           GZ=G[2].rstrip()
           self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_z.setText(GZ)

    def tke_PHY1D(self, parent=None): # Fonction des proprietes physiques pour fluid1d
        line=self.opstr.split()
        line2=self.opstr
        if (self.PHYi==self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table.rowCount()):
            self.Open_New_Line(self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table, self.PHYi)
            
        if self.chk==False:
            CKB=self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table.cellWidget(self.PHYi, 0)
            CKB.setChecked(False)
            self.chk=True

        CBB=self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table.cellWidget(self.PHYi, 1)
        CBB.setFocus()
        if line[0]=='CPHY_FL1D_T=': # analyse du type de donnée de la ligne (constant)
            CBB.setCurrentIndex(0)
        elif line[0]=='CPHY_FL1D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
            CBB.setCurrentIndex(1)
        elif line[0]=='CPHY_FL1D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
            CBB.setCurrentIndex(2)
            self.Table_prog2(self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table, CBB, self.PHYi)
        RO=QTableWidgetItem()
        if line[0]!='CPHY_FL1D_T_PROG=' :
            RO.setText(line[1])
            self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table.setItem(self.PHYi, 2, RO)
            CP=QTableWidgetItem()
            CP.setText(line[2])
            self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table.setItem(self.PHYi, 3, CP)
            KK=QTableWidgetItem()
            KK.setText(line[3])
            self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table.setItem(self.PHYi, 4, KK)
            MU=QTableWidgetItem()
            MU.setText(line[4])
            self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table.setItem(self.PHYi, 5, MU)
            a=line2.split(None, 5)
            a=a[5]
        else : 
            a=line2.split(None, 1)
            a=a[1]
        RE=QTableWidgetItem()
        RE.setText(a)
        self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table.setItem(self.PHYi, 6, RE)
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table.setItem(self.PHYi, 7, UC)
        self.PHYi=self.PHYi+1

    def tke_SOU1D(self, parent=None): # Fonction des termes sources pour fluid1d
        line=self.opstr.split()
        line2=self.opstr
        if (self.SOUi==self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table.rowCount()):
            self.Open_New_Line(self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table, self.SOUi)
            
        if self.chk==False:
            CKB=self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table.cellWidget(self.SOUi, 0)
            CKB.setChecked(False)
            self.chk=True

        CBB=self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table.cellWidget(self.SOUi, 1)
        CBB.setFocus()
        if line[0]=='CVOL_FL1D_T=': # analyse du type de donnée de la ligne (constant)
            CBB.setCurrentIndex(0)
        elif line[0]=='CVOL_FL1D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
            CBB.setCurrentIndex(1)
        elif line[0]=='CVOL_FL1D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
            CBB.setCurrentIndex(2)
            self.Table_prog2(self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table, CBB, self.SOUi)
        AA=QTableWidgetItem()
        if line[0]!='CVOL_FL1D_T_PROG=': 
            AA.setText(line[1])
            self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table.setItem(self.SOUi, 2, AA)
            BB=QTableWidgetItem()
            BB.setText(line[2])
            self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table.setItem(self.SOUi, 3, BB)
            a=line2.split(None, 3)
            a=a[3]
        else :
            a=line2.split(None, 1)
            a=a[1]
        RE=QTableWidgetItem()
        RE.setText(a)
        self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table.setItem(self.SOUi, 4, RE)
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table.setItem(self.SOUi, 5, UC)
        self.SOUi=self.SOUi+1

    def tke_PDCL1D(self, parent=None): # Fonction des pertes de charge lineaires pour fluid1d
        line=self.opstr.split()
        line2=self.opstr
        if (self.SOUi==self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table.rowCount()):
            self.Open_New_Line(self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table, self.PDCi)
            
        if self.chk==False:
            CKB=self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table.cellWidget(self.PDCi, 0)
            CKB.setChecked(False)
            self.chk=True

        CBB=self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table.cellWidget(self.PDCi, 1)
        CBB.setFocus()
        if line[0]=='PDC_LIN_FL1D_T=': # analyse du type de donnée de la ligne (constant)
            CBB.setCurrentIndex(0)
        elif line[0]=='PDC_LIN_FL1D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
            CBB.setCurrentIndex(1)
        elif line[0]=='PDC_LIN_FL1D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
            CBB.setCurrentIndex(2)
            self.Table_prog2(self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table, CBB, self.PDCi)
        KK=QTableWidgetItem()
        if line[0]!='PDC_LIN_FL1D_T_PROG=': 
            KK.setText(line[1])
            self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table.setItem(self.PDCi, 2, KK)
            a=line2.split(None, 2)
            a=a[2]
        else :
            a=line2.split(None, 1)
            a=a[1]
        RE=QTableWidgetItem()
        RE.setText(a)
        self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table.setItem(self.PDCi, 3, RE)
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table.setItem(self.PDCi, 4, UC)
        self.PDCi=self.PDCi+1
        
    def tke_PDCS1D(self, parent=None): # Fonction des pertes de charge pour fluid1d singuliere
        line=self.opstr.split()
        line2=self.opstr
        if (self.SOUi==self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table.rowCount()):
            self.Open_New_Line(self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table, self.PDCSi)
            
        if self.chk==False:
            CKB=self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table.cellWidget(self.PDCSi, 0)
            CKB.setChecked(False)
            self.chk=True

        CBB=self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table.cellWidget(self.PDCSi, 1)
        CBB.setFocus()
        if line[0]=='PDC_SING_FL1D_T=': # analyse du type de donnée de la ligne (constant)
            CBB.setCurrentIndex(0)
        elif line[0]=='PDC_SING_FL1D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
            CBB.setCurrentIndex(1)
        elif line[0]=='PDC_SING_FL1D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme)
            CBB.setCurrentIndex(2)
            self.Table_prog2(self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table, CBB, self.PDCSi)
        KK=QTableWidgetItem()
        if line[0]!='PDC_SING_FL1D_T_PROG=': 
            KK.setText(line[1])
            self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table.setItem(self.PDCSi, 2, KK)
            a=line2.split(None, 2)
            a=a[2]
        else :
            a=line2.split(None, 1)
            a=a[1]
        RE=QTableWidgetItem()
        RE.setText(a)
        self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table.setItem(self.PDCSi, 3, RE)
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table.setItem(self.PDCSi, 4, UC)
        self.PDCSi=self.PDCSi+1




    def tke_RO(self, parent=None): # RO pour Running Options. fonction de la lecture des mots clés uniquement compréhensible par l'IHM
        line=self.opstr.split(None, 2)
        if line[1] == 'NBPROC_COND=':
            self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd.setValue(int(line[2]))
        elif line[1] == 'NBPROC_RAD=':
            self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_ry.setValue(int(line[2]))
        elif line[1] == 'LISTING=':
            self.syrthesIHMCollector.Running_options_form.Ro_Ln_le.setText(line[2])
        elif line[1] == 'DOMAIN_POS=':
            self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb.setCurrentIndex(int(line[2]))
        elif line[1] == 'C_RESULT=':
            self.syrthesIHMCollector.Running_options_form.Ro_Cr_cb.setCurrentIndex(int(line[2]))
        elif line[1] == 'IHM_ADVANCED':
            self.action_Advanced_mode.setChecked(True)
    
    def tke_Advanced_mode(self, parent=None): # Advanced mode, all strange keyword will be considered as keyword only comprehensible by SYRTHES
        # line=self.opstr.split()
        line2=self.opstr
        Table=self.syrthesIHMCollector.Advanced_mode_form.Advanced_cmd_table
       
        if self.chk==False:
            CKB=Table.cellWidget(self.AdvModei, 0)
            CKB.setChecked(False)
            self.chk=True

        # advanced command
        cell = QTableWidgetItem()
        cell.setText((line2.rstrip()).lstrip())
        Table.setItem(self.AdvModei, Table.columnCount()-2, cell)
        
        # comments 
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        Table.setItem(self.AdvModei, Table.columnCount()-1, UC)
        
        # next row
        self.AdvModei += 1
      

    def tke_CPHY0D(self, parent=None):
        line=self.opstr.split()
        line2=self.opstr

        if (self.CP0Di==self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table.rowCount()):
            self.Open_New_Line(self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table, self.CP0Di)
                
        if self.chk==False:
            CKB=self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table.cellWidget(self.CP0Di, 0)
            CKB.setChecked(False)
            self.chk=True
                
        CBB=self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table.cellWidget(self.CP0Di, 1)
        CBB.setFocus()
        if line[0]=='CPHY_FL0D_T=': # analyse du type de donnée de la ligne (constant) 
            CBB.setCurrentIndex(0)
        elif line[0]=='CPHY_FL0D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
            CBB.setFocus()
            CBB.setCurrentIndex(1)
        elif line[0]=='CPHY_FL0D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme) 
            CBB.setCurrentIndex(2)
            self.Table_prog2(self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table, CBB, self.CP0Di)
        if line[1]==None: pass
        NB=QTableWidgetItem()
        NB.setText(line[1].rstrip())
        self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table.setItem(self.CP0Di, 2, NB)
        CV=QTableWidgetItem()
        if line[0] != 'CPHY_FL0D_T_PROG=':
            CV.setText(line[2].rstrip())
            self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table.setItem(self.CP0Di, 3, CV)
            CR=QTableWidgetItem()
            CR.setText(line[3].rstrip())
            self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table.setItem(self.CP0Di, 4, CR)
            CC=QTableWidgetItem()
            CC.setText(line[4].rstrip())
            self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table.setItem(self.CP0Di, 5, CC)

        
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table.setItem(self.CP0Di, 6, UC)
        self.CP0Di=self.CP0Di+1
        pass

    def tke_CVOL0D(self, parent=None):
        line=self.opstr.split()
        line2=self.opstr

        if (self.CV0Di==self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table.rowCount()):
            self.Open_New_Line(self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table, self.CV0Di)
                
        if self.chk==False:
            CKB=self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table.cellWidget(self.CV0Di, 0)
            CKB.setChecked(False)
            self.chk=True
                
        CBB=self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table.cellWidget(self.CV0Di, 1)
        CBB.setFocus()
        if line[0]=='CVOL_FL0D_T=': # analyse du type de donnée de la ligne (constant) 
            CBB.setCurrentIndex(0)
        elif line[0]=='CVOL_FL0D_T_FCT=': # analyse du type de donnée de la ligne (fonction)
            CBB.setFocus()
            CBB.setCurrentIndex(1)
        elif line[0]=='CVOL_FL0D_T_PROG=': # analyse du type de donnée de la ligne (sous-programme) 
            CBB.setCurrentIndex(2)
            self.Table_prog2(self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table, CBB, self.CV0Di)
        if line[1]==None: pass
        NB=QTableWidgetItem()
        NB.setText(line[1].rstrip())
        self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table.setItem(self.CV0Di, 2, NB)
        CA=QTableWidgetItem()
        if line[0] != 'CVOL_FL0D_T_PROG=':
            CA.setText(line[2].rstrip())
            self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table.setItem(self.CV0Di, 3, CA)
            CB=QTableWidgetItem()
            CB.setText(line[3].rstrip())
            self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table.setItem(self.CV0Di, 4, CB)
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table.setItem(self.CV0Di, 5, UC)
        self.CV0Di=self.CV0Di+1
        pass
    
    def Open_New_Line(self, table, iterator): # Fonction permettant d'ajouter une ligne dans un tableau si par défaut il y en a moin que le nombre de ligne décrite dans le fichier de
        # donnée
        i=0
        Strliste=QStringList()
        table.insertRow(iterator)
        
        while i<=iterator:
            Strliste.append("")
            i=i+1            
        table.setVerticalHeaderLabels(Strliste)
        
        if self.dic_Table_type[table] == 1 :
            self.checkbox=QtGui.QCheckBox()
            table.setCellWidget(iterator,0,self.checkbox)
            self.checkbox.setChecked(True)
            self.combobox=QtGui.QComboBox()
            self.combobox.addItem(QtCore.QString())
            self.combobox.addItem(QtCore.QString())
            self.combobox.addItem(QtCore.QString())
            self.combobox.setItemText(0, QtGui.QApplication.translate("MainWindow", "Constant", None, QtGui.QApplication.UnicodeUTF8))
            self.combobox.setItemText(1, QtGui.QApplication.translate("MainWindow", "Function", None, QtGui.QApplication.UnicodeUTF8))
            self.combobox.setItemText(2, QtGui.QApplication.translate("MainWindow", "Program", None, QtGui.QApplication.UnicodeUTF8))
            table.setCellWidget(iterator,1,self.combobox)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            
        elif self.dic_Table_type[table] == 2:
            self.checkbox=QtGui.QCheckBox()
            table.setCellWidget(iterator,0,self.checkbox)
            self.checkbox.setChecked(True)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
    
    def openTable1(self, line, table, keyWords, cr):
        # fill tables of type 1 (cf. dictionary)
        # table : table to be filled
        # keyWords : keyWords of the table
        # cr :current row
        
        if cr == table.rowCount():
            self.Open_New_Line(table, cr)
        
        if self.chk == False: # load state of checkbox
            CKB = table.cellWidget(cr, 0)
            CKB.setChecked(False)
            self.chk = True
            
        CBB = table.cellWidget(cr, 1)
        for i in range(3): # load state of combobox
            if line[0] == keyWords[i] : CBB.setCurrentIndex(i)
        
        if line[0] != keyWords[2]: # CONT or FUNC --> load all data
            beginCol = 2
            for i in range(table.columnCount())[beginCol:-1]: # loop through columns    
                cell = QTableWidgetItem()
                cell.setText((line[i].rstrip()).lstrip())
                table.setItem(cr, i, cell)
        else : # PROG --> load only reference field
            self.Table_prog2(table, CBB, cr)
            cell = QTableWidgetItem()
            cell.setText((line[len(line)-1].rstrip()).lstrip())
            table.setItem(cr, table.columnCount()-2, cell)
        
        # user comment field
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        table.setItem(cr, table.columnCount()-1, UC)
        

    def openTable2(self, line, line2, table, keyWords, cr, nbValue):

        if (cr==table.rowCount()):
            self.Open_New_Line(table, cr)
            pass
        if self.chk==False:
            CKB=table.cellWidget(cr, 0)
            CKB.setChecked(False)
            self.chk=True
            pass
        CBB=table.cellWidget(cr, 1)
        CBB.setFocus()

        for i in range(3): # load state of combobox
            if line[0] == keyWords[i] : CBB.setCurrentIndex(i)
            pass

        if line[0]!=keyWords[2]: # line2 = "CLIM_T_PROG= RES_CONTACT  1 2 -1 3"
            for i in range(nbValue):
                CG=QTableWidgetItem()
                CG.setText(line[2+i].rstrip())
                table.setItem(cr,2+i,CG)
            a = " ".join(line[2+nbValue:])
        else: # line2 = "CLIM_T_PROG= RES_CONTACT  2 -1 3"
            a = " ".join(line[2:])
        a1=(a.partition('-1')[0]).strip() # a = '2'
        a2=(a.partition('-1')[2]).strip() # a = '3'
        RE1=QTableWidgetItem()
        RE2=QTableWidgetItem()
        RE1.setText(a1)
        table.setItem(cr, 2+nbValue, RE1)
        RE2.setText(a2)
        table.setItem(cr, 2+nbValue+1, RE2)
        UC=QTableWidgetItem()
        UC.setText(self.com)
        self.com=''
        table.setItem(cr, 2+nbValue+2, UC)
        if line[0]==keyWords[2] : self.Table_prog2(table, CBB, cr)
        pass
