/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include "syr_parall.h"
#include "syr_bd.h"
#include "syr_tree.h"
#include "syr_hmt_bd.h"

#include "syr_cfd.h"
#ifdef _SYRTHES_CFD_
#include "syr_cfd_coupling.h"
#endif



/* AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA */
void add_var_in_file(int,double*,char*,int);
void affiche_fdf(int,double*,struct FacForme ***,double*,struct PropInfini,struct Horizon horiz,int);
void affiche_tree(struct node*,int);
void ajoute_liste(int,int,int*,int**,int**,int*,int*);
void alloue_linkcondray_parall(int,struct SDparall_ray*);
void alloue_resol(int,int,int,struct Maillage,struct Maillage,struct MaillageCL,
		  struct Matrice *,struct MatriceRay*,struct Prophy,struct Travail *,struct Travail *);
void alloue_matrice(struct Maillage,struct Prophy,struct Matrice*);
void alloue_nbvar(int,struct Humid,struct Variable*,struct Cvol **);
void alloue_rc(struct Contact *);
void alloue_tvar(int,struct Variable*);
void alloue_val_clim(int,struct Clim*,struct Clim*,struct Clim*,struct Contact*,
		     struct Clim*,struct Cvol*);
void alloue_val_clim_cond(struct Clim*,struct Clim*,struct Clim*);
void alloue_val_clim_all(struct Contact*,struct Clim*);
void alloue_val_cvol(struct Cvol*);
void alloueray(struct Maillage,struct ProphyRay*,
	       struct Clim*,double**,
	       double***,double***,double**,double**,double***);
void alter_axi1(int,struct Maillage,struct SymPer);
void assemb_complt(double*,int,double*,struct SDparall);
double axi_integ_vis(double x[],double y[]);
double axi_integ_fc(double x[],double y[]);
void azero1D(int,double*);
void azero2D(int,int,double**);
void avaleur1D(int,double*,double);
void avaleur2D(int,int,double**,double);

/* BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB */

int bary_cache_3d(double*,double*,double*,double,double,double);
void bary2d(double,double,double,double,double,double,double*,double*);
void bary3d(double,double,double,double,double,double,double,double,double,
	    double,double,double,double*,double*,double*);

void bary_tetra(double,double,double, double,double,double,  double,double,double,
		double,double,double, double,double,double,  double*,double*,double*,double*);
void bary_tria(double,double, double,double,  double,double, double,double,  
	       double*,double*,double*);
void besoin2d(int**,int**,int*,int,int**,double***,int**,double,double);
void bilsolaire(struct Maillage,struct Soleil,struct Bilan,struct Bande,double*,double);
void bilsurf(struct Maillage,struct MaillageCL,
	     struct Climcfd,struct Clim,struct Clim,struct Contact,struct Couple,
	     struct Clim,struct HmtClimhhh, struct ConstPhyhmt,
	     struct Variable,struct Bilan,double);
void bilvol(struct Maillage,struct Climcfd,struct Cvol,struct Variable,struct Bilan,double,
	    struct Humid,double **,struct ConstPhyhmt,struct ConstMateriaux*);
void boite(int,double**,double[]);
void boite2(int,int,double**,int,double**,double[]);
void box_2d(int,double**);
void box_3d(int,double**);

void build_quadtree_1d (struct node*,int,int,int**,double **, double*,double*,int);
void build_quadtree_2d (struct node *,int,int,int**,double**,double*,double[],int);
void build_octree(struct node*,int,int,int**,double **, double*,double[],int);
void build_octree_3d(struct node*,int,int,int**,double **, double*,double[],int);


/* CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
void cal_dmin(double,double,double,double*,double*,double*,double*,
	      double*,double*,double*,double*,double*,
	      int,double*,double*,double*,double*,int*,int*,int*);
double calbilsurf(int,int,int,int*,double**,double**,struct Maillage,struct MaillageCL,
		  struct Bilan,double*);
double calbilvol(int,int,int*,double*,double*,struct Maillage,struct Bilan,double*);
void calchampmax(int,double*,double*,double*);
void calcosdir(int,double*,double*,double*,double*);
void calcosdirS(int,double*,double*,double*,double*);
void calcul_extreme(double,struct Maillage,struct Prophy,struct Variable,struct Humid,struct SDparall);
void caldtvar(struct PasDeTemps*,int,double*,double*,struct SDparall);
void calfdf(struct Maillage,struct FacForme ***,double **,struct SymPer,
	    struct Compconnexe,struct Mask,struct Horizon*,struct SDparall_ray);
void calfdfH(struct Maillage,struct FacForme***,double**,struct Compconnexe,
	      struct Mask, struct Horizon *,struct SDparall_ray);
void calfdfnp1(struct Maillage,struct FacForme ***,double *,struct PropInfini*,struct Horizon,int);
void caltempmst(int,int,int,int*,double*,double*,int**,int**,double*);
double cal_mask_2d(double[],double[],double**,struct Mask);
double cal_mask_3d(double[],double[],double**,struct Mask);
double ccd(double,double); 
void cnor_2d(struct Maillage);
void cnor_3d(struct Maillage);
void connex_2d(int*,int*,int,int,int*,int); 
void connex_3d(int*,int*,int,int,int*,int);
void constantes_soleil(struct Soleil*);
double contou2d(double[],double[]);
double contou3d(double[],double[],double[]);
void contrib_perio(struct Maillage,struct Cperio,double*,double*,struct Matrice);
double coplanaire_2d(double,double,double,double);
double coplanaire_2a(int,double,double,double,double);
double coplanaire_3d(double,double,double,double,double,double);
void couplevitre(struct Vitre*,struct Maillage,struct Compconnexe);
double coutou2d(double[],double[]);
double coutou3d(double[],double[],double[]);
void corresSR(struct GestionFichiers,struct Maillage,struct MaillageCL,struct Maillage,
	      struct Couple,struct SDparall_ray);
void corresRS(struct GestionFichiers,struct MaillageBord,struct Maillage,struct MaillageCL,struct Maillage,
	      struct Couple,struct Couple,struct SDparall_ray);
void corresRS_parall(struct Maillage,struct Maillage,struct Maillage,struct Couple,struct Couple,struct SDparall_ray*);

void corresSR3d(struct Maillage,struct MaillageCL,struct Maillage,struct Couple);
void corresSR2d(struct Maillage,struct MaillageCL,struct Maillage,struct Couple);
void corresRS3d(struct Maillage,struct Maillage,struct Couple);
void corresRS2d(struct Maillage,struct Maillage,struct Couple);


void corresFS(struct GestionFichiers,struct Maillage,struct MaillageBord,struct MaillageCL,struct MaillageCL,
	      struct Couple,struct Couple);

double cpusyrt();
void cree_connexe(struct Maillage,int*,struct Compconnexe*);
void cree_liste_clim_cond(struct Maillage,struct MaillageBord,struct MaillageCL,    
			  struct Clim*,struct Clim*,struct Clim*);
void cree_liste_clim_all(struct Maillage,struct MaillageBord,struct MaillageCL,    
			 struct Contact*,struct Clim*,struct SDparall);
void cree_liste_noeuds_periorc(struct MaillageBord,struct Maillage,int,
			       int *,int,int*,int*,int**,int**);
void cree_paires_noeuds_perio(struct Maillage,int,int,int*,int*,struct Cperio*,int,
			      double,double,double,double,double,double,double);

void cree_liste_perio(struct Maillage,struct Maillage,
		      struct Cperio*,int *,int,int,
		      double,double,double,double,double,double,double);
void cree_paires_noeuds_rc(struct Maillage,int ,int,int*,int*,struct Contact*);

/* DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD */

void data_element_moy(int,struct Maillage,double*,int*,double*,double*,double*,double*);
void decode_clim_alloue(struct MaillageBord*);
void decode_clim_cond(struct MaillageBord*);
void decode_clim_all(struct MaillageBord*);
void decode_cvol(struct Maillage,struct Cvol*,char **);
void decode_clim_ray(struct Maillage *);
void decode_mst(struct Maillage,struct Mst*,struct SDparall);
void decode_prophy(struct Maillage,struct Prophy*);
void decoupe(struct node*,int**,double**,int,int,int,double*,int);
void decoupe1d(struct node*,int**,double**,int,int,int,double*,int);
void decoupe2d(struct node*,int**,double**,int,double*,int);
void decoupe3d(struct node*,int**,double**,int,double*,int);
void decouphor_2a(int,double*,double*,double,double,double,double);
void decoupe_qdqd(int,int, double**,double*,double[],double[],double[],double[],
		  double[],double[],double[],double[],double[],double[],int);
void decoupe_seg(int,int, double**,double*,double*,double*,double*,int);
void decoupe_totd(int,int, double**,double*,double[],double[],double[],double[],int);
void decoupe_toqd(int,int, double**,double*,double[],double[],double[],double[],
		  double[],double[],double[],int);
void decoupe_tdtd(int,int, double**,double*,double[],double[],double[],double[],int);
void decoupe_tdqd(int,int, double**,double*,double[],double[],double[],double[],
		  double[],double[],double[],double[],double[],double[],int);
void derriere_2d (int,int, double**,double*,double*,double*,double*,int*);
void derriere_2a (int,int,int,double,double,double,double,double,double,double,double,int*);
void derriere_3d (int,int, double**,double*,double*,double*,double*,double*,int*);
int derriere_soleil2d (double,double,double,double);
int derriere_soleil3d (double,double,double,double,double,double);
double determ(double,double,double,double,double,double,double,double,double);

int  diag_tria       (double,double,double,double,double,double,double,double*);
void difsol(struct PasDeTemps,
	    struct Maillage,struct MaillageCL,struct MaillageBord,
	    double*,double*,struct Prophy,
	    struct Clim,struct Clim,struct Contact,struct Couple,struct Clim,
	    struct Clim,struct Climcfd,struct Climcfd,struct Cvol,struct Cperio,
	    struct Mst mst,struct Matrice,struct Travail,struct Travail,
	    struct SDparall);
void dimension_2d(struct Maillage,double*,double*);
void dimension_3d(struct Maillage,double*,double*);
void divmst(int,double*,double*,double,double,double*);
void dptseg(double,double,double,double,double,double,double*,double*,double*,int*);
void dupliq2d_sym(struct SymPer,int,struct Maillage,struct Maillage*);
void dupliq3d_sym(struct SymPer,int,struct Maillage,struct Maillage*);
void dupliq2d_per(struct SymPer,int,struct Maillage,struct Maillage*);
void dupliq3d_per(struct SymPer,int,struct Maillage,struct Maillage*);
void dupliq2d_st(struct SymPer,int,int*,int*,double**,double**,int);
void dupliq3d_st(struct SymPer,int,int*,int*,double**,double**,int);
void dupliq_maill(struct Maillage,struct Maillage*,int);

/* EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE */

int ecrire_chrono(int flag,struct GestionFichiers,struct PasDeTemps,
		  struct Maillage,struct MaillageBord,struct Variable,struct Maillage,struct ProphyRay,
		  double*,double**,struct SDparall_ray,
		  struct Humid,struct ConstPhyhmt,struct ConstMateriaux*,
          struct Climcfd,struct Cfd,struct Travail,struct Travail);
void ecrire_entete(FILE**,char*,int,double,double);
void ecrire_flux(struct Maillage,struct Variable,struct Prophy);
void ecrire_geom(char*,struct Maillage*,struct MaillageBord*);
void ecrire_geom_cplcfd(char*,struct Maillage,struct MaillageCL,struct Climcfd,struct Cfd);
void ecrire_resu(struct GestionFichiers,struct PasDeTemps,struct Maillage,struct Variable,struct Maillage,
		 struct ProphyRay,double*,double**,struct SDparall_ray,
		 struct Humid,struct ConstPhyhmt,struct ConstMateriaux*,
         struct Climcfd,struct Cfd,struct Travail,struct Travail);
void ecrire_var(FILE*,int,double*,char*,int);
void ecrire_histo(struct PasDeTemps,struct Maillage,struct Histo,struct Variable,struct Humid,
		  struct ConstPhyhmt,struct ConstMateriaux*);
void ecrire_histo2(FILE**,char*,double,double,struct Histo,struct Maillage,struct Variable,int,
		   struct Humid,struct ConstPhyhmt,struct ConstMateriaux*);
void ecrire_corrSR(int,struct Couple);
void ecrire_corrRS(int,struct Couple);
void ecrire_corrFS(int,struct Couple,struct Couple);
void ecrire_fdf(struct FacForme***,double*,struct PropInfini,struct Horizon,int,struct SDparall_ray);
void elague_tree(struct node*);
void element_center(int,struct Maillage,double*,double*,double*);
void epure_connexe(int*,struct Compconnexe,struct Compconnexe*);
int existe_perio();
void extr_motcle_(char*,char*,int*,int*);
void extr_motcle(char*,char*,int*,int*);
void extrvois2(struct Maillage,int**);
void extrvois3(struct Maillage,int**);
void extrbord2(struct Maillage,struct MaillageBord*,int**);
void extrbord3(struct Maillage,struct MaillageBord*,int**);
void extract_nodecoupl_parall(struct Maillage,struct MaillageCL,struct Couple,
				  struct Maillage*,struct SDparall_ray);
void extraitmask(struct Maillage*,struct Mask*,struct Horizon*);
void extrarete(struct Maillage*);
void extreme(int,int,double*,struct Maillage, double*,struct SDparall);
void extreme_partial(int,int,double*,int*,struct Maillage,double*, struct SDparall);

/* FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF */

void facecache_2d(int,int**,double**,double**,double*,int*,int*,int,int);
void facecache_3d(int,int**,double**,double**,double*,int*,int*,int,int);
void facforme_2d(int,int,int,int**,double**,double*,double**,struct FacForme***,double*,int*,int,struct Mask,
		 struct Horizon*,int*,struct SDparall_ray);

void facforme_2a(int,int,int,int,int**,double**,double**,struct FacForme***,double*,int*,struct Mask,
		 struct SDparall_ray);
void facforme_3d(int,int,int,int**,double**,double*,double**,struct FacForme***,double*,
		 int*,int,struct Mask,struct Horizon*,int*,struct SDparall_ray);
void fdf_Y_parall(double*,double*,struct FacForme***,double*,double*,struct SDparall_ray);
void fdf_Y_parall_debug(double*,double*,struct FacForme***,double*,double*,
			struct SDparall_ray,struct ProphyRay);
void fic_bin_f__endswap(void*,size_t,size_t);
void fi2teq(int,double*,struct FacForme**,double*,double*,struct ProphyRay,
	    double**,double**,double*,double*,
	    struct PropInfini,struct Clim,struct Soleil,
	    struct Horizon,struct Vitre);
void fi2teq_parall(int,double*,struct FacForme***,double*,double*,struct ProphyRay,
		   double**,double**,double*,double*,
		   struct PropInfini,struct Clim,struct Soleil,
		   struct Horizon,struct Vitre,struct SDparall_ray,double*,double*,double*);
void findel(struct node*,int,double**,int**,double,double,double,int*,double,int*);
void find_node_3d(struct node **,double,double,double);
void find_node_2d(struct node **,double,double);
void flux2d_soleil(struct Lieu,struct Soleil,struct Meteo meteo,struct Maillage,
		   double*,struct PasDeTemps,struct Vitre,struct ProphyRay,
		   struct node*,double,double[]);  
void flux3d_soleil(struct Lieu,struct Soleil,struct Meteo meteo,struct Maillage,
		   double*,struct PasDeTemps,struct Vitre,struct ProphyRay,
		   struct node*,double,double[]);  
void flux2d_soleil_anglect(struct Soleil,struct Maillage,double,
			   struct Vitre,struct ProphyRay,
			   struct node*,double size_min,double[]);
void flux3d_soleil_anglect(struct Soleil,struct Maillage,double,
			   struct Vitre,struct ProphyRay,
			   struct node*,double size_min,double[]);

/* GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG */

void gauss();
void grconj(struct Maillage,double*,struct Contact,struct Cperio,
	    struct Travail,struct Matrice,double,int);
void grconj_parall(struct Maillage,double*,struct Contact,struct Cperio,
		   struct Travail,struct Matrice,double,int,struct SDparall);
void group_2d(int,int,int,int*,int*);
void group_3d(int,int,int,int*,int*);

/* HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH */

void hauteur_soleil(struct Lieu,struct Soleil*);
void histog (int,double*,double,double,int*,int);
void histog_corresp(int,double*,struct SDparall_ray);
void horizon(struct Maillage,struct Soleil*,struct Lieu);

/* IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII */
void imprime_Clim(struct Clim);
void imprime_Climp(struct Clim);
void imprime_maillage_ref(struct Maillage);
void imprime_maillage(struct Maillage);
void imprime_connectivite(struct Maillage);

int in_boite(double,double,double,double,double,double,double,double,double);
int in_rectan(double,double,double,double,double,double);
int in_seg(double, double, double, double,double, double);
int in_triangle(double , double , double , double ,
		double ,double ,double ,double ,double ,double ,
		double ,double ,double ,
		double ,double ,double);
int in_tria_2d (double,double,double,double,double,double,
		double,double,double);
int in_tetra2 (double,double,double,double,double,double,
	       double,double,double,double,double,double,
	       double,double,double,double);

int indvoir(int,int,int);
double int2eg(double,double,double,int); 
void inifdf(struct GestionFichiers,struct Maillage*,struct SymPer,struct Compconnexe,
	    struct PropInfini*,struct FacForme****,double**,struct Mask,
	    struct Vitre*,struct Horizon*,struct SDparall_ray*);

void inimst(struct Mst*,struct Maillage);
void iniori_2d(int*,int**,double**,int*,int,int,struct Compconnexe,int,int*,int*,int*);
void iniori_3d(int*,int**,double**,int*,int,int,struct Compconnexe,int,int*,int*,int*);
void iniparallray(struct Maillage,struct SDparall_ray*);
double intseg(double,double,double,double,double,double,double,double,double);
void init_clim(struct Maillage*,struct MaillageBord*,struct MaillageCL*,    
	    struct MaillageCL*,  struct Clim*,struct Cperio*,
	    struct Clim*,struct Clim *,struct Contact*,struct Clim*,struct Cvol*,
	    struct Humid,struct HmtClimhhh*,
	    struct Variable,struct SDparall);
void init_scoupr(struct MaillageCL,struct Couple*,struct SDparall_ray);
void init_rcoups(struct Maillage,struct Couple*);
void init_radiation(struct GestionFichiers,
		    struct Maillage *,struct Maillage, struct MaillageBord,struct MaillageCL,
		    struct FacForme ****,double**,struct Couple *,struct Couple *,
		    struct Compconnexe*,struct PropInfini*,struct Clim*,struct Clim *,
		    struct ProphyRay *,struct SymPer*,struct Soleil *,struct Bilan *,struct node **,
		    double*,double*,struct Lieu *,struct Meteo,struct Horizon*,struct Vitre*,struct Mask*,
		    double **,double ***,double ***,double **trayeq,double **,double ***,
		    struct SDparall_ray*,struct SDparall);
void init_radiation_parall(struct GestionFichiers,
			   struct Maillage *,struct Maillage, struct MaillageBord,struct MaillageCL,
			   struct FacForme ****,double**,struct Couple *,struct Couple *,
			   struct Compconnexe*,struct PropInfini*,struct Clim*,struct Clim *,
			   struct ProphyRay *,struct SymPer*,struct Soleil *,struct Bilan *,struct node **,
			   double*,double*,struct Lieu *,struct Meteo,struct Horizon*,struct Vitre*,struct Mask*,
			   double **,double ***,double ***,double **trayeq,double **,double ***,
			   struct SDparall_ray*,struct SDparall);
void initsolaire(struct Soleil*,struct Lieu,struct node**,struct Maillage,struct ProphyRay,
		 double*,double*,struct SDparall);
double interpol_table1D(double,int,double*,double*);
double interpol_table2D(double,double,int,int, double**);
void interp_meteo(double,struct Meteo,double*,double*,int);


void ivoitj_2d(struct node*,struct node*,struct node*,double*,double*,double*,
	       int*,double,int**,double**,int*,double*);

void ivoitj_3d(struct node*,struct node*,struct node*,double*,double*,double*,
	       int*,double,int**,double**,int*,double*);
void ivoitj_2dST(struct node*,struct node*,struct node*,double*,double*,double*,
	       int*,double,int**,double**,double**,int*,double*,int*,double**,double*,int);
void ivoitj_3dST(struct node*,struct node*,struct node*,double*,double*,double*,
	       int*,double,int**,double**,double**,int*,double*,int*,double**,double*,int);

/* LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL */

void link_condray_parall(int,struct Couple,struct SDparall_ray*);
void lire_bilan(struct Maillage,struct Bilan*,struct Bilan*);
void lire_cini(struct Maillage,struct Variable*);
void lire_condvol(struct Cvol*,int*);
void lire_corrSR(int,struct Couple);
void lire_corrRS(int,struct Couple);
void lire_corrFS(int,struct Couple,struct Couple);
void lire_conlire_tempdvol(struct Cvol*,int*,struct SDparall);
void lire_data_mc(struct GestionFichiers*,struct Maillage*,struct PasDeTemps*);
void lire_dirichlet(struct Clim*,struct Maillage,struct MaillageBord,char*,int,char*);
void lire_divers_ray(struct Maillage,struct SymPer*,struct Compconnexe *,struct Bande,
		     struct PropInfini*,struct Mask*,
		     struct Soleil*,struct Lieu*,struct Bilan*,struct Horizon*);
void lire_donnees(struct Maillage,struct Variable*,struct Prophy*,struct Histo*,struct Bilan*,
		  struct Bilan*,struct Humid);
void lire_donnees_ray(struct Maillage,struct Maillage*,
		      struct GestionFichiers*,
		      struct Bande*,struct PropInfini*,
		      struct Mask*,struct Soleil*,struct Horizon*,
		      struct Vitre*,struct SDparall_ray*);
void lire_echange(struct Clim*,struct MaillageCL,char*,int,char*);
void lire_env(int,int*);
void lire_fdf(struct FacForme***,double**,struct PropInfini*,struct Horizon*,struct SDparall_ray);
void lire_firstdata(struct Variable*,struct Maillage*,struct GestionFichiers *,
		    struct PasDeTemps*,struct Humid*,struct Meteo*);
void lire_flux(struct Clim*,struct MaillageCL,char*,int,char*);
void lire_fluxvol(struct Cvol*,int*,char*,int,char*);
void lire_histo(struct Maillage,struct Histo*);
void lire_ideas(struct Maillage*,struct MaillageBord*);
void lire_ideas_ray (struct Maillage*);
void lire_ini(double*,char*,struct Maillage,char*,int);
void lire_limite_cond(struct Clim*,struct Clim*,struct Clim*,
		      struct Maillage,struct MaillageBord,struct MaillageCL);
void lire_limite_all(struct Contact*,struct Clim*,
		     struct Maillage,struct MaillageBord,struct MaillageCL);
void lire_limite_mst(struct Mst*);
void lire_limite_ray(struct Maillage,struct ProphyRay,struct Vitre*,struct Clim*,double*);
void lire_maill(struct Maillage*,struct MaillageBord*,struct SDparall*);
void lire_main_options(struct Humid*,int*,int*);
void lire_meteo(struct Meteo*,int);

void limfray(struct Maillage,struct Clim*,struct Clim*,struct Vitre*);
void lire_meteo(struct Meteo*,int);
void lire_perio(struct Cperio*,struct Maillage,struct MaillageBord,struct SDparall);
void lire_prophy(struct Maillage,struct Prophy*);
void lire_ray_mc(struct GestionFichiers*,struct PropInfini*,struct Bande*,struct Soleil*,
		 struct Vitre*);
void lire_rayinf(struct Clim*,struct MaillageCL,char*,int);
void lire_pairesrc(struct Contact *,struct Maillage,struct MaillageBord,struct SDparall);
void lire_rescon(struct Contact*,struct MaillageCL,char*,int);
void lire_simail(struct Maillage*,struct MaillageBord*);
void lire_simail_ray (struct Maillage*);
void lire_suite(struct Maillage,struct PasDeTemps*,struct Variable *variable);
void lire_syrthes(struct Maillage*,struct MaillageBord*,struct SDparall*);
void lire_syrthes_ray(struct Maillage*);
void lumcent2d(int,int*,int*,int**,double***,double*,double*,double*,
	       double,double,double,double,double,int**,double**,
               double*,double*);
void lumfac31(int,int,int,int,int,int,int,int,int **,int**,double***,
	      double,double,double,double,double**,double*,double*);
void lumfac32(int,int,int,int,int,int,int,int,int **,int**,double***,
	      double,double,double,double,double**,double*,double*);
void lumbord2d(int,int**,double**,double*,double*,double*,double,double,double);

/* MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM */

void mamass(struct PasDeTemps,struct Maillage,double*,double*);
void madif_isotro(struct Maillage,struct Iso,double*,double*);
void madif_orthotro(struct Maillage,struct Ortho,double*,double*);
void madif_anisotro(struct Maillage,struct Aniso,double*,double*);

void madif1(double**,struct Maillage,struct Prophy,double**);
void madif2(double**,struct Maillage,struct Prophy,double**);
void madif3(double**,struct Maillage,struct Prophy,double**);
void M_mat(struct Maillage,double *,double *);
void mafcli(struct Clim,struct Couple,struct Clim,struct Climcfd,
	      struct Maillage,struct MaillageCL,
	      double*,double*,double*);
void mafclc(struct Contact,struct Maillage,struct MaillageCL,
	    double*,double*,double*);
void matbord(struct Maillage,struct MaillageCL,double*,double*);
int min_int_parall(int);
int max_int_parall(int);
double min_double_parall(double);
double max_double_parall(double);
void mise_a_jour_date(struct Lieu*,double);


/* NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN */

void normarete2d(struct Mst*,int**,double**);
void nummst(struct Mst*,struct Maillage);

/* OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO */

void omv(struct Maillage,double*,double*,struct Matrice,struct Cperio,double*);
void omv_parall(struct Maillage,double*,double*,struct Matrice,struct Cperio,double*,struct SDparall);
void omvdir(struct Maillage,struct Clim,double*,double*,struct Matrice);
void ordonne(int,int,int**,int**,int*,int*);
void oriene_2d(int*,int**,int,int*,int*,int,int*);
void oriene_3d(int*,int**,int,int*,int*,int,int*);
void orient2d(struct Maillage,struct Compconnexe,int*);
void orient3d(struct Maillage,struct Compconnexe,int*);
void ov(int ,int ,double *, double *, double *, double);


/* PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP */

void period(struct Maillage,double*,double*,struct Cperio);
void period_parall(struct Maillage,double*,double*,struct Cperio,struct SDparall);
void perio2(struct Maillage,struct Maillage,
	    struct Cperio*,int *,int,int,double,double,double);
void perio3(struct Maillage,struct Maillage,
	    struct Cperio*,int *,int,int,
	    double,double,double,double,double,double,double);
void persym3d(double[4][4],int,int,struct Maillage,struct Maillage,int,int);
void prepare_paires_rc(struct Maillage,struct Contact,double*,double*,struct SDparall);
void proj2d_soleil(struct Lieu,struct Soleil,double*,double*);
void proj3d_soleil(struct Lieu,struct Soleil,double*,double*,double*);
void proprvitre(struct Vitre,struct Maillage,struct ProphyRay);
void prosca_ray_parall(double*,double*,double*,struct SDparall_ray);

void postflux_isotro(struct Maillage,struct Iso,double*,double**);
void postflux_orthotro(struct Maillage,struct Ortho,double*,double**);
void postflux_anisotro(struct Maillage,struct Aniso,double*,double**);

/* QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ */

void quel_angle(int,int,double,double,double,double,double,double,double,double,int*,int*);
void quel_angle_hide (int,int,double,double,double,double,double,double,double,double);
int quel_fils_3d (double,double,double,struct child *,double,double,double);
int quel_fils_2d (double,double,struct child *,double,double);

/* RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR */

int racines_2d(double,double,double,double,double,double,double,double,int);
int racines_3d(double,double,double,double,double,double,double,double,double,
             double,double,double,double,double,double,int);
void radiation(struct PasDeTemps*,struct GestionFichiers,
	       struct Maillage,struct Maillage,struct MaillageCL,
	       double*,double*,struct Couple,struct Couple,struct FacForme***,double*,double*,
	       double**,double**,double*,double*,struct ProphyRay,double **,struct Clim,
	       struct PropInfini *,struct MatriceRay,struct Soleil *,struct Bilan,
	       struct Lieu,struct Meteo,struct Myfile,struct node*,double,double*,
	       struct Compconnexe,struct Horizon,struct Vitre,struct Travail,
	       struct SDparall,struct SDparall_ray);
int ray_inter_seg(struct node*,struct node*,int,double*,double*,
		  int**,double**,int*);
int ray_inter_triangle(struct node*,struct node*,int,double*,double*,
		       int**,double**,int*);
void recale_file_fadd(FILE**);
void recopie(int,struct Variable);
void reduire_maillage(struct Maillage*,struct Couple,struct SDparall_ray*);
void regufdf(struct GestionFichiers, struct Maillage, struct PropInfini,
	     struct FacForme ***,double *, struct Mask, struct Horizon,struct SDparall_ray);
int rejectfdf_3d (double,double,double,double,double,double,double [],double [],double []);
int rep_int(char*);
int rep_ch(char*,char*);
int rep_chw(char*,char*);
void rep_listint(int*,int*,char*);
void rep_listdble(double*,int*,char*);
double rep_dbl(char*);
void rep_ndbl(int,double*,int*,char*);
void rep_nint(int,int*,int*,char*);

void resol_solaire(struct Soleil *,struct Lieu *,struct node*,struct Maillage,struct ProphyRay,
		   struct PasDeTemps,struct Vitre,struct Meteo,struct Myfile,struct PropInfini,double,double*,struct SDparall);
void resoud3(double,double,double,double,double,double,double,double,double,
	     double*,double*,double*);
void resoumst(struct Mst*,struct Maillage,double*);
void ressol(struct PasDeTemps*,
	    struct Maillage,struct MaillageCL,struct MaillageCL,struct MaillageBord,
	    struct Clim,struct Cperio,struct Climcfd,struct Climcfd,
	    struct Clim,struct Clim,
	    struct Contact,struct Couple,struct Clim,struct Cvol,
	    struct Mst mst,
	    struct Variable,struct Prophy,struct Meteo,struct Myfile myfile,
	    struct Matrice,struct Travail,struct Travail,
	    struct SDparall);
void resray(int,struct Maillage,struct FacForme***,double*,double*,double**,double**,double*,double*,
	    struct ProphyRay,double**,struct Clim,struct PropInfini,struct MatriceRay,
	    struct Soleil,struct Bilan,struct Compconnexe,struct Horizon,struct Vitre,
	    double*,struct SDparall_ray);
void rotation2d(double,double,double,double,double,double*,double*);
void rotation3d(double,double,double,double,double,double,double,
	        double,double,double,double*,double*,double*);	
void rotation_k(int,double*);
void rorien_2d(int,int,int,int*,int*,int**,int,int*);
void rorien_3d(int,int,int,int*,int*,int**,int,int*);
void rrayrc(int,struct FacForme **,double*,double*,double*,double*,struct MatriceRay,int*);

void rrayrc_parall(int,struct FacForme***,double*,double*,double*,double*,
		   struct MatriceRay,int*,
		   double*,struct SDparall_ray);
void rrayrcglob(struct Maillage,struct FacForme**,double*,
		double*,double*,double*,struct Vitre,
		struct MatriceRay,int*);

/* SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS */

void s_en_h(double,int*,int*,int*);
int seg_rectanx     (double,double,double,double,double,double);
int seg_rectany     (double,double,double,double,double,double);
int seg_cubex       (double,double,double,double,double,double,double,double,double);
int seg_cubey       (double,double,double,double,double,double,double,double,double);
int seg_cubez       (double,double,double,double,double,double,double,double,double);
int seg_in_rectan(double,double,double,double,double,double,double,double);
void segfdf(struct node*,double,double*,int*,double*,double*,
	      int*,int*,int,int**, double**,int*,double*,int*);
void segfdfC(int,int,
	     struct node*,double,double[],double[],double[],
	     int[],int**,double**,int*,double*);
double segfdfaxi (int,int,int,int,double[],double[],int**,double **);
void smbray(int,int,double*,double*,struct PropInfini,struct ProphyRay,
	    double**,double**,struct Horizon,struct Vitre,struct Soleil);
void smfvol(struct Maillage,int,int*,double*,double*,double**);
int somme_int_parall(int);
double somme_double_parall(double);
void smoogc(int,struct FacForme**,double*,double*);
void smoogc_parall(int,struct FacForme***,double*,double*,struct SDparall_ray);
void smfvos_expl(struct Maillage,struct Cvol,double*);
void smfvos_impl(struct Maillage,struct Cvol,double*);
void smfflu(struct Clim,struct Couple,struct Clim,struct Climcfd,struct Clim,
	    struct Maillage,struct MaillageCL,double*,double*);
void smdirs(struct Clim diric,struct Maillage,struct MaillageBord,double*,
            struct Cperio,struct Matrice,struct Travail,
	    struct SDparall);
double s_optim(int,double*,double*,double*);
void soutri(double,double,double,double,double,double,double,double,double,
	    double,double,double,int*);

void surface_anneau(struct Maillage);
void surfbmst(struct Mst*,double**);
void surface_seg(struct Maillage);
void surface_tria(struct Maillage);
void svolum(struct Maillage*,struct MaillageCL*);
void sym2d(double[3][3],int,int,struct Maillage,struct Maillage,int,int);
void sym3d(double[4][4],int,int,struct Maillage,struct Maillage,int,int);
void syrban(int);
void syrthes(int,int);
void syrthes_exit(int);
void syrthes_initsdparall(struct SDparall*,struct SDparall_ray*);
void syrthes_initmpi(int,struct SDparall*);
void syrthes_initmpi_ray(int,struct SDparall,struct SDparall_ray*);

/* TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT */

void tab_integ(int,int);
int tetra_in_cube(double,double,double,double,double,double,
		  double,double,double,double,double,double,
		  double,double,double,double,double,double);
void transRS(int,struct Couple,double*,double*);
void transRS_parall(int,struct Couple,double*,double*,struct SDparall_ray);
void transSR(int,int**,struct Couple,double*,double*);
void transSR_parall(int,int**,struct Couple,double*,double*,struct SDparall_ray);
void triafdf (struct node*,double,double[],int*,double[],double[],double[],
	      double[],double[],double[],int[],int[],int,int**,double**,int*,double*,int*);
void triafdfC(struct node*,double,double[],int*,double[],double[],double[],
	      int[],int,int**,double**,int*,double*);
int  tria_in_cube(double,double,double,double,double,double,
		  double,double,double,double,double,double,
		  double,double,double);
int tria_in_rectan(double,double,double,double,double,double,
		   double,double,double,double);
void triface(struct element*,struct element*,int*,int,int,int**,double**,
	     double,double,double,double,double,double);

void triseg(struct element*,struct element*, 
	     int*,int,int,int**,double**,double,double,double,double);
void tritetra(struct element*,struct element*,int*,int**,double**,
              double,double,double,double,double,double);
void tritria(struct element*, struct element*,int*,int**,double**,
	     double,double,double,double);
void trimst(struct Mst*,struct Maillage,double,double);
void touta0(struct Maillage*,struct MaillageBord*,struct MaillageCL*,
	    struct Clim*,struct Cperio*,struct Clim*,struct Clim*,
	    struct Contact*,struct Clim*,struct Cvol*,struct Couple*,struct Couple*,
	    struct HmtClimhhh*);
void tuer_tree(struct node*);

/* UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU */
void user_add_var_in_file(struct Maillage,struct Cvol*,struct Variable,struct Prophy,struct PasDeTemps);
void user_cini(struct Maillage,double*,struct PasDeTemps*,struct Meteo,struct Myfile);
void user_cini_fct(struct Maillage,double *);
void user_cphyso    (struct Maillage,double*,struct Prophy,struct PasDeTemps*,struct Meteo,struct Myfile);
void user_cphyso_fct(struct Maillage,double*,struct Prophy,double);
void user_limfso    (struct Maillage,struct MaillageBord,struct MaillageCL,
		     double*,struct Clim,struct Clim,struct Clim,struct Clim,struct PasDeTemps*,
		     struct Meteo,struct Myfile);
void user_limfso_fct(struct Maillage,struct MaillageBord,struct MaillageCL,
		     double*,struct Clim,struct Clim,struct Clim,struct Clim,double);
int user_read_myfile(struct Myfile*);
void user_cfluvs    (struct Maillage,double*,struct Cvol,struct PasDeTemps*,struct Meteo,struct Myfile);
void user_cfluvs_fct(struct Maillage,double*,struct Cvol,double);
void user_propincidence(int,int,double,double**,double**,double**,double**,
			double**,double**,double**,double**);
void user_rescon     (struct Maillage,struct MaillageCL,double*,double*,struct Contact,
		      struct PasDeTemps*,struct SDparall);
void user_rescon_fct (struct Maillage,struct MaillageCL,double*,double*,struct Contact,double,struct SDparall);
void user_transfo_perio(int,int,double,double,double,double*,double*,double*);
void user_solaire(double*,double*,int,struct PasDeTemps);
void user_ray(struct Maillage,double*,struct ProphyRay,
	      struct PropInfini*,struct Clim,struct Vitre,struct PasDeTemps);


/* VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV */
void valeur_copain_parall(int,double*,double*,int,int,int*,int*,int*,double*,struct SDparall);
double var_element_moy(int,struct Maillage,double*);
void verif_alloue_int1d(char*,int*);
void verif_alloue_int2d(int,char*,int**);
void verif_alloue_double1d(char*,double*);
void verif_alloue_double2d(int,char*,double**);
void verif_alloue_char(char*,char*);
void verif_COND_data();
void verif_RAYT_data();
void verif_ray();	
void verif_coor_3d(struct Maillage maillnodray,int**,double**,struct SymPer,double);
void verif_data();
void verif_fdf(int,double*,struct FacForme***,double*,int);
void verify_initial_cphy(struct Maillage,struct Prophy);
void verify_initial_cvol(struct Maillage,struct Cvol*);
void verif_liste_ref_0N(char*,int,int*);
void verif_maill(struct Maillage*);
void verif_ray(struct Maillage,struct ProphyRay);
int verif_type_maillage(char*);
void voisic_2d(int*,int**,int,int);
void voisic_3d(int*,int**,int,int);
void voxel_voisin_2d(double*,double*,double,double,double,double,
	             double*,double*,double*,double);
void voxel_voisin_3d(double*,double*,double*,double,double,double,
		     double,double,double,double*,double*,double*,double);

/* WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW */
double wiebel(double);

/* XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX */
void xmaill(struct Maillage, struct MaillageBord,struct MaillageCL*,struct SDparall);     




/* bati */

#ifdef _SYRTHES_CFD_ 
void cfd_surf_init(struct Maillage,struct MaillageCL,struct Climcfd*, 
		   struct SDparall,struct Cfd*);
void cfd_vol_init(struct Maillage,struct Climcfd*,struct SDparall,struct Cfd*);
void cfd_trans_surf(struct Maillage,struct MaillageCL,struct Climcfd,double*,struct Cfd);
void cfd_trans_vol (struct Maillage,struct Climcfd,double*,struct Cfd);
void cfd_fin(struct Cfd*);
void cfd_synchro(struct PasDeTemps*,struct Cfd);
void cfd_init_couplage(struct SDparall,int,struct Cfd*);
double cfd_calbilsurf(int,int*,int**,double**,double**,struct Maillage,struct MaillageCL,
		      struct Bilan,double*);
double cfd_bilsurf_conserv(int,int*,double*,double*,struct Maillage,struct MaillageCL,double*);

double cfd_calbilvol(int,int*,int**,double**,double**,struct Maillage,struct Bilan,double*);
void cfd_vol_expl(struct Maillage,struct Climcfd,double*);
void cfd_vol_impl(struct Maillage,struct Climcfd,double*);


#endif


void syr_copy(int,double*,double*);
void syr_axpy(int,double,double*,double*);
double syr_prosca(int,double*,double*);
void syr_2prosca_parall(double*,double*,double*,double*,double*,struct SDparall);
void syr_prosca_parall(double*,double*,double*,struct SDparall);
