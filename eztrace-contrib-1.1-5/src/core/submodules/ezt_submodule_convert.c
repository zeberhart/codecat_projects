#include "ezt_submodule.h"
#include "ezt_getcpu.h"


static int __ezt_nb_submodules = 1;
static struct ezt_submodule __ezt_submodules[] = {
  {
    .exec_init            = NULL,
    .convert_init         = ezt_getcpu_convert_init,
    .convert_handle       = ezt_getcpu_convert_events,
    .convert_handle_stats = ezt_getcpu_convert_stats,
    .convert_print_stats  = ezt_getcpu_convert_print_stats
  }
};


void ezt_submodule_convert_init() {
  int i;

  for(i=0;i<__ezt_nb_submodules; i++) {
    __ezt_submodules[i].convert_init();
  }
}

int ezt_submodule_convert_handle(eztrace_event_t* ev) {

  int i;
  int ret = 0;
  for(i=0;i<__ezt_nb_submodules; i++) {
    ret = __ezt_submodules[i].convert_handle(ev);
    if(ret)
      return ret;
  }
  return ret;
}


int ezt_submodule_convert_handle_stats(eztrace_event_t* ev) {
  int i;
  int ret = 0;
  for(i=0;i<__ezt_nb_submodules; i++) {
    ret = __ezt_submodules[i].convert_handle_stats(ev);
    if(ret)
      return ret;
  }
  return ret;
}

void ezt_submodule_convert_print_stats() {
  int i;
  for(i=0;i<__ezt_nb_submodules; i++) {
    __ezt_submodules[i].convert_print_stats();
  }
}
