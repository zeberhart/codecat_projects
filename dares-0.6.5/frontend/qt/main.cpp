/* main.cpp */
/* part of DAta REScue dares */

/* DAta REScue (C) 2002 Oliver Diedrich, odi@ct.heise.de */
/* (C) 2005 c't magazine for computer technology, Heise Zeitschriften Verlag */
/* This file may be redistributed under the terms of the */
/* GNU Public License (GPL), see www.gnu.org */

extern "C" {
#include "dares.h"
}

extern char image[];                    // name of image to work on
extern char save_path[];                // path for recovered files
extern char h2iname[];                  // h2i file name
extern char logname[];                  // name of log file (-l); default: LOG_FILE
extern int use_mime;                    // 1: use mime types

#if defined(DOS)
#include <malloc.h>
#endif


#include "global.h"
#include "paramsdialog.h"
#include "daresview.h"

#include <qapplication.h>
#include <qmessagebox.h>

#include <cstdlib>

#define PROG_NAME "DAta REScue (C) 2005 c't"

int main(int argc, char *argv[])
{
  QApplication a( argc, argv );

  int myArgC = argc;
  char *  myArgV[20];
  for(int i=0; i<20; ++i)
  {
    myArgV[i] = (char*)malloc( 255 );
    if( i < argc )
      strncpy(myArgV[i], argv[i], 254);
  }

  int res = 0;
  QString sError;

  bool bTryAgain;
  int nFound;
  do
  {
    nFound = daresOpen(myArgC, myArgV);
    bTryAgain = DARES_OPEN_INVALID_PARAM == nFound;
    if( bTryAgain ){
      QString sInfo;
      for( int i=0; i< a.argc(); ++i ){
        const QString s( QString( a.argv()[i] ).lower() );
        if( s == "-v" || s == "-version" || s == "--version" ){
          sInfo = PROG_NAME;
        }else if( s == "-h" || s == "-help" || s == "--help" ){
          sInfo = QString(
            "</b>Synopsis: <b>%1 -i image -s path [-h2i h2ifile] [-a] [-m] [-l logfile]<br><br></b>"
            "<table border=\"1\">"
            "<tr><td>-i</td><td>name of CDROM/DVD image</td></tr>"
            "<tr><td>-s</td><td>any directory you can write to with enough space for your data</td></tr>"
            "<tr><td>-h2i</td><td>name of h2i file for image (as written by H2cdimage)</td></tr>"
            "<tr><td>-a</td><td>save all files in image without user interaction</td></tr>"
            "<tr><td>-m</td><td>use short mime types instead of detailed file types</td></tr>"
            "<tr><td>-l</td><td>name of log file (default: dares.log)</td></tr></table>"
            "&nbsp;<br>"
            "e.g. <b>%2 -i cdrom.image -s /tmp")
            .arg( argv[0] )
            .arg( argv[0] );
        }
      }
      if( ! sInfo.isEmpty() ){
        QMessageBox::information(a.desktop(), "Data Rescue",
          QString( "<u>%1</u><br>"
                  "&nbsp;<br>"
                  "&nbsp;<br>"
                  "<b>%2</b>" ).arg( VERSION ).arg( sInfo ));
        bTryAgain = false;
      }
    }
    QString imageName(   image );
    QString savePath(    save_path );
    QString h2iFileName( h2iname );
    QString logName(     logname );
    bool    useMime(     use_mime ? true : false );
    if( bTryAgain )
    {
      ParamsDialog *dlg = new ParamsDialog( imageName, savePath, h2iFileName, logName, useMime );
      dlg->setCaption( QString("%1 - %2").arg( VERSION ).arg( PROG_NAME ) );
      if ( dlg->exec() != QDialog::Accepted ) {
        delete dlg;
        return 0;
      }else{
        int i;
        for(i=1; i<10; ++i)
          myArgV[i][0] = '\0';
        i=1;
        if( ! dlg->mImageName.isEmpty() ){
          strcpy( myArgV[i  ], "-i");
          strncpy(myArgV[i+1], dlg->mImageName.latin1(), 254);
          i+=2;
        }
        if( ! dlg->mSavePath.isEmpty() ){
          strcpy( myArgV[i  ], "-s");
          strncpy(myArgV[i+1], dlg->mSavePath.latin1(), 254);
          i+=2;
        }
        if( ! dlg->mH2iFileName.isEmpty() ){
          strcpy( myArgV[i  ], "-h2i");
          strncpy(myArgV[i+1], dlg->mH2iFileName.latin1(), 254);
          i+=2;
        }
        if( dlg->mUseMIME ){
          strcpy( myArgV[i], "-m");
          ++i;
        }
        if( ! dlg->mLogName.isEmpty() ){
          strcpy( myArgV[i  ], "-l");
          strncpy(myArgV[i+1], dlg->mLogName.latin1(), 254);
          i+=2;
        }
        myArgC = i;
        delete dlg;
      }
    }
  }while( bTryAgain );

  switch( nFound ){
    case DARES_OPEN_OK: {
        QMessageBox::information(a.desktop(), "Data Rescue",
          "Sorry,&nbsp;processing&nbsp;successfull&nbsp;but&nbsp;no&nbsp;blocks&nbsp;found.<br>"
          "&nbsp;<br>"
          "See&nbsp;console&nbsp;messages&nbsp;for&nbsp;information.");
        res = 0;
      }
      break;
    case DARES_OPEN_INVALID_PARAM:
      break;
    case DARES_OPEN_CANNOT_OPEN_IMAGE: {
        sError = "Could not open image for reading.";
      }
      break;
    case DARES_OPEN_CANNOT_MALLOC: {
        sError = "Could not open allocate memory for read buffer.";
      }
      break;
    case DARES_OPEN_CANNOT_OPEN_MAGIC: {
        sError = "Could not open magic database.";
      }
      break;
    default: {
        DaResView mainWidget( 0, 0, nFound );
        mainWidget.resize( 640, 480 );
        mainWidget.setCaption( QString("%1 - DAta REScue (C) 2005 c't").arg( VERSION ) );
        a.setMainWidget( &mainWidget );
        mainWidget.show();

        QObject::connect(qApp, SIGNAL(lastWindowClosed()), qApp, SLOT(quit()));
        res = a.exec();

        daresClose();
      }
  }
  if( DARES_OPEN_OK > nFound && ! sError.isEmpty() ){
    QMessageBox::critical(a.desktop(), "Data Rescue",
      QString( "<u>%1</u><br>"
               "&nbsp;<br>"
               "&nbsp;<br>"
               "Sorry,&nbsp;processing&nbsp;stopped&nbsp;due&nbsp;to&nbsp;library&nbsp;error:<br>"
               "&nbsp;<br>"
               "<b>%2</b>" ).arg( VERSION ).arg( sError ));
    res = -1;
  }

  return res;
}
