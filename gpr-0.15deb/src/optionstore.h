#ifndef OPTION_STORE_H
#define OPTION_STORE_H
#include <stdbool.h>
#include <gdkconfig.h>
#include <gtk/gtktreemodel.h>
#include <gtk/gtkwidget.h>
#include <ppd.h>
#include "printeroptions.h"

G_BEGIN_DECLS

#define TYPE_OPTION_STORE (option_store_get_type())
#define OPTION_STORE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_OPTION_STORE, OptionStore))
#define OPTION_STORE_CLASS(klass) (G_TYPE_CHECK_CLASS(CAST((klass),TYPE_OPTION_STORE, OptionStoreClass))

typedef struct _OptionStore OptionStore;
typedef struct _OptionStoreClass OptionStoreClass;


GType option_store_get_type (void) G_GNUC_CONST;
GtkTreeModel * option_store_new(PrinterOptions *po, struct PrinterOption *o, void (*contraint_callback)(char *));

void combo_box_mode_set(GtkWidget *, PrinterOptions *, struct PrinterOption *, void (*callback)(char *));

G_END_DECLS

#endif
