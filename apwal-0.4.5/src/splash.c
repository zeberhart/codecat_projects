/* splash.c
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#include "splash.h"
#include "common.h"

static gboolean splash_window_delete_event(GtkWidget *widget,
                                           GdkEvent *event,
                                           splash_t *splash);

// ----------------------------------------------------------------------------
splash_t* splash_new(struct apwalapp_t *apwal)
{
  splash_t *splash;
  GtkWidget *frame;
  GtkWidget *vbox;
  GtkWidget *label;
  splash = (splash_t *) malloc (sizeof(splash_t));
  g_assert(splash != NULL);

  splash->apwal = apwal;
  splash->showed = FALSE;
  splash->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  //gtk_window_set_type_hint(GTK_WINDOW(splash->window),
  //                         GDK_WINDOW_TYPE_HINT_DIALOG);
  g_signal_connect(G_OBJECT(splash->window), "delete_event",
                   G_CALLBACK(splash_window_delete_event), splash);
  //gtk_window_set_decorated(GTK_WINDOW(splash->window), FALSE);
  //is gtk_window_set_auto_startup_notification(FALSE) useful in our situation?

  frame = gtk_frame_new(NULL); //" Loading icons ");
  gtk_container_set_border_width(GTK_CONTAINER(frame), 8);
  //gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_NONE);
  gtk_container_add(GTK_CONTAINER(splash->window), frame);
  gtk_widget_show(frame);

  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 0);
  gtk_container_add(GTK_CONTAINER(frame), vbox);
  gtk_widget_show(vbox);

  label = gtk_label_new("Load icons, please wait...");
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);
  
  splash->progressbar = gtk_progress_bar_new();
  gtk_progress_bar_set_pulse_step(GTK_PROGRESS_BAR(splash->progressbar), 0.02);
  gtk_box_pack_start(GTK_BOX(vbox), splash->progressbar, FALSE, FALSE, 0);
  gtk_widget_show(splash->progressbar);

  splash->busycursor = gdk_cursor_new(GDK_WATCH);

  return splash;
}
// ----------------------------------------------------------------------------
void splash_show(splash_t *splash)
{
  splash->showed = TRUE;
  
  // busy cursor on
  // FIXME: gdk_window_set_cursor on splash window do NOT work
  gdk_window_set_cursor(splash->window->window, splash->busycursor);
  gdk_window_set_cursor(splash->apwal->window->window, splash->busycursor);
  gdk_window_set_cursor(splash->apwal->window2->window, splash->busycursor);
  //gdk_flush();
  
  // no grab because if iconsel is modal then it is not anymore after
  // hiding the splash screen. use sensitive to do the job
  gtk_widget_set_sensitive(splash->apwal->window, FALSE);
  gtk_widget_set_sensitive(splash->apwal->window2, FALSE);

  // choose the window on which the user is currently looking at.
  if ( ! GTK_WIDGET_VISIBLE(splash->apwal->window2))
    gtk_window_set_transient_for(GTK_WINDOW(splash->window),
                                 GTK_WINDOW(splash->apwal->window));
  else
    gtk_window_set_transient_for(GTK_WINDOW(splash->window),
                                 GTK_WINDOW(splash->apwal->window2));

  gtk_widget_show(splash->window);
  while (gtk_events_pending())
    gtk_main_iteration();
}
// ----------------------------------------------------------------------------
void splash_hide(splash_t *splash)
{
  splash->showed = FALSE;
  gtk_widget_hide(splash->window);
  gtk_widget_set_sensitive(splash->apwal->window, TRUE);
  gtk_widget_set_sensitive(splash->apwal->window2, TRUE);

  //busy cursor off
  gdk_window_set_cursor(splash->window->window, NULL);
  gdk_window_set_cursor(splash->apwal->window->window, NULL);
  gdk_window_set_cursor(splash->apwal->window2->window, NULL);

  while (gtk_events_pending())
    gtk_main_iteration();
}
// ----------------------------------------------------------------------------
void splash_update(splash_t *splash)
{
  if (splash->showed == TRUE)
  {
    gtk_progress_bar_pulse(GTK_PROGRESS_BAR(splash->progressbar));
    while (gtk_events_pending())
      gtk_main_iteration();
  }
}

// ----------------------------------------------------------------------------
static gboolean splash_window_delete_event(GtkWidget *widget,
                                           GdkEvent *event,
                                           splash_t *splash)
{
  return TRUE;/*TRUE to stop other handlers from being invoked for the event*/
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
