# Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
# See COPYING in top-level directory.

TLCFLAGS=$(TL_CPPFLAGS)
TLLDFLAGS=$(TL_LDFLAGS)
TLLIBADD=$(TL_LIB)

GTGCFLAGS=$(GTG_CPPFLAGS)
GTGLDFLAGS=$(GTG_LDFLAGS)
GTGDEPENDENCIES = $(GTG_DEPENDENCIES)
GTGLIBADD=$(GTG_LIB)

bin_PROGRAMS = eztrace_convert eztrace_avail eztrace_loaded eztrace_stats

generated_scripts = eztrace_create_plugin eztrace_plugin_generator eztrace.preload
bin_SCRIPTS = $(generated_scripts) function.pm input/Makefile.template eztrace_indent_fortran

lib_LTLIBRARIES = libeztrace.la \
		  libeztrace-autostart.la \
		  libeztrace-convert-core.la

eztrace_CPPFLAGS = -I$(top_srcdir)/src/pptrace/ $(TLCFLAGS) -g -O0 -DEZTRACE_LIB_DIR=\"${libdir}/\"  -DEZTRACE_ABS_TOP_BUILDDIR=\"${abs_top_builddir}/\"

eztrace_LDFLAGS = -g -O0 $(TLLDFLAGS)
#TODO: lbfd is not needed when litl is selected
eztrace_LDADD = -L${prefix}/lib -ldl $(TLLIBADD)

eztrace_SOURCES = ezt_demangle.c

if USE_PPTRACE

bin_PROGRAMS += eztrace
check_PROGRAMS = eztrace

eztrace_SOURCES +=eztrace.c \
		  ../pptrace/pptrace.c \
		  ../pptrace/binary.c \
		  ../pptrace/errors.c \
		  ../pptrace/hijack.c \
		  ../pptrace/isize.c \
		  ../pptrace/memory.c \
		  ../pptrace/opcodes.c \
		  ../pptrace/tracing.c

eztrace_CPPFLAGS += -DUSE_PPTRACE=1
endif

if HAVE_DEMANGLE
eztrace_CPPFLAGS += -DHAVE_DEMANGLE
eztrace_LDADD += -liberty
endif


if HAVE_LIBOPCODE
eztrace_LDFLAGS+=-lopcodes
endif				#HAVE_LIBOPCODE
#eztrace_LDADD += -lbfd -liberty -lz
eztrace_LDADD += -lbfd -lz

eztrace_avail_CPPFLAGS = $(TLCFLAGS)
eztrace_avail_LDFLAGS = $(TLLDFLAGS)
eztrace_avail_LDADD = -ldl libeztrace-convert-core.la
eztrace_avail_SOURCES = eztrace_avail.c


eztrace_loaded_CPPFLAGS = $(TLCFLAGS)
eztrace_loaded_LDFLAGS = $(TLLDFLAGS)
eztrace_loaded_LDADD = -ldl libeztrace-convert-core.la
eztrace_loaded_SOURCES = eztrace_loaded.c


eztrace_stats_CPPFLAGS = $(TLCFLAGS)
eztrace_stats_LDFLAGS = $(TLLDFLAGS)
eztrace_stats_LDADD = -ldl libeztrace-convert-core.la $(TLLIBADD)
eztrace_stats_SOURCES = eztrace_stats.c
eztrace_stats_DEPENDENCIES = libeztrace-convert-core.la


eztrace_convert_CPPFLAGS = $(GTGCFLAGS) $(TLCFLAGS)
eztrace_convert_LDFLAGS = $(GTGLDFLAGS) $(TLLDFLAGS)
eztrace_convert_LDADD = $(GTGLIBADD) -ldl libeztrace-convert-core.la $(TLLIBADD)
eztrace_convert_DEPENDENCIES = $(GTGDEPENDENCIES)  libeztrace-convert-core.la
eztrace_convert_SOURCES = eztrace_convert.c \
			  eztrace_convert.h \
			  eztrace_convert_types.h \
			  eztrace_types.h \
			  eztrace_convert_macros.h \
			  eztrace_litl_packed.h \
			  eztrace_litl.h \
			  ev_codes.h \
			  eztrace_hook.h

libeztrace_convert_core_la_CPPFLAGS = $(TLCFLAGS) $(GTGCFLAGS) -DEZTRACE_LIB_DIR=\"${libdir}\" -DDYNLIB_EXT=\"${DYNLIB_EXT}\"

libeztrace_convert_core_la_LIBADD = -lm -lpthread $(TLLIBADD) $(GTGLIBADD)
libeztrace_convert_core_la_LDFLAGS = --no-undefined $(GTGLDFLAGS) $(TLLDFLAGS)
libeztrace_convert_core_la_DEPENDENCIES = $(GTGDEPENDENCIES)
libeztrace_convert_core_la_SOURCES = eztrace_convert_core.c \
				     eztrace_stats_core.c \
				     eztrace_stats_core.h \
				     eztrace_hierarchical_array.c \
				     eztrace_hierarchical_array.h \
				     eztrace_array.c \
				     eztrace_array.h \
				     eztrace_types.h \
				     eztrace_convert_types.h \
				     eztrace_convert_macros.h \
				     eztrace_litl_packed.h \
				     eztrace_litl.h \
				     eztrace_convert.h \
				     submodules/ezt_submodule_convert.c \
				     submodules/ezt_submodule.h \
				     submodules/ezt_getcpu_convert.c \
				     submodules/ezt_getcpu.h \
				     ezt_demangle.c \
				     ezt_demangle.h

if HAVE_DEMANGLE
libeztrace_convert_core_la_CPPFLAGS += -DHAVE_DEMANGLE
libeztrace_convert_core_la_LIBADD += -liberty
endif


libeztrace_la_CPPFLAGS = $(TLCFLAGS)
libeztrace_la_LIBADD = -lm -lpthread $(TLLIBADD)
libeztrace_la_LDFLAGS = --no-undefined $(TLLDFLAGS)
libeztrace_la_SOURCES = eztrace_core.c \
			eztrace_sampling.c \
			pthread_core.c \
			submodules/ezt_submodule.c \
			submodules/ezt_submodule.h \
			submodules/ezt_getcpu.c \
			submodules/ezt_getcpu.h


libeztrace_autostart_la_CPPFLAGS = $(TLCFLAGS)
libeztrace_autostart_la_CFLAGS = -DEZTRACE_AUTOSTART
libeztrace_autostart_la_LIBADD = -lm -lpthread $(TLLIBADD)
libeztrace_autostart_la_LDFLAGS = --no-undefined $(TLLDFLAGS)
libeztrace_autostart_la_SOURCES = eztrace_core.c \
				  eztrace_sampling.c \
				  pthread_core.c \
				  submodules/ezt_submodule.c \
				  submodules/ezt_submodule.h \
				  submodules/ezt_getcpu.c \
				  submodules/ezt_getcpu.h

inputdir   = $(datadir)/eztrace
input_DATA = input/example.c.template\
	     input/example_ev_codes.h.template\
	     input/eztrace_convert_example.c.template \
	     input/Makefile.template

EXTRA_DIST = $(input_DATA) eztrace_create_plugin.in eztrace_plugin_generator function.pm eztrace.c.in  eztrace.preload.in eztrace_indent_fortran
CLEANFILES = $(generated_scripts) eztrace eztrace_config.h

include_HEADERS = eztrace.h\
		  eztrace_litl_packed.h \
		  eztrace_litl.h \
		  ../pptrace/pptrace.h \
		  eztrace_config.h \
		  ev_codes.h \
		  eztrace_list.h \
		  eztrace_stack.h \
		  eztrace_hook.h \
		  eztrace_convert.h\
		  eztrace_convert_core.h\
		  eztrace_convert_macros.h\
		  eztrace_sampling.h\
		  eztrace_types.h \
		  eztrace_convert_types.h
