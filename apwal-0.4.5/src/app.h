/* app.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#ifndef APP__H
#define APP__H

#include "common.h"

// ----------------------------------------------------------------------------
typedef struct app_t
{
  gchar *cmdline;
  gchar *path;
  gchar *icon;
  gint   x;
  gint   y;
} app_t;


// ----------------------------------------------------------------------------
typedef struct app_list_t
{
  GList *apps;
  GList *hamster;
} app_list_t;

#define TRACE_APP(app, format, args...) { g_assert(app != NULL); TRACE("[APP icon:%s, cmdline:%s, x:%d, y:%d]" format, app->icon, app->cmdline, app->x, app->y, ## args);}

// ----------------------------------------------------------------------------
app_t *      app_new(gchar *cmdline, gchar *path, gchar *icon, gint x, gint y);
app_t *      app_new_noname(gint x, gint y);
void         app_free(app_t **app);
app_t *      app_clone(app_t *app_orig);
gboolean     app_exec(app_t *app, gboolean exit_at_app_launch);
void         app_dump(app_t *app);
void         app_dumpln(app_t *app);
gboolean     app_is_executable(app_t *app);
gboolean     app_path_is_valid(app_t *app);
gboolean     app_icon_is_valid(app_t *app);
const char*  app_get_cmdline(app_t *app);
void         app_set_cmdline(app_t *app, const char *value);
const char*  app_get_path(app_t *app);
void         app_set_path(app_t *app, const char *value);
const char*  app_get_icon(app_t *app);
void         app_set_icon(app_t *app, const char *value);

// ----------------------------------------------------------------------------
app_list_t * app_list_new(void);
void         app_list_free(app_list_t **apps);
int          app_list_add(app_list_t *apps, app_t *app);
int          app_list_remove(app_list_t *apps, app_t *app);
app_t *      app_list_first(app_list_t *apps);
app_t *      app_list_next(app_list_t *apps);
app_t *      app_list_nth(app_list_t *apps, int nth);
int          app_list_length(app_list_t *apps);
void         app_list_dump(app_list_t *apps);
app_t *      app_list_find_noname(app_list_t *apps);

gint         app_list_delta_x(app_list_t *apps);
gint         app_list_delta_y(app_list_t *apps);
int          app_list_min_x(app_list_t *apps);
int          app_list_min_y(app_list_t *apps);
int          app_list_max_x(app_list_t *apps);
int          app_list_max_y(app_list_t *apps);
app_t *      app_list_at_xy(app_list_t *apps, gint x, gint y);
gboolean     app_list_xy_empty(app_list_t *apps, gint x, gint y);

#endif /*APP__H*/
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
