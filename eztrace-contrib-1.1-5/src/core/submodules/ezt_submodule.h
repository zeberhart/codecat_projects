/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright © CNRS, INRIA, Université Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */
#ifndef EZT_SUBMODULE_H
#define EZT_SUBMODULE_H

#include "eztrace.h"
#include "eztrace_convert_types.h"
#include "ev_codes.h"

#define EZT_SUBMODULE_EVENTS_ID SYSTEM_MODULE_ID(0x10)
#define EZT_SUBMODULE_PREFIX    GENERATE_SYSTEM_MODULE_PREFIX(EZT_SUBMODULE_EVENTS_ID)

struct ezt_submodule {
  void (*exec_init)();
  int  (*convert_init)();
  int  (*convert_handle)(eztrace_event_t* ev);
  int  (*convert_handle_stats)(eztrace_event_t* ev);
  void (*convert_print_stats)();
};


void ezt_submodule_exec_init();
void ezt_submodule_convert_init();
int  ezt_submodule_convert_handle(eztrace_event_t *ev);
int ezt_submodule_convert_handle_stats(eztrace_event_t *ev);
void ezt_submodule_convert_print_stats();


/* GETCPU submodule */
extern void ezt_getcpu_init();
extern int ezt_getcpu_convert_init();
extern int ezt_getcpu_convert_events(eztrace_event_t *ev);
extern int ezt_getcpu_convert_stats(eztrace_event_t *ev);
extern void ezt_getcpu_convert_print_stats();

#define EZT_GETCPU            (EZT_SUBMODULE_PREFIX | 0X0001)


#endif /* EZT_SUBMODULE_H */
