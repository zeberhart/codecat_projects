SHELL = /bin/sh

prefix = @prefix@
datadir = @datadir@/spectemu
exec_prefix = @exec_prefix@
bindir = @bindir@
mandir = @mandir@/man1
srcdir = @srcdir@

CC = @CC@
CPP = @CPP@
CFLAGS = @CFLAGS@
CPPFLAGS = @DEFS@ @CPPFLAGS@ @X_CFLAGS@ -DDATADIR=\""$(datadir)"\"
INSTALL = @INSTALL@
INSTALL_PROGRAM = @INSTALL_PROGRAM@
INSTALL_DATA = @INSTALL_DATA@
LDFLAGS = @LDFLAGS@ @X_LIBS@

vgalib = @vgalib@
xlibs = @xlibs@
rllibs = @rllibs@


.SUFFIXES:
.SUFFIXES: .c .o .s

.c.o:
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $<

.c.s:
	$(CC) -S $(CFLAGS) $(CPPFLAGS) $<

progs = @progs@

all: $(progs)

installdirs:
	$(SHELL) $(srcdir)/mkinstalldirs $(bindir) $(mandir) $(datadir)

install_prog: $(progs)
	if test -f xspect; then \
	   $(INSTALL_PROGRAM) -s xspect $(bindir); fi
	if test -f vgaspect; then \
	   $(INSTALL_PROGRAM) -s -m 4755 vgaspect $(bindir); fi

install_man:
	$(INSTALL_DATA) xspect.1 $(mandir)
	$(INSTALL_DATA) tapeout.1 $(mandir)
	(cd $(mandir); rm -f vgaspect.1; ln -s xspect.1 vgaspect.1)

install_data:
	$(INSTALL_DATA) spectkey.gif $(datadir)
	$(INSTALL_DATA) specsinc.xpm $(datadir)
	$(INSTALL_DATA) example.cfg $(datadir)
	if test ! -f $(datadir)/spectemu.cfg; then \
	   $(INSTALL_DATA) spectemu.cfg $(datadir); fi

install: installdirs install_prog install_man install_data

z80_c_objs=z80.o z80optab.o z80_step.o spperif.o spect.o rom_imag.o \
           z80_op1.o z80_op2.o z80_op3.o z80_op4.o z80_op5.o z80_op6.o

z80_i386_objs=z80.o spperif.o rom_imag.o i386emul.o

spect_objs=spmain.o spscr.o spkey.o spsound.o sptape.o tapefile.o \
           snapshot.o compr.o sptiming.o interf.o misc.o spconf.o \
           loadim.o keynames.o $(@z80objs@)

xspect_objs=xspect.o xscr.o xutils.o xkey.o \
            ax.o spectkey.o xdispkb.o $(spect_objs)

xspect: $(xspect_objs)
	$(CC) -o xspect $(LDFLAGS) $(xspect_objs) $(xlibs) $(rllibs)

vgaspect_objs=vgaspect.o vgascr.o vgakey.o  $(spect_objs)
vgaspect: $(vgaspect_objs)
	$(CC) -o vgaspect $(LDFLAGS) $(vgaspect_objs) $(vgalib) $(rllibs)


tapeout.o: tapeout.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) tapeout.c

tapeout_objs=tapeout.o tapefile.o misc.o
tapeout: $(tapeout_objs)
	$(CC) -o tapeout $(LDFLAGS) $(tapeout_objs)

i386emul.sp: i386step.S
	$(CPP) $(CPPFLAGS) i386step.S > i386emul.sp

i386emul.s: i386emul.sp sp_to_s
	$(srcdir)/sp_to_s < i386emul.sp > i386emul.s

i386emul.o: i386emul.s
	$(CC) -c $(CFLAGS) i386emul.s

rom_imag.c: rom bin_to_c
	$(srcdir)/bin_to_c rom rom_imag

loadim.c: loadim.z80 bin_to_c
	$(srcdir)/bin_to_c loadim.z80 loadim

spectkey.c: spectkey.srl bin_to_c
	$(srcdir)/bin_to_c spectkey.srl spectkey

sp_to_s: sp_to_s.o
	$(CC) -o sp_to_s $(LDFLAGS) sp_to_s.o

bin_to_c: bin_to_c.o
	$(CC) -o bin_to_c $(LDFLAGS) bin_to_c.o

distclean:
	rm -f *~ *.o *.sp z80*.s i386emul.s bin_to_c sp_to_s
	rm -f spectkey.c rom_imag.c loadim.c
	rm -f Makefile.old gmon.out

clean: distclean
	rm -f xspect vgaspect tapeout

realclean: clean
	rm -f config.status config.cache config.log
	rm -f Makefile config.h

depend:
	cp -f Makefile Makefile.old
	sed '/^# DO NOT REMOVE THIS LINE/q' < Makefile.old > Makefile
	gcc -MM $(CPPFLAGS) *.c >> Makefile


i386emul.sp: i386def.S i386sp.S i386step.S 
i386emul.sp: i386op1.S i386op1x.S i386op2.S i386op2x.S
i386emul.sp: i386op3.S i386op3x.S i386op4.S
i386emul.sp: i386op5.S i386op6.S 

FORCE:

# DO NOT REMOVE THIS LINE, OR "make depend" WILL NOT WORK
