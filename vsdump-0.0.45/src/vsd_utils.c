/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * vsdump: test program to dump and parse content of vsd file
 *
 * Copyright (C) 2006-2007	Valek Filippov (frob@df.ru)
 * Copyright (C) 2002-2006	Jody Goldberg (jody@gnome.org)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 3 or later of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 */

#include <gsf/gsf-input-stdio.h>
#include <gsf/gsf-infile-zip.h>
#include <gsf/gsf-infile-msole.h>
#include <gsf/gsf-infile.h>
#include "vsdump.h"
#include "vsd_utils.h"

FILE *
vsd_openfd (char *dirname, char *filename) 
{
	char	*fullname;
	FILE	*file;

	fullname = g_build_path("/", dirname, filename, NULL);
	file = fopen(fullname, "w");
	g_free(fullname);

	return file;
}

/* ------------------------------------------------------------------ */

static GsfInfile *
open_archive (char const *filename)
{
	GsfInfile *infile;
	GError *error = NULL;	
	GsfInput *src;
	char *display_name;

	src = gsf_input_stdio_new (filename, &error);
	if (error) {
		display_name = g_filename_display_name (filename);
		g_printerr ("%s: Failed to open %s: %s\n",
			    g_get_prgname (),
			    display_name,
			    error->message);
		g_free (display_name);
		return NULL;
	}

	infile = gsf_infile_zip_new (src, NULL);
	if (infile)
		return infile;

	infile = gsf_infile_msole_new (src, NULL);
	if (infile)
		return infile;

	display_name = g_filename_display_name (filename);
	g_printerr ("%s: Failed to recognize %s as an archive\n",
		    g_get_prgname (),
		    display_name);
	g_free (display_name);
	g_free(error);	
	return NULL;
}

/* ------------------------------------------------------------------------- */

static GsfInput *
find_member (GsfInfile *arch, char const *name)
{
	char const *slash = strchr (name, '/');

	if (slash) {
		char *dirname = g_strndup (name, slash - name);
		GsfInput *member;
		GsfInfile *dir;

		member = gsf_infile_child_by_name (arch, dirname);
		g_free (dirname);
		if (!member)
			return NULL;
		dir = GSF_INFILE (member);
		member = find_member (dir, slash + 1);
		g_object_unref (dir);
		return member;
	} else {
		return gsf_infile_child_by_name (arch, name);
	}
}

/* ------------------------------------------------------------------------- */
/**
 * vsd_get_input:
 * Takes filename, returns 'VisioDocument' as a GsfInput.
 *
 **/

GsfInput *
vsd_get_input (char const *filename)
{
	GsfInfile *infile;
	GsfInput *member;

	infile = open_archive (filename);
	if (!infile)
		return NULL;

	member = find_member (infile, "VisioDocument");
		if (!member) {
			g_warning ("failed to find VisioDocument");
			return NULL;
		}
	return member;
}
