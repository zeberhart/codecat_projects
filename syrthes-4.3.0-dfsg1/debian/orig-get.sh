#!/bin/bash
# Download SYRTHES upstream archive and apply the script debian/orig-tar.sh
# the same way uscan would have done:
# debian/orig-tar.sh --upstream-version <version> <archive>
set -e

# The upstream release to download
UPSTREAM_RELEASE=${UPSTREAM_RELEASE:-4.3.0}

# Number of parts
UPSTREAM_PARTS=${UPSTREAM_PARTS:-6}

# Upstream tarball name
UPSTREAM_TARBALL="../syrthes-${UPSTREAM_RELEASE}.tar.gz"

[ ! -f "$UPSTREAM_TARBALL" ] || {
  echo "'$UPSTREAM_TARBALL' already exists!"
  exit 1
}

# Temporary directory and associated cleanup
unset tmpdir
cleanup () {
  [ -z "$tmpdir" ] || rm -fr "$tmpdir"
}
trap 'cleanup' EXIT ABRT TERM INT
tmpdir=$(mktemp -d)

# Download parts into $tmpdir
for part in $(seq -w 00 $((UPSTREAM_PARTS-1))); do
  wget -c -O- \
    "http://chercheurs.edf.com/fichiers/fckeditor/Commun/Innovation/logiciels/syrthes/syrthes${UPSTREAM_RELEASE}.tgz.part${part}.zip" | 
  funzip >>"$tmpdir/tarball"
done
# Get the tarball
mv "$tmpdir/tarball" "$UPSTREAM_TARBALL"

# Apply DFSG
./debian/orig-tar.sh --upstream-version "$UPSTREAM_RELEASE" "$UPSTREAM_TARBALL"
