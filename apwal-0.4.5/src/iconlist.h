/* iconlist.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#ifndef ICONLIST__H
#define ICONLIST__H

#include "common.h"

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
typedef struct icodir_t
{
  gchar           *path;
  gint             count_total;         // number of ico_t linked to this dir
  gint             count_selected;      // number of ico_t linked and selected
  gboolean         recursive;           // recursive
  gboolean         selected;            //
  struct icodir_t *parent;              //
  GList           *children;            // icodir_t children (NOT ico_t)
} icodir_t;

icodir_t* icodir_new      (gchar *dirname, gboolean sel, gboolean rec);
icodir_t* icodir_new_child(gchar *dirname, gboolean sel, gboolean rec,
                           icodir_t *parent);
void      icodir_free     (icodir_t *icodir);
gint      icodir_compare  (icodir_t *a, icodir_t *b);
gint      icodir_count_total_rec   (icodir_t *dir);
gint      icodir_count_selected_rec(icodir_t *dir);

icodir_t *icodir_list_find(GList *dirs, char *dirname);
GList    *icodir_list_find_with_parent(GList *dirs, icodir_t *parentdir);

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
typedef struct ico_t
{
  gchar     *name;
  icodir_t  *dir;
  GdkPixbuf *pixbuf;
  gint       width;
  gint       height;
  gboolean   selected;
} ico_t;

ico_t* ico_new(icodir_t *dir, gchar *iconame);
void   ico_free(ico_t *ico);
gint   ico_compare_by_name(ico_t *a, ico_t *b);
gint   ico_compare_by_path(ico_t *a, ico_t *b);
char * ico_get_full_filename(ico_t *ico);
ico_t* ico_list_find_icon(GList *icos, icodir_t *dir, char *ico_name);
struct icon_list_t;
ico_t* ico_list_try_select_icon(icodir_t *dir, char *ico_name,
                                struct icon_list_t *icons);
void    ico_list_append(GList **icos, icodir_t *dir, struct icon_list_t *icons);
void    ico_list_remove(GList **icos, icodir_t *dir, struct icon_list_t *icons);
void    ico_list_sort(GList **icos, gint sortmode);

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#define ICONSEL_SORT_NAME    1
#define ICONSEL_SORT_PATH    2
#define ICONSEL_SORT_LAST    2
#define ICONSEL_SORT_DEFAULT 1
typedef struct icon_list_t
{
  struct icon_selection_t *iconsel;
  GList     *dirs;
  GList     *icos;
  gint       sortmode;
  GdkPixbuf *pixbuf;
  ico_t     *selected_icon;
  gint       icon_per_row;
} icon_list_t;

icon_list_t * icon_list_new             (struct icon_selection_t *iconsel);
void          icon_list_free            (icon_list_t *icons);
void          icon_list_set_width       (icon_list_t *icons, int icon_per_row);
void          icon_list_fork            (icon_list_t *icons, icodir_t *dir, gboolean selected, gboolean recursive);
void          icon_list_append_children (icon_list_t *icons, icodir_t *dir); 
void          icon_list_append          (icon_list_t *icons, char *dirname, gboolean selected, gboolean recursive);
void          icon_list_reload          (icon_list_t *icons, char *dirname);
void          icon_list_reload_all            (icon_list_t *icons);
void          icon_list_reload_all_with_splash(icon_list_t *icons);
void          icon_list_remove_children (icon_list_t *icons, icodir_t *dir); 
void          icon_list_remove          (icon_list_t *icons, char *dirname);
void          icon_list_modify            (icon_list_t *icons, char *dirname, char *newdirname);
void          icon_list_modify_with_splash(icon_list_t *icons, char *dirname, char *newdirname);
void          icon_list_set_selected_children(icon_list_t *icons, icodir_t *dir, gboolean selected);
void          icon_list_set_selected    (icon_list_t *icons, char *dirname, gboolean selected);
void          icon_list_set_selected_icon(icon_list_t *icons, ico_t *ico);
void          icon_list_set_recursive   (icon_list_t *icons, char *dirname, gboolean recursive);
void          icon_list_set_recursive_with_splash   (icon_list_t *icons, char *dirname, gboolean recursive);
void          icon_list_select          (icon_list_t *icons, gboolean (*select_func)(ico_t *dir, void *data), void *data);

ico_t       * icon_list_find            (icon_list_t *icons, char *path, char *name);
icodir_t    * icon_list_find_path       (icon_list_t *icons, char *path);

void          icon_list_set_sortmode    (icon_list_t *icons, int sortmode);
gint          icon_list_selected_count  (icon_list_t *icons);
ico_t       * icon_list_selected_nth    (icon_list_t *icons, int nth);

GdkPixbuf   * icon_list_pixbuf(icon_list_t *icons);
void          icon_list_set_selected_icon_from_xy(icon_list_t *icons, int x, int y);
void          icon_list_set_selected_icon_from_filename(icon_list_t *icons, const char *file);
ico_t       * icon_list_get_selected_icon(icon_list_t *icons);
gchar       * icon_list_get_selected_icon_filename(icon_list_t *icons);
void          icon_list_print_not_selected(icon_list_t *icons); //debug stuff

#endif /*ICONLIST__H*/
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
