LANGUAGE = C++
HEADERS += *.h
SOURCES += *.cpp
RESOURCES += c2blib.qrc
DEFINES += C2B_USE_QMAKE
INCLUDEPATH += .
QT = core network
win32 {
    TARGET = ../../c2b
} else {
    TARGET = ../c2b
}
CONFIG -= dll
CONFIG += staticlib
VERSION = 0.0.0
TEMPLATE = lib
QMAKE_CXXFLAGS_DEBUG += -DC2B_DEBUG
CONFIG += use_lzo
use_lzo {
    DEFINES += C2B_USE_LZO
}
disable_lzo {
    CONFIG -= use_lzo
    DEFINES -= C2B_USE_LZO
}
