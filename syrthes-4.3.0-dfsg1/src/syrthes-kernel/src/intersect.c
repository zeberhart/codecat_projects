/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <math.h>
# include <stdlib.h>

# include "syr_abs.h"
# include "syr_bd.h"
# include "syr_tree.h"
# include "syr_proto.h"


/* # include "mpi.h" */

double epsi=1.e-6;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | seg_rectanx                                                          |
  |         tester l'intersection entre un segment et un segment x=cte   |
  |======================================================================| */
int seg_rectanx(double dx,double dy,double xa,double xb,double ya,double yb)
{
    double t,d,y;

    d=xb-xa;
    if (fabs(d)<epsi)
      return(0);
    else
      {
        t=(dx-xa)/d; y=ya+t*(yb-ya);
	if (t>-epsi && -dy<=y && y<=dy)
          return(1);
        else 
	  return(0);
      }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | seg_rectany                                                          |
  |         tester l'intersection entre un segment et un segment y=cte   |
  |======================================================================| */
int seg_rectany(double dx,double dy,double xa,double xb,double ya,double yb)
{
    double t,d,x;

    d=yb-ya;
    if (fabs(d)<epsi)
      return(0);
    else
      {
        t=(dy-ya)/d; x=xa+t*(xb-xa);
	if (t>-epsi && -dx<=x && x<=dx)
          return(1);
        else 
	  return(0);
      }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | seg_cubex                                                            |
  |         tester l'intersection entre une face x=cte                   |
  |======================================================================| */
int seg_cubex(double dx,double dy,double dz,
	      double xa,double xb,double ya,double yb,double za,double zb)
{
    double t,d,y,z;

    d=xb-xa;
    if (fabs(d)<epsi)
      return(0);
    else
      {
        t=(dx-xa)/d; y=ya+t*(yb-ya);
	if (t>-epsi && -dy<=y && y<=dy)
          {
	     z= za+t*(zb-za);
	     if (-dz<=z && z<=dz)
		 return(1);
	     else
		 return(0);
	  }
	else
	  return(0);
      }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | seg_cubey                                                            |
  |         tester l'intersection entre une face y=cte                   |
  |======================================================================| */
int seg_cubey(double dx,double dy,double dz,
	      double xa,double xb,double ya,double yb,double za,double zb)
{
    double t,d,x,z;

    d=yb-ya;
    if (fabs(d)<epsi)
      return(0);
    else
      {
        t=(dy-ya)/d; x=xa+t*(xb-xa);
	if (t>-epsi && -dx<=x && x<=dx)
          {
	     z= za+t*(zb-za);
	     if (-dz<=z && z<=dz)
		 return(1);
	     else
		 return(0);
	  }
	else
	  return(0);
      }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | seg_cubez                                                            |
  |         tester l'intersection entre une face z=cte                   |
  |======================================================================| */
int seg_cubez(double dx,double dy,double dz,
	      double xa,double xb,double ya,double yb,double za,double zb)
{
    double t,d,x,y;

    d=zb-za;
    if (fabs(d)<epsi)
      return(0);
    else
      {
        t=(dz-za)/d; y=ya+t*(yb-ya);
	if (t>-epsi && -dy<=y && y<=dy)
          {
	     x= xa+t*(xb-xa);
	     if (-dx<=x && x<=dx)
		 return(1);
	     else
		 return(0);
	  }
	else
	  return(0);
      }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | diag_tria                                                            |
  |         intersection d'une diagonale de cube avec un plan            |
  |======================================================================| */
int diag_tria(double ta, double tb, double tc, double td, 
	      double dx, double dy, double dz,double *t)
{
    double d;

    d=ta*dx+tb*dy+tc*dz;

    if (fabs(d)<epsi)
      return(0);
    else
      {
	*t= -td/d;
	if (-1<=*t && *t<=1) 
	  return(1);
	else
	  return(0);
      }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | in_triangle                                                          |
  |         voir si in point est dans un triangle                        |
  |======================================================================| */
int in_triangle (double a,  double b,  double c, double d,
                 double xa, double ya, double za,
                 double xab,double yab,double zab,
                 double xac,double yac,double zac,
                 double xp, double yp, double zp)
{
    int i1;
    double coo_min,alpha,beta,u0,u1,u2,v0,v1,v2;


    i1 = 1; coo_min=fabs(a);
    if (fabs(b)>coo_min) {i1=2;coo_min=fabs(b);}
    if (fabs(c)>coo_min) {i1=3;}

    switch (i1)
        {
        case 1 : u0=yp-ya; u1=yab; u2=yac;
                 v0=zp-za; v1=zab; v2=zac;
                 break;

        case 2 : u0=xp-xa; u1=xab; u2=xac;
                 v0=zp-za; v1=zab; v2=zac;
                 break;

        case 3 : u0=xp-xa; u1=xab; u2=xac;
                 v0=yp-ya; v1=yab; v2=yac;
                 break;
        }

    alpha = beta = -1;
    if (fabs(u1)<epsi)
        {
            beta = u0/u2;
            if (fabs(beta)<epsi) beta=0.;
            else if (fabs(beta-1.)<epsi) beta=1.;
            if (beta>=0 && beta<=1) alpha = (v0-beta*v2)/v1;
        }
    else
        {
            beta = (v0*u1-u0*v1)/(v2*u1-u2*v1);
            if (fabs(beta)<epsi) beta=0.;
            else if (fabs(beta-1.)<epsi) beta=1.;
            if (beta>=0 && beta<=1) alpha = (u0-beta*u2)/u1;
        }

/*  printf(" >> in_triangle: alpha, beta = %f %f\n", alpha,beta);   */
    if (alpha>=-epsi  && beta>=-epsi && (alpha+beta)<=1.+ 2.*epsi)
        return(1);
    else
        return(0);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | in_seg                                                               |
  |         voir si in point est dans un segment                         |
  |======================================================================| */
int in_seg (double xa, double ya, double xb, double yb,
	    double xp, double yp)
{
    double t;

    if (fabs(xb-xa)<epsi)
      t=(yp-ya)/(yb-ya);
    else
      t=(xp-xa)/(xb-xa);

    if (0<=t && t<=1)
      return(1);
    else
      return(0);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | in_rectan                                                            |
  |         voir si in point est dans la boite englobante                |
  |======================================================================| */
int in_rectan (double xv, double yv, double xmin,double xmax,
	       double ymin,double ymax)
{

    if (   xv>xmin-epsi && xv<xmax+epsi
        && yv>ymin-epsi && yv<ymax+epsi)
        return(1);
    else
        return(0);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | in_boite                                                             |
  |         voir si in point est dans la boite englobante                |
  |======================================================================| */
int in_boite (double xv, double yv, double zv, double xmin,double xmax,
              double ymin,double ymax, double zmin,double zmax)
{

    if (   xv>xmin-epsi && xv<xmax+epsi
        && yv>ymin-epsi && yv<ymax+epsi
        && zv>zmin-epsi && zv<zmax+epsi )
        return(1);
    else
        return(0);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | in_tria_2d                                                           |
  |         voir si in point est dans un triangle en dimension 2         |
  |======================================================================| */
int in_tria_2d (double xa,double ya,double xb,double yb,double xc,double yc,
		double x,double y,double epsi)
{
  double alpha,beta,u0,u1,u2,v0,v1,v2;
  
  
  u0=x-xa; u1=xb-xa; u2=xc-xa;
  v0=y-ya; v1=yb-ya; v2=yc-ya;
  
  alpha = beta = -1;
  if (fabs(u1)<epsi)
    {
      beta = u0/u2;
      if (fabs(beta)<epsi) beta=0.;
      else if (fabs(beta-1.)<epsi) beta=1.;
      if (beta>=0 && beta<=1) alpha = (v0-beta*v2)/v1;
    }
  else
    {
      beta = (v0*u1-u0*v1)/(v2*u1-u2*v1);
      if (fabs(beta)<epsi) beta=0.;
      else if (fabs(beta-1.)<epsi) beta=1.;
      if (beta>=0 && beta<=1) alpha = (u0-beta*u2)/u1;
    }
  
  if (alpha>=-epsi  && beta>=-epsi && (alpha+beta)<=1.+ 2.*epsi)
    return(1);
  else
    return(0);
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | in_tetra                                                             |
  |         voir si in point est dans un tetraedre                       |
  |======================================================================| */
int in_tetra2 (double xa,double ya,double za,double xb,double yb,double zb,
	       double xc,double yc,double zc,double xd,double yd,double zd,
	       double x,double y,double z,double epsi)
{
  double xn,xab,yab,zab,xac,yac,zac,xbc,ybc,zbc;
  double xbd,ybd,zbd,xad,yad,zad;
  double a1,b1,c1,d1,a2,b2,c2,d2,a3,b3,c3,d3,a4,b4,c4,d4;
  
  epsi = 1.E-8; /* essai chris , car probleme de precision eventuel */
  xab=xb-xa; yab=yb-ya; zab=zb-za;
  xac=xc-xa; yac=yc-ya; zac=zc-za;
  xad=xd-xa; yad=yd-ya; zad=zd-za;
  xbc=xc-xb; ybc=yc-yb; zbc=zc-zb;
  xbd=xd-xb; ybd=yd-yb; zbd=zd-zb;
  
  
  /* eq des 4 plans */
  a1=yab*zac-zab*yac; b1=-xab*zac+zab*xac; c1=xab*yac-yab*xac; d1=-(a1*xa+b1*ya+c1*za);
  if (a1*xd+b1*yd+c1*zd+d1>0) {a1=-a1;b1=-b1;c1=-c1;d1=-d1;}
  xn=sqrt(a1*a1+b1*b1+c1*c1); a1/=xn;  b1/=xn;  c1/=xn;  d1/=xn; 
  
  a2=yab*zad-zab*yad; b2=-xab*zad+zab*xad; c2=xab*yad-yab*xad; d2=-(a2*xa+b2*ya+c2*za);
  if (a2*xc+b2*yc+c2*zc+d2>0) {a2=-a2;b2=-b2;c2=-c2;d2=-d2;}
  xn=sqrt(a2*a2+b2*b2+c2*c2); a2/=xn;  b2/=xn;  c2/=xn;  d2/=xn; 
  
  a3=ybc*zbd-zbc*ybd; b3=-xbc*zbd+zbc*xbd; c3=xbc*ybd-ybc*xbd; d3=-(a3*xb+b3*yb+c3*zb);
  if (a3*xa+b3*ya+c3*za+d3>0) {a3=-a3;b3=-b3;c3=-c3;d3=-d3;}
  xn=sqrt(a3*a3+b3*b3+c3*c3); a3/=xn;  b3/=xn;  c3/=xn;  d3/=xn; 
  
  a4=yac*zad-zac*yad; b4=-xac*zad+zac*xad; c4=xac*yad-yac*xad; d4=-(a4*xa+b4*ya+c4*za);
  if (a4*xb+b4*yb+c4*zb+d4>0) {a4=-a4;b4=-b4;c4=-c4;d4=-d4;}
  xn=sqrt(a4*a4+b4*b4+c4*c4); a4/=xn;  b4/=xn;  c4/=xn;  d4/=xn; 
  
  /*	printf("el=%d  P1 %f P2 %f P3 %f P4 %f\n",numel,
    a1*x + b1*y+ c1*z + d1, a2*x + b2*y+ c2*z + d2,
    a3*x + b3*y+ c3*z + d3, a4*x + b4*y+ c4*z + d4); */
  
  if (a1*x + b1*y+ c1*z + d1<epsi &&
      a2*x + b2*y+ c2*z + d2<epsi &&
      a3*x + b3*y+ c3*z + d3<epsi &&
      a4*x + b4*y+ c4*z + d4<epsi)
    return(1);
  else
    return(0);
}
