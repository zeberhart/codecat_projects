
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "unistd.h"
#include <stdlib.h>
#include <string.h>

#ifdef USE_GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#include <popt.h>
#endif

/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  define Q_(String) g_strip_context ((String), gettext (String))
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define Q_(String) g_strip_context ((String), (String))
#  define N_(String) (String)
#endif



int main(int argc, char **argv)
{
 GtkWidget *gpr_main_window;
  GtkWidget *vbox1;
  GtkWidget *notebook1;

  /* Notebook tab 1 widgets */
  GtkWidget *vbox2;
  GtkWidget *hbox1;
  GtkWidget *printfile_label;
  GtkWidget *printfile_fileentry;
  GtkWidget *hseparator1;
  GtkWidget *hbox2;

#ifdef ENABLE_NLS
  bindtextdomain(PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain(PACKAGE);
#endif

  if(! (gtk_init_check(&argc, &argv))) 
    { g_error(_("error initializing GTK")) ;     return 1; }

  /* create main window */
  gpr_main_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data(GTK_OBJECT(gpr_main_window), "gpr_main_window",
		      gpr_main_window);


  printfile_fileentry =  gtk_file_chooser_button_new (_("Choose a file"), GTK_FILE_CHOOSER_ACTION_OPEN); 

  gtk_widget_show(printfile_fileentry);

  gtk_container_add(GTK_CONTAINER(gpr_main_window), printfile_fileentry);

  //  gtk_box_pack_start(GTK_BOX(gpr_main_win), printfile_fileentry, TRUE, FALSE, 0);

  gtk_widget_show(gpr_main_window);

  gtk_main();
 

  return 0;
}
