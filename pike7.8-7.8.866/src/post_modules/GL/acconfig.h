/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: ceb1c76e1f1480455ed5a006785ecf5f2765c990 $
*/

/* Define if you have libGL */
#undef HAVE_LIBGL

/* Define if you have libOpenGL */
#undef HAVE_LIBOPENGL

/* Define if you have libOpenGL32 */
#undef HAVE_LIBOPENGL32

/* Define if you have libMesaGL */
#undef HAVE_LIBMESAGL

/* Define if you have GL */
#undef HAVE_GL
