/* filesel.c
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#include <stdio.h>      /* define FILENAME_MAX */
#include "common.h"

typedef struct filesel_t
{
  GtkWidget  *filechooser;
  char       *old_value;
  void       *user_data;
  void      (*compute_result) (const char *old_value, const char *new_value,
                               void *data);
} filesel_t;

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
struct filesel_t* filesel_new(void)
{
  filesel_t *filesel;
  filesel = (filesel_t *) malloc(sizeof(filesel_t));
  g_assert(filesel != NULL);
  filesel->filechooser = gtk_file_chooser_dialog_new("Directory chooser",
      NULL/*parent_window*/, GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);
  gtk_widget_hide(filesel->filechooser);

  filesel->old_value = NULL;
  return filesel;
}
// ----------------------------------------------------------------------------
void filesel_select(
         struct filesel_t *filesel, const char *old_value, gboolean dir_only,
         void (*compute_result) (const char *old_value, const char *new_value,
                                 void *data),
         void *data)
{
  char *new_value;
  char *absolute_path = NULL;
  g_assert(filesel != NULL);
  g_assert(compute_result != NULL);
  g_assert(old_value != NULL);
  filesel->compute_result = compute_result;
  filesel->user_data = data;

  if (!g_file_test(old_value, G_FILE_TEST_IS_EXECUTABLE))
    absolute_path = g_find_program_in_path(old_value);
  
  if (filesel->old_value != NULL)
    g_free(filesel->old_value);
  if (absolute_path == NULL)
    filesel->old_value = g_strdup(old_value);
  else
    filesel->old_value = absolute_path;

  if (dir_only)
  {
    gtk_file_chooser_set_action(GTK_FILE_CHOOSER(filesel->filechooser),
                                GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);
    gtk_file_chooser_unselect_all(GTK_FILE_CHOOSER(filesel->filechooser));
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(filesel->filechooser),
                                        old_value);
  }
  else
  {
    gtk_file_chooser_set_action(GTK_FILE_CHOOSER(filesel->filechooser),
                                GTK_FILE_CHOOSER_ACTION_OPEN);
    gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(filesel->filechooser),
                                  old_value);
  }


  if (gtk_dialog_run(GTK_DIALOG(filesel->filechooser)) == GTK_RESPONSE_ACCEPT)
  {
    new_value = gtk_file_chooser_get_filename(
                                      GTK_FILE_CHOOSER(filesel->filechooser));
    filesel->compute_result(filesel->old_value, new_value, filesel->user_data);
    if (filesel->old_value != NULL)
    {
      g_free(filesel->old_value);
      filesel->old_value = NULL;
    }
  }
  else  // cancel
  {
    filesel->compute_result(filesel->old_value, NULL, filesel->user_data);
    if (filesel->old_value != NULL)
    {
      g_free(filesel->old_value);
      filesel->old_value = NULL;
    }
  }
  gtk_widget_hide(filesel->filechooser);
  
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
