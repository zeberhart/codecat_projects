/* apwalapp.c
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#include <string.h>     /* memset */
#include <stdio.h>      /* define FILENAME_MAX */


#include "common.h"
#include <gdk/gdkkeysyms.h>

static void apwalapp_build_interface(apwalapp_t *apwal);
static void apwalapp_clicked_ok(GtkWidget *widget, apwalapp_t *apwal);
static void apwalapp_clicked_cancel(GtkWidget *widget, apwalapp_t *apwal);
static void apwalapp_w2_close_clicked(GtkWidget *widget, apwalapp_t *apwal);
static void apwalapp_w2_apply_clicked(GtkWidget *widget, apwalapp_t *apwal);
static void apwalapp_w2_cancel_clicked(GtkWidget *widget, apwalapp_t *apwal);
static void apwalapp_w2_ok_clicked(GtkWidget *widget, apwalapp_t *apwal);
static gboolean apwalapp_w2_delete(GtkWidget *widget, GdkEvent *event, apwalapp_t *apwal);
static void apwalapp_switch_page(GtkWidget *widget, GtkNotebookPage *page,
                                 guint page_num, apwalapp_t *apwal);
static void apwalapp_switch_page2(GtkWidget *widget, GtkNotebookPage *page,
                                  guint page_num, apwalapp_t *apwal);
void apwal_load_apps(apwalapp_t *apwal, app_list_t *apps);
//static gboolean apwalapp_window_key_release_event(GtkWidget *widget,
//                                       GdkEventKey *event, apwalapp_t *apwal);
static void apwalapp_window_move_focus_event(GtkWindow *window,
                                        GtkDirectionType direction,
                                        apwalapp_t *apwal);
                                               

// ----------------------------------------------------------------------------
apwalapp_t* apwalapp_new(void)
{
  apwalapp_t  *apwal;
  GdkGeometry  hints;
  gint         width, height;

  apwal = (apwalapp_t *)malloc(sizeof(apwalapp_t));
  g_assert(apwal != NULL);
  memset(apwal, '\0', sizeof(apwalapp_t));

  xmlrc_load_from_file(&apwal->apps, &apwal->iconsel_pref, &apwal->apwal_pref);
  apwal->selected_app = NULL;
  
  apwalapp_build_interface(apwal);

  // append objects to this interface
  apwal->splash = splash_new(apwal);
  apwal->prop = property_new(apwal);
  apwal->editor = editor_new(apwal);
  apwal->iconsel = icon_selection_new(apwal);
  apwal->filesel = filesel_new();
  apwal_pref_build_interface(apwal);
  apwal->about = about_new(apwal);

  // window set size and show
  gtk_window_get_size(GTK_WINDOW(apwal->window), &width, &height);
  hints.base_width = width;
  hints.base_height = height;
  hints.min_width = width;
  hints.min_height = height;
  hints.max_width = width;
  hints.max_height = height;
  gtk_window_set_geometry_hints(GTK_WINDOW(apwal->window),
                                apwal->window, &hints, 
                                GDK_HINT_MIN_SIZE | GDK_HINT_MAX_SIZE |
                                GDK_HINT_RESIZE_INC |GDK_HINT_BASE_SIZE);
  gtk_widget_show(apwal->window);

  // window2: set the min size to something useable
  gtk_window_get_size(GTK_WINDOW(apwal->window), &width, &height);
  hints.min_width = 400;
  hints.min_height = height;
  gtk_window_set_geometry_hints(GTK_WINDOW(apwal->window2),
                                apwal->window2, &hints, 
                                GDK_HINT_MIN_SIZE );

  if (apwal->apwal_pref->iconsel_in_a_separate_window == TRUE &&
                     apwal->apwal_pref->iconsel_modal == FALSE)
    gtk_widget_show(apwal->window2);

  icon_selection_load_icons_with_splash(apwal->iconsel);
  iconsel_refresh(apwal->iconsel);
  // ./!\.IMPORTANT./!\.
  // because of a problem with the scrolled window in iconsel.c
  // gtk_widget_show_all has to be called to show the scrolled window.
  gtk_widget_show_all(apwal->window);
  return apwal;
}
// ----------------------------------------------------------------------------
void apwalapp_selected_app_modified(apwalapp_t *apwal)
{
  gint page;
  TRACE("%s", "");
  g_assert(apwal != NULL);
  g_assert(apwal->prop != NULL && apwal->editor != NULL);
  g_assert(apwal->iconsel != NULL && apwal->iconsel_pref);
  //g_assert((apwal->apwal_pref->iconsel_a_separate_window) 
  //         && apwal->window2 == NULL);
  
  page = gtk_notebook_get_current_page(GTK_NOTEBOOK(apwal->notebook));
  switch(page)
  {
  case 0: // editor & property
    editor_refresh(apwal->editor);
    property_refresh(apwal->prop);
    if (apwal->selected_app)
      iconsel_set_filename(apwal->iconsel, app_get_icon(apwal->selected_app));
    else
      iconsel_set_filename(apwal->iconsel, NULL);
    iconsel_refresh_selected(apwal->iconsel);
    break;
  default:
    ERR("BUG: should not happend, call from page:%d", page); 
  }
 /*
    // if icon selection window is showed and editor has focus
    // then refresh icon selection
    if (apwal->apwal_pref->iconsel_in_a_separate_window)
    {
      if (GTK_WIDGET_VISIBLE(apwal->window2) &&
        GTK_WIDGET_HAS_FOCUS(gtk_window_get_focus(GTK_WINDOW(apwal->window2))))
      {
        if (apwal->selected_app)
          iconsel_set_filename(apwal->iconsel,
                               app_get_icon(apwal->selected_app));
        else
          iconsel_set_filename(apwal->iconsel, NULL);
        iconsel_refresh(apwal->iconsel);
      }
    }
*/

}

// ----------------------------------------------------------------------------
void apwalapp_selected_icon_modified(apwalapp_t *apwal)
{
  gboolean refresh_iconsel;
  gboolean refresh_editor;
  const char *file;

  TRACE("%s", "");
  g_assert(apwal != NULL);
  g_assert(apwal->prop != NULL && apwal->editor != NULL);
  g_assert(apwal->iconsel != NULL && apwal->iconsel_pref);
 
  refresh_iconsel = TRUE;
  refresh_editor = TRUE;
/*
  refresh_iconsel = FALSE;
  refresh_editor = FALSE;
  if (apwal->apwal_pref->iconsel_in_a_separate_window == FALSE)
  {
    page = gtk_notebook_get_current_page(GTK_NOTEBOOK(apwal->notebook));
    if (page == 1)
    {
      refresh_iconsel = TRUE;
      refresh_editor = TRUE;
    }
  }
  else
  {
    TRACE("window2 visible:%d, focus:%d", GTK_WIDGET_VISIBLE(apwal->window2),
       GTK_WIDGET_HAS_FOCUS(gtk_window_get_focus(GTK_WINDOW(apwal->window2))));
    
    if (GTK_WIDGET_VISIBLE(apwal->window2) &&
        GTK_WIDGET_HAS_FOCUS(gtk_window_get_focus(GTK_WINDOW(apwal->window2))))
    {
    TRACE("%s", "");
      page = gtk_notebook_get_current_page(GTK_NOTEBOOK(apwal->notebook2));
      if (page == 0)
      {
    TRACE("%s", "");
        refresh_iconsel = TRUE;
        refresh_editor = TRUE;
      }
      else
        ERR("page:%d is anormal. must be 0", page);
    }
  }
*/

  if (refresh_iconsel == TRUE)
  {
    //refresh already done by iconsel, not needed:
    //iconsel_refresh(apwal->iconsel);
  }
  if (refresh_editor == TRUE)
  {
    if (apwal->selected_app != NULL)
    {
      file = iconsel_get_filename(apwal->iconsel);
      if (file != NULL)
        app_set_icon(apwal->selected_app, file);
      else
        app_set_icon(apwal->selected_app, "");
    }
    editor_refresh(apwal->editor);
    property_refresh(apwal->prop);
  }
}
// ----------------------------------------------------------------------------
void apwalapp_goto_iconsel(apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  if (apwal->apwal_pref->iconsel_in_a_separate_window == TRUE)
  {
    if (apwal->apwal_pref->iconsel_modal)
    {
      gtk_grab_add(apwal->window2);
    }
    gtk_widget_show(apwal->window2);
    gtk_notebook_set_current_page(GTK_NOTEBOOK(apwal->notebook2), 0);
  }
  else
    gtk_notebook_set_current_page(GTK_NOTEBOOK(apwal->notebook), 1);
}
// ----------------------------------------------------------------------------
void apwalapp_goto_editor(apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  if (apwal->apwal_pref->iconsel_in_a_separate_window == TRUE)
  {
    if (apwal->apwal_pref->iconsel_modal)
    {
      gtk_widget_hide(apwal->window2);
      gtk_grab_remove(apwal->window2);
    }
  }
  gtk_notebook_set_current_page(GTK_NOTEBOOK(apwal->notebook), 0);
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
void apwal_load_apps(apwalapp_t *apwal, app_list_t *apps)
{
  g_assert(apwal != NULL);
  if (apwal->apps != NULL)
    app_list_free(& apwal->apps);
  apwal->apps = apps;
  //if (apwal->apps != NULL)
  //  app_list_dump(apwal->apps);
}

// ----------------------------------------------------------------------------
static void apwalapp_build_interface(apwalapp_t *apwal)
{
//GtkWidget *window;
  GtkWidget   *vbox0;
  GtkWidget   *vbox02;
//GtkWidget     *notebook;
  GtkWidget       *editor_vbox, *editor_label;
//GtkWidget         *property_frame;
//GtkWidget         *editor_frame;
  GtkWidget       *iconsel_vbox, *iconsel_label;
//GtkWidget         *iconsel_frame;
  GtkWidget       *iconsel_pref_vbox, *iconsel_pref_label;
//GtkWidget         *iconsel_pref_frame;
  GtkWidget       *apwal_pref_vbox, *apwal_pref_label;
//GtkWidget         *apwal_pref_frame;
  GtkWidget       *about_vbox, *about_label;
  GtkWidget     *btn_bbox;
  GtkWidget       *btn_ok;
  GtkWidget       *btn_cancel;
  GtkWidget     *btn2_bbox;
//GtkWidget       *btn_help;
  
  g_assert(apwal != NULL);
  // window
  apwal->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(apwal->window),"Apwal - Editor");
  g_signal_connect(G_OBJECT(apwal->window), "delete_event",
                   gtk_main_quit, NULL);
  //g_signal_connect(G_OBJECT(apwal->window), "key_release_event",
  //                 G_CALLBACK(apwalapp_window_key_release_event), apwal);
  g_signal_connect(G_OBJECT(apwal->window), "move_focus",
                   G_CALLBACK(apwalapp_window_move_focus_event), apwal);

  apwal->window2 = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(apwal->window2),"Apwal - Icon Selection");
  //gtk_widget_hide_on_delete(apwal->window2);
  g_signal_connect(G_OBJECT(apwal->window2), "delete_event",
                   G_CALLBACK(apwalapp_w2_delete), apwal);
  

  apwal->tips = gtk_tooltips_new();

  // vbox0
  vbox0 = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(vbox0), 4);
  gtk_container_add(GTK_CONTAINER(apwal->window), vbox0);
  gtk_widget_show(vbox0);
  
  // vbox02
  vbox02 = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(vbox02), 4);
  gtk_container_add(GTK_CONTAINER(apwal->window2), vbox02);
  gtk_widget_show(vbox02);
  
  // notebook
  apwal->notebook = gtk_notebook_new();
  g_signal_connect(G_OBJECT(apwal->notebook), "switch-page",
                   G_CALLBACK(apwalapp_switch_page), apwal);
  gtk_box_pack_start(GTK_BOX(vbox0), apwal->notebook, FALSE, FALSE, 0);
  gtk_widget_show(apwal->notebook);

  // notebook2
  apwal->notebook2 = gtk_notebook_new();
  g_signal_connect(G_OBJECT(apwal->notebook2), "switch-page",
                   G_CALLBACK(apwalapp_switch_page2), apwal);
  gtk_box_pack_start(GTK_BOX(vbox02), apwal->notebook2, TRUE, TRUE, 0);
  gtk_widget_show(apwal->notebook2);

  // editor_label
  editor_label = gtk_label_new("Editor");
  gtk_widget_show(editor_label);

  // editor_vbox
  editor_vbox = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(editor_vbox);

  gtk_notebook_append_page(GTK_NOTEBOOK(apwal->notebook),
                           editor_vbox, editor_label);

  // property_frame
  apwal->property_frame = gtk_frame_new(" Application Property ");
  gtk_container_set_border_width(GTK_CONTAINER(apwal->property_frame), 4);
  gtk_box_pack_start(GTK_BOX(editor_vbox), apwal->property_frame,
                     FALSE, FALSE, 0);
  gtk_widget_show(apwal->property_frame);

  // editor_frame
  apwal->editor_frame = gtk_frame_new(NULL); //" Editor ");
  gtk_container_set_border_width(GTK_CONTAINER(apwal->editor_frame), 4);
  gtk_frame_set_shadow_type(GTK_FRAME(apwal->editor_frame), GTK_SHADOW_NONE);
  gtk_box_pack_start(GTK_BOX(editor_vbox), apwal->editor_frame,
                     FALSE, FALSE, 0);
  gtk_widget_show(apwal->editor_frame);


  // iconsel_label
  iconsel_label = gtk_label_new("Icon Selection");
  gtk_widget_show(iconsel_label);

  // iconsel_vbox
  iconsel_vbox = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(iconsel_vbox);

  if (apwal->apwal_pref->iconsel_in_a_separate_window == FALSE)
    gtk_notebook_append_page(GTK_NOTEBOOK(apwal->notebook),
                             iconsel_vbox, iconsel_label);
  else
    gtk_notebook_append_page(GTK_NOTEBOOK(apwal->notebook2),
                             iconsel_vbox, iconsel_label);

  // iconsel_frame
  apwal->iconsel_frame = gtk_frame_new(NULL);
  gtk_container_set_border_width(GTK_CONTAINER(apwal->iconsel_frame), 4);
  gtk_frame_set_shadow_type(GTK_FRAME(apwal->iconsel_frame), GTK_SHADOW_NONE);
  gtk_box_pack_start(GTK_BOX(iconsel_vbox), apwal->iconsel_frame,
                     TRUE, TRUE, 0);
  gtk_widget_show(apwal->iconsel_frame);


  // iconsel_pref_label
  iconsel_pref_label = gtk_label_new("Icon Selection Pref");
  gtk_widget_show(iconsel_pref_label);

  // iconsel_pref_vbox
  iconsel_pref_vbox = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(iconsel_pref_vbox);

  if (apwal->apwal_pref->iconsel_in_a_separate_window == FALSE)
    gtk_notebook_append_page(GTK_NOTEBOOK(apwal->notebook),
                             iconsel_pref_vbox, iconsel_pref_label);
  else
    gtk_notebook_append_page(GTK_NOTEBOOK(apwal->notebook2),
                             iconsel_pref_vbox, iconsel_pref_label);

  // iconsel_frame
  apwal->iconsel_pref_frame = gtk_frame_new(NULL);
  gtk_container_set_border_width(GTK_CONTAINER(apwal->iconsel_pref_frame), 4);
  gtk_frame_set_shadow_type(GTK_FRAME(apwal->iconsel_pref_frame),
                            GTK_SHADOW_NONE);
  gtk_box_pack_start(GTK_BOX(iconsel_pref_vbox), apwal->iconsel_pref_frame,
                     TRUE, TRUE, 0);
  gtk_widget_show(apwal->iconsel_pref_frame);


  // apwal_pref_label
  apwal_pref_label = gtk_label_new("Apwal Preference");
  gtk_widget_show(apwal_pref_label);

  // apwal_pref_vbox
  apwal_pref_vbox = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(apwal_pref_vbox);

  gtk_notebook_append_page(GTK_NOTEBOOK(apwal->notebook),
                           apwal_pref_vbox, apwal_pref_label);

  // apwal_pref_frame
  apwal->apwal_pref_frame = gtk_frame_new(NULL);
  gtk_container_set_border_width(GTK_CONTAINER(apwal->apwal_pref_frame), 4);
  gtk_frame_set_shadow_type(GTK_FRAME(apwal->apwal_pref_frame), GTK_SHADOW_NONE);
  gtk_box_pack_start(GTK_BOX(apwal_pref_vbox), apwal->apwal_pref_frame,
                     TRUE, TRUE, 0);
  gtk_widget_show(apwal->apwal_pref_frame);


  // about_label
  about_label = gtk_label_new("About");
  gtk_widget_show(about_label);

  // about_vbox
  about_vbox = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(about_vbox);

  gtk_notebook_append_page(GTK_NOTEBOOK(apwal->notebook),
                           about_vbox, about_label);
  // about_frame
  apwal->about_frame = gtk_frame_new(NULL);
  gtk_container_set_border_width(GTK_CONTAINER(apwal->about_frame), 4);
  gtk_frame_set_shadow_type(GTK_FRAME(apwal->about_frame),
                            GTK_SHADOW_NONE);
  gtk_box_pack_start(GTK_BOX(about_vbox), apwal->about_frame,
                     TRUE, TRUE, 0);
  gtk_widget_show(apwal->about_frame);


  // --------
  
  // horizontal button box  'ok' & 'cancel' & 'help'
  btn_bbox = gtk_hbutton_box_new();
  gtk_button_box_set_layout(GTK_BUTTON_BOX(btn_bbox), GTK_BUTTONBOX_END);
  gtk_box_set_spacing(GTK_BOX(btn_bbox), 10);
  gtk_container_set_border_width(GTK_CONTAINER(btn_bbox), 4);
  gtk_box_pack_end(GTK_BOX(vbox0), btn_bbox, FALSE, FALSE, 0);
  gtk_widget_show(btn_bbox);

  // button cancel
  btn_cancel = gtk_button_new_from_stock(GTK_STOCK_CANCEL);
  GTK_WIDGET_SET_FLAGS(btn_cancel, GTK_CAN_DEFAULT);
  g_signal_connect(G_OBJECT(btn_cancel), "clicked",
                   G_CALLBACK(apwalapp_clicked_cancel), apwal);
  gtk_box_pack_end(GTK_BOX(btn_bbox), btn_cancel, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(btn_cancel, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(btn_cancel);
  gtk_widget_show(btn_cancel);

  // button ok
  btn_ok = gtk_button_new_from_stock(GTK_STOCK_OK);
  GTK_WIDGET_SET_FLAGS(btn_ok, GTK_CAN_DEFAULT);
  g_signal_connect(G_OBJECT(btn_ok), "clicked",
                   G_CALLBACK(apwalapp_clicked_ok), apwal);
  gtk_box_pack_end(GTK_BOX(btn_bbox), btn_ok, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(btn_ok, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(btn_ok);
  gtk_widget_show(btn_ok);

  // window 2 horizontal button box 'apply' 'close' / 'cancel' 'ok'
  btn2_bbox = gtk_hbutton_box_new();
  gtk_button_box_set_layout(GTK_BUTTON_BOX(btn2_bbox), GTK_BUTTONBOX_END);
  gtk_box_set_spacing(GTK_BOX(btn2_bbox), 10);
  gtk_container_set_border_width(GTK_CONTAINER(btn2_bbox), 4);
  gtk_box_pack_end(GTK_BOX(vbox02), btn2_bbox, FALSE, FALSE, 0);
  gtk_widget_show(btn2_bbox);

  // button apply
  apwal->w2_btn_apply = gtk_button_new_from_stock(GTK_STOCK_APPLY);
  GTK_WIDGET_SET_FLAGS(apwal->w2_btn_apply, GTK_CAN_DEFAULT);
  g_signal_connect(G_OBJECT(apwal->w2_btn_apply), "clicked",
                   G_CALLBACK(apwalapp_w2_apply_clicked), apwal);
  gtk_box_pack_end(GTK_BOX(btn2_bbox), apwal->w2_btn_apply, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(apwal->w2_btn_apply, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(apwal->w2_btn_apply);
  if (apwal->apwal_pref->iconsel_modal == FALSE)
    gtk_widget_show(apwal->w2_btn_apply);
  gtk_widget_show(apwal->w2_btn_apply);

  // button close
  apwal->w2_btn_close = gtk_button_new_from_stock(GTK_STOCK_CLOSE);
  GTK_WIDGET_SET_FLAGS(apwal->w2_btn_close, GTK_CAN_DEFAULT);
  g_signal_connect(G_OBJECT(apwal->w2_btn_close), "clicked",
                   G_CALLBACK(apwalapp_w2_close_clicked), apwal);
  gtk_box_pack_end(GTK_BOX(btn2_bbox), apwal->w2_btn_close, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(apwal->w2_btn_close, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(apwal->w2_btn_close);
  if (apwal->apwal_pref->iconsel_modal == FALSE)
    gtk_widget_show(apwal->w2_btn_close);
  gtk_widget_show(apwal->w2_btn_close);

  // button cancel
  apwal->w2_btn_cancel = gtk_button_new_from_stock(GTK_STOCK_CANCEL);
  GTK_WIDGET_SET_FLAGS(apwal->w2_btn_cancel, GTK_CAN_DEFAULT);
  g_signal_connect(G_OBJECT(apwal->w2_btn_cancel), "clicked",
                   G_CALLBACK(apwalapp_w2_cancel_clicked), apwal);
  gtk_box_pack_end(GTK_BOX(btn2_bbox), apwal->w2_btn_cancel, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(apwal->w2_btn_cancel, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(apwal->w2_btn_cancel);
  if (apwal->apwal_pref->iconsel_modal == TRUE)
    gtk_widget_show(apwal->w2_btn_cancel);
  gtk_widget_show(apwal->w2_btn_cancel);

  // button OK
  apwal->w2_btn_ok = gtk_button_new_from_stock(GTK_STOCK_OK);
  GTK_WIDGET_SET_FLAGS(apwal->w2_btn_ok, GTK_CAN_DEFAULT);
  g_signal_connect(G_OBJECT(apwal->w2_btn_ok), "clicked",
                   G_CALLBACK(apwalapp_w2_ok_clicked), apwal);
  gtk_box_pack_end(GTK_BOX(btn2_bbox), apwal->w2_btn_ok, TRUE, TRUE, 0);
  GTK_WIDGET_SET_FLAGS(apwal->w2_btn_ok, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(apwal->w2_btn_ok);
  if (apwal->apwal_pref->iconsel_modal == TRUE)
    gtk_widget_show(apwal->w2_btn_ok);
  gtk_widget_show(apwal->w2_btn_ok);


  // gtk_widget_show window at the end of apwalapp_new()
}
// ----------------------------------------------------------------------------
static void apwalapp_switch_page(GtkWidget *widget, GtkNotebookPage *page,
                                 guint page_num, apwalapp_t *apwal)
{
  g_assert(apwal != NULL && page != NULL && widget != NULL);
  TRACE("%s", "notebook apwalapp_switch_page");
}
// ----------------------------------------------------------------------------
static void apwalapp_switch_page2(GtkWidget *widget, GtkNotebookPage *page,
                                  guint page_num, apwalapp_t *apwal)
{
  g_assert(apwal != NULL && page != NULL && widget != NULL);
  TRACE("%s", "notebook2 apwalapp_switch_page2");
}
// ----------------------------------------------------------------------------
static void apwalapp_clicked_ok(GtkWidget *widget, apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  TRACE("%s", "save and exit program...");
  xmlrc_save_to_file(apwal->apps, apwal->iconsel_pref, apwal->apwal_pref);
  gtk_main_quit();
}
// ----------------------------------------------------------------------------
static void apwalapp_clicked_cancel(GtkWidget *widget, apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  TRACE("%s", "exit program without saving...");
  gtk_main_quit();
}
// ----------------------------------------------------------------------------
static void apwalapp_w2_close_clicked(GtkWidget *widget, apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  TRACE("%s", "w2 close");
   gtk_widget_hide(apwal->window2);
   if (apwal->apwal_pref->iconsel_modal)
     gtk_grab_remove(apwal->window2);
}
// ----------------------------------------------------------------------------
static void apwalapp_w2_apply_clicked(GtkWidget *widget, apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  TRACE("%s", "w2 apply");
  // if no icon selected, nothing to do
  if (icon_list_get_selected_icon(apwal->iconsel->icons) == NULL)
    return;
  // if no application selected selected, nothing to do
  if (apwal->selected_app == NULL)
    return;

  apwalapp_selected_icon_modified(apwal);
  apwalapp_goto_editor(apwal);
}
// ----------------------------------------------------------------------------
static void apwalapp_w2_cancel_clicked(GtkWidget *widget, apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  TRACE("%s", "w2 cancel");
   gtk_widget_hide(apwal->window2);
   if (apwal->apwal_pref->iconsel_modal)
     gtk_grab_remove(apwal->window2);
}
// ----------------------------------------------------------------------------
static void apwalapp_w2_ok_clicked(GtkWidget *widget, apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  TRACE("%s", "w2 OK");
  // if no icon selected, nothing to do
  if (icon_list_get_selected_icon(apwal->iconsel->icons) == NULL)
    return;
  // if no application selected selected, nothing to do
  if (apwal->selected_app == NULL)
    return;

  apwalapp_selected_icon_modified(apwal);
   gtk_widget_hide(apwal->window2);
   if (apwal->apwal_pref->iconsel_modal)
     gtk_grab_remove(apwal->window2);
}
// ----------------------------------------------------------------------------
static gboolean apwalapp_w2_delete(GtkWidget *widget, GdkEvent *event, apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  TRACE("%s", "delete w2");
   gtk_widget_hide(apwal->window2);
   if (apwal->apwal_pref->iconsel_modal)
     gtk_grab_remove(apwal->window2);
   return TRUE; // do NOT propagate the event
}
// ----------------------------------------------------------------------------
void gtk_notebook_move_page(GtkNotebook *src, gint src_num,
                            GtkNotebook *dst, gint dst_num);
// ----------------------------------------------------------------------------
void apwal_iconsel_win2tab(apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  TRACE("%s", "");
  if (GTK_WIDGET_VISIBLE(apwal->window2))
    gtk_widget_hide(apwal->window2);
// don't be worried about the modal state because if the window2 is modal
// and showed then it is impossible to modify apwal preferences... hop :p

  gtk_widget_hide(apwal->window2);
  gtk_widget_show(apwal->iconsel->selected_apply_btn);

  gtk_notebook_move_page(GTK_NOTEBOOK(apwal->notebook2), 0,
                         GTK_NOTEBOOK(apwal->notebook), 1);
  gtk_notebook_move_page(GTK_NOTEBOOK(apwal->notebook2), 0,
                         GTK_NOTEBOOK(apwal->notebook), 2);
}
// ----------------------------------------------------------------------------
void apwal_iconsel_tab2win(apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  TRACE("%s", "");
  gtk_notebook_move_page(GTK_NOTEBOOK(apwal->notebook), 1,
                         GTK_NOTEBOOK(apwal->notebook2), -1);
  gtk_notebook_move_page(GTK_NOTEBOOK(apwal->notebook), 1,
                         GTK_NOTEBOOK(apwal->notebook2), -1);

  gtk_widget_hide(apwal->iconsel->selected_apply_btn);

  // if non modal mode, then show the w2
  if (apwal->apwal_pref->iconsel_modal == FALSE
       && ! GTK_WIDGET_VISIBLE(apwal->window2))
    gtk_widget_show(apwal->window2);

}
// ----------------------------------------------------------------------------
void apwal_iconsel_set_modal(apwalapp_t *apwal)
{
  g_assert(apwal != NULL);
  TRACE("%s", "");
  // if the iconsel is showed and modal is selected, the when hide the
  // iconsel window (because it can't be in modal mode and the grab_remove
  // when the user close this window has not to be called as well).
  if (GTK_WIDGET_VISIBLE(apwal->window2))
    gtk_widget_hide(apwal->window2);
  gtk_widget_hide(apwal->w2_btn_apply);
  gtk_widget_hide(apwal->w2_btn_close);
  gtk_widget_show(apwal->w2_btn_cancel);
  gtk_widget_show(apwal->w2_btn_ok);
}
// ----------------------------------------------------------------------------
void apwal_iconsel_set_not_modal(apwalapp_t *apwal)
{
  gtk_widget_show(apwal->w2_btn_apply);
  gtk_widget_show(apwal->w2_btn_close);
  gtk_widget_hide(apwal->w2_btn_cancel);
  gtk_widget_hide(apwal->w2_btn_ok);

  // if we are asked to switch to non modal mode, then show the w2
  if (! GTK_WIDGET_VISIBLE(apwal->window2))
    gtk_widget_show(apwal->window2);

}
// ----------------------------------------------------------------------------
void gtk_notebook_move_page(GtkNotebook *src, gint src_num,
                            GtkNotebook *dst, gint dst_num)
{
  GtkWidget *frame;
  GtkWidget *label;
  TRACE("%s", "");
  frame = gtk_notebook_get_nth_page(src, src_num);
  if (frame == NULL)
    ERR("gtk_notebook_get_nth_page(%p, %d) == NULL", src, src_num);
  label = gtk_notebook_get_tab_label(src, frame);
  if (label == NULL)
    ERR("gtk_notebook_get_tab_label(%p, %p) == NULL", src, frame);

  g_object_ref(frame);
  g_object_ref(label);
  gtk_notebook_remove_page(src, src_num);
  gtk_notebook_insert_page(dst, frame, label, dst_num);
  g_object_unref(frame);
  g_object_unref(label);
}
// ----------------------------------------------------------------------------
#if 0
static gboolean apwalapp_window_key_release_event(GtkWidget *widget,
                                          GdkEventKey *event, apwalapp_t *apwal)
{
  int cc;
  int page;
  g_assert(event != NULL && apwal != NULL);
  g_assert(apwal->editor != NULL);
  TRACE("key:%d ", event->keyval); 

  // key binding only activated on the editor tab, if not, exit!
  page = gtk_notebook_get_current_page(GTK_NOTEBOOK(apwal->notebook));
  if (page != 0)
    return FALSE; // FALSE: propagate the event further

  // see gdk/gdkkeysyms.h for the GDK key codes
  if ((event->state & gtk_accelerator_get_default_mod_mask ()) == 0)
  {
    switch(event->keyval)
    {
      case GDK_Delete:
      case GDK_KP_Delete:
        if (apwal->selected_app != NULL)
        {
          cc = app_list_remove(apwal->apps, apwal->selected_app);
          if (cc)
            ERR("%s", "ask to remove the selected app and not possible");
          apwal->selected_app = NULL;
          apwalapp_selected_app_modified(apwal);
        }
        break;
#if 0 /*do it if find a real interest in doing it*/
      case GDK_KP_Space:
      case GDK_space:
      case GDK_Return:  // NOT catcht because of key binding!
      case GDK_KP_Enter: // NOT catcht because of key binding!
        if (apwal->selected_app != NULL)
          apwalapp_goto_iconsel(apwal);
        break;
      case GDK_Left:
      case GDK_KP_Left:
        if (apwal->selected_app != NULL)
          editor_goto(apwal->editor,
                      apwal->selected_app->x-1, apwal->selected_app->y);
        break;
      case GDK_Up:
      case GDK_KP_Up:
        if (apwal->selected_app != NULL)
          editor_goto(apwal->editor,
                      apwal->selected_app->x, apwal->selected_app->y-1);
        break;
      case GDK_Right:
      case GDK_KP_Right:
        if (apwal->selected_app != NULL)
          editor_goto(apwal->editor,
                      apwal->selected_app->x+1, apwal->selected_app->y);
        break;
      case GDK_Down:
      case GDK_KP_Down:
        if (apwal->selected_app != NULL)
          editor_goto(apwal->editor,
                      apwal->selected_app->x, apwal->selected_app->y+1);
        break;
#endif
      default:
        return FALSE; // FALSE: propagate the event further
    }
  }
  g_signal_stop_emission_by_name (GTK_OBJECT(widget), "key_release_event");
  return TRUE; // TRUE: do not propagate the event further
}
#endif
// ----------------------------------------------------------------------------
static void apwalapp_window_move_focus_event(GtkWindow *window,
                                        GtkDirectionType direction,
                                        apwalapp_t *apwal)
{
g_signal_stop_emission_by_name(GTK_OBJECT(window), "move_focus");
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
