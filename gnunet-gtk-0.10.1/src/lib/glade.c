/*
     This file is part of GNUnet.
     (C) 2010 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/lib/glade.c
 * @brief code for integration with glade
 * @author Christian Grothoff
 */
#include "gnunet_gtk_config.h"
#include "gnunet_gtk.h"


/**
 * Initialize GTK search path for icons.
 */
void
GNUNET_GTK_set_icon_search_path ()
{
  char *buf;

  buf = GNUNET_GTK_installation_get_path (GNUNET_OS_IPK_ICONDIR);
  gtk_icon_theme_append_search_path (gtk_icon_theme_get_default (), buf);
  GNUNET_free (buf);
}


/**
 * Get the name of the directory where all of our package
 * data is stored ($PREFIX/share/)
 *
 * @return name of the data directory
 */
const char *
GNUNET_GTK_get_data_dir ()
{
  static char *dd;

  if (dd == NULL)
    dd = GNUNET_GTK_installation_get_path (GNUNET_OS_IPK_DATADIR);
  return dd;
}


/**
 * Obtain a string from a GtkTreeView's model.
 *
 * @param treeview treeview to inspect
 * @param treepath path that identifies the item
 * @param column number of the column with the string
 * @param value where to store the string
 * @return TRUE on success, FALSE on errors
 */
gboolean
GNUNET_GTK_get_tree_string (GtkTreeView * treeview, GtkTreePath * treepath,
                            guint column, gchar ** value)
{
  gboolean ok;
  GtkTreeModel *model;

  model = gtk_tree_view_get_model (treeview);
  if (!model)
    return FALSE;

  GtkTreeIter iter;

  ok = gtk_tree_model_get_iter (model, &iter, treepath);
  if (!ok)
    return FALSE;

  *value = NULL;
  gtk_tree_model_get (model, &iter, column, value, -1);
  if (*value == NULL)
    return FALSE;
  return TRUE;
}


/**
 * Create an initialize a new builder based on the
 * GNUnet-GTK glade file.
 *
 * @param filename name of the resource file to load
 * @param user_data user_data to pass to signal handlers,
 *        use "NULL" to pass the GtkBuilder itself.
 * @param cb function to call before connecting signals
 * @return NULL on error
 */
GtkBuilder *
GNUNET_GTK_get_new_builder2 (const char *filename,
                             void *user_data,
                             GtkBuilderConnectFunc cb)
{
  char *glade_path;
  GtkBuilder *ret;
  GError *error;

  ret = gtk_builder_new ();
  gtk_builder_set_translation_domain (ret, "gnunet-gtk");
  GNUNET_asprintf (&glade_path, "%s%s",
                   GNUNET_GTK_get_data_dir (),
                   filename);
  error = NULL;
  if (0 == gtk_builder_add_from_file (ret, glade_path, &error))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                _("Failed to load `%s': %s\n"),
                glade_path, error->message);
    g_error_free (error);
    GNUNET_free (glade_path);
    return NULL;
  }
  if (NULL == user_data)
    user_data = ret;
  if (NULL != cb)
    gtk_builder_connect_signals_full (ret, cb, user_data);
  else
    gtk_builder_connect_signals (ret, user_data);
  GNUNET_free (glade_path);
  return ret;
}


/**
 * Remove the given entry and all of its children from the tree store.
 *
 * @param ts tree store to change
 * @param root root of the subtree to remove
 */
void
GNUNET_FS_GTK_remove_treestore_subtree (GtkTreeStore * ts,
					GtkTreeIter * root)
{
  GtkTreeIter child;

  while (gtk_tree_model_iter_children (GTK_TREE_MODEL (ts), &child, root))
    GNUNET_FS_GTK_remove_treestore_subtree (ts, &child);
  gtk_tree_store_remove (ts, root);
}


/* end of glade.c */
