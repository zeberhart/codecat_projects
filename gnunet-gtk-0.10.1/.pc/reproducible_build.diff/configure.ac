# This file is part of GNUnet.
# (C) 2001--2013 Christian Grothoff (and other contributing authors)
#
# GNUnet is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; either version 3, or (at your
# option) any later version.
#
# GNUnet is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNUnet; see the file COPYING.  If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#
#
# Process this file with autoconf to produce a configure script.
#
AC_PREREQ(2.61)
AC_INIT([GNUnet GTK],0.10.1,[bug-gnunet@gnu.org],[gnunet-gtk])
AM_INIT_AUTOMAKE([silent-rules])
AC_CONFIG_HEADERS([gnunet_gtk_config.h])

AH_TOP([#define _GNU_SOURCE  1])

AC_ISC_POSIX
AC_PROG_AWK
AM_PROG_CC_C_O
AC_PROG_MKDIR_P
AC_PROG_CPP
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET
AC_LIBTOOL_WIN32_DLL
AC_HEADER_STDC
AC_CANONICAL_HOST

# dynamic libraries/plugins
AC_DISABLE_STATIC
AC_PROG_LIBTOOL

AC_SYS_LARGEFILE
AC_FUNC_FSEEKO
AC_TYPE_UID_T

CFLAGS="-Wall $CFLAGS"
# use '-fno-strict-aliasing', but only if the compiler can take it
if gcc -fno-strict-aliasing -S -o /dev/null -xc /dev/null >/dev/null 2>&1;
then
 CFLAGS="-fno-strict-aliasing $CFLAGS"
fi

# Default to unix configuration
ENABLE_ON_UNIX=""
ENABLE_ON_W32="#"
# Check system type
case "$host_os" in
*darwin* | *rhapsody* | *macosx*)
     AC_DEFINE_UNQUOTED(OSX,1,[This is an OS X system])
     CFLAGS="-no-cpp-precomp $CFLAGS"
     LDFLAGS="-flat_namespace -undefined suppress $LDFLAGS"
     ;;
linux*)
     AC_DEFINE_UNQUOTED(LINUX,1,[This is a Linux system])
     ;;
freebsd*)
     AC_DEFINE_UNQUOTED(SOMEBSD,1,[This is a BSD system])
     AC_DEFINE_UNQUOTED(FREEBSD,1,[This is a FreeBSD system])
     ;;
openbsd*)
     AC_DEFINE_UNQUOTED(SOMEBSD,1,[This is a BSD system])
     AC_DEFINE_UNQUOTED(OPENBSD,1,[This is an OpenBSD system])
     ;;
netbsd*)
     AC_DEFINE_UNQUOTED(SOMEBSD,1,[This is a BSD system])
     AC_DEFINE_UNQUOTED(NETBSD,1,[This is a NetBSD system])
     ;;
*solaris*)
     AC_DEFINE_UNQUOTED(SOLARIS,1,[This is a Solaris system])
     AC_DEFINE_UNQUOTED(_REENTRANT,1,[Need with solaris or errno doesnt work])
     build_target="solaris"
     ;;
*arm-linux*)
     AC_DEFINE_UNQUOTED(LINUX,1,[This is a Linux system])
     ;;
*cygwin*)
     AC_DEFINE_UNQUOTED(CYGWIN,1,[This is a Cygwin system])
     AC_DEFINE_UNQUOTED(WINDOWS,1,[This is a Windows system])
     AC_CHECK_LIB(intl, gettext)
     ENABLE_ON_W32=""
     ENABLE_ON_UNIX="#"
     build_target="cygwin"
     ;;
*mingw*)
     AC_DEFINE_UNQUOTED(MINGW,1,[This is a MinGW system])
     AC_DEFINE_UNQUOTED(WINDOWS,1,[This is a Windows system])
     AC_DEFINE_UNQUOTED(_WIN32,1,[This is a Windows system])
     AC_CHECK_LIB(intl, gettext)
     LDFLAGS="$LDFLAGS -Wl,--export-all-symbols -lws2_32"
     CFLAGS="-mms-bitfields -D__USE_MINGW_ANSI_STDIO=1 $CFLAGS"
     LIBS="$LIBS -lws2_32 -lplibc -lole32"
     ENABLE_ON_W32=""
     ENABLE_ON_UNIX="#"
     build_target="mingw"
     ;;
*)
     AC_MSG_RESULT(Unrecognised OS $host_os)
     AC_DEFINE_UNQUOTED(OTHEROS,1,[Some strange OS])
;;
esac

AM_CONDITIONAL(MINGW,   test "$build_target" = "mingw")



AC_SUBST(ENABLE_ON_UNIX)
AC_SUBST(ENABLE_ON_W32)

GN_LIB_LDFLAGS="-export-dynamic -no-undefined"
AC_SUBST(GN_LIB_LDFLAGS)

# check for gettext
AM_GNU_GETTEXT_VERSION([0.17])
AM_GNU_GETTEXT([external])

AC_CHECK_HEADERS([errno.h stdio.h unistd.h locale.h sys/stat.h sys/types.h langinfo.h libintl.h unistd.h stddef.h argz.h sys/socket.h netinet/in.h stdarg.h])

AC_CHECK_FUNCS([getpwnam])

AC_CHECK_DECLS([_stati64], [], [], [[#include <sys/stat.h>]])

GNUNET_FS_APP=""
AC_ARG_ENABLE([fs-app],
   AS_HELP_STRING([--disable-fs-app],[Do not build gnunet-fs-gtk application]))
AS_IF([test "x$enable_fs_app" != "xno"], [
  GNUNET_FS_APP="fs"
])
AC_SUBST(GNUNET_FS_APP)

GNUNET_SETUP_APP=""
AC_ARG_ENABLE([setup-app],
   AS_HELP_STRING([--disable-setup-app],[Do not build gnunet-setup application]))
AS_IF([test "x$enable_setup_app" != "xno"], [
  GNUNET_SETUP_APP="setup"
])
AC_SUBST(GNUNET_SETUP_APP)

GNUNET_PEERINFO_APP=""
AC_ARG_ENABLE([peerinfo-app],
   AS_HELP_STRING([--disable-peerinfo-app],[Do not build gnunet-peerinfo-gtk application]))
AS_IF([test "x$enable_peerinfo_app" != "xno"], [
  GNUNET_PEERINFO_APP="peerinfo"
])
AC_SUBST(GNUNET_PEERINFO_APP)

GNUNET_CONVERSATION_APP=""
AC_ARG_ENABLE([conversation-app],
   AS_HELP_STRING([--disable-conversation-app],[Do not build gnunet-conversation-gtk application]))
AS_IF([test "x$enable_conversation_app" != "xno"], [
  GNUNET_CONVERSATION_APP="conversation"
])
AC_SUBST(GNUNET_CONVERSATION_APP)

# test for libextractor
extractor=0
AC_MSG_CHECKING(for libextractor)
AC_ARG_WITH(extractor,
   [  --with-extractor=PFX    Base of libextractor installation],
   [AC_MSG_RESULT([$with_extractor])
    case $with_extractor in
      no)
        ;;
      yes)
        AC_CHECK_HEADERS(extractor.h,extractor=1)
        ;;
      *)
        CPPFLAGS="-I$with_extractor/include $CPPFLAGS"
        AC_CHECK_HEADERS(extractor.h,extractor=1)
        ;;
    esac
   ],
   [AC_MSG_RESULT([--with-extractor not specified])
    AC_CHECK_HEADERS(extractor.h,extractor=1)])
if test "$extractor" != 1
then
 AC_MSG_ERROR([gnunet-gtk requires libextractor headers])
fi


# test for libqrencode
qrencode=0
QR_LIBS="-lqrencode"
AC_MSG_CHECKING(for libqrencode)
AC_ARG_WITH(qrencode,
   [  --with-qrencode=PFX    Base of libqrencode installation],
   [AC_MSG_RESULT([$with_qrencode])
    case $with_qrencode in
      no)
        ;;
      yes)
        AC_CHECK_HEADERS(qrencode.h,qrencode=1)
        ;;
      *)
        CPPFLAGS="-I$with_qrencode/include $CPPFLAGS"
        QR_CFLAGS="-I$with_qrencode/include"
        QR_LIBS="-L$with_qrencode/lib -lqrencode"
        AC_CHECK_HEADERS(qrencode.h,qrencode=1)
        ;;
    esac
   ],
   [AC_MSG_RESULT([--with-qrencode not specified])
    AC_CHECK_HEADERS(qrencode.h,qrencode=1)])

if test "$qrencode" != 1
then
QR_LIBS=""
QR_CFLAGS=""
fi

AC_SUBST(QR_CFLAGS)
AC_SUBST(QR_LIBS)


AC_MSG_CHECKING(for gtk)
check_for_3=3.0.0
check_for_2=false
AC_ARG_WITH(gtk_version,
   [  --with-gtk-version=VERSION    version number of gtk to use (>=3.0.0 by default)],
   [AC_MSG_RESULT([$with_gtk_version])
    case $with_gtk_version in
      *)
        if test "x${with_gtk_version:0:1}" == "x2"
        then
          check_for_3=false
          check_for_2=$with_gtk_version
        elif test "x${with_gtk_version:0:1}" == "x3"
        then
          check_for_3=$with_gtk_version
          check_for_2=false
        fi
        ;;
    esac
   ],
   [AC_MSG_RESULT([--with-gtk-version not specified])])

without_gtk=true
if test "x$check_for_3" != "xfalse"
then
  AM_PATH_GTK_3_0([$check_for_3],without_gtk=false,)
  # test for libunique
  AC_ARG_WITH([libunique], AC_HELP_STRING([--without-libunique], [disable libunique]))
  have_libunique=no
  if test x$with_libunique != xno; then
    PKG_CHECK_MODULES([unique], [unique-3.0], AC_DEFINE(HAVE_LIBUNIQUE, 1, [Define if libunique is available]), AC_MSG_WARN([libunique would be nice to have]))
  fi
fi

if test "x$without_gtk" == "xtrue" -a "x$check_for_2" != "xfalse"
then
  AM_PATH_GTK_2_0([$check_for_2],without_gtk=false,)
  # test for libunique
  AC_ARG_WITH([libunique], AC_HELP_STRING([--without-libunique], [disable libunique]))
  have_libunique=no
  if test x$with_libunique != xno; then
    PKG_CHECK_MODULES([unique], [unique-1.0], AC_DEFINE(HAVE_LIBUNIQUE, 1, [Define if libunique is available]), AC_MSG_WARN([libunique would be nice to have]))
  fi
fi


AM_CONDITIONAL(HAVE_GTK, test x$without_gtk != xtrue)
if test $without_gtk != true
then
  AC_DEFINE_UNQUOTED([HAVE_GTK], 1, [We have GTK])
else
  AC_MSG_ERROR(Cannot find GTK: Is pkg-config in path?)
fi
CFLAGS="$CFLAGS $GTK_CFLAGS"
CPPFLAGS="$CPPFLAGS $GTK_CFLAGS"
LIBS="$LIBS $GTK_LIBS"
AC_CHECK_HEADERS([glib.h gmodule.h gtk/gtk.h gdk/gdk.h],,AC_MSG_ERROR([gnunet-gtk requires GTK]))



# test for Glade
glade=0
lookin=${prefix}
GLADE_CPPFLAGS=""
GLADE_LDFLAGS=""
GLADE_CFLAGS=""
AC_MSG_CHECKING(for Glade core)
AC_ARG_WITH(glade,
   [  --with-glade=PFX       Base of Glade installation (found with pkg-config if omitted)],
   [AC_MSG_RESULT([$with_glade])
    case $with_glade in
      no)
        lookin=""
        ;;
      yes)
        lookin="${prefix}"
        ;;
      *)
        lookin="$with_glade"
        ;;
    esac
   ],
   [
     AC_MSG_RESULT([--with-glade not specified])
     if test "x$check_for_2" != "xfalse"
     then
       # For gtk2 we need old glade
       PKG_CHECK_MODULES([GLADE], [gladeui-1.0 >= 3.6.0], glade=1,
         [
         PKG_CHECK_MODULES([GLADE], [gladeui-2.0 >= 3.10.0], glade=2)
         ]
       )
     else
       PKG_CHECK_MODULES([GLADE], [gladeui-2.0 >= 3.10.0], glade=2)
     fi
   ]
)

if test "x$glade" == "x0" -a ! "x$lookin" == "x"
then
  AC_MSG_CHECKING(for Glade3 in $lookin)
  backup_LIBS="$LIBS"
  backup_CFLAGS="$CFLAGS"
  backup_CPPFLAGS="$CPPFLAGS"
  if test "x$check_for_2" != "xfalse"
  then
    GLADE_LIBS="-L${lookin}/lib"
    GLADE_CFLAGS="-I${lookin}/include/libgladeui-1.0"
    GLADE_CPPFLAGS="-I${lookin}/include/libgladeui-1.0"
    LIBS="$GLADE_LIBS $backup_LIBS"
    CFLAGS="$GLADE_CFLAGS $backup_CFLAGS"
    CPPFLAGS="$GLADE_CPPFLAGS $backup_CPPFLAGS"
    AC_CHECK_HEADERS([glib.h gmodule.h gtk/gtk.h gdk/gdk.h gdk/gdkpixbuf.h  gladeui/glade.h],
      AC_CHECK_LIB([gladeui-1], [glade_xml_node_new],
        [glade=1]
      )
    )
  fi
  if test "x$glade" == "x0"
  then
    GLADE_LIBS="-L${lookin}/lib"
    GLADE_CFLAGS="-I${lookin}/include/libgladeui-2.0"
    GLADE_CPPFLAGS="-I${lookin}/include/libgladeui-2.0"
    LIBS="$GLADE_LIBS $backup_LIBS"
    CFLAGS="$GLADE_CFLAGS $backup_CFLAGS"
    CPPFLAGS="$GLADE_CPPFLAGS $backup_CPPFLAGS"
    AC_CHECK_HEADERS([glib.h gmodule.h gtk/gtk.h gdk/gdk.h gdk/gdkpixbuf.h gladeui/glade.h],
      AC_CHECK_LIB([gladeui-2], [glade_xml_node_new],
        glade=2)
    )
  fi
  if test ! "x$glade" == "x0"
  then
    EXT_LIB_PATH="-L${lookin}/lib $EXT_LIB_PATH"
  fi
  LIBS="$backup_LIBS"
  CFLAGS="$backup_CFLAGS"
  CPPFLAGS="$backup_CPPFLAGS"
fi

if test "x$glade" == "x0"
then
  AC_MSG_ERROR([gnunet-gtk requires Glade3 (library and headers)])
fi

AC_SUBST(GLADE_CFLAGS)
AC_SUBST(GLADE_LIBS)


# Adam shostack suggests the following for Windows:
# -D_FORTIFY_SOURCE=2 -fstack-protector-all
AC_ARG_ENABLE(gcc-hardening,
   AS_HELP_STRING(--enable-gcc-hardening, enable compiler security checks),
[if test x$enableval = xyes; then
    CFLAGS="$CFLAGS -D_FORTIFY_SOURCE=2 -fstack-protector-all"
    CFLAGS="$CFLAGS -fwrapv -fPIE -Wstack-protector"
    CFLAGS="$CFLAGS --param ssp-buffer-size=1"
    LDFLAGS="$LDFLAGS -pie"
fi])


# Linker hardening options
# Currently these options are ELF specific - you can't use this with MacOSX
AC_ARG_ENABLE(linker-hardening,
  AS_HELP_STRING(--enable-linker-hardening, enable linker security fixups),
[if test x$enableval = xyes; then
   LDFLAGS="$LDFLAGS -z relro -z now"
fi])


extra_logging=GNUNET_NO
AC_ARG_ENABLE([logging],
   AS_HELP_STRING([--enable-logging@<:@=value@:>@],[Enable logging calls. Possible values: yes,no,verbose,veryverbose ('yes' is the default)]),
   [AS_IF([test "x$enableval" = "xyes"], [],
          [test "x$enableval" = "xno"], [AC_DEFINE([GNUNET_CULL_LOGGING],[],[Define to cull all logging calls])],
          [test "x$enableval" = "xverbose"], [extra_logging=GNUNET_YES]
          [test "x$enableval" = "xveryverbose"], [extra_logging=\(GNUNET_YES+1\)])
   ], [])
AC_DEFINE_UNQUOTED([GNUNET_EXTRA_LOGGING],[$extra_logging],[1 if extra logging is enabled, 2 for very verbose extra logging, 0 otherwise])



# test for GNUnet core
gnunet=0
gnunet_conversation=0
lookin=${prefix}
GNUNET_CFLAGS=""
GNUNET_CPPFLAGS=""
GNUNET_LIBS=""
AC_MSG_CHECKING(for GNUnet core)
AC_ARG_WITH(gnunet,
   [  --with-gnunet=PFX       Base of GNUnet installation],
   [AC_MSG_RESULT([$with_gnunet])
    case $with_gnunet in
      no)
        lookin=""
        ;;
      yes)
        lookin="${prefix}"
        ;;
      *)
        lookin="$with_gnunet"
        ;;
    esac
   ],
   [
     AC_MSG_RESULT([--with-gnunet not specified])
     PKG_CHECK_MODULES([GNUNET], [gnunetutil >= 0.9.0], gnunet=1)
     AC_CHECK_HEADERS([gnunet/gnunet_conversation_service.h],
     AC_CHECK_LIB([gnunetspeaker], [GNUNET_SPEAKER_destroy],
      [
        gnunet_conversation=1
        EXT_LIB_PATH="-L${lookin}/lib $EXT_LIB_PATH"
      ]
      ),,[#include <gnunet/platform.h>]
     )
   ]
)

if test "x$gnunet" == "x0" -a ! "x$lookin" == "x"
then
  AC_MSG_CHECKING(for GNUnet util library in $lookin)
  backup_LIBS="$LIBS"
  backup_CFLAGS="$CFLAGS"
  backup_CPPFLAGS="$CPPFLAGS"
  GNUNET_LIBS="-L${lookin}/lib"
  GNUNET_CFLAGS="-I${lookin}/include"
  GNUNET_CPPFLAGS="-I${lookin}/include"
  LIBS="$GNUNET_LIBS $backup_LIBS"
  CFLAGS="$GNUNET_CFLAGS $backup_CFLAGS"
  CPPFLAGS="$GNUNET_CPPFLAGS $backup_CPPFLAGS"
  AC_CHECK_HEADERS([gnunet/gnunet_util_lib.h],
    AC_CHECK_LIB([gnunetutil], [GNUNET_xfree_],
      [
        gnunet=1
        EXT_LIB_PATH="-L${lookin}/lib $EXT_LIB_PATH"
      ]
    ),,[#include <gnunet/platform.h>]
  )
  AC_CHECK_HEADERS([gnunet/gnunet_conversation_service.h],
    AC_CHECK_LIB([gnunetspeaker], [GNUNET_SPEAKER_destroy],
      [
        gnunet_conversation=1
        EXT_LIB_PATH="-L${lookin}/lib $EXT_LIB_PATH"
      ]
    ),,[#include <gnunet/platform.h>]
  )
  LIBS="$backup_LIBS"
  CFLAGS="$backup_CFLAGS"
  CPPFLAGS="$backup_CPPFLAGS"
fi

if test "x$gnunet" == "x0"
then
  AC_MSG_ERROR([gnunet-gtk requires GNUnet])
fi

AC_SUBST(GNUNET_CFLAGS)
AC_SUBST(GNUNET_LIBS)

AM_CONDITIONAL(HAVE_CONVERSATION, test x$gnunet_conversation != x0)

backup_CPPFLAGS="$CPPFLAGS"
CPPFLAGS="$GNUNET_CPPFLAGS $backup_CPPFLAGS"
AC_CHECK_HEADERS([gnunet/gnunet_fs_service.h gnunet/gnunet_statistics_service.h gnunet/gnunet_core_service.h], [],
                 AC_MSG_ERROR([compiling gnunet-gtk requires GNUnet core headers]), [#include <gnunet/platform.h>])
CPPFLAGS="$backup_CPPFLAGS"

SAVELIBS=$LIBS
LIBS="$GNUNET_LIBS $LIBS"
AC_CHECK_LIB(gnunetutil,GNUNET_log_setup,,
             AC_MSG_ERROR([gnunet-gtk requires libgnunetutil]))
AC_CHECK_LIB(gnunetfs,GNUNET_FS_uri_to_string,,
             AC_MSG_ERROR([gnunet-gtk requires FS]))
AC_CHECK_LIB(gnunetstatistics,GNUNET_STATISTICS_get,,
             AC_MSG_ERROR([gnunet-gtk requires STATISTICS]))
AC_CHECK_LIB(gnunetcore,GNUNET_CORE_connect,,
             AC_MSG_ERROR([gnunet-gtk requires CORE]))
AC_CHECK_LIB(gladeui-1,glade_xml_node_new, [],
             [
             AC_CHECK_LIB(gladeui-2,glade_xml_node_new,,
                          AC_MSG_ERROR([gnunet-gtk requires glade3]))
             ]
            )
LIBS=$SAVELIBS

AC_CHECK_HEADERS([gtk/gtkx.h])

AC_DEFINE_DIR([PACKAGE_DATA], [datarootdir], [The directory for installing read-only architecture-independent data])

# Set PACKAGE_SOURCE_DIR in gnunet_gtk_config.h.
packagesrcdir=`cd $srcdir && pwd`
AC_DEFINE_UNQUOTED(PACKAGE_SOURCE_DIR, "${packagesrcdir}", [source dir])

GN_PLUGIN_LDFLAGS="-export-dynamic -avoid-version -module -no-undefined"

AC_SUBST(GN_PLUGIN_LDFLAGS)

AC_SUBST(GNUNET_GNS_GTK_NAME, "GNUnet-GNS-GTK", [name of the program])
AC_SUBST(GNUNET_FS_GTK_NAME, "GNUnet-FS-GTK", [name of the program])
AC_SUBST(GNUNET_PEERINFO_GTK_NAME, "GNUnet-Peerinfo-GTK", [name of the program])
AC_SUBST(GNUNET_STATISTICS_GTK_NAME, "GNUnet-Statistics-GTK", [name of the program])
AC_SUBST(GNUNET_SETUP_GTK_NAME, "GNUnet-Setup", [name of the program])

AC_SUBST(GNUNET_FS_GTK_VERSION3, $PACKAGE_VERSION, [version of the program])
AC_SUBST(GNUNET_PEERINFO_GTK_VERSION3, $PACKAGE_VERSION, [version of the program])
AC_SUBST(GNUNET_STATISTICS_GTK_VERSION3, $PACKAGE_VERSION, [version of the program])
AC_SUBST(GNUNET_SETUP_GTK_VERSION3, $PACKAGE_VERSION, [version of the program])

AC_SUBST(GNUNET_FS_GTK_YEARFROM, [2010], [year of the first release])
AC_SUBST(GNUNET_PEERINFO_GTK_YEARFROM, [2010], [year of the first release])
AC_SUBST(GNUNET_STATISTICS_GTK_YEARFROM, [2010], [year of the first release])
AC_SUBST(GNUNET_SETUP_GTK_YEARFROM, [2010], [year of the first release])

thisyear=$(date | sed -e 's/.* \([[[:digit:]]]\+\)$/\1/')
AC_SUBST(GNUNET_FS_GTK_YEARTO, ${thisyear}, [year of the most current release])
AC_SUBST(GNUNET_PEERINFO_GTK_YEARTO, ${thisyear}, [year of the most current release])
AC_SUBST(GNUNET_STATISTICS_GTK_YEARTO, ${thisyear}, [year of the most current release])
AC_SUBST(GNUNET_SETUP_GTK_YEARTO, ${thisyear}, [year of the most current release])

AC_OUTPUT([
Makefile
contrib/Makefile
doc/Makefile
doc/doxygen/Makefile
src/Makefile
src/include/Makefile
src/lib/Makefile
src/fs/Makefile
src/fs/gnunet-fs-gtk.conf
src/identity/Makefile
src/peerinfo/Makefile
src/main/Makefile
src/namestore/Makefile
src/setup/Makefile
src/statistics/Makefile
src/conversation/Makefile
pixmaps/Makefile
po/Makefile.in
po/Makefile
gnunet-fs-gtk.desktop
gnunet-setup.desktop
contrib/gnunet_fs_gtk_about_window.glade
contrib/gnunet_identity_gtk_about_window.glade
contrib/gnunet_peerinfo_gtk_about_window.glade
contrib/gnunet_namestore_gtk_about_window.glade
contrib/gnunet_statistics_gtk_about_window.glade
contrib/gnunet_setup_about_window.glade
contrib/gnunet_conversation_gtk_about_window.glade
])

if test "$qrencode" != 1
then
 AC_MSG_WARN([gnunet-setup will not include QR support])
fi

if test "x$gnunet_conversation" == "x0"
then
  AC_MSG_ERROR([gnunet-conversation-gtk will not be built])
fi
