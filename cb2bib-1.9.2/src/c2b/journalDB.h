/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#ifndef JOURNALDB_H
#define JOURNALDB_H

#include <QString>
#include <QStringList>


/**
    Database of Journal Names

    @author Pere Constans
*/
class journalDB
{

public:
    explicit journalDB(const QString& dbfile);
    inline ~journalDB() {}

    QString retrieve(const QString& JQuery) const;
    QString retrieveFull(const QString& JQuery) const;

    inline int count() const
    {
        return _nitems;
    }
    inline const QString& retrieve(const int index) const
    {
        return JAbbrev.at(index);
    }
    inline const QString& retrieveFull(const int index) const
    {
        return JExtended.at(index);
    }
    inline const QString& simplified(const int index) const
    {
        return JAbbrev_simp_w.at(index);
    }
    inline const QString& fullsimplified(const int index) const
    {
        return JAbbrev_simp.at(index);
    }
    inline const QString& fullsimplifiedFull(const int index) const
    {
        return JExtended_simp.at(index);
    }
    inline const QStringList& abbreviatedSimplifiedList() const
    {
        return JAbbrev_simp_w;
    }


private:
    QStringList JAbbrev;
    QStringList JAbbrev_simp;
    QStringList JAbbrev_simp_w;
    QStringList JCode;
    QStringList JExtended;
    QStringList JExtended_simp;
    int _nitems;

};

#endif
