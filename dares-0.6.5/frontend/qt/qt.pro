CONFIG += qt warn_on

DEFINES = WITH_QT

TEMPLATE = app
TARGET = dares

INCLUDEPATH += . ../../include

# Input
HEADERS += ../../include/dares.h \
           ../../include/global.h \
           paramsdialog.h \
           daresview.h
SOURCES += paramsdialog.cpp \
           daresview.cpp \
           ../../backend/dares.c \
           main.cpp


DEPENDPATH += ../../include

# care for Debian build options
contains( DEB_BUILD_OPTIONS, noopt ) {
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O0
}

win32:LIBS += ..\..\lib\dares.lib
linux-g++:LIBS += -lmagic

linux-g++:QMAKE_CXXFLAGS += -g -pedantic -Wno-long-long

CONFIG += depend_includepath

unix {
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
  UI_DIR= .ui
}
!unix {
  MOC_DIR = _moc
  OBJECTS_DIR = _obj
  UI_DIR= _ui
}
