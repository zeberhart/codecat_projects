/*
     This file is part of GNUnet.
     (C) 2010, 2011 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/setup/gnunet-setup-transport-test.c
 * @brief support for testing transport configurations
 * @author Christian Grothoff
 */
#ifndef GNUNET_SETUP_TRANSPORT_TEST_H
#define GNUNET_SETUP_TRANSPORT_TEST_H

/**
 * Function called whenever the user wants to test a
 * transport configuration.
 *
 * @param section_name section with the port numbers
 * @param is_tcp GNUNET_YES for TCP, GNUNET_NO for UDP
 * @param success_image image to show on success
 * @param failure_image image to show on failure
 */
void
GNUNET_setup_transport_test (const char *section_name, int is_tcp,
                             const char *success_image,
                             const char *failure_image);

#endif
