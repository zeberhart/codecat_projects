/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_tree.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_abs.h"

#include "syr_proto.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
MPI_Status status;
#endif

 
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |       Calcul des extremaux d'une variable 1D                         |
  |======================================================================| */
void extreme(int type,int nb,double *var,
	     struct Maillage maillnodes,double *xxabs,struct SDparall sdparall)
{
  int i,n,n1,n2,np1,np2,np3,np4;
  double *t,xmin=1.e10,xmax=-273.,xx[8];


  /* recherche des min/max sur le champ fourni */
  /* ----------------------------------------- */
  for (i=n1=n2=0,t=var;i<nb;i++,t++)
    {
      if (*t>xmax) {xmax=*t;n2=i;}
      if (*t<xmin) {xmin=*t;n1=i;}
    }

  /* detection de la position des min/max */
  /* ------------------------------------ */
  if (type==0) /* extrema sur les noeuds */
    {

      xx[0]=xmin; 
      xx[1]=maillnodes.coord[0][n1];
      xx[2]=maillnodes.coord[1][n1];
      if (maillnodes.ndim==3) xx[3]=maillnodes.coord[2][n1]; else xx[3]=0.;
      
      xx[4]=xmax;
      xx[5]=maillnodes.coord[0][n2];
      xx[6]=maillnodes.coord[1][n2];
      if (maillnodes.ndim==3) xx[7]=maillnodes.coord[2][n2]; else xx[7]=0.;
    }
  else if (type==1)  /* extrema sur les elements */
    {
      if (maillnodes.ndim==2)
	{
	  np1=maillnodes.node[0][n1];
	  np2=maillnodes.node[1][n1];
	  np3=maillnodes.node[2][n1];
	  xx[0]=xmin; 
	  xx[1]=(maillnodes.coord[0][np1]+maillnodes.coord[0][np2]+
		 maillnodes.coord[0][np3])/3.;
	  xx[2]=(maillnodes.coord[1][np1]+maillnodes.coord[1][np2]+
		 maillnodes.coord[1][np3])/3.;
	  xx[3]=0;
	  np1=maillnodes.node[0][n2];
	  np2=maillnodes.node[1][n2];
	  np3=maillnodes.node[2][n2];
	  xx[4]=xmax; 
	  xx[5]=(maillnodes.coord[0][np1]+maillnodes.coord[0][np2]+
		 maillnodes.coord[0][np3])/3.;
	  xx[6]=(maillnodes.coord[1][np1]+maillnodes.coord[1][np2]+
		 maillnodes.coord[1][np3])/3.;
	  xx[7]=0;
	}
      else
	{
	  np1=maillnodes.node[0][n1];
	  np2=maillnodes.node[1][n1];
	  np3=maillnodes.node[2][n1];
	  np4=maillnodes.node[3][n1];
	  xx[0]=xmin; 
	  xx[1]=(maillnodes.coord[0][np1]+maillnodes.coord[0][np2]+
		 maillnodes.coord[0][np3]+maillnodes.coord[0][np4])*0.25;
	  xx[2]=(maillnodes.coord[1][np1]+maillnodes.coord[1][np2]+
		 maillnodes.coord[1][np3]+maillnodes.coord[1][np4])*0.25;
	  xx[3]=(maillnodes.coord[2][np1]+maillnodes.coord[2][np2]+
		 maillnodes.coord[2][np3]+maillnodes.coord[2][np4])*0.25;

	  np1=maillnodes.node[0][n2];
	  np2=maillnodes.node[1][n2];
	  np3=maillnodes.node[2][n2];
	  xx[4]=xmax; 
	  xx[5]=(maillnodes.coord[0][np1]+maillnodes.coord[0][np2]+
		 maillnodes.coord[0][np3]+maillnodes.coord[0][np4])*0.25;
	  xx[6]=(maillnodes.coord[1][np1]+maillnodes.coord[1][np2]+
		 maillnodes.coord[1][np3]+maillnodes.coord[1][np4])*0.25;
	  xx[7]=(maillnodes.coord[2][np1]+maillnodes.coord[2][np2]+
		 maillnodes.coord[2][np3]+maillnodes.coord[2][np4])*0.25;
	}
    }
  else
    {
      if (SYRTHES_LANG == FR)
	printf(" %%%% ERREUR extreme1 : mauvais parametre de type ");
      else if (SYRTHES_LANG == EN)
	printf(" %%%% Error extreme1 : bad type parameter ");
      syrthes_exit(1);
    }

  if (sdparall.nparts==1 || sdparall.rang==0)
    {
      for (i=0;i<8;i++) xxabs[i]=xx[i];
      
      /* reception des donnees */
      for (n=1;n<sdparall.nparts;n++)
        {
#ifdef _SYRTHES_MPI_
          /*  reception des autres proc */
          MPI_Recv(xx,8,MPI_DOUBLE,n,0,sdparall.syrthes_comm_world,&status);	
#endif  
	  if (xx[0]<xxabs[0])
	    for (i=0;i<4;i++) xxabs[i]=xx[i];
	  if (xx[4]>xxabs[4])
	    for (i=4;i<8;i++) xxabs[i]=xx[i];
        }
    }

#ifdef _SYRTHES_MPI_
  else if (sdparall.nparts>1 && sdparall.rang>0)
    MPI_Send(xx,8,MPI_DOUBLE,0,0,sdparall.syrthes_comm_world);
#endif

}
 
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |       Calcul des extremaux d'une variable 1D                         |
  |======================================================================| */
void extreme_partial(int type,int nb,double *var,int *numglob,
		     struct Maillage maillnodes,double *xxabs,struct SDparall sdparall)
{
  int i,n,n1,n2,n1g,n2g,np1,np2,np3,np4,nball;
  double *t,xmin=1.e10,xmax=-273.,xx[8];


  nball=somme_int_parall(nb);

  /* si aucune variable du type nulle part --> rien a faire */
  if (!nball) return;


  if (nb>0) /* en // il est possible que la fonction soit appellee sur 0 elts */
    {
      for (i=n1=n2=0,t=var;i<nb;i++,t++)
	{
	  if (*t>xmax) {xmax=*t;n2=i;}
	  if (*t<xmin) {xmin=*t;n1=i;}
	}
      
      if (type==0) /* extrema sur les noeuds */
	{
	  
	  n1g=numglob[n1];
	  xx[0]=xmin; 
	  xx[1]=maillnodes.coord[0][n1g];
	  xx[2]=maillnodes.coord[1][n1g];
	  if (maillnodes.ndim==3) xx[3]=maillnodes.coord[2][n1g]; else xx[3]=0.;
	  
	  n2g=numglob[n2];
	  xx[4]=xmax;
	  xx[5]=maillnodes.coord[0][n2g];
	  xx[6]=maillnodes.coord[1][n2g];
	  if (maillnodes.ndim==3) xx[7]=maillnodes.coord[2][n2g]; else xx[7]=0.;
	}
      else if (type==1)  /* extrema sur les elements */
	{
	  if (maillnodes.ndim==2)
	    {
	      n1g=numglob[n1];
	      np1=maillnodes.node[0][n1g];
	      np2=maillnodes.node[1][n1g];
	      np3=maillnodes.node[2][n1g];
	      xx[0]=xmin; 
	      xx[1]=(maillnodes.coord[0][np1]+maillnodes.coord[0][np2]+
		     maillnodes.coord[0][np3])/3.;
	      xx[2]=(maillnodes.coord[1][np1]+maillnodes.coord[1][np2]+
		     maillnodes.coord[1][np3])/3.;
	      
	      n2g=numglob[n2];
	      np1=maillnodes.node[0][n2g];
	      np2=maillnodes.node[1][n2g];
	      np3=maillnodes.node[2][n2g];
	      xx[4]=xmax; 
	      xx[5]=(maillnodes.coord[0][np1]+maillnodes.coord[0][np2]+
		     maillnodes.coord[0][np3])/3.;
	      xx[6]=(maillnodes.coord[1][np1]+maillnodes.coord[1][np2]+
		     maillnodes.coord[1][np3])/3.;
	      
	    }
	  else
	    {
	      n1g=numglob[n1];
	      np1=maillnodes.node[0][n1g];
	      np2=maillnodes.node[1][n1g];
	      np3=maillnodes.node[2][n1g];
	      np4=maillnodes.node[3][n1g];
	      xx[0]=xmin; 
	      xx[1]=(maillnodes.coord[0][np1]+maillnodes.coord[0][np2]+
		     maillnodes.coord[0][np3]+maillnodes.coord[0][np4])*0.25;
	      xx[2]=(maillnodes.coord[1][np1]+maillnodes.coord[1][np2]+
		     maillnodes.coord[1][np3]+maillnodes.coord[1][np4])*0.25;
	      xx[3]=(maillnodes.coord[2][np1]+maillnodes.coord[2][np2]+
		     maillnodes.coord[2][np3]+maillnodes.coord[2][np4])*0.25;
	      
	      n2g=numglob[n2];
	      np1=maillnodes.node[0][n2g];
	      np2=maillnodes.node[1][n2g];
	      np3=maillnodes.node[2][n2g];
	      np4=maillnodes.node[3][n2g];
	      xx[4]=xmax; 
	      xx[5]=(maillnodes.coord[0][np1]+maillnodes.coord[0][np2]+
		     maillnodes.coord[0][np3]+maillnodes.coord[0][np4])*0.25;
	      xx[6]=(maillnodes.coord[1][np1]+maillnodes.coord[1][np2]+
		     maillnodes.coord[1][np3]+maillnodes.coord[1][np4])*0.25;
	      xx[7]=(maillnodes.coord[2][np1]+maillnodes.coord[2][np2]+
		     maillnodes.coord[2][np3]+maillnodes.coord[2][np4])*0.25;
	    }
	}
      else
	{
	  if (SYRTHES_LANG == FR)
	    printf(" %%%% ERREUR extreme1 : mauvais parametre de type ");
	  else if (SYRTHES_LANG == EN)
	    printf(" %%%% Error extreme1 : bad type parameter ");
	  syrthes_exit(1);
	}
    }

  /* si aucune variable du type sur cette part, on met les min/max aux extremes */
  if (nb==0) {xxabs[0]=xx[0]=xmin; xxabs[4]=xx[4]=xmax;}

  /* les min-max globaux sont faits a la main pour avoir */
  /* en meme temps leur localidation                     */
  if (sdparall.nparts==1 || sdparall.rang==0)
    {
      for (i=0;i<8;i++) xxabs[i]=xx[i];
      
      /* reception des donnees */
      for (n=1;n<sdparall.nparts;n++)
        {
#ifdef _SYRTHES_MPI_
          /*  reception des autres proc */
          MPI_Recv(xx,8,MPI_DOUBLE,n,0,sdparall.syrthes_comm_world,&status);	
#endif  
	  if (xx[0]<xxabs[0])
	    for (i=0;i<4;i++) xxabs[i]=xx[i];
	  if (xx[4]>xxabs[4])
	    for (i=4;i<8;i++) xxabs[i]=xx[i];
        }
    }
#ifdef _SYRTHES_MPI_
  else if (sdparall.nparts>1 && sdparall.rang>0)
    MPI_Send(xx,8,MPI_DOUBLE,0,0,sdparall.syrthes_comm_world);
#endif

}
 

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Calcul des champs max en temps                                |
  |======================================================================| */
void calchampmax(int npoin,double *tmps, double *tmpmin, double *tmpmax)
{
  int i;
  double *p;
  
  for (i=0,p=tmps;i<npoin;i++,p++)
    {
      if (*p>*(tmpmax+i)) *(tmpmax+i)=*p;
      if (*p<*(tmpmin+i)) *(tmpmin+i)=*p;
    }
}
