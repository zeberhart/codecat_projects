/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * vsdump: test program to dump and parse content of vsd file
 *
 * Copyright (C) 2006-2007	Valek Filippov (frob@df.ru)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 3 or later of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 */
#include <gsf/gsf-utils.h>
#include "vsdump.h"
#include "vsd_dump_stream_15.h"
#include "vsd_parse_chunks.h"

/*-----------------------------------------------------------------------------*/
int
vsd_dump_stream15(GByteArray *stream, int version, FILE *fdraw, char *dirname, GHashTable *vaetbls)
{
	guint	i = 0;
	int	num = 1;
	GArray	*chunk_strm = NULL;
	char *ext="img";
	guint numofparts = 1, counter = 0;

	chunk_strm = g_array_new(FALSE, FALSE, sizeof(VsdChunk));
	
	while(i < stream->len){
		if(vsd_parse_chunk(chunk_strm, stream, dirname, fdraw, vaetbls, &i, &num, version, &ext, &numofparts, &counter)){
			g_warning("Problem with stream %x! Position: 0x%x", stream->data[0], i);
			g_array_free(chunk_strm, TRUE);
			return 1;
		}
	}
/* NEED TO CALL SOME KIND OF VSD_CHUNK_PRINT function here */
	g_array_free(chunk_strm, TRUE);
	
return 0;
}
