#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------
#
#                         SYRTHES version 4.X.X.
#                         -------------------
#
#     This file is part of the SYRTHES Kernel, element of the
#     thermal code SYRTHES.
#
#     Copyright (C) 2009 EDF S.A., France
#
#     contact: syrthes-support@edf.fr
#
#
#     The SYRTHES Kernel is free software; you can redistribute it
#     and/or modify it under the terms of the GNU General Public License
#     as published by the Free Software Foundation; either version 2 of
#     the License, or (at your option) any later version.
#
#     The SYRTHES Kernel is distributed in the hope that it will be
#     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#
#     You should have received a copy of the GNU General Public License;
#     if not, write to the
#     Free Software Foundation, Inc.,
#     51 Franklin St, Fifth Floor,
#     Boston, MA  02110-1301  USA
#
#-----------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Library modules import
# 
#-------------------------------------------------------------------------------
import sys
#
if sys.version_info[:2] < (2,3):
    sys.stderr.write("This script needs Python 2.3 at least\n")
    
import os, re, os.path, shutil
import time, getpass
#
import platform
#
if platform.system == 'Windows':
    sys.stderr.write("This script only works on Unix-like platforms\n")


#-------------------------------------------------------------------------------
# Global methods
#-------------------------------------------------------------------------------
def main():
    #
    #
    # install
    # -------
    setupsyrthesprofile = Setupsyrthesprofile()


#-------------------------------------------------------------------------------
# Class definition Setupsyrthesprofile
#
# 
#-------------------------------------------------------------------------------
class Setupsyrthesprofile(object):
    def __init__(self, parent=None):
        self.parent = parent

    #----------------------------------------------------------------------------    
    #  Fonction configuration 
    #  Adjust libraries location and environment variables 
    #----------------------------------------------------------------------------
    def configuration(self, Setupsyrthesprofile):
        #
        # syrthes.profile file update
        #	
	print "!!! ",self.installPath+'/syrthes.profile_'+self.arch
	profileFile = file(self.installPath+'/syrthes.profile_'+self.arch, mode='w')

        profileFile.write("""#!/bin/bash
#-----------------------------------------------------------------------
#
#                        SYRTHES version 4.X
#                        -------------------
#
#     This file is part of the SYRTHES Kernel, element of the
#     thermal code SYRTHES.
#
#     Copyright (C) 2009 EDF S.A., France
#
#     contact: syrthes-support@edf.fr
#
#
#     The SYRTHES Kernel is free software; you can redistribute it
#     and/or modify it under the terms of the GNU General Public License
#     as published by the Free Software Foundation; either version 2 of
#     the License, or (at your option) any later version.
#
#     The SYRTHES Kernel is distributed in the hope that it will be
#     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#
#     You should have received a copy of the GNU General Public License
#     along with the Code_Saturne Kernel; if not, write to the
#     Free Software Foundation, Inc.,
#     51 Franklin St, Fifth Floor,
#     Boston, MA  02110-1301  USA
#
#
#-----------------------------------------------------------------------

# Architecture name and location of the directory for installation
# ================================================================

""")
        
        #profileFile.write("DEST_LIB="+self.installPath+'/../arch/'+self.arch+'lib'+"\n")
        print "SYRTHES4_HOME="+self.syrthesDir+"/arch/"+self.arch
        print "NOM_ARCH="+self.arch
        profileFile.write("SYRTHES4_HOME="+self.syrthesDir+"/arch/"+self.arch+"\n")
        profileFile.write("NOM_ARCH="+self.arch+"\n")
    
        profileFile.write(
"""      
#-----------------------------------------------------------
# LD_LIBRARY_PATH 
#-----------------------------------------------------------
""")
        for key in self.dicolib.keys():
            if key != 'specific_inc' and key != 'specific_lib' and self.dicolib[key].has_key('PATH'):
                line='LD_LIBRARY_PATH='+self.dicolib[key]['PATH']+'/lib:$LD_LIBRARY_PATH'
                profileFile.write("#"+key+"\n")
                profileFile.write(line)
                profileFile.write("\n")

        profileFile.write(
"""
#-----------------------------------------------------------
# MPI Path 
#-----------------------------------------------------------
""")
        if self.dicolib['mpi']['USE'].upper()=='YES':
            profileFile.write('SYRTHES4_MPIPATH='+self.dicolib['mpi']['PATH'])
            profileFile.write(
"""
#-----------------------------------------------------------
# Path 
#-----------------------------------------------------------
PATH=${SYRTHES4_HOME}/bin:$PATH
""")
            #profileFile.write('PATH='+self.dicolib['mpi']['PATH']+'/bin'+':$PATH')
            if self.dicolib['mpi']['PATH']=='/usr':
               profileFile.write('# For MPI in /usr # IF You use your bin or your local/bin exec\n') 
               profileFile.write('#PATH='+self.dicolib['mpi']['PATH']+'/bin'+':$PATH \n') 
            else:
               profileFile.write('PATH='+self.dicolib['mpi']['PATH']+'/bin'+':$PATH \n')                
        else:
            profileFile.write('SYRTHES4_MPIPATH=')


            
        profileFile.write(
"""
#-----------------------------------------------------------
# Export variables
#-----------------------------------------------------------
export NOM_ARCH SYRTHES4_HOME LD_LIBRARY_PATH PATH SYRTHES4_MPIPATH

#-----------------------------------------------------------
# Display 
#-----------------------------------------------------------
echo " "
""")
        profileFile.write(
"""
echo "SYRTHES4_HOME=" ${SYRTHES4_HOME}
echo "NOM_ARCH=     " ${NOM_ARCH}
echo "PATH=         " ${PATH}
echo "LD_LIBRARY_PATH="${LD_LIBRARY_PATH}          
echo " " 
""")
        profileFile.close()

#-------------------------------------------------------------------------------
# 
# Progam principal
# 
#
#
#-------------------------------------------------------------------------------
if __name__ == '__main__':
        main()
