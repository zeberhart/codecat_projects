#!/bin/sh
# vim:set sw=2 sts=2 et:

set -e

quake_atomic_write () {
  ## quake_atomic_write DIR FILE CONTENTS
  ## Create DIR/FILE with contents (CONTENTS + '\n').
  install -d "$1"
  echo "$3" > "$1/$2.$$.tmp"
  chmod 0644 "$1/$2.$$.tmp"
  mv "$1/$2.$$.tmp" "$1/$2"
}

quake_disable () {
  update-rc.d openarena-server disable

  # Make deb-systemd-helper think the systemd unit was already installed
  # and disabled, so that the disabledness sticks. Strictly speaking this
  # is internal state, but it replicates what init-system-helpers/1.22
  # would have done, so it should work as long as upgrades from jessie
  # are supported.
  quake_atomic_write /var/lib/systemd/deb-systemd-helper-enabled \
    openarena-server.service.dsh-also \
    /etc/systemd/system/multi-user.target.wants/openarena-server.service
  if [ -L /etc/systemd/system/multi-user.target.wants/openarena-server.service ]; then
    rm /etc/systemd/system/multi-user.target.wants/openarena-server.service
  fi
}

quake_migrate_disabledness () {
  echo "openarena-server.preinst: migrating from START_DAEMON to init script enable/disable status..." >&2

  e=0
  # Do this in a subshell just in case /etc/default/openarena-server has been
  # patched by the sysadmin to exit the init script.
  (
    set +e
    START_DAEMON=1
    [ -r /etc/default/openarena-server ] && . /etc/default/openarena-server

    case "$START_DAEMON" in
      (1)
        echo "Preserving current status of openarena-server init script (START_DAEMON=1 in /etc/default/openarena-server)" >&2
        exit 104
        ;;
      (unless-disabled-by-upgrade)
        if [ -e /var/games/openarena-server/init-script-disabled-by-upgrade ]; then
          echo "Disabling openarena-server init script to preserve default from openarena-server (<< 0.8.8)" >&2
          exit 101
        else
          echo "Preserving current status of openarena-server init script" >&2
          exit 104
        fi
        ;;
      (*)
        # 0 or unknown
        echo "Disabling openarena-server init script (previously disabled by START_DAEMON=$START_DAEMON in /etc/default/openarena-server)" >&2
        exit 101
        ;;
    esac
  ) || e="$?"

  case "$e" in
    (101)
      # we already printed a message
      quake_disable
      ;;
    (104)
      # nothing to do and we already printed a message
      ;;
    (*)
      # maybe the sysadmin stubbed it out with "exit 0" or "exit 1"?
      echo "Sourcing /etc/default/openarena-server returned unexpected code, disabling init script" >&2
      quake_disable
      ;;
  esac

  # we have done the migration, so remove this to avoid confusion
  rm -f /var/games/openarena-server/init-script-disabled-by-upgrade
}

case "$1" in
  (upgrade)
    if dpkg --compare-versions "$2" lt 0.8.8-10~; then
      quake_migrate_disabledness "$@"
    fi
  ;;
  (install|abort-upgrade)
  ;;
  *)
    echo "preinst called with unknown argument '$1'" >&2
    exit 1
  ;;
esac

#DEBHELPER#
