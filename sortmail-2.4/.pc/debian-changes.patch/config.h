/*	@(#)config.h 1.4 02/03/11	*/
/*	$Id: config.h,v 1.1.1.1 2002/03/12 05:38:52 efalk Exp $	*/

	/* system-dependent defaults */


#if defined(SUNOS)	/* SunOS 4.x */
# define	SPOOLDIR	"/usr/spool/mail/"
# define	VACATION	"/usr/bin/vacation"
# define	LOCKLOCKF	0	/* Don't use lockf() */
# define	LOCKFLOCK	0	/* Use flock() */
# define	LOCKFCNTL	0	/* Use fcntl() */
# define	LOCKDOTLOCK	1	/* Use mailboxname.lock file */
# define	BOUNCECHECK	1	/* use DBM-based bounce-detection */
# define	NOREGCOMP
#endif

#if defined(SOLARIS)
# define	SPOOLDIR	"/usr/mail/"
# define	VACATION	"/usr/bin/vacation"
# define	LOCKLOCKF	0
# define	LOCKFLOCK	0
# define	LOCKFCNTL	0	/* See README r.e. locking */
# define	LOCKDOTLOCK	1
# define	BOUNCECHECK	1	/* use DBM-based bounce-detection */
# define	HSTERROR	1	/* has strerror() function */
# define	NOSETENV
extern	char	*sys_errlist[] ;
#endif

#if defined(HPUX)
# define	SPOOLDIR	"/usr/mail/"
# define	VACATION	"/usr/bin/vacation"
# define	LOCKLOCKF	0
# define	LOCKFLOCK	0
# define	LOCKFCNTL	0
# define	LOCKDOTLOCK	1
# define	BOUNCECHECK	1	/* use DBM-based bounce-detection */
# define	HSTERROR	1	/* has strerror() function */
extern	char	*sys_errlist[] ;
#endif

#if defined(LINUX)
# define	SPOOLDIR	"/var/spool/mail/"
# define	VACATION	"/usr/bin/vacation"
# define	LOCKLOCKF	0
# define	LOCKFLOCK	0
# define	LOCKFCNTL	0
# define	LOCKDOTLOCK	1
# define	BOUNCECHECK	1	/* use GDBM-based bounce-detection */
# define	HSTERROR	1	/* has strerror() function */
#endif

#if defined(ULTRIX)
# define	STRDUP		0	/* no strdup on this system */
#endif



#ifndef	SPOOLDIR
# define	SPOOLDIR	"/usr/spool/mail/"
#endif

#ifndef	VACATION
# define	VACATION	"/usr/ucb/vacation"
#endif

#ifndef	LOCKLOCKF
# define	LOCKLOCKF	0
#endif
#ifndef	LOCKFLOCK
# define	LOCKFLOCK	0
#endif
#ifndef	LOCKFCNTL
# define	LOCKFCNTL	0
#endif
#ifndef	LOCKDOTLOCK
# define	LOCKDOTLOCK	1
#endif

#ifndef	STRDUP
# define	STRDUP		1	/* default is have strdup */
#endif

#ifndef	STRERROR
# define	STRERROR	1	/* default is have sterror */
#endif

#ifndef	HSTRERROR
# define	HSTRERROR	0	/* default is don't have hsterror */
#endif

#ifndef	BOUNCECHECK
# define	BOUNCECHECK	1	/* default is compile bouncecheck */
#endif

#define	FOLDER		"folders"

#ifndef	SENDMAIL
#define	SENDMAIL	"/usr/lib/sendmail -om -oi"
#endif

