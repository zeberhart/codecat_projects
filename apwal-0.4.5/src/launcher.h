/* launcher.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#ifndef LAUNCHER__H
#define LAUNCHER__H
#include <gtk/gtk.h>

#include "common.h"

// ----------------------------------------------------------------------------
#define ICON_WIDTH     48
#define ICON_HEIGHT    48

// ----------------------------------------------------------------------------
typedef struct launcher_t
{
  GtkWidget *window;
  GtkWidget *event_box;
  GtkWidget *image;
  GdkPixbuf *pixbuf;
  GdkBitmap *bitmap_mask;

  gint       x;
  gint       y;
  guint      width;
  guint      height;

  guint      xwidth;
  guint      xheight;

  guint      timeout_activated;
  guint      timeout_handler_id;

  gboolean   editor_started;

  app_list_t *apps;

  struct apwal_pref_t *apwal_pref;

} launcher_t;

// ----------------------------------------------------------------------------
launcher_t* launcher_new(void);
void launcher_load_apps(launcher_t *l, app_list_t *apps);
// ----------------------------------------------------------------------------
void launcher_timeout_start(launcher_t *l);
void launcher_timeout_stop(launcher_t *l);
gint launcher_timeout_function(gpointer data);
gboolean launcher_button_press_event(GtkWidget *widget,
                                     GdkEventButton *event, launcher_t *l);
gboolean launcher_button_release_event(GtkWidget *widget,
                                       GdkEventButton *event, launcher_t *l);
gboolean launcher_enter_notify_event(GtkWidget *widget,
                                     GdkEventCrossing *event,
                                     launcher_t *l);
gboolean launcher_leave_notify_event(GtkWidget *widget,
                                     GdkEventCrossing *event,
                                     launcher_t *l);

// ----------------------------------------------------------------------------
#endif /*LAUNCHER__H*/
