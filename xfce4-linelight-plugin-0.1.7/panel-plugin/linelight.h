/*
 *  Copyright 2008 Michael Pfeuti <m_pfeuti@students.unibe.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#ifndef LINELIGHT_H_INCLUDED
#define LINELIGHT_H_INCLUDED

#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <glib.h>

#include "defines.h"

typedef struct _appData
{
    unsigned int listPointer[SECTION_COUNT];
    GPtrArray *listSectionPaths[SECTION_COUNT];
    unsigned int search_result_count[SECTION_COUNT]; //counter that max_search_res is not exceeded
    unsigned int max_search_results;
    unsigned int all_found_results[SECTION_COUNT];
    GtkWidget *text_window;
    GtkListStore *list;
    GtkWidget *text_field;
    int section_enabled[SECTION_COUNT];
    int x,y;        //coordination of the text_window
    GrowingCorner grow_corner; //corner which is fixed
    int decorated; //frame decoration
    unsigned int min_char_for_search;
} LineLightData;

LineLightData* linelight_new(GtkWidget *window, GtkListStore *list);
void linelight_free(LineLightData *data);
void linelight_clear_list(LineLightData *data);
void linelight_add_search_result(LineLightData *data, char *result);
void linelight_toggle_show_hide(LineLightData *data);
int linelight_get_result_section(LineLightData *data, char *result);
void linelight_update_coordinates(LineLightData *data);

#endif // LINELIGHT_H_INCLUDED
