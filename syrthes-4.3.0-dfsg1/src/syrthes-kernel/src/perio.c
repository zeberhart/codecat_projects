/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <math.h>

#include "syr_usertype.h"
# include "syr_bd.h"
# include "syr_const.h"
# include "syr_abs.h"
# include "syr_tree.h"
# include "syr_proto.h"
# include "syr_option.h"

extern struct Performances perfo;
extern struct Affichages affich;

extern char nomdata[CHLONG];
extern FILE *fdata;

static char ch[CHLONG],motcle[CHLONG],repc[CHLONG];
static double dlist[100];
static int ilist[100];

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  recherche s'il y a des periodicites declarees                       |
  |  dans le fichier de donnees                                          |
  |======================================================================| */
int existe_perio()
{
  int ok,n,i,j,nr,numvar;
  int i1,i2,i3,i4,id;
  double val;

  ok=0;

  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"CLIM")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"PERIODICITE_2D") || 
		  !strcmp(motcle,"PERIODICITE_3D"))
		ok=1; 
	    }
	}
    }

  if (ok) return 1;
  else return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_perio(struct Cperio *perio,
		struct Maillage maillnodes,struct MaillageBord maillnodebord,
		struct SDparall sdparall)
{
  int i,j,k;
  int i1,i2,i3,i4,id,ii,nb,nr,n,it;
  int *itrav;
  double zero=0.;
  int *np1,*np2,nn1,nn2;

  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"CLIM")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"PERIODICITE_2D")) 
		{
		  extr_motcle(motcle,ch+id,&i3,&i4);
		  id+=i4+1;
		  
		  if (!strcmp(motcle,"T")) 
		    {
		      it=0;
		      rep_ndbl(2,dlist,&ii,ch+id);
		      rep_listint(ilist,&nb,ch+id+ii);
		      cree_liste_noeuds_periorc(maillnodebord,maillnodes,POSPERIO,
					      ilist,nb,&nn1,&nn2,&np1,&np2);
		      cree_paires_noeuds_perio(maillnodes,nn1,nn2,np1,np2,perio,
					       it,dlist[0],dlist[1],zero,
					       zero,zero,zero,zero);
		      free(np1); free(np2);
		    }
		  else if (!strcmp(motcle,"R")) 
		    {
		      it=1;
		      rep_ndbl(3,dlist,&ii,ch+id);
		      rep_listint(ilist,&nb,ch+id+ii);
		      cree_liste_noeuds_periorc(maillnodebord,maillnodes,POSPERIO,
					      ilist,nb,&nn1,&nn2,&np1,&np2);
		      cree_paires_noeuds_perio(maillnodes,nn1,nn2,np1,np2,perio,
					      it,dlist[0],dlist[1],dlist[2],
					      zero,zero,zero,zero);

		      free(np1); free(np2);
		    }
		  else if (!strcmp(motcle,"U")) 
		    {
		      rep_listint(ilist,&nb,ch+id);
		      cree_liste_noeuds_periorc(maillnodebord,maillnodes,POSPERIO,
					      ilist,nb,&nn1,&nn2,&np1,&np2);
		      cree_paires_noeuds_perio(maillnodes,nn1,nn2,np1,np2,perio,
					       it,zero,zero,zero,zero,zero,zero,zero);
		      free(np1); free(np2);
		    }
		  else
		    {
		      printf("     dans le message  motcle=%s\n",motcle);
		      
		      if (sdparall.nparts==1 ||sdparall.rang==0){
			if (SYRTHES_LANG == FR)
			  {
			    printf("\n !!! ERREUR lors de la lecture des conditions aux limites\n");
			    printf("            Le mot-cle suivant n'est pas reconnu :\n");
			    printf("            --> CLIM=  PERIODICITE_2D %s\n",motcle);
			  }  
			else if (SYRTHES_LANG == EN)
			  {
			    printf("\n !!! ERROR while reading boundary conditions\n");
			    printf("            The following keyword is not recognized :\n");
			    printf("            --> CLIM=  PERIODICITE_2D %s\n",motcle);
			  }  
		      }
		      syrthes_exit(1);
		    }   

		}
	      else if (!strcmp(motcle,"PERIODICITE_3D")) 
		{
		  extr_motcle(motcle,ch+id,&i3,&i4);
		  id+=i4+1;
		  
		  if (!strcmp(motcle,"T")) 
		    {
		      it=0;
		      rep_ndbl(3,dlist,&ii,ch+id);
		      rep_listint(ilist,&nb,ch+id+ii);
		      cree_liste_noeuds_periorc(maillnodebord,maillnodes,POSPERIO, 
					      ilist,nb,&nn1,&nn2,&np1,&np2);
		      cree_paires_noeuds_perio(maillnodes,nn1,nn2,np1,np2,perio,
					       it,dlist[0],dlist[1],dlist[2],
					       zero,zero,zero,zero);
		      free(np1); free(np2);
		    }
		  else if (!strcmp(motcle,"R")) 
		    {
		      it=1;
		      rep_ndbl(7,dlist,&ii,ch+id);
		      rep_listint(ilist,&nb,ch+id+ii);
		      cree_liste_noeuds_periorc(maillnodebord,maillnodes,POSPERIO,
					      ilist,nb,&nn1,&nn2,&np1,&np2);
		      cree_paires_noeuds_perio(maillnodes,nn1,nn2,np1,np2,perio,
					       it,dlist[0],dlist[1],dlist[2],
					       dlist[3],dlist[4],dlist[5],dlist[6]);
		      
		      free(np1); free(np2);
		    }
		  else if (!strcmp(motcle,"U")) 
		    {
		      rep_listint(ilist,&nb,ch+id);
		      cree_liste_noeuds_periorc(maillnodebord,maillnodes,POSPERIO,
					      ilist,nb,&nn1,&nn2,&np1,&np2);
		      cree_paires_noeuds_perio(maillnodes,nn1,nn2,np1,np2,perio,
					       it,zero,zero,zero,zero,zero,zero,zero);
		      free(np1); free(np2);
		    }
		  else
		    {
		      if (sdparall.nparts==1 ||sdparall.rang==0){
			if (SYRTHES_LANG == FR)
			  {
			    printf("\n !!! ERREUR lors de la lecture des conditions aux limites\n");
			    printf("            Le mot-cle suivant n'est pas reconnu :\n");
			    printf("            --> CLIM=  PERIODICITE_3D %s\n",motcle);
			  }  
			else if (SYRTHES_LANG == EN)
			  {
			    printf("\n !!! ERROR while reading boundary conditions\n");
			    printf("            The following keyword is not recognized :\n");
			    printf("            --> CLIM=  PERIODICITE_3D %s\n",motcle);
			  }  
		      }
		      syrthes_exit(1);
		    }   
		}
	    }
	}
    }

  if (SYRTHES_LANG == FR)
    {
      printf("\n *** PERIODICITE\n");
      printf("       - nombre final de noeuds periodiques  : %d\n",perio->npoin);
      printf("       - nombre final d'elements periodiques : %d\n",perio->nelem);
    }
  else if (SYRTHES_LANG == EN)
    {
      printf("\n *** PERIODICITY\n");
      printf("       - total number of periodic nodes  : %d\n",perio->npoin);
      printf("       - total number of periodic elements : %d\n",perio->nelem);
    }
  fflush(stdout);
} 
  
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | cree_liste_perio                                                     |
  |         Recherche des faces periodiques correspondantes              |
  |======================================================================| */
void cree_liste_noeuds_periorc(struct MaillageBord maillnodebord,
			       struct Maillage maillnodes,int POSTYPE,
			       int *iref,int nb,
			       int *nn1,int *nn2,int **np1,int **np2)
{
  /* ici POSTYPE vaut POSPERIO ou POSRESCON */
  int i,j,n,npl1,npl2,nb1,nb2,np,ng,ok;
  int *numel1,*numel2;
  int n1,n2,nr,l1,l2;
  int *per,nn,ng1,ng2;


  /* verif qu'il existe bien un "-1" dans la liste des ref */
  for (ok=0,i=0;i<MAX_REF;i++) if (iref[i]==-1) ok=1;
  if (!ok)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n\n %%%% ERREUR cree_liste_perio : la liste des references ");
	  printf("des elements periodiques est incorrecte\n");
	  printf("                              elle doit avoir la forme suivante :\n");
	  printf("                                    liste_references_1 -1 liste_references_2\n");
	  printf("                                    exemple : 2 34 12 -1 5 32\n");
	  printf("                              --> verifier le fichier fichier de donnees\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n\n %%%% ERROR cree_liste_perio : The list of references ");
	  printf("of periodic element is wrong\n");
	  printf("                              it must have the fllowing form :\n");
	  printf("                                    reference_list_1 -1 reference_list_2\n");
	  printf("                                    example : 2 34 12 -1 5 32\n");
	  printf("                              --> Please, check the data file\n");
	}
      syrthes_exit (1);
    }
      

  /* creation des 2 groupes en recherchant le -1 dans la liste des references */
  nb1 = 0;
  while (iref[nb1]>0) nb1 += 1;
  nb2=nb-nb1-1;

  /* compte des elements periodiques */
  npl1=npl2=0;
  for (i=0;i<maillnodebord.nelem;i++)
    {
      l1=l2=0; nr=maillnodebord.nrefe[i];
      for (n=0;n<nb1;n++) if (nr==iref[n]) {l1=1; maillnodebord.type[i][POSTYPE]==1;}
      if (!l1) for (n=nb1+1;n<nb1+1+nb2;n++) if (nr==iref[n]) {l2=1; maillnodebord.type[i][POSTYPE]==1;}
      if (l1) {npl1++;}
      else if (l2) {npl2++;}
    }

  if (npl1==0 || npl2==0)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n\n %%%% ERREUR cree_liste_periorc : la liste des references ");
	  printf("des elements periodiques ou RC est incorrecte\n");
	  printf("                              on a trouve :\n");
	  printf("                                  %d elements periodiques/RC sur le cote 1\n",npl1);
	  printf("                                  %d elements periodiques/RC sur le cote 2\n",npl2);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n\n %%%% ERROR cree_liste_perio : The list of references ");
	  printf("of periodic element or contact resistance is wrong\n");
	  printf("                              one has found :\n");
	  printf("                                  %d periodic/CR elements on side 1\n",npl1);
	  printf("                                  %d periodic/CR elements on side 2\n",npl2);
	}
      syrthes_exit(1);
    }


  /* ------------------------------------------------------------- */
  /* construction de la liste des noeuds de bord periodiques ou RC */
  /* a partir de la liste des elements de bord periodiques         */
  /* --> np1[], np2[]                                              */
  /* ------------------------------------------------------------- */

  /* creation de la liste des elements de bord periodiques (a partir de nodebord) */
  numel1 = (int*) malloc(npl1*sizeof(int));  verif_alloue_int1d("cree_liste_noeuds_perio",numel1);
  numel2 = (int*) malloc(npl2*sizeof(int));  verif_alloue_int1d("cree_liste_noeuds_perio",numel2);
  perfo.mem_cond+=(npl1+npl2) * sizeof(int);
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  npl1=npl2=0;
  for (i=0;i<maillnodebord.nelem;i++)
    {
      l1=l2=0; nr=maillnodebord.nrefe[i];
      for (n=0;n<nb1;n++) if (nr==iref[n])l1=1;
      if (!l1) for (n=nb1+1;n<nb1+1+nb2;n++) if (nr==iref[n]) l2=1;
      if (l1) {numel1[npl1]=i; npl1++;}
      else if (l2) {numel2[npl2]=i; npl2++;}
    }
 
  /* a partir des elts de bord periodiques, on recherche les NOEUDS de bord periodiques */
  per=(int*)malloc(maillnodes.npoin*sizeof(int)); verif_alloue_int1d("cree_liste_noeuds_perio",per);
  perfo.mem_cond += maillnodes.npoin * sizeof(int);
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  for (i=0;i<maillnodes.npoin;i++) *(per+i)=0;
  for (i=0;i<npl1;i++) 
    for (j=0;j<maillnodebord.ndmat;j++) per[maillnodebord.node[j][numel1[i]]]=1;
  for (i=0;i<npl2;i++) 
    for (j=0;j<maillnodebord.ndmat;j++) 
      {
	/* traitement du noeud de jointure en fin de fissure : si un noeud appartient deja a la liste 1     */
        /* et qu'il appartient a la liste 2 : c'est le noeud de bout qu'il faut mettre dans les deux listes */
	if (per[maillnodebord.node[j][numel2[i]]]==1) per[maillnodebord.node[j][numel2[i]]]=3;
	else per[maillnodebord.node[j][numel2[i]]]=2;
      }

  for (i=*nn1=*nn2=0;i<maillnodes.npoin;i++) 
    if (per[i]==1) (*nn1)++;
    else if (per[i]==2) (*nn2)++;
    else if (per[i]==3) {(*nn1)++; (*nn2)++;}  /* il appartient aux 2 listes */

  free(numel1);free(numel2);
  perfo.mem_cond-=(npl1+npl2+maillnodes.npoin) * sizeof(int);




  /* creation des listes des noeuds de bord periodiques des 2 cotes */
  *np1=(int*)malloc(*nn1*sizeof(int));   verif_alloue_int1d("cree_liste_noeuds_perio",*np1);
  *np2=(int*)malloc(*nn2*sizeof(int));   verif_alloue_int1d("cree_liste_noeuds_perio",*np2);
  perfo.mem_cond += (*nn1+*nn2)*sizeof(int);
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  for (i=l1=l2=0;i<maillnodes.npoin;i++) 
    if (per[i]==1) {(*np1)[l1]=i;l1++;}
    else if (per[i]==2) {(*np2)[l2]=i;l2++;}
    else if (per[i]==3) {(*np1)[l1]=i;l1++;  (*np2)[l2]=i;l2++;}

  free(per);
  perfo.mem_cond-=maillnodes.npoin;
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | cree_liste_perio                                                     |
  |         Recherche des faces periodiques correspondantes              |
  |======================================================================| */

void cree_paires_noeuds_perio(struct Maillage maillnodes,
			      int nn1,int nn2,int *np1,int *np2,
			      struct Cperio *perio,int ntyper,
			      double tx,double ty,double tz,
			      double ax,double ay,double az,
			      double teta)
{
  int i,j,n,npl1,npl2,nb1,nb2,np,ng,num;
  int iminmin;
  int n1,n2,nr,l1,l2,err=0,ok,idebp,idebe;
  int nn,ng1,ng2;
  double x1,y1,z1,x2,y2,z2,x3,y3,z3,rien;
  double x11,y11,z11,d1,dmin,dminmin;
  double zero=0.;



  /* ----------------------------------------------------------------------- */
  /* remplisssage de la liste des noeuds de bord periodiques perio->nump[]   */
  /* et de leur correspondant perio->numpc[]                                 */
  /* ----------------------------------------------------------------------- */

  if (perio->npoin)
    {
      /* s'il y a deja des noeuds periodiques, on rajoute les nouveaux */
      idebp=perio->npoin;
      perio->npoin+=nn1+nn2;
      perio->nump=realloc(perio->nump,perio->npoin*sizeof(int)); verif_alloue_int1d("perio",perio->nump);
      perio->numpc=realloc(perio->numpc,perio->npoin*sizeof(int)); verif_alloue_int1d("perio",perio->numpc);
    }
  else
    {
      perio->npoin=nn1+nn2;
      perio->nump=(int*)malloc(perio->npoin*sizeof(int)); verif_alloue_int1d("perio",perio->nump);
      perio->numpc=(int*)malloc(perio->npoin*sizeof(int)); verif_alloue_int1d("perio",perio->numpc);
      idebp=0;
    }

  /* remplissage de la liste des noeuds de bord periodiques */
  for (i=n=0;i<nn1;i++) perio->nump[idebp+i]=np1[i];
  for (i=n=0;i<nn2;i++) perio->nump[idebp+nn1+i]=np2[i];

  perio->ndmat=maillnodes.ndmat;

  /* mise en correspondance des noeuds de bord periodiques */
  if (maillnodes.ndim==2)
    {
      dminmin=0.; iminmin=0;
      for (i=0;i<nn1;i++)
	{
	  ng1=np1[i]; x1=maillnodes.coord[0][ng1]; y1=maillnodes.coord[1][ng1]; 
	  if (ntyper==0)        {x11=x1+tx; y11=y1+ty; z11=0;}
	  else if (ntyper==1)   rotation2d(tx,ty,teta,x1,y1,&x11,&y11);
	  else if (ntyper==2)   {num=0;user_transfo_perio(maillnodes.ndim,num,
							  x1,y1,zero,&x11,&y11,&rien);}

	  ok=0; dmin=1.e8;
	  for (j=0;j<nn2;j++)
	    {
	      ng2=np2[j]; 
	      x2=maillnodes.coord[0][ng2]; y2=maillnodes.coord[1][ng2]; 
	      d1=(x11-x2)*(x11-x2)+(y11-y2)*(y11-y2);
	      if (d1<dmin) {ok=j; dmin=d1;}
	    }
	  perio->numpc[idebp+i]=np2[ok]; perio->numpc[idebp+nn1+ok]=ng1; 
	  if (dmin>dminmin) {dminmin=dmin;iminmin=idebp+i;}
	}
    }
  else
    {
      dminmin=0.; iminmin=0;
      for (i=0;i<nn1;i++)
	{
	  ng1=np1[i]; x1=maillnodes.coord[0][ng1]; y1=maillnodes.coord[1][ng1]; 
	  z1=maillnodes.coord[2][ng1];
	  if (ntyper == 0)     {x11=x1+tx; y11=y1+ty; z11=z1+tz;}
	  else if (ntyper==1)  rotation3d(tx,ty,tz,ax,ay,az,teta,x1,y1,z1,&x11,&y11,&z11);
	  else if (ntyper==2)  user_transfo_perio(maillnodes.ndim,num,
						  x1,y1,z1,&x11,&y11,&z11);
	  ok=0; dmin=1.e8;
	  for (j=0;j<nn2;j++)
	    {
	      ng2=np2[j]; 
	      x2=maillnodes.coord[0][ng2]; y2=maillnodes.coord[1][ng2]; z2=maillnodes.coord[2][ng2]; 
	      d1=(x11-x2)*(x11-x2)+(y11-y2)*(y11-y2)+(z11-z2)*(z11-z2);
	      if (d1<dmin) {ok=j; dmin=d1;}
	    }
	  perio->numpc[idebp+i]=np2[ok]; perio->numpc[idebp+nn1+ok]=ng1; 
	  if (dmin>dminmin) {dminmin=dmin;iminmin=idebp+i;}
	}
    }
  

  if (SYRTHES_LANG == FR)
    {
      printf("\n *** PERIODICITE\n");
      printf("       - nombre de noeuds periodiques         : %d\n",perio->npoin);
      printf("       - distance maximale de correspondance\n");
      printf("         des noeuds periodiques               : %f\n",dminmin);
      printf("         atteinte pour le couple de noeuds    : %d %d\n",perio->nump[iminmin],
                                                                   perio->numpc[iminmin]);
    }
  else if (SYRTHES_LANG == EN)
    {
      printf("\n *** PERIODICITY\n");
      printf("       - number of periodic nodes         : %d\n",perio->npoin);
      printf("       - maximum distance between correponding\n");
      printf("         periodic nodes               : %f\n",dminmin);
      printf("         for the couple of nodes    : %d %d\n",perio->nump[iminmin],
                                                                   perio->numpc[iminmin]);
    }
  fflush(stdout);

  if (!perio->npoin)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n\n %%%% ERREUR cree_liste_perio : une periodicite est declaree, mais on ne trouve");
	  printf(" aucun noeud periodique \n");
	  printf("            --> verifier les references du maillage\n");
	  printf("            --> verifier la donnee des references dans le fichier de donnees\n");
	  printf("            --> verifier la definition de la transformation periodique\n\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n\n %%%% ERROR cree_liste_perio : a periodicity is set, but no");
	  printf(" periodic nodes is found \n");
	  printf("            --> Please check the references in the mesh\n");
	  printf("            --> check the references data in the data file\n");
	  printf("            --> check the periodic transformation definition\n\n");
	}
      syrthes_exit(1);
    }
	
  if (affich.cond_perio)
    {
      if (SYRTHES_LANG == FR)
	printf("\n--> Periodicite : %d noeuds - calcul des correspondances des noeuds \n",perio->npoin);
      else if (SYRTHES_LANG == EN)
	printf("\n--> Periodicity : %d nodes - corresponding nodes calculation \n",perio->npoin);
      for (i=0;i<perio->npoin;i++)
	printf("i=%d num_glob=%d corr_glob=%d\n",i,perio->nump[i],perio->numpc[i]);
    }

}
      


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | prot2d                                                               |
  |         Rotation d'un point en 2D                                    |
  |======================================================================| */
void rotation2d(double px,double py,double teta,
		double x,double y,double *xt,double *yt)
{
  double alfa,t[2][2];


  alfa = teta*2*Pi/360.;

  t[0][0]=cos(alfa); t[0][1]=-sin(alfa);
  t[1][0]=-t[0][1] ; t[1][1]=t[0][0]   ;

  *xt = t[0][0]*x + t[0][1]*y + px;
  *yt = t[1][0]*x + t[1][1]*y + py;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | prot3d                                                               |
  |         Rotation d'un point en 3D                                    |
  |======================================================================| */
void rotation3d(double px, double py,double pz,
		double ax, double ay,double az,double teta,
		double x,  double y, double z, 
		double *xt, double *yt,double *zt)
{
  double an,phi,theta,c,s,c2,s2;
  double aa,bb,cc,dd,ee,ff,gg,hh,ii;
  double alfa,t[3][3],eps;

  alfa = teta*Pi/180.;
  eps=1.e-6;


  if (fabs(ax) >  eps)
    {
      an = sqrt( ax* ax+ ay* ay);
      phi = atan2( ay, ax); theta = atan2( az,an) ;
      c=cos(phi) ; s=sin(phi) ; c2=cos(theta) ; s2=sin(theta);
    }
  else if (fabs( ay) >  eps)
    {
      an = sqrt( ax* ax+ ay* ay);
      theta=atan2(az,an); c=0.; s=1.; c2=cos(theta); s2=sin(theta);
    }
  else
    {
      c=1 ; s=0 ; c2=0 ; s2=1 ;
    }

  aa =  c2*c ;
  bb = -c2*s ;
  cc =  s2 ;
  dd =  cos(alfa)*s+sin(alfa)*s2*c ;
  ee =  cos(alfa)*c-sin(alfa)*s*s2 ;
  ff =  -sin(alfa)*c2 ;
  gg =  sin(alfa)*s-cos(alfa)*s2*c ;
  hh =  sin(alfa)*c+cos(alfa)*s*s2 ;
  ii =  cos(alfa)*c2 ;

  t[0][0] = aa*aa+s*dd-c*s2*gg;
  t[1][1] = -s*c2*bb+c*ee+s*s2*hh;
  t[2][2] = s2*cc+c2*ii;
  t[1][0] = -s*c2*aa+c*dd+s*s2*gg;
  t[0][1] = aa*bb+s*ee-c*s2*hh;
  t[2][0] = s2*aa+c2*gg;
  t[0][2] = aa*cc+s*ff-c*s2*ii;
  t[2][1] = s2*bb+c2*hh;
  t[1][2] = -s*c2*cc+c*ff+s*s2*ii;

  *xt = t[0][0]*x+t[0][1]*y+t[0][2]*z + px; 
  *yt = t[1][0]*x+t[1][1]*y+t[1][2]*z + py;  
  *zt = t[2][0]*x+t[2][1]*y+t[2][2]*z + pz;

}
