CFLAGS=-O2 -Wall
CC=gcc
SBIN_DIR?=/usr/sbin
MAN_DIR?=/usr/share/man
DESTDIR?=""
INSTALL?=install
LIBS=-lmysqlclient -ldotconf -L/usr/lib/mysql -L/usr/lib64/mysql
EXEC=mysqmail-pure-ftpd-logger mysqmail-postfix-logger mysqmail-courier-logger mysqmail-dovecot-logger
MYOB=mydaemon.o myconfig.o
HFILES=mydaemon.h myconfig.h

all: $(EXEC)

mydaemon.o: mydaemon.c mydaemon.h

myconfig.o: myconfig.c myconfig.h

mysqmail-dovecot-logger.o: mysqmail-dovecot-logger.c $(HFILES)

mysqmail-pure-ftpd-logger.o: mysqmail-pure-ftpd-logger.c $(HFILES)

mysqmail-postfix-logger.o: mysqmail-postfix-logger.c $(HFILES)

mysqmail-courier-logger.o: mysqmail-courier-logger.c $(HFILES)

mysqmail-pure-ftpd-logger: mysqmail-pure-ftpd-logger.o $(MYOB) $(HFILES)
	$(CC) $< $(MYOB) $(LIBS) -o $@

mysqmail-postfix-logger: mysqmail-postfix-logger.o $(MYOB) $(HFILES)
	$(CC) $< $(MYOB) $(LIBS) -o $@

mysqmail-qmail-logger: mysqmail-qmail-logger.o $(MYOB) $(HFILES)
	$(CC) $< $(MYOB) $(LIBS) -o $@

mysqmail-courier-logger: mysqmail-courier-logger.o $(MYOB) $(HFILES)
	$(CC) $< $(MYOB) $(LIBS) -o $@

mysqmail-dovecot-logger: mysqmail-dovecot-logger.o $(MYOB) $(HFILES)
	$(CC) $< $(MYOB) $(LIBS) -o $@

strip: all
	for i in $(EXEC) ; do strip $${i} ; done

clean:
	rm -rf *.o $(EXEC)

install-pure: mysqmail-pure-ftpd-logger
	$(INSTALL) -D -m 0755 mysqmail-pure-ftpd-logger $(DESTDIR)$(SBIN_DIR)/mysqmail-pure-ftpd-logger
	$(INSTALL) -D -m 0644 doc/mysqmail-pure-ftpd-logger.8 $(DESTDIR)$(MAN_DIR)/man8/mysqmail-pure-ftpd-logger.8
	gzip -9 $(DESTDIR)$(MAN_DIR)/man8/mysqmail-pure-ftpd-logger.8

install-post: mysqmail-postfix-logger
	$(INSTALL) -D -m 0755 mysqmail-postfix-logger $(DESTDIR)$(SBIN_DIR)/mysqmail-postfix-logger
	$(INSTALL) -D -m 0644 doc/mysqmail-postfix-logger.8 $(DESTDIR)$(MAN_DIR)/man8/mysqmail-postfix-logger.8
	gzip -9 $(DESTDIR)$(MAN_DIR)/man8/mysqmail-postfix-logger.8

install-qmail: mysqmail-qmail-logger
	$(INSTALL) -D -m 0755 mysqmail-qmail-logger $(DESTDIR)$(SBIN_DIR)/mysqmail-qmail-logger
	$(INSTALL) -D -m 0644 doc/mysqmail-qmail-logger.8 $(DESTDIR)$(MAN_DIR)/man8/mysqmail-qmail-logger.8
	gzip -9 $(DESTDIR)$(MAN_DIR)/man8/mysqmail-qmail-logger.8

install-courier: mysqmail-courier-logger
	$(INSTALL) -D -m 0755 mysqmail-courier-logger $(DESTDIR)$(SBIN_DIR)/mysqmail-courier-logger
	$(INSTALL) -D -m 0644 doc/mysqmail-courier-logger.8 $(DESTDIR)$(MAN_DIR)/man8/mysqmail-courier-logger.8
	gzip -9 $(DESTDIR)$(MAN_DIR)/man8/mysqmail-courier-logger.8

install-dovecot: mysqmail-dovecot-logger
	$(INSTALL) -D -m 0755 mysqmail-dovecot-logger $(DESTDIR)$(SBIN_DIR)/mysqmail-dovecot-logger
	$(INSTALL) -D -m 0644 doc/mysqmail-dovecot-logger.8 $(DESTDIR)$(MAN_DIR)/man8/mysqmail-dovecot-logger.8
	gzip -9 $(DESTDIR)$(MAN_DIR)/man8/mysqmail-dovecot-logger.8

install-conf:
	$(INSTALL) -D -m 0640 doc/mysqmail.conf $(DESTDIR)$(ETCDIR)/mysqmail.conf

install: all
	$(MAKE) strip
	$(MAKE) DESTDIR=$(DESTDIR) SBIN_DIR=$(SBIN_DIR) MAN_DIR=$(MAN_DIR) INSTALL=$(INSTALL) install-pure
	$(MAKE) DESTDIR=$(DESTDIR) SBIN_DIR=$(SBIN_DIR) MAN_DIR=$(MAN_DIR) INSTALL=$(INSTALL) install-post
	$(MAKE) DESTDIR=$(DESTDIR) SBIN_DIR=$(SBIN_DIR) MAN_DIR=$(MAN_DIR) INSTALL=$(INSTALL) install-courier
	$(MAKE) DESTDIR=$(DESTDIR) SBIN_DIR=$(SBIN_DIR) MAN_DIR=$(MAN_DIR) INSTALL=$(INSTALL) install-dovecot

dist:
	./dist

rpm:
	$(MAKE) dist
	VERS=`head -n 1 debian/changelog | cut -d'(' -f2 | cut -d')' -f1 | cut -d'-' -f1` ; \
	PKGNAME=`head -n 1 debian/changelog | cut -d' ' -f1` ; \
	cd .. ; rpmbuild -ta $${PKGNAME}-$${VERS}.tar.gz

deb:
	if [ -z $(SIGN)"" ] ; then \
		./deb ; \
	else \
		./deb --sign ; \
	fi

.PHONY: clean install all strip dist rpm install-conf install-courier install-qmail install-post install-pure deb install-dovecot
