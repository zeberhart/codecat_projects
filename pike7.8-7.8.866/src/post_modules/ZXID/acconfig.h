/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: df562b991d12c9d9b4dcfce95a1bec5533b1b8b5 $
*/

#ifndef ZXID_CONFIG_H
#define ZXID_CONFIG_H

@TOP@
@BOTTOM@

/* Define this if you want the ZXID module. */
#undef HAVE_ZXID

#endif
