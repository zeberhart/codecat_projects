int vsd_parse_chunk(GArray *chunk_strm, GByteArray* stream, char *dirname, FILE *fdraw, GHashTable *vaetbls, int *offset, int *num, int version, char **ext, guint *numofparts, guint *counter);

/* the stream is a garray made of the 'VsdChunk' structs */
typedef struct{
	guint8	type;
	gpointer data; /* garray of VsdChunkItem structs */
} VsdChunk;

typedef struct{
	char		**name;
	char		**formula;
	guint8	type;	/* type of value */
	gpointer value;
} VsdChunkItem;

typedef struct{
    guint32     type;
    guint32     ix;
    guint32     unkn1;
    guint32     len;
    guint16     unkn2;
    guint8      unkn3;
} VsdChunkHdr;
