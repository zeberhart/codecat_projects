/* apwalpref.c
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#include "common.h"

static void apwal_pref_timeout_changed(GtkSpinButton *spin, apwalapp_t *apwal);
static void apwal_pref_exit_at_app_launch_toggled(GtkToggleButton *chk,
                                                  apwalapp_t *apwal);
static void apwal_pref_tooltips_toggled(GtkToggleButton *chk,
                                        apwalapp_t *apwal);
static void apwal_pref_iconsel_in_a_sepwin_toggled(GtkToggleButton *chk,
                                                   apwalapp_t *apwal);
static void apwal_pref_iconsel_modal_toggled(GtkToggleButton *chk,
                                             apwalapp_t *apwal);
// ----------------------------------------------------------------------------
apwal_pref_t * apwal_pref_new(gint timeout, gboolean exit_at_app_launch,
                              gboolean activate_tooltips,
                              gboolean iconsel_in_a_separate_window,
                              gboolean iconsel_modal)
{
  apwal_pref_t *apwal_pref;
  TRACE("%s", "");
  apwal_pref = (apwal_pref_t *) malloc(sizeof(apwal_pref_t));
  g_assert(apwal_pref != NULL);
  apwal_pref->timeout = timeout;
  apwal_pref->exit_at_app_launch = exit_at_app_launch;
  apwal_pref->activate_tooltips = activate_tooltips;
  apwal_pref->iconsel_in_a_separate_window = iconsel_in_a_separate_window;
  apwal_pref->iconsel_modal = iconsel_modal;
  apwal_pref->iconsel_modal_chk = NULL;
  return apwal_pref;
}
// ----------------------------------------------------------------------------
apwal_pref_t * apwal_pref_new_default(void)
{
  TRACE("%s", "");
  return apwal_pref_new(1000, FALSE, TRUE, TRUE, FALSE);
}
// ----------------------------------------------------------------------------
void apwal_pref_build_interface(apwalapp_t *apwal)
{
  GtkWidget *vbox;
  GtkWidget   *launcher_frame;
  GtkWidget     *launcher_vbox;
  GtkWidget     *timeout_hbox;
  GtkWidget       *timeout_event;
  GtkWidget         *timeout_label;
  GtkWidget       *timeout_spin;
  GtkWidget     *exit_at_app_launch_chk;
  GtkWidget   *editor_frame;
  GtkWidget     *editor_vbox;
  GtkWidget     *tooltips_chk;
  GtkWidget     *iconsel_in_a_sepwin_chk;
//GtkWidget     *iconsel_modal_chk;

  gdouble     timeout;

  g_assert(apwal != NULL);
  g_assert(apwal->apwal_pref != NULL && apwal->apwal_pref_frame != NULL &&
           apwal->tips != NULL);
  TRACE("%s", "");
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 4);
  gtk_container_add(GTK_CONTAINER(apwal->apwal_pref_frame), vbox);
  gtk_widget_show(vbox);

  // launcher frame
  launcher_frame = gtk_frame_new(" Launcher Preferences ");
  gtk_container_set_border_width(GTK_CONTAINER(launcher_frame), 4);
  gtk_box_pack_start(GTK_BOX(vbox), launcher_frame, FALSE, FALSE, 0);
  gtk_widget_show(launcher_frame);
  
  // launcher vbox
  launcher_vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(launcher_vbox), 4);
  gtk_box_set_spacing(GTK_BOX(launcher_vbox), 4);
  gtk_container_add(GTK_CONTAINER(launcher_frame), launcher_vbox);
  gtk_widget_show(launcher_vbox);

  // timeout hbox
  timeout_hbox = gtk_hbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(timeout_hbox), 4);
  gtk_box_set_spacing(GTK_BOX(timeout_hbox), 4);
  gtk_container_add(GTK_CONTAINER(launcher_vbox), timeout_hbox);
  gtk_widget_show(timeout_hbox);

  // timeout event box
  timeout_event = gtk_event_box_new();
  gtk_box_pack_start(GTK_BOX(timeout_hbox), timeout_event, FALSE, FALSE, 0);
  gtk_widget_show(timeout_event);
  gtk_tooltips_set_tip(apwal->tips, timeout_event, "Period of time during which the Apwal Launcher will still be displayed if not focused", NULL);

  // timeout label
  timeout_label = gtk_label_new("Launcher Timout (in second)");
  gtk_container_add(GTK_CONTAINER(timeout_event), timeout_label);
  gtk_widget_show(timeout_label);

  // timeout spin button
  timeout_spin = gtk_spin_button_new_with_range(0.0, 10.0, 0.1);
  g_signal_connect(G_OBJECT(timeout_spin), "value-changed",
                   G_CALLBACK(apwal_pref_timeout_changed), apwal);
  gtk_box_pack_start(GTK_BOX(timeout_hbox), timeout_spin, FALSE, FALSE, 0);
  gtk_widget_show(timeout_spin);

  timeout = ((double)(apwal->apwal_pref->timeout / 100))/10;
  TRACE("apwal_pref->timeout:%d, timeout:%f",
         apwal->apwal_pref->timeout, timeout);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(timeout_spin), timeout);

  // exit_at_app_launch
  exit_at_app_launch_chk = gtk_check_button_new_with_label(
                                  "Exit Apwal when an application is clicked");
  gtk_box_pack_start(GTK_BOX(launcher_vbox), exit_at_app_launch_chk,
                     FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(exit_at_app_launch_chk), "toggled",
                   G_CALLBACK(apwal_pref_exit_at_app_launch_toggled), apwal);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(exit_at_app_launch_chk),
                               apwal->apwal_pref->exit_at_app_launch);
  gtk_widget_show(exit_at_app_launch_chk);
  gtk_tooltips_set_tip(apwal->tips, exit_at_app_launch_chk, "When you launch an application from the Apwal Launcher you can either prefer that Apwal hides itself immediatly or not", NULL);

  // iconsel_modal
  // editor frame
  editor_frame = gtk_frame_new(" Editor Preferences ");
  gtk_container_set_border_width(GTK_CONTAINER(editor_frame), 4);
  gtk_box_pack_start(GTK_BOX(vbox), editor_frame, FALSE, FALSE, 0);
  gtk_widget_show(editor_frame);
  
  // editor vbox
  editor_vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(editor_vbox), 4);
  gtk_box_set_spacing(GTK_BOX(editor_vbox), 4);
  gtk_container_add(GTK_CONTAINER(editor_frame), editor_vbox);
  gtk_widget_show(editor_vbox);

  // tooltips
  tooltips_chk = gtk_check_button_new_with_label("Activate Tooltips");
  gtk_box_pack_start(GTK_BOX(editor_vbox), tooltips_chk, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(tooltips_chk), "toggled",
                   G_CALLBACK(apwal_pref_tooltips_toggled), apwal);
  gtk_widget_show(tooltips_chk);
  gtk_tooltips_set_tip(apwal->tips, tooltips_chk, "Activate or not the contextual information boxes like the one you are currently reading", NULL);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tooltips_chk),
                               apwal->apwal_pref->activate_tooltips);
  if (apwal->apwal_pref->activate_tooltips == TRUE)
    gtk_tooltips_enable(apwal->tips);
  else
    gtk_tooltips_disable(apwal->tips);
  
  //iconsel_in_a_separate_window
  iconsel_in_a_sepwin_chk = gtk_check_button_new_with_label(
                                        "Icon Selection in a separate window");
  gtk_box_pack_start(GTK_BOX(editor_vbox), iconsel_in_a_sepwin_chk,
                     FALSE, FALSE, 0);
  // g_connect after iconsel_modal
  gtk_widget_show(iconsel_in_a_sepwin_chk);
  gtk_tooltips_set_tip(apwal->tips, iconsel_in_a_sepwin_chk, "The 'Icon Selection' and the 'Icon Selection Preference' tabs could be either showed in tabs in the main window or in a separate window", NULL);

  // iconsel_modal
  apwal->apwal_pref->iconsel_modal_chk = gtk_check_button_new_with_label(
                                                       "Icon Selection Modal");

  gtk_box_pack_start(GTK_BOX(editor_vbox),
                     apwal->apwal_pref->iconsel_modal_chk, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(apwal->apwal_pref->iconsel_modal_chk), "toggled",
                   G_CALLBACK(apwal_pref_iconsel_modal_toggled), apwal);
  gtk_widget_show(apwal->apwal_pref->iconsel_modal_chk);
  gtk_tooltips_set_tip(apwal->tips, apwal->apwal_pref->iconsel_modal_chk,
                       "If the 'Icon Selection' and the 'Icon Selection Preference' tabs are in a separate window, this separate window could either be modal or not.\nNote: Modal implies that you have to answer and close the Icon Selection Dialog Box in order to access again the main window", NULL);
  
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(
                                         apwal->apwal_pref->iconsel_modal_chk),
                               apwal->apwal_pref->iconsel_modal);
  gtk_widget_set_sensitive(apwal->apwal_pref->iconsel_modal_chk,
                           apwal->apwal_pref->iconsel_in_a_separate_window);
  // iconsel_in_a_separate_window
  // have to do that AFTER the creation of iconsel_modal_chk because
  // apwal_pref_iconsel_in_a_sepwin_toggled motify the enable status of it
  g_signal_connect(G_OBJECT(iconsel_in_a_sepwin_chk), "toggled",
                   G_CALLBACK(apwal_pref_iconsel_in_a_sepwin_toggled), apwal);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(iconsel_in_a_sepwin_chk),
                               apwal->apwal_pref->iconsel_in_a_separate_window);

}
// ----------------------------------------------------------------------------
static void apwal_pref_exit_at_app_launch_toggled(GtkToggleButton *chk,
                                                  apwalapp_t *apwal)
{
  g_assert(chk != NULL && apwal != NULL);
  g_assert(apwal->apwal_pref != NULL);
  TRACE("%s", "");
  apwal->apwal_pref->exit_at_app_launch = gtk_toggle_button_get_active(chk);
}
static void apwal_pref_tooltips_toggled(GtkToggleButton *chk,
                                        apwalapp_t *apwal)
{
  g_assert(chk != NULL && apwal != NULL);
  g_assert(apwal->apwal_pref != NULL);
  TRACE("%s", "");
  apwal->apwal_pref->activate_tooltips = gtk_toggle_button_get_active(chk);
  if (apwal->apwal_pref->activate_tooltips == TRUE)
    gtk_tooltips_enable(apwal->tips);
  else
    gtk_tooltips_disable(apwal->tips);
}
// ----------------------------------------------------------------------------
static void apwal_pref_iconsel_in_a_sepwin_toggled(GtkToggleButton *chk,
                                                   apwalapp_t *apwal)
{
  gboolean last_state;
  g_assert(chk != NULL && apwal != NULL);
  g_assert(apwal->apwal_pref != NULL);
  TRACE("%s", "");
  last_state = apwal->apwal_pref->iconsel_in_a_separate_window;
  apwal->apwal_pref->iconsel_in_a_separate_window =
                                              gtk_toggle_button_get_active(chk);
  gtk_widget_set_sensitive(apwal->apwal_pref->iconsel_modal_chk,
                           apwal->apwal_pref->iconsel_in_a_separate_window);
  // last_state == apwal->apwal_pref->iconsel_in_a_separate_window while
  // building the graphical interface
  if (last_state != apwal->apwal_pref->iconsel_in_a_separate_window)
  {
    // dynamic set
    if (apwal->apwal_pref->iconsel_in_a_separate_window)
      apwal_iconsel_tab2win(apwal);
    else
      apwal_iconsel_win2tab(apwal);
  }
}
// ----------------------------------------------------------------------------
static void apwal_pref_iconsel_modal_toggled(GtkToggleButton *chk,
                                             apwalapp_t *apwal)
{
  g_assert(chk != NULL && apwal != NULL);
  g_assert(apwal->apwal_pref != NULL);
  TRACE("%s", "");
  apwal->apwal_pref->iconsel_modal = gtk_toggle_button_get_active(chk);
  if (apwal->apwal_pref->iconsel_modal == TRUE)
    apwal_iconsel_set_modal(apwal);
  else
    apwal_iconsel_set_not_modal(apwal);
}
// ----------------------------------------------------------------------------
static void apwal_pref_timeout_changed(GtkSpinButton *spin, apwalapp_t *apwal)
{
  g_assert(spin != NULL && apwal != NULL);
  g_assert(apwal->apwal_pref != NULL);
  TRACE("%s", "");
  apwal->apwal_pref->timeout = (int)(gtk_spin_button_get_value(spin) * 1000.00);
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
