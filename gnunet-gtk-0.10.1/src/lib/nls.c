/*
     This file is part of GNUnet.
     (C) 2010, 2011 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/lib/nls.c
 * @brief natural language support
 * @author Christian Grothoff
 */
#include "gnunet_gtk.h"

#if ENABLE_NLS
#include <locale.h>
#endif


/**
 * Initialize GNU gettext for message translation.
 */
void
GNUNET_GTK_setup_nls ()
{
#if ENABLE_NLS
  char *path;

  setlocale (LC_ALL, "");
  GNUNET_asprintf (&path, "%s/%s/locale/", GNUNET_GTK_get_data_dir (),
                   PACKAGE_NAME);
  bindtextdomain ("gnunet-gtk", path);
  textdomain ("gnunet-gtk");
  bind_textdomain_codeset ("GNUnet", "UTF-8");
  bind_textdomain_codeset ("gnunet-gtk", "UTF-8");
  GNUNET_free (path);
#else
  fprintf (stderr,
           "WARNING: gnunet-gtk was compiled without i18n support (did CFLAGS contain -Werror?).\n");
#endif
}


/* This is copied from GLib */
/**
 * Obtain character set used for filenames on this system.
 *
 * @param filename_charset set to the character set used for filenames
 * @return TRUE if the locale is utf-8
 */
static gboolean
get_filename_charset (const gchar ** filename_charset)
{
  const gchar **charsets;
  gboolean is_utf8;

  is_utf8 = g_get_filename_charsets (&charsets);

  if (filename_charset)
    *filename_charset = charsets[0];

  return is_utf8;
}


/**
 * Convert a string from the current locale to UTF-8.
 *
 * @param str_loc string in current locale
 * @return string in UTF-8, NULL if str_loc was NULL
 */
char *
GNUNET_GTK_from_loc_to_utf8 (const char * str_loc)
{
  char *str_utf8;
  const char *loc_charset;
  gboolean is_UTF8;

  if (NULL == str_loc)
    return NULL;

  is_UTF8 = g_get_charset (&loc_charset);
  if (is_UTF8)
    str_utf8 = GNUNET_strdup (str_loc);
  else
    str_utf8 = GNUNET_STRINGS_to_utf8 (str_loc, strlen (str_loc), loc_charset);
  return str_utf8;
}


/**
 * Convert from locale used for filenames to UTF-8.
 *
 * @param filename filename in locale encoding
 * @return filename in utf-8 encoding
 */
static char *
from_filename_to_utf8 (gchar * filename)
{
  char *str_utf8;
  const char *filename_charset;
  gboolean is_UTF8;

  if (NULL == filename)
    return NULL;

  is_UTF8 = get_filename_charset (&filename_charset);
  if (is_UTF8)
    str_utf8 = GNUNET_strdup (filename);
  else
    str_utf8 =
        GNUNET_STRINGS_to_utf8 (filename, strlen (filename), filename_charset);

  return str_utf8;
}


/**
 * Returns filename form filechooser, encoded in UTF-8.
 *
 * @param fc file chooser to inspect
 * @return selected filename as UTF-8, NULL on errors
 */
char *
GNUNET_GTK_filechooser_get_filename_utf8 (GtkFileChooser * fc)
{
  char *filename_utf8;
  gchar *filename = gtk_file_chooser_get_filename (fc);

  if (NULL == filename)
    return NULL;
  filename_utf8 = from_filename_to_utf8 (filename);
  g_free (filename);
  return filename_utf8;
}

/* end of nls.c */
