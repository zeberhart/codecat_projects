cb2bib (1.9.2-1) unstable; urgency=medium

  * New upstream version (port to QT5; uses qmake and not cmake as of this
    version);
  * debian/control: Standards-Version: 3.9.6 (no changes needed). Add
    qt5-default to Build-Depends so that qmake is found.
    
  * debian/control: Change all build depends to new QT5-related package
    names and add support for lzo;

  * debian/rules: call qmake instead of cmake and fix call to rm in rule
    override_dh_clean;

  * debian/patches: add patch to security-harden the binary.

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 17 Aug 2015 10:20:21 +0200

cb2bib (1.4.9-4) unstable; urgency=medium

  * mv upstream upstream/metadata;

  * new debian/patches/fix-keywords-desktop-files.patch to fix missing
    keywords in desktop files.

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 18 Aug 2014 10:47:26 +0200

cb2bib (1.4.9-3) unstable; urgency=medium

  * Bump up debhelper version required for building to >= 9 in Build-Depends;
  
  * Remove dpkg-dev (>= 1.16.1~) from Build-Depends;

  * Got the new recent-debhelper-based rules file from the personal git
    repos of Barak Pearlmutter (<barak@cs.nuim.ie>) and implemented his
    version in this package (Bug#633876) closed with the previous upload.
  
 -- Filippo Rusconi <lopippo@debian.org>  Thu, 14 Aug 2014 15:45:18 +0200

cb2bib (1.4.9-2) unstable; urgency=low

  * Canonicalize the Vcs-Git and Vcs-Browser urls.
    
  * c2bscripts/c2bimport.desktop: fix typo with a patch (thanks Charlie
    Smotherman <cjsmo@cableone.net>) (Closes: #694399).
  
  * Close an old bug requesting for a new version and suggesting a new
    structure for the debian:rules file at a git repos that is no more
    available (Closes: #633876).
  
  * debian/rules: remove the "DM-Upload-Allowed: yes" field.
  
  * debian/control: Update Standards-Version to 3.9.5.

 -- Filippo Rusconi <lopippo@debian.org>  Wed, 13 Aug 2014 14:59:54 +0200

cb2bib (1.4.9-1) unstable; urgency=low

  * New upstream release.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 06 Nov 2012 21:43:57 +0100

cb2bib (1.4.8-2) unstable; urgency=low

  * Add dependency to dpkg-dev.
  
  * Add lintian override to silence the warning about misspelling errors.

 -- Filippo Rusconi <lopippo@debian.org>  Fri, 14 Sep 2012 12:46:22 +0200

cb2bib (1.4.8-1) unstable; urgency=low

  * New upstream release.

 -- Filippo Rusconi <lopippo@debian.org>  Wed, 06 Jun 2012 22:11:25 +0200

cb2bib (1.4.7-2) unstable; urgency=low

  * Add debian/upstream to document a reference to a paper describing this
    software.
  
  * debian/control: set Filippo Rusconi <lopippo@debian.org> as Uploaders.

  * Add fix-ftbfs-with-gcc-4.7 patch.
    Fix FTBFS with gcc 4.7 by fixing missing <unistd.h> includes.
    Thanks to Cyril Brulebois <kibi@debian.org> for the patch. (Closes: #667129)
  * Bump Standards-Version to 3.9.3
  * Both fixes above in git repos thanks to Salvatore Bonaccorso
    <carnil@debian.org>

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 07 May 2012 13:36:06 +0200

cb2bib (1.4.7-1) unstable; urgency=low

  * New upstream release.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 24 Jan 2012 17:26:47 +0100

cb2bib (1.4.6-1) unstable; urgency=low

  * New upstream release.
  
  * Removed debian/patches directory as there are no more patches.

 -- Filippo Rusconi <lopippo@debian.org>  Sun, 28 Aug 2011 19:01:28 +0200

cb2bib (1.4.4-3) unstable; urgency=low

  * Fix bug (Closes: #617750) revealing that qwebview.h is no more shipped
    in libqt4-dev but in libqtwebkit-dev. Thanks to Cyril Brulebois
    <kibi@debian.org> for reporting the FTBFS bug.

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Sat, 12 Mar 2011 14:31:22 +0100

cb2bib (1.4.4-2) unstable; urgency=low

  * Fix (Closes: #554060) the linker problem reported by Peter Fritzsche
    <peter.fritzsche@gmx.de> by adding -lX11 to the linker command line
    (binutils-gold does not complain anymore and link goes through
    fine);
  * 

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Thu, 10 Mar 2011 21:39:52 +0100

cb2bib (1.4.4-1) unstable; urgency=low

  * New upstream release.

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Fri, 28 Jan 2011 08:36:35 +0100

cb2bib (1.4.3-3) unstable; urgency=low

  * Fix 3 bashisms in one upstream code source files, as reported by
    Raphael Geissert <geissert@debian.org> along with a patch from Kai
    Wasserbäch <debian@carbon-project.org> (Closes: #547742). Upstream has
    been informed.
    
  * Set Standards-Version: 3.9.1.

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Wed, 03 Nov 2010 20:38:11 +0100

cb2bib (1.4.3-2) unstable; urgency=low

  * Implemented build system based on use of CMake as per upstream setup.

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Tue, 12 Oct 2010 23:23:51 +0200

cb2bib (1.4.3-1) unstable; urgency=low

  * New upstream release.

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Tue, 12 Oct 2010 20:48:16 +0200

cb2bib (1.4.0-1) unstable; urgency=low

  * New upstream release;

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Sun, 25 Apr 2010 12:02:37 +0200

cb2bib (1.3.6-1) unstable; urgency=low

  * New upstream release;
  
  * debian/rules: change flag for disabling lzo lib usage to reflect an
    upstream change in the configure file.

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Wed, 06 Jan 2010 21:32:10 +0100

cb2bib (1.3.5-1) unstable; urgency=low

  * New upstream release;

  * Switched to the 3.0 (quilt) source format;

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Sat, 28 Nov 2009 12:05:02 +0100

cb2bib (1.3.3-2) unstable; urgency=low

  * Fix the temporary creation directory call (sent fix to Upstream).

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Sat, 19 Sep 2009 09:42:53 +0200

cb2bib (1.3.3-1) unstable; urgency=low

  * New upstream release;
  * debian/rules: add the '--disable_lzo' flag to the configure script call to
    specify no use of the lzo lib;
  * Fix bib2pdf script (sent correction to Upstream).
  * Bunch of fixes (nroff formatting and contents in some places) of man
    pages.

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Fri, 11 Sep 2009 22:39:48 +0200

cb2bib (1.3.2-1) unstable; urgency=low

  * New upstream release;
  * Small packaging modifications to accomodate the small changes in the
    source tree.
  * Fixed some glitches in the nroff formatting of the man page.

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Wed, 09 Sep 2009 23:19:05 +0200

cb2bib (1.3.0-1) unstable; urgency=low

  * Initial release (Closes: #529399);

 -- Filippo Rusconi <rusconi-debian@laposte.net>  Fri, 04 Sep 2009 06:02:19 +0200
