qgo (2.1~git-20160623-1) unstable; urgency=medium

  * New supstream snapshot (Closes: #811786, #827301).
  * Migrate to dbgsym.

 -- Yann Dirson <dirson@debian.org>  Sun, 10 Jul 2016 18:05:30 +0200

qgo (2.1~git-20150530-1) unstable; urgency=medium

  * Snapshot from new upstream tree.
  * Bumped Standards-Version to 3.9.6, no change.
  * Fixed specification of additional CXXFLAGS not to shadow the
    standard flags.
  * Provide a qgo-dbg package.

 -- Yann Dirson <dirson@debian.org>  Tue, 02 Jun 2015 23:52:53 +0200

qgo (2.1~git-20141009-1) unstable; urgency=medium

  * Snapshot from new upstream tree.
    * Stop installing sgf MIME definition (in a wrong place, and useless,
      Closes: #749582)
    * Fixes installation on non-Linux archs.

 -- Yann Dirson <dirson@debian.org>  Tue, 14 Oct 2014 21:14:21 +0200

qgo (2.1~git-20140826-1) unstable; urgency=medium

  * Snapshot from new upstream tree.
    * Fixes "crash after game finished" (Closes: #755142).
  * Updated VCS URLs.
  * Change Homepage field to the github repo, since 2.x upstream does
    not have any control on the sourceforge project yet.
  * Bumped Standards-Version to 3.9.5, no change.

 -- Yann Dirson <dirson@debian.org>  Sat, 13 Sep 2014 21:43:21 +0200

qgo (2.1~git-20140518-1) unstable; urgency=medium

  * Snapshot from new upstream tree.
    * Fixes "segmentation fault on Talk via double click on name" (Closes:
    #734016).
    * Dropping patch 05_lrelease, not necessary any more.

 -- Yann Dirson <dirson@debian.org>  Mon, 19 May 2014 20:52:44 +0200

qgo (2.0~git-20131123-1) unstable; urgency=low

  * Snapshot from new upstream tree.
    * Working interface for managing credentials (Closes: #596439).
    * Avoid looping too much waiting for GTP engine (Closes: #419855).
    * Dropping patches 01_gnugo, 04_desktop, included upstream.

 -- Yann Dirson <dirson@debian.org>  Tue, 03 Dec 2013 21:07:19 +0100

qgo (2.0~git-20130914-1) unstable; urgency=low

  * Snapshot from new upstream tree.
    * Fix snapshot tarball generation to embed a "qgo2" toplevel dir.
    * Builds with qt5, and libpulse instead of libasound2.
    * New patch 05_lrelease to look for lrelease in PATH.
  * Move upstream URL from extended description to Homepage field.
  * Remove description of the game from extended description, point to the
    one in the description of gnugo instead.
  * Remove empty build/ hierarchy on clean.

 -- Yann Dirson <dirson@debian.org>  Sun, 22 Sep 2013 17:35:30 +0200

qgo (2.0~git-20130413-1) unstable; urgency=low

  * Snapshot from new upstream tree (Closes: #717336).
    * Fixes player list sorting (Closes: #458252)
    * Fixes updating files list in "open file" dialog (Closes: #684937).
  * Fixed MimeType in qgo.desktop to be a list (Closes: #629450).
  * Added a README.source explaining how to build upstream tarball.
  * Added VCS fields in control file.
  * Bumped Standards-Version to 3.9.4, no change.
  * Dropped obsolete build-dep on autotools-dev.
  * Added qgo2 authors to copyright file.

 -- Yann Dirson <dirson@debian.org>  Sun, 11 Aug 2013 22:52:24 +0200

qgo (2~svn764-1) unstable; urgency=low

  * The "Raise dead" release (Closes: #673520), new maintainer.
  * New upstream snapshot with Qt4 support (Closes: #604589), adjusted
    build-deps.
  * Switched to source format "3.0 (quilt)", adjusted build-deps.
  * Switched to dh and debhelper compat level 9, adjusted build-deps.
  * Build with -fpermissive.
  * New build-dep libasound2-dev, remove obsolete build-dep on libxinerama-dev.
  * Refreshed patches 01_gnugo and 04_desktop, leaving 20_kfreebsd away
    for this release, and removing the remaining ones, now obsolete.
  * Added patch 02_usrgames for FHS-correct install location.
  * Adjusted icon names in menu file.

 -- Yann Dirson <dirson@debian.org>  Sat, 19 May 2012 19:05:05 +0200

qgo (1.5.4-r3-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix g++-4.5 FTBFS, thanks Andreas Moog for the patch (closes: #565092).

 -- Laurent Fousse <lfousse@debian.org>  Tue, 30 Aug 2011 09:03:34 +0200

qgo (1.5.4-r3-2) unstable; urgency=low

  * fix MimeType in qgo.desktop (closes: #534132 , closes: #535673)
  * update libtool (Closes: #511434)
  * Bumped Standards-Version to 3.8.4. No changes needed.

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Fri,  4 Mar 2010 23:57:52 +0100

qgo (1.5.4-r3-1) unstable; urgency=low

  * New upstream release (polish translation)
  * fix icon in menus (closes: #498938)
  * Bumped Standards-Version to 3.8.0. No changes needed.

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Sat, 15 Nov 2008 11:25:02 +0100

qgo (1.5.4-r2-2) unstable; urgency=low

  * fix several minor issues
  * remove libqt3-compat-headers from control (closes: #464760)
  * Bumped Standards-Version to 3.7.3. No changes needed.
  * adding qgo-16.xpm

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Sun, 16 Mar 2008 23:53:01 +0000

qgo (1.5.4-r2-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix g++-4.3 FTBFS (Closes: 461676).

 -- Pierre Habouzit <madcoder@debian.org>  Sun, 16 Mar 2008 23:09:41 +0000

qgo (1.5.4-r2-1) unstable; urgency=low

  * New minor (-r2) upstream release (turkish translation)
  * fixing debian/watch file for uscan to work (closes: #449858)

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Fri, 09 Nov 2007 15:01:22 +0100

qgo (1.5.4-2) unstable; urgency=low

  * correct unused libdnet found by Steinar H. Gunderson's script
  * remove debian/copyright in rules (automatically installed by dh_installdocs)

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Sun, 22 Jul 2007 22:20:33 +0200

qgo (1.5.4-1) unstable; urgency=low

  * New upstream release
  * fix FTBFS on kFreeBSD, thanks to Cyril Brulebois (closes: #415146)

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Wed, 13 Jun 2007 00:26:02 +0200

qgo (1.5.3-1) unstable; urgency=low

  * New upstream release (closes: #405259)

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Fri,  5 Jan 2007 05:49:36 +0100

qgo (1.5.2-1) unstable; urgency=low

  * New upstream release

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Tue,  3 Oct 2006 23:46:07 +0200

qgo (1.5.1-1) unstable; urgency=low

  * New upstream release (closes #381919)
  * forgot to change standards-version in control
  * patch from upstream CVS to fix segfault (closes #375203)

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Thu, 13 Jul 2006 19:45:05 +0200

qgo (1.5-r1-2) unstable; urgency=low

  * Bumped Standards-Version to 3.7.2. No changes needed.
  * Change patch system to dpatch
  * Recompile with gcc-4.1 and newer libs

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Wed,  8 May 2006 18:46:12 +0200

qgo (1.5-r1-1) unstable; urgency=low

  * New major upstream release (see changelog for details)
  * Adding mime support (closes: #355805) in /usr/lib/mime/packages/qgo

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Wed,  5 Apr 2006 23:01:58 +0200

qgo (1.0.4-r2-1) unstable; urgency=low

  * New upstream release (minor corrections)

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Tue, 17 Jan 2006 18:20:53 +0100

qgo (1.0.4-1) unstable; urgency=low

  * New upstream release (going through old bugs)
  * fixed : now impossible to play on an illegal ko (closes: #342786)
  * fixed in qgo 1.0.3 : suppressed the 'open / new game' menu for observed and
    computer game (closes: #331646)
  * fixed in qgo 0.2.2 : illegal move when playing gnugo was taken as "pass"
    move (closes: #266544)
  * more use of debhelper

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Mon,  9 Jan 2006 22:56:20 +0100

qgo (1.0.3-r2-1) unstable; urgency=low

  * add build dependency on libxinerama-dev (closes: #320260)
  * New upstream subrelease (closes: #340071)
  * version 1.0.3 seems to have corrected some segfault (closes: #310715)

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Tue, 27 Dec 2005 11:47:51 +0100

qgo (1.0.3-1) unstable; urgency=low

  * New upstream version (closes: #331398).
  * Fix qgo.xpm installation
  * reworked debian/rules from dh_make
  * don't build-depends on automake1.6
  * add #DEBHELPER# to post{inst,rm} to make lintian/linda happy
  * new author added to copyright : Marin Ferecatu (from AUTHORS file)

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Mon, 03 Oct 2005 23:10:21 +0200

qgo (1.0.2-5) unstable; urgency=low

  * revert lots of changes which broke compilation (closes: #330607).

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Thu, 29 Sep 2005 00:32:07 +0200

qgo (1.0.2-4) unstable; urgency=low

  * Update debhelper build dependency (closes: #330485).

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Tue, 27 Sep 2005 23:04:31 +0200

qgo (1.0.2-3) unstable; urgency=low

  * build-depends on versionned libqt3-mt-dev (>= 3:3.3.4-3)
  * already the latest version 1.0.2, closes: #288221

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Sun, 25 Sep 2005 14:01:24 +0200

qgo (1.0.2-2) unstable; urgency=low

  * New maintainer, closes: #279553 (RFA), closes: #329283 (ITA)
  * Rebuilt with libqt3-mt, closes: #327096
  * C++ transition: build-depends on libqt3-mt-dev (>= 3.3.4-3)

 -- Cyril Chaboisseau <cyril.chaboisseau@free.fr>  Wed, 21 Sep 2005 10:46:42 +0200

qgo (1.0.2-1) unstable; urgency=low

  * New upstream release,
    closes: #291789 (server connect),
    closes: #266378 (crashes while playing against gnugo),
    closes: #266428 (delay before placing the stone).

 -- Martin A. Godisch <godisch@debian.org>  Thu, 14 Jul 2005 16:10:15 +0200

qgo (1.0.0-r2-2) unstable; urgency=low

  * Fixed MIME type, closes: #316667.
  * Moved menu file to /usr/share/menu/qgo.
  * Updated standards version.

 -- Martin A. Godisch <godisch@debian.org>  Sun, 03 Jul 2005 09:34:30 +0200

qgo (1.0.0-r2-1) unstable; urgency=low

  * New upstream release,
    closes: #270206 (eats up all CPU cycles).

 -- Martin A. Godisch <godisch@debian.org>  Sat, 01 Jan 2005 23:07:10 +0100

qgo (0.2.2-1) unstable; urgency=low

  * New upstream release, closes: #278813,
    closes: #266403 (invalid move is recognized as "pass"),
    closes: #266424 (context-menu of players-list disappears),
    closes: #266399 (filter for players-view not saved),
    closes: #266420 (present popup for playerstats instead of console-output),
    closes: #266426 (chatting in boardview after game has finished should...),
    closes: #266546 ("save" or "save as" should offer meaningful filename).

 -- Martin A. Godisch <godisch@debian.org>  Sat, 30 Oct 2004 18:21:20 +0200

qgo (0.2.1-2) unstable; urgency=low

  * Fixed build issues, closes: #265055.
    Thanks to Andreas Jochens and Adam Lackorzynski.
  * Fixed manual page.
  * Added watch file.

 -- Martin A. Godisch <godisch@debian.org>  Sat, 21 Aug 2004 07:57:06 +0200

qgo (0.2.1-1) unstable; urgency=low

  * Initial release, closes: #259577.

 -- Martin A. Godisch <godisch@debian.org>  Sat, 24 Jul 2004 22:38:31 +0200
