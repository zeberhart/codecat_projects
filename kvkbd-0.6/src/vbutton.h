/*
 * This file is part of the Kvkbd project.
 * Copyright (C) 2007-2008 Todor Gyumyushev <yodor@developer.bg>
 * Copyright (C) 2008 Guillaume Martres <smarter@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef VBUTTON_H
#define VBUTTON_H

#include <QPushButton>

class QString;
class QRect;
class QTimer;

class VButton : public QPushButton
{
    Q_OBJECT
public:
    explicit VButton(QWidget *parent = 0);
    ~VButton();
    void setKeyCode(unsigned int keycode);
    unsigned int getKeyCode();
    void setText(const QString& input, const QString& shiftInput = QString());
    void setShiftText(const QString& key);
    void setKeyCap(bool shiftPressed, bool capsPressed);

    void reposition(int width, int height);
    void res();
    static double pw;
    static double ph;

private:
    QTimer *keyTimer;
    bool rightClicked;

protected:
    unsigned int keycode;
    QString text;
    QString shiftText;
    QRect originalSize;

public slots:
    void sendKey();

protected slots:
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void repeatKey();

signals:
    void keyClick(unsigned int keycode);

};

#endif // VBUTTON_H
