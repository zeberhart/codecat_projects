2008-12-29  Valek Filippov  <frob@df.ru>

	* chunks_parse_cmds.tbl: dual-licensed.
	* Old stuff: removed.

2007-01-07  Valek Filippov  <frob@df.ru>

	* vsd_parse_chunks.c: Fixed bug with chunk parsing introduced with versions 4/5 support addition.

2007-01-03  Valek Filippov  <frob@df.ru>

	* vsd_parse_chunks.c: Added support for versions 4 and 5.

2007-12-31  Valek Filippov  <frob@df.ru>

	* vsd_parse_chunks.c: modified chunks parsing to avoid 4 bytes stream trailer found after SmartTagDef
	in one of the Max's files.

2007-12-17  Valek Filippov  <frob@df.ru>

	* vsd_parse_stream_c.c: modified type detection.

2007-07-28  Valek Filippov  <frob@df.ru>
	* vsd_parse_blocks.c: skip 'bad' blocks.
	* chunk_parse_cmds.tbl: corrections and additions

2007-07-09  Valek Filippov  <frob@df.ru>
	* vsd_parse_chunks.c: added more trailer/separator stuff.
	Copyright was set for all .c and .py files.
	Added statement about license -- GPLv3.
	* COPYING: old version (GPLv2.1) substituted with new one (GPLv3).

2007-06-21  Valek Filippov  <frob@df.ru>
	* pyneechego/chunks.py, vsd_parse_chunks.c: moving Max trailer/separator
	for 0xc9 to the right place, add similar treatment into pygtk version.

2007-06-18  Valek Filippov  <frob@df.ru>
	* COPYING: the license changed to GPLv3.

2007-06-12  Valek Filippov  <frob@df.ru>
	* vsd_parse_chunks.c: Reverted back part of Max trailer/separator 'fixing' patch
	    (support for ver.6 was broken with it).
	* pyneechego/*: Added quick-n-dirty python 'vsd_explorer' with primitive ability to show VSD files.
	
2007-04-09  Max Kosmach <maxkosmach at tcen ru>
	* chunk_parse_cmds.tbl: corrections and additions
	* vsd_parse_chunks.c: 'Units' were added; some tags were changed to comply with VDX;
	    more changes in trailers.
	* vsd_parse_blocks.c: memleak fixed.

2007-04-01  Max Kosmach <maxkosmach at tcen ru>
	* chunk_parse_cmds.tbl: lots of corrections
	* vsd_parse_chunks.c, vsdump.c, vsd_parse_blocks.c: UCS2 to UTF-8 conversion for text strings.

2007-02-07  Valek Filippov <frob@df.ru>
	* chunk_parse_cmds.tbl: corrections to 0x6* chunks.

2007-02-04  Valek Filippov <frob@df.ru>
	* vsd_parse_blocks.c, names.h: The most of _*() ("internal") functions
	  were added.

2007-02-04  Valek Filippov <frob@df.ru>
	* vsd_parse_blocks.c: Fixed allocation/free bug.
	* names.h: UNKNOWN_14 (LOC) and UNKNOWN_112 (_WALKGLUE) were found.

2007-02-01  Valek Filippov <frob@df.ru>
	* vsd_parse_blocks.c: Added '%' function.
  	
2007-01-30  Valek Filippov <frob@df.ru>
	* vsd_parse_blocks.c: Implemented support for all known 0x76 names.
   	Added AlignTop/Bottom/etc names. Added support for GetVal() (0x76 variant function).
   	Improved support for 0x70 names.
	* names.h: Added alignment names (UNKNOWN23 .. 28), fixed OR (UNKNOWN_30), added _MARKER (UNKNOWN_50).
	* chunk_parse_cmds.tbl: Added 0x4b (Guide?), 0x9f (Alignment), 0x82 (unknown), 0x10..0x12 (Data1..3) chunks.
	* vsd_parse_chunk.c: Workaround for 0x1f (version 11) chunks terminator.	

2007-01-30  Valek Filippov <frob@df.ru>
	* vsd_parse_blocks.c: fixed stupid bug with string copying.
	Streamlining the code because missing '&' (0x8) operation was found.
	Minimal support for 0x76 (GetRef) function.

2007-01-29  Valek Filippov <frob@df.ru>
	* vsd_dump_stream_23.c: small fix to correctly save icons on linux-ppc.
	* vsd_parse_chunk.c: Workaround for 0x65, 0x66 and 0x69 chunks terminator.
	* chunk_parse_cmds.tbl: added 0x72 (unknown) and 0xb9 (ConnectionPoints) chunks.
	* names.h: Changed 'Sharpen' to 'TheText' (as in the Ian file), UNKNOWN_216 == SETATREF.
	 
2007-01-28  Valek Filippov <frob@df.ru>
	* vsd_dump_stream_23.c,h: Implemented saving of streams type 0x23 as an .ico files.

2007-01-27  Valek Filippov <frob@df.ru>
	* vsd_parse_blocks.c: Added support for 0x70 names (seen only in Nortel stencil).
	Added support for alternative implementation of NURBS() function (some stencils use it).
	* vsdump.c: added number to 'Dumping stream' output string. It makes searching
	the place of problem much more easier.
	* chunk_parse_cmds.tbl: small fixes.

2007-01-26  Valek Filippov <frob@df.ru>
	* chunk_parse_cmds.tbl: added 0xd and 0x1f chunks.
	* vsd_parse_chunks.c,h, vsd_dump_stream_15.c: added support for OLE objects saving.
	  
2007-01-25  Valek Filippov <frob@df.ru>
	* names.h: changed some 'UNKNOWN_xx' with right names.
	* vsd_parse_blocks.c: assertions and fixups to workaround Nortel stencils problems.
	* vsd_dump_stream_c.c: correctly distinguish between EMFs and WMF-wrapped EMFs. 

2007-01-24  Valek Filippov <frob@df.ru>
	* vsd_parse_chunk.c: Correctly save bmp's of version 6 (v2k and v2k2) files.
	See RevEngeLog for details about images embedded in ver6 files.
	Distinguish between emf's and wmf's. 
	
2007-01-23  Valek Filippov <frob@df.ru>
	* vsd_parse_chunk.c: Added 'emf/wmf' and 'bmp'.
	It works correctly only with 2k3 files at the moment.
	Fixed stupid bug with 0..7 commands.

2007-01-23  Valek Filippov <frob@df.ru>
	* vsd_parse_chunks.c: Added command '26' for 4 bytes values.
	One more dirty tweek for chunk trailer (chunk type 0xc9).
	Added command '28' for image file name extension extraction.
	Implemented correct image file name extension assignment.

2007-01-03  Valek Filippov <frob@df.ru>
	* vsd_parse_chunks.c: Added command '21' to describe start of blocks in version 11,
	because for some blocks it differs from version 6.

2007-01-03  Valek Filippov <frob@df.ru>
	* vsd_parce_blocks.c: Added parsing of AND(), OR() and IF().
	Some memory leaks were fixed.
	All debugging output was switched off.

2007-01-02  Valek Filippov <frob@df.ru>
	* vsd_parse_blocks.c,h: Now blocks are parsed for all except AND(), OR() and IF() functions.
	Most likely vsdump will not segfaults on files with and/or/if but results for any blocks with such funcs will be meaningless.
	There are some unknown names, that can be traced in the future.
	The code is Ugly. At the moment parsed block is printed and free'ed from parse_chunks. I'm going to change it.

2007-01-02  Valek Filippov <frob@df.ru>
	* vsd_parse_chunks.c,h: the vsd_parse_chunks was moved here. A small part of it was segregated
	to a vsd_extract_chunk_hdr to shrink a size of the 1st function.
	* vsd_parse_blocks.c,h: Added.

2006-12-29  Valek Filippov <frob@df.ru>
	Start to implement block/slice parsing.
	Change chunk parsing to split printing out of parsing.

2006-12-27  Valek Filippov <frob@df.ru>
	Kick out 'decompressed length' from strm_ptr struct. Never used it anyway. 
	* vsd_pointers.c,h: changed to use GArray of strm_ptr instead of GByteArray.
	* vsdump.c: changed to conform with vsd_pointers.c,h changes.
	* vsd_pointers5.c,h: obsoleted. Improved version was implemented in vsd_pointers.c
	* vsd_extract_chunks5.c,h: obsoleted.
	* vsd_parse_cmds.c: add 'hdr_len' (of chunk) instead of 19 to support version 5.
		
2006-12-26  Valek Filippov <frob@df.ru>
	* vsd_parse_cmds.c: fixed and tested to work on linux-ppc.
	Changed way hash is formed, possible memory leak fixed.
	Thanks SVU.

2006-12-26  Valek Filippov <frob@df.ru>
	* vsd_pointers.c: not quite elegant but now vsdump should be able to work on linux-ppc.
	Thanks SVU.
	* vsdump.c, vsd_parse_cmds.c: more assertions for cmds tbl.

2006-12-23  Valek Filippov <frob@df.ru>
	* vsdump.c: added parsing of Type 0x33 and 0xA streams.

2006-12-22  Valek Filippov <frob@df.ru>
	* chunks_parse_cmds.tbl: Added dumping of some chunks.
	* vsd_parse_cmds.c: Fixed some alignment mistakes.
	  Added parsing of V2k FontFaces. Fixed || instead of && in 2 places.

2006-12-21  Valek Filippov <frob@df.ru>
	* vsd_parse_cmds.c,h; chunks_parse_cmds.tbl: Added file with some 'commands' to parse to decode chunks.
	Added functions to parse this command file to hash of {chunk type; pointer to cmds array}.
	Parsing of chunks was changed accordingly. At the moment name of cmds file hardcoded, so it must be
	in the same directory as a vsdump program.
	Lots of changes to adopt changes above.

2006-12-16  Valek Filippov <frob@df.ru>
	* vsd_parse15.c,h: renamed to vsd_dump_stream_15.c,h.
	Added 2 workarounds for trailer/separator issues.
	* vsdump.c: Added printing of file size, clarified structure.
	* vsd_pointers.c: Modified to use strm_ptr structure.
	'vsd_pointers_extract' removed, instead of it make a copy GsfInput to GByteArray
	and use 'vsd_pointers_search'.
	'vsd_pointers_print' calculates and prints count of bytes pointed by pointers (+ file header).
	A mismatch with file length printed by vsdump.c can means that some info was lost.
	* vsd_get_input.c,h: renamed to vsd_utils.c,h. Added function to open suggested file in the dir
	(returns FILE*), not really need but clarifies code.
	* vsd_dump_stream_c.c,h: attempt of dirty dumping of Type 0xC streams
	 (mostly EMF/WMF from 'commercial' VSS files).
	* vsd_extract_chunks5.c,h: my minimal support of Version 5 moved to its own file.
	 I believe it doesn't make the right things at the moment.

2006-12-13  Valek Filippov <frob@df.ru>
	* vsdump.c, vsd_parse15.c: Modified to minimal support of Version 5.
	* vsd_pointers5.c, vsd_pointers5.h: added files for pointers extraction from Version5 files.
	(unoptimal method, but will not change before parsing rewrite). 

2006-12-12  Valek Filippov <frob@df.ru>
	* vsdump.c: added dumping of EMF from 0xc streams to separated files.
	Splitted 0x52 streams dumps out to 'pointers' file.
	* vsd_pointers.c: added search for pointers in 0x53 streams (found a lot in VSS files).

2006-12-11  Valek Filippov <frob@df.ru>
	* vsd_parse15.c: Added a lot of chunk names decoding.
	* utils/morten.c, utils/remorten.c: console utils to hex2float/float2hex conversion.

2006-12-08  Valek Filippov <frob@df.ru>
	* vsd_parse15.c: Added 'Control' (0xAA) chunk name decoding.

2006-12-07  Valek Filippov <frob@df.ru>
	* vsd_parse15.c: Decoding of chunk 0xc0 fields implemented.
	* vsd_parse15.c, vsdump.c: change output to grep-friendly variant.
		Now most of chunks are dumped fully for research.

2006-12-06  Valek Filippov <frob@df.ru>
	* vsd_parse15.c: Decoding of names for about 12 chunks implemented.

2006-12-05  Valek Filippov <frob@df.ru>
	* vsd_parse15.c: Improved dumping of images, now uses counter to store more than one image correctly,
		because IX seems to be "1" always. Added partial parsing of chunk type 0x98 ('Foreign').
		Added recognishion of 'Shape ID Type="Foreign"' chunk (Type 0x4e).
		Temporary added 'debug' file for storing a stream with hardcoded p_subtype to it.
		Will change it with command option later (see TODO).
		Changed separator add behavior to exclude (unkn2 == 3 and unkn3 == 0x50) cases. This case found in Ian file.

2006-12-04  Valek Filippov <frob@df.ru>

	* vsd_pointers.c: 
	    Modified search of pointers. Printing of pointers detached.
	    Now prints 2 additional fields: from which stream extracted and length of
	    decompressed stream.
	* vsdump.c, vsd_pointers.c, vsd_parse15.c, vsd_fonts_colors_parse.c:
	    Changed method of dumping: 3rd args is the name of directory to store dumps
	    (if ommited, vsdump creates 'vsdmp' dir). All pointers are dumped to 'pointer' file,
	    all streams -- to streams. All chunks 0x0c will be saved in the 'file<IX>.img' files.

2006-12-02  Valek Filippov <frob@df.ru>

	* vsd_parse15.c: improved trailer/separator detection for Visio2k3 chunks.
	    Added 'End of chunk' notification in case we printf just unparsed part of chunk.
	    Otherwise (no notification) we printf all chunk for debugging purposes.
	    Cut 'TextBlock' Default TabStop parsing -- was wrong, need more info.
	    Added check that suggested length of chunk not more than length of stream
	    (still possible to have 'out of range' by trailer/separator).
	    Added parsing of chunk 0x19 (FontFace for Visio2k).
	* vsdump.c: added chunk parsing for 0x1a and 0x31 streams (0xd2 or 0xd6), also temporary dump content
	    of full stream for debugging.

2006-12-01  Valek Filippov <frob@df.ru>

	* vsd_parse15.c: changed to parse both Visio2k and Visio2k3, simplified 'start of chunks'.
	There is some difficulties (?) with separator placment. In test file there is no separator
	after 'Text' chunk. Suspect ('unkn3'&4 != 1).
	* vsdump.c: changed to parse chunks in streams Subtype 26 and Subtype 49 (both have Type d6) for Visio2k.

2006-11-30  Valek Filippov <frob@df.ru>

	* vsd_pointers.c, vsdump.c: Patch from gmorten:
	fixed more missed includes, initialise input_pointers,
	check it before freeing.
	* vsd_parse15.c: more parsing for 'Char', 'Line' and 'Para'.
	Droped putchars for everything but 'Text' and 'Image'.

2006-11-28  Valek Filippov <frob@df.ru>
	
	* Lots of restrictive CFLAGS added by gmorten recomendation.
	* vsd_pointers.c: Fixed missed include. Thanks gmorten.

2006-11-28  Valek Filippov <frob@df.ru>
	
	* vsd_parse15.c: Fixed some bytes counting mistakes.
	Added printf in addition to putchar to unrecognized part of chunk.
	Added types 0x48 (Shape) and 0x68 (Geom).

2006-11-27  Valek Filippov <frob@df.ru>

	* "Start making this into a real package."
	(This statement was stolen from Jody too =)
