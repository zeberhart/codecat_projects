/*
     This file is part of GNUnet
     (C) 2005, 2006, 2010 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/statistics/gnunet-statistics-gtk-about.c
 * @author Christian Grothoff
 * @author Igor Wronsky
 *
 * This file contains the about dialog.
 */
#include "gnunet_gtk.h"


/**
 * This displays an about window
 */
void
GNUNET_STATISTICS_GTK_main_menu_help_about_activate_cb (GtkWidget * dummy,
                                                        gpointer data)
{
  GNUNET_GTK_display_about ("gnunet_statistics_gtk_about_window.glade");
}


/* end of gnunet-peerinfo-gtk-about.c */
