/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#ifndef C2BCORECITER_H
#define C2BCORECITER_H

#include <QObject>
#include <QWidget>


class settings;


class c2bCoreCiter : public QObject
{

    Q_OBJECT

public:
    explicit c2bCoreCiter(QWidget* parentw = 0);
    ~c2bCoreCiter();

    void cite(const QStringList& keys) const;
    void citeToClipboard(const QStringList& keys) const;
    void citeToLyXPipe(const QStringList& keys) const;


public slots:
    void setLyXPipe();


private:
    QString _lyxpipe;
    QWidget* _parentWP;
    settings* _settingsP;

};

#endif
