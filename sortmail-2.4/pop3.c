#ifndef lint
static const char rcsid[] =
	"$Id: pop3.c,v 1.6 2003/08/21 15:59:26 efalk Exp $" ;
#endif

/*	POP3.C	-- manage POP3 protocol
 *
 *
 * Pop3Folder *
 * Pop3Open(char *hostname, char *user, char *pw, int timeout, int noApop)
 *	Open a network connection.  Return NULL on failure.
 *	Timeout is the timeout value in seconds for each stage of the
 *	connection and authentication.
 *
 *
 * int
 * Pop3ReadMessage( Pop3Folder *p3, FILE *, int msgno, int timeout)
 *	Read the pop3 message indicated by msgno and write it to the
 *	specified file.  Returns 0 on success, error code on failure.
 *	After a failure, the connection is closed and p3->fd is set to -1.
 *	Caller should now free the Pop3Folder structure.
 *
 *
 * int
 * Pop3ReadHeader( Pop3Folder *p3, FILE *, int msgno, int timeout)
 *	Read the message header only.
 *
 *
 * int
 * Pop3DeleteMessage(Pop3Folder *, int msgno, int timeout)
 *	Delete a message.
 *
 * void
 * Pop3Close(Pop3Folder *)
 *	Close connection if still open.  Free Pop3Folder structure.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <netinet/in.h>

	/* Note: this code is dependent on sortmail because of calls
	 * to logFile()
	 */

#include "sortmail.h"
#include "utils.h"

#include "netutils.h"
#include "pop3.h"
#include "md5global.h"
#include "md5.h"


#define	MAX_POP_CMD	512

typedef	enum {
	  APOP_OK,	/* apop authentication successful */
	  APOP_FAIL,	/* apop authentication failed, try USER */
	  APOP_FATAL	/* apop authentication failed, abort */
	} ApopStatus ;

static	ApopStatus	ApopAuth(Pop3Folder *, char *user, char *pw,
				char buf[MAX_POP_CMD+1], int to) ;

static	void	Pop3Abort(Pop3Folder *p3 ) ;
static	int	pop3ReadData( Pop3Folder *, FILE *, int msgno, int to) ;
static	void	chopEol( register char *ptr );
static	void	chopCR(char *buffer);



/*---------------------------------------------------------------------------
 * Initiate connection to host.  On success, connection
 * is open and this function returns non-null.  On failure,
 * there is no connection and this function returns null.
 * This function reports all errors to logfile as appropriate.
 */

Pop3Folder *
Pop3Open( char *hostname, char *user, char *pw, int to, int noApop)
{
	int	fd ;
	char	buffer[MAX_POP_CMD+1] ;
	char	*ptr ;
	Pop3Folder *rval ;
	ApopStatus status ;

	if( strlen(user) > MAX_POP_CMD-10  ||  strlen(pw) > MAX_POP_CMD-10 ) {
	  logFile("username and/or password too long\n") ;
	  logFile("  username = %s\n", user) ;
	  return NULL ;
	}

	rval = malloc(sizeof(*rval));
	if( rval == NULL ) {
	  logFile("out of memory in Pop3Open\n") ;
	  return NULL ;
	}
	rval->timeout = to;

	signal(SIGPIPE, SIG_IGN) ;

	if( (fd = openNet(hostname, "pop-3", 110)) < 0 ) {
	  free(rval) ;
	  return NULL ;
	}
	rval->fd = fd ;

	if( netReadln(fd, buffer, sizeof(buffer), to) <= 0 ) {
	  logFile("unable to connect to pop3 server %s, %s\n",
	  	hostname, strerror(errno)) ;
	  Pop3Abort(rval) ;
	  free(rval);
	  return NULL;
	}

	if( buffer[0] != '+' ) {	/* connect failed */
	  chopEol(buffer) ;
	  logFile("unable to connect to pop3 server %s: %s\n",
	  	hostname, buffer) ;
	  Pop3Abort(rval) ;
	  free(rval);
	  return NULL;
	}

	/* Try APOP first */

	if( noApop )
	  status = APOP_FAIL ;
	else if( (status = ApopAuth(rval, user, pw, buffer, to)) == APOP_FATAL )
	  return NULL ;

	if( status == APOP_FAIL )
	{
	  if( netWritef(fd, "USER %s\r\n", user) < 0  ||
	      netReadln(fd, buffer, sizeof(buffer), to) <= 0 )
	  {
	    logFile("APOP authentication to server %s has failed\n", hostname) ;
	    logFile("server has severed connection.  trying again.\n") ;
	    return Pop3Open(hostname, user, pw, to, 1) ;
	  }

	  if( buffer[0] != '+' ) {	/* login failed */
	    chopEol(buffer) ;
	    logFile("pop3 open for user %s failed: %s\n", user, buffer) ;
	    Pop3Abort(rval) ;
	    free(rval);
	    return NULL;
	  }

	  if( netWritef(fd, "PASS %s\r\n", pw) < 0  ||
	      netReadln(fd, buffer, sizeof(buffer), to) <= 0 )
	  {
	    logFile("unable to connect to pop3 server %s: %s\n",
	    	hostname, strerror(errno)) ;
	    Pop3Abort(rval) ;
	    free(rval);
	    return NULL;
	  }

	  if( buffer[0] != '+' ) {	/* connect failed */
	    chopEol(buffer) ;
	    logFile("pop3 open for user %s failed: %s\n", user, buffer) ;
	    Pop3Abort(rval) ;
	    free(rval);
	    return NULL;
	  }
	}

	/* OK, we're connected */

	if( netWritef(fd, "STAT\r\n") < 0  ||
	    netReadln(fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("unable to connect to pop3 server %s: %s\n",
	      hostname, strerror(errno)) ;
	  Pop3Abort(rval) ;
	  free(rval);
	  return NULL;
	}

	if( buffer[0] != '+' ) {	/* connect failed */
	  chopEol(buffer) ;
	  logFile("pop3 open for user %s failed: %s\n", user, buffer) ;
	  Pop3Abort(rval) ;
	  free(rval);
	  return NULL;
	}

	for(ptr=buffer; *ptr != '\0' && !isdigit(*ptr); ++ptr) ;
	rval->nmsg = atoi(ptr) ;

	logFilev(1, "%d messages waiting\n", rval->nmsg) ;

	return rval ;
}


static	ApopStatus
ApopAuth(
	Pop3Folder	*p3,
	char		*user,
	char		*pw,
	char		buffer[MAX_POP_CMD+1],
	int		to)
{
	char	*p1 ;
	char	*p2 ;
	MD5_CTX	ctx ;
	unsigned char digest[16] ;

	if( (p1 = strchr(buffer,'<'))  && (p2 = strchr(buffer,'>')) )
	{
	  MD5Init(&ctx) ;
	  MD5Update(&ctx, p1, p2-p1+1) ;
	  MD5Update(&ctx, pw, strlen(pw)) ;
	  MD5Final(digest, &ctx) ;

	  if( netWritef(p3->fd, "APOP %s %2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x\r\n",
		user, digest[0], digest[1], digest[2], digest[3],
		digest[4], digest[5], digest[6], digest[7], digest[8],
		digest[9], digest[10], digest[11], digest[12], digest[13],
		digest[14], digest[15] ) < 0  ||
	      netReadln(p3->fd, buffer, MAX_POP_CMD, to) <= 0 )
	  {
	    logFilev(2, "pop3 open for user %s failed: %s", user, strerror(errno)) ;
	    Pop3Abort(p3) ;
	    free(p3);
	    return APOP_FATAL ;
	  }

	  if( buffer[0] != '+' )	/* connect failed */
	    return APOP_FAIL ;

	  return APOP_OK ;
	}
	return APOP_FAIL ;
}



/*---------------------------------------------------------------------------
 * Read the header and body of a message, write to specified file.
 */

int
Pop3ReadMessage( Pop3Folder *p3, FILE *ofile, int msgno, int to)
{
	char	buffer[1024] ;

	if( netWritef(p3->fd, "RETR %d\r\n", msgno) < 0  ||
	    netReadln(p3->fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("pop3 fatal error reading message %d: %s\n",
	    	msgno, strerror(errno)) ;
	  Pop3Abort(p3) ;
	  return errno == ETIMEDOUT ? POP3_TIMEOUT : POP3_ERR ;
	}

	if( buffer[0] != '+' ) {
	  chopEol(buffer) ;
	  logFile("pop3 fatal error reading message %d: %s\n",
	  	msgno, buffer) ;
	  Pop3Abort(p3) ;
	  return POP3_ERR ;
	}

	return pop3ReadData(p3, ofile, msgno, to) ;
}


/*---------------------------------------------------------------------------
 * Read the header of one message, write to specified file.
 */

int
Pop3ReadHeader( Pop3Folder *p3, FILE *ofile, int msgno, int to )
{ 
	char	buffer[1024] ;

	if( netWritef(p3->fd, "TOP %d 0\r\n", msgno) < 0  ||
	    netReadln(p3->fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("pop3 fatal error reading header %d: %s\n",
		msgno, strerror(errno)) ;
	  Pop3Abort(p3) ;
	  return errno == ETIMEDOUT ? POP3_TIMEOUT : POP3_ERR ;
	}

	if( buffer[0] != '+' ) {
	  chopEol(buffer) ;
	  logFile("pop3 fatal error reading message %d: %s\n",
	  	msgno, buffer) ;
	  Pop3Abort(p3) ;
	  return POP3_ERR ;
	}

	return pop3ReadData(p3, ofile, msgno, to) ;
}




/*---------------------------------------------------------------------------
 * Read data from server, copy to tempfile.  Return error code on
 * failure, 0 on success.
 */

static	int
pop3ReadData( Pop3Folder *p3, FILE *ofile, int msgno, int to)
{
	char	*ptr ;
	char	buffer[1024] ;
	int	i ;


	/* Copy input to tmp file.  */

	p3->lines = 0;
	p3->size = 0;

	for(;;)
	{
	  i = netReadln(p3->fd, buffer, sizeof(buffer), to) ;
	  if( i <= 0 ) {
	    logFile("pop3 fatal error reading message %d: %s\n",
	  	msgno, strerror(errno));
	    Pop3Abort(p3) ;
	    return errno == ETIMEDOUT ? POP3_TIMEOUT : POP3_ERR ;
	  }
	  ++p3->lines ;
	  chopCR(buffer) ;

	  if( buffer[0] == '.'  &&  buffer[1] == '\n' )
	    break ;

	  if( *(ptr = buffer) == '.' ) ++ptr ;
	  if( fputs(buffer, ofile) == EOF ) {
	    logFile("pop3 fatal error reading message %d: %s\n",
	  	msgno, strerror(errno));
	    Pop3Abort(p3) ;
	    return POP3_TMPFILE ;
	  }
	  p3->size += strlen(ptr) ;
	}

	return POP3_OK ;
}


int
Pop3DeleteMessage( Pop3Folder *p3, int msgno, int to)
{
	char	buffer[1024] ;

	if( netWritef(p3->fd, "DELE %d\r\n", msgno) < 0  ||
	    netReadln(p3->fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("pop3 fatal error deleting message %d: %s\n",
	      msgno, strerror(errno));
	  Pop3Abort(p3) ;
	  return errno == ETIMEDOUT ? POP3_TIMEOUT : POP3_ERR ;
	}

	return POP3_OK ;
}


void
Pop3Close(Pop3Folder *p3)
{
	if( p3->fd >= 0 ) {
	  netWritef(p3->fd, "QUIT\r\n") ;
	  close(p3->fd) ;
	}
	free(p3) ;
}



static	void
Pop3Abort( Pop3Folder *p3 )
{
	close(p3->fd) ;
	p3->fd = -1;		/* Flag as closed */
}




static	void
chopEol( register char *ptr )
{
	register char *p2 ;

	if( (p2 = strchr(ptr,'\n')) != NULL ) *p2 = '\0' ;
	if( (p2 = strchr(ptr,'\r')) != NULL ) *p2 = '\0' ;
}



static	void
chopCR(char *buffer)
{
	char	*ptr ;

	if( (ptr = strchr(buffer,'\r')) != NULL ) {
	  *ptr = '\n' ;
	  *++ptr = '\0' ;
	}
}
