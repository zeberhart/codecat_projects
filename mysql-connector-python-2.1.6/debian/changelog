mysql-connector-python (2.1.6-1) unstable; urgency=medium

  * New upstream release
    - fixes CVE-2017-3590; Closes: #861511
  * debian/copyright
    - extend upstream copyright years
    - update packaging copyright years

 -- Sandro Tosi <morph@debian.org>  Sat, 27 May 2017 19:25:45 -0400

mysql-connector-python (2.1.5-1) unstable; urgency=medium

  * New upstream release
    - fixes CVE-2016-5598; Closes: #841677
  * debian/copyright
    - update upstream copyright years
  * debian/control
    - adjust b-d to "default-mysql-server | virtual-mysql-server", for new
      default mysql server provider; Closes: #848291
  * debian/patches/support_alternative_mysqld_implementation.patch
    - support MariaDB versioning schema and search binaries only in directories
      with 'bin' in their name
  * compat level 10

 -- Sandro Tosi <morph@debian.org>  Sat, 17 Dec 2016 19:53:41 -0500

mysql-connector-python (2.1.3-1) unstable; urgency=medium

  [ Sandro Tosi ]
  * New upstream release
  * debian/patches/
    - remove, merged upstream
  * debian/copyright
    - extend packaging copyright years
  * debian/control
    - bump Standards-Version to 3.9.8 (no changes needed)
    - add pylint to b-d, used by tests

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

 -- Sandro Tosi <morph@debian.org>  Fri, 17 Jun 2016 21:08:45 +0100

mysql-connector-python (2.0.4-1) unstable; urgency=medium

  * New upstream release

 -- Sandro Tosi <morph@debian.org>  Sun, 02 Aug 2015 17:32:23 +0100

mysql-connector-python (2.0.3-1) experimental; urgency=medium

  * New upstream release
  * debian/copyright
    - extend packaging copyright years
    - extend upstream copyright years
  * debian/{docs, rules}
    - update doc location
  * debian/rules
    - update examples location
  * debian/control
    - bump Standards-Version to 3.9.6 (no changes needed)
  * debian/{compat, control}
    - bump dh compat level to 9

 -- Sandro Tosi <morph@debian.org>  Mon, 02 Mar 2015 23:25:31 +0000

mysql-connector-python (1.2.3-2) unstable; urgency=low

  [ Brian May ]
  * debian/patches/fix_last_executed_query.patch
    - fix last_executed_query to return unicode. Closes: #761136

  [ Sandro Tosi ]
  * debian/rules
    - re-enable unittests at build-time (failing, so not blocking build if
      failing); Closes: #739189

 -- Sandro Tosi <morph@debian.org>  Wed, 17 Sep 2014 22:18:14 +0100

mysql-connector-python (1.2.3-1) unstable; urgency=low

  * New upstream version.
  * Compatible with Django 1.7. Closes: #758844.

 -- Brian May <bam@debian.org>  Thu, 04 Sep 2014 13:59:17 +1000

mysql-connector-python (1.2.2-1) unstable; urgency=low

  * New upstream release.
  * Django warnings become errors. This has been fixed.
    http://bugs.mysql.com/bug.php?id=71806.

 -- Brian May <bam@debian.org>  Wed, 09 Jul 2014 09:00:14 +1000

mysql-connector-python (1.1.6-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - add proper Replaces/Breaks on mysql-utilities, now they share the same
      namespace with dh_python2; thanks to Laurent Bigonville for the report and
      to Dmitry Smirnov for the work on mysql-utilities; Closes: #739488

 -- Sandro Tosi <morph@debian.org>  Sun, 09 Mar 2014 18:15:36 +0100

mysql-connector-python (1.1.5-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Sandro Tosi ]
  * New upstream release
  * debian/watch
    - updated to reflect upstream website changes
  * debian/copyright
    - fix license, it's only GPLv2
    - update upstream copyright years
    - extend packaging copyright years
    - add reference to local GPL-2 file for packaging copyright notice
  * debian/control
    - switch me to Maintainer (team to Uploaders)
    - bump Standards-Version to 3.9.5 (no changes needed)
  * debian/rules
    - Disable test execution, currently broken upstream - see 739189
  * Switch to dh_python2

 -- Sandro Tosi <morph@debian.org>  Tue, 18 Feb 2014 16:26:46 +0100

mysql-connector-python (1.0.9-1) experimental; urgency=low

  * New upstream release
  * debian/copyright
    - updated upstream copyright years

 -- Sandro Tosi <morph@debian.org>  Mon, 15 Apr 2013 00:05:40 +0200

mysql-connector-python (1.0.8-1) experimental; urgency=low

  * New upstream release (GA)
  * debian/watch
    - finally forged a watch file that can scan upstream webpage and is able to
      download updated tarballs
  * debian/rules
    - run unittests for IPv6 too
  * debian/copyright
    - use the correct format for machine-parsable copyright file
    - extended packaging copyright years

 -- Sandro Tosi <morph@debian.org>  Sat, 05 Jan 2013 22:31:26 +0100

mysql-connector-python (1.0.7-2) experimental; urgency=low

  * debian/{control, rules}
    - run tests at build-time (requires a MySQL server)
  * debian/rules
    - install *.py only as examples

 -- Sandro Tosi <morph@debian.org>  Mon, 29 Oct 2012 00:17:45 +0100

mysql-connector-python (1.0.7-1) experimental; urgency=low

  * New upstream release (GA)
  * debian/copyright
    - extend both upstream and packaging copyright years
    - update FLOSS exception comment
  * debian/{control, copyright}
    - update Homepage and Source fields to point to Oracle pages
  * debian/rules
    - install examples for both py2 and py3 packages
    - install documentation in all binary packages

 -- Sandro Tosi <morph@debian.org>  Sun, 28 Oct 2012 16:40:02 +0100

mysql-connector-python (0.3.2-2) experimental; urgency=low

  * debian/{control, rules}
    - provide Python 3 package
  * debian/control
    - bump Standards-Version to 3.9.3 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Mon, 27 Aug 2012 23:00:41 +0200

mysql-connector-python (0.3.2-1) unstable; urgency=low

  * Initial release (Closes: #650809)

 -- Sandro Tosi <morph@debian.org>  Sat, 03 Dec 2011 15:08:34 +0100
