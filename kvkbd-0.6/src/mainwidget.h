/*
 * This file is part of the Kvkbd project.
 * Copyright (C) 2007-2008 Todor Gyumyushev <yodor@developer.bg>
 * Copyright (C) 2008 Guillaume Martres <smarter@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef KVKBDMAINWIDGET_H
#define KVKBDMAINWIDGET_H

#include <QList>

#include <KSystemTrayIcon>

#include "resizabledragwidget.h"

#include <X11/Xlib.h>

class KAction;
class KAboutData;

class KbdDock;
class KbdTray;
class VButton;
class NumpadVButton;

class MainWidget : public ResizableDragWidget
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.kde.kvkbd.Kvkbd")
    Q_PROPERTY(bool alone READ isAlone)
    Q_PROPERTY(bool autoResize READ autoResized WRITE setAutoResized)

public:
    explicit MainWidget(const KAboutData *about, bool loginhelper, QWidget *parent = 0, Qt::WindowFlags f = Qt::X11BypassWindowManagerHint);
    void mappingNotify(XMappingEvent *e);
    const KAboutData *aboutData;
    void saveState();
    bool isAlone() const;
    bool autoResized() const;
    void setAutoResized(bool);
    KbdDock *dock;

public slots:
    void autoResizeFont(bool);
    void chooseFont();
    void lockWidget(bool);
    void showDock(bool);

signals:
    void autoResizeToggled(bool);

private slots:
    void keyPress(unsigned int keycode);

    void toggleCaps();
    void toggleNumlock();

    void resetKeysCap();

    void queryModState();

    void toggleNumericPad();
    void restorePosition();

protected:
    void resizeEvent(QResizeEvent *e);
    void closeEvent(QCloseEvent *e);


private:
    void calculateSize();

    void finishInit();
    void setupKeyboard();
    void restoreSettings();

    void updateFont(QFont &font);

    bool resizable;

    bool keyState(int iKey);
    void keyPress(int keycode);
    void setupText(VButton &v);

    QTimer *modTimer;


    VButton *caps;
    VButton *leftShift;
    VButton *rightShift;

    VButton *leftCtrl;
    VButton *rightCtrl;

    VButton *leftAlt;
    VButton *rightAlt;

    VButton *winButton;
    VButton *menuButton;

    VButton *numLock;
    VButton *div;
    VButton *mul;
    VButton *min;
    VButton *plu;
    VButton *ent;

    NumpadVButton *ins;
    NumpadVButton *del;

    QList<VButton *> buttonsList;
    QList<VButton *> modKeys;
    QList<VButton *> otherKeys;
    QList<NumpadVButton *> numLockKeys;

    void sendKey(unsigned int keycode);

    Display *display;
    KbdTray *tray;

    bool isExtentVisible;
    VButton *quit;
    VButton *buttonMenu;
    VButton *extent;
    double sdxs;
    double sdxb;

    bool autoResize;
    bool alone;

};

class KbdTray : public KSystemTrayIcon
{
    Q_OBJECT

public:
    KbdTray(MainWidget* parent = 0);
    ~KbdTray();

};

#endif
