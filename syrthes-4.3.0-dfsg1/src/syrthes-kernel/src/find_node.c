/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "syr_tree.h"
# include "syr_bd.h"
# include "syr_proto.h"


/* # include "mpi.h" */



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | find_node_3d                                                         |
  |         dans quel noeud se trouve le point x,y,z                     |
  |======================================================================| */
void find_node_3d (struct node **noeud, double x, double y, double z)
{
  struct child *p1;
  int i,nfils ;
  
  if ((**noeud).lfils != NULL)
    {
      nfils = quel_fils_3d((**noeud).xc,(**noeud).yc,(**noeud).zc,(**noeud).lfils,x,y,z);
      p1=(**noeud).lfils;
      for (i=0;i<nfils-1;i++) p1=p1->suivant;
      *noeud = p1->fils;
      find_node_3d(noeud, x, y,z); 
      }
}
	    
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | quel_fils_3d                                                         |
  |         dans quel fils faut-il aller ensuite                         |
  |======================================================================| */
int quel_fils_3d (double xcc,double ycc,double zcc,struct child *pfils, 
		  double x, double y, double z)
{
  if (x>xcc)
    if (y>ycc)
      if(z>zcc)
	return(6);
      else
	return(7);
    else
      if(z>zcc)
	return(2);
      else
	return(3);
  else
    if (y>ycc)
      if(z>zcc)
	return(5);
      else
	return(8);
    else
      if(z>zcc)
	return(1);
      else
	return(4);

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | find_node_2d                                                         |
  |         dans quel noeud se trouve le point x,y,z                     |
  |======================================================================| */
void find_node_2d (struct node **noeud, double x, double y)

{
  struct child *p1;
  int i,nfils ;
  
  if ((**noeud).lfils != NULL)
    {
      nfils = quel_fils_2d((**noeud).xc,(**noeud).yc,(**noeud).lfils,x,y);
      p1=(**noeud).lfils;
      for (i=0;i<nfils-1;i++) p1=p1->suivant;
      *noeud = p1->fils;
      find_node_2d(noeud, x, y); 
    }
}
	    
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | quel_fils_2d                                                         |
  |         dans quel fils faut-il aller ensuite                         |
  |======================================================================| */
int quel_fils_2d (double xcc,double ycc,struct child *pfils, 
		  double x, double y)
{
  if (x>xcc)
    if (y>ycc)
      return(2);
    else
      return(3);
  else
    if (y>ycc)
      return(1);
    else
      return(4);
}
