/* apwalpref.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#ifndef APWALPREF__H
#define APWALPREF__H

#include "common.h"

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
typedef struct apwal_pref_t
{
  gint        timeout;
  gboolean    exit_at_app_launch;
  gboolean    activate_tooltips;
  gboolean    iconsel_in_a_separate_window;
  gboolean    iconsel_modal;
  GtkWidget  *iconsel_modal_chk;
} apwal_pref_t;
apwal_pref_t * apwal_pref_new(gint timeout, gboolean exit_at_app_launch,
                              gboolean activate_tooltips,
                              gboolean iconsel_in_a_separate_window,
                              gboolean iconsel_modal);
apwal_pref_t * apwal_pref_new_default(void);
// ----------------------------------------------------------------------------

void apwal_pref_build_interface(struct apwalapp_t *apwal);

#endif /*APWALPREF__H*/
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
