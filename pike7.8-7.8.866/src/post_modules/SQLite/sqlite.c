/* Generated from "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod" by precompile.pike
 *
 * Do NOT edit this file.
 */

#undef PRECOMPILE_API_VERSION
#define PRECOMPILE_API_VERSION 5



#undef cmod___CMOD__
#define cmod___CMOD__ 1
#line 1 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
#line 1 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
#line 1 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
#line 2816 "/usr/local/pike/8.0.1/lib/modules/Tools.pmod/Standalone.pmod/precompile.pike"







#line 2823 "/usr/local/pike/8.0.1/lib/modules/Tools.pmod/Standalone.pmod/precompile.pike"
#line 1 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
#line 1 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
/* -*- c -*-
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: a959907cd3e75cc7e93cebb14e2eeda698efa4a8 $
*/

#include "global.h"
#include "interpret.h"
#include "backend.h"
#include "module_support.h"
#include "config.h"
#include "object.h"
#include "builtin_functions.h"
#include "mapping.h"
#include "threads.h"
#include "bignum.h"

#if defined(HAVE_SQLITE3_H) && defined(HAVE_LIBSQLITE3)

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif

#ifdef HAVE_WINDOWS_H
#include <windows.h>
#endif

#include <sqlite3.h>
#include <time.h>

#define SLEEP() sysleep(0.0001)



#ifndef TYPEOF
/* Compat with older Pikes. */
#define TYPEOF(SVAL)	((SVAL).type)
#define SUBTYPEOF(SVAL)	((SVAL).subtype)
#define SET_SVAL_TYPE(SVAL, TYPE)	(TYPEOF(SVAL) = TYPE)
#define SET_SVAL_SUBTYPE(SVAL, TYPE)	(SUBTYPEOF(SVAL) = TYPE)
#define SET_SVAL(SVAL, TYPE, SUBTYPE, FIELD, EXPR) do {	\
    /* Set the type afterwards to avoid a clobbered	\
     * svalue in case EXPR throws. */			\
    (SVAL).u.FIELD = (EXPR);				\
    SET_SVAL_TYPE((SVAL), (TYPE));			\
    SET_SVAL_SUBTYPE((SVAL), (SUBTYPE));		\
  } while(0)
#endif /* !TYPEOF */


#ifndef DEFAULT_CMOD_STORAGE
#define DEFAULT_CMOD_STORAGE
#endif
#line 40 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
#define ERR(X, db)				\
if((X)!=SQLITE_OK) {				\
  SQLite_handle_error((db));			\
}

static void SQLite_handle_error(sqlite3 *db)
{
if (db) {
push_text(sqlite3_errmsg(db));
f_utf8_to_string(1);
Pike_error("Sql.SQLite: %S\n", Pike_sp[-1].u.string);
} else {
Pike_error("Sql.SQLite: Internal module error\n");
}
}

static int step(sqlite3_stmt *stmt) {
int ret;
/* FIXME: This is not always a good way to handle SQLITE_BUSY:
   *
   *   SQLITE_BUSY means that the database engine was unable to
   *   acquire the database locks it needs to do its job. If the
   *   statement is a COMMIT or occurs outside of an explicit
   *   transaction, then you can retry the statement. If the statement
   *   is not a COMMIT and occurs within a explicit transaction then
   *   you should rollback the transaction before continuing.
   */
while( (ret=sqlite3_step(stmt))==SQLITE_BUSY ) {
THREADS_ALLOW();
SLEEP();
THREADS_DISALLOW();
}
return ret;
}

static void bind_arguments(sqlite3 *db,
sqlite3_stmt *stmt,
struct mapping *bindings) {
struct mapping_data *md = bindings->data;
INT32 e;
struct keypair *k;
NEW_MAPPING_LOOP(md) {
int idx;
switch(k->ind.type) {
case T_INT:
idx = k->ind.u.integer;
break;
case T_STRING:
ref_push_string(k->ind.u.string);
f_string_to_utf8(1);
idx = sqlite3_bind_parameter_index(stmt, Pike_sp[-1].u.string->str);
pop_stack();
if(!idx)
Pike_error("Unknown bind index \"%S\".\n", k->ind.u.string);
break;
default:
Pike_error("Bind index is not int|string.\n");
}
switch(k->val.type) {
case T_INT:
ERR( sqlite3_bind_int64(stmt, idx, k->val.u.integer),
db );
break;
case T_STRING:
{
struct pike_string *s = k->val.u.string;
switch(s->size_shift) {
case 0:
ERR( sqlite3_bind_blob(stmt, idx, s->str, s->len,
SQLITE_STATIC),
db);
break;
case 1:
case 2:
ref_push_string(s);
f_string_to_utf8(1);
s = Pike_sp[-1].u.string;
ERR( sqlite3_bind_text(stmt, idx, s->str, s->len,
SQLITE_TRANSIENT),
db);
pop_stack();
break;
#ifdef PIKE_DEBUG
 default:
Pike_error("Unknown size_shift.\n");
#endif
 }
}
break;
case T_FLOAT:
ERR( sqlite3_bind_double(stmt, idx, (double)k->val.u.float_number),
db);
break;
default:
Pike_error("Can only bind string|int|float.\n");
}
}
}

/*! @class SQLite
 *! @appears predef::Sql.sqlite
 *!
 *! Low-level interface to SQLite3 databases.
 *!
 *! This class should typically not be accessed directly, but instead
 *! via @[Sql.Sql()] with the scheme @expr{"sqlite://"@}.
 */


#undef class_SQLite_defined
#define class_SQLite_defined
DEFAULT_CMOD_STORAGE struct program *SQLite_program=NULL;
static int SQLite_program_fun_num=-1;

#undef var_db_SQLite_defined
#define var_db_SQLite_defined

#undef THIS
#define THIS ((struct SQLite_struct *)(Pike_interpreter.frame_pointer->current_storage))

#undef THIS_SQLITE
#define THIS_SQLITE ((struct SQLite_struct *)(Pike_interpreter.frame_pointer->current_storage))

#undef OBJ2_SQLITE
#define OBJ2_SQLITE(o) ((struct SQLite_struct *)(o->storage+SQLite_storage_offset))

#undef GET_SQLITE_STORAGE
#define GET_SQLITE_STORAGE(o) ((struct SQLite_struct *)(o->storage+SQLite_storage_offset)
static ptrdiff_t SQLite_storage_offset;
struct SQLite_struct {

#ifdef var_db_SQLite_defined
#line 150 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
sqlite3 *db;
#endif /* var_db_SQLite_defined */
};
#ifdef PIKE_DEBUG
/* Ensure the struct is used in a variable declaration, or else gdb might not see it. */
static struct SQLite_struct *SQLite_gdb_dummy_ptr;
#endif
/*! @class ResObj
 *!
 *! Result object from @[big_query()].
 */


#undef class_SQLite_ResObj_defined
#define class_SQLite_ResObj_defined
DEFAULT_CMOD_STORAGE struct program *SQLite_ResObj_program=NULL;
static int SQLite_ResObj_program_fun_num=-1;

#undef var_dbobj_SQLite_ResObj_defined
#define var_dbobj_SQLite_ResObj_defined

#undef var_bindings_SQLite_ResObj_defined
#define var_bindings_SQLite_ResObj_defined

#undef var_stmt_SQLite_ResObj_defined
#define var_stmt_SQLite_ResObj_defined

#undef var_eof_SQLite_ResObj_defined
#define var_eof_SQLite_ResObj_defined

#undef var_columns_SQLite_ResObj_defined
#define var_columns_SQLite_ResObj_defined

#undef THIS
#define THIS ((struct SQLite_ResObj_struct *)(Pike_interpreter.frame_pointer->current_storage))

#undef THIS_SQLITE_RESOBJ
#define THIS_SQLITE_RESOBJ ((struct SQLite_ResObj_struct *)(Pike_interpreter.frame_pointer->current_storage))

#undef OBJ2_SQLITE_RESOBJ
#define OBJ2_SQLITE_RESOBJ(o) ((struct SQLite_ResObj_struct *)(o->storage+SQLite_ResObj_storage_offset))

#undef GET_SQLITE_RESOBJ_STORAGE
#define GET_SQLITE_RESOBJ_STORAGE(o) ((struct SQLite_ResObj_struct *)(o->storage+SQLite_ResObj_storage_offset)
static ptrdiff_t SQLite_ResObj_storage_offset;
struct SQLite_ResObj_struct {

#ifdef var_dbobj_SQLite_ResObj_defined
#line 160 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
struct object *dbobj;
#endif /* var_dbobj_SQLite_ResObj_defined */

#ifdef var_bindings_SQLite_ResObj_defined
#line 161 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
struct mapping *bindings;
#endif /* var_bindings_SQLite_ResObj_defined */

#ifdef var_stmt_SQLite_ResObj_defined
#line 162 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
sqlite3_stmt *stmt;
#endif /* var_stmt_SQLite_ResObj_defined */

#ifdef var_eof_SQLite_ResObj_defined
#line 163 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
int eof;
#endif /* var_eof_SQLite_ResObj_defined */

#ifdef var_columns_SQLite_ResObj_defined
#line 164 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
int columns;
#endif /* var_columns_SQLite_ResObj_defined */
};
#ifdef PIKE_DEBUG
/* Ensure the struct is used in a variable declaration, or else gdb might not see it. */
static struct SQLite_ResObj_struct *SQLite_ResObj_gdb_dummy_ptr;
#endif
#line 166 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
static void ResObj_handle_error(void) {
Pike_error("Sql.SQLite: %s\n",
sqlite3_errmsg(OBJ2_SQLITE(THIS->dbobj)->db));
}

#define f_SQLite_ResObj_create_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_ResObj_create_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_ResObj_create(INT32 args) {
#line 171 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("create",args,0);
#line 173 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
THIS->columns = sqlite3_column_count(THIS->stmt);
}

}
#define f_SQLite_ResObj_num_rows_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_ResObj_num_rows_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_ResObj_num_rows(INT32 args) {
#line 177 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("num_rows",args,0);
#line 177 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
Pike_error("Sql.SQLite: Number of rows not known in advance.\n");
}

}
#define f_SQLite_ResObj_num_fields_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_ResObj_num_fields_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_ResObj_num_fields(INT32 args) {
#line 181 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("num_fields",args,0);
#line 181 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
do { INT_TYPE ret_=(THIS->columns);  push_int(ret_); return; }while(0);
#line 183 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
}

}
#define f_SQLite_ResObj_eof_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_ResObj_eof_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_ResObj_eof(INT32 args) {
#line 185 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("eof",args,0);
#line 185 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
do { INT_TYPE ret_=(THIS->eof);  push_int(ret_); return; }while(0);
#line 187 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
}

}
#define f_SQLite_ResObj_fetch_fields_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_ResObj_fetch_fields_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_ResObj_fetch_fields(INT32 args) {
#line 189 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("fetch_fields",args,0);
#line 189 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
int i,t;
for(i=0; i<THIS->columns; i++) {
push_constant_text("name");
push_text(sqlite3_column_name(THIS->stmt, i));
f_utf8_to_string(1);
push_constant_text("type");
t = sqlite3_column_type(THIS->stmt, i);
switch(t)
{
case SQLITE_INTEGER:
push_constant_text("integer");
break;
case SQLITE_FLOAT:
push_constant_text("float");
break;
case SQLITE_BLOB:
push_constant_text("blob");
break;
case SQLITE_NULL:
push_constant_text("null");
break;
case SQLITE_TEXT:
push_constant_text("text");
break;
default:
push_constant_text("unknown");
break;
}
f_aggregate_mapping(4);
}
f_aggregate(THIS->columns);
}

}
#define f_SQLite_ResObj_seek_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_ResObj_seek_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_ResObj_seek(INT32 args) {
#line 223 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
INT_TYPE skip;
#line 223 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 1) wrong_number_of_args_error("seek",args,1);
#line 223 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(TYPEOF(Pike_sp[0-1]) != PIKE_T_INT) SIMPLE_ARG_TYPE_ERROR("seek",1,"int");
#line 223 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
skip=Pike_sp[0-1].u.integer;
#line 223 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
int i;
for(i=0; i<skip; i++)
if( step(THIS->stmt)==SQLITE_DONE ) {
THIS->eof = 1;
return;
}
}

}
#define f_SQLite_ResObj_fetch_row_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_ResObj_fetch_row_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_ResObj_fetch_row(INT32 args) {
#line 232 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("fetch_row",args,0);
#line 232 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
int i;
sqlite3_stmt *stmt = THIS->stmt;

if(THIS->eof) {
push_int(0);
return;
}

switch( step(stmt) ) {
case SQLITE_DONE:
THIS->eof = 1;
sqlite3_finalize(stmt);
THIS->stmt = 0;
push_int(0);
return;
case SQLITE_ROW:
break;
default:
ResObj_handle_error();
}

for(i=0; i<THIS->columns; i++) {
push_string( make_shared_binary_string
( sqlite3_column_blob(stmt, i),
sqlite3_column_bytes(stmt, i) ) );
if( sqlite3_column_type(stmt, i)==SQLITE_TEXT )
f_utf8_to_string(1);
}
f_aggregate(THIS->columns);
}

}

#undef internal_init_SQLite_ResObj_defined
#define internal_init_SQLite_ResObj_defined

#undef SQLite_ResObj_event_handler_defined
#define SQLite_ResObj_event_handler_defined
static void init_SQLite_ResObj_struct(void)
#line 264 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
THIS->eof = 0;
THIS->columns = -1;
THIS->dbobj = NULL;
THIS->stmt = NULL;
THIS->bindings = NULL;
}


#undef internal_exit_SQLite_ResObj_defined
#define internal_exit_SQLite_ResObj_defined

#undef SQLite_ResObj_event_handler_defined
#define SQLite_ResObj_event_handler_defined
static void exit_SQLite_ResObj_struct(void)
#line 274 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
if(THIS->stmt)
{
sqlite3_finalize(THIS->stmt);
THIS->stmt = NULL;
}
if(THIS->dbobj)
{
free_object(THIS->dbobj);
THIS->dbobj = NULL;
}
if(THIS->bindings)
{
free_mapping(THIS->bindings);
THIS->bindings = NULL;
}
}

#ifdef SQLite_ResObj_event_handler_defined
static void SQLite_ResObj_event_handler(int ev) {
  switch(ev) {

#ifdef internal_init_SQLite_ResObj_defined
  case PROG_EVENT_INIT: init_SQLite_ResObj_struct(); break;

#endif /* internal_init_SQLite_ResObj_defined */

#ifdef internal_exit_SQLite_ResObj_defined
  case PROG_EVENT_EXIT: exit_SQLite_ResObj_struct(); break;

#endif /* internal_exit_SQLite_ResObj_defined */
  default: break; 
  }
}

#endif /* SQLite_ResObj_event_handler_defined */
/*! @endclass
 */

#line 296 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
#undef THIS
#define THIS THIS_SQLITE

/* @decl void create(string path)
   */
#define f_SQLite_create_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_create_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_create(INT32 args) {
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
struct pike_string * path;
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
struct svalue * a;
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
struct svalue * b;
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
struct svalue * c;
struct mapping * options;
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args < 1) wrong_number_of_args_error("create",args,1);
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args > 5) wrong_number_of_args_error("create",args,5);
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(TYPEOF(Pike_sp[0-args]) != PIKE_T_STRING) SIMPLE_ARG_TYPE_ERROR("create",1,"string");
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
debug_malloc_pass(path=Pike_sp[0-args].u.string);
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if (args > 1) {
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
a=Pike_sp+1-args; dmalloc_touch_svalue(Pike_sp+1-args);
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
} else a = NULL;
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if (args > 2) {
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
b=Pike_sp+2-args; dmalloc_touch_svalue(Pike_sp+2-args);
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
} else b = NULL;
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if (args > 3) {
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
c=Pike_sp+3-args; dmalloc_touch_svalue(Pike_sp+3-args);
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
} else c = NULL;
if (args > 4 &&    (TYPEOF(Pike_sp[4-args]) != PIKE_T_INT ||     Pike_sp[4-args].u.integer)) {
#line 302 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(TYPEOF(Pike_sp[4-args]) != PIKE_T_MAPPING) SIMPLE_ARG_TYPE_ERROR("create",5,"mapping(mixed:mixed)|void");
#line 302 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
debug_malloc_pass(options=Pike_sp[4-args].u.mapping);
#line 302 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
} else options = NULL;
#line 304 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
pop_n_elems(args-1);
f_string_to_utf8(1);
ERR( sqlite3_open(path->str, &THIS->db), THIS->db );
pop_stack();
}

}
#define f_SQLite_query_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_query_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_query(INT32 args) {
#line 311 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
struct pike_string * query;
struct mapping * bindings;
#line 311 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args < 1) wrong_number_of_args_error("query",args,1);
#line 311 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args > 2) wrong_number_of_args_error("query",args,2);
#line 311 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(TYPEOF(Pike_sp[0-args]) != PIKE_T_STRING) SIMPLE_ARG_TYPE_ERROR("query",1,"string");
#line 311 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
debug_malloc_pass(query=Pike_sp[0-args].u.string);
if (args > 1 &&    (TYPEOF(Pike_sp[1-args]) != PIKE_T_INT ||     Pike_sp[1-args].u.integer)) {
#line 312 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(TYPEOF(Pike_sp[1-args]) != PIKE_T_MAPPING) SIMPLE_ARG_TYPE_ERROR("query",2,"mapping(string|int:mixed)|void");
#line 312 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
debug_malloc_pass(bindings=Pike_sp[1-args].u.mapping);
#line 312 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
} else bindings = NULL;
#line 312 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{

sqlite3_stmt *stmt;
const char *tail;
struct pike_string *q;
INT32 res_count = 0;
INT32 columns;
INT32 i;

if(args==2) stack_swap();
f_string_to_utf8(1);
q = Pike_sp[-1].u.string;

ERR( sqlite3_prepare(THIS->db, q->str, q->len, &stmt, &tail),
THIS->db);
if( tail[0] )
Pike_error("Sql.SQLite->big_query: Trailing query data (\"%s\")\n",
tail);
pop_stack();


/* Add a reference to the database to prevent it from being
       destroyed before the query object. */

if(bindings) {
bind_arguments(THIS->db, stmt, bindings);
}

columns = sqlite3_column_count(stmt);

check_stack(128);

BEGIN_AGGREGATE_ARRAY(100) {
while(stmt) {

int sr=step(stmt);

switch(sr) {
case SQLITE_OK: /* Fallthrough */
case SQLITE_DONE:
sqlite3_finalize(stmt);
stmt = 0;
break;

case SQLITE_ROW:
for(i=0; i<columns; i++) {
push_text(sqlite3_column_name(stmt, i));
f_utf8_to_string(1);
push_string( make_shared_binary_string
( sqlite3_column_blob(stmt, i),
sqlite3_column_bytes(stmt, i) ) );
if( sqlite3_column_type(stmt, i)==SQLITE_TEXT )
f_utf8_to_string(1);
}
f_aggregate_mapping(columns*2);
DO_AGGREGATE_ARRAY(100);
break;

case SQLITE_MISUSE:
Pike_error("Sql.SQLite: Library misuse.");

default:
Pike_error("Sql.SQLite: (%d) %s\n", sr, sqlite3_errmsg(THIS->db));
}
}
} END_AGGREGATE_ARRAY;

if (!Pike_sp[-1].u.array->size && !columns) {
/* No rows and no columns. */
pop_stack();
push_int(0);
}
}

}
#define f_SQLite_big_query_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_big_query_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_big_query(INT32 args) {
#line 386 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
struct pike_string * query;
struct mapping * bindings;
#line 386 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args < 1) wrong_number_of_args_error("big_query",args,1);
#line 386 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args > 2) wrong_number_of_args_error("big_query",args,2);
#line 386 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(TYPEOF(Pike_sp[0-args]) != PIKE_T_STRING) SIMPLE_ARG_TYPE_ERROR("big_query",1,"string");
#line 386 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
debug_malloc_pass(query=Pike_sp[0-args].u.string);
if (args > 1 &&    (TYPEOF(Pike_sp[1-args]) != PIKE_T_INT ||     Pike_sp[1-args].u.integer)) {
#line 387 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(TYPEOF(Pike_sp[1-args]) != PIKE_T_MAPPING) SIMPLE_ARG_TYPE_ERROR("big_query",2,"mapping(string|int:mixed)|void");
#line 387 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
debug_malloc_pass(bindings=Pike_sp[1-args].u.mapping);
#line 387 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
} else bindings = NULL;
#line 387 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{

struct object *res;
sqlite3_stmt *stmt;
const char *tail;
struct SQLite_ResObj_struct *store;
struct pike_string *q;

if(args==2) stack_swap();
f_string_to_utf8(1);
q = Pike_sp[-1].u.string;

ERR( sqlite3_prepare(THIS->db, q->str, q->len, &stmt, &tail),
THIS->db);
if( tail[0] )
Pike_error("Sql.SQLite->big_query: Trailing query data (\"%s\")\n",
tail);
pop_stack();

res=fast_clone_object(SQLite_ResObj_program);
store = OBJ2_SQLITE_RESOBJ(res);
store->stmt = stmt;

/* Add a reference to the database to prevent it from being
       destroyed before the query object. */
store->dbobj = this_object();

if(bindings) {
bind_arguments(THIS->db, stmt, bindings);

/* Add a reference so that the bound strings are kept, which in
	 turn allows us to use SQLITE_STATIC. */
add_ref(bindings);
store->bindings = bindings;
}

apply_low(res, f_SQLite_ResObj_create_fun_num, 0);
push_object(res);
}

}
#define f_SQLite_changes_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_changes_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_changes(INT32 args) {
#line 427 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("changes",args,0);
#line 429 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
do { INT_TYPE ret_=(sqlite3_changes(THIS->db));  push_int(ret_); return; }while(0);
#line 431 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
}

}
#define f_SQLite_total_changes_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_total_changes_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_total_changes(INT32 args) {
#line 433 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("total_changes",args,0);
#line 435 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
do { INT_TYPE ret_=(sqlite3_total_changes(THIS->db));  push_int(ret_); return; }while(0);
#line 437 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
}

}
#define f_SQLite_interrupt_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_interrupt_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_interrupt(INT32 args) {
#line 439 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("interrupt",args,0);
#line 441 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
sqlite3_interrupt(THIS->db);
}

}
#define f_SQLite_server_info_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_server_info_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_server_info(INT32 args) {
#line 445 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("server_info",args,0);
#line 447 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
push_constant_text(sqlite3_libversion());
}

}
/*! @decl int insert_id()
   *!
   *! Returns the value of the @tt{ROWID@} (aka @tt{OID@}, aka @tt{_ROWID_@},
   *! or declared @tt{INTEGER PRIMARY KEY@}) column for the most recent
   *! successful @tt{INSERT@} operation, or @expr{0@} (zero) if no @tt{INSERT@}
   *! operations have been performed on the connection yet.
   */
#define f_SQLite_insert_id_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_insert_id_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_insert_id(INT32 args) {
#line 458 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("insert_id",args,0);
#line 460 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
push_longest (sqlite3_last_insert_rowid(THIS->db));
}

}
#define f_SQLite_error_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_error_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_error(INT32 args) {
#line 464 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("error",args,0);
#line 466 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
push_text(sqlite3_errmsg(THIS->db));
f_utf8_to_string(1);
}

}
#define f_SQLite_select_db_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_select_db_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_select_db(INT32 args) {
#line 471 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
struct pike_string * db;
#line 471 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 1) wrong_number_of_args_error("select_db",args,1);
#line 471 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(TYPEOF(Pike_sp[0-1]) != PIKE_T_STRING) SIMPLE_ARG_TYPE_ERROR("select_db",1,"string");
#line 471 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
debug_malloc_pass(db=Pike_sp[0-1].u.string);
#line 471 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
Pike_error("This operation is not possible with SQLite.\n");
}

}
#define f_SQLite_create_db_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_create_db_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_create_db(INT32 args) {
#line 475 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
struct pike_string * db;
#line 475 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 1) wrong_number_of_args_error("create_db",args,1);
#line 475 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(TYPEOF(Pike_sp[0-1]) != PIKE_T_STRING) SIMPLE_ARG_TYPE_ERROR("create_db",1,"string");
#line 475 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
debug_malloc_pass(db=Pike_sp[0-1].u.string);
#line 475 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
Pike_error("This operation is not possible with SQLite.\n");
}

}
#define f_SQLite_drop_db_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_drop_db_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_drop_db(INT32 args) {
#line 479 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
struct pike_string * db;
#line 479 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 1) wrong_number_of_args_error("drop_db",args,1);
#line 479 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(TYPEOF(Pike_sp[0-1]) != PIKE_T_STRING) SIMPLE_ARG_TYPE_ERROR("drop_db",1,"string");
#line 479 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
debug_malloc_pass(db=Pike_sp[0-1].u.string);
#line 479 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
Pike_error("This operation is not possible with SQLite.\n");
}

}
#define f_SQLite_list_dbs_defined
DEFAULT_CMOD_STORAGE ptrdiff_t f_SQLite_list_dbs_fun_num = 0;
DEFAULT_CMOD_STORAGE void f_SQLite_list_dbs(INT32 args) {
#line 483 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
if(args != 0) wrong_number_of_args_error("list_dbs",args,0);
#line 483 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
Pike_error("This operation is not possible with SQLite.\n");
}

}

#undef internal_init_SQLite_defined
#define internal_init_SQLite_defined

#undef SQLite_event_handler_defined
#define SQLite_event_handler_defined
static void init_SQLite_struct(void)
#line 487 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
THIS->db = NULL;
}


#undef internal_exit_SQLite_defined
#define internal_exit_SQLite_defined

#undef SQLite_event_handler_defined
#define SQLite_event_handler_defined
static void exit_SQLite_struct(void)
#line 493 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
{
if(THIS->db) {
int i;
/* FIXME: sqlite3_close can fail. What do we do then? */
for(i=0; i<5; i++) {
if( sqlite3_close(THIS->db)!=SQLITE_OK ) {
THREADS_ALLOW();
SLEEP();
THREADS_DISALLOW();
} else break;
}
}
}


#ifdef SQLite_event_handler_defined
static void SQLite_event_handler(int ev) {
  switch(ev) {

#ifdef internal_init_SQLite_defined
  case PROG_EVENT_INIT: init_SQLite_struct(); break;

#endif /* internal_init_SQLite_defined */

#ifdef internal_exit_SQLite_defined
  case PROG_EVENT_EXIT: exit_SQLite_struct(); break;

#endif /* internal_exit_SQLite_defined */
  default: break; 
  }
}

#endif /* SQLite_event_handler_defined */
/*! @endclass
 */

#line 512 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
#endif /* HAVE_SQLITE3_H && HAVE_LIBSQLITE3 */

PIKE_MODULE_INIT {

#ifdef class_SQLite_defined

#ifdef PROG_SQLITE_ID
#line 148 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
  START_NEW_PROGRAM_ID(SQLITE);
#else
#line 148 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
  start_new_program();

#endif /* PROG_SQLITE_ID */

#ifndef tObjImpl_SQLITE

#undef tObjImpl_SQLITE
#define tObjImpl_SQLITE tObj

#endif /* tObjImpl_SQLITE */

#ifdef THIS_SQLITE
  SQLite_storage_offset = ADD_STORAGE(struct SQLite_struct);
#endif /* THIS_SQLITE */

#ifdef class_SQLite_ResObj_defined

#ifdef PROG_SQLITE_RESOBJ_ID
#line 157 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
  START_NEW_PROGRAM_ID(SQLITE_RESOBJ);
#else
#line 157 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
  start_new_program();

#endif /* PROG_SQLITE_RESOBJ_ID */

#ifndef tObjImpl_SQLITE_RESOBJ

#undef tObjImpl_SQLITE_RESOBJ
#define tObjImpl_SQLITE_RESOBJ tObj

#endif /* tObjImpl_SQLITE_RESOBJ */

#ifdef THIS_SQLITE_RESOBJ
  SQLite_ResObj_storage_offset = ADD_STORAGE(struct SQLite_ResObj_struct);
#endif /* THIS_SQLITE_RESOBJ */

#ifdef SQLite_ResObj_event_handler_defined
  pike_set_prog_event_callback(SQLite_ResObj_event_handler);

#ifndef SQLite_ResObj_gc_live_obj
  Pike_compiler->new_program->flags &= ~PROGRAM_LIVE_OBJ;

#endif /* SQLite_ResObj_gc_live_obj */

#endif /* SQLite_ResObj_event_handler_defined */

#ifdef f_SQLite_ResObj_create_defined
  f_SQLite_ResObj_create_fun_num =
#line 171 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("create", f_SQLite_ResObj_create, tFunc(tNone,tVoid), ID_PROTECTED, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_ResObj_create_defined */

#ifdef f_SQLite_ResObj_num_rows_defined
  f_SQLite_ResObj_num_rows_fun_num =
    ADD_FUNCTION2("num_rows", f_SQLite_ResObj_num_rows, tFunc(tNone,"\10\200\0\0\0\177\377\377\377"), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_ResObj_num_rows_defined */

#ifdef f_SQLite_ResObj_num_fields_defined
  f_SQLite_ResObj_num_fields_fun_num =
#line 181 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("num_fields", f_SQLite_ResObj_num_fields, tFunc(tNone,"\10\200\0\0\0\177\377\377\377"), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_ResObj_num_fields_defined */

#ifdef f_SQLite_ResObj_eof_defined
  f_SQLite_ResObj_eof_fun_num =
#line 185 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("eof", f_SQLite_ResObj_eof, tFunc(tNone,"\10\200\0\0\0\177\377\377\377"), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_ResObj_eof_defined */

#ifdef f_SQLite_ResObj_fetch_fields_defined
  f_SQLite_ResObj_fetch_fields_fun_num =
#line 189 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("fetch_fields", f_SQLite_ResObj_fetch_fields, tFunc(tNone,tArr(tMap(tStr,tMix))), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_ResObj_fetch_fields_defined */

#ifdef f_SQLite_ResObj_seek_defined
  f_SQLite_ResObj_seek_fun_num =
#line 223 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("seek", f_SQLite_ResObj_seek, tFunc("\10\200\0\0\0\177\377\377\377",tVoid), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_ResObj_seek_defined */

#ifdef f_SQLite_ResObj_fetch_row_defined
  f_SQLite_ResObj_fetch_row_fun_num =
#line 232 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("fetch_row", f_SQLite_ResObj_fetch_row, tFunc(tNone,tArray), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_ResObj_fetch_row_defined */
#line 157 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
  SQLite_ResObj_program=end_program();
#line 157 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
  SQLite_ResObj_program_fun_num=add_program_constant("ResObj",SQLite_ResObj_program,ID_PRIVATE | ID_PROTECTED | ID_HIDDEN);

#endif /* class_SQLite_ResObj_defined */

#ifdef SQLite_event_handler_defined
  pike_set_prog_event_callback(SQLite_event_handler);

#ifndef SQLite_gc_live_obj
  Pike_compiler->new_program->flags &= ~PROGRAM_LIVE_OBJ;

#endif /* SQLite_gc_live_obj */

#endif /* SQLite_event_handler_defined */

#ifdef f_SQLite_create_defined
  f_SQLite_create_fun_num =
#line 301 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("create", f_SQLite_create, tFunc(tStr tOr(tMix,tVoid) tOr(tMix,tVoid) tOr(tMix,tVoid) tOr(tMapping,tVoid),tVoid), ID_PROTECTED, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_create_defined */

#ifdef f_SQLite_query_defined
  f_SQLite_query_fun_num =
#line 311 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("query", f_SQLite_query, tFunc(tStr tOr(tMap(tOr(tStr,"\10\200\0\0\0\177\377\377\377"),tMix),tVoid),tOr(tArray,"\10\200\0\0\0\177\377\377\377")), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_query_defined */

#ifdef f_SQLite_big_query_defined
  f_SQLite_big_query_fun_num =
#line 386 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("big_query", f_SQLite_big_query, tFunc(tStr tOr(tMap(tOr(tStr,"\10\200\0\0\0\177\377\377\377"),tMix),tVoid),tObj), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_big_query_defined */

#ifdef f_SQLite_changes_defined
  f_SQLite_changes_fun_num =
#line 427 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("changes", f_SQLite_changes, tFunc(tNone,"\10\200\0\0\0\177\377\377\377"), 0, OPT_EXTERNAL_DEPEND);

#endif /* f_SQLite_changes_defined */

#ifdef f_SQLite_total_changes_defined
  f_SQLite_total_changes_fun_num =
    ADD_FUNCTION2("total_changes", f_SQLite_total_changes, tFunc(tNone,"\10\200\0\0\0\177\377\377\377"), 0, OPT_EXTERNAL_DEPEND);

#endif /* f_SQLite_total_changes_defined */

#ifdef f_SQLite_interrupt_defined
  f_SQLite_interrupt_fun_num =
    ADD_FUNCTION2("interrupt", f_SQLite_interrupt, tFunc(tNone,tVoid), 0, OPT_SIDE_EFFECT);

#endif /* f_SQLite_interrupt_defined */

#ifdef f_SQLite_server_info_defined
  f_SQLite_server_info_fun_num =
    ADD_FUNCTION2("server_info", f_SQLite_server_info, tFunc(tNone,tStr), 0, OPT_TRY_OPTIMIZE);

#endif /* f_SQLite_server_info_defined */

#ifdef f_SQLite_insert_id_defined
  f_SQLite_insert_id_fun_num =
#line 458 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("insert_id", f_SQLite_insert_id, tFunc(tNone,"\10\200\0\0\0\177\377\377\377"), 0, OPT_EXTERNAL_DEPEND);

#endif /* f_SQLite_insert_id_defined */

#ifdef f_SQLite_error_defined
  f_SQLite_error_fun_num =
    ADD_FUNCTION2("error", f_SQLite_error, tFunc(tNone,tStr), 0, OPT_EXTERNAL_DEPEND);

#endif /* f_SQLite_error_defined */

#ifdef f_SQLite_select_db_defined
  f_SQLite_select_db_fun_num =
#line 471 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("select_db", f_SQLite_select_db, tFunc(tStr,tVoid), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_select_db_defined */

#ifdef f_SQLite_create_db_defined
  f_SQLite_create_db_fun_num =
#line 475 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("create_db", f_SQLite_create_db, tFunc(tStr,tVoid), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_create_db_defined */

#ifdef f_SQLite_drop_db_defined
  f_SQLite_drop_db_fun_num =
#line 479 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("drop_db", f_SQLite_drop_db, tFunc(tStr,tVoid), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_drop_db_defined */

#ifdef f_SQLite_list_dbs_defined
  f_SQLite_list_dbs_fun_num =
#line 483 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    ADD_FUNCTION2("list_dbs", f_SQLite_list_dbs, tFunc(tNone,tArr(tStr)), 0, OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT);

#endif /* f_SQLite_list_dbs_defined */
#line 148 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
  SQLite_program=end_program();
#line 148 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
  SQLite_program_fun_num=add_program_constant("SQLite",SQLite_program,0);

#endif /* class_SQLite_defined */
#line 515 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
;
}

PIKE_MODULE_EXIT {

#ifdef class_SQLite_defined

#ifdef class_SQLite_ResObj_defined
  if(SQLite_ResObj_program) {
#line 157 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    free_program(SQLite_ResObj_program);
    SQLite_ResObj_program=0;
  }

#endif /* class_SQLite_ResObj_defined */
  if(SQLite_program) {
#line 148 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
    free_program(SQLite_program);
    SQLite_program=0;
  }

#endif /* class_SQLite_defined */
#line 519 "/Users/hww3/pikebuild/src/post_modules/SQLite/sqlite.cmod"
;
}

