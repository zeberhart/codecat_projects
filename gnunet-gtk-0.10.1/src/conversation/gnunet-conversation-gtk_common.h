/*
 * temporary header file
 */

#include <stdbool.h>
#include <stdint.h>
//#include <gnunet/gnunet_util_lib.h>

#include "gtk/gtk.h"
#include "gnunet_gtk.h"
#include <time.h>

#include "gnunet_gtk.h"
#include "gnunet/gnunet_identity_service.h"
#include "gnunet/gnunet_namestore_service.h"
#include "gnunet/gnunet_conversation_service.h"
#include "gnunet/gnunet_speaker_lib.h"
#include "gnunet/gnunet_microphone_lib.h"

#include "gnunet/gnunet_identity_service.h"
#include "gnunet/gnunet_namestore_service.h"
#include "gnunet/gnunet_gnsrecord_lib.h"
/*
 * macro's
 */
#define UPDATE_STATUS(format, ...) update_status(g_strdup_printf (format, ## __VA_ARGS__))

#define UPDATE_INFOBAR(format, ...) set_infobar_text(g_strdup_printf (format, ## __VA_ARGS__))

#define LOG(format, ...) GNUNET_CONVERSATION_GTK_log_message(g_strdup_printf (format, ## __VA_ARGS__))

#define get_object(name) GNUNET_CONVERSATION_GTK_get_main_window_object(name)



/*
 * active calls treevieuw columns
 */

enum {
  AL_caller_id, //*gchar
  AL_caller,  //*
  AL_caller_num, //gint
  AL_type, //gint
  AL_caller_state, //gint
  AL_call, //*
  AL_call_state, //gint
  AL_call_num //gint
};
/**
 * callerstate (state of incoming call)
 */

enum {
  CT_active,
  CT_suspended,
  CT_ringing,
  CT_dead,
  CT_hangup,
  CT_rejected,
  CT_other
};

/**
 * type of call
 */
enum {
    CALL_IN,
    CALL_OUT
};

/**
 * Possible states of the phone.
 */
enum PhoneState
{
  /**
   * We're waiting for our own idenitty.
   */
  PS_LOOKUP_EGO,

  /**
   * We're listening for calls
   */
  PS_LISTEN,

  /**
   * We accepted an incoming phone call.
   */
  PS_ACCEPTED,

  /**
   * Internal error
   */
  PS_ERROR
};

/**
 * States for current outgoing call.
 */
enum CallState
{
  /**
   * We are looking up some other participant.
   */
  CS_RESOLVING,

  /**
   * We are now ringing the other participant.
   */
  CS_RINGING,

  /**
   * The other party accepted our call and we are now connected.
   */
  CS_CONNECTED,

  /**
   * The call is currently suspended (by us).
   */
  CS_SUSPENDED
};




enum CallHistoryType
{
  CH_ACCEPTED,
  CH_REJECTED,
  CH_OUTGOING,
  CH_HANGUP,
  CH_MISSED
};


