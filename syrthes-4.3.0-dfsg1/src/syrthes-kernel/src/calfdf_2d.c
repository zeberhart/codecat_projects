/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_const.h"
# include "syr_option.h"
# include "syr_bd.h"
# include "syr_parall.h"
# include "syr_proto.h"

/* # include "mpi.h" */


extern struct Performances perfo;
extern struct Affichages affich;
                    
int ss_seg[2][2] = { {0,2},
	             {2,1} };

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | facecache_2d                                                         |
  |          Algorithme de detection rapide de faces cachees             |
  |======================================================================| */
void facecache_2d(int nelem,int **nod,double **coord,
		  double **xnf,double *pland,int *grcon,int *faces_cachees,
		  int ligne_deb,int ligne_fin)

{
  int i,j,k,n,ns;
  int noeud[4];
  int nbfcoplanaire,code_decoupe,nbfca;
  double xi[4],yi[4],dsign[4];
  double xn1,yn1,x,y,fforme;


  nbfca=0;
  nbfcoplanaire=0;

  for (i=ligne_deb ; i<ligne_fin ; i++ ) 
    {
      xn1=xnf[0][i];          yn1=xnf[1][i];
      noeud[0]=nod[0][i];     noeud[1]=nod[1][i];
      for  (j=0;j<nelem;j++)   
	{
	  if (i!=j && grcon[i]==grcon[j])  /* si les facettes sont dans la meme comp connexe */
	    {
	      fforme=coplanaire_2d(xn1,yn1,xnf[0][j],xnf[1][j]);
	      if (fforme<-1.)  /* les faces ne sont pas coplanaires */
		{
		  noeud[2]=nod[0][j];   noeud[3]=nod[1][j];
		  for (k=0;k<4;k++) {xi[k]=coord[0][noeud[k]]; yi[k]=coord[1][noeud[k]];}
		  derriere_2d(i,j,xnf,pland,xi,yi,dsign,&code_decoupe); 
		  if (code_decoupe!=0) nbfca += 1 ;
		}
	    }
	}
    }
  
  if (nbfca != 0 ) 
    {
      *faces_cachees=1 ;
      if (syrglob_nparts==1 || syrglob_rang==0)
	if (SYRTHES_LANG == FR)
	  printf("\n *** facecache_2d : Le maillage comporte des faces cachees (nbre=%d) \n",nbfca ) ;
	else if (SYRTHES_LANG == EN)
	  printf("\n *** facecache_2d : The mesh contains hidden faces (number=%d) \n",nbfca ) ;
    }
  else
    {
      *faces_cachees=0 ;
      if (syrglob_nparts==1 || syrglob_rang==0)
	if (SYRTHES_LANG == FR)
	  printf("\n *** facecache_2d : Le maillage ne comporte a priori pas de faces cachees \n" ) ;
	else if (SYRTHES_LANG == EN)
	  printf("\n *** facecache_2d : A priori, the mesh does not contain any hidden face \n" ) ;
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | facforme_2d                                                          |
  |           calcul des facteurs de forme en dimension 2                |
  |======================================================================| */
void facforme_2d(int nelem0,int npoin,int nelem,int **nod,double **coord,
		 double *pland,double **xnf,
		 struct FacForme ***listefdf,double *diagfdf,
		 int *grcon,int faces_cachees,struct Mask mask,
		 struct Horizon *horiz,int *typf,
		 struct SDparall_ray sdparall_ray)
{
    
  int i,j,k,l,ik,il,id,ii,jj,nt,nbtour,iloc,nelemloc,np,idecal;
  int noeud[6],prem,ndecoup;
  int *voir,intersect,arrivee;
  int codem10,code0,code1,codem1,code2,codem2,codem6;
  int nbfcoplanaire,code_decoupe;
  double xi[4],yi[4],fforme,xp[4],yp[4],xq[4],yq[4],xt[2],yt[2];
  double xn1,yn1,zn1,x,y,z,size_min,dim_boite[4],dsign[4];
  struct node *arbre;
  double titi, titi1, titi2;
  int ncomplique,pasok;
  double total_fac,pourcent,pourcent_ecrit,valmask;
  double *lignefdf,*fdfH;
  int *existefdf;
  int nbffcree=0;
  struct FacForme *pff,*qff,*ppff;
  int ligne_deb,ligne_fin;
  int nbeltmax_tree=30;
  int centre=1;

  /* Initialisations 
     --------------- */
  if(horiz->nelem) fdfH=horiz->fdf;


  if (sdparall_ray.npartsray>1)    {
    ligne_deb=sdparall_ray.ieledeb[sdparall_ray.rang];
    ligne_fin=sdparall_ray.ieledeb[sdparall_ray.rang+1];
    nelemloc=sdparall_ray.nelrayloc[sdparall_ray.rang];
  }
  else {
    ligne_deb=0;
    ligne_fin=nelem0;
    nelemloc=nelem0;
  }

  nbfcoplanaire=ncomplique=0 ;
  codem10=code0=code1=codem1=code2=codem2=codem6=0;
  
  lignefdf=(double*)malloc(nelem0*sizeof(double));
  verif_alloue_double1d("facforme_2d",lignefdf);
  existefdf=(int*)malloc(nelem0*sizeof(int));
  verif_alloue_int1d("facforme_2d",existefdf);


  if (faces_cachees)
    { 
      if (!centre)
	{
	  voir = (int *)malloc((npoin+1)*npoin/2*(sizeof(int))); 
	  if (voir==NULL) 
	    {
	      if (SYRTHES_LANG == FR)
		printf(" ERREUR facforme_2d : probleme d'allocation memoire\n");
	      else if (SYRTHES_LANG == EN)
		printf(" ERROR facforme_2d : Memory allocation problem\n");
	      syrthes_exit(1);
	    }
	  for (i=0 ; i<npoin*(npoin+1)/2 ; i++ ) *(voir+i) = -2 ; 
	  for (i=0 ; i<npoin ; i++) voir[i*npoin+i*(1-i)/2]=-1; 
	}
      size_min = 1.E8;
      arbre= (struct node *)malloc(sizeof(struct node));
      if (arbre==NULL) 
	{
	  if (SYRTHES_LANG == FR)
	    printf(" ERREUR facforme_2d : probleme d'allocation memoire\n"); 
	  else if (SYRTHES_LANG == EN)
	    printf(" ERROR facforme_2d : Memory allocation problem\n"); 
	  syrthes_exit(1);
	}
      build_quadtree_1d (arbre,npoin,nelem,nod,coord,&size_min,dim_boite,nbeltmax_tree);
    }
  
  titi1 =0;
  titi2 =0;
  pourcent_ecrit=0.1;

  nbtour=nelem/nelem0;

  for (i=ligne_deb;i<ligne_fin;i++) 
    /* --------------------*/
    {
      iloc=i-ligne_deb;

      if(affich.ray_fdf) 
	if (SYRTHES_LANG == FR)
	  printf(" *** facforme_2d : facette i=%d \n",i+1);
	else if (SYRTHES_LANG == EN)
	  printf(" *** facforme_2d : face i=%d \n",i+1);

      pourcent=i/nelem;
      if (pourcent>pourcent_ecrit)
	{
	  if (SYRTHES_LANG == FR)
	    printf("                    %5.2f %% du calcul effectue\n",pourcent_ecrit*100);
	  else if (SYRTHES_LANG == EN)
	    printf("                    %5.2f %% of calculation done\n",pourcent_ecrit*100);

	  pourcent_ecrit += 0.1;
	}
      
      xn1=xnf[0][i]; yn1=xnf[1][i]; noeud[0]=nod[0][i]; noeud[1]=nod[1][i];
      for (k=0;k<nelem0;k++) {lignefdf[k]=0; existefdf[k]=0;}  /* on met les fdf de la ligne a 0 */ 
      
      for  (j=0;j<nelem;j++)
	/* --------------------*/
	{
	  if (grcon[i]!=grcon[j]) {continue;}

	  titi1 =0;
	  valmask=1; pasok=0;
	  fforme = coplanaire_2d(xn1,yn1,xnf[0][j],xnf[1][j]);
	  if (fforme>-0.1) {nbfcoplanaire++; pasok=1;}
	  if (fforme<-1.)  
	    {
	      noeud[2]=nod[0][j]; noeud[3]=nod[1][j];
	      for (k=0;k<4;k++) {xi[k]=coord[0][noeud[k]];yi[k]=coord[1][noeud[k]];}
	      valmask=1;
	      if (!faces_cachees && mask.nelem) 
		{
		  valmask=cal_mask_2d(xi,yi,coord,mask);
		  if (SYRTHES_LANG == FR)
		    printf(">>> facforme2d : facette %d et %d on applique un masque de %f\n",i,j,valmask);
		  else if (SYRTHES_LANG == EN)
		    printf(">>> facforme2d : face %d and %d one applies a mask of %f\n",i,j,valmask);


		}
	      
	      if (!faces_cachees)
		{	
		  fforme=contou2d(xi,yi);
		  pasok=0;
		}
	      else
		{
		  derriere_2d(i,j,xnf,pland,xi,yi,dsign,&code_decoupe) ; 
		  if (code_decoupe!=-10 && mask.nelem) 
		    {
		      valmask=cal_mask_2d(xi,yi,coord,mask);
		      if (SYRTHES_LANG == FR)
			printf(">>> facforme2d : facette %d et %d on applique un masque de %f\n",i,j,valmask);
		      else if (SYRTHES_LANG == EN)
			printf(">>> facforme2d : face %d and %d one applies a mask of %f\n",i,j,valmask);

		    }
		  prem=1; fforme=0; ndecoup=0;
		  
		  if (code_decoupe == -10)    /* les faces sont derrieres */  
		    {codem10 += 1; fforme=0; pasok=1;} 
		  else if (code_decoupe == 0)       /* les faces se voient potentiellement */  
		    {
		      code0+=1;
		      if (centre)
			segfdfC(i,j,
				arbre,size_min,dim_boite,xi,yi,
				noeud,nod,coord,&ndecoup,&fforme);  
		      else
			segfdf(arbre,size_min,dim_boite,&prem,xi,yi, 
			       noeud,voir,npoin,nod,coord,&ndecoup,&fforme,&ncomplique);
		    } 
		  else if (fabs(code_decoupe) == 1 || fabs(code_decoupe)==2 ) 
		    { 
		      if (code_decoupe==1)       code1   += 1;
		      else if(code_decoupe==-1)  codem1  += 1;
		      else if(code_decoupe==2 )  code2   += 1;
		      else                       codem2  += 1;
		      decoupe_seg(i,j,xnf,pland,xi,yi,dsign,code_decoupe);
		      if (centre)
			segfdfC(i,j,
				arbre,size_min,dim_boite,xi,yi,
				noeud,nod,coord,&ndecoup,&fforme);  
		      else
			segfdf (arbre,size_min,dim_boite,&prem,xi,yi,
				noeud,voir,npoin,nod,coord,&ndecoup,&fforme,&ncomplique);  
		    } 
		  else if ( code_decoupe == -6 )   { codem6  += 1;}
		} 
	      
	    } /* if (fforme<-1.)  les faces ne sont pas coplanaires  */
	  
	  if(fforme < 0.) 
	    {
	      if (affich.ray_fdf) 
		if (SYRTHES_LANG == FR)
		  printf(" facforme2d : Facteur de forme negatif i %d j %d  fdf= %f\n",i+1,j+1,1e6*fforme);
		else if (SYRTHES_LANG == EN)
		  printf(" facforme2d : Negative view factor i %d j %d  fdf= %f\n",i+1,j+1,1e6*fforme);
	    }
	  else if (!horiz->nelem && pasok==0)
	    {
	      /* Essai chris - ne pas stocker les 0 - stockage des fdf superieurs a 1e-15*/
	      if (fforme>1e-15) {lignefdf[j%nelem0] += fforme*valmask;  existefdf[j%nelem0]+=1;}
	    }
	  else if (horiz->nelem)
	    if (typf[i]!=20 && typf[j]!=20)   /* ni i, ni j sont des facettes horizon */
	      {if (pasok==0) {lignefdf[j%nelem0] += fforme*valmask; existefdf[j%nelem0]+=1;}}

/* 	    else if (typf[i]==20 && typf[j]!=20)  */
/* 	      {fdfH[j]+= fforme*valmask;printf("iJ %d %d  fdfH=%f\n",i,j,1e6*fforme);}  */

	    else if (typf[i]!=20 && typf[j]==20) 
	      {fdfH[i]+= fforme*valmask;printf("Ij %d %d  fdfH=%f\n",i,j,1e6*fforme);} 
	  
	  /* Impression chris */
	  if (affich.ray_fdf)
	    {
/* 	      if (i==0) printf("i+1,j+1 %d  %d fdf %f\n",i+1,j+1,fforme*1.e6); */
	    }
	  titi1 += fforme;
	}  /* for  (j=i ; j<nel ; j++ ) */
  
  


      /* reste a compacter la ligne calculee dans la listefdf et creer la diagonale */
      for (j=0;j<nelem0;j++)
	{
	  if (existefdf[j]) /* s'il y a un fdf entre les facettes i et j a stocker */
	    {
	      if (j==i)  /* on est sur la diagonale */
		{
		  diagfdf[j-ligne_deb]=lignefdf[j];
		}
	      else
		{
		  /* sur quel proc est j ? */
		  idecal=np=0;
		  if (sdparall_ray.npartsray>1)
		    {
		      while (j>=sdparall_ray.ieledeb[np+1]) np++;
		      idecal=sdparall_ray.ieledeb[np];
		    }
		  qff=(struct FacForme*)malloc(sizeof(struct FacForme));
		  qff->numenface=j-idecal; 
		  qff->suivant=NULL;
		  qff->fdf=lignefdf[j];
		  nbffcree++;
		  
		  if (!listefdf[np][i-ligne_deb])
		    listefdf[np][i-ligne_deb]=qff;
		  else
		    {
		      pff=listefdf[np][i-ligne_deb];
		      while(pff) {ppff=pff; pff=pff->suivant;}
		      ppff->suivant=qff;
		    }
		}
	    }
	}
      
/*       if (SYRTHES_LANG == FR) */
/* 	printf("facforme_2d : ligne %d : on a cree %d fdf\n",i,nbffcree); */
/*       else if (SYRTHES_LANG == EN) */
/* 	printf("facforme_2d : ligne %d : on a cree %d fdf\n",i,nbffcree); */

    } /*  for (i=0 ; i<nel ; i++ )  */
  
  free(lignefdf);
  
  /* liberation de l'espace memoire */  
  if (faces_cachees || mask.nelem) {if (!centre) free(voir); tuer_tree(arbre);}
/*   free(lignefdf); */
  free(existefdf);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | dupliq2d_sym                                                         |
  |          Dupliquer le maillage pour le traitement des symtries       |
  |======================================================================| */
void dupliq2d_sym(struct SymPer defsymper,int n2,
		  struct Maillage maillnodray,struct Maillage *maillnod2)
{
  int i,j,n,i1,i2,ii1,ii2,idebnel,idebnp,numsym,inverse,nv;
  double a,b,c,d,t[3][3],t1[3][3],t2[3][3];

  /* On duplique noeuds, coordonnees et normales */
  dupliq_maill(maillnodray,maillnod2,n2);


  /* Creation des symetries */
  /* ---------------------- */
    
  numsym=0;

  for (i=0;i<defsymper.nbsym;i++)
    {
      a=defsymper.sym[i][0];
      b=defsymper.sym[i][1];
      c=defsymper.sym[i][2];
      d=-2./(a*a+b*b);
      t[0][0] = 1.+ a*a*d;
      t[1][1] = 1.+ b*b*d;
      t[0][1] = t[1][0] = a*b*d;
      t[0][2] = a*c*d;
      t[1][2] = b*c*d;
      t[2][0]=t[2][1]=0; t[2][2]=1;
      
      for (j=i;j<defsymper.nbsym;j++)
	{
	  inverse = -1;
	  if (i!=j)
	    {
	      inverse = 1;
	      a=defsymper.sym[j][0];
	      b=defsymper.sym[j][1];
	      c=defsymper.sym[j][2];
	      d=-2./(a*a+b*b);
	      t1[0][0] = 1.+ a*a*d;
	      t1[1][1] = 1.+ b*b*d;
	      t1[0][1] = t1[1][0] = a*b*d;
	      t1[0][2] = a*c*d;
	      t1[1][2] = b*c*d;
	      t1[2][0]=t1[2][1]=0; t1[2][2]=1;
	      
	      t2[0][0]= t[0][0]*t1[0][0]+t[0][1]*t1[1][0]+t[0][2]*t1[2][0];
	      t2[0][1]= t[0][0]*t1[0][1]+t[0][1]*t1[1][1]+t[0][2]*t1[2][1];
	      t2[0][2]= t[0][0]*t1[0][2]+t[0][1]*t1[1][2]+t[0][2]*t1[2][2];
	      
	      t2[1][1]= t[1][0]*t1[0][1]+t[1][1]*t1[1][1]+t[1][2]*t1[2][1];
	      t2[1][2]= t[1][0]*t1[0][2]+t[1][1]*t1[1][2]+t[1][2]*t1[2][2];;
	      
	      t2[2][2]= t[2][0]*t1[0][2]+t[2][1]*t1[1][2]+t[2][2]*t1[2][2];
	      
	      t2[1][0]=t2[0][1];
	      t2[2][0]=t2[0][2]; t2[2][1]=t2[1][2];
	    }
	  else
	    for (i1=0;i1<3;i1++)
	      for (i2=0;i2<3;i2++)
		t2[i1][i2]=t[i1][i2];
	  
	  numsym +=1;
	  idebnel = numsym*maillnodray.nelem;	
	  idebnp  = numsym*maillnodray.npoin;  
	  sym2d(t2,numsym,inverse,maillnodray,*maillnod2,idebnel,idebnp);
	  
	}
    }

  /* impressions de controle */
  /* imprime_maillage(*maillnod2); */
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | sym2d                                                                |
  |          Calculer le symetrique d'un maillage                        |
  |======================================================================| */
void sym2d(double t[3][3],int numsym,int inverse,
	   struct Maillage maillnodray,struct Maillage maillnod2,
	   int idebnel,int idebnp)
{
  int n,nv;
  double x,y;
      
  for (n=0;n<maillnodray.nelem;n++)
    {
      x=maillnodray.xnf[0][n];  y=maillnodray.xnf[1][n];
      maillnod2.xnf[0][idebnel+n] = t[0][0]*x+t[0][1]*y;
      maillnod2.xnf[1][idebnel+n] = t[1][0]*x+t[1][1]*y;
    }  

  for (n=0;n<maillnodray.npoin;n++)
    {	
      x=maillnodray.coord[0][n]; y=maillnodray.coord[1][n];
      maillnod2.coord[0][idebnp+n]=t[0][0]*x+t[0][1]*y + t[0][2]; 
      maillnod2.coord[1][idebnp+n]=t[1][0]*x+t[1][1]*y + t[1][2]; 
    } 
  
  if (inverse==-1)
    for (n=0;n<maillnodray.nelem;n++)
      {
        maillnod2.node[0][idebnel+n] = maillnodray.node[1][n] + idebnp;
        maillnod2.node[1][idebnel+n] = maillnodray.node[0][n] + idebnp;
      }
  else
    for (n=0;n<maillnodray.nelem;n++)
      {
        maillnod2.node[0][idebnel+n] = maillnodray.node[0][n] + idebnp;
        maillnod2.node[1][idebnel+n] = maillnodray.node[1][n] + idebnp;
      }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | surface_seg                                                          |
  |           calcul de la longueur des segments                         |
  |======================================================================| */
void surface_seg(struct Maillage maill)
{
  int i,*n0,*n1,**nod;
  double x01,y01,**coo;

  nod=maill.node;
  coo=maill.coord;
  
  for (i=0,n0=*nod,n1=*(nod+1);i<maill.nelem;i++,n0++,n1++)   
    {
      x01=coo[0][*n1]-coo[0][*n0];        
      y01=coo[1][*n1]-coo[1][*n0];
      maill.volume[i]=sqrt(x01*x01+y01*y01);
    }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | coplanaire_2d                                                        |
  |           retourne 0 si les faces sont coplanaires, -1000 sinon      |
  |======================================================================| */
double coplanaire_2d (double xn1,double yn1,double xn2,double yn2)
{
    double dnx,dny,dnz,xn,epscop;
    
    epscop = 1.E-3;
    dnx = xn1 - xn2;
    dny = yn1 - yn2;

    xn = sqrt(dnx*dnx + dny*dny);
	
    if (xn<epscop) 
	return(0);
    else if ( fabs(dnx)<epscop && fabs(dny)<epscop)
	return(0);
    else
	return(-1000);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | segfdf                                                               |
  |        organisation du calcul du facteur de forme entre 2 segments   |
  |======================================================================| */

void segfdf (struct node *arbre,double size_min,double dim_boite[],int *prem,
	     double xi[],double yi[],
	     int noeud[],int voir[],int npoin,int **nod,
	     double **coord,int *ndecoup,double *fforme,int *ncomplique)
 
{
  int i,ok,k,l,ik,il,ii,intersect,arrivee,nd;
  double ro[2],rd[2],pt_arr[2],xp[4],yp[4],xq[4],yq[4],fdf,epsff;
  struct node *noeud_dep,*noeud_arr;
  
  
  epsff=1.E-10;
  ok=0; 
  i=-1;

  /* 1- tester si tous les points se voient 
     -------------------------------------- */
  if (*prem)
    {
      *prem=0;
      for (k=0;k<2;k++)
	for (l=2;l<4;l++)
	  {
	    ik = noeud[k];  il = noeud[l];
	    if (il<ik) {ii=ik; ik=il; il=ii;}
	    
	    if (voir[indvoir(ik,il,npoin)]<-1)
	      {
		intersect=-1;  
		ro[0]=xi[k]; ro[1]=yi[k];
		pt_arr[0]=xi[l];pt_arr[1]=yi[l];
		rd[0]=pt_arr[0]-ro[0];  rd[1]=pt_arr[1]-ro[1];
		noeud_dep=arbre; noeud_arr=arbre;
		find_node_2d (&noeud_dep,ro[0],ro[1]);
		find_node_2d (&noeud_arr,pt_arr[0],pt_arr[1]);
		arrivee = 0;
		ivoitj_2d(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
			  &intersect,size_min,nod,coord,&arrivee,dim_boite);
		voir[indvoir(ik,il,npoin)] = intersect;
	      }
	    else
	      intersect=voir[indvoir(ik,il,npoin)];
	    if (intersect==-1) ok += 1;
	  }
    }
  
  
  else
    
    for (k=0;k<2;k++)
      for (l=2;l<4;l++) 
	{
	  intersect=-1;  
	  ro[0]=xi[k]; ro[1]=yi[k];
	  pt_arr[0]=xi[l];pt_arr[1]=yi[l]; 
	  rd[0]=pt_arr[0]-ro[0];  rd[1]=pt_arr[1]-ro[1];
	  noeud_dep=arbre; noeud_arr=arbre;
	  find_node_2d (&noeud_dep,ro[0],ro[1]);
	  find_node_2d (&noeud_arr,pt_arr[0],pt_arr[1]);
	  arrivee = 0;
	  
	  /* printf(" \n noeud depart %d noeud arrivee %d\n",noeud_dep->name,noeud_arr->name); */
	  ivoitj_2d(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
		    &intersect,size_min,nod,coord,&arrivee,dim_boite);
	  /* printf(">>> calfdf k l %d %d inter %d\n",k,l,intersect); */
	  if (intersect==-1) ok += 1;
	}
  
  
    /* 2- En fonction du resultat 
       -------------------------- */
  
  /* si ok=0 => faces non visibles, on ne rajoute rien a fforme */
  if ( 0 < ok && ok < 4 ) *ncomplique += 1;
  
  /* printf(">> facforme_2d : ok final =%d\n",ok);  */
  if (ok!=0)
    {
      fdf=contou2d(xi,yi); 
      /* printf(">> facforme_2d : estimation fdf =%f\n",fdf*1.E6);  */
      
      
      if (ok==4)
	{*fforme += fdf; }
      
      else if (*ndecoup>=ndecoup_max)
	{  *fforme = *fforme + (fdf*ok/4.);
	 }
      
      else
	{
	  nd=*ndecoup+1;
	  xp[0]=xi[0]; yp[0]=yi[0];
	  xp[1]=xi[1]; yp[1]=yi[1];
	  xp[2]=(xi[0]+xi[1])/2 ; yp[2]=(yi[0]+yi[1])/2.;
	  
	  xq[0]=xi[2]; yq[0]=yi[2]; 
	  xq[1]=xi[3]; yq[1]=yi[3];
	  xq[2]=(xi[2]+xi[3])/2 ; yq[2]=(yi[2]+yi[3])/2.;
	  
	  for (k=0;k<2;k++)
	    for (l=0;l<2;l++)
	      {
		xi[0]=xp[ss_seg[k][0]]; yi[0]=yp[ss_seg[k][0]];
		xi[1]=xp[ss_seg[k][1]]; yi[1]=yp[ss_seg[k][1]];
		xi[2]=xq[ss_seg[l][0]]; yi[2]=yq[ss_seg[l][0]];
		xi[3]=xq[ss_seg[l][1]]; yi[3]=yq[ss_seg[l][1]];
		segfdf (arbre,size_min,dim_boite,prem,xi,yi,
			noeud,voir,npoin,nod,coord,
			&nd,fforme,ncomplique); 
	      }
	  *ndecoup++;
	  
	}
    }        
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | dupliq2d_per                                                         |
  |          Dupliquer le maillage pour le traitement de la periodicite  |
  |          avec eventuellement un plan de symetrie orthogonal a l'axe  |
  |          definissant la periodicite de rotation                      |
  |======================================================================| */
void dupliq2d_per(struct SymPer defsymper,int n2,
		  struct Maillage maillnodray,struct Maillage *maillnod2)
{
  int i,j,n,i1,i2,ii1,ii2,idebnel,idebnp,numper,npersym,inverse,nv;
  double sa,sb,sc,sd,se,t[3][3],t1[3][3],t2[3][3];
  double phi,theta,an;
  double px,py,pz,ax,ay,az,alfa,angle,x,y;
    
  /* On duplique noeuds, coordonnees et normales */
  dupliq_maill(maillnodray,maillnod2,n2);


  /* Creation des symetries ou periodicites */
  /* -------------------------------------- */
  px = defsymper.per[0][0];
  py = defsymper.per[0][1];
  alfa = defsymper.per[0][2]*2*Pi/360.;

  for (i=0;i<defsymper.nbper-1;i++)
    {
      /* calcul de la matrice */
      angle = alfa*(i+1);
      
      t[0][0] =  cos(angle); t[0][1] = -sin(angle); t[0][2] = px;
      t[1][0] =  sin(angle); t[1][1] =  cos(angle); t[1][2] = py;
      t[2][0] = t[2][1]=0; t[2][2]=1.;
      
      inverse = 1 ;
      idebnel = (i+1)*maillnodray.nelem;	
      idebnp  = (i+1)*maillnodray.npoin;  
      
      for (n=0;n<maillnodray.nelem;n++)
	{
	  x=maillnodray.xnf[0][n];  y=maillnodray.xnf[1][n];
	  maillnod2->xnf[0][idebnel+n]=t[0][0]*x+t[0][1]*y;
	  maillnod2->xnf[1][idebnel+n]=t[1][0]*x+t[1][1]*y;
	}  
      
      for (n=0;n<maillnodray.npoin;n++)
	{	
	  x=maillnodray.coord[0][n];	    y=maillnodray.coord[1][n];
	  maillnod2->coord[0][idebnp+n]=t[0][0]*x+t[0][1]*y+t[0][2]; 
	  maillnod2->coord[1][idebnp+n]=t[1][0]*x+t[1][1]*y+t[1][2]; 
	} 

      for (n=0;n<maillnodray.nelem;n++)
	{
	  maillnod2->node[0][idebnel+n]=maillnodray.node[0][n]+ (i+1)*maillnodray.npoin; 
	  maillnod2->node[1][idebnel+n]=maillnodray.node[1][n]+ (i+1)*maillnodray.npoin;
	}  

    }                     


  /* impressions de controle */
  /* imprime_maillage(*maillnod2); */
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | dupliq2d_st                                                        |
  |======================================================================| */
void dupliq2d_st(struct SymPer defsymper,int nel,
		 int *typf,int *typf2,double **trm,double **trm2,int nbande)
{
  int i,nn,j,n,idebnel,numsym;
     
    for (i=0; i<nel;i++)
      {typf2[i]=typf[i];   for (nn=0; nn<nbande;nn++) trm2[nn][i]=trm[nn][i];}
  
  if (defsymper.nbsym)
    {
      numsym=0;
      for (i=0;i<defsymper.nbsym;i++)
	for (j=i;j<defsymper.nbsym;j++)
	  {
	    numsym++;
	    idebnel = numsym*nel;	
	    for (n=0;n<nel;n++)
	      {
		typf2[idebnel+n]=typf[n];
		for (nn=0; nn<nbande;nn++) trm2[nn][idebnel+n]=trm[nn][n];
	      }
	  }
    }

  else if(defsymper.nbper) 
    for (i=0;i<defsymper.nbper-1;i++)
      {
	idebnel = (i+1)*nel;	
	
	for (n=0;n<nel;n++)
	  {
	    typf2[idebnel+n]=typf[n];
	    for (nn=0; nn<nbande;nn++) trm2[nn][idebnel+n]=trm[nn][n];
	  }  
      }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | dimension_2d                                                         |
  |           dimensions caracteristiques du probleme                    |
  |======================================================================| */
void dimension_2d(struct Maillage maillnodray,
		  double *taille_boite,double *taille_seg)
{
  int i,*na,*nb;
  double xmin,xmax,ymin,ymax,xa,ya,xb,yb,ds;
  double *px,*py;
  
  xmin= 1.e10; ymin=  1.e6;
  xmax=-1.e10; ymax= -1.e6; 
  
  for (i=0,px=maillnodray.coord[0],py=maillnodray.coord[1];i<maillnodray.npoin;i++,px++,py++)
    {
      xmin=min(*px,xmin); xmax=max(*px,xmax);
      ymin=min(*py,ymin); ymax=max(*py,ymax);
    }
  
  *taille_boite=0.;
  *taille_boite=max(*taille_boite,(xmax-xmin));
  *taille_boite=max(*taille_boite,(ymax-ymin));
  
  
  *taille_seg=1.E8;
  
  for (i=0,na=maillnodray.node[0],nb=maillnodray.node[1];i<maillnodray.nelem;i++,na++,nb++)
    {
      xa=maillnodray.coord[0][*na]; ya=maillnodray.coord[1][*na];
      xb=maillnodray.coord[0][*nb]; yb=maillnodray.coord[1][*nb];
      ds=sqrt((xb-xa)*(xb-xa)+(yb-ya)*(yb-ya));
      *taille_seg=min(*taille_seg,ds);
    }

  if (syrglob_nparts==1 || syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      {
	printf("\n\n *** dimension_2d : Dimensions caracteristiques :\n"); 
	printf("                      encombrement   =%f \n",*taille_boite); 
	printf("                      plus petit segment=%f \n",*taille_seg);
      } 
    else if (SYRTHES_LANG == EN)
      {
	printf("\n\n *** dimension_2d : Dimension :\n"); 
	printf("                      overall dimension   =%f \n",*taille_boite); 
	printf("                      smallest segment=%f \n",*taille_seg);
      } 
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | cnorm2                                                               |
  |         Calcul de normales en dimension 2                            |
  |======================================================================| */
void cnor_2d(struct Maillage maill)
{
  int n,*n1,*n2;
  double *xn1,*xn2;
  double xn,yn,an;

  for (n=0,xn1=maill.xnf[0],xn2=maill.xnf[1];n<maill.nelem;n++,xn1++,xn2++) 
    {*xn1=*xn2=0.;}
  
  for (n=0,n1=maill.node[0],n2=maill.node[1],xn1=maill.xnf[0],xn2=maill.xnf[1];
       n<maill.nelem;
       n++,n1++,n2++,xn1++,xn2++) 
    {
      xn=-maill.coord[1][*n2] + maill.coord[1][*n1]; 
      yn= maill.coord[0][*n2] - maill.coord[0][*n1];
      an=sqrt(xn*xn+yn*yn);
      *xn1=xn/an; *xn2=yn/an;
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | box_2d                                                               |
  |         Optimisation de la boite englobante en 2d                    |
  |======================================================================| */
void box_2d(int npoin,double **cooray)
{
  int n,ok;
  double xm,ym,xm2,ym2,xmym;
  double c11,c22,c12,c21,a11,a12,a21,a22,a,b,c;
  double epsi,delta,x1,x2,ux,uy,vx,vy,x,y,an,z;

  epsi=1.e-6;

  xm=ym=0.;
  for (n=0;n<npoin;n++)
    {xm += cooray[0][n];ym += cooray[1][n];}
   xm /= npoin; ym /= npoin;  


  xm2=ym2=xmym=0.;
  for (n=0;n<npoin;n++)
    {
      xm2  += cooray[0][n]*cooray[0][n];
      ym2  += cooray[1][n]*cooray[1][n];
      xmym += cooray[0][n]*cooray[1][n];
    }
  
  c11 = xm2/npoin -xm*xm;
  c22 = ym2/npoin -ym*ym;
  c12 = c21 = xmym/npoin - xm*ym;
      
      
  a = 1.;
  b = - (c11+c22);
  c = c11*c22 - c12*c21;
    
  ok = 1;
  delta = b*b - 4*a*c;
  if (delta<0)
    {
/*       if (syrglob_nparts==1 ||syrglob_rang==0) */
/* 	if (SYRTHES_LANG == FR) */
/* 	  printf("$$ box_2d : On ne trouve pas de valeurs propres pour realigner la boite\n"); */
/* 	else if (SYRTHES_LANG == EN) */
/* 	  printf("$$ box_2d : No eigen values to align the box\n"); */
      ok=0;
    }
  else if (fabs(delta)<epsi)
    {
/*       if (syrglob_nparts==1 ||syrglob_rang==0) */
/* 	if (SYRTHES_LANG == FR) */
/* 	  printf("$$ box_2d : Les valeurs propres pour realigner la boite sont confondues\n"); */
/* 	else if (SYRTHES_LANG == EN) */
/* 	  printf("$$ box_2d : The eigen values to align the box are identical\n"); */
      ok=0;
    }
  else
    {
      x1 = -0.5*(b-sqrt(delta))/a; 
      x2 = -0.5*(b+sqrt(delta))/a; 
    }

  if (ok)
    {
      a11 = c11-x1; a12 = c12;
      a21 = c21   ; a22 = c22-x1;
      if (fabs(a11)>epsi)  { ux = a12/a11; uy = 1.;}
      else                { ux = 1;       uy = 0.;}
      
      a11 = c11-x2; a12 = c12;
      a21 = c21   ; a22 = c22-x2;
      if (fabs(a11)>epsi)  { vx = a12/a11; vy = 1.;}
      else                { vx = 1;       vy = 0.;}
      
      an = sqrt( ux*ux +  uy*uy);
      ux /= an; uy /= an;
      an = sqrt( vx*vx + vy*vy);
      vx /= an; vy /= an;
      
      z= ux*vy - uy*vx;
      if (z<0.)
	{ x=ux;  y=uy;
	  ux=vx; uy=vy;
	  vx=x;  vy=y;
	}


  
      for (n=0;n<npoin;n++)
	{x = ux*cooray[0][n] + vx*cooray[1][n];  y = uy*cooray[0][n] + vy*cooray[1][n];
	 cooray[0][n] = x;                       cooray[1][n] = y;  
	}

    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | contou2d                                                             |
  |                                                                      |
  |======================================================================| */
double contou2d(double xi[],double yi[])
{
  double ad,bc,ac,bd,fforme;

  ad = sqrt((xi[3]-xi[0])*(xi[3]-xi[0])+(yi[3]-yi[0])*(yi[3]-yi[0]));
  bc = sqrt((xi[2]-xi[1])*(xi[2]-xi[1])+(yi[2]-yi[1])*(yi[2]-yi[1]));
  ac = sqrt((xi[2]-xi[0])*(xi[2]-xi[0])+(yi[2]-yi[0])*(yi[2]-yi[0]));
  bd = sqrt((xi[3]-xi[1])*(xi[3]-xi[1])+(yi[3]-yi[1])*(yi[3]-yi[1]));
  
  fforme = 0.5*(ac+bd-ad-bc);
  return fforme;
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | segfdf                                                               |
  |        organisation du calcul du facteur de forme entre 2 segments   |
  |======================================================================| */
void segfdfC(int ii,int jj,
	     struct node *arbre,double size_min,double dim_boite[],
	     double xi[],double yi[],int noeud[],int **nod,
	     double **coord,int *ndecoup,double *fforme)
{  
  int code_decoupe,i,j,intersect,arrivee,nd;
  double dsign[4],xxi[4],yyi[4];
  double ro[2],rd[2],pt_arr[2];
  struct node *noeud_dep,*noeud_arr;

  if (*ndecoup==ndecoup_max)
    {
      ro[0]=0.5*(xi[0]+xi[1]);     ro[1]=0.5*(yi[0]+yi[1]);
      pt_arr[0]=0.5*(xi[2]+xi[3]); pt_arr[1]=0.5*(yi[2]+yi[3]);
      rd[0]=pt_arr[0]-ro[0];  rd[1]=pt_arr[1]-ro[1];
      noeud_dep=arbre; noeud_arr=arbre;
      find_node_2d (&noeud_dep,ro[0],ro[1]);
      find_node_2d (&noeud_arr,pt_arr[0],pt_arr[1]);
      arrivee=0; intersect=-1;
      ivoitj_2d(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
		&intersect,size_min,nod,coord,&arrivee,dim_boite);

      if (intersect==-1) *fforme+=contou2d(xi,yi); 
    }

  else
    {
      for (i=0;i<2;i++)
	{
	  if (i==0)      
	    {xxi[0]=xi[0];  yyi[0]=yi[0];
	     xxi[1]=0.5*(xi[0]+xi[1]);  yyi[1]=0.5*(yi[0]+yi[1]);}
	  else if (i==1) 
	    {xxi[0]=0.5*(xi[0]+xi[1]);  yyi[0]=0.5*(yi[0]+yi[1]);
	     xxi[1]=xi[1];  yyi[1]=yi[1];}
	  for (j=0;j<2;j++)
	    {
	      if (j==0)      
		{xxi[2]=xi[2];  yyi[2]=yi[2];
		 xxi[3]=0.5*(xi[2]+xi[3]); yyi[3]=0.5*(yi[2]+yi[3]);}
	      else if (j==1) 
		{xxi[2]=0.5*(xi[2]+xi[3]); yyi[2]=0.5*(yi[2]+yi[3]);
		 xxi[3]=xi[3];  yyi[3]=yi[3];}
	      nd=*ndecoup+1;
	      segfdfC(ii,jj,arbre,size_min,dim_boite,xxi,yyi,
		      noeud,nod,coord,&nd,fforme);  
	    }
	}      
      (*ndecoup)++;
    }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | cal_mask_2d                                                            |
  |         Detecter si deux point se voient                             |
  |======================================================================| */
double cal_mask_2d(double xi[],double yi[],double **cooray,struct Mask mask)
{
  int i;
  int na,nb;
  double xv,yv,epsi;
  double xa,ya,xb,yb,a,b,c,den,valmask,t,xp,yp;
  double ro[2],rd[2],pt_arr[2];

  epsi=1.E-6;

  valmask=1;
  ro[0]=0.5*(xi[0]+xi[1]);     ro[1]=0.5*(yi[0]+yi[1]);
  pt_arr[0]=0.5*(xi[2]+xi[3]); pt_arr[1]=0.5*(yi[2]+yi[3]);
  rd[0]=pt_arr[0]-ro[0];  rd[1]=pt_arr[1]-ro[1];



  for (i=0,nb=0;i<mask.nelem;i++)
    {
      na=mask.node[0][i]; nb=mask.node[1][i];
      xa=cooray[0][na]; ya=cooray[1][na];
      xb=cooray[0][nb]; yb=cooray[1][nb];
      a=ya-yb; b=xb-xa;  c= -(a*xa+b*ya);
      den = a*rd[0]+b*rd[1];
      if (fabs(den)<epsi) continue;
      t=-(c  + a*ro[0]+b*ro[1] ) / den; 
      if (t<epsi) continue;
      else if (t>(1+epsi)) continue;
      xp=ro[0]+t*rd[0];  yp=ro[1]+t*rd[1]; 
      if (in_seg(xa,ya,xb,yb,xp,yp)) valmask*=mask.opacite[i];
    }

  return valmask;
}


