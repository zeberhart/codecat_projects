# Norwegian bokmaal translation of gtkspell.
# Copyright (C) 2008 Free Software Foundation, Inc.
# This file is distributed under the same license as the gtkspell package.
# Roy Hvaara <roy.hvaara@gmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: gtkspell 2.0.6-pre1\n"
"Report-Msgid-Bugs-To: nathan@silverorange.com\n"
"POT-Creation-Date: 2004-06-10 19:31-0400\n"
"PO-Revision-Date: 2008-03-22 16:42+0100\n"
"Last-Translator: Roy Hvaara <roy.hvaara@gmail.com>\n"
"Language-Team: Norwegian Bokmaal <i18n-nb@lister.ping.uio.no>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: gtkspell/gtkspell.c:381
msgid "<i>(no suggestions)</i>"
msgstr "<i>(ingen forslag)</i>"

#: gtkspell/gtkspell.c:395
msgid "More..."
msgstr "Flere..."

#. + Add to Dictionary
#: gtkspell/gtkspell.c:421
#, c-format
msgid "Add \"%s\" to Dictionary"
msgstr "Legg til \"%s\" i ordboken"

#. - Ignore All
#: gtkspell/gtkspell.c:432
msgid "Ignore All"
msgstr "Ignorer alle"

#: gtkspell/gtkspell.c:465
msgid "Spelling Suggestions"
msgstr "Stavingsforslag"
