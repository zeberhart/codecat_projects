/*
     This file is part of GNUnet.
     (C) 2010-2013 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/conversation/gnunet-conversation-gtk_contacts.c
 * @brief 
 * @author yids
 * @author hark
 */
#include "gnunet-conversation-gtk_common.h"
#include "gnunet-conversation-gtk.h"


//#include "gnunet/gnunet_gnsrecord_lib.h"



///////////////////
//addressbook
//#include "gnunet_gtk.h"

//#include "gnunet/gnunet_identity_service.h"
//#include "gnunet/gnunet_namestore_service.h"
//#include "gnunet/gnunet_gnsrecord_lib.h"

/*************
 * contacts  *
 *************/
/**
 * Our configurations.
 */
static struct GNUNET_CONFIGURATION_Handle *cfg;

/**
 * Our ego.
 */
static struct GNUNET_IDENTITY_Ego *contacts_ego;



//static struct GNUNET_CONFIGURATION_Handle *cfg1;
//static struct GNUNET_CONFIGURATION_Handle *cfg2;



char *currentlySelectedCallAddress;

//static void
//identity_cb (void *cls, struct GNUNET_IDENTITY_Ego *ego, void **ctx,
//             const char *name);


/**
 * List of contacts (records).
 */
static GtkListStore *contacts_liststore;


/**
 * list of zones
 */
static GtkListStore *zone_liststore;

/**
 * zone treestore
 */
/*
static GtkTreeStore *zone_treestore;
*/

/**
 * zone treeview
 */
static GtkTreeView *zone_treeview;

/**
 * zone tree model
 */
static GtkTreeModel *zone_treemodel;



/**
 * List of contacts.
 */
//static GtkTreeStore *contacts_treestore;

/**
 * The main tree view for 'gns' that shows the records.
 */
static GtkTreeView *contacts_treeview;

/**
 * Tree model (same object as 'contacts_treestore', just different type).
 */
static GtkTreeModel *contacts_treemodel;

/*
 * List iterator for the 'list' operation.
 */
static struct GNUNET_NAMESTORE_ZoneIterator *list_it;


static struct GNUNET_IDENTITY_Ego *currentAddressBookEgo;
/**
 * Handle to identity service.
 */
static struct GNUNET_IDENTITY_Handle *id;



/**
 * Handle to the namestore.
 */
static struct GNUNET_NAMESTORE_Handle *ns;

/**
 * Private key for the our zone.
 */
static struct GNUNET_CRYPTO_EcdsaPrivateKey zone_pkey;

/**
  * Public key of the zone we are currently editing.
  */
/*
static struct GNUNET_CRYPTO_EcdsaPublicKey pubkey;
*/




/**
 * Name of the records to add/list/remove.
 */
 static char *name;

/**
 * Queue entry for the 'add' operation.
 */
static struct GNUNET_NAMESTORE_QueueEntry *add_qe;

/**
 * Queue entry for the 'del' operation.
 */
static struct GNUNET_NAMESTORE_QueueEntry *del_qe;



////////////////////////////
static void
display_record (void *cls, const struct GNUNET_CRYPTO_EcdsaPrivateKey *zone_key,
                const char *rname, unsigned int rd_len,
                const struct GNUNET_GNSRECORD_Data *rd)
{
/*  const char *typestring;*/
  char *s, *type;
  unsigned int i;
/*  const char *ets;*/
  struct GNUNET_TIME_Absolute at;
  struct GNUNET_TIME_Relative rt;
  GtkTreeIter display_iter;


  if (NULL == rname)
  {
  
    list_it = NULL;
    //test_finished ();
    return;
  }

  for (i = 0; i < rd_len; i++)
  {

    if ((GNUNET_GNSRECORD_TYPE_NICK == rd[i].record_type) &&
        (0 != strcmp (rname, "+")))
      continue;
/*    typestring = GNUNET_GNSRECORD_number_to_typename (rd[i].record_type);*/
    s = GNUNET_GNSRECORD_value_to_string (rd[i].record_type, rd[i].data,
                                          rd[i].data_size);
    if (NULL == s)
    {
      FPRINTF (stdout, _("\tCorrupt or unsupported record of type %u\n"),
               (unsigned int) rd[i].record_type);
      continue;
    }
    if (0 != (rd[i].flags & GNUNET_GNSRECORD_RF_RELATIVE_EXPIRATION))
    {
      rt.rel_value_us = rd[i].expiration_time;
/*      ets = GNUNET_STRINGS_relative_time_to_string (rt, GNUNET_YES);*/
    }
    else
    {
      at.abs_value_us = rd[i].expiration_time;
/*      ets = GNUNET_STRINGS_absolute_time_to_string (at);*/
    }
    if (rd[i].record_type == 65536)
    {
      type = "PKEY";
    }                           // if pubkey record
    if (rd[i].record_type == 65542)
    {
      type = "PHONE";
    }
//    FPRINTF (stdout, "%s", rname);
    if (rd[i].record_type == 65536 || rd[i].record_type == 65542)
    {
      gtk_list_store_append (contacts_liststore, &display_iter);
      gtk_list_store_set (contacts_liststore, &display_iter, 1, type, 0, rname, -1);
    }

/*        FPRINTF (stdout,
		     "\t%s: %s (%s)\t%s\t%s\t%s\n",
	   	  typestring,
	 	    s,
           	  ets,
           	  (0 != (rd[i].flags & GNUNET_GNSRECORD_RF_PRIVATE)) ? "PRIVATE" : "PUBLIC",
            	  (0 != (rd[i].flags & GNUNET_GNSRECORD_RF_SHADOW_RECORD)) ? "SHADOW" : "",
            	  (0 != (rd[i].flags & GNUNET_GNSRECORD_RF_PENDING)) ? "PENDING" : "");
*/
    //gtk_widget_show(contacts_liststore);
    GNUNET_free (s);
  }
//  FPRINTF (stdout, "%s", "\n");
  
  GNUNET_NAMESTORE_zone_iterator_next (list_it);
//  GNUNET_NAMESTORE_zone_iteration_stop(list_it);

}



/**
 * Function called by identity service with information about egos.
 *
 * @param cls NULL
 * @param ego ego handle
 * @param ctx unused
 * @param name name of the ego
 */
static void
identity_cb (void *cls, struct GNUNET_IDENTITY_Ego *ego, void **ctx,
             const char *name)
{
/*
  struct GNUNET_CRYPTO_EcdsaPublicKey pk;
  char *s;
*/
  GtkTreeIter iter;
  
  if (NULL != ego) 
  { 
/*
  GNUNET_IDENTITY_ego_get_public_key (ego, &pk);
  s = GNUNET_CRYPTO_ecdsa_public_key_to_string (&pk);
*/
  fprintf (stderr, "contacts idenity_cb: %s \n", name);


  /* FIXME: this should be done in gnunet-conversation-gtk.c */
  gtk_list_store_insert_with_values (zone_liststore,
                                       &iter, -1,
                                       0, name,
                                       1, ego,
                                       -1); 

  }

  contacts_ego = ego;

  
//  zone_pkey = *GNUNET_IDENTITY_ego_get_private_key (caller_id);
  ns = GNUNET_NAMESTORE_connect (cfg);
     
}
/**
 * Continuation called to notify client about result of the
 * operation.
 *
 * @param cls closure, location of the QueueEntry pointer to NULL out
 * @param success #GNUNET_SYSERR on failure (including timeout/queue drop/failure to validate)
 *                #GNUNET_NO if content was already there
 *                #GNUNET_YES (or other positive value) on success
 * @param emsg NULL on success, otherwise an error message
 */
static void
add_continuation (void *cls, int32_t success, const char *emsg)
{

  struct GNUNET_NAMESTORE_QueueEntry **qe = cls;

  *qe = NULL;
  if (GNUNET_YES != success)
  {
    fprintf (stderr, _("Adding record failed: %s\n"),
             (GNUNET_NO == success) ? "record exists" : emsg);
    if (GNUNET_NO != success)
      LOG("GNUNET_NO != success");
        //ret = 1;
  }
  LOG("blaat");
  //ret = 0;
  //test_finished ();
}

/**
 * Continuation called to notify client about result of the
 * operation.
 *
 * @param cls closure, unused
 * @param success #GNUNET_SYSERR on failure (including timeout/queue drop/failure to validate)
 *                #GNUNET_NO if content was already there
 *                #GNUNET_YES (or other positive value) on success
 * @param emsg NULL on success, otherwise an error message
 */
static void
del_continuation (void *cls, int32_t success, const char *emsg)
{
  del_qe = NULL;
  if (GNUNET_NO == success)
  {
    fprintf (stderr, _("Deleting record failed, record does not exist%s%s\n"),
             (NULL != emsg) ? ": " : "", (NULL != emsg) ? emsg : "");
  }
  if (GNUNET_SYSERR == success)
  {
    fprintf (stderr, _("Deleting record failed%s%s\n"),
             (NULL != emsg) ? ": " : "", (NULL != emsg) ? emsg : "");
  }
  // test_finished ();
}

/*
 * add a new contact
 * @param name
 * @param address
 */
static void
add_contact (const gchar * name, const gchar * address)
{
//      memmove(&address+1,&address+51,1);
  GtkTreeIter iter;
  struct GNUNET_GNSRECORD_Data rd;
  struct GNUNET_GNSRECORD_Data *rde;
  static void *data;
  static size_t data_size;
  struct GNUNET_CRYPTO_EcdsaPublicKey pkey;
  
  struct GNUNET_IDENTITY_Ego *tempEgo;
  char *tempName;
  GtkTreeIter testIter;
  gtk_combo_box_get_active_iter(GTK_COMBO_BOX (GNUNET_CONVERSATION_GTK_get_main_window_object  ("gnunet_conversation_gtk_contacts_zone_combobox")), &testIter);

  gtk_tree_model_get (GTK_TREE_MODEL (zone_liststore),
                      &testIter,
                      0, &tempName,
                      1, &tempEgo,
                      -1);
  
  
  GNUNET_CRYPTO_ecdsa_public_key_from_string (address, strlen (address), &pkey);
  rde = &rd;
  zone_pkey = *GNUNET_IDENTITY_ego_get_private_key (tempEgo);
  rd.data = &pkey;
  rd.data_size = sizeof (struct GNUNET_CRYPTO_EcdsaPublicKey);
  rd.record_type = GNUNET_GNSRECORD_TYPE_PKEY;
  rd.flags |= GNUNET_GNSRECORD_RF_RELATIVE_EXPIRATION;  // always set to relative for testing purposes
  rde->flags |= GNUNET_GNSRECORD_RF_PRIVATE;
  rde->expiration_time = GNUNET_TIME_UNIT_FOREVER_ABS.abs_value_us;
  //FPRINTF (stderr, "adding\n");
  //FPRINTF (stderr, "name: %s\n", name);
  //FPRINTF (stderr, "address: %s\n", address);
  if (GNUNET_OK !=
      GNUNET_GNSRECORD_string_to_value (65536, address, &data, &data_size))
  {
    FPRINTF (stderr, "invalid address\n");
  }
  else
  {
    add_qe =
        GNUNET_NAMESTORE_records_store (ns, &zone_pkey, name, 1, rde,
                                        &add_continuation, &add_qe);
    gtk_list_store_append (contacts_liststore, &iter);
    gtk_list_store_set (contacts_liststore, &iter, 1, "PKEY", 0, name, -1);
  }
}
/*
 * executed when clicked on add contact
 * @param button
 * @param user_data
 */

void
GNUNET_CONVERSATION_GTK_on_add_clicked (GtkButton * button,
                                        gpointer * user_data)
{

  GtkEntry *nameEntry, *addressEntry;

  nameEntry = GTK_ENTRY (GNUNET_CONVERSATION_GTK_get_main_window_object  ("GNUNET_GTK_conversation_nameAdd"));
  addressEntry = GTK_ENTRY (GNUNET_CONVERSATION_GTK_get_main_window_object ("GNUNET_GTK_conversation_addressAdd"));
  FPRINTF(stderr,"name %s \n", gtk_entry_get_text (nameEntry));
  FPRINTF(stderr,"addr %s \n", gtk_entry_get_text (addressEntry));

  add_contact (gtk_entry_get_text (nameEntry),gtk_entry_get_text(addressEntry));
}

void
GNUNET_CONVERSATION_GTK_on_remove_clicked (GtkButton * button,
                                           gpointer * user_data)
{
  GtkTreeSelection *selection;
  GtkTreeModel *model;
  GtkTreeIter iter;
  GtkDialog *confirm;
  GtkWindow *main_window;
  GtkLabel *notification;
  GtkHBox *content_area;

  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (contacts_treeview));
  if (gtk_tree_selection_get_selected (selection, &model, &iter))
  {
    gtk_tree_model_get (model, &iter, 0, &name, -1);
    //FPRINTF (stderr, "selected %s \n", name);
    main_window = GTK_WINDOW (GNUNET_CONVERSATION_GTK_get_main_window_object  ("GNUNET_GTK_conversation_window"));
    confirm =
        GTK_DIALOG (gtk_dialog_new_with_buttons
                    ("Removing contact", main_window,
                     GTK_DIALOG_DESTROY_WITH_PARENT, _("Yes"),
                     GTK_RESPONSE_ACCEPT, _("No"), GTK_RESPONSE_CANCEL, NULL));
    content_area =
        GTK_HBOX (gtk_dialog_get_content_area (GTK_DIALOG (confirm)));
    notification =
        GTK_LABEL (gtk_label_new
                   ("Do you really want to remove this contact?"));
    gtk_container_add (GTK_CONTAINER (content_area), GTK_WIDGET (notification));
    gtk_widget_show_all (GTK_WIDGET (confirm));
    switch (gtk_dialog_run (confirm))
    {
    case GTK_RESPONSE_ACCEPT:
      zone_pkey = *GNUNET_IDENTITY_ego_get_private_key (contacts_ego);
      del_qe =
          GNUNET_NAMESTORE_records_store (ns, &zone_pkey, name, 0, NULL,
                                          &del_continuation, NULL);
      gtk_list_store_remove (contacts_liststore, &iter);
      gtk_widget_destroy (GTK_WIDGET (confirm));
      break;
    case GTK_RESPONSE_CANCEL:
      FPRINTF (stderr, "not removing \n");
      gtk_widget_destroy (GTK_WIDGET (confirm));
      break;
    }

  }
}

void
GNUNET_CONVERSATION_GTK_on_current_clicked (GtkButton * button,
                                        gpointer * user_data)
{
  GtkEntry *addressEntry;
//  FPRINTF(stderr,"on current clicked %s\n",callerName);
  //char *strippedAddress; 
  addressEntry = GTK_ENTRY (GNUNET_CONVERSATION_GTK_get_main_window_object  ("GNUNET_GTK_conversation_addressAdd"));
  currentlySelectedCallAddress[52] = '\0';
  gtk_entry_set_text(addressEntry, currentlySelectedCallAddress);
    
}


/*
 * row activated
 * @return void
 */

void
GNUNET_CONVERSATION_GTK_row_activated ()
{
  gchar *callAddress;
  gchar *type;

//  FPRINTF (stderr, "row activated \n");

  GtkTreeSelection *selection;

  GtkTreeIter iterA;


  selection = gtk_tree_view_get_selection (contacts_treeview);

  gtk_tree_selection_get_selected (selection, &contacts_treemodel, &iterA);
  gtk_tree_model_get (contacts_treemodel, &iterA, 0, &name, 1, &type, -1);
//  g_print ("ego name %s\n", ego_name);
//  g_print ("selected row is: %s\n", name);
//  g_print ("selected rowtype is: %s\n", type);


  g_print ("type @row active%s", type);
  if (strcmp (type, "PKEY") == 0)
  {
    GNUNET_asprintf (&callAddress, "call.%s.gnu", name);
  }
  if (strcmp (type, "PHONE") == 0)
  {
    GNUNET_asprintf (&callAddress, "%s.gnu", name);
  }
//   else { GNUNET_asprintf(&callAddress, "%s", peer_id);}

  g_print ("ego name %s\n", callAddress);
  GtkEntry *address_entry;

  address_entry = GTK_ENTRY ( GNUNET_CONVERSATION_GTK_get_main_window_object ("GNUNET_GTK_conversation_address"));
  gtk_entry_set_text (address_entry, callAddress);
  do_call (callAddress);
}

/*
static void
print_ego (void *cls,
	   struct GNUNET_IDENTITY_Ego *ego,
	   void **ctx,
	   const char *identifier)
{
  struct GNUNET_CRYPTO_EcdsaPublicKey pk;
  char *s;
  GNUNET_IDENTITY_ego_get_public_key (ego, &pk);
  s = GNUNET_CRYPTO_ecdsa_public_key_to_string (&pk);
  fprintf (stdout, "%s \n", identifier);
//  GNUNET_free (s);
}
*/
/*
static void 
setCurrentAddressbookEgo(void *cls, struct GNUNET_IDENTITY_Ego *ego)
{
  currentAddressBookEgo = ego;
}
*/

/**
 * A different zone was selected in the zone toggle bar.  Load the
 * appropriate zone.
 *
 * @param widget button that was toggled (could be to "on" or "off", we only react to "on")
 * @param user_data builder, unused
 */
void
gnunet_conversation_gtk_contacts_zone_combobox_changed_cb (GtkComboBox *widget,
                                               gpointer user_data)
{
  GtkTreeIter contacts_zone_iter;
  struct GNUNET_IDENTITY_Ego *tempEgo;
  char *tempName;
  struct GNUNET_CRYPTO_EcdsaPrivateKey temp_zone_pkey;
  //GtkTreeSelection *selection;
  //GtkTreeIter iterA;
  

  gtk_combo_box_get_active_iter(widget, &contacts_zone_iter);   

  gtk_tree_model_get (GTK_TREE_MODEL (zone_liststore),
                      &contacts_zone_iter,
                      0, &tempName,
                      1, &tempEgo,
                      -1);
//  FPRINTF(stderr,"blat: %s\n", tempName);
  temp_zone_pkey = *GNUNET_IDENTITY_ego_get_private_key (tempEgo);
  gtk_list_store_clear(contacts_liststore);
/*
  GNUNET_IDENTITY_ego_lookup (cfg,
	  	      	      tempName,
   		 	      setCurrentAddressbookEgo,
		 	      NULL); 
*/	  
  //selection = gtk_tree_view_get_selection (zone_treeview);
  
  //gtk_tree_selection_get_selected (selection, &zone_treemodel, &iterA);
  
  //gtk_tree_model_get_iter_first(zone_treemodel, &iterA);
  //gtk_tree_model_get (zone_treemodel, &iterA, 0, &tempName, 1, &tempEgo, -1);
 

 list_it =
    GNUNET_NAMESTORE_zone_iteration_start (ns, &temp_zone_pkey, &display_record,
                                           NULL);
  
//  GNUNET_IDENTITY_disconnect (id);

}


    
//////    
extern void
GNUNET_CONVERSATION_GTK_CONTACTS_init ()
{
  GtkTreeIter iterContactsInit;

  cfg = GIG_get_configuration ();


  // contacts
  contacts_liststore =
      GTK_LIST_STORE (GNUNET_CONVERSATION_GTK_get_main_window_object
                      ("gnunet_conversation_gtk_contacts_liststore"));
  contacts_treeview =
      GTK_TREE_VIEW (GNUNET_CONVERSATION_GTK_get_main_window_object ("gnunet_conversation_gtk_treeview"));
  contacts_treemodel = GTK_TREE_MODEL (contacts_liststore);

    // zone list
  zone_liststore =
     GTK_LIST_STORE (GNUNET_CONVERSATION_GTK_get_main_window_object ("gnunet_conversation_gtk_contacts_zone_liststore")); 

// zone_treestore =
//      GTK_TREE_STORE (GNUNET_CONVERSATION_GTK_get_main_window_object ("gnunet_conversation_gtk_contacts_zone_treestore"));
  zone_treemodel = GTK_TREE_MODEL (zone_liststore);
 zone_treeview =
      GTK_TREE_VIEW (get_object ("gnunet_conversation_gtk_zone_treeview"));

  gtk_tree_model_get_iter_first(zone_treemodel, &iterContactsInit);
  gtk_tree_model_iter_next(zone_treemodel, &iterContactsInit);

  gtk_combo_box_set_active_iter(GTK_COMBO_BOX (GNUNET_CONVERSATION_GTK_get_main_window_object ("gnunet_conversation_gtk_contacts_zone_combobox")), &iterContactsInit);

  id = GNUNET_IDENTITY_connect (cfg, &identity_cb, NULL);
//  zone_pkey = *GNUNET_IDENTITY_ego_get_private_key (caller_id);
//  gtk_combo_box_set_active(GTK_WIDGET (get_object ("gnunet_conversation_gtk_contacts_zone_combobox")), 1);
//  gtk_combo_box_set_active(GTK_WIDGET (get_object (ml,"gnunet_conversation_gtk_outgoing_zone_combobox")), 1);

}

extern void
GNUNET_CONVERSATION_GTK_CONTACTS_shutdown()
{

  if (NULL != id)
  {
    GNUNET_IDENTITY_disconnect (id);
    id = NULL;
  } else {
      GNUNET_break(0);
  }

  if (NULL != list_it)
  {
  FPRINTF(stderr,"LIST_IT == NULL");
//  GNUNET_NAMESTORE_zone_iteration_stop(list_it);
  }
  if (NULL != ns)
  {
    GNUNET_NAMESTORE_disconnect (ns);
    ns = NULL;
  } else {
     GNUNET_break(0);
  }

  GNUNET_CRYPTO_ecdsa_key_clear (&zone_pkey);

}




