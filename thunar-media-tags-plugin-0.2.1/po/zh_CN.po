# Simplified Chinese translations for
# the thunar-media-tags-plugin package.
# This file is distributed under the same license as
# the thunar-media-tags-plugin package.
# Hunt Xu <huntxu@live.cn>, 2008.
# Chipong Luo <chipong.luo@yahoo.com>, 2011, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: thunar-media-tags-plugin\n"
"Report-Msgid-Bugs-To: <xfce-i18n@xfce.org>\n"
"POT-Creation-Date: 2012-04-20 11:18+0000\n"
"PO-Revision-Date: 2008-12-04 16:51+0800\n"
"Last-Translator: Chipong Luo <chipong.luo@yahoo.com>\n"
"Language-Team: Chinese (Simplified) <xfce-i18n-cn@xfce.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../thunar-plugin/tag-renamer.c:59 ../thunar-plugin/tag-renamer.c:657
msgid "Title"
msgstr "标题"

#: ../thunar-plugin/tag-renamer.c:60
msgid "Artist - Title"
msgstr "艺术家-标题"

#: ../thunar-plugin/tag-renamer.c:61
msgid "Track - Title"
msgstr "音轨-标题"

#: ../thunar-plugin/tag-renamer.c:62
msgid "Track - Artist - Title"
msgstr "音轨-艺术家-标题"

#: ../thunar-plugin/tag-renamer.c:63
msgid "Track. Title"
msgstr "音轨.标题"

#: ../thunar-plugin/tag-renamer.c:64
msgid "Track. Artist - Title"
msgstr "音轨.艺术家-标题"

#: ../thunar-plugin/tag-renamer.c:65
msgid "Artist - Track - Title"
msgstr "艺术家-音轨-标题"

#: ../thunar-plugin/tag-renamer.c:67
msgid "Custom"
msgstr "自定义"

#. Custom format
#: ../thunar-plugin/tag-renamer.c:243
msgid "Cust_om format:"
msgstr "自定义格式(_O)："

#. Format label
#: ../thunar-plugin/tag-renamer.c:271
msgid "_Format:"
msgstr "格式(_F)："

#: ../thunar-plugin/tag-renamer.c:312
msgid "_Underscores"
msgstr "下划线(_U)"

#: ../thunar-plugin/tag-renamer.c:314
msgid ""
"Activating this option will replace all spaces in the target filename with "
"underscores."
msgstr "激活此项会用下划线代替目标文件名中的所有空格"

#: ../thunar-plugin/tag-renamer.c:319
msgid "_Lowercase"
msgstr "小写字母(_L)"

#: ../thunar-plugin/tag-renamer.c:321
msgid ""
"If you activate this, the resulting filename will only contain lowercase "
"letters."
msgstr "激活此项会使最终文件名中只包含小写字母。"

#: ../thunar-plugin/tag-renamer.c:464 ../thunar-plugin/audio-tags-page.c:182
msgid "Unknown Artist"
msgstr "未知艺术家"

#: ../thunar-plugin/tag-renamer.c:472 ../thunar-plugin/audio-tags-page.c:208
msgid "Unknown Title"
msgstr "未知标题"

#. Edit tags action
#: ../thunar-plugin/tag-renamer.c:627
msgid "Edit _Tags"
msgstr "编辑标签(_T)"

#: ../thunar-plugin/tag-renamer.c:627
msgid "Edit ID3/OGG tags of this file."
msgstr "编辑此文件的 ID3/OGG 标签"

#: ../thunar-plugin/tag-renamer.c:646
msgid "Tag Help"
msgstr "标签帮助"

#: ../thunar-plugin/tag-renamer.c:657
msgid "Artist"
msgstr "艺术家"

#: ../thunar-plugin/tag-renamer.c:658
msgid "Album"
msgstr "专辑"

#: ../thunar-plugin/tag-renamer.c:658
msgid "Genre"
msgstr "流派"

#: ../thunar-plugin/tag-renamer.c:659
msgid "Track number"
msgstr "音轨编号"

#: ../thunar-plugin/tag-renamer.c:659
msgid "Year"
msgstr "年份"

#: ../thunar-plugin/tag-renamer.c:660
msgid "Comment"
msgstr "注释"

#: ../thunar-plugin/tag-renamer.c:717
msgid "Audio Tags"
msgstr "音频标签"

#: ../thunar-plugin/audio-tags-page.c:195
msgid "Unknown Album"
msgstr "未知专辑"

#: ../thunar-plugin/audio-tags-page.c:328
msgid "<b>Track:</b>"
msgstr "<b>音轨：</b>"

#: ../thunar-plugin/audio-tags-page.c:343
msgid "Enter the track number here."
msgstr "在这里输入音轨编号。"

#: ../thunar-plugin/audio-tags-page.c:351
msgid "<b>Year:</b>"
msgstr "<b>年份：</b>"

#: ../thunar-plugin/audio-tags-page.c:366
msgid "Enter the release year here."
msgstr "在这里输入发行年份。"

#: ../thunar-plugin/audio-tags-page.c:374
msgid "<b>Artist:</b>"
msgstr "<b>艺术家：</b>"

#: ../thunar-plugin/audio-tags-page.c:381
msgid "Enter the name of the artist or author of this file here."
msgstr "在这里输入艺术家或此文件作者的姓名。"

#: ../thunar-plugin/audio-tags-page.c:389
msgid "<b>Title:</b>"
msgstr "<b>标题：</b>"

#: ../thunar-plugin/audio-tags-page.c:396
msgid "Enter the song title here."
msgstr "在这里输入歌曲标题。"

#: ../thunar-plugin/audio-tags-page.c:403
msgid "<b>Album:</b>"
msgstr "<b>专辑：</b>"

#: ../thunar-plugin/audio-tags-page.c:410
msgid "Enter the album/record title here."
msgstr "在这里输入专辑/唱片标题。"

#: ../thunar-plugin/audio-tags-page.c:417
msgid "<b>Comment:</b>"
msgstr "<b>注释：</b>"

#: ../thunar-plugin/audio-tags-page.c:424
msgid "Enter your comments here."
msgstr "在此输入您的注释。"

#: ../thunar-plugin/audio-tags-page.c:431
msgid "<b>Genre:</b>"
msgstr "<b>流派：</b>"

#: ../thunar-plugin/audio-tags-page.c:438
msgid "Select or enter the genre of this song here."
msgstr "在此选择或输入歌曲流派。"

#. Create and add the save action
#: ../thunar-plugin/audio-tags-page.c:449
msgid "_Save"
msgstr "保存(_S)"

#: ../thunar-plugin/audio-tags-page.c:449
msgid "Save audio tags."
msgstr "保存音频标签。"

#. Create and add the info action
#: ../thunar-plugin/audio-tags-page.c:456
msgid "_Information"
msgstr "信息(_I)"

#: ../thunar-plugin/audio-tags-page.c:456
msgid "Display more detailed information about this audio file."
msgstr "显示此音频文件的相关详细信息。"

#: ../thunar-plugin/audio-tags-page.c:524
msgid "Audio"
msgstr "音频"

#. Set up the dialog
#: ../thunar-plugin/audio-tags-page.c:560
msgid "Edit Tags"
msgstr "编辑标签"

#. Create dialog
#: ../thunar-plugin/audio-tags-page.c:982
msgid "Audio Information"
msgstr "音频信息"

#: ../thunar-plugin/audio-tags-page.c:993
#, c-format
msgid "%d:%d Minutes"
msgstr "%d:%d 分钟"

#: ../thunar-plugin/audio-tags-page.c:994
#, c-format
msgid "%d KBit/s"
msgstr "%d K/s"

#: ../thunar-plugin/audio-tags-page.c:995
#, c-format
msgid "%d Hz"
msgstr "%d Hz"

#: ../thunar-plugin/audio-tags-page.c:1019
msgid "<b>Filename:</b>"
msgstr "<b>文件名：</b>"

#: ../thunar-plugin/audio-tags-page.c:1032
msgid "<b>Filesize:</b>"
msgstr "<b>文件大小：</b>"

#: ../thunar-plugin/audio-tags-page.c:1045
msgid "<b>MIME Type:</b>"
msgstr "<b>MIME 类型：</b>"

#: ../thunar-plugin/audio-tags-page.c:1058
msgid "<b>Bitrate:</b>"
msgstr "<b>比特率：</b>"

#: ../thunar-plugin/audio-tags-page.c:1071
msgid "<b>Samplerate:</b>"
msgstr "<b>采样率：</b>"

#: ../thunar-plugin/audio-tags-page.c:1084
msgid "<b>Channels:</b>"
msgstr "<b>声道：</b>"

#: ../thunar-plugin/audio-tags-page.c:1097
msgid "<b>Length:</b>"
msgstr "<b>长度：</b>"
