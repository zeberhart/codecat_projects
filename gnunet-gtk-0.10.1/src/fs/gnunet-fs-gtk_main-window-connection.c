/*
     This file is part of GNUnet
     (C) 2011, 2012 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/fs/gnunet-fs-gtk_main-window-connection.c
 * @author LRN
 * @brief event handlers for the connection indicator in the main window
 */
#include "gnunet-fs-gtk_common.h"
#include "gnunet-fs-gtk.h"

/**
 * Update the connection indicator widget.
 *
 * @param main_ctx context
 * @param connected TRUE if connected to arm, FALSE otherwise
 * @param tooltip new tooltip text
 */
void
GNUNET_FS_GTK_update_connection_indicator (
    struct GNUNET_GTK_MainWindowContext *main_ctx,
    gboolean connected, const gchar *tooltip)
{
  gtk_image_set_from_stock (main_ctx->connection_indicator,
      connected ? "gtk-yes" : "gtk-no", GTK_ICON_SIZE_BUTTON);
  gtk_widget_set_tooltip_text (GTK_WIDGET (main_ctx->connection_indicator),
      tooltip);
}

/**
 * Runs gnunet-armview-gtk (no, armview does not exist. Yet?).
 * @param main_ctx main window context
 */
static void
run_armview (struct GNUNET_GTK_MainWindowContext *main_ctx)
{
  /* TODO: implement armview? */
}

/**
 * User clicked on the connection indicator in the main window.
 *
 * @param widget the image widget
 * @param event event describing the button that was clicked
 * @param user_data the main window context
 */
gboolean
GNUNET_FS_GTK_main_window_connection_indicator_button_press_event_cb (
    GtkWidget *widget, GdkEvent  *event, gpointer user_data)
{
  run_armview (user_data);
  return FALSE;
}

/* end of gnunet-fs-gtk_main-window-search.c */
