#pike __REAL_VERSION__

// $Id: 8fb3ce0bbf9fc5ef70a58b5b2ccf203a2af0bbab $

protected constant name = "Mozilla Public License 1.1";
protected constant text = #string "mpl.txt";

string get_name() {
  return name;
}

string get_text() {
  return text;
}

string get_xml() {
  return "<pre>\n" + get_text() + "</pre>\n";
}
