/* sysstuff.c
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#include "log.h"
#include <stdlib.h>
#include <errno.h>
#include <gtk/gtk.h>

#include <stdio.h>      // FILENAME_MAX
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#define  __USE_GNU 1
#include <unistd.h>     // group_member, chdir

// ----------------------------------------------------------------------------
gboolean sys_spawn(gchar *cmdline, gchar *path)
{
  int          cc;
  gboolean     spawned;
  GError      *err = NULL;
  gchar       *oldpwd = NULL;
  
  g_assert(cmdline != NULL);

  // switch to desired PATH
  if ((path != NULL) && (strcmp(path, "") != 0))
  {
    cc = chdir(path);
    WARN("chdir:%d, errno:%d, path:%s", cc, errno, path);
    oldpwd = g_get_current_dir();
  }

  spawned = g_spawn_command_line_async(cmdline, &err/*GError*/);

  if (oldpwd != NULL)
  {
    cc = chdir(oldpwd);
    if (cc != 0)
      WARN("chdir:%d, errno:%d, oldpwd:%s", cc, errno, oldpwd);
    g_free(oldpwd);
  }

  if (!spawned)
  {
    if (err != NULL && err->message != NULL)
    {
      WARN("g_spawn_command_line_async, cmdline:%s, errcode:%d, msg:%s",
           cmdline, err->code, err->message);
      g_error_free(err);
    }
    else
    {
      WARN("g_spawn_command_line_async, cmdline:%s", cmdline);
    }
  }
  return spawned;
}
// ----------------------------------------------------------------------------
// remove all not necessary '/' in the path
void make_path_uniq(char **path)
{
  int i, len, dbl_slash_cnt, dbl_slash_cnt_old;
  char *new_path;
  len = strlen(*path);
  dbl_slash_cnt = 0;
  for (i=0; i<len; i++)
  {
    if ( (*path)[i] == '/' )
    {
      if ( (i != 0) && ((*path)[i-1] == '/') )
        dbl_slash_cnt++;
      if ( ( i != 0) && (i == (len-1)) )
        dbl_slash_cnt++;
    }
  }
  if (dbl_slash_cnt == 0)
    return;

  new_path = (char*) g_malloc((len - dbl_slash_cnt + 1) * sizeof(char));
  dbl_slash_cnt = 0;
  dbl_slash_cnt_old = 0;
  for (i=0; i<len; i++)
  {
    if ( (*path)[i] == '/' )
    {
      if ( (i != 0) && ((*path)[i-1] == '/') )
        dbl_slash_cnt++;
      if ( (i != 0) && (i == (len-1)) )
        dbl_slash_cnt++;
    }
    if (dbl_slash_cnt_old == dbl_slash_cnt)
      new_path[i - dbl_slash_cnt] = (*path)[i];
    dbl_slash_cnt_old = dbl_slash_cnt;
  }
  new_path[len - dbl_slash_cnt] = '\0';
  g_free(*path);
  *path = new_path;
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
