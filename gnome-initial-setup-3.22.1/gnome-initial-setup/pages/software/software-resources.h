#ifndef __RESOURCE_software_H__
#define __RESOURCE_software_H__

#include <gio/gio.h>

extern GResource *software_get_resource (void);
#endif
