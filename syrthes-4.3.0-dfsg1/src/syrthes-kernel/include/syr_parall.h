/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#ifndef _PARALL_H_
#define _PARALL_H_

#ifdef _SYRTHES_MPI_
#include "mpi.h"
#endif

/* ---------------------------------------------------------------- */
/* variables globales                                               */
/* ---------------------------------------------------------------- */
extern int syrglob_rang;
extern int syrglob_nparts;
extern int syrglob_npartsray;
#ifdef _SYRTHES_MPI_
extern   MPI_Comm syrglob_comm_world; /* communicateur propre a syrthes */
#endif


/* ---------------------------------------------------------------- */
/* parallelisation du solveur : structure propre a chaque partition */
/* ---------------------------------------------------------------- */
struct SDparall
{
  int nparts,rang;
  int nbno_interne;          /* nbre de noeuds internes                                 */
  int nbno_front;            /* nbre de noeuds frontiere                                */
  int nbno_global;           /* nbre de noeuds global (sur le maill entier)             */
  int *nfoisfront;           /* nbre part auxquelles appartient un noeud frontiere      */
  int *npglob;               /* numero global des noeuds                                */

  int *nbcommun;             /* [npart] nombre de noeuds communs avec les autres part   */
  int *adrcommun;            /* numero de depart des noeuds de la part i dans tcommun   */
  int *tcommun;              /* numeros de noeuds communs                               */

  int *nbcommunrc;           /* [npart] nombre de paires RC avec les autres part      */
  int *adrcommunrc;          /* numero de depart des paires de la part i dans tcommunrc */
  int *tcommunrc;            /* numeros des noeuds des paires (i,i+1)                 */
  int *nfoisrc;              /* nombre de contributions recues par le noeud           */

  int *nbcommunperio;        /* [npart] nombre de paires perio avec les autres part      */
  int *adrcommunperio;       /* numero de depart des paires de la part i dans tcommunperio */
  int *tcommunperio;         /* numeros des noeuds des paires (i,i+1)                 */
  int *nfoisperio;           /* nombre de contributions recues par le noeud           */
#ifdef _SYRTHES_MPI_
  MPI_Request *request;              /* label pour les envois non bloquants              */
  MPI_Comm syrthes_comm_world; /* communicateur propre a syrthes */
#endif
  double *trav1,*trav2;      /* tab de travail 0[nbno_front]                     */
} ;


/* ----------------------------------------------------------------------------------- */
/* pour le couplage conduction/rayonnement                                             */
/* ----------------------------------------------------------------------------------- */
struct SDparall_ray
{
  int nparts,rang;       /* nparts=nbre de proc pour la conduction     */
  int npartsray;         /* npartsray=nbre de proc pour le rayonnement */
  int rangray;           /* rang dans le communic propre au rayt 
			    (=-1 si le proc ne fait pas de ray)  */
  int *nelrayloc;      /* nelrayloc[npartsray] nombre de facettes par proc */
  int nelrayloc_max;   /* max du nbre de facettes par proc (le dernier proc en a plus) */
  int nelraytot;       /* nbre d'elt total de rayonnement (avant parallelisme) */
  int *ieledeb;        /* ieledeb[npartsray+1 ] : indice de la 1ere facette sur chaque proc */
                       /* et donc ieledeb[npartsray+1]=derniere facette */
  int *frglob;         /* numero global de la facette de rayonnement */
  int *fdfproc;        /* fdfproc[npartsray]=0 ou 1 si le proc courant a des fdf en commun */
                       /* avec le proc i                                                   */

  int *rcoups_proc;      /* numero du proc ou se trouve le corresp                           */
  int *nbval_a_envoyer;  /* nbval_a_envoyer[nbprocray]                                       */
                         /* nombre de valeurs a envoyer a chaque proc conduction vers le ray */
  int *nbval_a_recevoir; /* nbval_a_recevoir[nbproccond]                                     */
                         /* nombre de valeurs a recevoir de chaque proc conduction           */
  int *indice_a_recevoir;/* indice_a_recevoir[nbproccond]                                    */
                         /* indice de depart dans le tableau rcoups des facettes couplees    */
                         /* avec le proc i                                                   */
  int **num_a_envoyer;   /* num_a_envoyer[nbprocray][nbval_a_envoyer]                        */
                         /* numero des facettes solides couplees pour lesquelles il faut     */
                         /* envoyer la temp solide                                           */
  double ***bary_a_envoyer;  /* num_a_envoyer[nbprocray][ndim][nbval_a_envoyer]              */
#ifdef _SYRTHES_MPI_
  MPI_Comm syrthes_comm_world; /* communicateur propre a syrthes */
  MPI_Comm syrthes_comm_ray;   /* communicateur propre au rayonnement dans syrthes */
#endif
                         /* coeff bary pour le calcul de la temp solide a envoyer            */
} ;


/* -------------------------------------------------------------------------------------- */
/* parallelisation des resistances de contact : structure connue par le proc 0 uniquement */
/* -------------------------------------------------------------------------------------- */
struct SDparall_rc
{
  int nparts;        /* nbre de partitions                                             */
  int rang;          /* rang du process                                                */
  int *nbress;             /* nbre de RC dans chaque partition                            */

  int **corresp_part;      /* numero de la part dans lequel se trouve le correspondant    */
                           /*  corresp_part[nparts][nbress(nparts)]                       */

  int **corresp_num;       /* numero local du correspondant                               */
                           /*  corresp_num[nparts][nbress(nparts)]                        */

  double **trecu;          /*  trecu[nparts][nbress(nparts)] temperature recues des proc  */
  double **tenface;        /*  tenface[nparts][nbress(nparts)] temp du noeud en face      */
} ;



#endif


