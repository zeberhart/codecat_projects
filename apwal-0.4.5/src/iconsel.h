/* iconsel.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#ifndef ICONSEL__H
#define ICONSEL__H

#include "common.h"

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
typedef struct icon_dir_t
{
  gchar    *path;
  gboolean  selected;
  gboolean  recursive;
} icon_dir_t;

icon_dir_t *icon_dir_new(gchar *path, gboolean selected, gboolean recursive);
void        icon_dir_free(icon_dir_t *icon_dir);
gint        icon_dir_compare(icon_dir_t *a, icon_dir_t *b);
void        icon_dir_list_append(GList **list, icon_dir_t *dir);
icon_dir_t *icon_dir_list_find(GList *list, char *olddir);
void        icon_dir_list_remove(GList **list, char *olddir);
void        icon_dir_list_modify(GList **list, char *olddir, char *newdir);
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
typedef struct file_ext_t
{
  gchar    *extension;
  gboolean  selected;
} file_ext_t;

file_ext_t *file_ext_new(gchar *extension, gboolean selected);
void        file_ext_free(file_ext_t *file_ext);
gint        file_ext_compare(file_ext_t *a, file_ext_t *b);
void        file_ext_list_append(GList **list, file_ext_t *ext);
file_ext_t *file_ext_list_find(GList *list, char *oldext);
void        file_ext_list_remove(GList **list, char *oldext);
void        file_ext_list_modify(GList **list, char *oldext, char *newext);
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
typedef struct iconsel_pref_t
{
  GList      *icon_dirs;
  GList      *file_exts;
  gboolean    select_48;
  gboolean    select_lt48;
  gboolean    select_gt48;
  gint        sort_mode;
} iconsel_pref_t;
iconsel_pref_t * iconsel_pref_new(GList *icon_dirs, GList *file_exts,
                                  gboolean eq48, gboolean lt48, gboolean gt48,
                                  gint sort);
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
typedef struct icon_selection_t
{
  struct apwalapp_t     *apwal;
  struct icon_list_t    *icons;
  char          *filter;
  GtkWidget     *path_tree;
  GtkWidget     *exts_tree;
  GtkWidget     *image;
  GtkWidget     *filter_entry;
  GtkWidget     *filter_btn;
  gboolean      never_refreshed;

  GdkPixbuf    *transparent_pixbuf;
  GdkPixbuf    *pixbuf_open;
  
  GtkWidget    *selected_frame;
  GtkWidget    *selected_image;
  GtkWidget    *selected_file_label;
  GtkWidget    *selected_path_label;
  GtkWidget    *selected_res_label;
  GtkWidget    *selected_apply_btn;

  GTimer       *dblclick_selected_timer;
  GTimer       *dblclick_timer;
  gint          dblclick_last_x;
  gint          dblclick_last_y;

  gint          scroll_border_width;
} icon_selection_t;
// ----------------------------------------------------------------------------
icon_selection_t* icon_selection_new(struct apwalapp_t *apwal);
void     icon_selection_load_icons_with_splash(icon_selection_t *iconsel);
gboolean iconsel_select_func(ico_t *ico, void *data);
void     iconsel_refresh(icon_selection_t *iconsel);
void     iconsel_refresh_selected(icon_selection_t *iconsel);
void     iconsel_path_refresh(icon_selection_t *iconsel);
void     iconsel_set_filename(icon_selection_t *iconsel, const char *filename);
char*    iconsel_get_filename(icon_selection_t *iconsel);

#endif /*ICONSEL__H*/
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
