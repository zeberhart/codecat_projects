/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 1988-2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/
/* TYPE POUR CONVERSION DE MAILLAGE SYRTHES */

struct typ_maillage {

   FILE *fichier_ext; /* descripteur de fichier d'entree */
   FILE *fichier_syr; /* descripteur de fichier de syrthes */
   FILE *fichier_desc; /* descripteur de fichier de correspondance noms familles MED - references Syrthes */
   int version_syr; /* version du fichier syrthes a ecrire */

   int dimension; /* dimension de l'espace (1), 2 ou 3 */
   int dim_elem; /* dimension des elements 1, 2 ou 3 */
   int nbnoeuds; /* nombre de noeuds */
   int nbelem; /* nombre d'elements */
   int nbnoeuds_par_elem; /* nombre de noeuds par element */
   int nbelem_de_bord; /* nombre d'elements de bord pour version Syrthes 4.0 */

   int nbzones; /* nombre de zones */
   int nbcl; /* nombre de conditions limites */

   double *xcoord; /* coordonnee x */
   double *ycoord; /* coordonnee y */
   double *zcoord; /* coordonnee z */
   int *coul_noeud; /* couleur du noeud */
   
   int *coul_elem; /* couleur de l'element */
   int **liste_elem; /* connectivite nodale */
	
   int **liste_reffaces; /* stockage des references aux faces pour Syrthes 3.4 */
	
   int *coul_elembord; /* stockage elements de bord pour Syrthes 4.0 */
   int **liste_elembord; /* connectivite nodale */

   int numligne; /* numero de ligne du fichier en lecture (compteur) */

   int nbfam; /*Nombre de familles*/
   int *entites_familles; /*Association entre numero de famille et entite de la famille*/
   int *numeros_familles; /*Association entre indice de famille et numero de famille*/
   int mode_ecriture; /*0 : ASCII, 1 : binaire*/
};

struct Maillage
{
  int ndim,ndiele,nelem,npoin,ndmat,nbface;
  int ncoema,iaxisy;
  double **coord,*volume,**xnf;
  int **node;
  int *nref,*nrefe,*type;
  int **nvoisin; /* seulement pour les mst */
  int nbarete,**arete, **eltarete;   /* seulement pour maillnodes */
};


struct MaillageBord
{
  int ndim,ndiele,nelem,ndmat;
  double *volume;
  int **node;
  int *nrefe,***type;  /* type[nbvar][nelem][MAXPOS] */
};
