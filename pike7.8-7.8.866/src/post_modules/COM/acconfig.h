/*
 * $Id: 5f13d79a920f22fc4c9aa11d216c652bd46062b2 $
 *
 * Config-file for the COM module.
 *
 * Tomas Nilsson
 */

#ifndef PIKE_COM_CONFIG_H
#define PIKE_COM_CONFIG_H

@TOP@
@BOTTOM@

/* Define if you have COM */
#undef HAVE_COM

/* Define to home of Java */
#undef COM_HOME

/* Define to the library path for COM libraries */
#undef COM_LIBPATH

#endif /* PIKE_COM_CONFIG_H */
