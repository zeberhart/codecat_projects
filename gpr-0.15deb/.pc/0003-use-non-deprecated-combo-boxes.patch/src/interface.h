/*
 * GPR
 * Copyright (C) 2000 CompuMetric Labs, Inc.
 *
 * For more information contact:
 *        Thomas Hubbell
 *        CompuMetric Labs, Inc.
 *        5920 Grelot Road, Suite C-2
 *        Mobile, AL 36609
 * 
 * Voice: (334) 342-2220
 *   Fax: (334) 343-2261
 * Email: thubbell@compumetric.com
 *   Web: http://www.compumetric.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *    
 *        File: Interface.h
 * 
 * Description: Routines to create the user interface
 */

#include <gtk/gtk.h>
#include <ppd.h>

#define PRINTCAP "/etc/printcap"
#define PF "PPD file"
#define SS "saved settings"
#define PN "printer name"
#define SD "spool directory"

/*
<<<<<<< interface.h
 * Used to determine spooler type
 */
enum {
  SPOOLER_LPR = 0,
  SPOOLER_LPRNG,
  SPOOLER_CUPS,
  SPOOLER_PPR,
  SPOOLER_PDQ,
  SPOOLER_FOOMATIC_DIRECT,
  SPOOLER_UNKNOWN,
  SPOOLER_LAST
};

/*
 * This is a "kitchen sink" structure to allow the passing of various,
 * related bits of data to the 'gtk_signal_connect' functions, which
 * can only take one data argument. 
 */
typedef struct {
  char *ppd_file;		/* file name of the ppd (plus the path) */
  char *default_ppd_file;	/* file name of the system default ppd (plus
				   the path) */
  PpdFile *ppd_handle;		/* ppd file structure */
  gchar *lpr_opts;		/* options to be passed directly to lpr */
  gchar *printer_name;		/* printer name */
  gchar *spool_dir;		/* spool directory */
  gchar *file_to_print;		/* the name of the file to be printed */
  gchar *config_dir;		/* current directory holding user config info
				   for a printer */
  gchar *setting_name;		/* the name of the saved setting */
  GtkWidget *pagesize_optionmenu;	/* the pagesize optionmenu */
  GtkWidget *ppd_fileentry;	/* The ppd file entry box */
  GtkWidget *opt_window;	/* may not need this here */
  GtkWidget *opt_vbox;		/* printer-specific options container */
  GtkWidget *opt_viewport;	/* printer-specific options viewport */
  GtkWidget *inst_opt_window;	/* may not need this here */
  GtkWidget *inst_opt_vbox;	/* installable options container */
  GtkWidget *inst_opt_viewport;	/* installable options viewport */
  GSList *choice_list;		/* linked list of job-specific choices */
  GSList *inst_opt_list;	/* linked list of installable options */
  GtkWidget *savesett_optionmenu;	/* saved settings option menu */
  GtkWidget *savesett_optionmenu_menu;	/* saved settings optionmenu menu */
  GtkWidget *savesett_textentry;	/* text box for entering settings name */
  GHashTable *constr_hash;	/* hash table of constraints */
  GHashTable *cr_to_hr;		/* hash table to translate computer readable to 
				   human readable */
  char *num_copies;		/* Non-PPD: number of copies */
  char *n_up;			/* Non-PPD: n-up option */
  char *page_range;		/* Non-PPD: the range of pages to print */
  char *range_from;		/* Non-PPD: the first page in the range */
  char *range_to;		/* Non-PPD: the last page in the range */
  char *page_set;		/* Non-PPD: the page set (odd,even,all) to
				   print */
  char *collate;		/* Non-PPD: the collation choice */
  char *output_order;		/* Non-PPD: the output order choice */
  char *gamma;			/* may not use this */
  char *brightness;		/* may not use this */
  gint8 debug_flag;		/* Flag to trigger debug mode */
  gint8 norun_flag;		/* Flag to trigger norun mode */
  gint8 ppdfilt;		/* Mode of ppdfilt usage */
  gint8 spooler;		/* the spooler type 0=lpr, 1=LPRng */
} ppd_struct;


GtkWidget *create_gpr_main_window(ppd_struct * ppd,
				  GtkWidget * inst_opts_window);
GtkWidget *create_gpr_inst_options_window(ppd_struct * ppd);
GtkWidget *create_constraint_messagebox(gchar * message);

int get_spooler_type_debian(void);
int get_spooler_type_obsolete(void);
/* int get_spooler_type(void); */
void error_box(gchar * string);

extern GtkWidget *border_toggle;
extern GtkWidget *header_toggle;
extern GtkWidget *notebook1;
extern GtkWidget *nup_1up_radiobutton;
extern GtkWidget *nup_2up_radiobutton;
extern GtkWidget *nup_4up_radiobutton;
extern GtkWidget *numcopies_spinbutton;
extern GtkWidget *collate_yes_radiobutton;
extern GtkWidget *collate_no_radiobutton;
extern   GtkWidget *revordr_yes_radiobutton;
extern   GtkWidget *revordr_no_radiobutton;
