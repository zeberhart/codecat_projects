/*
 *  Copyright 2008 Michael Pfeuti <m_pfeuti@students.unibe.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#include "linelight.h"

#include <libxfcegui4/libxfcegui4.h>
#include <libxfce4panel/libxfce4panel.h>

#include <gio/gio.h>

#include <string.h>
#include <stdlib.h>
#include <gdk/gdk.h>


#include "defines.h"

int linelight_is_result_in_section(LineLightData *data, Section sec,char* result);
void linelight_add_list_entry(LineLightData *data, Section sec, char *entry);

LineLightData* linelight_new(GtkWidget *window, GtkListStore *list)
{
    LineLightData *data = malloc(sizeof(LineLightData));
    data->max_search_results = 5;
    data->text_window = window;
    data->list= list;
    linelight_clear_list(data);
    data->x = gdk_screen_width()/2;
    data->y = gdk_screen_height()/2;
    data->grow_corner=TOP_LEFT_CORNER;
    data->decorated = TRUE;
    data->min_char_for_search = 2;

    int i = 0;
    for (; i< SECTION_COUNT;i++)
    {
        data->section_enabled[i] = TRUE;
        data->listSectionPaths[i] = g_ptr_array_new();
    }

    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".wav");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".midi");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".aif");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".aiff");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".aifc");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".ief");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".snd");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".au");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".pcm");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".aud");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".voc");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".svx");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".smp");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".flac");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".pac");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".ogg");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".fla");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".ggf");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".ape");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".mac");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".ofr");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".ofs");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".osq");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".mka");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".ra");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".shn");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".tta");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".wv");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".wma");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".vox");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".dts");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".ac3");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".mp1");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".mp2");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".mp3");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".aac");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".mpc");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".mpp");
    g_ptr_array_add(data->listSectionPaths[MUSIC_SECTION],".mp+");


    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".mpg");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".wmv");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".mpeg");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".3gp");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".mp4");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".mkv");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION], ".avi");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".divx");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".xvid");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".asf");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".flv");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".qt");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".vob");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".mov");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".rm");
    g_ptr_array_add(data->listSectionPaths[VIDEO_SECTION],".nuv");


    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".jpeg");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".jpg");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".tiff");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".tif");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".emf");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".raw");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".png");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".gif");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".bmp");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".ppm");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".pgm");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".pbm");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".pnm");
    g_ptr_array_add(data->listSectionPaths[PICTURES_SECTION], ".svg");

    //data->listSectionPaths[WEB_SECTION], NULL;

    g_ptr_array_add(data->listSectionPaths[BIN_SECTION], ".desktop");

    char *home = malloc(sizeof(char) * MAX_PATH_LENGTH);
    strcpy(home, xfce_get_homedir());
    g_ptr_array_add(data->listSectionPaths[FILES_SECTION],home);
    g_ptr_array_add(data->listSectionPaths[FILES_SECTION], "/mnt");
    g_ptr_array_add(data->listSectionPaths[FILES_SECTION], "/media");

    return data;
}

void linelight_free(LineLightData *data)
{
    int i;
    for (i=0;i<SECTION_COUNT;i++)
    {
        g_ptr_array_free(data->listSectionPaths[i],TRUE);
    }
    free(data);
}

void remove_newline(char* string)
{
    if (strlen(string) != 0 && string[strlen(string)-1] == '\n')
        string[strlen(string)-1] = '\0' ;
}

static GdkPixbuf* load_icon(char* path)
{
    if (path == NULL || strlen(path) == 0)
        return NULL;

    GdkPixbuf *icon = NULL;
    GtkIconTheme *icon_theme = gtk_icon_theme_get_default();
    if ((icon = gtk_icon_theme_load_icon (icon_theme, path, ICON_SIZE, GTK_ICON_LOOKUP_USE_BUILTIN,NULL)) != NULL)
    {
        return icon;
    }

    if (icon == NULL)
    {
        char *basename = g_path_get_basename (path);
        char *extension = g_utf8_strrchr (basename, -1, '.');

        if (extension != NULL && basename != NULL)
        {
            char new_icon_name[sizeof(char)*(g_utf8_strlen (basename, -1) - g_utf8_strlen (extension, -1))];
            g_utf8_strncpy (new_icon_name, basename,
                            g_utf8_strlen (basename, -1) - g_utf8_strlen (extension, -1));

            icon =
                gtk_icon_theme_load_icon (icon_theme, new_icon_name, ICON_SIZE,
                                          GTK_ICON_LOOKUP_USE_BUILTIN, NULL);
            g_free (basename);
        }

        if (icon == NULL)
        {
            char *new_item_name = g_utf8_strdown (path, -1);
            icon =
                gtk_icon_theme_load_icon (icon_theme, new_item_name, ICON_SIZE,
                                          GTK_ICON_LOOKUP_USE_BUILTIN, NULL);
            g_free (new_item_name);
        }
    }
    return icon;

}

void linelight_add_list_entry(LineLightData *data, Section sec, char* entry)
{
    if (entry==NULL || !g_utf8_validate(entry,-1,NULL) || !data->section_enabled[sec])
        return;

    remove_newline(entry);

    GtkTreeIter iter;
    char *icon;
    GdkPixbuf *pixbuf;

    gtk_list_store_insert(data->list, &iter, data->listPointer[sec]);

    GtkIconTheme *icon_theme = gtk_icon_theme_get_default();
    GFile *file = g_file_new_for_path(entry);

    //icon
    if (g_file_query_exists (file, NULL) && sec == BIN_SECTION )
    {
        XfceDesktopEntry* app = xfce_desktop_entry_new(entry,categories,3);

        if (app != NULL && xfce_desktop_entry_get_string(app, "Icon", TRUE, &icon)  && NULL != (pixbuf= load_icon(icon)))
        {
            gtk_list_store_set(data->list, &iter, ICON_COL, pixbuf, -1);
            g_object_unref(app);
            free(icon);
        }
    }
    else if (file != NULL)
    {
        GFileInfo *info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE, G_FILE_QUERY_INFO_NONE, NULL, NULL);
        if (info != NULL)
        {
            GIcon *mime_icon = g_content_type_get_icon (g_file_info_get_content_type (info));
            if (mime_icon != NULL)
            {
                GtkIconInfo *icon_info = gtk_icon_theme_lookup_by_gicon (icon_theme, mime_icon, ICON_SIZE, GTK_ICON_LOOKUP_USE_BUILTIN);
                if (icon_info != NULL)
                {
                    pixbuf = gtk_icon_info_load_icon (icon_info, NULL);
                    gtk_icon_info_free (icon_info);
                    gtk_list_store_set(data->list, &iter, ICON_COL, pixbuf, -1);
                }
                g_object_unref (mime_icon);
            }
        }
    }
    g_object_unref(file);

    //text
    gtk_list_store_set(data->list, &iter, TEXT_COL, entry, -1);

    int i = sec;
    for (;i <SECTION_COUNT; i++)
    {
        data->listPointer[i]++;
    }
}

int linelight_is_max_reached(LineLightData *data, Section sec)
{
    return (data->search_result_count[sec] >= data->max_search_results);
}

/*
 *  Public Methods
 */

int linelight_get_result_section(LineLightData *data, char* result)
{
    if (result == NULL)
        return -1;

    remove_newline(result);

    GFile *file = g_file_new_for_path (result);
    GFileInfo *info = g_file_query_info (file, "standard::*", G_FILE_QUERY_INFO_NONE, NULL, NULL);
    g_object_unref(file);

    if (info == NULL)
    {
        return -1;
    }

    GFileType type = g_file_info_get_file_type (info);

    if ((type != G_FILE_TYPE_REGULAR && type != G_FILE_TYPE_DIRECTORY) || g_file_info_get_is_hidden (info))
    {
        g_object_unref (info);
        return -1;
    }

    if (type == G_FILE_TYPE_DIRECTORY)
    {
        g_object_unref (info);
        return FOLDER_SECTION;
    }


    int i,j;
    char *ending, *path_lower, *path_ending, *path_begining;
    path_lower = g_utf8_strdown(result,-1);
    for (j = 0; j< SECTION_COUNT; j++)
    {
        for (i = 0; i < data->listSectionPaths[j]->len  && type != G_FILE_TYPE_DIRECTORY; i++)
        {
            ending = g_ptr_array_index(data->listSectionPaths[j], i);
            path_ending = strrchr(path_lower,'.');
            path_begining = strstr(path_lower, ending);
            if ( (path_ending != NULL && strcmp(path_ending, ending) == 0) ||
                 (path_begining != NULL && j == FILES_SECTION && strcmp(path_begining, path_lower) == 0))
            {
                g_object_unref (info);
                free(path_lower);
                return j;
            }
        }
    }

    free(path_lower);
    g_object_unref (info);

    return -1;
}

void linelight_add_search_result(LineLightData *data, char* result)
{
    int section = linelight_get_result_section(data, result);
    if (section != -1)
    {
        data->all_found_results[section]++;
        if (!linelight_is_max_reached(data,section))
        {
            data->search_result_count[section]++;
            linelight_add_list_entry(data,section,result);
        }
    }
}

void linelight_update_coordinates(LineLightData *data)
{
    int x,y,dx,dy;
    gtk_window_get_position(GTK_WINDOW(data->text_window), &x, &y);
    gtk_window_get_size(GTK_WINDOW(data->text_window), &dx, &dy);
    switch (data->grow_corner)
    {
    case TOP_RIGHT_CORNER:
        data->x = x + dx;
        data->y = y;
        break;
    case BOTTOM_LEFT_CORNER:
        data->x = x;
        data->y = y + dy;
        break;
    case BOTTOM_RIGHT_CORNER:
        data->x = x + dx;
        data->y = y + dy;
        break;
    case TOP_LEFT_CORNER:
        data->x = x;
        data->y = y;
        break;
    }
}

void linelight_clear_list(LineLightData *data)
{
    gtk_list_store_clear(data->list);

    int i, last_enabled_section = 0;
    for (i=0; i < SECTION_COUNT; i++)
    {
        data->listPointer[i] = 0;
        data->search_result_count[i] = 0;
        data->all_found_results[i] = 0;
    }

    for (i = 0; i<SECTION_COUNT; i++)
    {
        if (data->section_enabled[i])
        {
            last_enabled_section = i;
            linelight_add_list_entry(data,i, sectionNames[i]);
            linelight_add_list_entry(data,i, " ");
            data->listPointer[i]--;
        }

    }
    data->listPointer[last_enabled_section]++;
    //last_enabled_section; must be solved this way because add won't add if section is not enabled
    linelight_add_list_entry(data,last_enabled_section, "more...");
    data->listPointer[last_enabled_section] = data->listPointer[last_enabled_section] - 2;
}

void linelight_toggle_show_hide(LineLightData *data)
{
    if (GTK_WIDGET_VISIBLE(data->text_window))
    {
        linelight_update_coordinates(data);
        gtk_widget_hide(data->text_window);
        gtk_entry_set_text(GTK_ENTRY(data->text_field),"");
        linelight_clear_list(data);
    }
    else
    {
        gtk_window_move(GTK_WINDOW(data->text_window), data->x, data->y);
        gtk_widget_show_all(data->text_window);
    }
}
