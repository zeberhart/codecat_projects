#ifndef lint
static const char sccsid[] = "@(#)utils.c 1.5 02/03/11 falk" ;
static const char rcsid[] = "$Id: utils.c,v 1.5 2003/12/22 06:03:14 efalk Exp $" ;
#endif


#include <stdio.h>
#include <stdlib.h>	/* defines getenv(3) */
#include <string.h>
#include <ctype.h>
#include <errno.h>
#ifdef __STDC__
#include <stdarg.h>
#else
#include <varargs.h>
#endif
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <pwd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>		/* defines MAXPATHLEN */

#include "sortmail.h"
#include "utils.h"


extern	int	errno ;

static	char	*vrealloc(char **, char *, int *) ;
static	void	timestamp() ;

#ifndef	min
#define	min(a,b)	((a)<(b)?(a):(b))
#endif




	/* Expand an input string:
	 *
	 *	'...'	=>	'...'	(single quotes override expansion)
	 *	\x	=>	\x	(backslash overrides expansion)
	 *	"..."	=>	"..."
	 *	$$x	=>	$x
	 *	$x	=>	expand variable x
	 *
	 * Keep flag indicates whether or not quote marks are kept in
	 * the output.  Quotes are kept when the initfile is read, and
	 * discarded when the quotes string is used in the 2nd pass.
	 */

#define	makeroom(n)	do { if( (olen += (n)) >= dlen ) \
				optr = vrealloc(&dest,optr,&dlen) ; } while(0)

#define	addc(c)		do { makeroom(1) ; *optr++ = (c) ; } while(0)



	/* This function accepts an input string, expands embedded variables
	 * and returns the resulting input string.  Free with free().
	 */

char *
variable_expand(
	char	*src,		/* string to expand */
	bool	keep)		/* keep quote marks */
{
	char		*dest ;
	char		*ptr2 ;		/* temporary end-of-name pointer */
	char		*env ;		/* env variable value */
register char		c ;
register char		*iptr = src ;
register char		*optr ;
register int		olen = 0 ;
	int		dlen = 64 ;
	int		i ;

	optr = dest = malloc(dlen) ;

	/* The +folder, ~/, and ~user/ forms are only valid
	 * at the start of the string.
	 */

	for(c=*iptr++;  c != '\0'; )
	{
	  if( c == '\\' ) {
	    if( keep)
	      addc(c) ;

	    if( (c = *iptr++) != '\0' ) {
	      addc(c) ;
	      c = *iptr++ ;
	    }
	  }

	  else if( c == '\'' ) {
	    if( keep )
	      addc(c) ;
	    if( (ptr2 = strchr(iptr, '\'')) != NULL ) {
	      i = ptr2 - iptr ;
	      makeroom(i) ; memcpy(optr, iptr, i) ; optr += i ;
	      iptr = ptr2+1 ;
	      if( keep )
		addc(c) ;
	    }
	    c = *iptr++ ;
	  }

	  else if( c == '"' ) {
	    if( keep )
	      addc(c) ;
	    c = *iptr++ ;
	  }

	  else if( c == '$' ) {
	    if( *iptr == '$' ) {		/* "$$" => "$" */
	      ++iptr ;
	      addc('$') ;
	    }
	    else if( *iptr == '{' ) {
	      ++iptr ;
	      if( (ptr2 = strchr(iptr,'}')) != NULL ) {
		*ptr2 = '\0' ;
		if( (env=getenv(iptr)) == NULL )
		  logFile( "$%s undefined\n", iptr) ;
		else {
		  i = strlen(env) ;
		  makeroom(i) ; memcpy(optr, env, i) ; optr += i ;
		}
		*ptr2 = '}' ;
		iptr = ptr2+1 ;
	      }
	    }
	    else if( isalnum(*iptr) || *iptr == '_' ) {
	      for(ptr2=iptr; isalnum(*ptr2) || *ptr2 == '_'; ++ptr2) ;
	      c = *ptr2 ; *ptr2 = '\0' ;
	      if( (env=getenv(iptr)) == NULL )
		logFile( "$%s undefined\n", src) ;
	      else {
		i = strlen(env) ;
		makeroom(i) ; memcpy(optr, env, i) ; optr += i ;
	      }
	      *ptr2 = c ;
	      iptr = ptr2 ;
	    }
	    else
	      addc('$') ;

	    c = *iptr++ ;
	  }

	  else {
	    addc(c) ;
	    c = *iptr++ ;
	  }
	}

	*optr = '\0' ;

	return dest ;
}
#undef	makeroom
#undef	addc


/*---------------------------------------------------------------------------
 * Given a buffer (*dest) and a pointer within that buffer (optr), this
 * function doubles the size of the buffer and returns a corrected optr.
 */
static	char *
vrealloc( char **dest, char *optr, int *len)
{
	int	i = optr - *dest ;

	*dest = realloc(*dest, (*len *= 2)) ;
	return *dest + i;
}



/*---------------------------------------------------------------------------
 * This function expands the string as described above, and then expands it
 * further if it starts with +, ~/ or ~user/.
 */
void
filename_expand(
	char	dst[MAXPATHLEN],	/* result */
	char	*src)			/* string to expand */
{
	char		*namex ;	/* temporary end-of-name pointer */
	struct passwd	*passent ;
	char		*src2 ;
register char		*iptr ;
register char		*optr = dst ;
register int		olen = MAXPATHLEN ;
	int		i ;

	if( (src2 = variable_expand(src, False)) != NULL )
	  iptr = src2 ;
	else
	  iptr = src ;

	/* The +folder, ~/, and ~user/ forms are only valid
	 * at the start of the string.
	 */


	if( *iptr == '+' ) {
	  iptr++ ;
	  if( folder[0] != '/' ) {
	    i = strlen(home) ; i = min(i,olen) ;
	    memcpy(optr, home, i) ; optr += i ; olen -= i ;
	    if( optr[-1] != '/' )
	      { *optr++ = '/' ; --olen; }
	  }
	  i = strlen(folder) ; i = min(i,olen) ;
	  memcpy(optr, folder, i) ; optr += i ; olen -= i ;
	  if( optr[-1] != '/' )
	    { *optr++ = '/' ; --olen; }
	}

	else if( *iptr == '~' ) {
	  ++iptr ;
	  if( *iptr == '/' || *iptr == '\0' ) {
	    i = strlen(home) ; i = min(i,olen) ;
	    memcpy(optr, home, i) ; optr += i ; olen -= i ;
	  }
	  else {
	    namex = strchr(iptr, '/') ;
	    if( namex != NULL )
	      *namex = '\0' ;
	    passent = getpwnam(iptr) ;
	    iptr += strlen(iptr) ;
	    if( namex != NULL )
	      *namex = '/' ;
	    if( passent != NULL )
	    {
	      i = strlen(passent->pw_dir) ; i = min(i,olen) ;
	      memcpy(optr, passent->pw_dir, i) ; optr += i ; olen -= i ;
	    }
	  }
	  if( optr[-1] != '/' )
	    { *optr++ = '/' ; --olen; }
	  if( *iptr == '/' )
	    ++iptr ;
	}

	strncpy(optr, iptr, olen) ;

	if( src2 != NULL )
	  free(src2) ;
}



off_t
fileSize(char *name)
{
	struct stat statbuf ;

	if( stat(name, &statbuf) == -1 )
	  return -1 ;

	return statbuf.st_size ;
}




void
assfail( char *expr, char *file, int line)
{
	fprintf(logfile, "assertion failed: \"%s\", file %s, line %d\n",
	  expr, file, line) ;
}



	/* like printf(), but output to logfile */

#ifdef __STDC__
void
logFile(char *fmt, ...)
{
	va_list	args ;

	va_start(args, fmt) ;
	if( !terse )
	  timestamp() ;
	vfprintf(logfile, fmt, args);
	fflush(logfile) ;
	va_end(args) ;
}
#else
/*VARARGS0*/
void
logFile(va_alist)
	va_dcl
{
	va_list	args ;
	char	*fmt ;

	va_start(args) ;

	fmt = va_arg(args, char *);
	if( !terse )
	  timestamp() ;
	vfprintf(logfile, fmt, args);
	fflush(logfile) ;
	va_end(args) ;
}
#endif


static	void
timestamp()
{
	time_t t = time(NULL) ;
	struct tm *lt = localtime(&t) ;

	fprintf(logfile, "sortmail %d-%d-%d %d:%2.2d:%2.2d: ",
	  lt->tm_year+1900, lt->tm_mon+1, lt->tm_mday,
	  lt->tm_hour, lt->tm_min, lt->tm_sec) ;
}

#ifdef __STDC__
void
logFilev(int level, char *fmt, ...)
{
	va_list	args ;

	va_start(args, fmt) ;
	if( verbose >= level ) {
	  if( !terse )
	    timestamp() ;
	  vfprintf(logfile, fmt, args);
	  fflush(logfile) ;
	}
	va_end(args) ;
}
#else
/*VARARGS0*/
void
logFilev(va_alist)
	va_dcl
{
	va_list	args ;
	int	level ;
	char	*fmt ;

	va_start(args) ;

	level = va_arg(args, int);
	fmt = va_arg(args, char *);
	if( verbose >= level ) {
	  if( !terse )
	    timestamp() ;
	  vfprintf(logfile, fmt, args);
	  fflush(logfile) ;
	}
	va_end(args) ;
}
#endif



	/* functional equivalent to fopen(), but special-cases for "+"
	 * and "~" forms.
	 */

FILE *
pathOpen( char *path, char *mode )
{
	char	tmp[MAXPATHLEN] ;

	if( *path == '+' || *path == '~' ) {
	  filename_expand(tmp, path) ;
	  path = tmp ;
	}

	return fopen(path, mode) ;
}


	/* Create a temporary file, open r/w, set permissions to 400
	 * and return both name and file handle.
	 * TODO: set permissions to 000?
	 */

FILE *
tmpOpen( char path[MAXPATHLEN] )
{
	FILE	*rval ;
	sprintf(path, "%s/sortmail%d", tmpdir, getpid()) ;
	if( (rval = pathOpen(path, "w+")) != NULL )
	  (void) chmod(path, 0400) ;
	return rval ;
}


	/* Read a number string, convert to a number.  Recognize
	 * trailing 'k' or 'm'.
	 */

long
getLimit(char *ptr)
{
	long	rval ;

	rval = strtol(ptr, &ptr, 10) ;
	if( tolower(*ptr) == 'k' )
	  rval *= 1024 ;
	else if( tolower(*ptr) == 'm' )
	  rval *= 1024*1024 ;
	return rval ;
}


char *
firstNonBlank(register char *ptr)
{
	while( isspace(*ptr) ) ++ptr ;
	return ptr ;
}


char *
firstBlank(register char *ptr)
{
	while( *ptr != '\0' && !isspace(*ptr) ) ++ptr ;
	return ptr ;
}


void
removeNL(register char *ptr)
{
	register char *p2 ;

	if( (p2 = strchr(ptr,'\n')) != NULL ) *p2 = '\0' ;
	if( (p2 = strchr(ptr,'\r')) != NULL ) *p2 = '\0' ;
}


void
tolowerstr(register char *str)
{
	for(; *str != '\0'; ++str)
	  if( isupper(*str) )
	    *str = tolower(*str) ;
}



#ifdef __STDC__
int
die(int xval, char *fmt, ...)
{
	va_list	args ;

	va_start(args, fmt) ;
	if( !terse )
	  timestamp() ;
	vfprintf(logfile, fmt, args);
	va_end(args) ;
	exit(xval) ;
}
#else
/*VARARGS0*/
int
die(va_alist)
	va_dcl
{
	va_list	args ;
	int	xval ;
	char	*fmt ;

	va_start(args) ;
	if( !terse )
	  timestamp() ;
	xval = va_arg(args, int);
	fmt = va_arg(args, char *);
	vfprintf(logfile, fmt, args);
	va_end(args) ;
	exit(xval) ;
}
#endif




	/* Now define a few functions that are missing in some
	 * versions of unix.
	 */

#if	!STRDUP
char *
strdup(char *ptr)
{
	char	*rval ;

	rval = (char *) malloc(strlen(ptr)+1) ;
	strcpy(rval,ptr) ;
	return rval ;
}
#endif


#if	!STRERROR
char	*
strerror(int err)
{
	if( err >= 0 && err < sys_nerr )
	  return sys_errlist[err] ;
	else
	  return "unknown error" ;
}
#endif


#ifndef	__STDC__
#define	const
#endif

#if	!HSTERROR
char *
hstrerror(int err)
{
	switch(err) {
	  case NETDB_SUCCESS: return "No error" ;
	  case NETDB_INTERNAL: return strerror(errno) ;
	  case HOST_NOT_FOUND: return "No such host" ;
	  case TRY_AGAIN: return "Temporary error, try again" ;
	  case NO_RECOVERY: return "Unexpected server failure" ;
	  case NO_DATA: return "Data not found" ;
	}
}
#endif


#ifdef	NOSETENV
	/* Set an environment variable.  TODO: This function leaks memory */

void
setenv( char *name, char *value, int overwrite)
{
	char	*line ;
	int	l1,l2 ;

	l1 = strlen(name) ;
	l2 = strlen(value) ;
	line = malloc(l1+l2+2) ;
	memcpy(line, name, l1) ;
	line[l1] = '=' ;
	memcpy(line+l1+1, value, l2+1) ;
	putenv(line) ;
}


void
unsetenv(char *name)
{}
#endif


#ifdef	NOSTRCASECMP
int
strncasecmp( char *s1, char *s2, int len)
{
	register int	c1, c2 ;
	for(; (c1=*s1) != '\0' && (c2=*s2) != '\0' && --len >= 0; ++s1,++s2 )
	  if( (c1=toupper(c1)) != (c2=toupper(c2)) )
	    return c1 - c2 ;
	return len >= 0 ? *s1-*s2 : 0 ;
}
#endif


	/* convert a buffer to a base64 string.  Caller must make sure that
	 * the output buffer is at least 4/3 as long as input.
	 */

static	char	mimestr[64] =
	  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/" ;

void
makeBase64( unsigned char *inbuf, char *outbuf, int n)
{
	int	bits ;

	for(; n >= 3; n -= 3 ) {
	  bits = *inbuf++ << 16 ;
	  bits |= *inbuf++ << 8 ;
	  bits |= *inbuf++ ;
	  *outbuf++ = mimestr[bits>>18] ;
	  *outbuf++ = mimestr[(bits>>12)&0x3f] ;
	  *outbuf++ = mimestr[(bits>>6)&0x3f] ;
	  *outbuf++ = mimestr[bits&0x3f] ;
	}

	switch(n) {
	  case 1:
	    bits = *inbuf ;
	    *outbuf++ = mimestr[bits>>2] ;
	    *outbuf++ = mimestr[(bits<<4)&0x3f] ;
	    *outbuf++ = '=' ;
	    *outbuf++ = '=' ;
	    break ;
	  case 2:
	    bits = *inbuf++ << 8 ;
	    bits |= *inbuf ;
	    *outbuf++ = mimestr[bits>>10] ;
	    *outbuf++ = mimestr[(bits>>4)&0x3f] ;
	    *outbuf++ = mimestr[(bits<<2)&0x3f] ;
	    *outbuf++ = '=' ;
	    break ;
	}

	  *outbuf = '\0' ;
}




	/* convert base64 back to binary */

static	unsigned char strmime[128] = {
	  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	  0, 0, 62, 0, 0, 0, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61,
	  0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
	  13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 0, 0, 0,
	  0, 0, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
	  40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0, 0, 0, 0, 0} ;

extern	int
readBase64( char *inbuf, unsigned char *outbuf, int max)
{
	int	len = strlen(inbuf) ;
	int	bits, blen, olen ;

	if( max < (len+1)*3/4 )
	  return -1 ;

	bits=0; blen=0 ; olen=0 ;
	while( --len >= 0 && *inbuf != '=') {
	  bits = (bits<<6) | strmime[(int)*inbuf++] ;
	  blen += 6 ;
	  if( blen >= 8 ) {
	    *outbuf++ = (bits >> (blen-8)) & 0xff ;
	    blen -= 8 ;
	    ++olen ;
	  }
	}

	return olen ;

}
