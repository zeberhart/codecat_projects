/*
     This file is part of GNUnet.
     (C) 2010-2013 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/conversation/gnunet-conversation-gtk.c
 * @brief Main function of gnunet-conversation-gtk
 * @author yids
 * @author hark
 */
#include "gnunet-conversation-gtk_common.h"
#include "gnunet-conversation-gtk_history.h"
#include "gnunet-conversation-gtk_contacts.h"


struct GNUNET_CONVERSATION_Caller *caller_selected = NULL;
struct GNUNET_CONVERSATION_Call *call_selected = NULL;

/*************
 * common    *
 *************/
/**   
 *  * Handle to our main loop.
 *   */ 
static struct GNUNET_GTK_MainLoop *ml;

/**
 * Get our configuration.
 *
 * @return configuration handle
 */
const struct GNUNET_CONFIGURATION_Handle *
GIG_get_configuration ();

/**
 * Our configurations.
 */
static struct GNUNET_CONFIGURATION_Handle *cfg;

/**
 * Name of our ego.
 */
static char *ego_name;

/**
 * Be verbose.
 */
static int verbose = 1;


/**
 * Should gnunet-identity-gtk start in tray mode?
 */
static int tray_only;


/*************
 * phone     *
 *************/

/**
  * List of active calls
  */
static GtkListStore *active_liststore;
 
/**
 * List of active calls
 */
static GtkTreeView *active_treeview;

/**
 * List of incoming calls
 */
struct CallList
{

  /**
   * A DLL.
   */
  struct CallList *prev;

  /**
   * A DLL.
   */
  struct CallList *next;

  /**
   * Handle to hang up or activate.
   */
  struct GNUNET_CONVERSATION_Caller *caller;

  /**
   * Handle to call currently selected in list
   */
  struct GNUNET_CONVERSATION_Caller *caller_selected;

  /**
   * String identifying the caller.
   */
  char *caller_id;

  /**
   * Unique number of the caller.
   */
  unsigned int caller_num;

};
  /**
   * Unique number of call (outgoing)
   */
  unsigned int call_counter;


/**
 * Phone handle
 */
static struct GNUNET_CONVERSATION_Phone *phone;

/**
 * Call handle (for active outgoing call).
 */
static struct GNUNET_CONVERSATION_Call *call;

/**
 * Caller handle (for active incoming call).
 */
static struct CallList *cl_active;

/**
 * Head of calls waiting to be accepted.
 */
static struct CallList *cl_head;

/**
 * Tail of calls waiting to be accepted.
 */
static struct CallList *cl_tail;

/**
 * Desired phone line.
 */
static unsigned int line;

/**
 * debug box enabled
 */

static unsigned int debug_box_enabled;

/**
 * Our speaker.
 */
static struct GNUNET_SPEAKER_Handle *speaker;

/**
 * Our microphone.
 */
static struct GNUNET_MICROPHONE_Handle *mic;

/**
 * Handle to identity service.
 */
static struct GNUNET_IDENTITY_Handle *id;

/**
 * Name of conversation partner (if any).
 */
static char *peer_name;

/**
 * Our phone's current state.
 */
static enum PhoneState phone_state;

/**
 * Our call's current state.
 */
static enum CallState call_state;

/**
 * Counts the number of incoming calls we have had so far.
 */
static unsigned int caller_num_gen;

/**
 * GNS address for this phone.
 */
static char *address;

GtkWidget *b_contact, *b_accept, *b_hangup, *b_suspend, *b_resume;


/*******************
 * identity select *
 *******************/


/**
 * list of zones
 */
static GtkListStore *zone_liststore;

/**
 * zone tree model
 */
//static GtkTreeModel *zone_treemodel;

/**
 * Our ego.
 */
static struct GNUNET_IDENTITY_Ego *caller_id;


void GNUNET_CONVERSATION_GTK_on_active_calls_selection_changed ();

void set_status_icon (const char *icon_name);

/************
 * extern's *
 * **********/


/**
 * Get an object from the main window.
 *
 * @param name name of the object
 * @return NULL on error
 */
extern GObject *
GNUNET_CONVERSATION_GTK_get_main_window_object (const char *name)
{
      return GNUNET_GTK_main_loop_get_object (ml, name);
}


/**
 * log a message to gtk log textbuffer
 * @param Message to be logged
 */

extern void
GNUNET_CONVERSATION_GTK_log_message (const char *message) 
{
  //
  // log
  //
  GtkTextBuffer *logbuff;
  GtkTextView *log_view;
  GtkTextIter iter;
  gchar *fmsg;

  log_view = GTK_TEXT_VIEW (get_object ("GNUNET_GTK_conversation_log"));

  logbuff = GTK_TEXT_BUFFER (gtk_text_view_get_buffer (log_view));

  fmsg = g_strdup_printf (" %s \n", message);

  gtk_text_buffer_get_start_iter (logbuff, &iter);

  gtk_text_buffer_insert (logbuff, &iter, fmsg, -1);
  g_free (fmsg);


}

/**
 * update status
 *
 * @param message Message to put in statusbar
 */

static void
update_status (const gchar * message)
{

  GtkStatusbar *status_bar;
  guint status_bar_context;

  gchar *buff;

  status_bar = GTK_STATUSBAR (get_object ("GNUNET_GTK_conversation_statusbar"));
  status_bar_context = gtk_statusbar_get_context_id (status_bar, "blaat");


  buff = g_strdup_printf ("%s", message);

  gtk_statusbar_push (GTK_STATUSBAR (status_bar),
                      GPOINTER_TO_INT (status_bar_context), buff);
  g_free (buff);
  
  GNUNET_CONVERSATION_GTK_on_active_calls_selection_changed();


}


/**
 * update statusbar
 *
 * @param args arguments given to the command
 */
static void
do_status ()
{
  struct CallList *cl;

  switch (phone_state)
  {
  case PS_LOOKUP_EGO:
    UPDATE_STATUS (_
                   ("We are currently trying to locate the private key for the ego `%s'."),
                   ego_name);
    set_status_icon ("gnunet-conversation-gtk-tray-pending");
    break;
  case PS_LISTEN:
    UPDATE_STATUS (_
                   ("We are listening for incoming calls for ego `%s' on line %u."),
                   ego_name, line);
    set_status_icon ("gnunet-conversation-gtk-tray-available");

    break;
  case PS_ACCEPTED:
    UPDATE_STATUS (_("You are having a conversation with `%s'.\n"), peer_name);
    set_status_icon ("gnunet-conversation-call-active");

    break;
  case PS_ERROR:
    UPDATE_STATUS (_
                   ("We had an internal error setting up our phone line. You can still make calls."));
    set_status_icon ("gnunet-conversation-offline");

    break;
  }
  if (NULL != call)
  {
    switch (call_state)
    {
    case CS_RESOLVING:
      UPDATE_STATUS (_
                     ("We are trying to find the network address to call `%s'."),
                     peer_name);
      set_status_icon ("gnunet-conversation-gtk-tray-call-pending");

      break;
    case CS_RINGING:
      UPDATE_STATUS (_("We are calling `%s', his phone should be ringing."),
                     peer_name);
      set_status_icon ("gnunet-conversation-gtk-tray-call-ringing");

      break;
    case CS_CONNECTED:
      UPDATE_STATUS (_("You are having a conversation with `%s'."), peer_name);
      set_status_icon ("gnunet-conversation-gtk-tray-call-active");

      break;
    case CS_SUSPENDED:
      set_status_icon ("gnunet-conversation-gtk-tray-call-suspended");

      /* ok to accept incoming call right now */
      break;
    }
  }
  if ((NULL != cl_head) && ((cl_head != cl_active) || (cl_head != cl_tail)))
  {
    set_status_icon ("gnunet-conversation-gtk-tray-call-incoming");

    for (cl = cl_head; NULL != cl; cl = cl->next)
    {
      if (cl == cl_active)
        continue;
      //UPDATE_STATUS (_("#%u: `%s'"), cl->caller_num, cl->caller_id);
//      LOG ("%s", _("Calls waiting:"));
    }
  }
}



/*
 * @brief print info for currently selected call
 */
static void
print_call_info()
{
    GtkTreeIter gtkiter;
    gboolean valid;
    gint row_count = 0;

    valid = gtk_tree_model_get_iter_first( GTK_TREE_MODEL( active_liststore ), &gtkiter ); 
    
    if (!valid) 
        GNUNET_break(0);
    
    while (valid)
    {
      gchar *str_data;
      gint   int_data;
      gpointer cl_caller;
      gpointer cl_call;

      gtk_tree_model_get (GTK_TREE_MODEL(active_liststore), &gtkiter, 
                          AL_caller,     &cl_caller,
                          AL_caller_id,  &str_data,
                          AL_caller_num, &int_data,
                          AL_call,       &cl_call,
                          -1);
      if (caller_selected == cl_caller)
      {
       // LOG (_("info for active call:%s number: %u row: %u"), str_data,int_data,row_count);
        currentlySelectedCallAddress = str_data;
//	FPRINTF(stderr,"cal addr: %s\n",currentlySelectedCallAddress);
        break ;
      }
      if (call_selected == cl_call)
      {
        LOG (_("info for active outgoing call:%s number: %u row: %u"), str_data,int_data,row_count);
        break;
      }
    g_free (str_data);
    row_count++;
    valid = gtk_tree_model_iter_next (GTK_TREE_MODEL(active_liststore), &gtkiter);
    }



}



/*
 * @brief sets caller_selected, and enables or disables the active call list buttons
 */
static void
update_active_call_list_buttons()
{
   gchar *caller_id;
   gpointer cl_caller;
   gpointer cl_call;
   gint     cl_caller_state;
   gint     cl_type;
   //gint     cl_caller_type;
   GtkTreeSelection *active_selection;
   GtkTreeIter gcl_selected;
//   active_liststore_selection = get_object(ml,"GNUNET_CONVERSATION_GTK_active_calls_selection");

   // reset references to selected call/caller
   //caller_selected = NULL;
   //call_selected = NULL;
   LOG("reset caller selected");
   active_selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (active_treeview));


   if (gtk_tree_selection_get_selected(active_selection,NULL,&gcl_selected))
   {

       // get selected call 
       gtk_tree_model_get (GTK_TREE_MODEL(active_liststore), &gcl_selected, 
                            AL_caller, &cl_caller, // reference to incoming call
                            AL_caller_id, &caller_id,
                            AL_caller_state, &cl_caller_state,
                            AL_type, &cl_type,
                            AL_call, &cl_call, // reference to outgoing call
                            -1);

        // check if selected call is a incoming or outgoing call
        switch (cl_type)
        {
            case CALL_IN:
                call_selected = NULL;
                caller_selected = cl_caller;
                if (caller_selected == NULL){
                    GNUNET_break(0);
                }

            break;
            case CALL_OUT:
                caller_selected = NULL;
                call_selected = cl_call;
                LOG("outgoing selected");
                if (call_selected == NULL){
                    GNUNET_break(0);
                }
            break;
            default:
            GNUNET_break(0);
            break;
        }

        
        
        gtk_widget_show(GTK_WIDGET(get_object("GNUNET_GTK_conversation_active_call_list_buttons" )));

        LOG("caller state: %u phone_state: %u",cl_caller_state, phone_state);
        
        switch (cl_caller_state) 
        {
            /* buttons:
             *  contact
             *  accept
             *  hangup
             *  suspend
             *  resume
             *
             *  TODO: check if there is incoming or outgoing call, 
             *         disable resume and accept buttons.
             *         or suspend that other call
             */

            case CT_active:
            // hangup, pause
            //LOG("CT_active state: %u ",cl_caller_state);
            gtk_widget_set_sensitive(b_contact, 1);
            gtk_widget_set_sensitive(b_accept, 0);
            gtk_widget_set_sensitive(b_hangup, 1);
            gtk_widget_set_sensitive(b_suspend, 1);
            gtk_widget_set_sensitive(b_resume, 0);
            break;
            case CT_ringing:
            // pickup, phonebook
            //LOG("CT_ring show button");
            gtk_widget_set_sensitive(b_contact, 1);
            if (phone_state == PS_LISTEN)
            {
            gtk_widget_set_sensitive(b_accept, 1);
            }else{
            gtk_widget_set_sensitive(b_accept, 0);
            }

            
            gtk_widget_set_sensitive(b_hangup, 0);
            gtk_widget_set_sensitive(b_suspend, 0);
            gtk_widget_set_sensitive(b_resume, 0);
            break;
            case CT_rejected:
            //add to phonebook
            //LOG("CT_rejected ");
            gtk_widget_set_sensitive(b_contact, 1);
            gtk_widget_set_sensitive(b_accept, 0);
            gtk_widget_set_sensitive(b_hangup, 0);
            gtk_widget_set_sensitive(b_suspend, 0);
            gtk_widget_set_sensitive(b_resume, 0);

            break;
            case CT_suspended:
            // resume, hangup
            //LOG("CT_suspended ");
            gtk_widget_set_sensitive(b_contact, 1 );
            gtk_widget_set_sensitive(b_accept, 0);
            gtk_widget_set_sensitive(b_hangup, 1);
            gtk_widget_set_sensitive(b_suspend, 0);
            
            if (phone_state == PS_LISTEN)
            {
            LOG("enable resume button");
            gtk_widget_set_sensitive(b_resume, 1);
            }else{
            LOG("do not disable resume button (for test)");
            gtk_widget_set_sensitive(b_resume, 1);
            }
            break;
            case CT_other:
            //add to phonebook
            //LOG("CT_rejected ");
            gtk_widget_set_sensitive(b_contact, 1);
            gtk_widget_set_sensitive(b_accept, 1);
            gtk_widget_set_sensitive(b_hangup, 1);
            gtk_widget_set_sensitive(b_suspend, 1);
            gtk_widget_set_sensitive(b_resume,1);

            break;

            default:
            GNUNET_break(0);
            break;
        }        
       print_call_info();    
   }
   else
   {
    
    LOG("nothing selected");
    
    //gtk_widget_hide(GTK_WIDGET(get_object("GNUNET_GTK_conversation_active_call_list_buttons" )));
    
   }

} //end function

/*
 * @brief executed when selecting a different item in active call list
 */

void
GNUNET_CONVERSATION_GTK_on_active_calls_selection_changed()
{
    update_active_call_list_buttons();
}


void
disable_list_buttons()
{
    gtk_widget_set_sensitive(b_contact, 0);
    gtk_widget_set_sensitive(b_accept, 0);
    gtk_widget_set_sensitive(b_hangup, 0);
    gtk_widget_set_sensitive(b_suspend, 0);
    gtk_widget_set_sensitive(b_resume, 0);
}



/*
 * set_status_icon
 * 
 * available icons:
 * gnunet-conversation-gtk-tray-pending
 * gnunet-conversation-gtk-tray-available
 * gnunet-conversation-gtk-tray-offline
 * gnunet-conversation-gtk-tray-call-pending
 * gnunet-conversation-gtk-tray-call-ringing
 * gnunet-conversation-gtk-tray-call-active
 * gnunet-conversation-gtk-tray-call-suspended
 * gnunet-conversation-gtk-tray-call-incoming
 *
 */

void
set_status_icon (const char *icon_name)
{
  GtkImage *status_icon;

  status_icon = GTK_IMAGE (get_object ("GNUNET_GTK_status_icon"));

  gtk_image_set_from_icon_name (status_icon, icon_name, GTK_ICON_SIZE_BUTTON);

  //LOG (_("Tray icon changed to: `%s' "), icon_name);

  GNUNET_GTK_tray_icon_change (icon_name, "gnunet-conversation-gtk-status");
}


/*
 * set button text
 * @param button_name name of button
 * @param label label on the button
 */

void
set_button_text (const char *button_name, const char *label)
{
  //GtkButton *button;
  GtkWidget *button;

  button = GTK_WIDGET (get_object (button_name));
  gtk_widget_hide (button);
}

/*
 * disable button
 */

void
disable_button (const char *button_name)
{
  //GtkButton *button;
  GtkWidget *button;

  button = GTK_WIDGET (get_object (button_name));
  gtk_widget_hide (button);
}

/*
 * enable button
 */

void
enable_button (const char *button_name)
{
  //GtkButton *button;
  GtkWidget *button;

  button = GTK_WIDGET (get_object (button_name));
  gtk_widget_show (button);
}

/**
 * set state of outgoing call
 */
void
set_outgoing_call_state(struct GNUNET_CONVERSATION_Call *call, int state)
{
    LOG("set state to: %u", state);
  GtkTreeIter gtkiter;
  gint valid = 0;
  gint cl_type;
  //FPRINTF (stderr,"set incoming call state:%u caller: ",state);

//  LOG (_("set incoming call state:%u caller: "),state);
 
  valid = gtk_tree_model_get_iter_first( GTK_TREE_MODEL( active_liststore ), &gtkiter ); 
    
    if (!valid) 
        GNUNET_break(0);
    
    while (valid)
    {
      gchar *cl_caller_id;
      gint   cl_caller_num;
      gpointer cl_call;

      gtk_tree_model_get ( GTK_TREE_MODEL( active_liststore ), &gtkiter, 
                          AL_call, &cl_call,
                          AL_caller_id,&cl_caller_id,
                          AL_caller_num,&cl_caller_num,
                          AL_type, &cl_type,
                          -1);

      if (cl_type == CALL_OUT) {

          if (call == NULL) // function called by phone event handler
          {
            LOG("event handler");
            gtk_list_store_set(active_liststore, &gtkiter,
                                  AL_call_state, state,
                                  -1);
            switch (state)
            {
                    
          /**
           * We are the caller and are now ringing the other party (GNS lookup
           * succeeded).
           */
         case  GNUNET_CONVERSATION_EC_CALL_RINGING:

        break;

          /**
           * We are the caller and are now ready to talk as the callee picked up.
           */
          case GNUNET_CONVERSATION_EC_CALL_PICKED_UP:
            break;
          /**
           * We are the caller and failed to locate a phone record in GNS.
           * After this invocation, the respective call handle will be
           * automatically destroyed and the client must no longer call
           * #GNUNET_CONVERSATION_call_stop or any other function on the
           * call object.
           */
          case GNUNET_CONVERSATION_EC_CALL_GNS_FAIL:
           gtk_list_store_remove(active_liststore,&gtkiter);
            disable_list_buttons();
            break;

          /**
           * We are the caller and the callee called
           * #GNUNET_CONVERSATION_caller_hang_up.  After this invocation, the
           * respective call handle will be automatically destroyed and the
           * client must no longer call #GNUNET_CONVERSATION_call_stop.
           */
          case GNUNET_CONVERSATION_EC_CALL_HUNG_UP:
           gtk_list_store_remove(active_liststore,&gtkiter);
           disable_list_buttons(); 
            break;

          /**
           * We are the caller and the callee suspended the call.  Note that
           * both sides can independently suspend and resume calls; a call is
           * only "working" of both sides are active.
           */
          case GNUNET_CONVERSATION_EC_CALL_SUSPENDED:
                    break;

          /**
           * We are the caller and the callee suspended the call.  Note that
           * both sides can independently suspend and resume calls; a call is
           * only "working" of both sides are active.
           */
          case GNUNET_CONVERSATION_EC_CALL_RESUMED:
            break;

          /**
           * We had an error handing the call, and are now restarting it
           * (back to lookup).  This happens, for example, if the peer
           * is restarted during a call.
           */
          case GNUNET_CONVERSATION_EC_CALL_ERROR:
            break;

           default:
            break;
            }






          }
          else if (call == cl_call) // function called for specific call
          {
          //LOG (_("setting state for call:%u row: %u state: %u"),cl_caller_num,row_count,state);

              switch (state)
                {
                case CT_hangup:
                  //LOG("remove line cause hangup");
                  gtk_list_store_remove(active_liststore,&gtkiter);
                  disable_list_buttons();
                break;  

                case CT_rejected:
                  //LOG("remove line cause rejected");
                  gtk_list_store_remove(active_liststore,&gtkiter);
                  disable_list_buttons();
                break;  
                default:
                             
                   gtk_list_store_set(active_liststore, &gtkiter,
                                      AL_caller_state, state,
                                      -1);
                break;
                
                }//end switch 
            }//end call=cl_call 
         } //end cl_type
          g_free (cl_caller_id);
          valid = gtk_tree_model_iter_next (GTK_TREE_MODEL(active_liststore), &gtkiter);
       
    }//end while

    //update statsbar
    update_status("");



}

/**
 * set call state of a incoming call
 */
static void
set_incoming_call_state(struct GNUNET_CONVERSATION_Caller *caller, int state)
{
  GtkTreeIter gtkiter;
  gint valid = 0;
  //FPRINTF (stderr,"set incoming call state:%u caller: ",state);

//  LOG (_("set incoming call state:%u caller: "),state);
 
  valid = gtk_tree_model_get_iter_first( GTK_TREE_MODEL( active_liststore ), &gtkiter ); 
    
    if (!valid) 
        GNUNET_break(0);
    
    while (valid)
    {
      gchar *cl_caller_id;
      gint   cl_caller_num;
      gpointer cl_caller;

      gtk_tree_model_get ( GTK_TREE_MODEL( active_liststore ), &gtkiter, 
                          AL_caller, &cl_caller,
                          AL_caller_id,&cl_caller_id,
                          AL_caller_num,&cl_caller_num
                          ,-1);

      if (caller == cl_caller)
      {
      //LOG (_("setting state for call:%u row: %u state: %u"),cl_caller_num,row_count,state);

          switch (state)
            {
            case CT_hangup:
              //LOG("remove line cause hangup");
              gtk_list_store_remove(active_liststore,&gtkiter);
              disable_list_buttons();

            break;  

            case CT_rejected:
              //LOG("remove line cause rejected");
              gtk_list_store_remove(active_liststore,&gtkiter);
              disable_list_buttons();

            break;  
            default:
                         
               gtk_list_store_set(active_liststore, &gtkiter,
                                  AL_caller_state, state,
                                  -1);
            break;
            
            }//end switch 
        }//endif
       
          g_free (cl_caller_id);
          valid = gtk_tree_model_iter_next (GTK_TREE_MODEL(active_liststore), &gtkiter);
       
    }//end while

    //update statsbar
    update_status("");


}




/**
 * Function called with an event emitted by a phone.
 *
 * @param cls closure
 * @param code type of the event
 * @param caller handle for the caller
 * @param caller_id name of the caller in GNS
 */
static void
phone_event_handler (void *cls, enum GNUNET_CONVERSATION_PhoneEventCode code,
                     struct GNUNET_CONVERSATION_Caller *caller,
                     const char *caller_id)
{

    //gtk
    GtkTreeIter gtkiter;
    GtkTreeIter gtkiter1;
    gboolean valid;


  switch (code)
  {
  case GNUNET_CONVERSATION_EC_PHONE_RING:
    //increment call #
    caller_num_gen++; 
 

    LOG (_("A Incoming call from `%s' with number %u\n"), caller_id,
         caller_num_gen);
  
    //old
    struct CallList *cl;

    cl = GNUNET_new (struct CallList);
    cl->caller = caller;
    cl->caller_id = GNUNET_strdup (caller_id);
    cl->caller_num = caller_num_gen;
    GNUNET_CONTAINER_DLL_insert (cl_head, cl_tail, cl);
    //gtk 
    gtk_list_store_append (active_liststore, &gtkiter);

    gtk_list_store_set (active_liststore, &gtkiter, 
                        AL_caller_id, caller_id, 
                        AL_caller, caller,
                        AL_caller_num, caller_num_gen,
                        AL_caller_state, CT_ringing,
                        AL_type, CALL_IN 
                        ,-1);


    break;

  case GNUNET_CONVERSATION_EC_PHONE_HUNG_UP:
    //gtk
    
    valid = gtk_tree_model_get_iter_first( GTK_TREE_MODEL( active_liststore ), &gtkiter1 ); 
    
    if (!valid)
        GNUNET_break(0);
    
    while (valid)
    {
      //FPRINTF(stderr,"GNUNET_CONVERSATION_EC_PHONE_HUNG_UP: while valid");

      gchar *str_data;
      gint   int_data;
      gpointer cl_caller;
     
      gtk_tree_model_get (GTK_TREE_MODEL(active_liststore), &gtkiter1, 
                          AL_caller, &cl_caller,
                          AL_caller_id,&str_data,
                          AL_caller_num,&int_data,-1);
      if (caller == cl_caller)
      {

        LOG (_("phone hung up:%s number: %u "), str_data,int_data);
        set_incoming_call_state(caller,CT_rejected);
        break ;
      }
    g_free (str_data);
    valid = gtk_tree_model_iter_next (GTK_TREE_MODEL(active_liststore), &gtkiter1);
    }
    
    

    phone_state = PS_LISTEN;
    //add to call history list
    //history_add(CH_HANGUP, cl->caller_id);
    
    break;

  }
  do_status();
}


/**
 * Function called with an event emitted by a caller.
 *
 * @param cls closure with the `struct CallList` of the caller
 * @param code type of the event issued by the caller
 */
static void
caller_event_handler (void *cls, enum GNUNET_CONVERSATION_CallerEventCode code)
{

  if (cls == NULL){
      LOG("caller_event_handler: cls == NULL");
      GNUNET_break(0);
  } else {
      struct CallList *cl = cls;

      switch (code)
      {
      case GNUNET_CONVERSATION_EC_CALLER_SUSPEND:
        //TODO: should this be cls? not cl->caller 
        set_incoming_call_state(cl->caller,CT_suspended);
        LOG (_("Call from `%s' suspended by other user\n"), cl->caller_id);
        break;
      case GNUNET_CONVERSATION_EC_CALLER_RESUME:
        set_incoming_call_state(cl->caller,CT_active);
        LOG (_("Call from `%s' resumed by other user\n"), cl->caller_id);
        break;
      }
  }
  do_status();
}


/**
 * Start our phone.
 */
static void
start_phone ()
{
  struct GNUNET_GNSRECORD_Data rd;
  GtkLabel *label;
  if (NULL == caller_id)
  {
    LOG (_("Ego `%s' no longer available, phone is now down.\n"), ego_name);
    phone_state = PS_LOOKUP_EGO;
    return;
  }
  //GNUNET_assert (NULL == phone);
  phone =
      GNUNET_CONVERSATION_phone_create (cfg, caller_id, &phone_event_handler,
                                        NULL);
  /* FIXME: get record and print full GNS record info later here... */
  if (NULL == phone)
  {
    LOG ("%s", _("Failed to setup phone (internal error)\n"));
    phone_state = PS_ERROR;
  }
  else
  {
    GNUNET_CONVERSATION_phone_get_record (phone, &rd);
    GNUNET_free_non_null (address);
    address =
        GNUNET_GNSRECORD_value_to_string (rd.record_type, rd.data,
                                          rd.data_size);

    LOG (_("address: `%s' \n"), address);

    label = GTK_LABEL(get_object("GNUNET_CONVERSATION_GTK_my_address"));
    gtk_label_set_text(label, address);

    if (verbose)
      LOG (_("Phone active on line %u\n"), (unsigned int) line);
    phone_state = PS_LISTEN;
  }
  do_status();
}


/**
 * Function called with an event emitted by a call.
 *
 * @param cls closure, NULL
 * @param code type of the event on the call
 */
static void
call_event_handler (void *cls, enum GNUNET_CONVERSATION_CallEventCode code)
{
  //struct OutgoingCallClosure *cl = cls; 
 
  //LOG("call event handler code: %u num: %u", code, cl->call_num);

  //if (cls == NULL){
  set_outgoing_call_state(NULL, code);
  //GNUNET_break(0);
  //} else 
  //{
      switch (code)
      {
      case GNUNET_CONVERSATION_EC_CALL_RINGING:
        GNUNET_break (CS_RESOLVING == call_state);
        LOG (_("Resolved address of `%s'. Now ringing other party."), peer_name);
     //   set_outgoing_call_state(cls, CT_ringing);

        call_state = CS_RINGING;
        break;
      case GNUNET_CONVERSATION_EC_CALL_PICKED_UP:
        GNUNET_break (CS_RINGING == call_state);
        LOG (_("Connection established to `%s'"), peer_name);
        call_state = CS_CONNECTED;
        break;
      case GNUNET_CONVERSATION_EC_CALL_GNS_FAIL:
        GNUNET_break (CS_RESOLVING == call_state);
        LOG (_("Failed to resolve %s in ego `%s'"), peer_name, ego_name);
        call = NULL;
        break;
      case GNUNET_CONVERSATION_EC_CALL_HUNG_UP:
        LOG ("%s", _("Call terminated"));
        call = NULL;
        break;
      case GNUNET_CONVERSATION_EC_CALL_SUSPENDED:
        GNUNET_break (CS_CONNECTED == call_state);
        LOG (_("Connection to `%s' suspended (by other user)\n"), peer_name);
        break;
      case GNUNET_CONVERSATION_EC_CALL_RESUMED:
        GNUNET_break (CS_CONNECTED == call_state);
        LOG (_("Connection to `%s' resumed (by other user)\n"), peer_name);
        break;
      case GNUNET_CONVERSATION_EC_CALL_ERROR:
        LOG ("GNUNET_CONVERSATION_EC_CALL_ERROR %s", peer_name);
      }
  //}
}



/**
 * Initiating a new call
 *
 * @param arg arguments given to the command
 */
extern void
do_call (const char *arg)
{
  if (NULL == caller_id)
  {
    LOG (_("Ego `%s' not available\n"), ego_name);
    return;
  }
  if (NULL != call)
  {
    LOG (_("You are calling someone else already, hang up first!\n"));
    return;
  }
  switch (phone_state)
  {
  case PS_LOOKUP_EGO:
    LOG (_("Ego `%s' not available\n"), ego_name);
    return;
  case PS_LISTEN:
    /* ok to call! */
    break;
  case PS_ACCEPTED:
    LOG (_
         ("You are answering call from `%s', hang up or suspend that call first!\n"),
         peer_name);
    GNUNET_break(0);
    return;
  case PS_ERROR:
    /* ok to call */
    break;
  }
  //GNUNET_free_non_null (peer_name);
  peer_name = GNUNET_strdup (arg);
  LOG (_("now calling: %s"), peer_name);
  call_state = CS_RESOLVING;
  GNUNET_assert (NULL == call);

  call_counter++;
  call =
     GNUNET_CONVERSATION_call_start (cfg, caller_id, arg, speaker, mic,
                                     &call_event_handler, NULL);
  //call = newcall;

  // add call to active call list
    GtkTreeIter gtkiter;
    
    gtk_list_store_append (active_liststore, &gtkiter);

    gtk_list_store_set (active_liststore, &gtkiter,
                        AL_caller_id, peer_name,
                        AL_caller, NULL,
                        AL_caller_num, NULL,
                        AL_caller_state, CT_other,
                        AL_type, CALL_OUT,
                        AL_call, call,
                        AL_call_num, call_counter,
                        AL_call_state, CS_RESOLVING,
                        -1
                        );
 

  UPDATE_STATUS (_("We are calling `%s', his phone should be ringing."),
                 peer_name);
  GNUNET_CONVERSATION_GTK_history_add (CH_OUTGOING, peer_name);
}


/**
 * Accepting an incoming call
 *
 * @param args arguments given to the command
 */
static void
do_accept (struct GNUNET_CONVERSATION_Caller *sel_caller)
{
  struct CallList *cl;
  //char buf[32];


  if ((NULL != call) && (CS_SUSPENDED != call_state))
  {
    LOG (_("You are calling someone else already, hang up first!\n"));
    GNUNET_break(0);
    return;
  }
  switch (phone_state)
  {
  case PS_LOOKUP_EGO:
    GNUNET_break (0);
    break;
  case PS_LISTEN:
    /* this is the expected state */
    break;
  case PS_ACCEPTED:
    LOG (_
         ("You are answering call from `%s', hang up or suspend that call first!\n"),
         peer_name);
    GNUNET_break(0);
    return;
  case PS_ERROR:
    GNUNET_break (0);
    break;
  }//endswitch

    phone_state = PS_ACCEPTED;
    set_incoming_call_state(sel_caller,CT_active);
    
    for (cl = cl_head; cl; cl = cl->next)
    {
      /* FIXME: this may not be unique enough to identify the right item!
       * Why not store CallList items in treeview instead of just callers?
       */
      if (cl->caller == sel_caller)
        break;
    }
    GNUNET_CONVERSATION_caller_pick_up (sel_caller, &caller_event_handler, cl,
                                      speaker, mic);

    GNUNET_CONVERSATION_GTK_history_add (CH_ACCEPTED, peer_name);

}








/**
 * Suspending a call
 *
 * @param args arguments given to the command
 */

static void
do_suspend ()
{

  /*
  switch (phone_state)
  {
  case PS_LOOKUP_EGO:
  case PS_LISTEN:
  case PS_ERROR:
    LOG ("%s", _(" There is no call that could be suspended right now. (PS_ERROR)"));
    return;
  case PS_ACCEPTED:
    // expected state, do rejection logic 
    break;
  }
  */
  if (call_selected != NULL && caller_selected != NULL)
  {
      LOG("this should not be possible");
      GNUNET_break(0);
  }
  else 
  {

      // outgoing
      if (NULL != call_selected)
      {
        GNUNET_CONVERSATION_call_suspend (call_selected);
        set_outgoing_call_state(call_selected,CT_suspended);
        
        return;
      }

      // incoming
      if (NULL != caller_selected)
      {
        GNUNET_CONVERSATION_caller_suspend (caller_selected);
        set_incoming_call_state(caller_selected,CT_suspended);
        phone_state = PS_LISTEN;
        return;

      }
  }
}


/**
 * Resuming a call
 *
 * @param args arguments given to the command
 */
static void
do_resume ()
{

   switch (phone_state)
  {
  case PS_LOOKUP_EGO:
  case PS_ERROR:
    LOG ("%s", _("There is no call that could be resumed right now.(PS_ERROR)"));
    return;
  case PS_LISTEN:
    break;
  case PS_ACCEPTED:
    LOG (_("Already talking with `%s', cannot resume a call right now."),
         peer_name);
    return;
  }
////
  if (call_selected != NULL && caller_selected != NULL)
  {
      LOG("this should not be possible");
      GNUNET_break(0);
      return;
  }
  else 
  {

      // outgoing
      if (NULL != call_selected)
      {
        GNUNET_CONVERSATION_call_resume (call_selected, speaker, mic);
        set_outgoing_call_state(call_selected,CT_active);
        
        return;
      }

      // incoming
      if (NULL != caller_selected)
      {
        GNUNET_CONVERSATION_caller_resume (caller_selected, speaker, mic);
        set_incoming_call_state(caller_selected,CT_active);
        phone_state = PS_ACCEPTED;
        return;

      }
      GNUNET_break(0);
  }

//
//// 
}


/**
 / Rejecting a call
 *
 * @param args arguments given to the command
 */
static void
do_reject ()
{
  
  if (call_selected == NULL && caller_selected == NULL){
      GNUNET_break(0);
  }else {

  // if selected call is outgoing, stop it
  if (NULL != call_selected)
  {
    set_outgoing_call_state(call_selected,CT_hangup);
    
    GNUNET_CONVERSATION_call_stop(call);
    //GNUNET_CONVERSATION_call_stop (call_selected);
    
    call = NULL;
    call_selected = NULL;
    return;
  } else

  // if selected call is incoming, hang it up
  if (NULL != caller_selected)
  {
    set_incoming_call_state(caller_selected,CT_hangup);
    //FPRINTF(stderr,"hangup: %u", caller_selected);
    GNUNET_CONVERSATION_caller_hang_up(caller_selected);
    //cl_active = NULL;
    phone_state = PS_LISTEN;
    caller_selected = NULL;
  } else {
      GNUNET_break(0);
  }
    }
}






/**
 * Function called by identity service with information about egos.
 *
 * @param cls NULL
 * @param ego ego handle
 * @param ctx unused
 * @param name name of the ego
 */
static void
identity_cb (void *cls, struct GNUNET_IDENTITY_Ego *ego, void **ctx,
             const char *name)
{

  struct GNUNET_CRYPTO_EcdsaPublicKey pk;

  GtkTreeIter iter;
  
  if (NULL != ego) 
  { 
  GNUNET_IDENTITY_ego_get_public_key (ego, &pk);

  fprintf (stderr, "main identity_cb: %s \n", name);


  gtk_list_store_insert_with_values (zone_liststore,
                                       &iter, -1,
                                       0, name,
                                       1, ego,
                                       -1); 

  }


  if (NULL == name)
    return;
  if (ego == caller_id)
  {
    if (verbose)
      LOG (_("Name of our ego changed to `%s'\n"), name);
    //GNUNET_free (ego_name);
    ego_name = GNUNET_strdup (name);
    return;
  }
  if (0 != strcmp (name, ego_name))
    return;
  if (NULL == ego)
  {
    if (verbose)
      LOG (_("Our ego `%s' was deleted!\n"), ego_name);
    caller_id = NULL;
    return;
  }
  
  caller_id = ego;
 
  // do not remove this, it tells the phone which line to use
  GNUNET_CONFIGURATION_set_value_number (cfg, "CONVERSATION", "LINE", line);
       
  if (NULL == phone)
	start_phone();
}

/**
 * Get our configuration.
 *
 * @return configuration handle
 */
const struct GNUNET_CONFIGURATION_Handle *
GIG_get_configuration ()
{
  /* FIXME: Configuration handle returned is const, but we DO alter the config */
  return GNUNET_GTK_main_loop_get_configuration (ml);
}




/**
 * Task run on shutdown.
 *
 * @param cls unused
 * @param tc scheduler context, unused
 */
static void
shutdown_task (void *cls, const struct GNUNET_SCHEDULER_TaskContext *tc)
{

//TODO: make this work
  //struct OperationContext *oc;

/*
  GIG_advertise_shutdown_ ();
  while (NULL != (oc = oc_head))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
		_("Operation not completed due to shutdown\n"));
    GNUNET_IDENTITY_cancel (oc->op);
    GNUNET_CONTAINER_DLL_remove (oc_head,
				 oc_tail,
				 oc);
    GNUNET_free (oc);
  }
  if (NULL != identity)
  {
    GNUNET_IDENTITY_disconnect (identity);
    identity = NULL;
  }
  */
  GNUNET_GTK_tray_icon_destroy ();
  GNUNET_GTK_main_loop_quit (ml);
  ml = NULL;
}




/**
 * Callback invoked if the application is supposed to exit.
 *
 * @param object
 * @param user_data unused
 */
void
GNUNET_GTK_conversation_quit_cb (GObject * object, gpointer user_data)
{
  if (NULL != call)
  {
    GNUNET_CONVERSATION_call_stop (call);
    call = NULL;
  }
  if (NULL != phone)
  {
    GNUNET_CONVERSATION_phone_destroy (phone);
    phone = NULL;
  }
  if (NULL != id)
  {
    GNUNET_IDENTITY_disconnect (id);
    id = NULL;
  }
  //if (NULL != ns)
  //{
  //  GNUNET_NAMESTORE_disconnect (ns);
  //  ns = NULL;
  //}
  GNUNET_CONVERSATION_GTK_CONTACTS_shutdown();
  
  GNUNET_SPEAKER_destroy (speaker);
  speaker = NULL;
  GNUNET_MICROPHONE_destroy (mic);
  mic = NULL;
  ego_name = NULL;
  //GNUNET_free_non_null (peer_name);
  phone_state = PS_ERROR;

  GNUNET_SCHEDULER_shutdown ();


  //return 0;

}


/**
 * Actual main function run right after GNUnet's scheduler
 * is initialized.  Initializes up GTK and Glade.
 *
 * @param cls NULL
 * @param tc schedule context
 */
static void
run (void *cls, const struct GNUNET_SCHEDULER_TaskContext *tc)
{
  GtkWindow *main_window;

  //line = 0;

  ml = cls;
  if (GNUNET_OK != GNUNET_GTK_main_loop_build_window (ml, NULL))
    return;
  GNUNET_GTK_set_icon_search_path ();
  GNUNET_GTK_setup_nls ();
  /* setup main window */
  main_window = GTK_WINDOW (get_object ("GNUNET_GTK_conversation_window"));
  main_window =
      GTK_WINDOW (GNUNET_GTK_plug_me
                  ("GNUNET_CONVERSATION_GTK_PLUG", GTK_WIDGET (main_window)));
  gtk_window_set_default_size (main_window, 700, 700);
  
  // active calls
  active_liststore =
     GTK_LIST_STORE (get_object ("gnunet_conversation_gtk_active_calls_liststore"));
  active_treeview =
      GTK_TREE_VIEW (get_object ("gnunet_conversation_gtk_active_calls_treeview"));



//gtk_tree_view_set_activate_on_single_click(contacts_treeview, TRUE);

  
  //  gtk_window_maximize (GTK_WINDOW (main_window));

  if (NULL == getenv ("GNUNET_CONVERSATION_GTK_PLUG"))
    GNUNET_GTK_tray_icon_create (ml, GTK_WINDOW (main_window),
                                 "gnunet-conversation-gtk"
                                 /* FIXME: different icon? */ ,
                                 "gnunet-conversation-gtk");

  /* make GUI visible */
  if (!tray_only)
  {
    gtk_widget_show (GTK_WIDGET (main_window));
    gtk_window_present (GTK_WINDOW (main_window));
  }

  /* get gui objects */

  if(debug_box_enabled)
  {
      gtk_widget_show(GTK_WIDGET(get_object("GNUNET_CONVERSATION_log_box")));
  }
  GNUNET_SCHEDULER_add_delayed (GNUNET_TIME_UNIT_FOREVER_REL, &shutdown_task,
                                NULL);
  
  cfg = GIG_get_configuration ();

  speaker = GNUNET_SPEAKER_create_from_hardware (cfg);
  mic = GNUNET_MICROPHONE_create_from_hardware (cfg);

        b_contact = GTK_WIDGET (get_object ("GNUNET_GTK_conversation_use_current_button"));
        b_accept  = GTK_WIDGET (get_object ("GNUNET_GTK_conversation_accept_button"));
        b_hangup  = GTK_WIDGET (get_object ("GNUNET_GTK_conversation_hangup_button"));
        b_suspend = GTK_WIDGET (get_object ("GNUNET_GTK_conversation_suspend_button"));
        b_resume  = GTK_WIDGET (get_object ("GNUNET_GTK_conversation_resume_button"));




  if (NULL == ego_name)
  {
    ego_name = "phone-ego";
    LOG (_("No ego given, using default: %s "), ego_name);

  }
  id = GNUNET_IDENTITY_connect (cfg, &identity_cb, NULL);
 
  GNUNET_CONVERSATION_GTK_CONTACTS_init ();
}


/**
 * Main function of gnunet-conversation-gtk.
 *
 * @param argc number of arguments
 * @param argv arguments
 * @return 0 on success
 */
int
main (int argc, char *const *argv)
{
  static struct GNUNET_GETOPT_CommandLineOption options[] = {
    {'p', "phone", "LINE",
     gettext_noop ("sets the LINE to use for the phone"),
     1, &GNUNET_GETOPT_set_uint, &line},

    {'e', "ego", "ego",
     gettext_noop ("select ego to use"), 1,
     &GNUNET_GETOPT_set_string, &ego_name},

    {'d', "debug_box", "1 or 0",
     gettext_noop ("enable debug box"), 1,
     &GNUNET_GETOPT_set_uint, &debug_box_enabled},


    {'t', "tray", NULL,
     gettext_noop ("start in tray mode"), 0,
     &GNUNET_GETOPT_set_one, &tray_only},
    GNUNET_GETOPT_OPTION_END
  };
  if (GNUNET_OK !=
      GNUNET_GTK_main_loop_start ("gnunet-conversation-gtk",
                                  "GTK GUI for conversation", argc, argv,
                                  options,
                                  "gnunet_conversation_gtk_main_window.glade",
                                  &run))
    return 1;
  return 0;
}

/**
 * call clicked
 */
void
GNUNET_CONVERSATION_GTK_on_call_clicked ()
{
  GtkEntry *address_entry;

  address_entry = GTK_ENTRY (get_object ("GNUNET_GTK_conversation_address"));

  do_call (gtk_entry_get_text(address_entry));
  //disable_button ("GNUNET_GTK_conversation_accept_button");
  do_status ();
//  free(to_addr);
}

/**
 *  hangup clicked
 */
void
GNUNET_CONVERSATION_GTK_on_hangup_clicked ()
{

  do_reject ();

  do_status ();
  //history_add(3,peer_name);
}

/**
 *  accept clicked
 */
void
GNUNET_CONVERSATION_GTK_on_accept_clicked ()
{
  if (caller_selected != NULL)
  {
  do_accept (caller_selected);
  } else {
      GNUNET_break(0);
  }
  do_status(); 
}


/**
 *  reject clicked
 */
void
GNUNET_CONVERSATION_GTK_on_reject_clicked ()
{
  do_reject ();
  do_status();
}

/**
 * pause clicked
 */
void
GNUNET_CONVERSATION_GTK_on_pause_clicked ()
{
  do_suspend ();
  do_status();
}

/**
 *  resume clicked
 */
void
GNUNET_CONVERSATION_GTK_on_resume_clicked ()
{
  do_resume ();
  do_status();
}

/**
 * status clicked
 */
void
GNUNET_CONVERSATION_GTK_on_status_clicked ()
{
  do_status ();
}

/*
 * test function
 */
void
GNUNET_contact_test ()
{
  GtkTreeIter iter;
  char *caller_id = "testje";
  int  caller_num = 10; 

enum {
  AL_caller_id, // *gchar
  AL_caller,  // *
  AL_caller_num //gint
};

  gtk_list_store_append (active_liststore, &iter);
  gtk_list_store_set (active_liststore, &iter, 2, caller_num, 0, caller_id, -1);

  gtk_list_store_insert_with_values (zone_liststore,
                                       &iter, -1,
                                       0, "test",
                                       -1);
} 

/*
 * @brief outgoing ego selector changed
 */
void
gnunet_conversation_gtk_outgoing_zone_combobox_changed_cb (GtkComboBox *widget,
                                               gpointer user_data)
{
  GtkTreeIter iter;
  struct GNUNET_IDENTITY_Ego *tempEgo;
  char *tempName;
  //struct GNUNET_CRYPTO_EcdsaPrivateKey temp_zone_pkey;
  //GtkTreeSelection *selection;


  gtk_combo_box_get_active_iter(widget, &iter);

  gtk_tree_model_get (GTK_TREE_MODEL (zone_liststore),
                      &iter,
                      0, &tempName,
                      1, &tempEgo,
                      -1);
  //LOG(stderr,"outgoing ego: %s", tempName);
 // caller_id = tempEgo;
  /*
  if ( NULL != phone)
    {
	GNUNET_CONVERSATION_phone_destroy (phone);
        phone == NULL;
    }
  start_phone();
  */
}

/*
 * @brief active call list type data function
 */
void
   active_calls_type_data_function (GtkTreeViewColumn *col,
                                               GtkCellRenderer   *renderer,
                                               GtkTreeModel      *model,
                                               GtkTreeIter       *iter,
                                               gpointer           user_data)
   {
     gint  state;
     gchar   buf[20];

     gtk_tree_model_get(model, iter, AL_caller_state, &state, -1);
     g_snprintf(buf, sizeof(buf), "status %u", state);
     g_object_set(renderer, "text", buf, NULL);
       /* set 'cell-background' property of the cell renderer */
      
        switch (state) 
        {
            case CT_active:
      g_object_set(renderer,
                    "cell-background", "Green",
                    "cell-background-set", TRUE,
                     NULL);


           break;
            case CT_ringing:
      g_object_set(renderer,
                    "cell-background", "Blue",
                    "cell-background-set", TRUE,
                     NULL);


           break;
            case CT_rejected:

           break;
            case CT_suspended:
      g_object_set(renderer,
                    "cell-background", "Orange",
                    "cell-background-set", TRUE,
                     NULL);


           break;
            default:
            GNUNET_break(0);
            break;
        }  

     //gtk_tree_model_get(model, iter, AL_caller_state, &state, -1);
 
     //g_snprintf(buf, sizeof(buf), "state: %u", state);
 
     //g_object_set(renderer, "text", buf, NULL);
     //
//     if (foo->is_important)
        g_object_set(renderer, "markup", "<b>important</b>", "text", NULL, NULL);
//     else
//        g_object_set(renderer, "markup", NULL, "text", "not important", NULL);
     //
   }


/*
 * @brief second test button
 */
void
GNUNET_CONVERSATION_GTK_test_button_two()
{
 GtkTreeViewColumn *column;
// GtkCellRenderer *cell;
 GtkCellRenderer *renderer;
 //LOG("test");
// treeview = get_object(ml,"gnunet_conversation_gtk_active_calls_treeview");

// column = get_object(ml,"GNUNET_CONVERSATION_GTK_caller_typeColumn");
 //cell = gtk_cell_renderer_text_new();
// cell = get_object(ml,"GNUNET_CONVERSATION_GTK_active_calls_type");
// gtk_tree_view_column_set_cell_data_func(column, cell, active_calls_type_data_function, NULL, NULL );

 column = GTK_TREE_VIEW_COLUMN(get_object("caller_testcolumn"));
 gtk_tree_view_column_set_title(column, "test stateC");
 renderer = gtk_cell_renderer_text_new();

   /* pack cell renderer into tree view column */
   gtk_tree_view_column_pack_start(column, renderer, TRUE);

     /* set 'text' property of the cell renderer */
     g_object_set(renderer, "text", "Boooo!", NULL);
       /* set 'cell-background' property of the cell renderer */
       g_object_set(renderer,
                    "cell-background", "Orange",
                    "cell-background-set", TRUE,
                     NULL);
   gtk_tree_view_column_add_attribute(column, renderer, "text", AL_caller_state);
 gtk_tree_view_column_set_cell_data_func(column, renderer, active_calls_type_data_function, NULL, NULL );

}


