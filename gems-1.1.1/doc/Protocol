
gems communication protocol
---------------------------

Data streams sent in either way (server - >client or client -> server) have
the following structure:

	| msg type (1 byte) | Size n (4 bytes) | Message (n bytes) |

Integer numbers are sent with the least significant byte first (this is
how integers are stored in i386).

Types of messages:

	| DATA | Size: n | Data (n bytes) |

		Code: DATA = 0x00

		With this message the server sends all the data which is
		to be shown on screen by the clients.

		n is at most 1024 bytes.

	| WINSIZE | Size: 8 | Cols (4 bytes) | Rows (4 bytes) |

		Code: WINSIZE = 0x01

		By sending this message, the server informs the client about
		its terminal size.

	| VERSION | SIZE: n | Version string (n bytes) |

		Code: VERSION = 0x02

		Informs about the protocol version used by the program.

	| ACK | SIZE: 0 |

		Code: ACK = 0x03

		Acknowledge.

	| DISCONNECT | SIZE: 0 |

		Code: DISCONNECT = 0x04

		After any of both parts send this message, the connection 
		is closed.

When a client starts the connection with the server, the following messages 
are interchanged in order to decide if the connection is accepted or not.

	CLIENT					SERVER

				VERSION <----
	Client can
	close connection
	if its protocol version
	is incompatible.
				----> ACK

				----> VERSION
						Server can refuse connection
						if client's protocol version
						is incompatibe.
				    ACK <----

				WINSIZE <----
	Client can close
	connection if its
	terminal size is small.
				----> ACK
						Connection accepted. Server
						starts sending data.
				   (...)
				DATA <----
				DATA <----
				DATA <----
				   (...)


During the initial communication (before any DATA messages are sent), if
the server refuses the client, or the client decides to close connection,
the DISCONNECT message must be sent instead of ACK, before physically
closing the connection.

If the first message sent to the client is DISCONNECT (instead of VERSION),
this means that the server refuses the connection because it has reached
the maximum number of allowed connections.

It is not necessary to send DISCONNECT to close the connection after it has
been accepted (i.e., if a user closes manually the server or the client).

If a change in the terminal size occurs, the server can send another
WINSIZE message, where clients can disconnect or not (client must not answer
with ACK in this case).

