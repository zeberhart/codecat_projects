/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: 99f6447f0952ceca754c8e462f41246fbbcca763 $
*/

STRING(bpp);
STRING(channels);
STRING(data);
STRING(height);
STRING(image_data);
STRING(layers);
STRING(mask);
STRING(name);
STRING(properties);
STRING(tiles);
STRING(type);
STRING(width);
