/*
     This file is part of GNUnet.
     (C) 2010-2013 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/conversation/gnunet-conversation-gtk_history.h
 * @brief 
 * @author yids
 * @author hark
 */
/*
extern void
display_record (void *cls, const struct GNUNET_CRYPTO_EcdsaPrivateKey *zone_key,
                const char *rname, unsigned int rd_len,
                const struct GNUNET_GNSRECORD_Data *rd);
*/

extern char *currentlySelectedCallAddress;

/**
 * function to initialize the contact list
*/

extern void
GNUNET_CONVERSATION_GTK_CONTACTS_init ();

/** 
 * function to shutdown the contact list
*/

extern void
GNUNET_CONVERSATION_GTK_CONTACTS_shutdown();


