/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include "pp_usertype.h"
# include "pp_tree.h"
# include "pp_bd.h"
# include "pp_proto.h"
# include "pp_abs.h"


/*|======================================================================|
  | SYRTHES PARALLELE                                  JANV 07           |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | METIS  (Decomposition d'un maillage 2D/3D) :                         |
  |                                                                      |
  |    Creation du partitionnement                                       |
  |                                                                      |
  |======================================================================|*/

void pp_geopart(rp_int nelems,rp_int npoins,rp_int ndiele,
		double **coord,rp_int **nodes,
		rp_int nbeltmax_tree,rp_int *nparts, rp_int  *numdome) 
{
  /*|=======================================================================|
    |   Nom    | Type | Mode |             Role                             |
    |=======================================================================|
    | nelems   |  e   |  d   | nombre d'elements du maillage                |
    | npoins   |  e   |  d   | nombre de noeuds du maillage                 |
    | ndiele   |  e   |  d   | dimension des elements du probleme           |
    | nbeltmaxpart| e |  d   | nombre max d'elts par partition              |
    | nparts   |  e   |  r   | nombre de partitions reelles                 |
    | numdome  |  te  |  r   | attribution d'une partition a chaque element |
    | nodes    |  te  |  d   | connectivite globale                         |
    | coord    |  tr  |  d   | coordonnees                                  |
    |=======================================================================| 

    Type : e (entier), r (reel), t (tableau)
    Mode : d (donnee non modifiee), r (resultat), m (donnee modifiee)      
    Rq : on suppose que le maillage d'entree est P1                         */
        
  
  /* variables : 
     ----------- */
  rp_int i,numpart;
  double dim_boite3d[6],dim_boite2d[4],size_min;
  struct node *arbre;


  if (SYRTHES_LANG == FR) {
  printf("\n *** GEOPART : PARTITIONNEMENT DU MAILLAGE SUR CRITERES GEOMETRIQUES\n");
  printf("             nombre de noeuds %d\n",npoins);
  printf("             nombre d'elements %d\n",nelems);}
  else if (SYRTHES_LANG == EN) {
  printf("\n *** GEOPART : MESH PARTITIONNING WITH GEOMETRIC CRITERIONS\n");
  printf("             number of nodes    %d\n",npoins);
  printf("             number of elements %d\n",nelems);}
 
  
  size_min=1.E8;
  arbre= (struct node *)malloc(sizeof(struct node));
  if (arbre==NULL) 
    {printf(" ERROR geopart : allocation memory error\n");exit(0);}

  if (ndiele==3)
    pp_build_octree_3d(arbre,npoins,nelems,nodes,coord,&size_min,dim_boite3d,nbeltmax_tree);
  else
    pp_build_quadtree_2d(arbre,npoins,nelems,nodes,coord,&size_min,dim_boite2d,nbeltmax_tree);


  numpart=-1;
  pp_attribue_part(arbre,numdome,&numpart);

  *nparts=numpart+1;
  
  tuer_tree(arbre,8);


  if (SYRTHES_LANG == FR) printf("             Nombre de partitions crees : %d\n",numpart); 
  else if (SYRTHES_LANG == EN) printf("             Creation of %d partitions\n",numpart); 
  for (i=0;i<min(nelems,100);i++)
    printf("             elt %d  --> partition %d\n",i,numdome[i]);

}  
   
/*|======================================================================|
  | SYRTHES 3.2                JUIN 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | tuer_tree                                                            |
  |         Tuer l'arbre...                                              |
  |======================================================================| */
void pp_attribue_part(struct node *noeud,rp_int *numdome,
		      rp_int *numpart)

{
    rp_int i,nbf;
    struct element *fa;
    struct child *f1;

    fa=noeud->lelement;
    if (fa) (*numpart)++;
    while (fa) 
	{
            numdome[fa->num]=*numpart;
	    fa=fa->suivant;
	}

    f1 = noeud->lfils;
    while (f1)
	{
	  pp_attribue_part(f1->fils,numdome,numpart);
	  f1 = f1->suivant;
	}
}	    



/*|======================================================================|
  | SYRTHES 3.2                JUIN 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | tuer_tree                                                            |
  |         Tuer l'arbre...                                              |
  |======================================================================| */
void pp_elimine_part(rp_int nelem,rp_int nbeltmax_tree,
		     rp_int *numdome, rp_int *nparts)
{
  rp_int i,n,fin;
  rp_int *nelemloc;

  /* compte du nombre d'elements volumique par partition */
  /* --------------------------------------------------- */
/*   nelemloc=(rp_int*)malloc((npart)*sizeof(rp_int)); */
/*   if (!nelemloc) {printf(" ERREUR pp_cree_tables : probleme d'allocation memoire (nelemloc)\n");exit(0);} */
/*   for (n=0;n<npart;n++) nelemloc[n]=0; */
/*   for (i=0;i<nelem;i++) nelemloc[numdome[i]]++; */

/*   n=0;   fin=npart; */

/*   while(n<npart && n<fin) */
/*     { */
/*       if (nelemloc[n]=0) */
/* 	{ */
/* 	  for (i=0;i<nelem;i++)  */
/* 	    if (numdome[i]>=n) numdome[i]--; */
/* 	} */
/*     } */
}
