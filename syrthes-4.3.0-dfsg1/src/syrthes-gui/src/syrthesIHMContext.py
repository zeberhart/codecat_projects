# -*- coding: utf-8 -*-

import os

class syrthesIHMContext(object):
    exeAbsDirPath = "" # absolute path to the folder containing the python main file(SyrthesMain.py) or the binary executable (SyrthesMain) 
    dataAbsFullPath = "" # absolute path to the data file (.syd)
    advancedModeActivated=False # equivalent to action Menu Tools -> Advanced mode in GUI
    clipboard = [] # clipboard
    embedded = False # True means SYRTHES is called from SALOME.
    notFullyFilledException = False
    
    def setExeFile(argv0):
        syrthesIHMContext.exeAbsDirPath = os.path.dirname(os.path.abspath(argv0))

    def getExeAbsDirPath():
        return syrthesIHMContext.exeAbsDirPath

    def setDataFile(datafile):
        if datafile != '' :
            syrthesIHMContext.dataAbsFullPath = os.path.abspath(datafile)
        else :
            syrthesIHMContext.dataAbsFullPath = ''
            
    def getDataAbsFullPath():
        return syrthesIHMContext.dataAbsFullPath
    
    def reinitClipboard():
        syrthesIHMContext.clipboard = []
    def addToClipboard(anything):
        syrthesIHMContext.clipboard.append(anything)
    def getFromClipboard(k):
        if k < len(syrthesIHMContext.clipboard) :
            return syrthesIHMContext.clipboard[k]
        else :
            return ""
    
    def setEmbedded(bool):
        syrthesIHMContext.embedded=bool
    
    def isEmbedded():
        return syrthesIHMContext.embedded
    
    
    setExeFile=staticmethod(setExeFile)
    getExeAbsDirPath=staticmethod(getExeAbsDirPath)
    
    setDataFile=staticmethod(setDataFile)
    getDataAbsFullPath=staticmethod(getDataAbsFullPath)
    
    reinitClipboard=staticmethod(reinitClipboard)
    addToClipboard=staticmethod(addToClipboard)
    getFromClipboard=staticmethod(getFromClipboard)
    
    setEmbedded=staticmethod(setEmbedded)
    isEmbedded=staticmethod(isEmbedded)
        