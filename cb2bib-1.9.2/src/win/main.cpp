/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#include <QFileInfo>
#include <QProcess>

int main(int argc, char* argv[])
{
    if (argc < 1)
        return 1;
    QFileInfo fi(argv[0]);
    const QString exe(fi.absolutePath() + "/cb2bib.exe");
    QStringList arguments;
    for (int i = 1; i < argc; ++i)
        arguments.append(argv[i]);
    const int code(QProcess::execute(exe, arguments));
    return code;
}
