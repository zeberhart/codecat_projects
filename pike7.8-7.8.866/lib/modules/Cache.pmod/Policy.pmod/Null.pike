/*
 * Null policy-manager for the generic Caching system
 * by Francesco Chemolli <kinkie@roxen.com>
 *
 * $Id: d2dea32e0f29f5dc3fc5d9e0cfd3b0f903a4dbfd $
 *
 * This is a policy manager that doesn't actually expire anything.
 * It is useful in multilevel and/or network-based caches.
 */

#pike __REAL_VERSION__

void expire (Cache.Storage.Base storage) {
  /* empty */
}
