/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    Proprietes du bois_agglo                                               |
  |======================================================================| */

/* Initialisations des constantes */
double fmat_const_bois_agglo(struct ConstMateriaux *constmat,
			struct ConstPhyhmt constphyhmt)
{
  constmat->eps0=0.25;
  constmat->rhos=600;
  constmat->cs=1700;
  constmat->xknv=0;
  constmat->xk=3.21e-14;
  constmat->taumax=250;
  return 1;
}


/* taux d'humidite volumique */
/* ---------------------------------------------------------------------- */
double fmat_ftauv_bois_agglo(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double xpv,double psat,double t)
{
  double x,tauv;

  x=xpv/psat;
  if (x < 0.202e0)
    tauv=0.1900990099e3 * x;
  else if (x < 0.432e0) 
    tauv=0.2153739130e2 + 0.8347826087e2 * x;
  else if (x < 0.651e0)
    tauv=0.1735890411e2 + 0.9315068493e2 * x;
  else if (x < 0.852e0)
    tauv=-0.1916417910e2 + 0.1492537313e3 * x;
  else if (x < 0.949e0)
    tauv=-0.1923958763e3 + 0.3525773196e3 * x;
  else if (x < 0.973e0)
    tauv=-0.7830750000e3 + 0.9750000000e3 * x;
  else if (x<=1.)
    tauv=-0.2875925926e4 + 0.3125925926e4 * x;
  else
    {
/*       printf("error bois_aglo tauv : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/*       exit(1); */
      tauv=constmat.taumax;
    }
  return tauv;
}

/* pente de l'isotherme de sorption */
/* ---------------------------------------------------------------------- */
double fmat_falpha_bois_agglo(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double pv,double psat,double t)
{
  double x,alpha;
  x=pv/psat;

  if (x < 0.432e0)
    alpha=0.2837398417e3 - 0.4635684739e3 * x;
  else if ( x < 0.651e0)
    alpha=0.6439841068e2 + 0.4416631991e2 * x;
  else if ( x < 0.852e0)
    alpha=-0.8855619662e2 + 0.2791196337e3 * x;
  else if ( x < 0.949e0)
    alpha=-0.1636640055e4 + 0.2096119467e4 * x;
  else if ( x < 0.973e0)
    alpha=-0.2425905283e5 + 0.2593427835e5 * x;
  else if ( x<=1.0)
    alpha=-0.7653799726e5 + 0.7966392319e5 * x;
  else
    {
/*       printf("error bois_aglo alfa : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/*       exit(1); */
      alpha=-0.7653799726e5 + 0.7966392319e5 * 1.;
    }
  return alpha/psat;
  
}

/* permeabilite relative au gaz */
/* ---------------------------------------------------------------------- */
double fmat_fkrg_bois_agglo(struct ConstMateriaux constmat,double tauv)
{
  return 1-tauv/constmat.taumax;
}

/* permeabilite relative au liquide */
/* ---------------------------------------------------------------------- */
double fmat_fkrl_bois_agglo(struct ConstMateriaux constmat,double tauv)
{
  double x,fkrl;
  x=tauv/constmat.taumax;

  if (x < 0.138e0)
    fkrl=0.2753623188e-6 * x;
  else if ( x < 0.207e0)
    fkrl=-0.1140000000e-6 + 0.1101449275e-5 * x;
  else if (  x < 0.243e0)
    fkrl=-0.1703000000e-5 + 0.8777777778e-5 * x;
  else if (  x < 0.366e0)
    fkrl=-0.2000000000e-5 + 0.1000000000e-4 * x;
  else if (  x < 0.500e0)
    fkrl=0.7313432836e-6 + 0.2537313433e-5 * x;
  else if (  x < 0.660e0)
    fkrl=-0.1125000000e-5 + 0.6250000000e-5 * x;
  else if (  x < 0.750e0)
    fkrl=-0.2933083333e0 + 0.4444111111e0 * x;
  else if (  x < 0.850e0)
    fkrl=-0.1010000000e1 + 0.1400000000e1 * x;
  else if (  x < 0.900e0)
    fkrl=-0.2370000000e1 + 0.3000000000e1 * x;
  else if (  x < 0.950e0)
    fkrl=-0.4530000000e1 + 0.5400000000e1 * x;
  else if (x<=1.)
    fkrl=-0.7000000000e1 + 0.8000000000e1 * x;
  else
    {
/*       printf("probleme bois_aglo krl : tauv=%f taumax=%f\n",tauv,constmat.taumax); */
/*       return 0; */
      fkrl=-0.7000000000e1 + 0.8000000000e1 * 1.;
    }

  return fkrl;
}


/* conductivite thermique du materiau humide */
/* ---------------------------------------------------------------------- */
double fmat_fklambt_bois_agglo(double t,double tauv)
{
  return 0.15;
}


/* coefficient de diffusion de la vapeur dans le materiau */
/* ---------------------------------------------------------------------- */
double fmat_fpiv_bois_agglo(double xpv,double psat,double t)
{
  double x,xpiv;
  x=xpv/psat;
  if (x < 0.625e-1)
    xpiv=0.2000000000e-7 + 0.1859200000e-7 * x;
  else if ( x < 0.262e0)
    xpiv=0.2069771429e-7 + 0.7428571429e-8 * x;
  else if (  x < 0.492e0)
    xpiv=0.9627156522e-8 + 0.4968260870e-7 * x;
  else if (  x < 0.646e0)
    xpiv=0.1784458442e-7 + 0.3298051948e-7 * x;
  else if (  x < 0.846e0)
    xpiv=-0.3945851000e-7 + 0.1216850000e-6 * x;
  else if (x<=1.)
    xpiv=-0.8216233766e-7 + 0.1721623377e-6 * x;
  else
    {
/*       printf("error bois_aglo piv  : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/*       exit(1); */
      xpiv=-0.8216233766e-7 + 0.1721623377e-6 * 1.;
    }
  return xpiv;
}

/* Fonction utilisateur hm : chaleur latente complementaire */
/* ---------------------------------------------------------------------- */
double fmat_fhm_bois_agglo(double tauv)
{
  return 0.;
}

/* derivee par rapport a tauv de la fonction hm */
/* ---------------------------------------------------------------------- */
double fmat_fdhmdtauv_bois_agglo(double tauv)
{
  return 0.;
}

/* Fonction utilisateur fbetap */
/* ---------------------------------------------------------------------- */
double fmat_fbetap_bois_agglo(struct ConstPhyhmt constphyhmt,
			 struct ConstMateriaux constmat,
			 double xpv,double psat,double tauv,double t)
{
  return -fmat_falpha_bois_agglo(constphyhmt,constmat,xpv,psat,t)*xpv
    *(fphyhmt_fxl(constphyhmt,t)+fmat_fhm_bois_agglo(tauv))/constphyhmt.Rv/(t*t);
}

/* fonction supplementaire dhp - betap * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdhp_bois_agglo(double betap, double tauv)
{
  return 0.;
}

/* fonction supplementaire dht - alpha * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdht_bois_agglo(double alphat, double tauv)
{
  return 0.;
}


