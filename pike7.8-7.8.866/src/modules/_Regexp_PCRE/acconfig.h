/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: 8e87099046b84b7de39da53b523a2d09e7d64063 $
*/

@TOP@

/* Define this if you have -lpcre */
#undef HAVE_LIBPCRE

@BOTTOM@
