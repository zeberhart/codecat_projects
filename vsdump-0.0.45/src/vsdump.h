#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

#define VER_OFFSET  0x1a
#define SIZE_OFFSET 0x1c
#define TRLR_OFFSET 0x24

typedef struct {
	guint32	type;		/* was 'subtype' in the previous version */
	guint32	addr;
	guint32	offset;
	guint32	len;
	guint16	frmt;		/* was 'type' in previous version */
	guint32	prnt;		/* offset of stream from which this pointer was extracted -- "parent" */
} strm_ptr;

