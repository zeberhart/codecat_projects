/*
 * This file is part of the Kvkbd project.
 * Copyright (C) 2007-2008 Todor Gyumyushev <yodor@developer.bg>
 * Copyright (C) 2008 Guillaume Martres <smarter@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef NUMPADVBUTTON_H
#define NUMPADVBUTTON_H

#include "vbutton.h"

class NumpadVButton : public VButton
{
    Q_OBJECT
public:
    explicit NumpadVButton(QWidget *parent = 0);
    ~NumpadVButton();
    void numLockPressed(bool press);

};

#endif // NUMPADVBUTTON_H
