/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#ifndef C2BWEBBROWSER_H
#define C2BWEBBROWSER_H

#include <QMenu>
#include <qwebview.h>


class c2bWebBrowser : public QWebView
{

    Q_OBJECT

public:
    explicit c2bWebBrowser(QWidget* parentw = 0);
    virtual ~c2bWebBrowser();

    QAction* viewBackwardAction;
    QAction* viewForwardAction;
    QAction* viewHomeAction;
    QAction* viewReloadAction;
    QAction* viewZoomInAction;
    QAction* viewZoomOutAction;


signals:
    void statusMessage(const QString ms);


public slots:
    void loadPage(const QString& p_url);
    void setFixedFont(const QFont& qfont);
    void setFont(const QFont& qfont);
    void setHomePage(const QString& hp_url);


protected:
    virtual QWebView* createWindow(QWebPage::WebWindowType type);


protected slots:
    void copy();
    void home();
    void zoomIn();
    void zoomOut();


private:
    QString _home_page;
    QString _hovered_link;


private slots:
    void _linkHovered(const QString& link, const QString& link_title, const QString& link_text);

};

#endif
