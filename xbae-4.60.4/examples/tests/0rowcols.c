
#include <stdlib.h>
#include <stdio.h>

#include <X11/Intrinsic.h>
#include <Xm/Xm.h>

#include <Xbae/Matrix.h> /* for version info */
#include <Xm/Form.h>

void
doit(XtPointer data)
{
    Widget w = (Widget) data;

    static int n = 0;

    static Boolean do_cols = True;
    static Boolean row_insert = True;
    static Boolean col_insert = True;

    int n_rows = XbaeMatrixNumRows(w);
    int n_cols = XbaeMatrixNumColumns(w);

    int i;

    printf("n_rows = %d, n_cols = %d ", n_rows, n_cols);

    if (do_cols) {
        if (col_insert) {
            printf("adding a column\n");
            XbaeMatrixAddColumns(w, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);
            if (n_rows) {
                for( i = 0; i < n_rows; i++) {
                    XbaeMatrixSetCell(w, i, 0, "c");
                }
            }
        } else {
            printf("deleting a column\n");
            XbaeMatrixDeleteColumns(w, 0, 1);
        }
    } else {
        if (row_insert) {
            printf("adding a row\n");
            XbaeMatrixAddVarRows(w, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);
            if (n_cols) {
                for( i = 0; i < n_cols; i++) {
                    XbaeMatrixSetCell(w, 0, i, "r");
                }
            }
        } else {
            printf("deleting a row\n");
            XbaeMatrixDeleteRows(w, 0, 1);
        }
    }
    
    n++;
    if (n == 3) {
        if (do_cols) {
            col_insert = !col_insert;
        } else {
            row_insert = !row_insert;
        }

        if (!row_insert || !col_insert) {
            do_cols = !do_cols;
        }
        n = 0;
    }
    
    XtAppAddTimeOut(
	    XtWidgetToApplicationContext(w), 500, (XtTimerCallbackProc)doit,
	    (XtPointer)w);
}

int
main (int argc, char **argv)
{
        Widget toplevel, form, mw;

        /* Print run-time version of libXbae to stdout */
        printf("Using Xbae %s\n", XbaeGetVersionTxt());
        printf("This example was built with %s\n", XbaeVersionTxt);

        /* Print compile-time version of libXm to stdout */
        printf("Xbae was built with %s\n", XbaeGetXmVersionTxt());
        printf("and is running with %s\n", XmVERSION_STRING);

        toplevel = XtInitialize (argv[0], "Test", NULL, 0, &argc, argv);
        form = XtVaCreateManagedWidget("form", xmFormWidgetClass, toplevel,
                                       XmNwidth, 	500,
                                       XmNheight,	400,
                                       NULL);

        mw = XtVaCreateManagedWidget ("test_xbae",
            xbaeMatrixWidgetClass,
            form,
            XmNtopAttachment,    XmATTACH_FORM,
            XmNleftAttachment,   XmATTACH_FORM,
            XmNbottomAttachment, XmATTACH_FORM,
            XmNtopOffset, 10,
            XmNbottomOffset, 10,
            XmNleftOffset, 10,
            XmNrightOffset, 10,
            XmNrightAttachment,  XmATTACH_FORM,
            XmNcolumns, 0,
            XmNrows, 0,
            NULL);

        XtRealizeWidget (toplevel);
        doit(mw);
        XtMainLoop ();

        return (EXIT_SUCCESS);
}
