#!/bin/sh

BASE_REL=$(dpkg-parsechangelog 2>/dev/null | sed -ne 's/^Version: \(\([0-9]+\)\)\?\(.*\)-.*/\3/p' | sed 's/[+~-].*//') 
OLDDIR=${PWD}
GOS_DIR=${OLDDIR}/get-orig-source
SVN_COMMIT='svn log https://swami.svn.sourceforge.net/svnroot/swami/trunk/swami -l 1 | sed -ne "s/r\([0-9]\+\).*/\1/p"'

if [ -z ${BASE_REL} ]; then
	echo 'Please run this script from the sources root directory.'
	exit 1
fi


rm -rf ${GOS_DIR}
mkdir ${GOS_DIR} && cd ${GOS_DIR}
SWAMI_SVN_COMMIT=$(eval "${SVN_COMMIT}")
svn export -r ${SWAMI_SVN_COMMIT} https://swami.svn.sourceforge.net/svnroot/swami/trunk/swami/ swami
cd swami/
cd .. && tar cfz \
	${OLDDIR}/swami_${BASE_REL}+svn${SWAMI_SVN_COMMIT}.orig.tar.gz \
	swami
rm -rf ${GOS_DIR}
