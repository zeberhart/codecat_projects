/* cereimg.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* cereimg.h
 * Copyright (C) 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTK_CEREIMG_H__
#define __GTK_CEREIMG_H__

#include <gtk/gtkcellrenderer.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_TYPE_CEREIMG			(cereimg_get_type ())
#define GTK_CEREIMG(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_CEREIMG, Cereimg))
#define GTK_CEREIMG_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_CEREIMG, CereimgClass))
#define GTK_IS_CEREIMG(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_CEREIMG))
#define GTK_IS_CEREIMG_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_CEREIMG))
#define GTK_CEREIMG_GET_CLASS(obj)         (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_CEREIMG, CereimgClass))

typedef struct _Cereimg Cereimg;
typedef struct _CereimgClass CereimgClass;

struct _Cereimg
{
  GtkCellRenderer parent;

  /*< private >*/
  GdkPixbuf *pixbuf;
  GdkPixbuf *pixbuf_expander_open;
  GdkPixbuf *pixbuf_expander_closed;
  GdkColor   background_color;

  guint activatable : 1;
  guint background_set : 1;
  
};

struct _CereimgClass
{
  GtkCellRendererClass parent_class;

  void (* clicked) (Cereimg *cereimg, const gchar *path);
  
  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
  void (*_gtk_reserved3) (void);
  void (*_gtk_reserved4) (void);
};

GType            cereimg_get_type (void);
GtkCellRenderer *cereimg_new      (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTK_CEREIMG_H__ */
