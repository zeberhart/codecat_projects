/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
# include <string.h>
# include <math.h>
#include "pp_usertype.h"
#include "pp_abs.h"
#include "pp_bd.h"
#include "pp_proto.h"


rp_int somfac[4][3];

/*|======================================================================|
  | SYRTHES 4.3                                        COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Extraction du maillage de bord                                       |
  |======================================================================| */

void extrbord2(struct Maillage maillnodes,struct MaillageBord *maillnodebord,
	       rp_int **nrefac)
{
  rp_int i,j;
  rp_int is1,is2,is3,isad,js1,js2,iso1,iso2,jso1,jso2;
  rp_int nmemax,nelep1,ip,ip1,ipmax,ipmin;
  rp_int nel,neli,nelj,ifac,ifaci,ifacj,ifauxi,ifauxj;
  rp_int nfacbo,nr;
  rp_int *iadr,*itrav;
  rp_int nbface=3,**nvoisin;

  nvoisin=(rp_int**)malloc(nbface*sizeof(rp_int*));
  for (i=0; i<nbface; i++) nvoisin[i]=(rp_int*)malloc(maillnodes.nelem*sizeof(rp_int));
  verif_alloue_int2d(nbface,"extrbord2",nvoisin);

  nmemax =  maillnodes.nelem * 3 + 2 * maillnodes.npoin;
  nelep1 =  maillnodes.nelem + 1;
  ipmin  =  2 * maillnodes.npoin+1;
  ip     =  2 * maillnodes.npoin;
  

  somfac[0][0] = 0; somfac[0][1] = 1;
  somfac[1][0] = 1; somfac[1][1] = 2;
  somfac[2][0] = 2; somfac[2][1] = 0;


  iadr  = (rp_int *)malloc(nmemax * sizeof(rp_int));
  itrav = (rp_int *)malloc(nmemax * sizeof(rp_int));
  verif_alloue_int1d("extrbord2",iadr);
  verif_alloue_int1d("extrbord2",itrav);

  for (i=0; i < nmemax ; i++) *(iadr+i) = -1 ;
  for (i=0; i < nmemax ; i++) *(itrav+i) = 0 ;
  for (j=0; j < nbface; j++)
    for (i=0; i < maillnodes.nelem ; i++) nvoisin[j][i]=-1;


  for ( ifac=0;ifac<3;ifac++)
    for (i=0;i<maillnodes.nelem;i++)
      {
	is1 = maillnodes.node[somfac[ifac][0]][i];
	is2 = maillnodes.node[somfac[ifac][1]][i];
	
	isad = is1+is2;
	
	if(iadr[isad] == -1)
	  {
	    iadr[isad] =i+ifac*nelep1 ;
	    itrav[isad]=isad;
	  }
	else 
	  {
	    ip++; iadr[ip]=i+ifac*nelep1; itrav[ip]=itrav[isad];
	    itrav[isad] = ip;
	  }
      }

  ipmax = ip ;

  for ( i=ipmax;i>=ipmin;i--)
    {
      ifauxi  = iadr[i];
      ifaci   = ifauxi/nelep1;
      neli    = ifauxi-ifaci*nelep1;

      if (nvoisin[ifaci][neli] != -1) continue;

      is1 = maillnodes.node[somfac[ifaci][0]][neli];
      is2 = maillnodes.node[somfac[ifaci][1]][neli];

      if (is1<is2){iso1=is1; iso2=is2;}
      else {iso1=is2; iso2=is1;}

      ip1 = i;
      while (ip1 >= ipmin)
	{
	  ip1     = itrav[ip1];
	  ifauxj  = iadr[ip1];
	  ifacj   = ifauxj/nelep1;
	  nelj    = ifauxj-ifacj*nelep1;

	  js1 = maillnodes.node[somfac[ifacj][0]][nelj];
	  js2 = maillnodes.node[somfac[ifacj][1]][nelj];

	  if (js1<js2){jso1=js1; jso2=js2;}
	  else {jso1=js2; jso2=js1;}

	  if (iso1==jso1 && iso2==jso2)
	    {
	      nvoisin[ifaci][neli] = nelj;
	      nvoisin[ifacj][nelj] = neli;
	      continue ;
	    }
	}

    }

  free(iadr);
  free(itrav);


  maillnodebord->nelem=0;
  nfacbo=0;

  for (ifac=0;ifac<nbface;ifac++)
    for (nel=0;nel<maillnodes.nelem;nel++)
      if ( nvoisin[ifac][nel] == -1 ) nfacbo++;
  
  maillnodebord->nelem=nfacbo;
  maillnodebord->ndim=2;
  maillnodebord->ndmat=2;
  maillnodebord->ndiele=1;
  maillnodebord->nrefe=(rp_int*)malloc(maillnodebord->nelem*sizeof(rp_int));
  maillnodebord->node=(rp_int**)malloc((maillnodebord->ndmat+1)*sizeof(rp_int*));
  for (i=0;i<maillnodebord->ndmat+1;i++) 
    maillnodebord->node[i]=(rp_int*)malloc(maillnodebord->nelem*sizeof(rp_int));
  verif_alloue_int1d("extrbord2",maillnodebord->nrefe);
  verif_alloue_int2d(maillnodebord->ndmat+1,"extrbord2",maillnodebord->node);


  nfacbo=0;
  for (ifac=0;ifac<nbface;ifac++)
    for (i=0;i<maillnodes.nelem;i++)
      if ( nvoisin[ifac][i] == -1 )
	{
	  is1 = maillnodes.node[somfac[ifac][0]][i]; 
	  is2 = maillnodes.node[somfac[ifac][1]][i]; 
	  maillnodebord->node[0][nfacbo]=is1;
	  maillnodebord->node[1][nfacbo]=is2;
	  maillnodebord->node[2][nfacbo]=i;
	  nr=nrefac[ifac][i]; 
	  maillnodebord->nrefe[nfacbo]=nr;
	  nfacbo++;
	}

  printf("\n *** EXTRBORD2 : nombre d'elements du maillage de bord : %d",maillnodebord->nelem);


/*   if (affich.cond_creemaill) */
/*     { */
/*       printf("\n *** EXTRBORD2 : impression de nodebord\n"); */
/*       imprime_connectivite(*maillnodebord);  */
/*     } */

  /*  if (affich.cond_creemaill)
      {
      printf("\n >>> extrbord2 : impression de nvoisin (ex- nfabor)\n");
      for (i=0;i<maillnodes.nelem;i++) 
	{
	printf("\n element %d voisin ",i);
	for (j=0;j<nbface;j++) printf(" %d ",nvoisin[j][i]);
	}
	} 
  */

  for (i=0; i<nbface; i++) free(nvoisin[i]);
  free(nvoisin);
}

/*|======================================================================|
  | SYRTHES 4.3                                        COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Extraction du maillage de bord                                       |
  |======================================================================| */

void extrbord3(struct Maillage maillnodes,struct MaillageBord *maillnodebord,
	       rp_int **nrefac)
{
  rp_int i,j;
  rp_int is1,is2,is3,is4,is5,is6,isad,js1,js2,js3;
  rp_int iso1,iso2,iso3,jso1,jso2,jso3;
  rp_int nmemax,nelep1,ip,ip1,nr;
  rp_int ipmax,ipmin;
  rp_int nel,neli,nelj;
  rp_int ifac,ifaci,ifacj;
  rp_int ifauxi,ifauxj;
  rp_int nfacbo;
  rp_int *iadr,*itrav;
  rp_int nbface=4,**nvoisin;

  nvoisin=(rp_int**)malloc(nbface*sizeof(rp_int*));
  for (i=0; i<nbface; i++) nvoisin[i]=(rp_int*)malloc(maillnodes.nelem*sizeof(rp_int));
  /* bizarre chris  verif_alloue_int2d(maillnodebord->nbface,"extrbord3",nvoisin); */
  verif_alloue_int2d(nbface,"extrbord3",nvoisin); 

  nmemax = maillnodes.nelem * 4 + 3 *maillnodes.npoin;
  nelep1 = maillnodes.nelem + 1;
  ipmin  = 3*maillnodes.npoin+1;
  ip     = 3*maillnodes.npoin;
  

  somfac[0][0]=0; somfac[0][1]=1; somfac[0][2]=2;
  somfac[1][0]=0; somfac[1][1]=3; somfac[1][2]=1;
  somfac[2][0]=0; somfac[2][1]=2; somfac[2][2]=3;
  somfac[3][0]=1; somfac[3][1]=3; somfac[3][2]=2;


  iadr  = (rp_int *)malloc(nmemax * sizeof(rp_int));
  itrav = (rp_int *)malloc(nmemax * sizeof(rp_int));
  verif_alloue_int1d("extrbord3",iadr);
  verif_alloue_int1d("extrbord3",itrav);

  for (i=0; i < nmemax ; i++) *(iadr+i) = -1 ;
  for (i=0; i < nmemax ; i++) *(itrav+i) = 0 ;
  for (j=0; j < nbface; j++)
    for (i=0; i < maillnodes.nelem ; i++) nvoisin[j][i]=-1;


  for ( ifac=0;ifac<4;ifac++)
    {
    
      for ( i=0;i<maillnodes.nelem;i++)
	{
	  is1 = maillnodes.node[somfac[ifac][0]][i]; 
	  is2 = maillnodes.node[somfac[ifac][1]][i]; 
	  is3 = maillnodes.node[somfac[ifac][2]][i]; 

	  isad = is1+is2+is3;

	  if(iadr[isad] == -1)
	    {
	      iadr[isad] = i + ifac*nelep1;
	      itrav[isad]= isad;
	    }
	  else 
	    {
	      ip++;  iadr[ip]=i+ifac*nelep1; itrav[ip]=itrav[isad];
	      itrav[isad] = ip;
	    }
	}
    }

  ipmax = ip ;

  for ( i=ipmax;i>=ipmin;i--)
    {
      ifauxi  = iadr[i];
      ifaci   = ifauxi/nelep1;
      neli    = ifauxi-ifaci*nelep1;


      if (nvoisin[ifaci][neli] != -1) continue;

      is1 = maillnodes.node[somfac[ifaci][0]][neli];
      is2 = maillnodes.node[somfac[ifaci][1]][neli];
      is3 = maillnodes.node[somfac[ifaci][2]][neli];

      if ( is1 <= is2 && is1 <= is3) 
	{
	  if (is2 <= is3) { iso1=is1; iso2=is2; iso3=is3;}
	  else            { iso1=is1; iso2=is3; iso3=is2;}
	}
      else if ( is2 <= is1 && is2 <= is3) 
	{
	  if (is1 <= is3) { iso1=is2; iso2=is1; iso3=is3;}
	  else            { iso1=is2; iso2=is3; iso3=is1;}
	}
      else
	{
	  if (is1 <= is2) { iso1=is3; iso2=is1; iso3=is2;}
	  else            { iso1=is3; iso2=is2; iso3=is1;}
	}

      ip1 = i;
      while ( ip1 >= ipmin )
	{
	  ip1     = itrav[ip1];
	  ifauxj  = iadr[ip1];
	  ifacj   = ifauxj/nelep1;
	  nelj    = ifauxj-ifacj*nelep1;

	  js1 = maillnodes.node[somfac[ifacj][0]][nelj];
	  js2 = maillnodes.node[somfac[ifacj][1]][nelj];
	  js3 = maillnodes.node[somfac[ifacj][2]][nelj];

	  if ( js1 <= js2 && js1 <= js3) 
	    {
	      if (js2 <= js3) { jso1=js1; jso2=js2; jso3=js3;}
	      else            { jso1=js1; jso2=js3; jso3=js2;}
	    }
	  else if ( js2 <= js1 && js2 <= js3) 
	    {
	      if (js1 <= js3) { jso1=js2; jso2=js1; jso3=js3;}
	      else            { jso1=js2; jso2=js3; jso3=js1;}
	    }
	  else
	    {
	      if (js1 <= js2) { jso1=js3; jso2=js1; jso3=js2;}
	      else            { jso1=js3; jso2=js2; jso3=js1;}
	    }

	  if ( iso1 == jso1 &&  iso2 == jso2 &&  iso3 == jso3)
	    {
	      nvoisin[ifaci][neli] = nelj;
	      nvoisin[ifacj][nelj] = neli;
	      continue ;
	    }
	}

    }

  free(iadr);
  free(itrav);


  nfacbo=0;

  for (ifac=0;ifac<nbface;ifac++)
    for (nel=0;nel<maillnodes.nelem;nel++)
      if ( nvoisin[ifac][nel] == -1 ) nfacbo++;

  maillnodebord->nelem=nfacbo;
  maillnodebord->ndim=3;
  maillnodebord->ndmat=3;
  maillnodebord->ndiele=2;
  maillnodebord->nrefe=(rp_int*)malloc(maillnodebord->nelem*sizeof(rp_int));
  maillnodebord->node=(rp_int**)malloc((maillnodebord->ndmat+1)*sizeof(rp_int*));
  for (i=0;i<maillnodebord->ndmat+1;i++) maillnodebord->node[i]=(rp_int*)malloc(maillnodebord->nelem*sizeof(rp_int));
  verif_alloue_int1d("extrbord3",maillnodebord->nrefe);
  verif_alloue_int2d(maillnodebord->ndmat+1,"extrbord3",maillnodebord->node);

  nfacbo=0;
  for (ifac=0;ifac<nbface;ifac++)
    for (i=0;i<maillnodes.nelem;i++)
      {
      if ( nvoisin[ifac][i] == -1 )
	{
	  is1 = maillnodes.node[somfac[ifac][0]][i]; 
	  is2 = maillnodes.node[somfac[ifac][1]][i]; 
	  is3 = maillnodes.node[somfac[ifac][2]][i];
	  maillnodebord->node[0][nfacbo]=is1;
	  maillnodebord->node[1][nfacbo]=is2;
	  maillnodebord->node[2][nfacbo]=is3;
	  maillnodebord->node[3][nfacbo]=i;
	  nr=nrefac[ifac][i]; maillnodebord->nrefe[nfacbo]=nr;
	  nfacbo++;
	}
      }

  for (i=0; i<nbface; i++) free(nvoisin[i]);
  free(nvoisin);

  printf("\n *** EXTRBORD3 : nombre d'elements du maillage de bord : %d",maillnodebord->nelem);
/*    imprime_connectivite(*maillnodebord); */

}
/*|======================================================================|
  | SYRTHES 4.3                                        COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Extraction des voisins                                               |
  |======================================================================| */

void extrvois2(struct Maillage maillnodes,rp_int **nvoisin)
{
  rp_int i,j;
  rp_int is1,is2,is3,isad,js1,js2,iso1,iso2,jso1,jso2;
  rp_int nmemax,nelep1,ip,ip1,ipmax,ipmin;
  rp_int nel,neli,nelj,ifac,ifaci,ifacj,ifauxi,ifauxj;
  rp_int nfacbo,nr;
  rp_int *iadr,*itrav,nbface=3;
  

  nmemax =  maillnodes.nelem * 3 + 2 * maillnodes.npoin;
  nelep1 =  maillnodes.nelem + 1;
  ipmin  =  2 * maillnodes.npoin+1;
  ip     =  2 * maillnodes.npoin;
  

  somfac[0][0] = 0; somfac[0][1] = 1;
  somfac[1][0] = 1; somfac[1][1] = 2;
  somfac[2][0] = 2; somfac[2][1] = 0;


  iadr  = (rp_int *)malloc(nmemax * sizeof(rp_int));
  itrav = (rp_int *)malloc(nmemax * sizeof(rp_int));
  verif_alloue_int1d("xmaillbord2",iadr);
  verif_alloue_int1d("xmaillbord2",itrav);


  for (i=0; i < nmemax ; i++) *(iadr+i) = -1 ;
  for (i=0; i < nmemax ; i++) *(itrav+i) = 0 ;
  for (j=0; j < nbface; j++)
    for (i=0; i < maillnodes.nelem ; i++) nvoisin[j][i]=-1;


  for ( ifac=0;ifac<3;ifac++)
    for (i=0;i<maillnodes.nelem;i++)
      {
	is1 = maillnodes.node[somfac[ifac][0]][i];
	is2 = maillnodes.node[somfac[ifac][1]][i];
	
	isad = is1+is2;
	
	if(iadr[isad] == -1)
	  {
	    iadr[isad] =i+ifac*nelep1 ;
	    itrav[isad]=isad;
	  }
	else 
	  {
	    ip++; iadr[ip]=i+ifac*nelep1; itrav[ip]=itrav[isad];
	    itrav[isad] = ip;
	  }
      }

  ipmax = ip ;

  for ( i=ipmax;i>=ipmin;i--)
    {
      ifauxi  = iadr[i];
      ifaci   = ifauxi/nelep1;
      neli    = ifauxi-ifaci*nelep1;

      if (nvoisin[ifaci][neli] != -1) continue;

      is1 = maillnodes.node[somfac[ifaci][0]][neli];
      is2 = maillnodes.node[somfac[ifaci][1]][neli];

      if (is1<is2){iso1=is1; iso2=is2;}
      else {iso1=is2; iso2=is1;}

      ip1 = i;
      while (ip1 >= ipmin)
	{
	  ip1     = itrav[ip1];
	  ifauxj  = iadr[ip1];
	  ifacj   = ifauxj/nelep1;
	  nelj    = ifauxj-ifacj*nelep1;

	  js1 = maillnodes.node[somfac[ifacj][0]][nelj];
	  js2 = maillnodes.node[somfac[ifacj][1]][nelj];

	  if (js1<js2){jso1=js1; jso2=js2;}
	  else {jso1=js2; jso2=js1;}

	  if (iso1==jso1 && iso2==jso2)
	    {
	      nvoisin[ifaci][neli] = nelj;
	      nvoisin[ifacj][nelj] = neli;
	      continue ;
	    }
	}

    }

  free(iadr);
  free(itrav);

/*   if (affich.ray_extrbord) */
/*     { */
/*       printf("\n >>> extrvois2 : impression de nvoisin (ex- nfabor)\n"); */
/*       for (i=0;i<maillnodes.nelem;i++)  */
/* 	{ */
/* 	  printf("\n element %d voisin ",i); */
/* 	  for (j=0;j<nbface;j++) printf(" %d ",nvoisin[j][i]); */
/* 	} */
/*     }  */
}

/*|======================================================================|
  | SYRTHES 4.3                                        COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Extraction des voisins                                               |
  |======================================================================| */
void extrvois3(struct Maillage maillnodes,rp_int **nvoisin)
{
  rp_int i,j;
  rp_int is1,is2,is3,is4,is5,is6,isad,js1,js2,js3;
  rp_int iso1,iso2,iso3,jso1,jso2,jso3;
  rp_int nmemax,nelep1,ip,ip1,nr;
  rp_int ipmax,ipmin;
  rp_int nel,neli,nelj;
  rp_int ifac,ifaci,ifacj;
  rp_int ifauxi,ifauxj;
  rp_int nfacbo; 
  rp_int *iadr,*itrav,nbface=4;
  
  nmemax = maillnodes.nelem * 4 + 3 *maillnodes.npoin;
  nelep1 = maillnodes.nelem + 1;
  ipmin  = 3*maillnodes.npoin+1;
  ip     = 3*maillnodes.npoin;
  

  somfac[0][0]=0; somfac[0][1]=1; somfac[0][2]=2;
  somfac[1][0]=0; somfac[1][1]=3; somfac[1][2]=1;
  somfac[2][0]=0; somfac[2][1]=2; somfac[2][2]=3;
  somfac[3][0]=1; somfac[3][1]=3; somfac[3][2]=2;


  iadr  = (rp_int *)malloc(nmemax * sizeof(rp_int));
  itrav = (rp_int *)malloc(nmemax * sizeof(rp_int));
  verif_alloue_int1d("xmaillbord3",iadr);
  verif_alloue_int1d("xmaillbord3",itrav);


  for (i=0; i < nmemax ; i++) *(iadr+i) = -1 ;
  for (i=0; i < nmemax ; i++) *(itrav+i) = 0 ;
  for (j=0; j < nbface; j++)
    for (i=0; i < maillnodes.nelem ; i++) nvoisin[j][i]=-1;


  for ( ifac=0;ifac<4;ifac++)
    {
    
      for ( i=0;i<maillnodes.nelem;i++)
	{
	  is1 = maillnodes.node[somfac[ifac][0]][i]; 
	  is2 = maillnodes.node[somfac[ifac][1]][i]; 
	  is3 = maillnodes.node[somfac[ifac][2]][i]; 

	  isad = is1+is2+is3;

	  if(iadr[isad] == -1)
	    {
	      iadr[isad] = i + ifac*nelep1;
	      itrav[isad]= isad;
	    }
	  else 
	    {
	      ip++;  iadr[ip]=i+ifac*nelep1; itrav[ip]=itrav[isad];
	      itrav[isad] = ip;
	    }
	}
    }

  ipmax = ip ;

  for ( i=ipmax;i>=ipmin;i--)
    {
      ifauxi  = iadr[i];
      ifaci   = ifauxi/nelep1;
      neli    = ifauxi-ifaci*nelep1;


      if (nvoisin[ifaci][neli] != -1) continue;

      is1 = maillnodes.node[somfac[ifaci][0]][neli];
      is2 = maillnodes.node[somfac[ifaci][1]][neli];
      is3 = maillnodes.node[somfac[ifaci][2]][neli];

      if ( is1 <= is2 && is1 <= is3) 
	{
	  if (is2 <= is3) { iso1=is1; iso2=is2; iso3=is3;}
	  else            { iso1=is1; iso2=is3; iso3=is2;}
	}
      else if ( is2 <= is1 && is2 <= is3) 
	{
	  if (is1 <= is3) { iso1=is2; iso2=is1; iso3=is3;}
	  else            { iso1=is2; iso2=is3; iso3=is1;}
	}
      else
	{
	  if (is1 <= is2) { iso1=is3; iso2=is1; iso3=is2;}
	  else            { iso1=is3; iso2=is2; iso3=is1;}
	}

      ip1 = i;
      while ( ip1 >= ipmin )
	{
	  ip1     = itrav[ip1];
	  ifauxj  = iadr[ip1];
	  ifacj   = ifauxj/nelep1;
	  nelj    = ifauxj-ifacj*nelep1;

	  js1 = maillnodes.node[somfac[ifacj][0]][nelj];
	  js2 = maillnodes.node[somfac[ifacj][1]][nelj];
	  js3 = maillnodes.node[somfac[ifacj][2]][nelj];

	  if ( js1 <= js2 && js1 <= js3) 
	    {
	      if (js2 <= js3) { jso1=js1; jso2=js2; jso3=js3;}
	      else            { jso1=js1; jso2=js3; jso3=js2;}
	    }
	  else if ( js2 <= js1 && js2 <= js3) 
	    {
	      if (js1 <= js3) { jso1=js2; jso2=js1; jso3=js3;}
	      else            { jso1=js2; jso2=js3; jso3=js1;}
	    }
	  else
	    {
	      if (js1 <= js2) { jso1=js3; jso2=js1; jso3=js2;}
	      else            { jso1=js3; jso2=js2; jso3=js1;}
	    }

	  if ( iso1 == jso1 &&  iso2 == jso2 &&  iso3 == jso3)
	    {
	      nvoisin[ifaci][neli] = nelj;
	      nvoisin[ifacj][nelj] = neli;
	      continue ;
	    }
	}

    }

  free(iadr);
  free(itrav);


/*   if (affich.ray_extrbord) */
/*     { */
/*       printf("\n >>> extrvois3 : impression de nvoisin (ex- nfabor)\n"); */
/*       for (i=0;i<maillnodes.nelem;i++)  */
/* 	{ */
/* 	  printf("\n element %d voisin ",i); */
/* 	  for (j=0;j<nbface;j++) printf(" %d ",nvoisin[j][i]); */
/* 	} */
/*     }  */

}

   
/*|======================================================================|
  | SYRTHES PARALLELE                                  JANV 07           |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | METIS  (Decomposition d'un maillage 2D/3D) :                         |
  |                                                                      |
  |    Creation du partitionnement                                       |
  |                                                                      |
  |======================================================================|*/
void pp_decoupbord(struct Maillage maillnodes, struct MaillageBord maillnodebord,
		   rp_int *numdome, rp_int *numdomebord)
{
  rp_int n,i,j,nf,isom,isomv,ok,np,s,ne,trouve;
  rp_int somfac[4][3];
  rp_int *noeudbord;
  rp_int *toto;

  struct NodeEle{
    rp_int num;
    struct NodeEle *suivant;
  };
  struct NodeEle **tnodele,*p,*q;
  
  if (SYRTHES_LANG == FR) {
    printf("\n *** DECOUPAGE DU MAILLAGE DE BORD\n");
    printf("             nombre d'elements de bord : %d\n",maillnodebord.nelem);
  }
  else if (SYRTHES_LANG == EN) {
    printf("\n *** PARTITIONNING OF THE SURFACIC MESH\n");
    printf("             number of surfacic elements : %d\n",maillnodebord.nelem);
  }

  fflush(stdout);

  if (maillnodes.ndim==2)
    {
      somfac[0][0] = 0; somfac[0][1] = 1;
      somfac[1][0] = 1; somfac[1][1] = 2;
      somfac[2][0] = 2; somfac[2][1] = 0;
    }
  else
    {
      somfac[0][0]=0; somfac[0][1]=1; somfac[0][2]=2;
      somfac[1][0]=0; somfac[1][1]=3; somfac[1][2]=1;
      somfac[2][0]=0; somfac[2][1]=2; somfac[2][2]=3;
      somfac[3][0]=1; somfac[3][1]=3; somfac[3][2]=2;
    }



  /* rq, pour l'instant, on cree des listes sur npoin.
     on pourra ensuite optimiser et compter les noeuds de bord
     et faire des listes locales */
 
  /* identification des noeuds de bord */
  /* ------------------------------------------------------- */
  noeudbord=(rp_int*)malloc(maillnodes.npoin*sizeof(rp_int));
  for (i=0;i<maillnodes.npoin;i++)noeudbord[i]=0;

  for (i=0;i<maillnodebord.nelem;i++)
    for (j=0;j<maillnodebord.ndmat;j++)
      noeudbord[maillnodebord.node[j][i]]=1;


  /* pour chaque noeud (de bord), liste des elts qui lui sont attaches */
  /* ----------------------------------------------------------------- */
  tnodele=(struct NodeEle**)malloc(maillnodes.npoin*sizeof(struct NodeEle*)); /* tableau de nodele */
  for (i=0;i<maillnodes.npoin;i++) {
    tnodele[i]=(struct NodeEle*)malloc(sizeof(struct NodeEle));
    tnodele[i]->num=-1; tnodele[i]->suivant=NULL;
  }

  for (i=0;i<maillnodes.nelem;i++)
    for (j=0;j<maillnodes.ndmat;j++)
      {
	np=maillnodes.node[j][i];
	if (noeudbord[np])
	  {
	    if (tnodele[np]->num==-1)
	      {
		tnodele[np]->num=i;
		tnodele[np]->suivant=NULL;
	      }
	    else
	      {
		p=tnodele[np];
		s=0;if (p->num==i) s++;
		while(p->suivant){
		  p=p->suivant;
		  if (p->num==i) s++; 
		}
		if (s==0){
		  p->suivant=(struct NodeEle*)malloc(sizeof(struct NodeEle));
		  p=p->suivant;
		  p->num=i;
		  p->suivant=NULL;
		}
	      }
	  }
      }


  free(noeudbord);

  /* Impression de controle */
/*   printf(" Pour chaque noeud, liste des elts attaches\n"); */
/*   for (i=0;i<maillnodes.npoin;i++) */
/*     { */
/*       printf("Noeud %d --> parts ",i); */
/*       p=tnodele[i];s=0; */
/*       while(p){ */
/* 	printf("%d ",p->num); */
/* 	p=p->suivant; */
/*       } */
/*       printf("\n"); */
/*     } */


  for (n=0;n<maillnodebord.nelem;n++) numdomebord[n]=-1;

  for (i=0;i<maillnodebord.nelem;i++)
    {
      trouve=0;
      np=maillnodebord.node[0][i];
      p=tnodele[np];
      while (p && !trouve){
	ne=p->num;
	for (nf=0;nf<maillnodes.nbface;nf++)
	  {
	    if (maillnodes.ndim==2) ok=pp_egalele2d(maillnodes.node[somfac[nf][0]][ne],
						    maillnodes.node[somfac[nf][1]][ne],
						    maillnodebord.node[0][i],
						    maillnodebord.node[1][i]);
	    else ok=pp_egalele3d(maillnodes.node[somfac[nf][0]][ne],
				 maillnodes.node[somfac[nf][1]][ne],
				 maillnodes.node[somfac[nf][2]][ne],
				 maillnodebord.node[0][i],
				 maillnodebord.node[1][i],
				 maillnodebord.node[2][i]);
	    if (ok) {
	      numdomebord[i]=numdome[ne];
	      trouve=1;
	      break;
	    }
	  }
	p=p->suivant;
      }
    }


  /* on libere la memoire */
  for (i=0;i<maillnodes.npoin;i++) 
    {
      p=tnodele[i];
      while(p){
	q=p;
	p=p->suivant;
 	free(q); 
      }
    }
  free(tnodele);

  /* impressions de controle */
/*   printf("\n Numero de partition des elements du maillage de bord\n"); */
/*   for (n=0;n<maillnodebord.nelem;n++) */
/*     printf("             elt %d --> part %d\n",n,numdomebord[n]); */



}

/*|======================================================================|
  | SYRTHES PARALLELE                                  JANV 07           |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | PP_EGALELE2D                                                         |
  |                                                                      |
  |    test l'identite de 2 elt en 2D                                    |
  |                                                                      |
  |======================================================================|*/
rp_int pp_egalele2d(rp_int nv1,rp_int nv2,rp_int nb1,rp_int nb2)
{
  if ( (nv1==nb1 && nv2==nb2) || (nv1==nb2 && nv2==nb1) )
    return 1;
  else
    return 0;
}
/*|======================================================================|
  | SYRTHES PARALLELE                                  JANV 07           |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | PP_EGALELE2D                                                         |
  |                                                                      |
  |    test l'identite de 2 elt en 3D                                    |
  |                                                                      |
  |======================================================================|*/
rp_int pp_egalele3d(rp_int nv0,rp_int nv1,rp_int nv2,rp_int nb0,rp_int nb1,rp_int nb2)
{
  rp_int nv[3],nb[3];

  if (nv0<nv1)
    {
      if (nv2<nv0)
	{nv[0]=nv2; nv[1]=nv0 ;nv[2]=nv1;}
      else if(nv2>nv1)
	{nv[0]=nv0; nv[1]=nv1 ;nv[2]=nv2;} 
      else
	{nv[0]=nv0; nv[1]=nv2 ;nv[2]=nv1;} 

    }
  else
    {
      if (nv2<nv1)
	{nv[0]=nv2; nv[1]=nv1 ;nv[2]=nv0;} 
      else if(nv2>nv0)
	{nv[0]=nv1; nv[1]=nv0 ;nv[2]=nv2;}
      else
	{nv[0]=nv1; nv[1]=nv2 ;nv[2]=nv0;} 
    }

  if (nb0<nb1)
    {
      if (nb2<nb0)
	{nb[0]=nb2; nb[1]=nb0 ;nb[2]=nb1;}
      else if(nb2>nb1)
	{nb[0]=nb0; nb[1]=nb1 ;nb[2]=nb2;} 
      else
	{nb[0]=nb0; nb[1]=nb2 ;nb[2]=nb1;} 

    }
  else
    {
      if (nb2<nb1)
	{nb[0]=nb2; nb[1]=nb1 ;nb[2]=nb0;} 
      else if(nb2>nb0)
	{nb[0]=nb1; nb[1]=nb0 ;nb[2]=nb2;}
      else
	{nb[0]=nb1; nb[1]=nb2 ;nb[2]=nb0;} 
    }

  return (nv[0]==nb[0] && nv[1]==nb[1] && nv[2]==nb[2]);
            

}
/*|======================================================================|
  | SYRTHES PARALLELE                                  JANV 07           |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | GATEAU  (Decomposition d'un maillage 2D/3D) :                        |
  |                                                                      |
  |    Creation du partitionnement                                       |
  |               methode radicale : on coupe en nparts comme un gateau !|
  |                                                                      |
  |======================================================================|*/
void pp_gateau(rp_int nelems,rp_int nparts,rp_int *numdome)
{
  rp_int n,i,nb;

  nb=nelems/nparts;

  for (n=0;n<nparts;n++)
    for (i=n*nb;i<(n+1)*nb;i++)
      numdome[i]=n;

  for (i=nb*nparts;i<nelems;i++) numdome[i]=nparts-1;
}
  
  
  
