GHashTable* vsd_parse_cmd_file(void);
int vsd_parse_cmd_test(void);
int vsd_parse_chunk_old(GByteArray* stream, FILE *fstrm, char *dirname, GHashTable *vaetbls, int *offset, int *num, int version);
gboolean vsd_cmd_tbl_free(gpointer key, gpointer value, gpointer user_data);

typedef struct{
	guint8	type;
	guint32	offset;
	char		**name;
} VsdArgEntry;
