/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <string.h>

# include "post_usertype.h"
# include "post_bd.h"
# include "post_proto.h"


/*|======================================================================|
  | SYRTHES 4.3                2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void ecrire_geom(char *nomfich,
	 	 struct Maillage maillnodes,struct Maillage maillnodebord)
{
  FILE  *fich;
  rp_int i,j,bidon=0;
  rp_int *ne0,*ne1,*ne2,*ne3,*nref,*nrefe,**nodes,**nodebord,*nrefb;
  double z=0,*c0,*c1,*c2,**coords;

  if ((fich=fopen(nomfich,"w")) == NULL)
    {
      if (SYRTHES_LANG == FR)
	printf("Impossible d'ouvrir le fichier %s\n",nomfich);
      else if (SYRTHES_LANG == EN)
	printf("Impossible to opn the file %s\n",nomfich);
      exit(1);
    }

  nodes=maillnodes.node;
  coords=maillnodes.coord;
  nref=maillnodes.nref;
  nrefe=maillnodes.nrefe;
  nodebord=maillnodebord.node;
  nrefb=maillnodebord.nrefe;



  fprintf(fich,"C*V4.0*******************************************C\n");
  fprintf(fich,"C            FICHIER GEOMETRIQUE SYRTHES         C\n");
  fprintf(fich,"C************************************************C\n");
  fprintf(fich,"C  DIMENSION = %1d\n",maillnodes.ndim);
  fprintf(fich,"C  DIMENSION DES ELTS = %1d\n",maillnodes.ndim);
  fprintf(fich,"C  NOMBRE DE NOEUDS = %12d\n",maillnodes.npoin);
  fprintf(fich,"C  NOMBRE D'ELEMENTS =%12d\n",maillnodes.nelem);
  fprintf(fich,"C  NOMBRE D'ELEMENTS DE BORD =%12d\n",maillnodebord.nelem);
  fprintf(fich,"C  NOMBRE DE NOEUDS PAR ELEMENT = %3d\n",maillnodes.ndmat);
  fprintf(fich,"C************************************************C\n");

  fprintf(fich,"C\nC$ RUBRIQUE = NOEUDS\nC\n");
  if (maillnodes.ndim==2)
    for (i=0,c0=*coords,c1=*(coords+1);i<maillnodes.npoin;i++,c0++,c1++) 
      fprintf(fich,"%10d%3d %14.7e %14.7e %14.7e \n", i+1,*(nref+i),*c0,*c1,z);
  else
    for (i=0,c0=*coords,c1=*(coords+1),c2=*(coords+2);i<maillnodes.npoin;i++,c0++,c1++,c2++) 
      fprintf(fich,"%10d%3d %14.7e %14.7e %14.7e \n",i+1,*(nref+i),*c0,*c1,*c2);



  fprintf(fich,"C\nC$ RUBRIQUE = ELEMENTS\nC\n");
  if (maillnodes.ndmat==2)
    for (i=0,ne0=*(nodes),ne1=*(nodes+1);i<maillnodes.nelem;i++,ne0++,ne1++) 
      fprintf(fich,"%12d%3d%10d%10d\n",i+1,*(nrefe+i),*ne0+1,*ne1+1);

  else if (maillnodes.ndmat==3)
    for (i=0,ne0=*(nodes),ne1=*(nodes+1),ne2=*(nodes+2);i<maillnodes.nelem;i++,ne0++,ne1++,ne2++) 
      fprintf(fich,"%12d%3d%10d%10d%10d\n",i+1,*(nrefe+i),*ne0+1,*ne1+1,*ne2+1);

  else if (maillnodes.ndmat==4)
    for (i=0,ne0=*(nodes),ne1=*(nodes+1),ne2=*(nodes+2),ne3=*(nodes+3);
	 i<maillnodes.nelem;i++,ne0++,ne1++,ne2++,ne3++) 
      fprintf(fich,"%12d%3d%10d%10d%10d%10d\n",i+1,*(nrefe+i),*ne0+1,*ne1+1,*ne2+1,*ne3+1);

  else 
    {
      if (SYRTHES_LANG == FR)
	printf("\n ERREUR ecrire_geom : type d'elements inconnus\n");
      else if (SYRTHES_LANG == EN)
	printf("\n ERROR ecrire_geom : unknown element type\n");
      exit(1);
    }


  fprintf(fich,"C\nC$ RUBRIQUE = ELEMENTS DE BORD\nC\n");
  if (maillnodebord.ndmat==2)
    for (i=0,ne0=*(nodebord),ne1=*(nodebord+1);i<maillnodebord.nelem;i++,ne0++,ne1++) 
      fprintf(fich,"%12d%3d%10d%10d\n",i+1,*(nrefb+i),*ne0+1,*ne1+1);

  else if (maillnodebord.ndmat==3)
    for (i=0,ne0=*(nodebord),ne1=*(nodebord+1),ne2=*(nodebord+2);i<maillnodebord.nelem;i++,ne0++,ne1++,ne2++) 
      fprintf(fich,"%12d%3d%10d%10d%10d\n",i+1,*(nrefb+i),*ne0+1,*ne1+1,*ne2+1);
  else 
    {
      if (SYRTHES_LANG == FR)
	printf("\n ERREUR ecrire_geom : type d'elements de bord inconnus\n");
      else if (SYRTHES_LANG == EN)
	printf("\n ERROR ecrire_geom : unknown boundary element type\n");
      exit(1);
    }
   

}




/*|======================================================================|
  | SYRTHES 4.3                2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void ecrire_entete(FILE  *fich,rp_int ntsyr,double rdtts,double tempss)
{
  rp_int nelebo;
  char *version,*date,*titre;
 
 
  titre="CALCUL SYRTHES";

  fprintf(fich,"***SYRTHES V4.0**************************************************************************************\n");
  fprintf(fich,"***%s\n",titre);
  fprintf(fich,"*****************************************************************************************************\n");
  fprintf(fich,"***NTSYR= %12d  ***TEMPS= %25.17e    ***DT= %25.17e\n",ntsyr,tempss,rdtts);
  fprintf(fich,"*****************************************************************************************************\n");

}

/*|======================================================================|
  | SYRTHES 4.3                2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void ecrire_resu(FILE  *fich,rp_int nbre,
		 double *var,char *nomvar,rp_int idiscr)
{
  rp_int i,nv;

  fprintf(fich,"***VAR= %12s ***TYPE= %1d  ***NB= %12d\n",nomvar,idiscr,nbre);

  nv=nbre/6;  
  for (i=0;i<nv*6;i+=6) fprintf(fich,"%16.9e %16.9e %16.9e %16.9e %16.9e %16.9e\n",
			      *(var+i),*(var+i+1),*(var+i+2),*(var+i+3),*(var+i+4),*(var+i+5));
  for (i=nv*6;i<nbre;i++) fprintf(fich,"%16.9e ",*(var+i));
  if (nbre-nv*6) fprintf(fich,"\n");
}

