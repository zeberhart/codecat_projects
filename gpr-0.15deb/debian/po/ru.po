# translation of ru.po to Russian
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Yuri Kozlov <yuray@komyakino.ru>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: gpr 0.14deb1\n"
"Report-Msgid-Bugs-To: mennucc1@debian.org\n"
"POT-Creation-Date: 2007-01-18 14:02+0100\n"
"PO-Revision-Date: 2009-01-29 20:46+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "divert lpr with a wrapper that points to gpr?"
msgstr "Перевести lpr на обёртку, указывающую на gpr?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"gpr can install a wrapper in place of the command lpr. The wrapper will call "
"the real lpr command if: either it is called from a terminal, or there is no "
"access to an X DISPLAY; otherwise it will call gpr. This wrapper can be "
"useful when printing from graphical programs (such as mozilla or "
"openoffice), so that the users will be able to choose the printer and other "
"printer-related settings."
msgstr ""
"Пакет gpr может установить обёртку, заменяющую команду lpr. Обёртка будет "
"вызывать реальную команду lpr, если она вызывается из терминала, или "
"недоступен X DISPLAY; иначе будет вызываться gpr. Данная обёртка может "
"пригодиться при печати из программ с графическим интерфейсом "
"(например, mozilla или openoffice), позволяя пользователям выбирать принтер "
"и изменять настройки печати."

