/* main.c
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#include "common.h"
#include "launcher.h"
#include <string.h>
#include <stdio.h>

void usage(char *exec)
{
    fprintf(stderr, "usage: %s [--edit] [--config <config file>]\n", exec);
    exit(1);
}

int main(int argc, char **argv)
{
  launcher_t *launcher;
  apwalapp_t *apwal;
  gboolean    launch_editor = FALSE;
  char       *config_file = NULL;

  gtk_init(&argc, &argv);

  if (argc > 1)
  {
    int i;
    for (i = 1; i < argc; i++)
    {
      TRACE();
      if (strstr(argv[i], "--edit") != NULL)
      {
        launch_editor = TRUE;
      }
      else if (strstr(argv[i], "--config") != NULL)
      {
        i++;
        if (i < argc)
        {
          config_file = argv[i];
        }
        else
        {
          usage(argv[0]);
        }
      }
      else
      {
        usage(argv[0]);
      }
    }
  }

  xmlrc_set_resource_file(config_file); // NULL is ok.

  if (!xmlrc_resource_file_exist())
  {
    xmlrc_resource_file_create();
    apwal = apwalapp_new();
  }
  else if (launch_editor)
  {
    apwal = apwalapp_new();
  }
  else
  {
    launcher = launcher_new();
  }
  gtk_main();
  return 0;
}

