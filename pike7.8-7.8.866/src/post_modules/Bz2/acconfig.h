/* $Id: d1030b768400e1fe871387fe7e11a41481815dc8 $ */

/* Define if you have a working libbz2  */
#undef HAVE_BZ2LIB

/* Define if your bz_stream has the field total_out */
#undef HAVE_BZ_STREAM_TOTAL_OUT
