From: Dominik Haumann <dhaumann@kde.org>
Date: Sat, 7 Jan 2017 12:15:39 +0100
Subject: Fix: Forward dragging text results in wrong selection

Thanks to Serge Roussak for the patch!

REVIEW: 129758
BUG: 374163
---
 autotests/src/kateview_test.cpp | 67 +++++++++++++++++++++++++++++++++++++++++
 autotests/src/kateview_test.h   |  2 ++
 src/view/kateviewinternal.cpp   | 12 +++++---
 3 files changed, 77 insertions(+), 4 deletions(-)

diff --git a/autotests/src/kateview_test.cpp b/autotests/src/kateview_test.cpp
index 950184f4..7372d235 100644
--- a/autotests/src/kateview_test.cpp
+++ b/autotests/src/kateview_test.cpp
@@ -317,4 +317,71 @@ void KateViewTest::testFoldFirstLine()
     QVERIFY(view->textFolding().isLineVisible(1));
 }
 
+// test for bug https://bugs.kde.org/374163
+void KateViewTest::testDragAndDrop()
+{
+    KTextEditor::DocumentPrivate doc(false, false);
+    doc.setText("line0\n"
+                "line1\n"
+                "line2\n"
+                "\n"
+                "line4");
+
+    KTextEditor::View* view = static_cast<KTextEditor::View*>(doc.createView(Q_NULLPTR));
+    view->show();
+    view->resize(400, 300);
+
+    QWidget *internalView = nullptr;
+    foreach (QObject* child, view->children()) {
+        if (child->metaObject()->className() == QByteArrayLiteral("KateViewInternal")) {
+            internalView = qobject_cast<QWidget *>(child);
+            break;
+        }
+    }
+    QVERIFY(internalView);
+
+    // select "line1\n"
+    view->setSelection(Range(1, 0, 2, 0));
+    QCOMPARE(view->selectionRange(), Range(1, 0, 2, 0));
+
+    QTest::qWaitForWindowExposed(view);
+    const QPoint startDragPos = internalView->mapFrom(view, view->cursorToCoordinate(KTextEditor::Cursor(1, 2)));
+    const QPoint endDragPos = internalView->mapFrom(view, view->cursorToCoordinate(KTextEditor::Cursor(3, 0)));
+    const QPoint gStartDragPos = internalView->mapToGlobal(startDragPos);
+    const QPoint gEndDragPos = internalView->mapToGlobal(endDragPos);
+
+    // now drag and drop selected text to Cursor(3, 0)
+    QMouseEvent pressEvent(QEvent::MouseButtonPress, startDragPos, gStartDragPos,
+                                Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
+    QCoreApplication::sendEvent(internalView, &pressEvent);
+
+    // ugly workaround: Drag & Drop has own blocking event queue. Therefore, we need a single-shot timer to
+    // break out of the blocking event queue, see (*)
+    QTimer::singleShot(50, [&](){
+        QMouseEvent moveEvent(QEvent::MouseMove, endDragPos + QPoint(5, 0), gEndDragPos + QPoint(5, 0),
+                            Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
+        QMouseEvent releaseEvent(QEvent::MouseButtonRelease, endDragPos, gEndDragPos,
+                            Qt::LeftButton, Qt::NoButton, Qt::NoModifier);
+        QCoreApplication::sendEvent(internalView, &moveEvent);
+        QCoreApplication::sendEvent(internalView, &releaseEvent);
+    });
+
+    // (*) this somehow blocks...
+    QMouseEvent moveEvent1(QEvent::MouseMove, endDragPos + QPoint(10, 0), gEndDragPos + QPoint(10, 0),
+                           Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
+    QCoreApplication::sendEvent(internalView, &moveEvent1);
+
+    QTest::qWait(100);
+
+    // final tests of dragged text
+    QCOMPARE(doc.text(), QString("line0\n"
+                                 "line2\n"
+                                 "line1\n"
+                                 "\n"
+                                 "line4"));
+
+    QCOMPARE(view->cursorPosition(), KTextEditor::Cursor(3, 0));
+    QCOMPARE(view->selectionRange(), Range(2, 0, 3, 0));
+}
+
 // kate: indent-mode cstyle; indent-width 4; replace-tabs on;
diff --git a/autotests/src/kateview_test.h b/autotests/src/kateview_test.h
index 672e74e6..a48720d9 100644
--- a/autotests/src/kateview_test.h
+++ b/autotests/src/kateview_test.h
@@ -43,6 +43,8 @@ private Q_SLOTS:
     void testKillline();
 
     void testFoldFirstLine();
+
+    void testDragAndDrop();
 };
 
 #endif // KATE_VIEW_TEST_H
diff --git a/src/view/kateviewinternal.cpp b/src/view/kateviewinternal.cpp
index 35545cc1..23a1e21e 100644
--- a/src/view/kateviewinternal.cpp
+++ b/src/view/kateviewinternal.cpp
@@ -3241,22 +3241,26 @@ void KateViewInternal::dropEvent(QDropEvent *event)
         doc()->insertText(targetCursor, text, m_view->blockSelection());
 
         KTextEditor::DocumentCursor startCursor(doc(), targetCursor);
+        KTextEditor::DocumentCursor endCursor1(doc(), targetCursor);
+        const int textLength = text.length();
 
         if (event->dropAction() != Qt::CopyAction) {
             m_view->removeSelectedText();
+            if (m_cursor.toCursor() < startCursor.toCursor()) {
+                startCursor.move(-textLength);
+                endCursor1.move(-textLength);
+            }
         }
 
-        KTextEditor::DocumentCursor endCursor1(doc(), startCursor);
-
         if (!m_view->blockSelection()) {
-            endCursor1.move(text.length());
+            endCursor1.move(textLength);
         } else {
             endCursor1.setColumn(startCursor.column() + selectionWidth);
             endCursor1.setLine(startCursor.line() + selectionHeight);
         }
 
         KTextEditor::Cursor endCursor(endCursor1);
-        qCDebug(LOG_KTE) << startCursor << "---(" << text.length() << ")---" << endCursor;
+        qCDebug(LOG_KTE) << startCursor << "---(" << textLength << ")---" << endCursor;
         setSelection(KTextEditor::Range(startCursor, endCursor));
         editSetCursor(endCursor);
 
