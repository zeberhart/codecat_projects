/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*===========================================================================
 * Main API functions for coupling between Syrthes and Code_Saturne
 * AUTHORS  : J. Bonelle, Y Fournier
 *
 * Library: Syrthes                                   Copyright EDF 2008-2014
 *===========================================================================*/

#if defined(_SYRTHES_CFD_)

/*---------------------------------------------------------------------------
 * System and PLE headers
 *---------------------------------------------------------------------------*/

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined(_SYRTHES_MPI_)
#include <mpi.h>
#endif

#include <ple_defs.h>
#include <ple_coupling.h>
#include <ple_locator.h>

#include "syr_cfd_mesh.h"
#include "syr_cfd_point_location.h"
#include "syr_cfd_coupling.h"

/*============================================================================
 * Local macro definitions
 *============================================================================*/

/* Geometric operation macros*/

enum {X, Y, Z};

#define _DOT_PRODUCT(vect1, vect2) \
  (vect1[X] * vect2[X] + vect1[Y] * vect2[Y] + vect1[Z] * vect2[Z])

#define _MODULE(vect) \
  sqrt(vect[X] * vect[X] + vect[Y] * vect[Y] + vect[Z] * vect[Z])

#define _CROSS_PRODUCT(prod_vect, vect1, vect2)  \
  (prod_vect[X] = vect1[Y] * vect2[Z] - vect2[Y] * vect1[Z], \
   prod_vect[Y] = vect2[X] * vect1[Z] - vect1[X] * vect2[Z], \
   prod_vect[Z] = vect1[X] * vect2[Y] - vect2[X] * vect1[Y])

#define _DOT_PRODUCT_2D(vect1, vect2) \
  (vect1[X] * vect2[X] + vect1[Y] * vect2[Y])

/*
 * Macros for future internationalization via gettext() or a similar
 * function (to mark translatable character strings)
 */

#if defined(ENABLE_NLS)

#include <libintl.h>
#define _(String) gettext(String)
#define gettext_noop(String) String
#define N_(String) gettext_noop(String)

#else

#define _(String) String
#define N_(String) String
#define textdomain(Domain)
#define bindtextdomain(Package, Directory)

#endif

/*===========================================================================
 * Structure definitions
 *===========================================================================*/

/* Interpolation helper structure */
/*--------------------------------*/

typedef struct _syr_cfd_interpolation_t {

  ple_locator_t  *locator;             /* Associated locator */

  int             n_element_vertices;  /* Number of vertices per element */

  int             n_coupled_elts;      /* Number of coupled elements */

  int            *coupled_elt_list;    /* List of coupled elements (1 to n) */
  float          *coeffs;              /* Interpolation coefficients */

  const int     **elt_vertices;        /* Pointer to SYRTHES coonectivity */

} syr_cfd_interpolation_t;

/* Coupling helper structure */
/*---------------------------*/

struct _syr_cfd_coupling_t {

  char            *app_name;        /* application name, if given */

  int              dim;             /* Spatial dimension */

#if defined(_SYRTHES_MPI_)
  MPI_Comm         comm;            /* Associated MPI communicator */
#endif

  int              n_dist_ranks;    /* Number of associated distant ranks */
  int              root_dist_rank;  /* First associated distant rank */

  syr_cfd_coupling_type_t   type;
  syr_cfd_interpolation_t  *cell_ip;    /* Interpolation for cells */
  syr_cfd_interpolation_t  *face_ip;    /* Interpolation for faces */

  int              conservativity;  /* Conservativity forcing flag */
  int              allow_nearest;   /* Allow nearest flag */
  double           tolerance[2];    /* Tolerance for location
                                       0: absolute, 1: relative*/

};

/*===========================================================================
 * Static global variables
 *===========================================================================*/

static int  _syr_coupling_loc_rank = 0;

#if defined(_SYRTHES_MPI_)

const int  syr_cfd_coupling_tag = 'C'+'S'+'_'+'C'+'O'+'U'+'P'+'L'+'A'+'G'+'E';

static MPI_Comm _syr_coupling_loc_comm = MPI_COMM_NULL;

static ple_coupling_mpi_set_t *_syr_coupling_mpi_app_world = NULL;

static int _syr_coupling_app_sync_flags = 0;

#endif

/*===========================================================================
 * Private function definitions
 *===========================================================================*/

static void _syr_error_handler
(
 const char     *const filename,
 const int             linenum,
 const int             sys_err_code,
 const char     *const format,
       va_list         arg_ptr
)
{
  FILE *err = stderr;

  fflush(stdout);
  fflush(stderr);

  if (_syr_coupling_loc_rank > 0) {
    char  errname[32];
    sprintf(errname, "syrthes_error_log_%05d", _syr_coupling_loc_rank);
    err = fopen(errname, "w");
  }
  else
    fprintf(stderr, _("\n"
                      "Error during SYRTHES execution\n"
                      "==============================\n"));

  if (err != NULL) {

    if (sys_err_code != 0)
      fprintf(err, _("\nSystem error: %s\n"), strerror(sys_err_code));

    fprintf(err, _("\n%s:%d: Fatal error.\n\n"), filename, linenum);

    fprintf(err, format, arg_ptr);

    fprintf(err, "\n");

    if (_syr_coupling_loc_rank > 0)
      fclose(err);
  }

  assert(0);   /* Use of assert() here allows interception by a debugger. */

#if defined(_SYRTHES_MPI_)
  {
    int mpi_flag;

    MPI_Initialized(&mpi_flag);

    if (mpi_flag != 0)
      MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
  }
#endif /* _SYRTHES_MPI_ */

  exit(EXIT_FAILURE);
}

/*----------------------------------------------------------------------------
 * Print a message to standard output
 *----------------------------------------------------------------------------*/

static int
_syr_ple_printf(const char  *const format,
                va_list      arg_ptr)
{
  int retval = 0;

  if (_syr_coupling_loc_rank == 0)
    retval = vprintf(format, arg_ptr);

  return retval;
}

/*----------------------------------------------------------------------------
 * Exchange synchronization messages between SYRTHES and CFD code.
 *
 * parameters:
 *   coupling      <--  CFD coupling structure
 *   op_name_send  <--  operation name to send, or NULL. Only the 32
 *                      first characters are sent if the nae is longer.
 *   op_name_recv  <--  operation name to receive, or NULL (size: 33)
 *----------------------------------------------------------------------------*/

static void
_exchange_sync(syr_cfd_coupling_t  *coupling,
               const char           op_name_send[33],
               char                 op_name_recv[33])
{
#if defined(_SYRTHES_MPI_)

  if (_syr_coupling_loc_rank == 0) {

    MPI_Status status;

    if (op_name_send != NULL) {

      char _op_name_send[33];
      strncpy(_op_name_send, op_name_send, 32);
      _op_name_send[32] = '\0';


    /* Exchange command messages */
      if (op_name_recv != NULL) {
        MPI_Sendrecv(_op_name_send, 32, MPI_CHAR,
                     coupling->root_dist_rank, syr_cfd_coupling_tag,
                     op_name_recv, 32, MPI_CHAR,
                     coupling->root_dist_rank, syr_cfd_coupling_tag,
                     coupling->comm, &status);
      }

      else
        MPI_Send(_op_name_send, 32, MPI_CHAR,
                 coupling->root_dist_rank, syr_cfd_coupling_tag,
                 coupling->comm);

    }
    else if (op_name_recv != NULL) {
      MPI_Recv(op_name_recv, 32, MPI_CHAR,
               coupling->root_dist_rank, syr_cfd_coupling_tag,
               coupling->comm, &status);
    }

  }

  if (op_name_recv != NULL) {
    MPI_Bcast(op_name_recv, 32, MPI_CHAR, 0, _syr_coupling_loc_comm);
    op_name_recv[32] = '\0';
  }

#endif
}

/*----------------------------------------------------------------------------
 * Exchange of start (supervision) messages
 *
 * parameters:
 *  coupling <-- Associated coupling object
 *  error    <-- 1 if location error has occured previously, 0 otherwise
 *----------------------------------------------------------------------------*/

static void
_coupling_start(syr_cfd_coupling_t  *coupling,
                int                  error)
{
  char  op_name_send[32 + 1];
  char  op_name_recv[32 + 1];

  int verbose = 0;

  memset(op_name_send, 0, 32);

  if (error) {
    strcpy(op_name_send, "coupling:error:location");
    _syr_coupling_app_sync_flags
      = _syr_coupling_app_sync_flags | PLE_COUPLING_STOP;
  }
  else
    strcpy(op_name_send, "coupling:start");

  _exchange_sync(coupling, op_name_send, op_name_recv);

  /* If message is unexpected */

  if (strcmp(op_name_recv, "coupling:start"))  {

    if (strcmp(op_name_recv, "coupling:error:location") == 0)  {

      _syr_coupling_app_sync_flags
        = _syr_coupling_app_sync_flags | PLE_COUPLING_STOP;

      ple_printf(_("Message \"%s\" indicates a mesh matching error.\n"
                   "--> abort."),
                 op_name_recv);
      fflush(stdout);

    }
    else
      ple_error(__FILE__, __LINE__, 0,
                _("Message \"%s\" indicates an error or unexpected.\n"
                  "--> abort."),
                op_name_recv);

  }
}
#if defined(_SYRTHES_MPI_)

/*----------------------------------------------------------------------------
 * Initialize communicator for CFD coupling.
 *
 * parameters:
 *   cfd_coupling  <-> CFD coupling structure
 *   coupling_id   <-- CFD coupling's id/number
 *   cfd_root_rank <-- CFD root rank
 *   n_cfd_ranks   <-- Number of ranks associated with CFD code
 *----------------------------------------------------------------------------*/

static void
_init_mpi_comm(syr_cfd_coupling_t *cfd_coupling,
               int                 coupling_id,
               int                 cfd_root_rank,
               int                 n_cfd_ranks)
{

  int  mpi_flag = 0;
  int  scheme_min = 2;
  int  scheme_max = 2;

  char  volume_flag = ' ';
  char  boundary_flag = ' ';
  char  conserv_flag = ' ';
  char  op_name_send[32 + 1];
  char  op_name_recv[32 + 1];

  cfd_coupling->n_dist_ranks = n_cfd_ranks;
  cfd_coupling->root_dist_rank = cfd_root_rank;

  /* Check that MPI is initialized */

  MPI_Initialized(&mpi_flag);

  if (mpi_flag == 0)
    return;

  /* Create associated communicator */

  ple_printf(_("  coupling %d: initializing MPI communication ...\n"),
             coupling_id);
  fflush(stdout);

  {
    int local_range[2] = {-1, -1};
    int distant_range[2] = {-1, -1};

    ple_coupling_mpi_intracomm_create(MPI_COMM_WORLD,
                                      _syr_coupling_loc_comm,
                                      cfd_coupling->root_dist_rank,
                                      &(cfd_coupling->comm),
                                      local_range,
                                      distant_range);

    ple_printf(_("CFD coupling: local_ranks =                 [%d..%d]\n"
                 "              distant ranks =               [%d..%d]\n"),
               local_range[0], local_range[1] - 1,
               distant_range[0], distant_range[1] - 1);

    cfd_coupling->n_dist_ranks = distant_range[1] - distant_range[0];
    cfd_coupling->root_dist_rank = distant_range[0];

  }

  fflush(stdout);

  /* Exchange coupling options */

  if (   cfd_coupling->type == SYR_SURF_COUPLING
      || cfd_coupling->type == SYR_SURFVOL_COUPLING)
    boundary_flag = 'b';
  if (   cfd_coupling->type == SYR_VOL_COUPLING
      || cfd_coupling->type == SYR_SURFVOL_COUPLING)
    volume_flag = 'v';

  sprintf(op_name_send, "coupling:type:%c%c",
          boundary_flag, volume_flag);

  _exchange_sync(cfd_coupling, op_name_send, op_name_recv);

  /* For Code_Saturne 2.2 and later  */
  /* Treatment of the conservativity forcing flag */
  if (op_name_recv[16] == '0') {
    cfd_coupling->conservativity = 0;
    conserv_flag = '0';
  }
  else if (op_name_recv[16] == '1') {
    cfd_coupling->conservativity = 1;
    conserv_flag = '1';
  }

  /* Leave id 17 open for volume conservativity flag */

  /* Exchange scheme version information, with a min and max scheme number so as to
     allow forward/backward compatibility across some scheme changes:
     both codes should follow the maximum common handled scheme.
     Scheme numbers are only to be increased when additional
     or different data are passed. Changes not breaking a given scheme (such as
     better error handling by passing additional messages instead of aborting
     immediately do not require increasing its number. As the first versioned scheme
     is the current version, a blank in the scheme version character places
     is assumed to be scheme 1. */

  if (op_name_recv[18] == ' ' || op_name_recv[18] == '\2')
    scheme_min = 2;
  else
    scheme_min = (int)(op_name_recv[18]);
  if (op_name_recv[19] == ' ' || op_name_recv[19] == '\2')
    scheme_max = 2;
  else
    scheme_max = (int)(op_name_recv[19]);

  /* Tolerance information is exchanged also */

  if (op_name_recv[20] == '0')
    cfd_coupling->allow_nearest = 0;
  else
    cfd_coupling->allow_nearest = 1;

  cfd_coupling->tolerance[0] = 0.;  /* default */
  cfd_coupling->tolerance[1] = 0.1; /* default */
  if (op_name_recv[21] == '(') {
    int i = 0, j = 22;
    char tolerance_str[32] = "";
    while (j < 33) {
     if (op_name_recv[j] == ')')
        break;
      else
        tolerance_str[i++] = op_name_recv[j++];
    }
    tolerance_str[i++] = '\0';
    cfd_coupling->tolerance[1] = atof(tolerance_str);
    if (cfd_coupling->tolerance[1] <= 0)
      cfd_coupling->tolerance[1] = 0.1;
  }
  ple_printf(_("              communication scheme version: %d\n"
               "              conservativity flag:          %d\n"
               "              allow nonmatching:            %d\n"
               "              tolerance:                    %-6.2g %-6.2g\n"),
             scheme_max, cfd_coupling->conservativity,
             cfd_coupling->allow_nearest,
             cfd_coupling->tolerance[0], cfd_coupling->tolerance[1]);
  fflush(stdout);

  /* Check compatibility */

  if (strncmp(op_name_recv, op_name_send, 16))
    ple_error
      (__FILE__, __LINE__, 0,
       _("========================================================\n"
         "   ** Incompatible CFD coupling options:\n"
         "      ------------------------------------------------\n"
         "      SYRTHES options:  \"%s\"\n"
         "      CFD code options: \"%s\"\n"
         "========================================================\n"),
       op_name_send, op_name_recv);
}

/*----------------------------------------------------------------------------
 * Initialize CFD couplings using MPI.
 *
 * This function may be called once all couplings have been defined,
 * and it will match defined couplings with available applications.
 *
 * arguments:
 *   n_coupling <-- number of CFD couplings
 *   couplings  <-- array of CFD couplings
 *----------------------------------------------------------------------------*/

static void
_init_all_mpi_cfd(int                   n_couplings,
                  syr_cfd_coupling_t  **couplings)
{
  int i;

  int n_apps = 0;
  int n_cfd_apps = 0;
  int app_id = -1, app_type = 0;

  const ple_coupling_mpi_set_t *mpi_apps = _syr_coupling_mpi_app_world;

  if (mpi_apps == NULL)
    return;

  n_apps = ple_coupling_mpi_set_n_apps(mpi_apps);

  /* First pass to count available CFD couplings */

  for (i = 0; i < n_apps; i++) {
    const ple_coupling_mpi_set_info_t
      ai = ple_coupling_mpi_set_get_info(mpi_apps, i);
    if (   strncmp(ai.app_type, "Code_Saturne", 12) == 0
        || strncmp(ai.app_type, "NEPTUNE_CFD", 11) == 0) {
      n_cfd_apps += 1;
      app_id = i;
      app_type = 4;
    }
  }

  /* In single-coupling mode, no identification necessary */

  if ((n_cfd_apps == 1) && n_couplings == 1) {
    const ple_coupling_mpi_set_info_t
      ai = ple_coupling_mpi_set_get_info(mpi_apps, app_id);
    _init_mpi_comm(couplings[0], 0, ai.root_rank, ai.n_ranks);
  }

  /* In multiple-coupling mode, identification is necessary */

  else {

    int j;

    /* First loop on available CFD instances to match app_names */

    for (i = 0; i < n_couplings; i++) {

      for (j = 0; j < n_apps; j++) {

        const ple_coupling_mpi_set_info_t
          ai = ple_coupling_mpi_set_get_info(mpi_apps, j);

        if (   (   strncmp(ai.app_type, "Code_Saturne", 12) == 0
                || strncmp(ai.app_type, "NEPTUNE_CFD", 11))
            && (strcmp(ai.app_name, (couplings[i])->app_name) == 0)) {
          _init_mpi_comm((couplings[i]), i, ai.root_rank, ai.n_ranks);
          break;
        }

      }
    }

  } /* End of test on single or multiple CFD matching algorithm */

  /* Check that all couplings are matched */

  for (i = 0; i < n_couplings; i++) {

    if ((couplings[i])->comm == MPI_COMM_NULL) {

      const char *app_name = "";
      if ((couplings[i])->app_name != NULL)
        app_name = (couplings[i])->app_name;

      ple_error
        (__FILE__, __LINE__, 0,
         _("========================================================\n"
           "   ** No matching CFD code using MPI for CFD coupling %d:\n"
           "      -----------------------------------------------\n"
           "      requested application name:   \"%s\"\n"
           "========================================================\n"),
         app_name);
    }
  }

}

#endif /* defined(_SYRTHES_MPI_) */

/*----------------------------------------------------------------------------
 * Build interpolation coefficients for coupled cells (tetrahedra).
 *----------------------------------------------------------------------------*/

static void
_shape_functions_tetra(syr_cfd_interpolation_t   *ip,
                       const int                **elt_vertices,
                       const double             **vertex_coords)
{
  /* Local variables */

  int i, j;

  const ple_locator_t *l = ip->locator;
  const ple_lnum_t n_dist = ple_locator_get_n_dist_points(l);
  const ple_lnum_t *dist_loc = ple_locator_get_dist_locations(l);
  const ple_coord_t *dist_coords = ple_locator_get_dist_coords(l);

  /* Build interpolation coefficients */

  PLE_MALLOC(ip->coeffs, 4*n_dist, float);

  for (i = 0; i < n_dist; i++) {

    int    e_id = ip->coupled_elt_list[dist_loc[i] - 1];
    double t_coords[4][3];

    double vol6;

    double isop_0, isop_1, isop_2;
    double t00, t10, t20, t01, t02, t03, t11, t12, t13, t21, t22, t23;
    double v01[3], v02[3], v03[3];

    for (j = 0; j < 4; j++) {
      ple_lnum_t coord_idx = elt_vertices[j][e_id];
      t_coords[j][0] = vertex_coords[0][coord_idx];
      t_coords[j][1] = vertex_coords[1][coord_idx];
      t_coords[j][2] = vertex_coords[2][coord_idx];
    }

    for (j = 0; j < 3; j++) {
      v01[j] = t_coords[1][j] - t_coords[0][j];
      v02[j] = t_coords[2][j] - t_coords[0][j];
      v03[j] = t_coords[3][j] - t_coords[0][j];
    }

    vol6 = fabs(  v01[0] * (v02[1]*v03[2] - v02[2]*v03[1])
                - v02[0] * (v01[1]*v03[2] - v01[2]*v03[1])
                + v03[0] * (v01[1]*v02[2] - v01[2]*v02[1]));

    if (vol6 > 1.e-18) {

      t00  =   dist_coords[i*3]     - t_coords[0][0];
      t10  =   dist_coords[i*3 + 1] - t_coords[0][1];
      t20  =   dist_coords[i*3 + 2] - t_coords[0][2];

      t01  = - t_coords[0][0] + t_coords[1][0];
      t02  = - t_coords[0][0] + t_coords[2][0];
      t03  = - t_coords[0][0] + t_coords[3][0];

      t11  = - t_coords[0][1] + t_coords[1][1];
      t12  = - t_coords[0][1] + t_coords[2][1];
      t13  = - t_coords[0][1] + t_coords[3][1];

      t21  = - t_coords[0][2] + t_coords[1][2];
      t22  = - t_coords[0][2] + t_coords[2][2];
      t23  = - t_coords[0][2] + t_coords[3][2];

      isop_0 = (  t00 * (t12*t23 - t13*t22)
                - t10 * (t02*t23 - t22*t03)
                + t20 * (t02*t13 - t12*t03)) / vol6;
      isop_1 = (- t00 * (t11*t23 - t13*t21)
                + t10 * (t01*t23 - t21*t03)
                - t20 * (t01*t13 - t03*t11)) / vol6;
      isop_2 = (  t00 * (t11*t22 - t21*t12)
                - t10 * (t01*t22 - t21*t02)
                + t20 * (t01*t12 - t11*t02)) / vol6;

      ip->coeffs[i*4]     = 1. - isop_0 - isop_1 - isop_2;
      ip->coeffs[i*4 + 1] =      isop_0;
      ip->coeffs[i*4 + 2] =               isop_1;
      ip->coeffs[i*4 + 3] =                        isop_2;

    }

    else { /* if vol6 <= 1.e-18 */
      ip->coeffs[i*4]     = 0.25;
      ip->coeffs[i*4 + 1] = 0.25;
      ip->coeffs[i*4 + 2] = 0.25;
      ip->coeffs[i*4 + 3] = 0.25;
    }

  }
}

/*----------------------------------------------------------------------------
 * Build interpolation coefficients for coupled faces (3D triangles).
 *----------------------------------------------------------------------------*/

static void
_shape_functions_tria_3d(syr_cfd_interpolation_t   *ip,
                         const int                **elt_vertices,
                         const double             **vertex_coords)
{
  /* Local variables */

  int   i;

  const ple_locator_t *l = ip->locator;
  const ple_lnum_t n_dist = ple_locator_get_n_dist_points(l);
  const ple_lnum_t *dist_loc = ple_locator_get_dist_locations(l);
  const ple_coord_t *dist_coords = ple_locator_get_dist_coords(l);
  const double one_third = 1./3.;

  /* Build interpolation coefficients */

  PLE_MALLOC(ip->coeffs, 3*n_dist, float);

  for (i = 0; i < n_dist; i++) {

    int    j, coord_idx_0, coord_idx_1, coord_idx_2;

    double t[3], u[3], v[3], w[3], vect_tmp[3];
    double uu, vv, uv, ut, vt, ww, det;
    double isop_0, isop_1;

    int    e_id = ip->coupled_elt_list[dist_loc[i] - 1];

    coord_idx_0 = elt_vertices[0][e_id];
    coord_idx_1 = elt_vertices[1][e_id];
    coord_idx_2 = elt_vertices[2][e_id];

    /* Calculate triangle-constant values for barycentric coordinates */

    for (j = 0; j < 3; j++) {
      u[j] = - vertex_coords[j][coord_idx_0] + vertex_coords[j][coord_idx_1];
      v[j] = - vertex_coords[j][coord_idx_0] + vertex_coords[j][coord_idx_2];
      w[j] =   vertex_coords[j][coord_idx_1] - vertex_coords[j][coord_idx_2];
    }

    uu = _DOT_PRODUCT(u, u);
    vv = _DOT_PRODUCT(v, v);
    ww = _DOT_PRODUCT(w, w);
    uv = _DOT_PRODUCT(u, v);

    det = (uu*vv - uv*uv);

    if (det > 1.e-18) {

      /* Calculation of the barycenter coordinates for the projected node */

      for (j = 0; j < 3; j++)
        t[j] = - vertex_coords[j][coord_idx_0] + dist_coords[i*3 + j];

      ut = _DOT_PRODUCT(u, t);
      vt = _DOT_PRODUCT(v, t);

      isop_0 = (ut*vv - vt*uv) / det;
      isop_1 = (uu*vt - uv*ut) / det;

      _CROSS_PRODUCT(vect_tmp, u, v);

      /* if the projected point is not on triangle, we project it
         on the nearest edge or node */

      if (isop_0 < 0.)
        isop_0 = 0.;

      if (isop_1 < 0.)
        isop_1 = 0.;

      if ((1.0 - isop_0 - isop_1) < 0.) {
        isop_0 = isop_0 / (isop_0 + isop_1);
        isop_1 = isop_1 / (isop_0 + isop_1);
      }

      ip->coeffs[i*3]     = 1. - isop_0 - isop_1;
      ip->coeffs[i*3 + 1] =      isop_0;
      ip->coeffs[i*3 + 2] =               isop_1;

    }

    else { /* if det <= 1.e-18 */
      ip->coeffs[i*3]     = one_third;
      ip->coeffs[i*3 + 1] = one_third;
      ip->coeffs[i*3 + 2] = one_third;
    }

  }
}

/*----------------------------------------------------------------------------
 * Build interpolation coefficients for coupled cells (2D triangles).
 *----------------------------------------------------------------------------*/

static void
_shape_functions_tria_2d(syr_cfd_interpolation_t  *ip,
                         const int                **elt_vertices,
                         const double             **vertex_coords)
{
  /* Local variables */

  int   i;

  const ple_locator_t *l = ip->locator;
  const ple_lnum_t n_dist = ple_locator_get_n_dist_points(l);
  const ple_lnum_t *dist_loc = ple_locator_get_dist_locations(l);
  const ple_coord_t *dist_coords = ple_locator_get_dist_coords(l);
  const double one_third = 1./3.;

  /* Build interpolation coefficients */

  PLE_MALLOC(ip->coeffs, 3*n_dist, float);

  for (i = 0; i < n_dist; i++) {

    int    j, coord_idx_0, coord_idx_1, coord_idx_2;

    double t[3], u[3], v[3];
    double uu, vv, uv, ut, vt, det;
    double isop_0, isop_1;

    int    e_id = ip->coupled_elt_list[dist_loc[i] - 1];

    coord_idx_0 = elt_vertices[0][e_id];
    coord_idx_1 = elt_vertices[1][e_id];
    coord_idx_2 = elt_vertices[2][e_id];

    /* Calculate triangle-constant values for barycentric coordinates */

    for (j = 0; j < 2; j++) {
      u[j] = - vertex_coords[j][coord_idx_0] + vertex_coords[j][coord_idx_1];
      v[j] = - vertex_coords[j][coord_idx_0] + vertex_coords[j][coord_idx_2];
    }

    uu = _DOT_PRODUCT_2D(u, u);
    vv = _DOT_PRODUCT_2D(v, v);
    uv = _DOT_PRODUCT_2D(u, v);

    det = (uu*vv - uv*uv);

    if (det > 1.e-18) {

      /* Calculation of the barycenter coordinates for the projected node */

      for (j = 0; j < 2; j++)
        t[j] = - vertex_coords[j][coord_idx_0] + dist_coords[i*2 + j];

      ut = u[0]*t[0] + u[1]*t[1];
      vt = v[0]*t[0] + v[1]*t[1];

      isop_0 = (ut*vv - vt*uv) / det;
      isop_1 = (uu*vt - uv*ut) / det;

      ip->coeffs[i*3]     = 1. - isop_0 - isop_1;
      ip->coeffs[i*3 + 1] =      isop_0;
      ip->coeffs[i*3 + 2] =               isop_1;

    }

    else { /* if det <= 1.e-18 */
      ip->coeffs[i*3]     = one_third;
      ip->coeffs[i*3 + 1] = one_third;
      ip->coeffs[i*3 + 2] = one_third;
    }

  }
}

/*----------------------------------------------------------------------------
 * Build interpolation coefficients for coupled faces (2D edges).
 *----------------------------------------------------------------------------*/

static void
_shape_functions_edge_2d(syr_cfd_interpolation_t   *ip,
                         const int                **elt_vertices,
                         const double             **vertex_coords)
{
  /* Local variables */

  int   i;

  const ple_locator_t *l = ip->locator;
  const ple_lnum_t n_dist = ple_locator_get_n_dist_points(l);
  const ple_lnum_t *dist_loc = ple_locator_get_dist_locations(l);
  const ple_coord_t *dist_coords = ple_locator_get_dist_coords(l);

  /* Build interpolation coefficients */

  PLE_MALLOC(ip->coeffs, 2*n_dist, float);

  for (i = 0; i < n_dist; i++) {

    int    j, coord_idx_0, coord_idx_1;

    double u[2], v[2];
    double uv, len, isop_0;

    int    e_id = ip->coupled_elt_list[dist_loc[i] - 1];

    coord_idx_0 = elt_vertices[0][e_id];
    coord_idx_1 = elt_vertices[1][e_id];

    /* Calculate edge vector and length */

    for (j = 0; j < 2; j++)
      u[j] =   vertex_coords[j][coord_idx_1] - vertex_coords[j][coord_idx_0];

    len = u[0]*u[0] + u[1]*u[1];

    if (len > 1.e-18) {

      /* Calculate linear coordinates of projection of point on edge axis */

      for (j = 0; j < 2; j++)
        v[j] = dist_coords[i*2 + j] - vertex_coords[j][coord_idx_0];

      uv = u[0]*v[0] + u[1]*v[1];

      isop_0 = uv / (len*len);

      ip->coeffs[i*2]     = 1. - isop_0;
      ip->coeffs[i*2 + 1] =      isop_0;

    }

    else { /* if len <= 1.e-18 */
      ip->coeffs[i*2]     = 0.5;
      ip->coeffs[i*2 + 1] = 0.5;
    }

  }
}

/*----------------------------------------------------------------------------
 * Create and initialize syr_cfd_mesh_t mesh structure from SYRTHES
 * mesh connectivity.
 *
 * arguments:
 *   dim             <-- spatial dimension
 *   elt_dim         <-- element dimension
 *   n_coupled_cells <-- number of coupled cells
 *   coupled_elts    <-- list of coupled elements (indirection, 0 to n-1)
 *   elt_vertices    <-- element vertex ids (0 to n-1)
 *   vertex_coords   <-- vertex coordinates ([dim][n_points], 0 to n-1)
 *
 * returns:
 *   pointer to new syr_cfd_mesh_t structure
 *----------------------------------------------------------------------------*/

static syr_cfd_mesh_t *
_build_syr_cfd_mesh(int             dim,
                    int             elt_dim,
                    int             n_coupled_elts,
                    const int       coupled_elts[],
                    const int     **elt_vertices,
                    const double  **vertex_coords)
{
  /* Local variables */

  int  i, j;

  int  max_vertex_num = 0;   /* Highest vertex number for elements in list*/

  syr_cfd_mesh_t  *elts = NULL; /* Local elements used as interpolation basis
                                   for distant point values */

  syr_cfd_element_t elt_type = SYR_CFD_N_ELEMENT_TYPES;

  int  *_elt_vertices = NULL;
  double  *_vertex_coords = NULL;

  int nve = elt_dim + 1;

  if (dim == 3) {
    if (elt_dim == 2)
      elt_type = SYR_CFD_TRIA;
    else if (elt_dim == 3)
      elt_type = SYR_CFD_TETRA;
  }
  else if (dim == 2) {
    if (elt_dim == 1)
      elt_type = SYR_CFD_EDGE;
    else if (elt_dim == 3)
      elt_type = SYR_CFD_TRIA;
  }

  /* Build partial vertex coordinates array */

  for (i = 0; i < n_coupled_elts; i++) {
    int elt_id = coupled_elts[i];
    for (j = 0; j < nve; j++) {
      int vertex_num = elt_vertices[j][elt_id] + 1;
      if (vertex_num > max_vertex_num)
        max_vertex_num = vertex_num;
    }
  }

  PLE_MALLOC(_vertex_coords, max_vertex_num*dim, double);

  for (i = 0; i < max_vertex_num; i++) {
    for (j = 0; j < dim; j++)
      _vertex_coords[i*dim + j] = vertex_coords[j][i];
  }

  /* Build connectivity array */

  PLE_MALLOC(_elt_vertices, n_coupled_elts*nve, int);

  for (i = 0; i < n_coupled_elts; i++) {
    int elt_id = coupled_elts[i];
    for (j = 0; j < nve; j++) {
      int vertex_id = elt_vertices[j][elt_id];
      _elt_vertices[i*nve + j] = vertex_id + 1;
    }
  }

  /* Create corresponding syr_cfd_mesh_t structures. */

  elts = syr_cfd_mesh_create(dim,
                             max_vertex_num,
                             n_coupled_elts,
                             elt_type,
                             _vertex_coords,
                             _elt_vertices);

  _elt_vertices = NULL;
  _vertex_coords = NULL;

  return elts;
}

/*----------------------------------------------------------------------------
 * Create empty interpolation structure
 *
 * returns:
 *   pointer to new interpolation structure
 *----------------------------------------------------------------------------*/

static syr_cfd_interpolation_t *
_interpolation_create(void)
{
  syr_cfd_interpolation_t *ip;

  PLE_MALLOC(ip, 1, syr_cfd_interpolation_t);

  ip->locator = NULL;

  ip->n_element_vertices = 0;
  ip->n_coupled_elts = 0;

  ip->coupled_elt_list = NULL;
  ip->coeffs = NULL;

  ip->elt_vertices = NULL;

  return ip;
}

/*----------------------------------------------------------------------------
 * Initialize interpolation structure
 *
 * parameters:
 *   ip <-> pointer to interpolation structure pointer
 *----------------------------------------------------------------------------*/

static void
_interpolation_destroy(syr_cfd_interpolation_t **ip)
{
  syr_cfd_interpolation_t *_ip = *ip;

  if (_ip == NULL)
    return;

  if (_ip->locator != NULL)
    _ip->locator = ple_locator_destroy(_ip->locator);

  PLE_FREE(_ip->coupled_elt_list);
  PLE_FREE(_ip->coeffs);

  PLE_FREE(*ip);
}

/*----------------------------------------------------------------------------
 * Check if coupling location is complete
 *
 * arguments:
 *   coupl          <-> coupling helper structure
 *   ip             <-> coupling interpolation helper structure

 * returns:
 *   1 if location is incomplete, 0 if it is complete
 *----------------------------------------------------------------------------*/

static int
_is_location_incomplete(syr_cfd_coupling_t       *coupl,
                        syr_cfd_interpolation_t  *ip)
{
  /* Local variables */

  int  location_incomplete = 0;
  int  flag_glob = 0;

  char  op_name_send[32 + 1];
  char  op_name_recv[32 + 1];

  if (ple_locator_get_n_exterior(ip->locator) > 0)
    location_incomplete = 1;

#if defined(_SYRTHES_MPI_)
  {
    int n_ranks = 1;
    MPI_Comm_size(_syr_coupling_loc_comm, &n_ranks);
    if (n_ranks > 1) {
      int flag_loc = location_incomplete;
      MPI_Allreduce(&flag_loc, &location_incomplete, 1, MPI_INT, MPI_MAX,
                    _syr_coupling_loc_comm);
    }
  }
#endif

  if (location_incomplete > 0)
    strcpy(op_name_send, "coupling:location:incomplete");
  else
    strcpy(op_name_send, "coupling:location:ok");

  _exchange_sync(coupl, op_name_send, op_name_recv);
  if (!strcmp(op_name_recv, "coupling:location:incomplete"))
    location_incomplete = 1;

  return location_incomplete;
}

/*----------------------------------------------------------------------------
 * Initialize interpolation structure.
 *
 * Local elements are used as a location basis for values of distant
 * "coupled" elements, and their vertices are located relative to the
 * corresponding distant supports.
 *
 * arguments:
 *   coupl          <-> coupling helper structure
 *   ip             <-> coupling interpolation helper structure
 *   dim            <-- spatial dimension
 *   elt_dim        <-- element dimension
 *   n_coupled_elts <-- number of coupled elements
 *   coupled_elts   <-- list of coupled elements (0 to n-1)
 *   elt_vertices    <-- element vertex ids (0 to n-1)
 *   vertex_coords   <-- vertex coordinates ([dim][n_points], 0 to n-1)
 *----------------------------------------------------------------------------*/

static void
_interpolation_init(syr_cfd_coupling_t       *coupl,
                    syr_cfd_interpolation_t  *ip,
                    int                       dim,
                    int                       elt_dim,
                    int                       n_coupled_elts,
                    const int                 coupled_elts[],
                    const int               **elt_vertices,
                    const double            **vertex_coords)
{
  /* Local variables */

  int   i;

  int  flag_glob = 0;
  int  flag_loc = 0;

  syr_cfd_mesh_t  *elts = NULL; /* Local elements used as interpolation basis
                                   for distant point values */

  syr_cfd_element_t elt_type = SYR_CFD_N_ELEMENT_TYPES;
  ple_coord_t  *elt_centers = NULL;
  float *dist_to_cfd = NULL;

  /* Check if cells are coupled, and return if this is not the case */

  if (n_coupled_elts > 0) {
    flag_loc = 1;
    flag_glob = 1;
  }

#if defined(_SYRTHES_MPI_)
  {
    int  n_ranks = 1;
    MPI_Comm_size(_syr_coupling_loc_comm, &n_ranks);
    if (n_ranks > 1)
      MPI_Allreduce(&flag_loc, &flag_glob, 1, MPI_INT, MPI_MAX,
                    _syr_coupling_loc_comm);
  }
#endif

  if (flag_glob <= 0)
    return;

  /* Initialization */

#if defined(_SYRTHES_MPI_)
  ip->locator = ple_locator_create(coupl->comm,
                                   coupl->n_dist_ranks,
                                   coupl->root_dist_rank);
#else
  ip->locator = ple_locator_create();
#endif

  if (dim == 3) {
    if (elt_dim == 2)
      elt_type = SYR_CFD_TRIA;
    else if (elt_dim == 3)
      elt_type = SYR_CFD_TETRA;
  }
  else if (dim == 2) {
    if (elt_dim == 1)
      elt_type = SYR_CFD_EDGE;
    else if (elt_dim == 3)
      elt_type = SYR_CFD_TRIA;
  }

  ip->n_coupled_elts = n_coupled_elts;
  ip->n_element_vertices = elt_dim + 1;

  /* Count and build list of coupled elements */

  PLE_MALLOC(ip->coupled_elt_list, n_coupled_elts, int);

  for (i = 0; i < n_coupled_elts; i++)
    ip->coupled_elt_list[i] = coupled_elts[i];

  /* Create corresponding syr_cfd_mesh structures */

  elts = _build_syr_cfd_mesh(dim,
                             elt_dim,
                             n_coupled_elts,
                             coupled_elts,
                             elt_vertices,
                             vertex_coords);

  /* Obtain element centers (interpolation points) */

  PLE_MALLOC(elt_centers, n_coupled_elts*dim, ple_coord_t);

  syr_cfd_mesh_get_element_centers(elts, elt_centers);

  /* Initialize location of correspondants */

  if (elt_dim == dim - 1)
    PLE_MALLOC(dist_to_cfd, n_coupled_elts, float);

  ple_locator_set_mesh(ip->locator,
                       elts,
                       NULL,
                       coupl->tolerance[0],
                       coupl->tolerance[1],
                       dim,
                       n_coupled_elts,
                       NULL,
                       NULL,
                       elt_centers,
                       dist_to_cfd,
                       syr_cfd_point_location_extents,
                       syr_cfd_point_location_contain);

  flag_loc = _is_location_incomplete(coupl, ip);

  if (coupl->allow_nearest) {

    float tolerance = coupl->tolerance[1];

    while (flag_loc > 0) {

      tolerance *= 4;

      ple_locator_extend_search(ip->locator,
                                elts,
                                NULL,
                                0,
                                tolerance,
                                n_coupled_elts,
                                NULL,
                                NULL,
                                elt_centers,
                                dist_to_cfd,
                                syr_cfd_point_location_extents,
                                syr_cfd_point_location_contain);

      flag_loc = _is_location_incomplete(coupl, ip);

    }

  }

  if (elt_dim == dim - 1) {
    ple_locator_exchange_point_var(ip->locator,
                                   NULL,
                                   dist_to_cfd,
                                   NULL,
                                   sizeof(float),
                                   1,
                                   1);
    PLE_FREE(dist_to_cfd);
  }

  /* Check that all element centers are located */

  if (ple_locator_get_n_exterior(ip->locator) == 0)  {

    PLE_FREE(elt_centers);

    /* Build interpolation coefficients */

    if (dim == 3) {
      if (elt_dim == 2)
        _shape_functions_tria_3d(ip, elt_vertices, vertex_coords);
      else if (elt_dim == 3)
        _shape_functions_tetra(ip, elt_vertices, vertex_coords);
    }
    else if (dim == 2) {
      if (elt_dim == 1)
        _shape_functions_edge_2d(ip, elt_vertices, vertex_coords);
      if (elt_dim == 2)
        _shape_functions_tria_2d(ip, elt_vertices, vertex_coords);
    }

    flag_loc = 0;

  }
  else {

    PLE_FREE(elt_centers);

    flag_loc = 1;

  }

  /* Interpolation meshes are not required anymore */

  if (elts != NULL)
    syr_cfd_mesh_destroy(&elts);

  /* Set pointer to main connectivity */

  ip->elt_vertices = elt_vertices;

  /* Exchange synchronization message */

  _coupling_start(coupl, flag_loc);
}

/*----------------------------------------------------------------------------
 * Send values at vertices prior to iteration
 *
 * parameters:
 *   ip   <-- interpolation structure
 *   var  <-- variable defined on all vertices
 *----------------------------------------------------------------------------*/

static void
_send_nodal_var(syr_cfd_interpolation_t  *ip,
                const double             *var)
{
  int i;

  int e_id;
  int v0, v1, v2, v3;

  int     n_dist = ple_locator_get_n_dist_points(ip->locator);
  double *var_send = NULL;

  const ple_lnum_t *dist_loc = ple_locator_get_dist_locations(ip->locator);

  PLE_MALLOC(var_send, n_dist, double);

  /* Send data */

  if (ip->n_element_vertices == 4) {

    for (i = 0; i < n_dist; i++) {

      e_id = ip->coupled_elt_list[dist_loc[i] - 1];

      v0 = ip->elt_vertices[0][e_id];
      v1 = ip->elt_vertices[1][e_id];
      v2 = ip->elt_vertices[2][e_id];
      v3 = ip->elt_vertices[2][e_id];

      var_send[i] =   (ip->coeffs[i*4]     * var[v0])
                    + (ip->coeffs[i*4 + 1] * var[v1])
                    + (ip->coeffs[i*4 + 2] * var[v2])
                    + (ip->coeffs[i*4 + 3] * var[v3]);
    }
  }

  else if (ip->n_element_vertices == 3) {

    for (i = 0; i < n_dist; i++) {

      e_id = ip->coupled_elt_list[dist_loc[i] - 1];

      v0 = ip->elt_vertices[0][e_id];
      v1 = ip->elt_vertices[1][e_id];
      v2 = ip->elt_vertices[2][e_id];

      var_send[i] =   (ip->coeffs[i*3]     * var[v0])
                    + (ip->coeffs[i*3 + 1] * var[v1])
                    + (ip->coeffs[i*3 + 2] * var[v2]);
    }
  }

  else if (ip->n_element_vertices == 2) {

    for (i = 0; i < n_dist; i++) {

      e_id = ip->coupled_elt_list[dist_loc[i] - 1];

      v0 = ip->elt_vertices[0][e_id];
      v1 = ip->elt_vertices[1][e_id];

      var_send[i] =   (ip->coeffs[i*2]     * var[v0])
                    + (ip->coeffs[i*2 + 2] * var[v1]);
    }
  }

  ple_locator_exchange_point_var(ip->locator,
                                 var_send,
                                 NULL,
                                 NULL,
                                 sizeof(double),
                                 1,
                                 0);

  PLE_FREE(var_send);
}

/*----------------------------------------------------------------------------
 * Send values at element centers prior to iteration
 *
 * parameters:
 *   ip   <-- interpolation structure
 *   var  <-- variable defined on all elements
 *----------------------------------------------------------------------------*/

static void
_send_elt_var(syr_cfd_interpolation_t  *ip,
              const double             *var)
{
  int i;

  ple_lnum_t n_dist = ple_locator_get_n_dist_points(ip->locator);
  double *var_send = NULL;

  const ple_lnum_t *dist_loc = ple_locator_get_dist_locations(ip->locator);

  PLE_MALLOC(var_send, n_dist, double);

  /* Send data */

  for (i = 0; i < n_dist; i++)
    var_send[i] = var[dist_loc[i] - 1];

  ple_locator_exchange_point_var(ip->locator,
                                 var_send,
                                 NULL,
                                 NULL,
                                 sizeof(double),
                                 1,
                                 0);

  PLE_FREE(var_send);
}

/*===========================================================================
 * Public functions
 *===========================================================================*/

#if defined(_SYRTHES_MPI_)

/*----------------------------------------------------------------------------
 * Initialize CFD coupling.
 *
 * Discover other applications in the same MPI root communicator.
 *
 * parameters:
 *   syrthes_app_name <-- optional name of this SYRTHES instance, or NULL.
 *   syrthes_comm     <-- communicator for this SYRTHES application
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_mpi_init(const char  *syrthes_app_name,
                          MPI_Comm     syrthes_comm)
{
  /* First, initialize PLE library */

  ple_error_handler_set(_syr_error_handler) ;

  ple_printf_function_set(_syr_ple_printf);

  /* Set global variables */

  _syr_coupling_loc_comm = syrthes_comm;

  /* Discover other applications in root MPI communicator */

  if (_syr_coupling_loc_comm != MPI_COMM_NULL)
    MPI_Comm_rank(_syr_coupling_loc_comm, &_syr_coupling_loc_rank);

  if (_syr_coupling_loc_comm != MPI_COMM_WORLD) {

    int i, n_apps, app_id;
    int sync_flag = 0;

    /* App_type contains a string such as
       "Code_Saturne 1.4.0" or "NEPTUNE_CFD 1.2.1" */

    if (_syr_coupling_loc_rank == 0) {
      ple_printf(_("\n"
                   "Applications accessible through MPI:\n"
                   "------------------------------------\n\n"));
      fflush(stdout);
    }

    _syr_coupling_mpi_app_world
      = ple_coupling_mpi_set_create(sync_flag,
                                    "SYRTHES 4",
                                    syrthes_app_name,
                                    MPI_COMM_WORLD,
                                    _syr_coupling_loc_comm);

    n_apps = ple_coupling_mpi_set_n_apps(_syr_coupling_mpi_app_world);
    app_id = ple_coupling_mpi_set_get_app_id(_syr_coupling_mpi_app_world);

    if (_syr_coupling_loc_rank < 1) {

      const char local_add[] = " (this instance)";
      const char nolocal_add[] = "";

      for (i = 0; i< n_apps; i++) {
        const char *is_local = nolocal_add;
        ple_coupling_mpi_set_info_t
          ai = ple_coupling_mpi_set_get_info(_syr_coupling_mpi_app_world, i);
        if (i == app_id)
          is_local = local_add;
        ple_printf(_("  %d; type:      \"%s\"%s\n"
                     "     case name: \"%s\"\n"
                     "     lead rank: %d; n_ranks: %d\n\n"),
                   i, ai.app_type, is_local,
                   ai.app_name, ai.root_rank, ai.n_ranks);
      }

      fflush(stdout);
    }
  }

}

#endif /* _SYRTHES_MPI_ */

/*----------------------------------------------------------------------------
 * Finalize MPI coupling helper structures.
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_finalize(void)
{
#if defined(_SYRTHES_MPI_)
  {
    int mpi_flag, ierror;

    MPI_Initialized(&mpi_flag);

    if (mpi_flag != 0) {

      _syr_coupling_loc_comm = MPI_COMM_NULL;

      ierror = MPI_Barrier(MPI_COMM_WORLD);

      if (ierror != 0)
        ple_error(__FILE__, __LINE__, 0,
                  _("Error in MPI_Barrier in finalization of global "
                    "communicator on SYRTHES side.\n"));

    }

    if (_syr_coupling_mpi_app_world != NULL)
      ple_coupling_mpi_set_destroy(&_syr_coupling_mpi_app_world);
  }
#endif
}

/*----------------------------------------------------------------------------
 * Create syr_cfd_coupling_t structure
 *
 * Couplings are possible either with completely separate process groups
 * from the main group (Syrthes communicator), or with this same group.
 *
 * arguments:
 *   dim        <-- Spatial dimension
 *   app_name   <-- Optional application name, or NULL
 *   type       <-- surface coupling, volume coupling or both
 *
 * returns:
 *   pointer to new coupling structure
 *----------------------------------------------------------------------------*/

syr_cfd_coupling_t *
syr_cfd_coupling_create(int                       dim,
                        const char               *app_name,
                        syr_cfd_coupling_type_t   type)
{
  /* local variables */
  int  len;
  syr_cfd_coupling_t  *coupling = NULL;

  /* Create associated structure and associated MPI communcator */

  PLE_MALLOC(coupling, 1, syr_cfd_coupling_t);

  coupling->dim = dim;

  if (app_name != NULL) {
    len = strlen(app_name);
    PLE_MALLOC(coupling->app_name, len+1, char);
    strncpy(coupling->app_name, app_name, len);
    coupling->app_name[len] = '\0';
  }
  else {
    PLE_MALLOC(coupling->app_name, 1, char);
    coupling->app_name[0] = '\0';
  }

  /* Create locator structures and define coupling type */

  coupling->type = type;
  coupling->cell_ip = NULL;
  coupling->face_ip = NULL;

  if (type == SYR_SURF_COUPLING)
    coupling->face_ip = _interpolation_create();
  else if (type == SYR_VOL_COUPLING)
    coupling->cell_ip = _interpolation_create();
  else if (type == SYR_SURFVOL_COUPLING) {
    coupling->face_ip = _interpolation_create();
    coupling->cell_ip = _interpolation_create();
  }
  else if (type == SYR_NO_COUPLING)
    ple_printf(_("\n  Warning: No coupling type defined\n"));
  else
    ple_error(__FILE__, __LINE__, 0,
              _("\n  Unknown coupling type.\n  --> abort."));

  /* Define some defaults */

  coupling->conservativity = 0;
  coupling->allow_nearest = 1;
  coupling->tolerance[0] = 0.;
  coupling->tolerance[1] = 0.1;

  return coupling;
}

/*---------------------------------------------------------------------------
 * Finalize syr_cfd_coupling_t structure
 *
 * arguments:
 *   coupling <-- Pointer to coupling structure pointer
 *---------------------------------------------------------------------------*/

void
syr_cfd_coupling_destroy(syr_cfd_coupling_t **coupling)
{
  syr_cfd_coupling_t *_coupling = *coupling;

  _interpolation_destroy(&(_coupling->cell_ip));
  _interpolation_destroy(&(_coupling->face_ip));

#if defined(_SYRTHES_MPI_)
  if (   _coupling->comm != MPI_COMM_WORLD
      && _coupling->comm != _syr_coupling_loc_comm)
    MPI_Comm_free(&(_coupling->comm));
#endif

  if (_coupling->app_name != NULL)
    PLE_FREE(_coupling->app_name);
  PLE_FREE(_coupling);

  *coupling = _coupling;
}

/*----------------------------------------------------------------------------
 * Initialize CFD coupling communication using MPI.
 *
 * This function may be called once all couplings have been defined,
 * and it will match defined couplings with available applications.
 *
 * arguments:
 *   n_coupling <-- number of CFD couplings
 *   couplings  <-- array of CFD couplings
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_init_comm(int                   n_couplings,
                           syr_cfd_coupling_t  **couplings)
{
#if defined(_SYRTHES_MPI_)

  _init_all_mpi_cfd(n_couplings, couplings);

#endif
}

/*----------------------------------------------------------------------------
 * Get CFD coupling type.
 *
 * arguments:
 *   coupl  <-- CFD coupling
 *
 * returns:
 *   type related to a syr_cfd_coupling_t structure
 *----------------------------------------------------------------------------*/

syr_cfd_coupling_type_t
syr_cfd_coupling_get_type(syr_cfd_coupling_t  *coupl)
{
  if (coupl != NULL)
    return coupl->type;
  else
    return SYR_NO_COUPLING;
}

/*----------------------------------------------------------------------------
 * Assign lists of cells associated with a coupling.
 *
 * Local boundary cells are used as a location basis for values of distant
 * "coupled" cells, and their vertices are located relative to the
 * corresponding distant supports.
 *
 * arguments:
 *   coupling        <-- pointer to coupling structure pointer
 *   n_coupled_cells <-- number of coupled cells
 *   coupled_cells   <-- list of coupled cells (indirection, 0 to n-1)
 *   cell_vertices   <-- cell vertex ids (0 to n-1)
 *   vertex_coords   <-- vertex coordinates ([dim][n_points], 0 to n-1)
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_set_cells(syr_cfd_coupling_t  *coupl,
                           int                  n_coupled_cells,
                           const int            coupled_cells[],
                           const int          **cell_vertices,
                           const double       **vertex_coords)
{
  _interpolation_init(coupl,
                      coupl->cell_ip,
                      coupl->dim,
                      coupl->dim,
                      n_coupled_cells,
                      coupled_cells,
                      cell_vertices,
                      vertex_coords);
}

/*----------------------------------------------------------------------------
 * Assign lists of faces associated with a coupling.
 *
 * Local boundary faces are used as a location basis for values of distant
 * "coupled" faces, and their vertices are located relative to the
 * corresponding distant supports.
 *
 * arguments:
 *   coupling        <-- pointer to coupling structure pointer
 *   n_coupled_faces <-- number of coupled faces
 *   coupled_faces   <-- list of coupled faces (indirection, 0 to n-1)
 *   face_vertices   <-- face vertex ids (0 to n-1)
 *   vertex_coords   <-- vertex coordinates ([dim][n_points], 0 to n-1)
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_set_faces(syr_cfd_coupling_t  *coupl,
                           int                  n_coupled_faces,
                           const int            coupled_faces[],
                           const int          **face_vertices,
                           const double       **vertex_coords)
{
  _interpolation_init(coupl,
                      coupl->face_ip,
                      coupl->dim,
                      coupl->dim - 1,
                      n_coupled_faces,
                      coupled_faces,
                      face_vertices,
                      vertex_coords);
}

/*----------------------------------------------------------------------------
 * Synchronize with applications in the same PLE coupling group.
 *
 * This function should be called before starting a new time step. The
 * current time step id is that of the last finished time step, or 0 at
 * initialization.
 *
 * Default synchronization flags indicating a new iteration or end of
 * calculation are set automatically, but the user may set additional flags
 * to this function if necessary.
 *
 * parameters:
 *   flags         <-- optional additional synchronization flags
 *   current_ts_id <-- current time step id
 *   max_ts_id     <-> maximum time step id
 *   ts            <-> suggested time step value
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_sync_apps(int      flags,
                           int      current_ts_id,
                           int     *max_ts_id,
                           double  *ts)
{
#if defined(PLE_HAVE_MPI)

  if (_syr_coupling_mpi_app_world != NULL) {

    int i;

    int sync_flags = 0;
    int leader_id = -1;
    double ts_min = -1.;

    int n_apps
      = ple_coupling_mpi_set_n_apps(_syr_coupling_mpi_app_world);
    int app_id
      = ple_coupling_mpi_set_get_app_id(_syr_coupling_mpi_app_world);

    const int *app_status = NULL;
    const double *app_ts = NULL;

    ple_coupling_mpi_set_info_t ai;

    /* Set synchronization flag */

    app_status
      = ple_coupling_mpi_set_get_status(_syr_coupling_mpi_app_world);

    sync_flags = app_status[app_id] | (flags | _syr_coupling_app_sync_flags);

    if (current_ts_id >= *max_ts_id)
      sync_flags = sync_flags | PLE_COUPLING_STOP;
    else if (current_ts_id == *max_ts_id - 1)
      sync_flags = sync_flags | PLE_COUPLING_LAST;
    else
      sync_flags = sync_flags | PLE_COUPLING_NEW_ITERATION;

    if (flags & PLE_COUPLING_REDO_ITERATION) {
      if (sync_flags & PLE_COUPLING_NEW_ITERATION)
        sync_flags -= PLE_COUPLING_NEW_ITERATION;
      if (sync_flags & PLE_COUPLING_STOP)
        sync_flags -= PLE_COUPLING_STOP;
    }

    /* Synchronize applications */

    ple_coupling_mpi_set_synchronize(_syr_coupling_mpi_app_world,
                                     sync_flags,
                                     *ts);

    app_status
      = ple_coupling_mpi_set_get_status(_syr_coupling_mpi_app_world);
    app_ts
      = ple_coupling_mpi_set_get_timestep(_syr_coupling_mpi_app_world);

    /* Check if we should use the smallest time step */

    if (app_status[app_id] & PLE_COUPLING_TS_MIN)
      ts_min = *ts;

    /* Loop on applications */

    for (i = 0; i < n_apps; i++) {

      if (app_status[i] & PLE_COUPLING_NO_SYNC)
        continue;

      /* Handle leader or minimum time step update */

      if (app_status[i] & PLE_COUPLING_TS_LEADER) {
        if (leader_id > -1) {
          ple_coupling_mpi_set_info_t ai_prev
            = ple_coupling_mpi_set_get_info(_syr_coupling_mpi_app_world,
                                            ts_min);
          ai = ple_coupling_mpi_set_get_info(_syr_coupling_mpi_app_world, i);
          ple_error
            (__FILE__, __LINE__, 0,
             _("\nApplication \"%s\" (%s) tried to set the group time step, but\n"
               "application \"%s\" (%s) has already done so."),
             ai.app_name, ai.app_type, ai_prev.app_name, ai_prev.app_type);
        }
        else {
          leader_id = i;
          *ts = app_ts[i];
        }
      }
      else if (app_status[i] & PLE_COUPLING_TS_MIN) {
        if (ts_min > 0) {
          if (app_ts[i] < ts_min)
            ts_min = app_ts[i];
        }
      }

      /* Handle time stepping behavior */

      if (app_status[i] & PLE_COUPLING_STOP) {
        if (*max_ts_id > current_ts_id) {
          ai = ple_coupling_mpi_set_get_info(_syr_coupling_mpi_app_world, i);
          ple_printf
            (_("\nApplication \"%s\" (%s) requested calculation stop.\n"),
             ai.app_name, ai.app_type);
          *max_ts_id = current_ts_id;
        }
      }
      else if (app_status[i] & PLE_COUPLING_REDO_ITERATION) {
        ai = ple_coupling_mpi_set_get_info(_syr_coupling_mpi_app_world, i);
        ple_error
          (__FILE__, __LINE__, 0,
           _("\nApplication \"%s\" (%s) requested restarting iteration,\n"
             "but this is not currently handled."),
           ai.app_name, ai.app_type);
      }
      else if (! (app_status[i] & PLE_COUPLING_NEW_ITERATION)) {
        ai = ple_coupling_mpi_set_get_info(_syr_coupling_mpi_app_world, i);
        ple_error
          (__FILE__, __LINE__, 0,
           _("\nApplication \"%s\" (%s) synchronized with status flag %d,\n"
             "which does not specify a known behavior."),
           ai.app_name, ai.app_type, app_status[i]);
      }

      if (app_status[i] & PLE_COUPLING_LAST) {
        if (*max_ts_id > current_ts_id + 1) {
          ai = ple_coupling_mpi_set_get_info(_syr_coupling_mpi_app_world, i);
          ple_printf
            (_("\nApplication \"%s\" (%s) requested last iteration.\n"),
             ai.app_name, ai.app_type);
          *max_ts_id = current_ts_id + 1;
        }
      }

    } /* end of loop on applications */

    if (ts_min > 0)
      *ts = ts_min;
  }

#else

  return;

#endif /* PLE_HAVE_MPI */
}

/*----------------------------------------------------------------------------
 * Exchange of synchronization (supervision) messages
 *
 * parameters:
 *  coupling <-- Associated coupling object
 *  is_end   --> Calculation stop indicator
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_supervise(syr_cfd_coupling_t  *coupling,
                           int                 *is_end)
{
  char  op_name_send[32 + 1];
  char  op_name_recv[32 + 1];

  int verbose = 0;

  memset(op_name_send, 0, 32);

  /* Prepare command message */

  if (*is_end == 1 || _syr_coupling_app_sync_flags | PLE_COUPLING_STOP)
    strcpy(op_name_send, "cmd:stop");

  else
    strcpy(op_name_send, "cmd:iter:start");

  /* Exchange command messages */

  _exchange_sync(coupling, op_name_send, op_name_recv);

  /* Interpret message */
  /*-------------------*/

  /* If message indicates calculation stop */

  if (!strcmp(op_name_recv, "cmd:stop"))  {

    ple_printf(_(" .-----------------------------------.\n"
                 " |  Coupling %2d stopped by CFD code |\n"
                 " .-----------------------------------.\n"));

    *is_end = 1;
  }

  /* If message indicates a new iteration */

  else if (!strcmp(op_name_recv, "cmd:iter:start")) {

    if (verbose)
      ple_printf(_(" CFD code indicates new iteration.\n\n"));

  }

  /* Otherwise, message is unknown */

  else
    ple_error(__FILE__, __LINE__, 0,
              _("Message \"%s\" unknown or unexpected at this stage:\n"
                "--> abort."),
              op_name_recv);
}

/*----------------------------------------------------------------------------
 * Data exchange for coupled cell values prior to iteration
 *
 * Send wall temperature
 * Receive fluid temperature and pseudo-exchange coefficient
 *
 * parameters:
 *   coupling <-- Associated coupling object
 *   t_node   <-- Temperature at node, or NULL
 *   t_cell   <-> Solid temperature on coupled cells in (if t_node is NULL),
 *                Fluid temperature on coupled cells out
 *   h_cell   --> Volume pseudo-exchange coefficient on coupled cells
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_cell_vars(syr_cfd_coupling_t  *coupling,
                           const double        *t_node,
                           double              *t_cell,
                           double              *h_cell)
{
  int i;

  syr_cfd_interpolation_t  *ip = coupling->cell_ip;
  double *recv_buf = NULL;

  if (ip == NULL)
    return;

  /* Send wall temperature */

  if (t_node != NULL)
    _send_nodal_var(ip, t_node);
  else
    _send_elt_var(ip, t_cell);

  /* Receive data */

  PLE_MALLOC(recv_buf, ip->n_coupled_elts*2, double);

  ple_locator_exchange_point_var(ip->locator,
                                 NULL,
                                 recv_buf,
                                 NULL,
                                 sizeof(double),
                                 2,
                                 0);

  for (i = 0; i < ip->n_coupled_elts; i++) {
    t_cell[i] = recv_buf[i*2];
    h_cell[i] = recv_buf[i*2 + 1];
  }

  PLE_FREE(recv_buf);
}

/*----------------------------------------------------------------------------
 * Data exchange for coupled face values prior to iteration
 *
 * Send wall temperature
 * Receive fluid temperature and pseudo-exchange coefficient
 * Receive Code_Saturne computed flux and send correction coefficient
 *
 * parameters:
 *   coupling <-- Associated coupling object
 *   t_node   <-- Temperature at node, or NULL
 *   t_face   <-> Wall temperature on coupled faces in (if t_node is NULL),
 *                Fluid temperature on coupled faces out
 *   h_face   --> Wall pseudo-exchange coefficient on coupled faces
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_face_vars(syr_cfd_coupling_t  *coupling,
                           const double        *t_node,
                           double              *t_face,
                           double              *h_face)
{
  int i;

  syr_cfd_interpolation_t  *ip = coupling->face_ip;
  double *recv_buf = NULL;

  if (ip == NULL)
    return;

  /* Send wall temperature */

  if (t_node != NULL)
    _send_nodal_var(ip, t_node);
  else
    _send_elt_var(ip, t_face);

  /* Receive data */

  PLE_MALLOC(recv_buf, ip->n_coupled_elts*2, double);

  ple_locator_exchange_point_var(ip->locator,
                                 NULL,
                                 recv_buf,
                                 NULL,
                                 sizeof(double),
                                 2,
                                 0);

  for (i = 0; i < ip->n_coupled_elts; i++) {
    t_face[i] = recv_buf[i*2];
    h_face[i] = recv_buf[i*2 + 1];
  }

  PLE_FREE(recv_buf);

}

/*----------------------------------------------------------------------------
 * Ensure conservativity thanks to a corrector coefficient computed by SYRTHES
 * SYRTHES computes a global flux for a given tfluid and hfluid field.
 * CFD code sent before its computed global flux for this time step.
 * h_face is modified to ensure conservativity.
 *
 * parameters:
 *   coupling   <-- coupling structure
 *   sur_flux   <-- thermal flux computed by SYRTHES to compare with CFD
 *   h_surf     <-> exchange coefficient defined by face
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_ensure_conservativity(syr_cfd_coupling_t   *coupling,
                                       double                syr_flux,
                                       double                h_surf[])
{
  int ii;

  double  cfd_flux = 0.0, coef = 0.0;

  syr_cfd_interpolation_t  *ip = coupling->face_ip;

  if (coupling->conservativity < 1) /* Nothing to do */
    return;

  /* Get CFD code global flux and send corrector coefficient */

#if defined(_SYRTHES_MPI_)
  if (_syr_coupling_loc_rank < 1) { /* Receive CFD code global flux */

    MPI_Status  status;

    MPI_Recv(&cfd_flux, 1, MPI_DOUBLE,
             coupling->root_dist_rank,
             syr_cfd_coupling_tag,
             coupling->comm,
             &status);

    /* Compute correction coefficient */

    if (fabs(cfd_flux) > DBL_MIN)
      coef = syr_flux / cfd_flux;
    else {
      assert(fabs(syr_flux) < DBL_MIN);
      coef = 1.0;
    }

    /* Send correction coefficient */

    MPI_Send(&coef, 1, MPI_DOUBLE,
             coupling->root_dist_rank,
             syr_cfd_coupling_tag,
             coupling->comm);

  }

  MPI_Bcast(&coef, 1, MPI_DOUBLE, 0, _syr_coupling_loc_comm);
#endif

  /* Print message */

  ple_printf(" Correction coefficient used to ensure conservativity:"
             " %5.3e\n", coef);

  /* Apply correction */

  for (ii = 0; ii < ip->n_coupled_elts; ii++)
    h_surf[ii] *= coef;

}

/*===========================================================================*/

#endif /* defined(_SYRTHES_CFD_) */
