/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: b30fdfd9c47ef24d626a9152a2241acf50f5b633 $
*/

void init_math_transforms();
void exit_math_transforms();
