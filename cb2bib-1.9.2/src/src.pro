FORMS += c2bCiterWidget.ui \
         c2bConfigure.ui \
         c2bConfigureFR.ui \
         c2bEditor.ui \
         c2bExportDialog.ui \
         c2bLogWidget.ui \
         c2bPdfImport.ui \
         c2bRLWebSearchSettings.ui \
         c2bReferenceList.ui \
         c2bSaveRegExp.ui \
         c2bSearchInFiles.ui \
         c2bSearchInFilesPattern.ui \
         c2bSearchInFilesPatternEdit.ui \
         c2bShortcutWidget.ui \
         cb2Bib.ui \
         findDialog.ui \
         proxyDialog.ui
RESOURCES += cb2bib.qrc 
HEADERS += qtsingleapplication/src/*.h \
           bookmarkPlugin.h \
           c2b.h \
           c2bBibHighlighter.h \
           c2bBibMenu.h \
           c2bBibParser.h \
           c2bBibPreparserLog.h \
           c2bCiter.h \
           c2bCiterExcerptView.h \
           c2bCiterHistory.h \
           c2bCiterModel.h \
           c2bCiterView.h \
           c2bCiterWidget.h \
           c2bClipEdit.h \
           c2bClipboard.h \
           c2bCollectionIndex.h \
           c2bColors.h \
           c2bComboBox.h \
           c2bConfigure.h \
           c2bConfigureFR.h \
           c2bConsole.h \
           c2bCoreCiter.h \
           c2bEditor.h \
           c2bExport.h \
           c2bExportDialog.h \
           c2bFileDialog.h \
           c2bHighlighter.h \
           c2bIdLineEdit.h \
           c2bLineEdit.h \
           c2bNetworkQuery.h \
           c2bNetworkQueryInfo.h \
           c2bPdfImport.h \
           c2bPostprocess.h \
           c2bREHighlighter.h \
           c2bRLWebSearchSettings.h \
           c2bReferenceList.h \
           c2bSaveREHighlighter.h \
           c2bSaveRegExp.h \
           c2bSearchInFiles.h \
           c2bSearchInFilesPattern.h \
           c2bSettings.h \
           c2bShortcutPushButton.h \
           c2bShortcutWidget.h \
           c2bTests.h \
           c2bTextBrowser.h \
           c2bTextEdit.h \
           c2bUpdateMetadata.h \
           c2bUtils.h \
           cb2Bib.h \
           clipboardPoll.h \
           findDialog.h
SOURCES += qtsingleapplication/src/*.cpp \
           bookmarkPlugin.cpp \
           c2b.cpp \
           c2bBibHighlighter.cpp \
           c2bBibMenu.cpp \
           c2bBibParser.cpp \
           c2bBibPreparserLog.cpp \
           c2bCiterExcerptView.cpp \
           c2bCiterHistory.cpp \
           c2bCiterModel.cpp \
           c2bCiterView.cpp \
           c2bCiterWidget.cpp \
           c2bClipEdit.cpp \
           c2bClipboard.cpp \
           c2bCollectionIndex.cpp \
           c2bColors.cpp \
           c2bComboBox.cpp \
           c2bConfigure.cpp \
           c2bConfigureFR.cpp \
           c2bConsole.cpp \
           c2bCoreCiter.cpp \
           c2bEditor.cpp \
           c2bExport.cpp \
           c2bExportDialog.cpp \
           c2bFileDialog.cpp \
           c2bHighlighter.cpp \
           c2bIdLineEdit.cpp \
           c2bLineEdit.cpp \
           c2bNetworkQuery.cpp \
           c2bNetworkQueryInfo.cpp \
           c2bPdfImport.cpp \
           c2bPostprocess.cpp \
           c2bREHighlighter.cpp \
           c2bRLWebSearchSettings.cpp \
           c2bReferenceList.cpp \
           c2bSaveREHighlighter.cpp \
           c2bSaveRegExp.cpp \
           c2bSearchInFiles.cpp \
           c2bSearchInFilesPattern.cpp \
           c2bSettings.cpp \
           c2bShortcutPushButton.cpp \
           c2bShortcutWidget.cpp \
           c2bTests.cpp \
           c2bTextBrowser.cpp \
           c2bTextEdit.cpp \
           c2bUpdateMetadata.cpp \
           c2bUtils.cpp \
           cb2Bib.cpp \
           clipboardPoll.cpp \
           findDialog.cpp \
           main.cpp
TEMPLATE = app
QT += widgets network
CONFIG +=
LIBS += -lc2b
QMAKE_LIBDIR += .
INCLUDEPATH += c2b .
DEFINES += C2B_USE_QMAKE

CONFIG += use_webkit
use_webkit {
    message(The cb2Bib will be linked against webkit library)
    HEADERS += c2bAnnote.h c2bWebBrowser.h
    SOURCES += c2bAnnote.cpp c2bWebBrowser.cpp
    DEFINES += C2B_USE_WEBKIT
    QT += webkitwidgets
}

CONFIG += use_lzo
use_lzo {
    LIBS += -llzo2
    DEFINES += C2B_USE_LZO
}
disable_lzo {
    CONFIG -= use_lzo
    LIBS -= -llzo2
    DEFINES -= C2B_USE_LZO
}
unix {
    QT += x11extras
    LIBS += -lX11
}
TARGET = cb2bib
DESTDIR = ../bin
DEPENDPATH += .
POST_TARGETDEPS += libc2b.a
QMAKE_CXXFLAGS_DEBUG += -DC2B_DEBUG
DEFINES += C2B_USE_CBPOLL
disable_cbpoll {
    message(Building without clipboardPoll)
    HEADERS -= clipboardPoll.h
    SOURCES -= clipboardPoll.cpp
    DEFINES -= C2B_USE_CBPOLL
}
CONFIG(static) {
    use_webkit {
        DEFINES += C2B_STATIC_LINKING
        QTPLUGIN += qjpeg qgif
    }
}
# Attention. Scripts are not set executable to avoid qmake to write a call
# to strip, thus avoiding a confusing 'error (ignored)' message. As for now,
# 'chmod +x' is called when 'make install' is invoked. This also gives some
# other warnings on some build systems, stating that a text file is copied to
# /bin directory.
unix {
    INSTALLS += target script cb2bibdata icons desktop
    target.path = /usr/bin
    script.files += ../c2bscripts/c2bimport ../c2bscripts/c2bciter
    script.path = /usr/bin
    script.extra = chmod +x ../c2bscripts/c2bimport; chmod +x ../c2bscripts/c2bciter
    desktop.files += ../c2bscripts/cb2bib.desktop ../c2bscripts/c2bciter.desktop ../c2bscripts/c2bimport.desktop
    desktop.path = /usr/share/applications
    icons.files += ../src/icons/cb2bib.png
    icons.path = /usr/share/pixmaps
    cb2bibdata.files += ../AUTHORS ../COPYRIGHT ../LICENSE ../CHANGELOG ../data ../c2btools ../testPDFImport
    cb2bibdata.path = /usr/share/cb2bib
}
win32 {
    HEADERS -= clipboardPoll.h
    SOURCES -= clipboardPoll.cpp
    DEFINES -= C2B_USE_CBPOLL
    RC_FILE = ../cb2bib.rc
}
macx {
    HEADERS -= clipboardPoll.h
    SOURCES -= clipboardPoll.cpp
    DEFINES -= C2B_USE_CBPOLL
    ICON = ../src/icons/cb2bib.icns
    INSTALLS += target script cb2bibdata
    target.path = /Applications
    script.files += ../c2bscripts/c2bimport ../c2bscripts/c2bciter
    script.path = /Applications
    script.extra = chmod +x ../c2bscripts/c2bimport; chmod +x ../c2bscripts/c2bciter
    cb2bibdata.files += ../AUTHORS ../COPYRIGHT ../LICENSE ../CHANGELOG ../data ../c2btools ../testPDFImport
    cb2bibdata.path = Contents/Resources
    QMAKE_BUNDLE_DATA += script cb2bibdata
#   Set this directory if third party tools, like pdftotext are packaged
#   INSTALLS += externtools
#   externtools.files +=
#   externtools.path = c2bExternTools
#   Set Library data
#   QMAKE_BUNDLE_DATA += externtools
}

# Addition by FRusconi <lopippo@debian.org> to security-harden the
# binary.

QMAKE_CPPFLAGS *= $$system(dpkg-buildflags --get CPPFLAGS)
QMAKE_CFLAGS   *= $$system(dpkg-buildflags --get CFLAGS)
QMAKE_CXXFLAGS *= $$system(dpkg-buildflags --get CXXFLAGS)

message($$QMAKE_CPPFLAGS)
message($$QMAKE_CFLAGS)
message($$QMAKE_CXXFLAGS)
