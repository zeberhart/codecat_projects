/*
 * GPR
 * Copyright (C) 2000 CompuMetric Labs, Inc.
 *
 * For more information contact:
 *        Thomas Hubbell
 *        CompuMetric Labs, Inc.
 *        5920 Grelot Road, Suite C-2
 *        Mobile, AL 36609
 * 
 * Voice: (334) 342-2220
 *   Fax: (334) 343-2261
 * Email: thubbell@compumetric.com
 *   Web: http://www.compumetric.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *    
 *        File: Callbacks.h
 * 
 * Description:
 */

#ifdef USE_GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#endif
#include <ppd.h>

/* Initialization and control callbacks */

void grab_file_to_print(GtkWidget * entrybox, gpointer ppd);
void update_ppd_path(GtkWidget * fileentry, gpointer ppd);
void grab_ppd_file(GtkWidget * entrybox, gpointer ppd);
void grab_default_ppd(GtkWidget * menu_item, gpointer ppd);
void init_ppd(GtkButton * button, gpointer ppd);
void set_spool_dir(GtkWidget * menu_item, gpointer ppd);
void set_printer_name(GtkWidget * menu_item, gpointer ppd);
void send_print(GtkWidget * button, gpointer ppd);

gboolean if_no_errors_quit(GtkWidget * main, gpointer data);
void clean_up(GtkWidget * button, gpointer ppd);

/* Advanced/PPD option callbacks */
void fill_option_menus(GtkWidget * button, gpointer ppd);
void select_option_choice(GtkWidget * combo_item, gpointer ppd);
void select_inst_option_choice(GtkWidget * combo_item, gpointer ppd);
void choice_process(PpdOption * option, PpdChoice * choice, GtkWidget * combo,
		    int interator, int inst_opt, GtkWidget * combo_item,
		    int count, gpointer ppd);

/* saved settings callbacks */
void save_sett(GtkWidget * button, gpointer ppd);
void retrieve_sett(GtkWidget * menuitem, gpointer ppd);

/* Common Option callbacks */
void update_num_copies(GtkSpinButton * spinbutton, gpointer ppd);
void update_pageset_all(GtkWidget * radiobutton, gpointer ppd);
void update_pageset_odd(GtkWidget * radiobutton, gpointer ppd);
void update_pageset_even(GtkWidget * radiobutton, gpointer ppd);
void update_range_from(GtkWidget * entrybox, gpointer ppd);
void update_range_to(GtkWidget * entrybox, gpointer ppd);
void update_collate_yes(GtkWidget * radiobutton, gpointer ppd);
void update_collate_no(GtkWidget * radiobutton, gpointer ppd);
void update_revordr_yes(GtkWidget * radiobutton, gpointer ppd);
void update_revordr_no(GtkWidget * radiobutton, gpointer ppd);
void update_nup_1up(GtkWidget * radiobutton, gpointer ppd);
void update_nup_2up(GtkWidget * radiobutton, gpointer ppd);
void update_nup_4up(GtkWidget * radiobutton, gpointer ppd);

/* UI Modification callbacks */
void ungrey_entrybox(GtkWidget * button, gpointer entrybox);
void regrey_entrybox(GtkWidget * button, gpointer entrybox);

/* this is a preliminary work to substitute gtk_combo with gtk_combo_box 
  it does not yet work */
#define USE_COMBO_BOX 0
