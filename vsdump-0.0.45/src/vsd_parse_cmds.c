/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * vsdump: test program to dump and parse content of vsd file
 *
 * Copyright (C) 2006-2007	Valek Filippov (frob@df.ru)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 3 or later of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 */

#define _GNU_SOURCE
#include <glib.h>
#include <glib/gstdio.h>
#include <gsf/gsf-utils.h>
#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include "vsdump.h"
#include "vsd_parse_cmds.h"

/* ------------------------------------------------------------------------- */
/**
 * vsd_parse_cmd_file():
 * Put content of 'chunk_parse_cmds.tbl' to the hash of {chunk type; garray of VsdArgEntry(s)}
 *
 **/
GHashTable*
vsd_parse_cmd_file()
{
	char				*s, *t;
	gchar				**v = NULL;
	gint				ch_type=0;
	unsigned int	n=64;
	FILE 				*ftbl; 
	
	VsdArgEntry		vae, *vaep; /* one 'command' for parser */
	GArray 			*vaet;		/* one table with commands for one chunk */
	GHashTable		*vaetbls; 	/* set of tables */
	
	s = (char *) malloc (n + 1);
	vaep=&vae;
	vaetbls = g_hash_table_new(g_direct_hash,g_direct_equal);
	if(NULL==(ftbl = fopen("chunks_parse_cmds.tbl", "r"))){
	 g_printf("The command table file can't be opened!\n");
	 return NULL;
	}

	while(-1!=getline(&s,&n,ftbl)){
		vaet = g_array_new (FALSE, FALSE, sizeof (vae));
		if(g_str_has_prefix(s, "#")) continue; /* skip comments */

		if(g_str_has_prefix(s, "start")){
			ch_type = atoi(s+6);
			g_hash_table_insert(vaetbls, GINT_TO_POINTER(ch_type), vaet);
			while(1){
				if(-1 == getline(&s,&n,ftbl)) break; /* don't merge with next line! */										
				if(g_str_has_prefix(s,"end")) break;
				v = g_strsplit_set(s," ", 3);
				v[2]= g_strndup(v[2], strlen(v[2])-1);
				vaep->name   = &v[2];
				t = g_strndup(v[2], strlen(v[2])-1);							
				vaep->type   = atoi(v[0]);
				vaep->offset = atoi(v[1]);
				free(v[0]);
				free(v[1]);				
				g_array_append_vals (vaet, vaep, 1);
			}
		}			
	}
g_printf("HT size: %d.\n", g_hash_table_size (vaetbls));

if(s) free(s);

fclose(ftbl);
return vaetbls;
}

int
vsd_parse_cmd_test()
{
	GHashTable* vaetabs;

	if(NULL == (vaetabs = vsd_parse_cmd_file())) return 1;
	g_print("HT size: %d\n", g_hash_table_size (vaetabs));
	return 0;
	
}

/* Not used at the moment */
/*	
	static char const *le_size[] = {
	 "Very Small", "Small", "Medium", "Large", "Extra Large", "Jumbo", "Colossal",
	};
	static char const *align[] = {
	 "Left", "Center", "Right", "Justify", "Force Justify",
	};
*/	
/*------------------------*/

gboolean
vsd_cmd_tbl_free(gpointer key, gpointer value, gpointer user_data){
	g_array_free(value, TRUE);
	free(key);
return TRUE;
}
