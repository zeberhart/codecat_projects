/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: 974c0d4bbdfad41aa99480a197685052f6ebcb28 $
*/

#ifndef MAIN_H
#define MAIN_H

/* Prototypes begin here */
int main(int argc, char **argv);
/* Prototypes end here */

#endif /* !MAIN_H */
