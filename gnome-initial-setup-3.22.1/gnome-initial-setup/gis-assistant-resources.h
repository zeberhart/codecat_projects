#ifndef __RESOURCE_gis_assistant_H__
#define __RESOURCE_gis_assistant_H__

#include <gio/gio.h>

extern GResource *gis_assistant_get_resource (void);
#endif
