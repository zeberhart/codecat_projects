%define name        gpr
%define version     0.10
%define rel         1
%define prefix      /usr
%define pixmapsdir  @PACKAGE_PIXMAPS_DIR@
%define scriptsdir  @PACKAGE_SCRIPTS_DIR@

Name: %{name}
Version: %{version}
Release: %{rel}
Summary: GUI to configure printer-specific options for PostScript print jobs.
License: GPL
Group: Applications/Productivity
Requires: libppd ppdfilt

Source0: %{name}-%{version}.tar.gz
BuildRoot: /tmp/%{name}-root

%description
GPr is a graphical interface that provides for easy configuration of
printer-specific options. GPr interfaces with a PostScript printer's
PPD file to create a user-interface of configurable options. Based
upon user choice, the device-specific option code is then inserted
into the PostScript job and sent to the printer.

%prep
%setup

%build
./configure --prefix=%{_prefix}
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_prefix}
make DESTDIR="$RPM_BUILD_ROOT" install

find "$RPM_BUILD_ROOT" -type f -print \
  | sed -e "s@^${RPM_BUILD_ROOT}@@" > rpm.filelist

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f rpm.filelist

%changelog
* Thu Nov 16 2000 Tom Dyas <tdyas@valinux.com>
- Make the spec more portable by using RPM macros.

* Fri Jun 16 2000 Thomas Hubbell <thubbell@compumetric.com>
- Updated version number for major Netscape bug-fix build

* Tue Jun 06 2000 Thomas Hubbell <thubbell@compumetric.com>
- Built with support for lpr -o options
- Incorporates latest bug fixes

* Mon Jun 05 2000 Ben Woodard <ben@valinux.com>
- Added a couple of dependencies
- Built against libppd 0.4








