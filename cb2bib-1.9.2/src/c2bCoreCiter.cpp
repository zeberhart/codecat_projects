/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 *
 *   The LyX pipe procedure in citeToLyXPipe has been adapted from Tellico
 *   (Tellico (C) 2003-2005 by Robby Stephenson)
 ***************************************************************************/
#include "c2bCoreCiter.h"

#include "c2bFileDialog.h"

#include <settings.h>

#include <QApplication>
#include <QClipboard>
#include <QMessageBox>
#include <QTextStream>

// fifo to lyx
#include <fcntl.h>

#include <unistd.h>


c2bCoreCiter::c2bCoreCiter(QWidget* parentw) : QObject(parentw)
{
    _parentWP = parentw;
    _settingsP = settings::instance();
    _settingsP->setDefaultValue("c2bCoreCiter/LyXPipe", QDir::cleanPath(QDir::homePath() + QDir::separator() + ".lyx/lyxpipe.in"));
    _lyxpipe = _settingsP->value("c2bCoreCiter/LyXPipe").toString();
}

c2bCoreCiter::~c2bCoreCiter()
{}


void c2bCoreCiter::cite(const QStringList& keys) const
{
    QByteArray pipe = QFile::encodeName(_lyxpipe);
    if (QFile::exists(pipe))
        citeToLyXPipe(keys);
    else
        citeToClipboard(keys);
}

void c2bCoreCiter::citeToClipboard(const QStringList& keys) const
{
    const QString c("\\cite{" + keys.join(", ").trimmed() + '}');
    QClipboard* cb = QApplication::clipboard();
    cb->setText(c, QClipboard::Clipboard);
}

void c2bCoreCiter::citeToLyXPipe(const QStringList& keys) const
{
    // This procedure was adapted from Tellico
    // Tellico (C) 2003-2005 by Robby Stephenson
    QByteArray pipe = QFile::encodeName(_lyxpipe);
    const QString errorStr(tr("Unable to write to the server pipe at '%1'.").arg(QString(pipe)));

    if (!QFile::exists(pipe))
    {
        QMessageBox::warning(_parentWP, tr("Warning - cb2Bib"), errorStr, QMessageBox::Ok);
        return;
    }

    int pipeFd = ::open(pipe, O_WRONLY);
    QFile file(QString::fromUtf8(pipe));
    if (file.open(pipeFd, QIODevice::WriteOnly))
    {
        // pybliographer uses comma-space, and pyblink expects the space there
        const QString c(keys.join(", ").trimmed());
        QTextStream st(&file);
        st << QString::fromLatin1("LYXCMD:cb2bib:citation-insert:%1\n").arg(c).toLatin1();
        file.flush();
        file.close();
    }
    else
        QMessageBox::warning(_parentWP, tr("Warning - cb2Bib"), errorStr, QMessageBox::Ok);
    ::close(pipeFd);
}

void c2bCoreCiter::setLyXPipe()
{
    const QString new_pn(c2bFileDialog::getSystemFilename(_parentWP, "Select LyX pipe", _lyxpipe, "All (*)"));
    if (!new_pn.isEmpty())
    {
        _lyxpipe = new_pn;
        _settingsP->setValue("c2bCoreCiter/LyXPipe", _lyxpipe);
    }
}
