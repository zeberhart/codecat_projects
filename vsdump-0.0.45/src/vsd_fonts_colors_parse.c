/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * vsdump: test program to dump and parse content of vsd file
 *
 * Copyright (C) 2006-2007	Valek Filippov (frob@df.ru)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 3 or later of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 */
#include <gsf/gsf-utils.h>
#include "vsdump.h"
#include "vsd_fonts_colors_parse.h"

typedef struct{
  guint32 	flags;   		/* 5 - 8 */ 	
  guint8		name[64];		/* 9 - 72 */
  guint32	uc_ranges[4];	/* 73-76, 77-80, 81-84, 85-88 */
  guint32 	chset[2];		/* 89-92, 93-96 */
  guint8  	panos[10];		/* 97 - 107 */
} fd;

static fd *
vsd_parse_fontdescriptor (GByteArray *input)
{
	fd fd1, *fdscr;
	int i;
	
	fdscr = &fd1;
	 
	fdscr->flags = GSF_LE_GET_GUINT32(input->data + 4);	
	for (i = 0; i < 64; i++){
		fdscr->name[i] = input->data[8 + i];
	}	
	for (i = 0; i < 4; i++){
		fdscr->uc_ranges[i] = GSF_LE_GET_GUINT32(input->data + 72 + (4*i));
	}	
	fdscr->chset[0] = GSF_LE_GET_GUINT32(input->data + 88);
	fdscr->chset[1] = GSF_LE_GET_GUINT32(input->data + 92);
	for (i = 0; i < 10; i++){
		fdscr->panos[i] = GSF_LE_GET_GUINT8(input->data + 96 + i);
	}
		
	return fdscr;
}


int
vsd_dump_fontdescriptor(GByteArray *decomp, FILE *fstrm, int *fdindex)
{
		int j;
		fd *fdscr;
		
		fdscr = vsd_parse_fontdescriptor (decomp);
		fprintf(fstrm, "FaceName ID=%d\nName=\"",*fdindex);
		for (j = 0; j < 64; j++){				
				if(fdscr->name[j])		/* skips 0x0 bytes of utf-8 font names */
		 			fprintf(fstrm,"%c", fdscr->name[j]);
				}
				 	
		fprintf(fstrm,"\"\nUnicodeRanges=\"%d %d %d %d\"\n", fdscr->uc_ranges[0],fdscr->uc_ranges[1],fdscr->uc_ranges[2],fdscr->uc_ranges[3]);
		fprintf(fstrm,"CharSets=\"%d %d\"\n", fdscr->chset[0], fdscr->chset[1]);
		fprintf(fstrm,"Panos=\"");
					 
		for (j = 0; j < 10; j++){
			fprintf(fstrm,"%d ",fdscr->panos[j]);
		}
					 	
		fprintf(fstrm,"\nFlags=\"%d\"", fdscr->flags);
		(*fdindex)++;

		return 1;
}

int
vsd_dump_colorentries (GByteArray *input, FILE *fstrm)
{
	guint8 	num; /* num of entries */
	int		i;	
	
	num = input->data[6];
	
	for (i =0; i < num; i++){
		fprintf(fstrm, "ColorEntry IX=\"%d\"  RGB=\"#%02x%02x%02x\"\n",i,input->data[8+(i*4)],input->data[9+(i+4)],input->data[10+(i*4)]);
	}
	return 0;	 
}
