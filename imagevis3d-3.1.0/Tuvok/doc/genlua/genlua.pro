TEMPLATE       = app
win32:TEMPLATE = vcapp
CONFIG = exceptions largefile qt rtti static stl warn_on
QT += core opengl
TARGET = bluebook
DEPENDPATH = .
INCLUDEPATH  = ../../
INCLUDEPATH += /usr/include/lua5.2
QMAKE_LIBDIR += ../../Build
QMAKE_LIBDIR += ../../IO/expressions
LIBS             = -lTuvok -ltuvokexpr -lboost_serialization
unix:LIBS       += -lz
unix:LIBS       += -llua5.2 -lGLEW -ltiff -lbz2 -llzma -llz4
win32:LIBS      += shlwapi.lib
macx:LIBS       += -stdlib=libc++
macx:LIBS       += -mmacosx-version-min=10.7
macx:LIBS       += -framework CoreFoundation
unix:!macx:LIBS += -lGLU -lGL
# don't complain about not understanding OpenMP pragmas.
QMAKE_CXXFLAGS      += -Wno-unknown-pragmas
macx:QMAKE_CXXFLAGS += -stdlib=libc++
macx:QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
unix:QMAKE_CXXFLAGS += -std=c++0x
unix:QMAKE_CXXFLAGS += -fno-strict-aliasing
unix:QMAKE_CFLAGS   += -fno-strict-aliasing
!macx:unix:QMAKE_LFLAGS += -fopenmp

SOURCES = \
  main.cpp
