/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <math.h>

# include "syr_usertype.h"
# include "syr_const.h"
# include "syr_bd.h"
# include "syr_hmt_libmat.h"
# include "syr_parall.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_proto.h"
# include "syr_hmt_proto.h"

#ifdef _SYRTHES_MPI_
# include "mpi.h"
MPI_Status status;
#endif

struct Performances perfo;
struct Affichages affich;
 
extern char nomfdf[CHLONG];
extern char nomcorrr[CHLONG];
extern int nbVar;

extern char nomresu[CHLONG],nomresuc[CHLONG];
extern char nomresur[CHLONG],nomresucr[CHLONG];
extern char nomhisto[CHLONG],nomdata[CHLONG];
extern char nomadd[CHLONG];
extern char nomcplcfdr[CHLONG],nomcplcfdc[CHLONG]; 
extern FILE  *fdata,*fresu,*fchro,*fresur,*fchror,*fhisto,*fadd;
extern FILE  *fresucplcfd,*fchrocplcfd;

extern char titre[CHLONG];
extern int lray,lhumid;

FILE *fmnx;


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Ecriture des fichiers resultats (flag=1)                             |
  |                                                                      |
  | Permet aussi de savoir s'il faut ecrire ou non un fichier additionnel|
  | la frequence est alors la meme que celle des fichiers chrono, mais   |
  | il n'y a pas d'interpolation (flag=0)                                |
  |======================================================================| */
void ecrire_resu(struct GestionFichiers gestionfichiers,
		 struct PasDeTemps pasdetemps,struct Maillage maillnodes,
		 struct Variable variable,
		 struct Maillage maillnodray,struct ProphyRay phyray,
		 double *tmpray, double **firay,struct SDparall_ray sdparall_ray,
		 struct Humid humid,
		 struct ConstPhyhmt constphyhmt,struct ConstMateriaux *constmateriaux,
         struct Climcfd scoupf,struct Cfd cfd,
		 struct Travail trav,struct Travail trave)
{
  int i,j,n,nb,nmat;
  char chi[5],chn[20];
  double psat,valt,valpv;
  double *t,*pv,*xhr,*tauv;

  t=variable.var[variable.adr_t];
  if (lhumid)
    {
      pv=variable.var[variable.adr_pv];
      xhr=trav.tab[0];
      tauv=trave.tab[0];
    }
 
  /* resultats conduction */
  /* -------------------- */
  ecrire_entete(&fresu,nomresu,pasdetemps.ntsyr,pasdetemps.rdtts,pasdetemps.tempss);
  ecrire_var(fresu,maillnodes.npoin,variable.var[variable.adr_t],"TEMPERATURE",3);
  if (variable.nbvar>1) ecrire_var(fresu,maillnodes.npoin,variable.var[variable.adr_pv],"PRESS_VAP",3);
  if (variable.nbvar>2) ecrire_var(fresu,maillnodes.npoin,variable.var[variable.adr_pt],"PRESS_TOT",3);
  
  if (gestionfichiers.champmax) 
    {
      ecrire_var(fresu,maillnodes.npoin,variable.var[variable.adr_tmin],"TEMP_MIN",3);
      ecrire_var(fresu,maillnodes.npoin,variable.var[variable.adr_tmax],"TEMP_MAX",3);
      if (variable.nbvar>1){
	ecrire_var(fresu,maillnodes.npoin,variable.var[variable.adr_pvmin],"PV_MIN",3);
	ecrire_var(fresu,maillnodes.npoin,variable.var[variable.adr_pvmax],"PV_MAX",3);
      }
      if (variable.nbvar>2){
	ecrire_var(fresu,maillnodes.npoin,variable.var[variable.adr_ptmin],"PT_MIN",3);
	ecrire_var(fresu,maillnodes.npoin,variable.var[variable.adr_ptmax],"PT_MAX",3);
      }	
    }

  if (lhumid){
    for (i=0;i<maillnodes.npoin;i++) 
      {
	psat=fphyhmt_fpsat(t[i]+tkel);
	xhr[i]=pv[i]/psat;
      }
    ecrire_var(fresu,maillnodes.npoin,xhr,"HUMID_REL",3);

    for (n=0;n<maillnodes.nelem;n++)
      { 
	nmat=humid.mat[n];
	valt=var_element_moy(n,maillnodes,variable.var[variable.adr_t]);
	valpv=var_element_moy(n,maillnodes,variable.var[variable.adr_pv]);
	psat=fphyhmt_fpsat(valt+tkel);
	tauv[n]=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],valpv,psat,valt+tkel); 
      }
    ecrire_var(fresu,maillnodes.nelem,tauv,"TAUV",2);
  }

/* faut faire le tauv ici  et reporter le tout dans les chrono.... */

  /* resultats rayonnement */
  /* --------------------- */
    if (lray && gestionfichiers.resu_r==1 && sdparall_ray.rangray>-1)
    {
      ecrire_entete(&fresur,nomresur,pasdetemps.ntsyr,pasdetemps.rdtts,pasdetemps.tempss);
      ecrire_var(fresur,maillnodray.nelem,tmpray,"T_RAYT",2);
      for (i=0;i<phyray.bandespec.nb;i++)
	{
	  sprintf(chi,"%d",i);  strcpy(chn,"FLUX_RAYT_"); strcat(chn,chi);
	  ecrire_var(fresur,maillnodray.nelem,firay[i],chn,2);
	}
    }

    /* resultats zones de couplage CFD */
    /* ------------------------------- */
#ifdef _SYRTHES_CFD_ 
    double *tab;
    for (nb=n=0; n<cfd.n_couplings; n++) nb+=scoupf.nelem[n];
    tab=(double*)malloc(nb*sizeof(double));
    ecrire_entete(&fresucplcfd,nomcplcfdr,pasdetemps.ntsyr,pasdetemps.rdtts,pasdetemps.tempss);
    for (nb=n=0; n<cfd.n_couplings; n++)
      for (i=0;i<scoupf.nelem[n];i++)  {tab[nb]=scoupf.tfluid[n][i]; nb++;}
    ecrire_var(fresucplcfd,nb,tab,"T_Clim_CFD",2);
    for (nb=n=0; n<cfd.n_couplings; n++)
      for (i=0;i<scoupf.nelem[n];i++)  {tab[nb]=scoupf.hfluid[n][i]; nb++;}
    ecrire_var(fresucplcfd,nb,tab,"H_Clim_CFD",2);
    free(tab);
    fflush(fresucplcfd);
#endif

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Ecriture des fichiers resultats (flag=1)                             |
  |                                                                      |
  | Permet aussi de savoir s'il faut ecrire ou non un fichier additionnel|
  | la frequence est alors la meme que celle des fichiers chrono, mais   |
  | il n'y a pas d'interpolation (flag=0)                                |
  |======================================================================| */
int ecrire_chrono(int flag,
		  struct GestionFichiers gestionfichiers,
		  struct PasDeTemps pasdetemps,
		  struct Maillage maillnodes,struct MaillageBord maillnodebord,
		  struct Variable variable,
		  struct Maillage maillnodray,struct ProphyRay phyray,
		  double *tmpray, double **firay,struct SDparall_ray sdparall_ray,
		  struct Humid humid,
		  struct ConstPhyhmt constphyhmt,struct ConstMateriaux *constmateriaux,
          struct Climcfd scoupf,struct Cfd cfd,
		  struct Travail trav,struct Travail trave)

{
  int i,n,nb,ok=0,nmat;
  double alpha,r,tt;
  static int prem=1;
  static double tsortie_prec=-1,tsortie_avenir=-1;
  static int numlist;
  char chi[5],chn[20];
  double *trav1,*t,*pv,*xhr,*tauv;
  double psat,valt,valpv,xhr1,xhr2,psat1,psat2,valt1,valt2,valpv1,valpv2,tauv1,tauv2;

  if (flag!=0){
    t=variable.var[variable.adr_t];
    trav1=trav.tab[0];
    if (lhumid){
      pv=variable.var[variable.adr_pv];
      xhr=trav.tab[1];
      tauv=trave.tab[0];
    }
  }

  /* initialisations au premier passage */
  /* ---------------------------------- */
  if (pasdetemps.premier)
    { 
      if (gestionfichiers.freq_chrono_s>0)
	{
	  tsortie_prec=0;
	  tsortie_avenir=gestionfichiers.freq_chrono_s;
	}
      else if  (gestionfichiers.freq_chrono_list_nb>0)
	{
	  tsortie_prec=0;
	  tsortie_avenir=gestionfichiers.freq_chrono_list_s[0];
	  numlist=0;
	}
    }
  else if (pasdetemps.suiteprem)
    {
      if (gestionfichiers.freq_chrono_s>0)
	{
	  tsortie_prec=(int)(pasdetemps.tempss/gestionfichiers.freq_chrono_s)*gestionfichiers.freq_chrono_s;
	  tsortie_avenir=tsortie_prec+gestionfichiers.freq_chrono_s;
	}
      else if (gestionfichiers.freq_chrono_list_nb>0)
	{
	  i=0;
	  while (gestionfichiers.freq_chrono_list_s[i]<pasdetemps.tempss) i++;
	  numlist=i;
	  tsortie_avenir=gestionfichiers.freq_chrono_list_s[i];
	  if (i>0) tsortie_prec=gestionfichiers.freq_chrono_list_s[i-1]; 
	  else  tsortie_prec=0;
	}
    }


  /* si c'est juste pour savoir s'il faut faire des sorties additionnelles ou non */
  /* ---------------------------------------------------------------------------- */
  if (flag==0)
    {
      if ( gestionfichiers.freq_chrono_nt>0 && !(pasdetemps.ntsyr%gestionfichiers.freq_chrono_nt))
	return 1;
      else if (tsortie_avenir>0  && fabs(pasdetemps.tempss-tsortie_avenir)<1.e-7)
	return 1;
      else if (pasdetemps.tempss-pasdetemps.rdtts<tsortie_avenir && tsortie_avenir<pasdetemps.tempss)
	return 2;
      else
	return 0;
    }

  
  /* sorties chrono */
  /* -------------- */


 boucle:

  if ( gestionfichiers.freq_chrono_nt>0 && !(pasdetemps.ntsyr%gestionfichiers.freq_chrono_nt))
    ok=1;
  else if (tsortie_avenir>0  && fabs(pasdetemps.tempss-tsortie_avenir)<1.e-7)
    ok=1;

/*   else if (tsortie_prec<pasdetemps.tempss && pasdetemps.tempss<tsortie_avenir) */
  else if (pasdetemps.tempss-pasdetemps.rdtts<tsortie_avenir && tsortie_avenir<pasdetemps.tempss)
    {
/*       alpha=(tsortie_avenir-pasdetemps.tempss-pasdetemps.rdtts)/tsortie_avenir; */
      alpha=1-(pasdetemps.tempss-tsortie_avenir)/pasdetemps.rdtts;
      ok=2;
    }
  else
    ok=0;

  
  if (!ok) return 0;


  if (ok==1)
    {
      ecrire_entete(&fchro,nomresuc,pasdetemps.ntsyr,pasdetemps.rdtts,pasdetemps.tempss);
      ecrire_var(fchro,maillnodes.npoin,variable.var[variable.adr_t],"TEMPERATURE",3);
      if (variable.nbvar>1) ecrire_var(fchro,maillnodes.npoin,variable.var[variable.adr_pv],"PRESS_VAP",3);
      if (variable.nbvar>2) ecrire_var(fchro,maillnodes.npoin,variable.var[variable.adr_pt],"PRESS_TOT",3);

      if (gestionfichiers.champmax) 
	{
	  ecrire_var(fchro,maillnodes.npoin,variable.var[variable.adr_tmin],"TEMP_MIN",3);
	  ecrire_var(fchro,maillnodes.npoin,variable.var[variable.adr_tmax],"TEMP_MAX",3);
	  if (variable.nbvar>1){
	    ecrire_var(fchro,maillnodes.npoin,variable.var[variable.adr_pvmin],"PV_MIN",3);
	    ecrire_var(fchro,maillnodes.npoin,variable.var[variable.adr_pvmax],"PV_MAX",3);
	  }
	  if (variable.nbvar>2){
	    ecrire_var(fchro,maillnodes.npoin,variable.var[variable.adr_ptmin],"PT_MIN",3);
	    ecrire_var(fchro,maillnodes.npoin,variable.var[variable.adr_ptmax],"PT_MAX",3);
	  }
	}

      if (lhumid){
	for (i=0;i<maillnodes.npoin;i++) 
	  {
	    psat=fphyhmt_fpsat(t[i]+tkel);
	    xhr[i]=pv[i]/psat;
	  }
	ecrire_var(fchro,maillnodes.npoin,xhr,"HUMID_REL",3);
	
	for (n=0;n<maillnodes.nelem;n++)
	  { 
	    nmat=humid.mat[n];
	    valt=var_element_moy(n,maillnodes,variable.var[variable.adr_t]);
	    valpv=var_element_moy(n,maillnodes,variable.var[variable.adr_pv]);
	    psat=fphyhmt_fpsat(valt+tkel);
	    tauv[n]=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],valpv,psat,valt+tkel); 
	  }
	ecrire_var(fchro,maillnodes.nelem,tauv,"TAUV",2);
      }

      if (lray && gestionfichiers.resu_r==1 && sdparall_ray.rangray>-1)
	{
	  ecrire_entete(&fchror,nomresucr,pasdetemps.ntsyr,pasdetemps.rdtts,pasdetemps.tempss);
	  ecrire_var(fchror,maillnodray.nelem,tmpray,"T_RAYT",2);
	  for (i=0;i<phyray.bandespec.nb;i++)
	    {
	      sprintf(chi,"%d",i);  strcpy(chn,"FLUX_RAYT_"); strcat(chn,chi);
	      ecrire_var(fchror,maillnodray.nelem,firay[i],chn,2);
	    }
	  fflush(fchror);
	}

#ifdef _SYRTHES_CFD_ 
	      double *tab;
	      for (nb=n=0; n<cfd.n_couplings; n++) nb+=scoupf.nelem[n];
	      tab=(double*)malloc(nb*sizeof(double));
	      ecrire_entete(&fchrocplcfd,nomcplcfdc,pasdetemps.ntsyr,pasdetemps.rdtts,pasdetemps.tempss);
	      for (nb=n=0; n<cfd.n_couplings; n++)
		for (i=0;i<scoupf.nelem[n];i++)  {tab[nb]=scoupf.tfluid[n][i]; nb++;}
	      ecrire_var(fchrocplcfd,nb,tab,"T_Clim_CFD",2);
	      for (nb=n=0; n<cfd.n_couplings; n++)
		for (i=0;i<scoupf.nelem[n];i++)  {tab[nb]=scoupf.hfluid[n][i]; nb++;}
	      ecrire_var(fchrocplcfd,nb,tab,"H_Clim_CFD",2);
	      free(tab);
	      fflush(fchrocplcfd);
#endif

    }

  else if (ok==2) /* il faut interpoler les champs */
    {
      tt=tsortie_avenir;

      ecrire_entete(&fchro,nomresuc,pasdetemps.ntsyr,pasdetemps.rdtts,tt);

      for (i=0;i<maillnodes.npoin;i++) 
	trav1[i]=alpha*variable.var[variable.adr_t][i]+(1-alpha)*variable.var[variable.adr_t_m1][i];
      ecrire_var(fchro,maillnodes.npoin,trav1,"TEMPERATURE",3);

      if (variable.nbvar>1){
	for (i=0;i<maillnodes.npoin;i++) 
	  trav1[i]=alpha*variable.var[variable.adr_pv][i] + (1-alpha)*variable.var[variable.adr_pv_m1][i];
	ecrire_var(fchro,maillnodes.npoin,trav1,"PRESS_VAP",3);
      }

      if (variable.nbvar>2){
	for (i=0;i<maillnodes.npoin;i++) 
	  trav1[i]=alpha*variable.var[variable.adr_pt][i]+(1-alpha)*variable.var[variable.adr_pt_m1][i];
	ecrire_var(fchro,maillnodes.npoin,trav1,"PRESS_TOT",3);
      }

      if (lhumid){
	for (i=0;i<maillnodes.npoin;i++) 
	  {
	    psat1=fphyhmt_fpsat(variable.var[variable.adr_t][i]+tkel);
	    psat2=fphyhmt_fpsat(variable.var[variable.adr_t_m1][i]+tkel);
	    xhr1=variable.var[variable.adr_pv][i]/psat1;  
	    xhr2=variable.var[variable.adr_pv_m1][i]/psat2;  
	    xhr[i]=alpha*xhr1+(1-alpha)*xhr2;
	  }
	ecrire_var(fchro,maillnodes.npoin,xhr,"HUMID_REL",3);
	
	for (n=0;n<maillnodes.nelem;n++)
	  { 
	    nmat=humid.mat[n];
	    valt1=var_element_moy(n,maillnodes,variable.var[variable.adr_t]);
	    valt2=var_element_moy(n,maillnodes,variable.var[variable.adr_t_m1]);
	    valpv1=var_element_moy(n,maillnodes,variable.var[variable.adr_pv]);
	    valpv2=var_element_moy(n,maillnodes,variable.var[variable.adr_pv_m1]);
	    psat1=fphyhmt_fpsat(valt1+tkel);
	    psat2=fphyhmt_fpsat(valt2+tkel);
	    tauv1=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],valpv1,psat1,valt1+tkel); 
	    tauv2=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],valpv2,psat2,valt2+tkel); 
	    tauv[i]=alpha*tauv1+(1-alpha)*tauv2;
	  }
	ecrire_var(fchro,maillnodes.nelem,tauv,"TAUV",2);
      }

    }

  if (ok==1 || ok==2)
    {
      fflush(fchro);
      if (syrglob_nparts==1 ||syrglob_rang==0)
	if (SYRTHES_LANG == FR)	       printf(" --> Ecriture du fichier transitoire (.rdt)\n");
	else if (SYRTHES_LANG == EN)   printf(" --> Writing transient file (.rdt)\n");
    }


  if (gestionfichiers.freq_chrono_s>0)
    {
      tsortie_prec+=gestionfichiers.freq_chrono_s;
      tsortie_avenir+=gestionfichiers.freq_chrono_s;
      if (tsortie_avenir<=pasdetemps.tempss) goto boucle;
    }
  else if (gestionfichiers.freq_chrono_list_nb>0)
    {
      numlist++;
      tsortie_prec=tsortie_avenir;
      tsortie_avenir=gestionfichiers.freq_chrono_list_s[numlist];
      if (tsortie_avenir<=pasdetemps.tempss) goto boucle;
    }

  return 1;

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Ecriture des fichiers d'historiques                                  |
  |======================================================================| */
void ecrire_histo(struct PasDeTemps pasdetemps,struct Maillage maillnodes,
		  struct Histo histos,
		  struct Variable variable,
		  struct Humid humid,
		  struct ConstPhyhmt constphyhmt,struct ConstMateriaux *constmateriaux)
{
  int i,ok;
  double alpha;
  static double thisso;
  static double tsortie_prec=-1,tsortie_avenir=-1;
  static int numlist;

  /* initialisations au premier passage */
  /* ---------------------------------- */
/*   if (pasdetemps.premier)  thisso=0; */
/*   else if (pasdetemps.suiteprem) thisso=(int)(pasdetemps.tempss/histos.freq)*histos.freq; */


  if (pasdetemps.premier)
    { 
      if (histos.freq>0)
	{
	  tsortie_prec=0;
	  tsortie_avenir=histos.freq;
	}
      else if  (histos.freq_list_nb>0)
	{
	  tsortie_prec=0;
	  tsortie_avenir=histos.freq_list_s[0];
	  numlist=0;
	}
    }
  else if (pasdetemps.suiteprem)
    {
      if (histos.freq>0)
	{
	  tsortie_prec=(int)(pasdetemps.tempss/histos.freq)*histos.freq;
	  tsortie_avenir=tsortie_prec + histos.freq;
	}
      else if (histos.freq_list_nb>0)
	{
	  i=0;
	  while (histos.freq_list_s[i]<pasdetemps.tempss) i++;
	  numlist=i;
	  tsortie_avenir=histos.freq_list_s[i];
	  if (i>0) tsortie_prec=histos.freq_list_s[i-1]; 
	  else  tsortie_prec=0;
	}

    }

  /* historiques */
  /* ----------- */
  
 boucle:
      
  if ( histos.freq_nt>0 && (!(pasdetemps.ntsyr%histos.freq_nt) || pasdetemps.premier || pasdetemps.suiteprem))
    {
      alpha=-1;
      ok=1;
    }
  else if (tsortie_avenir>0  && fabs(pasdetemps.tempss-tsortie_avenir)<1.e-7)
    {
      alpha=-1;
      ok=1;
    }
  else if (pasdetemps.tempss-pasdetemps.rdtts<tsortie_avenir && tsortie_avenir<pasdetemps.tempss)
    {
      alpha=1-(pasdetemps.tempss-tsortie_avenir)/pasdetemps.rdtts;
      ok=2;
    }
  else
    ok=0;
  
  
  if (!ok) return;
  
  if (ok==1) /* ecriture sans interpolation (on est au bon moment) */
    ecrire_histo2(&fhisto,nomhisto,pasdetemps.tempss,alpha,histos,maillnodes,variable,1,
		  humid,constphyhmt,constmateriaux);

  else if (ok==2) /* ecriture avec interpolation  */
    ecrire_histo2(&fhisto,nomhisto,tsortie_avenir,alpha,histos,maillnodes,variable,1,
		  humid,constphyhmt,constmateriaux);

  fflush(fhisto);

  if (histos.freq>0)
    {
      tsortie_prec+=histos.freq;
      tsortie_avenir+=histos.freq;
      if (tsortie_avenir<=pasdetemps.tempss) goto boucle;
    }
  else if (histos.freq_list_nb>0)
    {
      numlist++;
      tsortie_prec=tsortie_avenir;
      tsortie_avenir=histos.freq_list_s[numlist];
      if (tsortie_avenir<=pasdetemps.tempss) goto boucle;
    }


/*   if (histos.actif && pasdetemps.tempss-thisso>=histos.freq) */
/*     { */
/*       ecrire_histo2(&fhisto,nomhisto,pasdetemps.tempss,histos,maillnodes,variable,1); */
/*       thisso += histos.freq;  */
/*     }   */
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void ecrire_geom(char *nomfich,
	 	 struct Maillage *maillnodes,struct MaillageBord *maillnodebord)
{
  FILE  *fich;
  int i,j,nelembord,bidon=0;
  int nrefp_free,nrefe_free;
  int *ne0,*ne1,*ne2,*ne3,*nrefe,*nrefp,**nodes,**nodebord,*nrefb;
  double z=0,*c0,*c1,*c2,**coords;

  if ((fich=fopen(nomfich,"w")) == NULL)
    {
      if (SYRTHES_LANG == FR)
	printf("Impossible d'ouvrir le fichier %s\n",nomfich);
      else if (SYRTHES_LANG == EN)
	printf("Impossible to opn the file %s\n",nomfich);
      syrthes_exit(1);
    }

  nodes=maillnodes->node;
  coords=maillnodes->coord;

  nrefe=maillnodes->nrefe;  nrefe_free=0;
  if (!nrefe) {
    nrefe_free=1;
    nrefe=(int*)malloc(maillnodes->nelem*sizeof(int));
    for (i=0;i<maillnodes->nelem;i++) nrefe[i]=0;
  }

  nrefp=maillnodes->nref;   nrefp_free=0;
  if (!nrefp) {
    nrefp_free=1;
    nrefp=(int*)malloc(maillnodes->npoin*sizeof(int));
    for (i=0;i<maillnodes->npoin;i++) nrefp[i]=0;
  }


  nelembord=0;
  if (maillnodebord) {
    nodebord=maillnodebord->node;
    nrefb=maillnodebord->nrefe;
    nelembord=maillnodebord->nelem;
  }


  fprintf(fich,"C*V4.0*******************************************C\n");
  fprintf(fich,"C            FICHIER GEOMETRIQUE SYRTHES         C\n");
  fprintf(fich,"C************************************************C\n");
  fprintf(fich,"C  DIMENSION = %1d\n",maillnodes->ndim);
  fprintf(fich,"C  DIMENSION DES ELTS = %1d\n",maillnodes->ndiele);
  fprintf(fich,"C  NOMBRE DE NOEUDS = %12d\n",maillnodes->npoin);
  fprintf(fich,"C  NOMBRE D'ELEMENTS =%12d\n",maillnodes->nelem);
  fprintf(fich,"C  NOMBRE D'ELEMENTS DE BORD =%12d\n",nelembord);
  fprintf(fich,"C  NOMBRE DE NOEUDS PAR ELEMENT = %3d\n",maillnodes->ndmat);
  fprintf(fich,"C************************************************C\n");

  fprintf(fich,"C\nC$ RUBRIQUE = NOEUDS\nC\n");
  if (maillnodes->ndim==2)
    for (i=0,c0=*coords,c1=*(coords+1);i<maillnodes->npoin;i++,c0++,c1++) 
      fprintf(fich,"%10d%4d %14.7e %14.7e %14.7e \n", i+1,*(nrefp+i),*c0,*c1,z);
  else
    for (i=0,c0=*coords,c1=*(coords+1),c2=*(coords+2);i<maillnodes->npoin;i++,c0++,c1++,c2++) 
      fprintf(fich,"%10d%4d %14.7e %14.7e %14.7e \n",i+1,*(nrefp+i),*c0,*c1,*c2);



  fprintf(fich,"C\nC$ RUBRIQUE = ELEMENTS\nC\n");
  if (maillnodes->ndmat==2)
    for (i=0,ne0=*(nodes),ne1=*(nodes+1);i<maillnodes->nelem;i++,ne0++,ne1++) 
      fprintf(fich,"%12d%4d%10d%10d\n",i+1,*(nrefe+i),*ne0+1,*ne1+1);

  else if (maillnodes->ndmat==3)
    for (i=0,ne0=*(nodes),ne1=*(nodes+1),ne2=*(nodes+2);i<maillnodes->nelem;i++,ne0++,ne1++,ne2++) 
      fprintf(fich,"%12d%4d%10d%10d%10d\n",i+1,*(nrefe+i),*ne0+1,*ne1+1,*ne2+1);

  else if (maillnodes->ndmat==4)
    for (i=0,ne0=*(nodes),ne1=*(nodes+1),ne2=*(nodes+2),ne3=*(nodes+3);
	 i<maillnodes->nelem;i++,ne0++,ne1++,ne2++,ne3++) 
      fprintf(fich,"%12d%4d%10d%10d%10d%10d\n",i+1,*(nrefe+i),*ne0+1,*ne1+1,*ne2+1,*ne3+1);

  else 
    {
      if (SYRTHES_LANG == FR)
	printf("\n ERREUR ecrire_geom : type d'elements inconnus\n");
      else if (SYRTHES_LANG == EN)
	printf("\n ERROR ecrire_geom : unknown element type\n");
      syrthes_exit(1);
    }


  if (maillnodebord)
    {
      fprintf(fich,"C\nC$ RUBRIQUE = ELEMENTS DE BORD\nC\n");
      if (maillnodebord->ndmat==2)
	for (i=0,ne0=*(nodebord),ne1=*(nodebord+1);i<maillnodebord->nelem;i++,ne0++,ne1++) 
	  fprintf(fich,"%12d%4d%10d%10d\n",i+1,*(nrefb+i),*ne0+1,*ne1+1);
      
      else if (maillnodebord->ndmat==3)
	for (i=0,ne0=*(nodebord),ne1=*(nodebord+1),ne2=*(nodebord+2);i<maillnodebord->nelem;i++,ne0++,ne1++,ne2++) 
	  fprintf(fich,"%12d%4d%10d%10d%10d\n",i+1,*(nrefb+i),*ne0+1,*ne1+1,*ne2+1);
      else 
	{
	  if (SYRTHES_LANG == FR)
	    printf("\n ERREUR ecrire_geom : type d'elements de bord inconnus\n");
	  else if (SYRTHES_LANG == EN)
	    printf("\n ERROR ecrire_geom : unknown boundary element type\n");
	  syrthes_exit(1);
	}
    }
   
  if (nrefe_free) free(nrefe);
  if (nrefp_free) free(nrefp);


  fclose(fich);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void ecrire_entete(FILE  **fich,char *nomfich,
		   int ntsyr,double rdtts,double tempss)
{
   if (!(*fich))
    if ((*fich=fopen(nomfich,"w")) == NULL)
      {
	if (SYRTHES_LANG == FR)
	  printf("Impossible d'ouvrir le fichier %s\n",nomfich);
	else if (SYRTHES_LANG == EN)
	  printf("Impossible to open the file %s\n",nomfich);
	syrthes_exit(1);
      }
 

  fprintf(*fich,"***SYRTHES V4.3**************************************************************************************\n");
  fprintf(*fich,"***%s\n",titre);
  fprintf(*fich,"*****************************************************************************************************\n");
  fprintf(*fich,"***NTSYR= %12d  ***TEMPS= %25.17e    ***DT= %25.17e\n",ntsyr,tempss,rdtts);
  fprintf(*fich,"*****************************************************************************************************\n");
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void ecrire_var(FILE  *fich,int nb,double *var,char *nomvar,int idiscr)
{
  int i,nv;

  /* avant le format etait 13.6 */
  nv=nb/6;
  fprintf(fich,"***VAR= %12s ***TYPE= %1d  ***NB= %12d\n",nomvar,idiscr,nb);
  for (i=0;i<nv*6;i+=6) fprintf(fich,"%16.9e %16.9e %16.9e %16.9e %16.9e %16.9e\n",
			      *(var+i),*(var+i+1),*(var+i+2),*(var+i+3),*(var+i+4),*(var+i+5));
  for (i=nv*6;i<nb;i++) fprintf(fich,"%16.9e ",*(var+i));
  if (nb-nv*6) fprintf(fich,"\n");
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Ecriture de variables dans le fichier additionnel                    |
  | (encapsulation pour appel utilisateur sans avoir a gerer le fichier  |
  |    - nb    = nbre valeurs du tableau (int)                           |
  |    - var   = variable (double*)                                      |
  |    - nomvar= variable (char* de 12 carateres max)                    |
  |    - idiscr= variable sur : 1->les elts de bord                      |
  |                             2->les elements                          |
  |                             3-> les noeuds                           |
  |======================================================================| */
void add_var_in_file(int nb,double *var,char *nomvar,int idiscr)
{
  ecrire_var(fadd,nb,var,nomvar,idiscr);
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  Ecriture des historiques pour une variable de dim 2                 |
  |======================================================================| */
void ecrire_histo2(FILE **fich,char *nomfich,double tempsreel,double alpha,
		   struct Histo histo,
		   struct Maillage maill,struct Variable variable,int type,
		   struct Humid humid,
		   struct ConstPhyhmt constphyhmt,struct ConstMateriaux *constmateriaux)
{
  int i,j,k,n,nmat;
  int zero=0;
  double val,val1,val2;
  double valt,valpv,val1t,val1pv,val2t,val2pv;
  double psat,xhr,tauv,psat1,xhr1,tauv1,psat2,xhr2,tauv2;

  if (!(*fich))
    if ((*fich=fopen(nomfich,"w")) == NULL)
      {
	if (SYRTHES_LANG == FR)
	  printf("Impossible d'ouvrir le fichier %s\n",nomfich);
	else if (SYRTHES_LANG == EN)
	  printf("Impossible to open the file %s\n",nomfich);
	syrthes_exit(1);
      }
    else
      {
	if (maill.ndim==2){
	  if (variable.nbvar==1){
	    fprintf(*fich,"#    time             Temp              x                y              [num_no] [ref_no] [num_ele]\n");
	  }
	  else if (variable.nbvar==2){
	    fprintf(*fich,"#    time             Temp              Pv               HR              tauv               x");
	    fprintf(*fich,"              y              [num_no] [ref_no]  [num_ele]\n");
	  }
	  else if (variable.nbvar==3){
	    fprintf(*fich,"#    time             Temp              Pv               Pt              HR               tauv");
	    fprintf(*fich,"              x                y             [num_no] [ref_no]  [num_ele]\n");
	  }
	}
	else if(maill.ndim==3){
	  if (variable.nbvar==1){
	    fprintf(*fich,"#    time             Temp              x                y                z             [num_no] [ref_no]  [num_ele]\n");
	  }
	  else if (variable.nbvar==2){
	    fprintf(*fich,"#    time             Temp              Pv               HR              tauv               x");
	    fprintf(*fich,"              y                z            [num_no] [ref_no]  [num_ele]\n");
	  }
	  else if (variable.nbvar==3){
	    fprintf(*fich,"#    time             Temp              Pv               Pt              HR               tauv");
	    fprintf(*fich,"              x                y              z              [num_no] [ref_no]  [num_ele]\n");
	  }
	}
      }

  /* ecriture sans interpolation temporelle */
  /* -------------------------------------- */
  if (alpha<0)
    {
      /* on ecrit : temps, variables, coord, num_noeud, ref_noeud, numelem */
      if (type==1) /* variable sur les noeuds (Temp conduction par ex) */
	{
	  for (i=0;i<histo.npoin;i++)
	    {
	      n=histo.nump[i];
	      fprintf(*fich," %16.9e",tempsreel);
	      fprintf(*fich," %16.9e",variable.var[variable.adr_t][n]);
	      if (variable.nbvar>=2) fprintf(*fich," %16.9e",variable.var[variable.adr_pv][n]);
	      if (variable.nbvar==3) fprintf(*fich," %16.9e",variable.var[variable.adr_pt][n]);
	      for (j=0;j<maill.ndim;j++) fprintf(*fich," %16.9e",maill.coord[j][n]);
	      fprintf(*fich," %12d %3d %12d\n",n+1,maill.nref[n],zero);
	    }
	  for (i=0;i<histo.npoincoo;i++) /* HISTO_COORD dans ce cas, c'est le numero de l'elt qui est ecrit */
	    {
	      n=histo.numev[i];
	      fprintf(*fich," %16.9e",tempsreel);
	      for (k=0,valt=0;k<maill.ndim+1;k++) valt+=histo.bary[k][i]*variable.var[variable.adr_t][maill.node[k][n]];
	      fprintf(*fich," %16.9e", valt);
	      if (variable.nbvar>=2) {
		for (k=0,valpv=0;k<maill.ndim+1;k++) 
		  valpv+=histo.bary[k][i]*variable.var[variable.adr_pv][maill.node[k][n]];
		fprintf(*fich," %16.9e", valpv);
	      }
	      if (variable.nbvar==3) {
		for (k=0,val=0;k<maill.ndim+1;k++) 
		  val+=histo.bary[k][i]*variable.var[variable.adr_pt][maill.node[k][n]];
		fprintf(*fich," %16.9e", val);
	      }
	      if (variable.nbvar>=2) {
		psat=fphyhmt_fpsat(valt+tkel);
		xhr=valpv/psat;
		nmat=humid.mat[n];
		tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],valpv,psat,valt+tkel); 
		fprintf(*fich," %16.9e %16.9e", xhr,tauv);
	      }
	      for (j=0;j<maill.ndim;j++) fprintf(*fich," %16.9e",histo.coord[j][i]);
	      fprintf(*fich," %12d %3d %12d\n",zero,zero,n+1);
	    }
	  for (i=0;i<histo.npoinref;i++)
	    {
	      n=histo.numpr[i];
	      fprintf(*fich," %16.9e",tempsreel);
	      fprintf(*fich," %16.9e",variable.var[variable.adr_t][n]);
	      if (variable.nbvar==3) {
		fprintf(*fich," %16.9e",variable.var[variable.adr_pv][n]);
		fprintf(*fich," %16.9e",variable.var[variable.adr_pt][n]);
	      }
	      for (j=0;j<maill.ndim;j++) fprintf(*fich," %16.9e",maill.coord[j][n]);
	      fprintf(*fich," %12d %3d %12d\n",n+1,maill.nref[n],zero);
	    }
	  fflush(*fich);

	} /* fin du type */
    } /* fin du alpha */


  /* ecriture avec interpolation temporelle */
  /* -------------------------------------- */
  else 
    {
      /* on ecrit : temps, variables, coord, num_noeud, ref_noeud, numelem */
      if (type==1) /* variable sur les noeuds (Temp conduction par ex) */
	{
	  for (i=0;i<histo.npoin;i++)
	    {
	      n=histo.nump[i];
	      fprintf(*fich," %16.9e",tempsreel);
	      val=alpha*variable.var[variable.adr_t][n]+(1-alpha)*variable.var[variable.adr_t_m1][n];
	      fprintf(*fich," %16.9e",val);
	      if (variable.nbvar==3) {
		val=alpha*variable.var[variable.adr_pv][n]+(1-alpha)*variable.var[variable.adr_pv_m1][n];
		fprintf(*fich," %16.9e",val);
		val=alpha*variable.var[variable.adr_pt][n]+(1-alpha)*variable.var[variable.adr_pt_m1][n];
		fprintf(*fich," %16.9e",val);
	      }
	      for (j=0;j<maill.ndim;j++) fprintf(*fich," %16.9e",maill.coord[j][n]);
	      fprintf(*fich," %12d %3d %12d\n",n+1,maill.nref[n],zero);
	    }
	  for (i=0;i<histo.npoincoo;i++) /* HISTO_COORD dans ce cas, c'est le numero de l'elt qui est ecrit */
	    {
	      n=histo.numev[i];
	      fprintf(*fich," %16.9e",tempsreel);
	      for (k=0,val1t=0;k<maill.ndim+1;k++) val1t+=histo.bary[k][i]*variable.var[variable.adr_t][maill.node[k][n]];
	      for (k=0,val2t=0;k<maill.ndim+1;k++) val2t+=histo.bary[k][i]*variable.var[variable.adr_t_m1][maill.node[k][n]];
	      fprintf(*fich," %16.9e", alpha*val1t+(1-alpha)*val2t);
	      if (variable.nbvar>=2) {
		for (k=0,val1pv=val2pv=0;k<maill.ndim+1;k++) {
		  val1pv += histo.bary[k][i]*variable.var[variable.adr_pv][maill.node[k][n]];
		  val2pv += histo.bary[k][i]*variable.var[variable.adr_pv_m1][maill.node[k][n]];
		}
		fprintf(*fich," %16.9e", alpha*val1+(1-alpha)*val2);
	      }
	      if (variable.nbvar==3) {
		for (k=0,val1=val2==0;k<maill.ndim+1;k++) {
		  val1+=histo.bary[k][i]*variable.var[variable.adr_pt][maill.node[k][n]];
		  val2+=histo.bary[k][i]*variable.var[variable.adr_pt_m1][maill.node[k][n]];
		}
		fprintf(*fich," %16.9e", alpha*val1+(1-alpha)*val2);
	      }
	      if (variable.nbvar>=2) {
		psat1=fphyhmt_fpsat(val1t+tkel); psat2=fphyhmt_fpsat(val2t+tkel);
		xhr1=val1pv/psat1;          xhr2=val2pv/psat2;
		nmat=humid.mat[n];
		tauv1=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],val1pv,psat1,val1t+tkel); 
		tauv2=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],val2pv,psat2,val2t+tkel);
 		fprintf(*fich," %16.9e %16.9e", alpha*xhr1+(1-alpha)*xhr2, alpha*tauv1+(1-alpha)*tauv2);
	      }

	      for (j=0;j<maill.ndim;j++) fprintf(*fich," %16.9e",histo.coord[j][i]);
	      fprintf(*fich," %12d %3d %12d\n",zero,zero,n+1);
	    }
	  for (i=0;i<histo.npoinref;i++)
	    {
	      n=histo.numpr[i];
	      fprintf(*fich," %16.9e",tempsreel);
	      val=alpha*variable.var[variable.adr_t][n]+(1-alpha)*variable.var[variable.adr_t_m1][n];
	      fprintf(*fich," %16.9e",val);
	      if (variable.nbvar==3) {
		val=alpha*variable.var[variable.adr_pv][n]+(1-alpha)*variable.var[variable.adr_pv_m1][n];
		fprintf(*fich," %16.9e",val);
		val=alpha*variable.var[variable.adr_pt][n]+(1-alpha)*variable.var[variable.adr_pt_m1][n];
		fprintf(*fich," %16.9e",val);
	      }
	      for (j=0;j<maill.ndim;j++) fprintf(*fich," %16.9e",maill.coord[j][n]);
	      fprintf(*fich," %12d %3d %12d\n",n+1,maill.nref[n],zero);
	    }
	  fflush(*fich);
	}
  
    }/* fin du alpha */
}




/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void ecrire_fdf(struct FacForme ***listefdf,double *diagfdf,
		struct PropInfini propinf,struct Horizon horiz,
		int nelray,struct SDparall_ray sdparall_ray)
{ 

  FILE *ffdf;
  int j,i,ii,nn,n,nb,id;
  struct FacForme *pff;
  int *itrav,*itravf,itaill,*itaill1,*nbfdfproc,maxtaill,nbfdf;
  double *trav,*diagtrav;

  /* ouverture des fichiers */
  /* ---------------------- */


  if (sdparall_ray.npartsray>1)
    {
#ifdef _SYRTHES_MPI_


#ifdef _SYRTHES_MPI_IO_

      MPI_File  ffdfp;
      MPI_File_open(sdparall_ray.syrthes_comm_ray,nomfdf,MPI_MODE_WRONLY | MPI_MODE_CREATE,MPI_INFO_NULL,&ffdfp);  

      /* nbre de facettes locales */
      nb=sdparall_ray.nelrayloc[sdparall_ray.rangray];

      /* ecriture du nombre de fdf local */
      /* ------------------------------- */
      MPI_File_write_ordered(ffdfp,&nb,1,MPI_INT,&status);

      /* ecriture de la diagonale des fdf */
      /* -------------------------------- */
      MPI_File_write_ordered(ffdfp,diagfdf,nb,MPI_DOUBLE,&status);
      
 
     
      /* ecriture des tailles des listes */
      /* ------------------------------- */
      /* nombre de fdf non nuls sur le proc courant */
      for (itaill=0,i=0;i<nb;i++) 
	for (n=0;n<sdparall_ray.npartsray;n++)
	  if (listefdf[n][i])
	    {
	      pff=listefdf[n][i];
	      while(pff) {itaill++; pff=pff->suivant;}
	    }

      itravf=(int*)malloc(itaill*sizeof(int));      /* numero de la facette en face */
      trav=(double*)malloc(itaill*sizeof(double));  /* valeur du fdf entre les 2 facettes */

      /* nombre de fdf nuls nuls par proc */
      nbfdfproc=(int*)malloc(nb*sdparall_ray.npartsray*sizeof(int));
      for (n=0;n<nb*sdparall_ray.npartsray;n++) nbfdfproc[n]=0;


      for (ii=0,i=0;i<nb;i++)
	{
	  for (n=0;n<sdparall_ray.npartsray;n++)
	    {	
	      nn=0;
	      if (listefdf[n][i])
		{
		  pff=listefdf[n][i];
		  while(pff) {itravf[ii]=pff->numenface; trav[ii]= pff->fdf; nn++; ii++; pff=pff->suivant;}
		}
	      nbfdfproc[i*sdparall_ray.npartsray+n]=nn;
	    }
	}


      /* ecriture du nbre de facettes visibles par proc, numeros de facettes en face et fdf */
      MPI_File_write_ordered(ffdfp,&itaill,1,MPI_INT,&status);   
      MPI_File_write_ordered(ffdfp,nbfdfproc,nb*sdparall_ray.npartsray,MPI_INT,&status);   
      MPI_File_write_ordered(ffdfp,itravf,itaill,MPI_INT,&status);
      MPI_File_write_ordered(ffdfp,trav,itaill,MPI_DOUBLE,&status);

      free(itravf);
      free(trav);
      free(nbfdfproc);

      MPI_File_close(&ffdfp);

#else
      if (syrglob_rang==0)
	{
	  if ((ffdf=fopen(nomfdf,"wb")) == NULL)
	    {
	      if (SYRTHES_LANG == FR)
		printf("Impossible d'ouvrir le fichier des facteurs de forme :%s\n",nomfdf);
	      else if (SYRTHES_LANG == EN)
		printf("Impossible to open the view factor file :%s\n",nomfdf);
	      syrthes_exit(1) ;
	    }

	  /* ecrire les donnees du proc 0 */
	  /* ============================ */

	  /* nbre de facettes locales */
	  nb=sdparall_ray.nelrayloc[sdparall_ray.rangray];
	  fwrite(&nb,sizeof(int),1,ffdf); 
 
	  /* ecriture de la diagonale des fdf */
	  fwrite(diagfdf,sizeof(double),nb,ffdf);   
	  
	  /* ecriture des tailles des listes */
	  /* nombre de fdf non nuls sur le proc courant */
	  for (itaill=0,i=0;i<nb;i++) 
	    for (n=0;n<sdparall_ray.npartsray;n++)
	      if (listefdf[n][i])
		{
		  pff=listefdf[n][i];
		  while(pff) {itaill++; pff=pff->suivant;}
		}
	  
	  itravf=(int*)malloc(itaill*sizeof(int));      /* numero de la facette en face */
	  trav=(double*)malloc(itaill*sizeof(double));  /* valeur du fdf entre les 2 facettes */
	  
	  /* nombre de fdf nuls nuls par proc */
	  nbfdfproc=(int*)malloc(nb*sdparall_ray.npartsray*sizeof(int));
	  for (n=0;n<nb*sdparall_ray.npartsray;n++) nbfdfproc[n]=0;
	  
	  
	  for (ii=0,i=0;i<nb;i++)
	    {
	      for (n=0;n<sdparall_ray.npartsray;n++)
		{	
		  nn=0;
		  if (listefdf[n][i])
		    {
		      pff=listefdf[n][i];
		      while(pff) {itravf[ii]=pff->numenface; trav[ii]= pff->fdf; nn++; ii++; pff=pff->suivant;}
		    }
		  nbfdfproc[i*sdparall_ray.npartsray+n]=nn;
		}
	    }
	  
	  
	  /* ecriture du nbre de facettes visibles par proc, numeros de facettes en face et fdf */
	  fwrite(&itaill,sizeof(int),1,ffdf); 
	  fwrite(nbfdfproc,sizeof(int),nb*sdparall_ray.npartsray,ffdf); 
	  fwrite(itravf,sizeof(int),itaill,ffdf); 
	  fwrite(trav,sizeof(double),itaill,ffdf); 

	  free(itravf);
	  free(trav);
	  free(nbfdfproc);
	

	  /* boucler sur les autres proc pour recevoir et ecrire les donnees */
	  /* =============================================================== */
	  for (n=1;n<sdparall_ray.npartsray;n++)
	    {
	      /* nbre de facettes locales */
	      /* ------------------------ */
	      MPI_Recv(&nb,1,MPI_INT,n,0,sdparall_ray.syrthes_comm_ray,&status);
	      fwrite(&nb,sizeof(int),1,ffdf); 
 
	      /* ecriture de la diagonale des fdf */
	      /* -------------------------------- */
	      diagtrav=(double*)malloc(nb*sizeof(double));
	      MPI_Recv(diagtrav,nb,MPI_DOUBLE,n,0,sdparall_ray.syrthes_comm_ray,&status);
	      fwrite(diagtrav,sizeof(double),nb,ffdf);   
	      
	      /* ecriture des tailles des listes */
	      /* ------------------------------- */
	      /* nombre de fdf non nuls sur le proc courant */
	      MPI_Recv(&itaill,1,MPI_INT,n,0,sdparall_ray.syrthes_comm_ray,&status);
	      fwrite(&itaill,sizeof(int),1,ffdf); 


	      itravf=(int*)malloc(itaill*sizeof(int));      /* numero de la facette en face */
	      trav=(double*)malloc(itaill*sizeof(double));  /* valeur du fdf entre les 2 facettes */
	      nbfdfproc=(int*)malloc(nb*sdparall_ray.npartsray*sizeof(int));

	      MPI_Recv(nbfdfproc,nb*sdparall_ray.npartsray,MPI_INT,n,0,sdparall_ray.syrthes_comm_ray,&status);
	      fwrite(nbfdfproc,sizeof(int),nb*sdparall_ray.npartsray,ffdf); 
	      
	      MPI_Recv(itravf,itaill,MPI_INT,n,0,sdparall_ray.syrthes_comm_ray,&status);
	      fwrite(itravf,sizeof(int),itaill,ffdf); 
	      
	      MPI_Recv(trav,itaill,MPI_DOUBLE,n,0,sdparall_ray.syrthes_comm_ray,&status);
	      fwrite(trav,sizeof(double),itaill,ffdf); 
	      
	      free(itravf);
	      free(diagtrav); 
	      free(trav);
	      free(nbfdfproc);
	    }
	}
      else /* sur les proc autre que 0 */
	{
	  /* nbre de facettes locales */
	  /* ------------------------ */
	  nb=sdparall_ray.nelrayloc[sdparall_ray.rangray];
	  MPI_Send(&nb,1,MPI_INT,0,0,sdparall_ray.syrthes_comm_ray);
	  
	  /* diagonale des fdf */
	  /* ----------------- */
	  MPI_Send(diagfdf,nb,MPI_DOUBLE,0,0,sdparall_ray.syrthes_comm_ray);
	  

	  /* ecriture des tailles des listes */
	  /* ------------------------------- */
	  /* nombre de fdf non nuls sur le proc courant */
	  for (itaill=0,i=0;i<nb;i++) 
	    for (n=0;n<sdparall_ray.npartsray;n++)
	      if (listefdf[n][i])
		{
		  pff=listefdf[n][i];
		  while(pff) {itaill++; pff=pff->suivant;}
		}
	  
	  itravf=(int*)malloc(itaill*sizeof(int));      /* numero de la facette en face */
	  trav=(double*)malloc(itaill*sizeof(double));  /* valeur du fdf entre les 2 facettes */
	  
	  /* nombre de fdf nuls nuls par proc */
	  nbfdfproc=(int*)malloc(nb*sdparall_ray.npartsray*sizeof(int));
	  for (n=0;n<nb*sdparall_ray.npartsray;n++) nbfdfproc[n]=0;
	  
	  
	  for (ii=0,i=0;i<nb;i++)
	    {
	      for (n=0;n<sdparall_ray.npartsray;n++)
		{	
		  nn=0;
		  if (listefdf[n][i])
		    {
		      pff=listefdf[n][i];
		      while(pff) {itravf[ii]=pff->numenface; trav[ii]= pff->fdf; nn++; ii++; pff=pff->suivant;}
		    }
		  nbfdfproc[i*sdparall_ray.npartsray+n]=nn;
		}
	    }
	  
	  MPI_Send(&itaill,1,MPI_INT,0,0,sdparall_ray.syrthes_comm_ray);
	  MPI_Send(nbfdfproc,nb*sdparall_ray.npartsray,MPI_INT,0,0,sdparall_ray.syrthes_comm_ray);
	  MPI_Send(itravf,itaill,MPI_INT,0,0,sdparall_ray.syrthes_comm_ray);
	  MPI_Send(trav,itaill,MPI_DOUBLE,0,0,sdparall_ray.syrthes_comm_ray);
	  
	  free(itravf);
	  free(trav);
	  free(nbfdfproc);
	}
   

/* fin du else du ifdef MPI_IO */
#endif

#endif
    }
  else
    {
      /* ouverture des fichiers */
      /* ---------------------- */
      if ((ffdf=fopen(nomfdf,"wb")) == NULL)
	{
	  if (SYRTHES_LANG == FR)
	    printf("Impossible d'ouvrir le fichier des facteurs de forme :%s\n",nomfdf);
	  else if (SYRTHES_LANG == EN)
	    printf("Impossible to open the view factor file :%s\n",nomfdf);
	  syrthes_exit(1) ;
	}
      
      /* nbre de fdf */
      nb=nelray;
      fwrite(&nelray,sizeof(int),1,ffdf);   

      /* ecriture de la diagonale des fdf */
      /* -------------------------------- */
      fwrite(diagfdf,sizeof(double),nelray,ffdf);   

      /* ecriture des tailles des listes */
      /* ------------------------------- */
      /* determination de  la taille des listes de fdf pour chaque facette */
      itaill1=(int*)malloc(nb*sizeof(int));
      for (i=0;i<nb;i++) itaill1[i]=0;
      
      for (i=0;i<nb;i++)
	if (listefdf[0][i])
	  {
	    pff=listefdf[0][i];
	    while(pff) {itaill1[i]++; pff=pff->suivant;}
	  }
          
      fwrite(itaill1,sizeof(int),nelray,ffdf);

      
      /* ecriture des numeros de facettes */
      /* -------------------------------- */
      /* on concatene dans itrav la liste des facettes en face et on l'ecrit */
      for (maxtaill=0,i=0;i<nb;i++)
	if (itaill1[i]>maxtaill) maxtaill=itaill1[i];

      itrav=(int*)malloc(maxtaill*sizeof(int));
      trav=(double*)malloc(maxtaill*sizeof(double));

      for (i=0;i<nb;i++)
	{
	  if (listefdf[0][i])
	    {
	      nn=0;
	      pff=listefdf[0][i];
	      while(pff) {itrav[nn]=pff->numenface; trav[nn]= pff->fdf; pff=pff->suivant; nn++;}
	    }
	  
	  fwrite(itrav,sizeof(int),itaill1[i],ffdf);
	  fwrite(trav,sizeof(double),itaill1[i],ffdf);
	}

      free(itrav);
      free(trav);

      fclose(ffdf);
    }
  
}
 
 
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Ecriture du flux de temperature sur le fichier additionnel           |
  |======================================================================| */
void ecrire_flux(struct Maillage maillnodes,struct Variable variable,
		 struct Prophy physol)
{
  int i,j;
  double **fluxT;

  fluxT=(double**)malloc(maillnodes.ndim*sizeof(double*));
  for (i=0;i<maillnodes.ndim;i++) 
    fluxT[i]=(double*)malloc(maillnodes.nelem*sizeof(double));

  
  for (i=0;i<maillnodes.ndim;i++)
    for (j=0;j<maillnodes.nelem;j++)
      fluxT[i][j]=0.;

  postflux_isotro(maillnodes,physol.kiso,variable.var[variable.adr_t],fluxT);
  postflux_orthotro(maillnodes,physol.kortho,variable.var[variable.adr_t],fluxT);
  postflux_anisotro(maillnodes,physol.kaniso,variable.var[variable.adr_t],fluxT);

  add_var_in_file(physol.nelem,fluxT[0],"FluxT_x",2);
  add_var_in_file(physol.nelem,fluxT[1],"FluxT_y",2);
  if (maillnodes.ndim==3) add_var_in_file(physol.nelem,fluxT[2],"FluxT_z",2);

  for (i=0;i<maillnodes.ndim;i++) free(fluxT[i]);
  free(fluxT);
 }
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2012 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |   fichier additionnel : gestion de l'ecriture de l'entete            |
  |======================================================================| */
void recale_file_fadd(FILE  **fich)
{
  long on_est_la;
  char *ch;
  int i;
 
  ch=(char*)(malloc(201*sizeof(char)));

  /* si le fichier n'existe pas, il faut ecrire l'entete */
  if (*fich) 
    {
      fclose(*fich);
      *fich=fopen(nomadd,"r");
      rewind(*fich);
      i=0;
      while (!feof(*fich) && i<12) {fgets(ch,200,*fich); i++;}
      fclose(*fich);
     
      if (i>10) *fich=fopen(nomadd,"a");
      else  *fich=fopen(nomadd,"w");
    }
  free(ch);
}

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  ecriture du maillage solide couple au fluide (cfd)                  |
  |======================================================================| */
void ecrire_geom_cplcfd(char *nomfich,
			struct Maillage maillnodes,struct MaillageCL maillnodeus,
			struct Climcfd scoupf,struct Cfd cfd)
{
  FILE  *fich;
  int i,n,ne,np,j,nbp,necoupltot,bidon=0;
  int *ne0,*ne1,*ne2,*itemp;
  double **coords;


  if ((fich=fopen(nomfich,"w")) == NULL)
    {
      if (SYRTHES_LANG == FR)
	printf("Impossible d'ouvrir le fichier %s\n",nomfich);
      else if (SYRTHES_LANG == EN)
	printf("Impossible to opn the file %s\n",nomfich);
      syrthes_exit(1);
    }

  /* nombre total de faces couplees */
  for (n=0,necoupltot=0; n<cfd.n_couplings; n++) necoupltot+=scoupf.nelem[n];


  /* il faut passer les noeuds en numerotation locale */
  itemp=(int*)malloc(maillnodes.npoin*sizeof(int));
  for (i=0;i<maillnodes.npoin;i++)  itemp[i]=-1;
  
  nbp=0;
  for (n=0; n<cfd.n_couplings; n++) 
    for (i=0;i<scoupf.nelem[n];i++) 
      {
	ne=scoupf.nume[n][i];    /* numero dans nodeus */
	for (j=0;j<maillnodeus.ndmat;j++) 
	  if (itemp[maillnodeus.node[j][ne]]<0) {itemp[maillnodeus.node[j][ne]]=nbp; nbp++;}
      }
  /* et remettre les noeuds dans l'ordre ! */
  coords=(double**)malloc(maillnodes.ndim*sizeof(double*));
  for (i=0;i<maillnodes.ndim;i++) coords[i]=(double*)malloc(maillnodes.npoin*sizeof(double));
  for (i=0;i<maillnodes.npoin;i++)
    if (itemp[i]>-1) {
      np=itemp[i];
      coords[0][np]=maillnodes.coord[0][i];
      coords[1][np]=maillnodes.coord[1][i];
      coords[2][np]=maillnodes.coord[2][i];
    }

  fprintf(fich,"C*V4.0*******************************************C\n");
  fprintf(fich,"C            FICHIER GEOMETRIQUE SYRTHES         C\n");
  fprintf(fich,"C************************************************C\n");
  fprintf(fich,"C  DIMENSION = %1d\n",maillnodeus.ndim);
  fprintf(fich,"C  DIMENSION DES ELTS = %1d\n",maillnodeus.ndiele);
  fprintf(fich,"C  NOMBRE DE NOEUDS = %12d\n",nbp);
  fprintf(fich,"C  NOMBRE D'ELEMENTS =%12d\n",necoupltot);
  fprintf(fich,"C  NOMBRE D'ELEMENTS DE BORD =%12d\n",bidon);
  fprintf(fich,"C  NOMBRE DE NOEUDS PAR ELEMENT = %3d\n",scoupf.ndmat);
  fprintf(fich,"C************************************************C\n");

  fprintf(fich,"C\nC$ RUBRIQUE = NOEUDS\nC\n");
  if (maillnodes.ndim==3)
    for (i=0;i<nbp;i++) 
      fprintf(fich,"%10d%4d %14.7e %14.7e %14.7e \n",i+1,bidon,coords[0][i],coords[1][i],coords[2][i]);
  
  fprintf(fich,"C\nC$ RUBRIQUE = ELEMENTS\nC\n");
  
  if (scoupf.ndmat==3)
    {
      for (n=0; n<cfd.n_couplings; n++) 
	for (i=0;i<scoupf.nelem[n];i++) 
	  {
	    ne=scoupf.nume[n][i];    /* numero dans nodeus */
	    fprintf(fich,"%12d%4d%10d%10d%10d\n",i+1,bidon,itemp[maillnodeus.node[0][ne]]+1,
		    itemp[maillnodeus.node[1][ne]]+1,itemp[maillnodeus.node[2][ne]]+1);
	  }
    }
  
  for (i=0;i<maillnodes.ndim;i++) free(coords[i]);
  free(coords);
  free(itemp);
  
  fclose(fich);
}

