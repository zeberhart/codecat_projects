/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: 52f1a369c45639a06bcd370c785f868d36169211 $
*/

@TOP@

/* Define if <math.h> defines FP_RZ */
#undef HAVE_FP_RZ

/* Define if fpsetmask actually works. */
#undef HAVE_WORKING_FPSETMASK

@BOTTOM@
