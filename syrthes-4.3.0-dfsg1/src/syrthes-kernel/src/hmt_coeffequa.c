/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_hmt_libmat.h"
#include "syr_bd.h"
#include "syr_hmt_bd.h"
#include "syr_option.h"
#include "syr_abs.h"
#include "syr_tree.h"
#include "syr_const.h"
#include "syr_parall.h"

#include "syr_proto.h"
#include "syr_hmt_proto.h"


extern struct Performances perfo;
extern struct Affichages affich;
extern int optim,lmst;
extern FILE  *fresu;

/*|---------------------------------------------------------------------|*/
/*|    Heat and Moisture Transfer                                        */
/*|---------------------------------------------------------------------|*/


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Calcul des coefficients des equations                                |
  |======================================================================| */
void hmt_coeffequa(int numvar,int numterm,struct Maillage maillnodes,
		   struct Humid humid,double **tmps_ele,
		   struct ConstPhyhmt constphyhmt,
		   struct ConstMateriaux *constmateriaux,
		   double *coeff)		      
{
  int i;
  int nmat,num;
  double *te,*pve,*pte;
  double t,psat,alphat,tauv,eps,xlt,xhm,xkrg,xkt;
  double xkrl,xkl,xpiv,xpi,betap,dhp,dht;
  double toto1,toto2,toto3,toto4,toto5;
  FILE *ftoto=NULL;
  /* les fonctions sont definies dans libmat.h */
 
/*      double *toto,*titi,*tutu; */
/*       toto=(double*)malloc(maillnodes.nelem*sizeof(double)); */
/*       titi=(double*)malloc(maillnodes.nelem*sizeof(double)); */
/*       tutu=(double*)malloc(maillnodes.nelem*sizeof(double)); */

  te=tmps_ele[0];
  pve=tmps_ele[1];
  pte=tmps_ele[2];
  
  num = numvar*100 + numterm;

  switch (num)
    {


      /* *********************************************************************** */
      /* EQUATION DE CONSERVATION SUR T                                          */
      /* *********************************************************************** */


      /* Modele a 3 equations - T : T : COEFF*DT/Dt - MATRICE DE MASSE  */
      /* -------------------------------------------------------------- */
    case 0 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
 	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t); 
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);
	  xlt=fphyhmt_fxl(constphyhmt,t);
	  xhm=fmat_fhm[nmat](tauv);
	  betap=fmat_fbetap[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,tauv,t);
	  dhp=fmat_fdhp[nmat](betap,tauv);


	  coeff[i] = constmateriaux[nmat].rhos*constmateriaux[nmat].cs;
	              + tauv*constphyhmt.Cpl;
		      + eps*pve[i]/(constphyhmt.Rv*t)*
			(constphyhmt.Cpl+fphyhmt_fdl(constphyhmt,t)-(xlt+xhm)/t+dhp)
		      + eps*constphyhmt.Cpas*(pte[i]-pve[i])/(constphyhmt.Ras*t)
		      + betap/constphyhmt.rhol*(pte[i]-pve[i]*(xlt+xhm)/(constphyhmt.Rv*t));
	}
/*       if (!ftoto) ftoto=fopen("coeff","w"); */
/*       ecrire_var(ftoto,maillnodes.nelem,coeff,"COEFTMAS\0",2); */
      
      break;

      /* Modele a 2 equations - T : T : COEFF*DT/Dt - MATRICE DE MASSE  */
      /* -------------------------------------------------------------- */
    case 10 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
 	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t); 
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);
	  xlt=fphyhmt_fxl(constphyhmt,t);
	  xhm=fmat_fhm[nmat](tauv);
	  betap=fmat_fbetap[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,tauv,t);
	  dhp=fmat_fdhp[nmat](betap,tauv);


	  coeff[i] = constmateriaux[nmat].rhos*constmateriaux[nmat].cs;
	              + tauv*constphyhmt.Cpl;
		      + eps*pve[i]/(constphyhmt.Rv*t)*
			(constphyhmt.Cpl+fphyhmt_fdl(constphyhmt,t)-(xlt+xhm)/t+dhp)
		      + betap/constphyhmt.rhol*pve[i]*(1.-(xlt+xhm)/(constphyhmt.Rv*t));
	}
      if (!ftoto) ftoto=fopen("coeff","w");
      ecrire_var(ftoto,maillnodes.nelem,coeff,"COEFTMAS\0",2);
      
      break;



      /* Modele a 3 equations - T : T : div(COEFF*grad(T)) - MATRICE DE DIFFUSION */
      /* ------------------------------------------------------------------------ */
    case 1 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t); 
	  coeff[i]=fmat_fklambt[nmat](t,tauv);
	}
      break;
      
      /* Modele a 3 equations - T : T : COEFF*DPV/Dt */
      /* ------------------------------------------- */
    case 2 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t); 
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);
	  xlt=fphyhmt_fxl(constphyhmt,t);
	  alphat=fmat_falpha[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
	  dht= fmat_fdht[nmat](alphat,tauv);
	  xhm=fmat_fhm[nmat](tauv);
	  coeff[i] = -  alphat*pte[i]/constphyhmt.rhol
	    - (eps*pve[i]*dht+(xlt+xhm)*(pve[i]*alphat/constphyhmt.rhol-eps))
			/(constphyhmt.Rv*t);
	}
      break;
      
      
      /* Modele a 2 equations - T : T : COEFF*DPV/Dt */
      /* ------------------------------------------- */
    case 12 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t); 
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);
	  xlt=fphyhmt_fxl(constphyhmt,t);
	  alphat=fmat_falpha[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
	  dht= fmat_fdht[nmat](alphat,tauv);
	  xhm=fmat_fhm[nmat](tauv);
	  coeff[i] = -  alphat*pve[i]/constphyhmt.rhol + eps
	    - (eps*pve[i]*dht-(xlt+xhm)*(pve[i]*alphat/constphyhmt.rhol-eps))
			/(constphyhmt.Rv*t);
	}
      break;
      
      
      
      /* Modele a 3 equations - T : T : div(COEFF*grad(PV)) */
      /* -------------------------------------------------- */
    case 3 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  xlt=fphyhmt_fxl(constphyhmt,t);
	  xhm=fmat_fhm[nmat](tauv);
	  xpiv=fmat_fpiv[nmat](pve[i],psat,t);
	  coeff[i]=-(xhm+xlt) * (  xpiv/pte[i] 
                                 + constmateriaux[nmat].xknv); 
	}
      break;

      /* Modele a 2 equations - T : T : div(COEFF*grad(PV)) */
      /* -------------------------------------------------- */
    case 13 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  xlt=fphyhmt_fxl(constphyhmt,t);
	  xhm=fmat_fhm[nmat](tauv);
	  xpi=fmat_fpiv[nmat](pve[i],psat,t)/101325.;

	  coeff[i]=-(xhm+xlt) * (  xpi
                                 + constmateriaux[nmat].xknv); 
	}
      break;

      /* Modele a 3 equations - T : T : div(COEFF*grad(PT)) */
      /* -------------------------------------------------- */
    case 4 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  xlt=fphyhmt_fxl(constphyhmt,t);
	  xhm=fmat_fhm[nmat](tauv);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t); 
 	  xkrg=fmat_fkrg[nmat](constmateriaux[nmat],tauv); 
	  xkt=fphyhmt_fxkt(constphyhmt,constmateriaux[nmat],t,pve[i],
			   constphyhmt.Cpas,xkrg);
	  xpiv=fmat_fpiv[nmat](pve[i],psat,t);
	  coeff[i]= -(xhm+xlt) *
                       (constphyhmt.xmv*pve[i]*xkt/((constphyhmt.xmv-constphyhmt.xmas)
						    *pve[i]+constphyhmt.xmas*pte[i])
			-xpiv*pve[i]/(pte[i]*pte[i]));
	}
      break;


      /* Modele a 3 equations - T : T : COEFF*DPT/Dt */
      /* ------------------------------------------- */
    case 5 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t); 
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);
	  coeff[i] = eps;
	}
      break;


      /* Modele a 3 equations - T : AUTRES TERMES EXPLICITES EVENTUELS */
      /* ------------------------------------------------------------- */
    case 6 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  xlt=fphyhmt_fxl(constphyhmt,t);
	  xhm=fmat_fhm[nmat](tauv);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t); 
	  coeff[i]=0;   /* ???????? Ici on met 0. pour l'instant */
	}
      break;




      /* *********************************************************************** */
      /* EQUATION DE CONSERVATION SUR PV                                         */
      /* *********************************************************************** */

      /* Modele a 3 equations - PV : COEFF*DPV/Dt - MATRICE DE MASSE  */
      /* ------------------------------------------------------------ */
    case 100 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t); 
	  alphat=fmat_falpha[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);
	  
	  if (constmateriaux[nmat].eps0 < 1e-16)
	    /* chris : en v4 pour les metaux je veux artificiellement conserver une matrice de masse non nulle pour pv */
	    coeff[i]=1.;
	  else 
	    coeff[i]= alphat + eps/(constphyhmt.Rv*t);

	}

      break;


      /* Modele a 3 equations - PV : div(COEFF*grad(PV)) - MATRICE DE DIFFUSION  */
      /* ----------------------------------------------------------------------- */
    case 101 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
	  xkrl=fmat_fkrl[nmat](constmateriaux[nmat],tauv);
	  xkl=fphyhmt_fxkl(constphyhmt,constmateriaux[nmat],t,xkrl); 
	  xpiv=fmat_fpiv[nmat](pve[i],psat,t);
	  coeff[i]=  xkl*constphyhmt.rhol*constphyhmt.Rv*t/pve[i]
	           + xpiv/pte[i]
	           + constmateriaux[nmat].xknv;
	}
      break;

      /* Modele a 2 equations - PV : div(COEFF*grad(PV)) - MATRICE DE DIFFUSION  */
      /* ----------------------------------------------------------------------- */
    case 111 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
	  xkrl=fmat_fkrl[nmat](constmateriaux[nmat],tauv);
	  xkl=fphyhmt_fxkl(constphyhmt,constmateriaux[nmat],t,xkrl); 
	  xhm=fmat_fhm[nmat](tauv);
	  xpi=fmat_fpiv[nmat](pve[i],psat,t)/101325;

	  coeff[i]=  xkl*constphyhmt.rhol*constphyhmt.Rv*t/pve[i]
	           + xpi
	           + constmateriaux[nmat].xknv;
	}
      break;


      /* Modele a 2 ou 3 equations - PV :  COEFF*DT/Dt  */
      /* ----------------------------------------- */
    case 102 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);
	  betap=fmat_fbetap[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,tauv,t);

	  coeff[i]= - (betap - pve[i]*eps/(constphyhmt.Rv*t*t));
	}
      break;


      /* Modele a 2 ou 3 equations - PV :  div(COEFF*grad(T))  */
      /* ------------------------------------------------ */
    case 103 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  xlt=fphyhmt_fxl(constphyhmt,t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
	  xkrl=fmat_fkrl[nmat](constmateriaux[nmat],tauv);
	  xkl=fphyhmt_fxkl(constphyhmt,constmateriaux[nmat],t,xkrl); 
	  xhm=fmat_fhm[nmat](tauv);

	  coeff[i]= xkl*(xlt-constphyhmt.Rv*t*log(pve[i]/psat))*constphyhmt.rhol/t;
	}
      break;


      /* Modele a 3 equations - PV :  div(COEFF*grad(PT)) */
      /* ------------------------------------------------ */
    case 104 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
 	  xkrg=fmat_fkrg[nmat](constmateriaux[nmat],tauv); 
	  xkrl=fmat_fkrl[nmat](constmateriaux[nmat],tauv);
	  xkl=fphyhmt_fxkl(constphyhmt,constmateriaux[nmat],t,xkrl); 
	  xkt=fphyhmt_fxkt(constphyhmt,constmateriaux[nmat],t,pve[i],
			   constphyhmt.Cpas,xkrg);
	  xpiv=fmat_fpiv[nmat](pve[i],psat,t);

	  coeff[i]= -( xkt*constphyhmt.xmv*pve[i] / 
	               (constphyhmt.xmas*pte[i] + 
		        pve[i]*(constphyhmt.xmv-constphyhmt.xmas))
		       - xpiv*pve[i]/(pte[i]*pte[i]) );
	}
      break;


      /* *********************************************************************** */
      /* EQUATION DE CONSERVATION SUR PT                                         */
      /* *********************************************************************** */

      /* PT : COEFF*DPT/Dt - MATRICE DE MASSE */
      /* ------------------------------------ */
    case 200 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);

	  if (constmateriaux[nmat].eps0 < 1e-16)
	    /* chris : en v4 pour les metaux je veux artificiellement conserver une matrice de masse non nulle pour pt */
	    coeff[i]=  1. ;
	  else
	    coeff[i]=  eps / (constphyhmt.Ras*t);
	}
      break;



      /* PT : div(COEFF*grad(PT)) - MATRICE DE DIFFUSION */
      /* ----------------------------------------------- */
    case 201 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
 	  xkrg=fmat_fkrg[nmat](constmateriaux[nmat],tauv); 
	  xkt=fphyhmt_fxkt(constphyhmt,constmateriaux[nmat],t,pve[i],
			   constphyhmt.Cpas,xkrg);
	  xpiv=fmat_fpiv[nmat](pve[i],psat,t);

	  coeff[i]=  xkt*constphyhmt.xmas*(pte[i]-pve[i]) /
	             (constphyhmt.xmas*pte[i]+(constphyhmt.xmv-constphyhmt.xmas)*pve[i])
	           + xpiv*pve[i]/(pte[i]*pte[i]) * constphyhmt.xmas/constphyhmt.xmv
	           + sqrt(constphyhmt.xmas/constphyhmt.xmv) * constmateriaux[nmat].xknv;
	}
      break;



      /* PT : COEFF*DT/Dt  */
      /* ----------------- */
    case 202 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);
	  betap=fmat_fbetap[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,tauv,t);

	  coeff[i]= + (pte[i]-pve[i]) / (constphyhmt.Ras*t)
                      * (betap/constphyhmt.rhol+eps/t);
	}
      break;


      /* PT : COEFF*DPV/Dt */
      /* ----------------- */
    case 203 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
	  alphat=fmat_falpha[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t);
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);

	  coeff[i]= + (alphat*(pte[i]-pve[i])/constphyhmt.rhol+eps)/(constphyhmt.Ras*t);
	}
      break;



      /* PT : div(COEFF*grad PV) */
      /* ----------------------- */
    case 204 : 
      for (i=0;i<maillnodes.nelem;i++)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  xpiv=fmat_fpiv[nmat](pve[i],psat,t);

	  coeff[i]= + xpiv/pte[i] * (constphyhmt.xmas/constphyhmt.xmv)
	            + sqrt(constphyhmt.xmas/constphyhmt.xmv)*constmateriaux[nmat].xknv;
	}
      break;





    } /* fin du switch */
}
