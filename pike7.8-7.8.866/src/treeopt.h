/* Tree transformation code.
 *
 * This file was generated from "/Users/hww3/pikebuild/src/treeopt.in" by
 * $Id: 3629ac0d11586132d7517d3ff7d03e6c2f63da3f $
 *
 * Do NOT edit!
 */

case '?':
  if (!CAR(n)) {
    if (!CDR(n)) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""'?'{710}(-{711}, -{712})""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""-{713}""\n");
        }
#endif /* PIKE_DEBUG */
      goto zap_node;
    } else {
      if (CDR(n)->token == ':') {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""'?'{714}(-{715}, ':'{716}(*, 0 = *{718}))""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""0 = *{719}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CDDR(n),
            tmp1 = CDDR(n);
          );
          goto use_tmp1;
        }
      }
    }
  } else if (!CDR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""'?'{720}(0 = *{721}, -{722})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""0 = *{723}""\n");
      }
#endif /* PIKE_DEBUG */
    goto use_car;
  } else {
    if (CAR(n)->token == F_APPLY) {
      if (!CAAR(n)) {
      } else {
        if (CAAR(n)->token == F_CONSTANT) {
          if ((CAAR(n)->u.sval.type == T_FUNCTION) &&
              (CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
              (CAAR(n)->u.sval.u.efun->function == f_not)) {
            if (!CDR(n)) {
            } else {
              if (CDR(n)->token == ':') {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""'?'{730}(F_APPLY{731}(F_CONSTANT{732}[CAAR(n)->u.sval.type == T_FUNCTION][CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAR(n)->u.sval.u.efun->function == f_not], 0 = *{733}), ':'{734}(1 = *{735}, 2 = *{736}))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""'?'{737}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CDAR(n),
                  ADD_NODE_REF2(CDDR(n),
                  ADD_NODE_REF2(CADR(n),
                    tmp1 = mknode('?', CDAR(n), mknode(':', CDDR(n), CADR(n)));
                  )));
                  goto use_tmp1;
                }
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_LAND) {
      if (!CAAR(n)) {
      } else {
        if (!CDAR(n)) {
        } else {
          if ((node_is_false(CDAR(n)))) {
            if (!CDR(n)) {
            } else {
              if (CDR(n)->token == ':') {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""'?'{754}(F_LAND{755}(0 = +{756}, +{757}[node_is_false(CDAR(n))]), ':'{758}(*, 1 = *{760}))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_COMMA_EXPR{761}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CDDR(n),
                    tmp1 = mknode(F_COMMA_EXPR, CAAR(n), CDDR(n));
                  ));
                  goto use_tmp1;
                }
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_LOR) {
      if (!CAAR(n)) {
      } else {
        if (!CDAR(n)) {
        } else {
          if ((node_is_true(CDAR(n)))) {
            if (!CDR(n)) {
            } else {
              if (CDR(n)->token == ':') {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""'?'{764}(F_LOR{765}(0 = +{766}, +{767}[node_is_true(CDAR(n))]), ':'{768}(1 = *{769}, *))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_COMMA_EXPR{771}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CADR(n),
                    tmp1 = mknode(F_COMMA_EXPR, CAAR(n), CADR(n));
                  ));
                  goto use_tmp1;
                }
              }
            }
          }
        }
      }
    }
    if (CDR(n)->token == ':') {
      if (!CADR(n)) {
        if (!CDDR(n)) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""'?'{724}(0 = *{725}, ':'{726}(-{727}, -{728}))""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{729}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      } else if (!CDDR(n)) {
      } else {
        if ((CDDR(n) == CADR(n))
#ifdef SHARED_NODES_MK2
          || (CDDR(n) && CADR(n) &&
              ((CDDR(n)->master?CDDR(n)->master:CDDR(n))==
               (CADR(n)->master?CADR(n)->master:CADR(n))))
#endif /* SHARED_NODES_MK2 */
          ) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""'?'{774}(0 = *{775}, ':'{776}(1 = *{777}, $CADR(n)$))""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""F_COMMA_EXPR{778}""\n");
            }
#endif /* PIKE_DEBUG */
          {
            ADD_NODE_REF2(CAR(n),
            ADD_NODE_REF2(CADR(n),
              tmp1 = mknode(F_COMMA_EXPR, CAR(n), CADR(n));
            ));
            goto use_tmp1;
          }
        }
        {
          if (CADR(n)->token == F_COMMA_EXPR) {
            if (!CDDR(n)) {
            } else {
              if ((CDDR(n) == CDADR(n))
#ifdef SHARED_NODES_MK2
                || (CDDR(n) && CDADR(n) &&
                    ((CDDR(n)->master?CDDR(n)->master:CDDR(n))==
                     (CDADR(n)->master?CDADR(n)->master:CDADR(n))))
#endif /* SHARED_NODES_MK2 */
                ) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""'?'{781}(0 = *{782}, ':'{783}(F_COMMA_EXPR{784}(1 = *{785}, 2 = *{786}), $CDADR(n)$))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_COMMA_EXPR{787}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAR(n),
                  ADD_NODE_REF2(CAADR(n),
                  ADD_NODE_REF2(CDADR(n),
                    tmp1 = mknode(F_COMMA_EXPR, mknode('?', CAR(n), mknode(':', CAADR(n), 0)), CDADR(n));
                  )));
                  goto use_tmp1;
                }
              }
              {
                if (CDDR(n)->token == F_COMMA_EXPR) {
                  {
                    if ((CDDDR(n) == CDADR(n))
#ifdef SHARED_NODES_MK2
                      || (CDDDR(n) && CDADR(n) &&
                          ((CDDDR(n)->master?CDDDR(n)->master:CDDDR(n))==
                           (CDADR(n)->master?CDADR(n)->master:CDADR(n))))
#endif /* SHARED_NODES_MK2 */
                      ) {
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "Match: ""'?'{807}(0 = *{808}, ':'{809}(F_COMMA_EXPR{810}(1 = *{811}, 2 = *{812}), F_COMMA_EXPR{813}(3 = *{814}, $CDADR(n)$)))""\n");
                        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "=> ""F_COMMA_EXPR{815}""\n");
                        }
#endif /* PIKE_DEBUG */
                      {
                        ADD_NODE_REF2(CAR(n),
                        ADD_NODE_REF2(CAADR(n),
                        ADD_NODE_REF2(CADDR(n),
                        ADD_NODE_REF2(CDADR(n),
                          tmp1 = mknode(F_COMMA_EXPR, mknode('?', CAR(n), mknode(':', CAADR(n), CADDR(n))), CDADR(n));
                        ))));
                        goto use_tmp1;
                      }
                    }
                  }
                }
              }
            }
          }
          if (CDDR(n)->token == F_COMMA_EXPR) {
            {
              if ((CDDDR(n) == CADR(n))
#ifdef SHARED_NODES_MK2
                || (CDDDR(n) && CADR(n) &&
                    ((CDDDR(n)->master?CDDDR(n)->master:CDDDR(n))==
                     (CADR(n)->master?CADR(n)->master:CADR(n))))
#endif /* SHARED_NODES_MK2 */
                ) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""'?'{794}(0 = *{795}, ':'{796}(1 = *{797}, F_COMMA_EXPR{798}(2 = *{799}, $CADR(n)$)))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_COMMA_EXPR{800}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAR(n),
                  ADD_NODE_REF2(CADDR(n),
                  ADD_NODE_REF2(CADR(n),
                    tmp1 = mknode(F_COMMA_EXPR, mknode('?', CAR(n), mknode(':', 0, CADDR(n))), CADR(n));
                  )));
                  goto use_tmp1;
                }
              }
            }
          }
          if (!CDDR(n)) {
          } else {
            if (((pike_types_le(CADR(n)->type, void_type_string) !=
		   pike_types_le(CDDR(n)->type, void_type_string)))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""'?'{822}(*, ':'{824}(0 = +{825}, +{826}[(pike_types_le(CADR(n)->type, void_type_string) !=\n"
              "\t\t   pike_types_le(CDDR(n)->type, void_type_string))]))""\n");
                }
#endif /* PIKE_DEBUG */
              {
                yyerror("The arguments to ?: may not be void.");
              }
            }
          }
        }
      }
    }
    if (( node_is_tossable(CAR(n)) )) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == ':') {
          if (!CADR(n)) {
          } else {
            if (CADR(n)->token == F_RETURN) {
              {
                if ((CAADR(n) == CAR(n))
#ifdef SHARED_NODES_MK2
                  || (CAADR(n) && CAR(n) &&
                      ((CAADR(n)->master?CAADR(n)->master:CAADR(n))==
                       (CAR(n)->master?CAR(n)->master:CAR(n))))
#endif /* SHARED_NODES_MK2 */
                  ) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""'?'{1352}(0 = +{1353}[ node_is_tossable(CAR(n)) ], ':'{1354}(F_RETURN{1355}($CAR(n)$, *), 1 = *{1357}))""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                    struct pike_type *type = CAR(n)->type;
                    struct pike_string *tmpname;
                    int tmpvar;

                    MAKE_CONST_STRING(tmpname, " ");
                    tmpvar = islocal(tmpname);
                    if(tmpvar == -1)
                    {
                      add_ref(mixed_type_string);
                      tmpvar = add_local_name(tmpname, mixed_type_string, 0);
                    }
                    if (tmpvar >= 0) {
                      
                    ADD_NODE_REF2(CDDR(n),
                    ADD_NODE_REF2(CAR(n),
                      tmp1 = mknode('?', mknode(F_ASSIGN, CAR(n), mklocalnode(tmpvar,0)),
                  		mknode(':',mknode(F_RETURN,
                  				  mksoftcastnode(type, mklocalnode(tmpvar,0)),
                  				  0),
                  		       CDDR(n)));
                    ));
                    goto use_tmp1;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    if ((node_is_false(CAR(n)))) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == ':') {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""'?'{748}(+{749}[node_is_false(CAR(n))], ':'{750}(*, 0 = *{752}))""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{753}""\n");
            }
#endif /* PIKE_DEBUG */
          {
            ADD_NODE_REF2(CDDR(n),
              tmp1 = CDDR(n);
            );
            goto use_tmp1;
          }
        }
      }
    }
    if ((node_is_true(CAR(n)))) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == ':') {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""'?'{742}(+{743}[node_is_true(CAR(n))], ':'{744}(0 = *{745}, *))""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{747}""\n");
            }
#endif /* PIKE_DEBUG */
          {
            ADD_NODE_REF2(CADR(n),
              tmp1 = CADR(n);
            );
            goto use_tmp1;
          }
        }
      }
    }
  }
  break;

case F_ADD_EQ:
  if (!CAR(n)) {
  } else if (!CDR(n)) {
  } else {
    if (CDR(n)->token == F_APPLY) {
      if (!CADR(n)) {
      } else {
        if (CADR(n)->token == F_CONSTANT) {
          if ((CADR(n)->u.sval.type == T_FUNCTION) &&
              (CADR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
              (CADR(n)->u.sval.u.efun->function == debug_f_aggregate)) {
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "Match: ""F_ADD_EQ{911}(0 = *{912}, F_APPLY{913}(F_CONSTANT{914}[CADR(n)->u.sval.type == T_FUNCTION][CADR(n)->u.sval.subtype == FUNCTION_BUILTIN][CADR(n)->u.sval.u.efun->function == debug_f_aggregate], 1 = *{915}))""\n");
              }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "=> ""F_APPEND_ARRAY{916}""\n");
              }
#endif /* PIKE_DEBUG */
            {
              ADD_NODE_REF2(CAR(n),
              ADD_NODE_REF2(CDDR(n),
                tmp1 = mknode(F_APPEND_ARRAY, CAR(n), CDDR(n));
              ));
              goto use_tmp1;
            }
          }
        }
      }
    } else if (CDR(n)->token == F_CONSTANT) {
      if ((CDR(n)->u.sval.type == T_INT) &&
          ((CDR(n)->u.sval.u.integer) == -1)) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_ADD_EQ{889}(0 = *{890}, 1 = F_CONSTANT{891}[CDR(n)->u.sval.type == T_INT][(CDR(n)->u.sval.u.integer) == -1])""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""F_DEC{892}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CAR(n),
            tmp1 = mknode(F_DEC, CAR(n), 0);
          );
          goto use_tmp1;
        }
      }
      if ((CDR(n)->u.sval.type == T_INT) &&
          ((CDR(n)->u.sval.u.integer) == 1)) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_ADD_EQ{883}(0 = *{884}, 1 = F_CONSTANT{885}[CDR(n)->u.sval.type == T_INT][(CDR(n)->u.sval.u.integer) == 1])""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""F_INC{886}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CAR(n),
            tmp1 = mknode(F_INC, CAR(n), 0);
          );
          goto use_tmp1;
        }
      }
    }
    if (( CAR(n)->token != F_AUTO_MAP_MARKER )) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == F_CONSTANT) {
          if ((CDR(n)->u.sval.type == T_INT) &&
              (!(CDR(n)->u.sval.u.integer))) {
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "Match: ""F_ADD_EQ{879}(0 = +{880}[ CAR(n)->token != F_AUTO_MAP_MARKER ], 1 = F_CONSTANT{881}[CDR(n)->u.sval.type == T_INT][!(CDR(n)->u.sval.u.integer)])""\n");
              }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "=> ""0 = *{882}""\n");
              }
#endif /* PIKE_DEBUG */
            goto use_car;
          }
        }
      }
    }
  }
  break;

case F_APPLY:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_APPLY{6}(-{7}, *)""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""F_COMMA_EXPR{9}""\n");
      }
#endif /* PIKE_DEBUG */
    {
      ADD_NODE_REF2(CDR(n),
        tmp1 = mknode(F_COMMA_EXPR, mknode(F_POP_VALUE, CDR(n), 0), mkintnode(0));
      );
      goto use_tmp1;
    }
  } else {
    if (CAR(n)->token == F_CONSTANT) {
      if ((CAR(n)->u.sval.type == T_FUNCTION) &&
          (CAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
          (CAR(n)->u.sval.u.efun->function == f_add)) {
        if (!CDR(n)) {
        } else {
          if (CDR(n)->token == F_ARG_LIST) {
            if (!CADR(n)) {
            } else {
              if (CADR(n)->token == F_APPLY) {
                if (!CAADR(n)) {
                } else {
                  if (CAADR(n)->token == F_CONSTANT) {
                    if ((CAADR(n)->u.sval.type == T_FUNCTION) &&
                        (CAADR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                        (CAADR(n)->u.sval.u.efun->function == f_add)) {
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "Match: ""F_APPLY{112}(0 = F_CONSTANT{113}[CAR(n)->u.sval.type == T_FUNCTION][CAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAR(n)->u.sval.u.efun->function == f_add], 2 = F_ARG_LIST{114}(F_APPLY{115}(F_CONSTANT{116}[CAADR(n)->u.sval.type == T_FUNCTION][CAADR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAADR(n)->u.sval.u.efun->function == f_add], 1 = *{117}), 3 = *{118}))""\n");
                        }
#endif /* PIKE_DEBUG */
                      {
                          node *arglist = CDR(n);
                          ADD_NODE_REF2(CDADR(n),
                          ADD_NODE_REF2(CDDR(n),
                      		  _CDR(n) = mknode(F_ARG_LIST, CDADR(n), CDDR(n))));
                          _CDR(n)->parent = NULL;
                          fix_type_field(_CDR(n));
                          free_node(arglist);
#ifdef PIKE_DEBUG
                          if (l_flag > 4) {
                            fprintf(stderr, "Result:    ");
                            print_tree(n);
                          }
#endif /* PIKE_DEBUG */
                          continue;
                        }
                    }
                  }
                }
              }
            }
          }
        }
      }
      if ((CAR(n)->u.sval.type == T_FUNCTION) &&
          (CAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
          (CAR(n)->u.sval.u.efun->function == f_minus)) {
        if (!CDR(n)) {
        } else {
          if (CDR(n)->token == F_ARG_LIST) {
            if (!CADR(n)) {
            } else {
              if (CADR(n)->token == F_APPLY) {
                if (!CAADR(n)) {
                } else {
                  if (CAADR(n)->token == F_CONSTANT) {
                    if ((CAADR(n)->u.sval.type == T_FUNCTION) &&
                        (CAADR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                        (CAADR(n)->u.sval.u.efun->function == f_minus)) {
                      if (!CDADR(n)) {
                      } else {
                        if (CDADR(n)->token == F_ARG_LIST) {
                          if (!CADADR(n)) {
                          } else {
                            if (!CDDADR(n)) {
                            } else {
#ifdef PIKE_DEBUG
                                if (l_flag > 4) {
                                  fprintf(stderr, "Match: ""F_APPLY{119}(0 = F_CONSTANT{120}[CAR(n)->u.sval.type == T_FUNCTION][CAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAR(n)->u.sval.u.efun->function == f_minus], 2 = F_ARG_LIST{121}(F_APPLY{122}(F_CONSTANT{123}[CAADR(n)->u.sval.type == T_FUNCTION][CAADR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAADR(n)->u.sval.u.efun->function == f_minus], 1 = F_ARG_LIST{124}(+{125}, +{126})), 3 = *{127}))""\n");
                                }
#endif /* PIKE_DEBUG */
                              {
                                  node *arglist = CDR(n);
                                  ADD_NODE_REF2(CDADR(n),
                                  ADD_NODE_REF2(CDDR(n),
                              		  _CDR(n) = mknode(F_ARG_LIST, CDADR(n), CDDR(n))));
                                  _CDR(n)->parent = NULL;
                                  fix_type_field(_CDR(n));
                                  free_node(arglist);
#ifdef PIKE_DEBUG
                                  if (l_flag > 4) {
                                    fprintf(stderr, "Result:    ");
                                    print_tree(n);
                                  }
#endif /* PIKE_DEBUG */
                                  continue;
                                }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      if ((CAR(n)->u.sval.type == T_FUNCTION) &&
          (CAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
          (CAR(n)->u.sval.u.efun->function == f_multiply)) {
        if (!CDR(n)) {
        } else {
          if (CDR(n)->token == F_ARG_LIST) {
            if (!CADR(n)) {
            } else {
              if (CADR(n)->token == F_APPLY) {
                if (!CAADR(n)) {
                } else {
                  if (CAADR(n)->token == F_CONSTANT) {
                    if ((CAADR(n)->u.sval.type == T_FUNCTION) &&
                        (CAADR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                        (CAADR(n)->u.sval.u.efun->function == f_multiply)) {
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "Match: ""F_APPLY{128}(0 = F_CONSTANT{129}[CAR(n)->u.sval.type == T_FUNCTION][CAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAR(n)->u.sval.u.efun->function == f_multiply], 2 = F_ARG_LIST{130}(F_APPLY{131}(F_CONSTANT{132}[CAADR(n)->u.sval.type == T_FUNCTION][CAADR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAADR(n)->u.sval.u.efun->function == f_multiply], 1 = *{133}), 3 = *{134}))""\n");
                        }
#endif /* PIKE_DEBUG */
                      {
                          node *arglist = CDR(n);
                          ADD_NODE_REF2(CDADR(n),
                          ADD_NODE_REF2(CDDR(n),
                      		  _CDR(n) = mknode(F_ARG_LIST, CDADR(n), CDDR(n))));
                          _CDR(n)->parent = NULL;
                          fix_type_field(_CDR(n));
                          free_node(arglist);
#ifdef PIKE_DEBUG
                          if (l_flag > 4) {
                            fprintf(stderr, "Result:    ");
                            print_tree(n);
                          }
#endif /* PIKE_DEBUG */
                          continue;
                        }
                    }
                  }
                }
              }
            }
          }
        }
      }
      if ((CAR(n)->u.sval.type == T_FUNCTION) &&
          (CAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
          (CAR(n)->u.sval.u.efun->function == f_sizeof)) {
        if (!CDR(n)) {
        } else {
          if (CDR(n)->token == F_APPLY) {
            if (!CADR(n)) {
            } else {
              if (CADR(n)->token == F_CONSTANT) {
                if ((CADR(n)->u.sval.type == T_FUNCTION) &&
                    (CADR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                    (CADR(n)->u.sval.u.efun->function == f_multiply)) {
                  if (!CDDR(n)) {
                  } else {
                    if (CDDR(n)->token == F_ARG_LIST) {
                      if (!CADDR(n)) {
                      } else {
                        if ((!match_types(object_type_string, CADDR(n)->type))) {
                          if (!CDDDR(n)) {
                          } else {
                            if ((pike_types_le(CDDDR(n)->type, int_type_string))) {
#ifdef PIKE_DEBUG
                                if (l_flag > 4) {
                                  fprintf(stderr, "Match: ""F_APPLY{43}(0 = F_CONSTANT{44}[CAR(n)->u.sval.type == T_FUNCTION][CAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAR(n)->u.sval.u.efun->function == f_sizeof], F_APPLY{45}(1 = F_CONSTANT{46}[CADR(n)->u.sval.type == T_FUNCTION][CADR(n)->u.sval.subtype == FUNCTION_BUILTIN][CADR(n)->u.sval.u.efun->function == f_multiply], F_ARG_LIST{47}(2 = +{48}[!match_types(object_type_string, CADDR(n)->type)], 3 = +{49}[pike_types_le(CDDDR(n)->type, int_type_string)])))""\n");
                                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                                if (l_flag > 4) {
                                  fprintf(stderr, "=> ""F_APPLY{50}""\n");
                                }
#endif /* PIKE_DEBUG */
                              {
                                ADD_NODE_REF2(CADR(n),
                                ADD_NODE_REF2(CAR(n),
                                ADD_NODE_REF2(CADDR(n),
                                ADD_NODE_REF2(CDDDR(n),
                                  tmp1 = mknode(F_APPLY, CADR(n), mknode(F_ARG_LIST, mknode(F_APPLY, CAR(n), CADDR(n)), CDDDR(n)));
                                ))));
                                goto use_tmp1;
                              }
                            }
                          }
                        }
                        if ((pike_types_le(CADDR(n)->type, int_type_string))) {
                          if (!CDDDR(n)) {
                          } else {
                            if ((!match_types(object_type_string, CDDDR(n)->type))) {
#ifdef PIKE_DEBUG
                                if (l_flag > 4) {
                                  fprintf(stderr, "Match: ""F_APPLY{29}(0 = F_CONSTANT{30}[CAR(n)->u.sval.type == T_FUNCTION][CAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAR(n)->u.sval.u.efun->function == f_sizeof], F_APPLY{31}(1 = F_CONSTANT{32}[CADR(n)->u.sval.type == T_FUNCTION][CADR(n)->u.sval.subtype == FUNCTION_BUILTIN][CADR(n)->u.sval.u.efun->function == f_multiply], F_ARG_LIST{33}(2 = +{34}[pike_types_le(CADDR(n)->type, int_type_string)], 3 = +{35}[!match_types(object_type_string, CDDDR(n)->type)])))""\n");
                                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                                if (l_flag > 4) {
                                  fprintf(stderr, "=> ""F_APPLY{36}""\n");
                                }
#endif /* PIKE_DEBUG */
                              {
                                ADD_NODE_REF2(CADR(n),
                                ADD_NODE_REF2(CADDR(n),
                                ADD_NODE_REF2(CAR(n),
                                ADD_NODE_REF2(CDDDR(n),
                                  tmp1 = mknode(F_APPLY, CADR(n), mknode(F_ARG_LIST, CADDR(n), mknode(F_APPLY, CAR(n), CDDDR(n))));
                                ))));
                                goto use_tmp1;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      if ((CAR(n)->u.sval.type == T_FUNCTION) &&
          (CAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
          (CAR(n)->u.sval.u.efun->optimize) &&
          ( (tmp1=CAR(n)->u.sval.u.efun->optimize(n)) )) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""0 = F_APPLY{0}(F_CONSTANT{1}[CAR(n)->u.sval.type == T_FUNCTION][CAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAR(n)->u.sval.u.efun->optimize][ (tmp1=CAR(n)->u.sval.u.efun->optimize(n)) ], *)""\n");
          }
#endif /* PIKE_DEBUG */
        {
          goto use_tmp1;
        }
      }
      if ((CAR(n)->u.sval.type == T_PROGRAM) &&
          (CAR(n)->u.sval.u.program->optimize) &&
          ( (tmp1=CAR(n)->u.sval.u.program->optimize(n)) )) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""0 = F_APPLY{3}(F_CONSTANT{4}[CAR(n)->u.sval.type == T_PROGRAM][CAR(n)->u.sval.u.program->optimize][ (tmp1=CAR(n)->u.sval.u.program->optimize(n)) ], *)""\n");
          }
#endif /* PIKE_DEBUG */
        {
          goto use_tmp1;
        }
      }
    }
  }
  break;

case F_ARG_LIST:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_ARG_LIST{486}(-{487}, 0 = *{488})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""0 = *{489}""\n");
      }
#endif /* PIKE_DEBUG */
    goto use_cdr;
  } else if (!CDR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_ARG_LIST{490}(0 = *{491}, -{492})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""0 = *{493}""\n");
      }
#endif /* PIKE_DEBUG */
    goto use_car;
  } else {
    if (CAR(n)->token == F_BREAK) {
      if (!CDR(n)) {
      } else {
        if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_ARG_LIST{512}(0 = F_BREAK{513}, +{514}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{515}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    } else if (CAR(n)->token == F_COMMA_EXPR) {
      if (!CDAR(n)) {
      } else {
        if (CDAR(n)->token == F_BREAK) {
          if (!CDR(n)) {
          } else {
            if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_ARG_LIST{528}(0 = F_COMMA_EXPR{529}(*, F_BREAK{531}), +{532}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""0 = *{533}""\n");
                }
#endif /* PIKE_DEBUG */
              goto use_car;
            }
          }
        } else if (CDAR(n)->token == F_CONTINUE) {
          if (!CDR(n)) {
          } else {
            if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_ARG_LIST{522}(0 = F_COMMA_EXPR{523}(*, F_CONTINUE{525}), +{526}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""0 = *{527}""\n");
                }
#endif /* PIKE_DEBUG */
              goto use_car;
            }
          }
        } else if (CDAR(n)->token == F_RETURN) {
          if (!CDR(n)) {
          } else {
            if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_ARG_LIST{516}(0 = F_COMMA_EXPR{517}(*, F_RETURN{519}), +{520}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""0 = *{521}""\n");
                }
#endif /* PIKE_DEBUG */
              goto use_car;
            }
          }
        }
      }
    } else if (CAR(n)->token == F_CONTINUE) {
      if (!CDR(n)) {
      } else {
        if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_ARG_LIST{508}(0 = F_CONTINUE{509}, +{510}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{511}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    } else if (CAR(n)->token == F_RETURN) {
      if (!CDR(n)) {
      } else {
        if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_ARG_LIST{504}(0 = F_RETURN{505}, +{506}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{507}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    }
    if (CDR(n)->token == F_ARG_LIST) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_ARG_LIST{494}(0 = *{495}, F_ARG_LIST{496}(1 = *{497}, 2 = *{498}))""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""F_ARG_LIST{499}""\n");
        }
#endif /* PIKE_DEBUG */
      {
        ADD_NODE_REF2(CAR(n),
        ADD_NODE_REF2(CADR(n),
        ADD_NODE_REF2(CDDR(n),
          tmp1 = mknode(F_ARG_LIST, mknode(F_ARG_LIST, CAR(n), CADR(n)), CDDR(n));
        )));
        goto use_tmp1;
      }
    }
  }
  break;

case F_ARROW:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_ARROW{933}(-{934}, *)""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""F_COMMA_EXPR{936}""\n");
      }
#endif /* PIKE_DEBUG */
    {
      ADD_NODE_REF2(CDR(n),
        tmp1 = mknode(F_COMMA_EXPR, mknode(F_POP_VALUE, CDR(n), 0), mkintnode(0));
      );
      goto use_tmp1;
    }
  } else {
    if (CAR(n)->token == F_CONSTANT) {
      if ((CAR(n)->u.sval.type == T_OBJECT) &&
          (CAR(n)->u.sval.u.object->prog)) {
        if (!CDR(n)) {
        } else {
          if (CDR(n)->token == F_CONSTANT) {
            if ((CDR(n)->u.sval.type == T_STRING)) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_ARROW{941}(0 = F_CONSTANT{942}[CAR(n)->u.sval.type == T_OBJECT][CAR(n)->u.sval.u.object->prog], 1 = F_CONSTANT{943}[CDR(n)->u.sval.type == T_STRING])""\n");
                }
#endif /* PIKE_DEBUG */
              {
                /*
                if (find_identifier("`->", CAR(n)->u.sval.u.object->prog) == -1) {
                  int i = find_shared_string_identifier(CDR(n)->u.sval.u.string,
              					  CAR(n)->u.sval.u.object->prog);
                  if (i) {
                    struct identifier *id = ID_FROM_INT(CAR(n)->u.sval.u.object->prog, i);
                    if (IDENTIFIER_IS_VARIABLE(id->identifier_flags))
              	goto next_arrow_opt;
                  }
                  ref_push_object(CAR(n)->u.sval.u.object);
                  ref_push_string(CDR(n)->u.sval.u.string);
                  f_index(2);
                  tmp1 = mksvaluenode(sp-1);
                  pop_stack();
                  goto use_tmp1;
                }
               next_arrow_opt:
                ;
                */
              }
            }
          }
        }
      }
    }
  }
  break;

case F_ASSIGN:
  if (!CAR(n)) {
  } else {
    if (CAR(n)->token == F_APPLY) {
      if (!CAAR(n)) {
      } else {
        if (CAAR(n)->token == F_CONSTANT) {
          if ((CAAR(n)->u.sval.type == T_FUNCTION) &&
              (CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
              (CAAR(n)->u.sval.u.efun->function == debug_f_aggregate)) {
            if (!CDAR(n)) {
            } else {
              if ((count_args(CDAR(n)) >= 0)) {
                if (!CDR(n)) {
                } else {
                  if (CDR(n)->token == F_ARRAY_LVALUE) {
                    if (!CDDR(n)) {
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "Match: ""F_ASSIGN{469}(F_APPLY{470}(F_CONSTANT{471}[CAAR(n)->u.sval.type == T_FUNCTION][CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAR(n)->u.sval.u.efun->function == debug_f_aggregate], 0 = +{472}[count_args(CDAR(n)) >= 0]), F_ARRAY_LVALUE{473}(1 = *{474}, -{475}))""\n");
                        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "=> ""F_MULTI_ASSIGN{476}""\n");
                        }
#endif /* PIKE_DEBUG */
                      {
                        ADD_NODE_REF2(CDAR(n),
                        ADD_NODE_REF2(CADR(n),
                          tmp1 = mknode(F_MULTI_ASSIGN, CDAR(n), CADR(n));
                        ));
                        goto use_tmp1;
                      }
                    }
                  }
                }
              }
            }
          }
          if ((CAAR(n)->u.sval.type == T_FUNCTION) &&
              (CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
              (CAAR(n)->u.sval.u.efun->function == f_allocate)) {
            if (!CDR(n)) {
            } else {
              if (CDR(n)->token == F_ARRAY_LVALUE) {
                if (!CDDR(n)) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_ASSIGN{479}(0 = F_APPLY{480}(F_CONSTANT{481}[CAAR(n)->u.sval.type == T_FUNCTION][CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAR(n)->u.sval.u.efun->function == f_allocate], *), F_ARRAY_LVALUE{483}(1 = *{484}, -{485}))""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                      int cnt = 0;
                      node **arg1 = my_get_arg(&_CDR(CAR(n)), 0);
                      node **arg2 = my_get_arg(&_CDR(CAR(n)), 1);
                      if (arg1) {
                        node *res;
                        node *lvalues = CADR(n);

                        if (arg2 && *arg2) {
                  	ADD_NODE_REF2(*arg2, res = *arg2;);
                        } else {
                  	res = mkintnode(0);
                        }
                        while (lvalues && (lvalues->token == F_LVALUE_LIST)) {
                  	ADD_NODE_REF2(CAR(lvalues),
                  		      res = mknode(F_ASSIGN, res, CAR(lvalues)););
                  	lvalues = CDR(lvalues);
                  	cnt++;
                        }
                        if (lvalues) {
                  	ADD_NODE_REF2(lvalues, res = mknode(F_ASSIGN, res, lvalues););
                  	cnt++;
                        }
                        /* FIXME: Check that the number of arguments actually matches arg1. */
                        
                    tmp1 = res;
                    goto use_tmp1;
                      }
                    }
                }
              }
            }
          }
        }
      }
    }
  }
  break;

case F_CAST:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_CAST{239}(-{240}, *)""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""-{242}""\n");
      }
#endif /* PIKE_DEBUG */
    goto zap_node;
  } else {
    if (CAR(n)->token == F_CAST) {
      if ((CAR(n)->type == n->type)) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""0 = F_CAST{246}(1 = F_CAST{247}[CAR(n)->type == n->type], *)""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""1 = *{251}""\n");
          }
#endif /* PIKE_DEBUG */
        goto use_car;
      }
    } else if (CAR(n)->token == F_CONSTANT) {
      if ((CAR(n)->type == n->type)) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""0 = F_CAST{252}(1 = F_CONSTANT{253}[CAR(n)->type == n->type], *)""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""1 = *{257}""\n");
          }
#endif /* PIKE_DEBUG */
        goto use_car;
      }
    }
    if ((CAR(n)->type == void_type_string)) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_CAST{243}(0 = +{244}[CAR(n)->type == void_type_string], *)""\n");
        }
#endif /* PIKE_DEBUG */
      {
        yywarning("Casting a void expression\n");
        
        ADD_NODE_REF2(CAR(n),
          tmp1 = CAR(n);
        );
        goto use_tmp1;
      }
    }
  }
  break;

case F_COMMA_EXPR:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_COMMA_EXPR{286}(-{287}, 0 = *{288})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""0 = *{289}""\n");
      }
#endif /* PIKE_DEBUG */
    goto use_cdr;
  } else if (!CDR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_COMMA_EXPR{290}(0 = *{291}, -{292})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""0 = *{293}""\n");
      }
#endif /* PIKE_DEBUG */
    goto use_car;
  } else {
    if (CAR(n)->token == F_ARG_LIST) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_COMMA_EXPR{359}(F_ARG_LIST{360}(0 = *{361}, 1 = *{362}), 2 = *{363})""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""F_COMMA_EXPR{364}""\n");
        }
#endif /* PIKE_DEBUG */
      {
        ADD_NODE_REF2(CAAR(n),
        ADD_NODE_REF2(CDAR(n),
        ADD_NODE_REF2(CDR(n),
          tmp1 = mknode(F_COMMA_EXPR, mknode(F_COMMA_EXPR, CAAR(n), CDAR(n)), CDR(n));
        )));
        goto use_tmp1;
      }
    } else if (CAR(n)->token == F_ASSIGN) {
      if (!CDAR(n)) {
      } else {
        if (CDAR(n)->token == F_LOCAL) {
          if (!CDR(n)) {
          } else {
            if (CDR(n)->token == F_COMMA_EXPR) {
              if (!CADR(n)) {
              } else {
                if (CADR(n)->token == F_DEC) {
                  if (!CDADR(n)) {
                    if (!CAADR(n)) {
                    } else {
                      if (CAADR(n)->token == F_LOCAL) {
                        if ((CDAR(n)->u.integer.a == CAADR(n)->u.integer.a) &&
                            (CDAR(n)->u.integer.b == CAADR(n)->u.integer.b)) {
#ifdef PIKE_DEBUG
                            if (l_flag > 4) {
                              fprintf(stderr, "Match: ""F_COMMA_EXPR{451}(F_ASSIGN{452}(0 = *{453}, 1 = F_LOCAL{454}), F_COMMA_EXPR{455}(F_DEC{456}(F_LOCAL{457}[CDAR(n)->u.integer.a == CAADR(n)->u.integer.a][CDAR(n)->u.integer.b == CAADR(n)->u.integer.b], -{458}), 2 = *{459}))""\n");
                            }
#endif /* PIKE_DEBUG */
                          {
                              
                            ADD_NODE_REF2(CDDR(n),
                            ADD_NODE_REF2(CDAR(n),
                            ADD_NODE_REF2(CAAR(n),
                              tmp1 = mknode(F_COMMA_EXPR,
                          		mknode(F_ASSIGN,
                          		       mkefuncallnode("`-",
                          				      mknode(F_ARG_LIST, CAAR(n), mkintnode(1))),
                          		       CDAR(n)),
                          		CDDR(n));
                            )));
                            goto use_tmp1;
                            }
                        }
                      }
                    }
                  }
                } else if (CADR(n)->token == F_INC) {
                  if (!CDADR(n)) {
                    if (!CAADR(n)) {
                    } else {
                      if (CAADR(n)->token == F_LOCAL) {
                        if ((CDAR(n)->u.integer.a == CAADR(n)->u.integer.a) &&
                            (CDAR(n)->u.integer.b == CAADR(n)->u.integer.b)) {
#ifdef PIKE_DEBUG
                            if (l_flag > 4) {
                              fprintf(stderr, "Match: ""F_COMMA_EXPR{460}(F_ASSIGN{461}(0 = *{462}, 1 = F_LOCAL{463}), F_COMMA_EXPR{464}(F_INC{465}(F_LOCAL{466}[CDAR(n)->u.integer.a == CAADR(n)->u.integer.a][CDAR(n)->u.integer.b == CAADR(n)->u.integer.b], -{467}), 2 = *{468}))""\n");
                            }
#endif /* PIKE_DEBUG */
                          {
                              
                            ADD_NODE_REF2(CDDR(n),
                            ADD_NODE_REF2(CDAR(n),
                            ADD_NODE_REF2(CAAR(n),
                              tmp1 = mknode(F_COMMA_EXPR,
                          		mknode(F_ASSIGN,
                          		       mkefuncallnode("`+",
                          				      mknode(F_ARG_LIST, CAAR(n), mkintnode(1))),
                          		       CDAR(n)),
                          		CDDR(n));
                            )));
                            goto use_tmp1;
                            }
                        }
                      }
                    }
                  }
                }
              }
            } else if (CDR(n)->token == F_DEC) {
              if (!CDDR(n)) {
                if (!CADR(n)) {
                } else {
                  if (CADR(n)->token == F_LOCAL) {
                    if (( CDAR(n)->u.integer.a == CADR(n)->u.integer.a ) &&
                        ( CDAR(n)->u.integer.b == CADR(n)->u.integer.b )) {
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "Match: ""F_COMMA_EXPR{437}(F_ASSIGN{438}(0 = *{439}, 1 = F_LOCAL{440}), F_DEC{441}(F_LOCAL{442}[ CDAR(n)->u.integer.a == CADR(n)->u.integer.a ][ CDAR(n)->u.integer.b == CADR(n)->u.integer.b ], -{443}))""\n");
                        }
#endif /* PIKE_DEBUG */
                      {
                          
                        ADD_NODE_REF2(CDAR(n),
                        ADD_NODE_REF2(CAAR(n),
                          tmp1 = mknode(F_ASSIGN,
                      		mkefuncallnode("`-",
                      			       mknode(F_ARG_LIST, CAAR(n), mkintnode(1))), CDAR(n));
                        ));
                        goto use_tmp1;
                        }
                    }
                  }
                }
              }
            } else if (CDR(n)->token == F_INC) {
              if (!CDDR(n)) {
                if (!CADR(n)) {
                } else {
                  if (CADR(n)->token == F_LOCAL) {
                    if (( CDAR(n)->u.integer.a == CADR(n)->u.integer.a ) &&
                        ( CDAR(n)->u.integer.b == CADR(n)->u.integer.b )) {
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "Match: ""F_COMMA_EXPR{444}(F_ASSIGN{445}(0 = *{446}, 1 = F_LOCAL{447}), F_INC{448}(F_LOCAL{449}[ CDAR(n)->u.integer.a == CADR(n)->u.integer.a ][ CDAR(n)->u.integer.b == CADR(n)->u.integer.b ], -{450}))""\n");
                        }
#endif /* PIKE_DEBUG */
                      {
                          
                        ADD_NODE_REF2(CDAR(n),
                        ADD_NODE_REF2(CAAR(n),
                          tmp1 = mknode(F_ASSIGN,
                      		mkefuncallnode("`+",
                      			       mknode(F_ARG_LIST, CAAR(n), mkintnode(1))), CDAR(n));
                        ));
                        goto use_tmp1;
                        }
                    }
                  }
                }
              }
            }
          }
        }
        if ((!depend_p(CDAR(n), CDAR(n)))) {
          if (!CDR(n)) {
          } else {
            if (CDR(n)->token == F_COMMA_EXPR) {
              if (!CADR(n)) {
              } else {
                if (CADR(n)->token == F_DEC) {
                  {
                    if ((CAADR(n) == CDAR(n))
#ifdef SHARED_NODES_MK2
                      || (CAADR(n) && CDAR(n) &&
                          ((CAADR(n)->master?CAADR(n)->master:CAADR(n))==
                           (CDAR(n)->master?CDAR(n)->master:CDAR(n))))
#endif /* SHARED_NODES_MK2 */
                      ) {
                      if (!CDADR(n)) {
#ifdef PIKE_DEBUG
                          if (l_flag > 4) {
                            fprintf(stderr, "Match: ""F_COMMA_EXPR{421}(F_ASSIGN{422}(0 = *{423}, 1 = +{424}[!depend_p(CDAR(n), CDAR(n))]), F_COMMA_EXPR{425}(F_DEC{426}($CDAR(n)$, -{427}), 2 = *{428}))""\n");
                          }
#endif /* PIKE_DEBUG */
                        {
                            
                          ADD_NODE_REF2(CDAR(n),
                          ADD_NODE_REF2(CAAR(n),
                          ADD_NODE_REF2(CDDR(n),
                            tmp1 = mknode(F_COMMA_EXPR,
                        		mknode(F_ASSIGN,
                        		       mkefuncallnode("`-",
                        				      mknode(F_ARG_LIST, CAAR(n), mkintnode(1))),
                        		       CDAR(n)),
                        		CDDR(n));
                          )));
                          goto use_tmp1;
                          }
                      }
                    }
                  }
                } else if (CADR(n)->token == F_INC) {
                  {
                    if ((CAADR(n) == CDAR(n))
#ifdef SHARED_NODES_MK2
                      || (CAADR(n) && CDAR(n) &&
                          ((CAADR(n)->master?CAADR(n)->master:CAADR(n))==
                           (CDAR(n)->master?CDAR(n)->master:CDAR(n))))
#endif /* SHARED_NODES_MK2 */
                      ) {
                      if (!CDADR(n)) {
#ifdef PIKE_DEBUG
                          if (l_flag > 4) {
                            fprintf(stderr, "Match: ""F_COMMA_EXPR{429}(F_ASSIGN{430}(0 = *{431}, 1 = +{432}[!depend_p(CDAR(n), CDAR(n))]), F_COMMA_EXPR{433}(F_INC{434}($CDAR(n)$, -{435}), 2 = *{436}))""\n");
                          }
#endif /* PIKE_DEBUG */
                        {
                            
                          ADD_NODE_REF2(CDDR(n),
                          ADD_NODE_REF2(CDAR(n),
                          ADD_NODE_REF2(CAAR(n),
                            tmp1 = mknode(F_COMMA_EXPR,
                        		mknode(F_ASSIGN,
                        		       mkefuncallnode("`+",
                        				      mknode(F_ARG_LIST, CAAR(n), mkintnode(1))),
                        		       CDAR(n)),
                        		CDDR(n));
                          )));
                          goto use_tmp1;
                          }
                      }
                    }
                  }
                }
              }
            } else if (CDR(n)->token == F_DEC) {
              {
                if ((CADR(n) == CDAR(n))
#ifdef SHARED_NODES_MK2
                  || (CADR(n) && CDAR(n) &&
                      ((CADR(n)->master?CADR(n)->master:CADR(n))==
                       (CDAR(n)->master?CDAR(n)->master:CDAR(n))))
#endif /* SHARED_NODES_MK2 */
                  ) {
                  if (!CDDR(n)) {
#ifdef PIKE_DEBUG
                      if (l_flag > 4) {
                        fprintf(stderr, "Match: ""F_COMMA_EXPR{409}(F_ASSIGN{410}(0 = *{411}, 1 = +{412}[!depend_p(CDAR(n), CDAR(n))]), F_DEC{413}($CDAR(n)$, -{414}))""\n");
                      }
#endif /* PIKE_DEBUG */
                    {
                        
                      ADD_NODE_REF2(CDAR(n),
                      ADD_NODE_REF2(CAAR(n),
                        tmp1 = mknode(F_ASSIGN,
                    		mkefuncallnode("`-",
                    			       mknode(F_ARG_LIST, CAAR(n), mkintnode(1))), CDAR(n));
                      ));
                      goto use_tmp1;
                      }
                  }
                }
              }
            } else if (CDR(n)->token == F_INC) {
              {
                if ((CADR(n) == CDAR(n))
#ifdef SHARED_NODES_MK2
                  || (CADR(n) && CDAR(n) &&
                      ((CADR(n)->master?CADR(n)->master:CADR(n))==
                       (CDAR(n)->master?CDAR(n)->master:CDAR(n))))
#endif /* SHARED_NODES_MK2 */
                  ) {
                  if (!CDDR(n)) {
#ifdef PIKE_DEBUG
                      if (l_flag > 4) {
                        fprintf(stderr, "Match: ""F_COMMA_EXPR{415}(F_ASSIGN{416}(0 = *{417}, 1 = +{418}[!depend_p(CDAR(n), CDAR(n))]), F_INC{419}($CDAR(n)$, -{420}))""\n");
                      }
#endif /* PIKE_DEBUG */
                    {
                        
                      ADD_NODE_REF2(CDAR(n),
                      ADD_NODE_REF2(CAAR(n),
                        tmp1 = mknode(F_ASSIGN,
                    		mkefuncallnode("`+",
                    			       mknode(F_ARG_LIST, CAAR(n), mkintnode(1))), CDAR(n));
                      ));
                      goto use_tmp1;
                      }
                  }
                }
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_BREAK) {
      if (!CDR(n)) {
      } else {
        if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_COMMA_EXPR{387}(0 = F_BREAK{388}, +{389}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{390}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    } else if (CAR(n)->token == F_CAST) {
      if (!CDR(n)) {
      } else {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_COMMA_EXPR{346}(F_CAST{347}(0 = *{348}, *), 1 = +{350})""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""F_COMMA_EXPR{351}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CAAR(n),
          ADD_NODE_REF2(CDR(n),
            tmp1 = mknode(F_COMMA_EXPR, CAAR(n), CDR(n));
          ));
          goto use_tmp1;
        }
      }
    } else if (CAR(n)->token == F_COMMA_EXPR) {
      if (!CDAR(n)) {
      } else {
        if (CDAR(n)->token == F_BREAK) {
          if (!CDR(n)) {
          } else {
            if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_COMMA_EXPR{403}(0 = F_COMMA_EXPR{404}(*, F_BREAK{406}), +{407}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""0 = *{408}""\n");
                }
#endif /* PIKE_DEBUG */
              goto use_car;
            }
          }
        } else if (CDAR(n)->token == F_CONSTANT) {
          if (!CDR(n)) {
          } else {
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "Match: ""F_COMMA_EXPR{302}(F_COMMA_EXPR{303}(0 = *{304}, F_CONSTANT{305}), 1 = +{306})""\n");
              }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "=> ""F_COMMA_EXPR{307}""\n");
              }
#endif /* PIKE_DEBUG */
            {
              ADD_NODE_REF2(CAAR(n),
              ADD_NODE_REF2(CDR(n),
                tmp1 = mknode(F_COMMA_EXPR, CAAR(n), CDR(n));
              ));
              goto use_tmp1;
            }
          }
        } else if (CDAR(n)->token == F_CONTINUE) {
          if (!CDR(n)) {
          } else {
            if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_COMMA_EXPR{397}(0 = F_COMMA_EXPR{398}(*, F_CONTINUE{400}), +{401}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""0 = *{402}""\n");
                }
#endif /* PIKE_DEBUG */
              goto use_car;
            }
          }
        } else if (CDAR(n)->token == F_POP_VALUE) {
          if (!CDR(n)) {
          } else {
            if (CDR(n)->token == F_POP_VALUE) {
              if (!CADR(n)) {
              } else {
                if ((!(CADR(n)->tree_info & OPT_APPLY))) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_COMMA_EXPR{330}(F_COMMA_EXPR{331}(0 = *{332}, F_POP_VALUE{333}(1 = *{334}, *)), F_POP_VALUE{336}(2 = +{337}[!(CADR(n)->tree_info & OPT_APPLY)], *))""\n");
                    }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "=> ""F_COMMA_EXPR{339}""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                    ADD_NODE_REF2(CAAR(n),
                    ADD_NODE_REF2(CADAR(n),
                    ADD_NODE_REF2(CADR(n),
                      tmp1 = mknode(F_COMMA_EXPR, CAAR(n), mknode(F_POP_VALUE, mknode(F_COMMA_EXPR, CADAR(n), CADR(n)), 0));
                    )));
                    goto use_tmp1;
                  }
                }
              }
            }
          }
        } else if (CDAR(n)->token == F_RETURN) {
          if (!CDR(n)) {
          } else {
            if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_COMMA_EXPR{391}(0 = F_COMMA_EXPR{392}(*, F_RETURN{394}), +{395}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""0 = *{396}""\n");
                }
#endif /* PIKE_DEBUG */
              goto use_car;
            }
          }
        }
        if ((node_is_tossable(CDAR(n)))) {
          if (!CDR(n)) {
          } else {
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "Match: ""F_COMMA_EXPR{310}(F_COMMA_EXPR{311}(0 = *{312}, +{313}[node_is_tossable(CDAR(n))]), 1 = +{314})""\n");
              }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "=> ""F_COMMA_EXPR{315}""\n");
              }
#endif /* PIKE_DEBUG */
            {
              ADD_NODE_REF2(CAAR(n),
              ADD_NODE_REF2(CDR(n),
                tmp1 = mknode(F_COMMA_EXPR, CAAR(n), CDR(n));
              ));
              goto use_tmp1;
            }
          }
        }
      }
    } else if (CAR(n)->token == F_CONSTANT) {
      if (!CDR(n)) {
      } else {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_COMMA_EXPR{294}(F_CONSTANT{295}, 0 = +{296})""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""0 = *{297}""\n");
          }
#endif /* PIKE_DEBUG */
        goto use_cdr;
      }
    } else if (CAR(n)->token == F_CONTINUE) {
      if (!CDR(n)) {
      } else {
        if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_COMMA_EXPR{383}(0 = F_CONTINUE{384}, +{385}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{386}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    } else if (CAR(n)->token == F_POP_VALUE) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == F_POP_VALUE) {
          if (!CADR(n)) {
          } else {
            if ((!(CADR(n)->tree_info & OPT_APPLY))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_COMMA_EXPR{318}(F_POP_VALUE{319}(0 = *{320}, *), F_POP_VALUE{322}(1 = +{323}[!(CADR(n)->tree_info & OPT_APPLY)], *))""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""F_POP_VALUE{325}""\n");
                }
#endif /* PIKE_DEBUG */
              {
                ADD_NODE_REF2(CAAR(n),
                ADD_NODE_REF2(CADR(n),
                  tmp1 = mknode(F_POP_VALUE, mknode(F_COMMA_EXPR, CAAR(n), CADR(n)), 0);
                ));
                goto use_tmp1;
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_RETURN) {
      if (!CDR(n)) {
      } else {
        if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_COMMA_EXPR{379}(0 = F_RETURN{380}, +{381}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{382}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    }
    if (CDR(n)->token == F_CAST) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_COMMA_EXPR{354}(0 = *{355}, 1 = F_CAST{356}(2 = *{357}, *))""\n");
        }
#endif /* PIKE_DEBUG */
      {
        struct pike_type *type = CDR(n)->type;
        
        ADD_NODE_REF2(CADR(n),
        ADD_NODE_REF2(CAR(n),
          tmp1 = mkcastnode(type, mknode(F_COMMA_EXPR, CAR(n), CADR(n)));
        ));
        goto use_tmp1;
      }
    } else if (CDR(n)->token == F_COMMA_EXPR) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_COMMA_EXPR{369}(0 = *{370}, F_COMMA_EXPR{371}(1 = *{372}, 2 = *{373}))""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""F_COMMA_EXPR{374}""\n");
        }
#endif /* PIKE_DEBUG */
      {
        ADD_NODE_REF2(CAR(n),
        ADD_NODE_REF2(CADR(n),
        ADD_NODE_REF2(CDDR(n),
          tmp1 = mknode(F_COMMA_EXPR, mknode(F_COMMA_EXPR, CAR(n), CADR(n)), CDDR(n));
        )));
        goto use_tmp1;
      }
    }
    if ((node_is_tossable(CAR(n)))) {
      if (!CDR(n)) {
      } else {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_COMMA_EXPR{298}(+{299}[node_is_tossable(CAR(n))], 0 = +{300})""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""0 = *{301}""\n");
          }
#endif /* PIKE_DEBUG */
        goto use_cdr;
      }
    }
  }
  break;

case F_DEC_LOOP:
  if (!CDR(n)) {
    if (!CAR(n)) {
    } else {
      if (CAR(n)->token == F_VAL_LVAL) {
        if (!CAAR(n)) {
        } else {
          if (( !(CAAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|
						OPT_CASE|OPT_CONTINUE|
						OPT_BREAK|OPT_RETURN)) )) {
            if (!CDAR(n)) {
            } else {
              if (( !(CDAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|
						OPT_CASE|OPT_CONTINUE|
						OPT_BREAK|OPT_RETURN)) )) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_DEC_LOOP{1281}(F_VAL_LVAL{1282}(0 = +{1283}[ !(CAAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|\n"
                "\t\t\t\t\t\tOPT_CASE|OPT_CONTINUE|\n"
                "\t\t\t\t\t\tOPT_BREAK|OPT_RETURN)) ], 1 = +{1284}[ !(CDAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|\n"
                "\t\t\t\t\t\tOPT_CASE|OPT_CONTINUE|\n"
                "\t\t\t\t\t\tOPT_BREAK|OPT_RETURN)) ]), -{1285})""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_POP_VALUE{1286}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CDAR(n),
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CDAR(n),
                    tmp1 = mknode(F_POP_VALUE, mknode('?', mknode(F_GT, mknode(F_DEC, CDAR(n), 0), CAAR(n)), mknode(':', mknode(F_ASSIGN, CAAR(n), CDAR(n)), 0)), 0);
                  ))));
                  goto use_tmp1;
                }
              }
            }
          }
        }
      }
    }
  }
  break;

case F_DO:
  if (!CAR(n)) {
    if (!CDR(n)) {
    } else {
      if ((node_is_true(CDR(n)))) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_DO{952}(-{953}, 0 = +{954}[node_is_true(CDR(n))])""\n");
          }
#endif /* PIKE_DEBUG */
        {
          /* Infinite loop */
          
          ADD_NODE_REF2(CDR(n),
            tmp1 = mknode(F_DO, mkefuncallnode("sleep", mkintnode(255)), CDR(n));
          );
          goto use_tmp1;
        }
      }
    }
  } else if (!CDR(n)) {
    if (!CAR(n)) {
    } else {
      if ((!(CAR(n)->tree_info & (OPT_BREAK|OPT_CONTINUE)))) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_DO{944}(0 = +{945}[!(CAR(n)->tree_info & (OPT_BREAK|OPT_CONTINUE))], -{946})""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""0 = *{947}""\n");
          }
#endif /* PIKE_DEBUG */
        goto use_car;
      }
    }
  } else {
    if ((!(CAR(n)->tree_info & (OPT_BREAK|OPT_CONTINUE)))) {
      if (!CDR(n)) {
      } else {
        if ((node_is_false(CDR(n)))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_DO{948}(0 = +{949}[!(CAR(n)->tree_info & (OPT_BREAK|OPT_CONTINUE))], +{950}[node_is_false(CDR(n))])""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{951}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    }
  }
  break;

case F_EQ:
  if (!CAR(n)) {
  } else {
    if (CAR(n)->token == F_APPLY) {
      if (!CAAR(n)) {
      } else {
        if (CAAR(n)->token == F_CONSTANT) {
          if ((CAAR(n)->u.sval.type == T_FUNCTION) &&
              (CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
              (CAAR(n)->u.sval.u.efun->function == f_search)) {
            if (!CDAR(n)) {
            } else {
              if (CDAR(n)->token == F_ARG_LIST) {
                if (!CADAR(n)) {
                } else {
                  if (CADAR(n)->token == F_APPLY) {
                    if (!CAADAR(n)) {
                    } else {
                      if (CAADAR(n)->token == F_CONSTANT) {
                        if ((CAADAR(n)->u.sval.type == T_FUNCTION) &&
                            (CAADAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                            (CAADAR(n)->u.sval.u.efun->function == f_indices)) {
                          if (!CDADAR(n)) {
                          } else {
                            if ((pike_types_le(CDADAR(n)->type, mapping_type_string))) {
                              if (!CDR(n)) {
                              } else {
                                if (CDR(n)->token == F_CONSTANT) {
                                  if ((CDR(n)->u.sval.type == T_INT) &&
                                      (CDR(n)->u.sval.u.integer == -1)) {
#ifdef PIKE_DEBUG
                                      if (l_flag > 4) {
                                        fprintf(stderr, "Match: ""F_EQ{72}(F_APPLY{73}(F_CONSTANT{74}[CAAR(n)->u.sval.type == T_FUNCTION][CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAR(n)->u.sval.u.efun->function == f_search], F_ARG_LIST{75}(F_APPLY{76}(F_CONSTANT{77}[CAADAR(n)->u.sval.type == T_FUNCTION][CAADAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAADAR(n)->u.sval.u.efun->function == f_indices], 0 = +{78}[pike_types_le(CDADAR(n)->type, mapping_type_string)]), 1 = *{79})), F_CONSTANT{80}[CDR(n)->u.sval.type == T_INT][CDR(n)->u.sval.u.integer == -1])""\n");
                                      }
#endif /* PIKE_DEBUG */
                                    {
                                      
                                      ADD_NODE_REF2(CDDAR(n),
                                      ADD_NODE_REF2(CDADAR(n),
                                        tmp1 = mkefuncallnode("zero_type",mknode(F_ARG_LIST, CDADAR(n), CDDAR(n)));
                                      ));
                                      goto use_tmp1;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  break;

case F_FOR:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_FOR{1025}(-{1026}, *)""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""-{1028}""\n");
      }
#endif /* PIKE_DEBUG */
    goto zap_node;
  } else if (!CDR(n)) {
    if (!CAR(n)) {
    } else {
      if (CAR(n)->token == F_DEC) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_FOR{1143}(F_DEC{1144}(0 = *{1145}, *), -{1147})""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""F_DEC_NEQ_LOOP{1148}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CAAR(n),
            tmp1 = mknode(F_DEC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(0), CAAR(n)), 0);
          );
          goto use_tmp1;
        }
      } else if (CAR(n)->token == F_INC) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_FOR{1047}(F_INC{1048}(0 = *{1049}, *), -{1051})""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""F_INC_NEQ_LOOP{1052}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CAAR(n),
            tmp1 = mknode(F_INC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(0), CAAR(n)), 0);
          );
          goto use_tmp1;
        }
      } else if (CAR(n)->token == F_POST_DEC) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_FOR{1191}(F_POST_DEC{1192}(0 = *{1193}, *), -{1195})""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""F_DEC_NEQ_LOOP{1196}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CAAR(n),
            tmp1 = mknode(F_DEC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(-1), CAAR(n)), 0);
          );
          goto use_tmp1;
        }
      } else if (CAR(n)->token == F_POST_INC) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_FOR{1095}(F_POST_INC{1096}(0 = *{1097}, *), -{1099})""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""F_INC_NEQ_LOOP{1100}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CAAR(n),
            tmp1 = mknode(F_INC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(1), CAAR(n)), 0);
          );
          goto use_tmp1;
        }
      }
      if ((node_is_false(CAR(n)))) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_FOR{1037}(+{1038}[node_is_false(CAR(n))], *)""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""-{1040}""\n");
          }
#endif /* PIKE_DEBUG */
        goto zap_node;
      }
      if ((node_is_tossable(CAR(n)))) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_FOR{1044}(0 = +{1045}[node_is_tossable(CAR(n))], -{1046})""\n");
          }
#endif /* PIKE_DEBUG */
        {
          /* CPU-wasting delay loop [bug 3907]. */
          
          ADD_NODE_REF2(CAR(n),
            tmp1 = mknode(F_FOR, CAR(n), mknode(':',
        				mkefuncallnode("sleep", mkintnode(1)),
        				0));
          );
          goto use_tmp1;
        }
      }
      if ((node_is_true(CAR(n)))) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_FOR{1041}(0 = +{1042}[node_is_true(CAR(n))], -{1043})""\n");
          }
#endif /* PIKE_DEBUG */
        {
          /* Infinite loop */
          
          ADD_NODE_REF2(CAR(n),
            tmp1 = mknode(F_FOR, CAR(n), mknode(':',
        				mkefuncallnode("sleep", mkintnode(255)),
        				0));
          );
          goto use_tmp1;
        }
      }
    }
  } else {
    if (CAR(n)->token == F_APPLY) {
      if (!CAAR(n)) {
      } else {
        if (CAAR(n)->token == F_CONSTANT) {
          if ((CAAR(n)->u.sval.type == T_FUNCTION) &&
              (CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN)) {
            if (!CDR(n)) {
            } else {
              if (CDR(n)->token == ':') {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_FOR{1251}(0 = F_APPLY{1252}(1 = F_CONSTANT{1253}[CAAR(n)->u.sval.type == T_FUNCTION][CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN], 2 = *{1254}), 4 = ':'{1255}(3 = *{1256}, *))""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  node **last;

                  /* Last is a pointer to the place where the incrementor is in the
                   * tree. This is needed so we can nullify this pointer later and
                   * free the rest of the tree
                   */
                  last = &_CDR(CDR(n));
                  tmp1 = *last;

                  /* We're not interested in casts to void */
                  while(tmp1 &&
                	( (tmp1->token == F_CAST && tmp1->type == void_type_string) ||
                	  tmp1->token == F_POP_VALUE))
                  {
                    last = &_CAR(tmp1);
                    tmp1 = *last;
                  }

                  /* If there is an incrementor, and it is one of x++, ++x, x-- or ++x */
                  if(tmp1 && (tmp1->token == F_INC ||
                	      tmp1->token == F_POST_INC ||
                	      tmp1->token == F_DEC ||
                	      tmp1->token == F_POST_DEC))
                  {
                    node **arg1, **arg2;
                    int oper;
                    int inc;
                    int token;

                    /* does it increment or decrement ? */
                    inc = (tmp1->token==F_INC || tmp1->token==F_POST_INC);

                    /* for(; arg1 oper arg2; z ++) p; */

                    if(CAAR(n)->u.sval.u.efun->function == f_gt)
                      oper = F_GT;
                    else if(CAAR(n)->u.sval.u.efun->function == f_ge)
                      oper = F_GE;
                    else if(CAAR(n)->u.sval.u.efun->function == f_lt)
                      oper = F_LT;
                    else if(CAAR(n)->u.sval.u.efun->function == f_le)
                      oper = F_LE;
                    else if(CAAR(n)->u.sval.u.efun->function == f_ne)
                      oper = F_NE;
                    else
                      goto next_for_opt;

                    if(count_args(CDAR(n)) != 2)
                      goto next_for_opt;

                    arg1 = my_get_arg(&_CDR(CAR(n)), 0);
                    arg2 = my_get_arg(&_CDR(CAR(n)), 1);

                    /* it was not on the form for(; x op y; z++) p; */
                    if(!node_is_eq(*arg1, CAR(tmp1)) || /* x == z */
                       depend_p(*arg2, *arg2) ||	/* does y depend on y? */
                       depend_p(*arg2, *arg1) ||	/* does y depend on x? */
                       depend_p(*arg2, CADR(n)) ||		/* does y depend on p? */
                       depend_p(*arg2, tmp1))		/* does y depend on z? */
                    {
                      /* it was not on the form for(; x op y; z++) p; */
                      if(!node_is_eq(*arg2, CAR(tmp1)) || /* y == z */
                	 depend_p(*arg1, *arg2) ||	/* does x depend on y? */
                	 depend_p(*arg1, *arg1) ||	/* does x depend on x? */
                	 depend_p(*arg1, CADR(n)) ||		/* does x depend on p? */
                	 depend_p(*arg1, tmp1))		/* does x depend on z? */
                      {
                	/* it was not on the form for(; x op y; y++) p; */
                	goto next_for_opt;
                      }else{
                	node **tmparg;
                	/* for(; x op y; y++) p; -> for(; y op^-1 x; y++) p; */
                	
                	switch(oper)
                	{
                	case F_LT: oper = F_GT; break;
                	case F_LE: oper = F_GE; break;
                	case F_GT: oper = F_LT; break;
                	case F_GE: oper = F_LE; break;
                	}
                	    
                	tmparg = arg1;
                	arg1 = arg2;
                	arg2 = tmparg;
                      }
                    }

                    if(inc)
                    {
                      if(oper == F_LE) {
                	static struct pike_string *plus_name;
                	node *fun;
                	if ((!plus_name && !(plus_name = findstring("`+"))) ||
                	    !(fun=find_module_identifier(plus_name, 0))) {
                	  yyerror("Internally used efun undefined: `+");
                	  tmp3 = mkintnode(0);
                        } else {
                	  ADD_NODE_REF2(*arg2,
                	    tmp3 = mkapplynode(fun, mknode(F_ARG_LIST, *arg2, mkintnode(1)));
                	  );
                	}
                      } else if(oper == F_LT) {
                	ADD_NODE_REF2(*arg2,
                	  tmp3 = *arg2;
                	);
                      } else
                	goto next_for_opt;
                    }else{
                      if(oper == F_GE) {
                	static struct pike_string *minus_name;
                	node *fun;
                	if ((!minus_name && !(minus_name = findstring("`-"))) ||
                	    !(fun=find_module_identifier(minus_name, 0))) {
                	  yyerror("Internally used efun undefined: `-");
                	  tmp3 = mkintnode(0);
                        } else {
                	  ADD_NODE_REF2(*arg2,
                	    tmp3 = mkapplynode(fun, mknode(F_ARG_LIST, *arg2, mkintnode(1)));
                	  );
                	}
                      } else if(oper == F_GT) {
                	ADD_NODE_REF2(*arg2,
                	  tmp3 = *arg2;
                	);
                      } else
                	goto next_for_opt;
                    }
                    if(oper == F_NE)
                    {
                      if(inc)
                	token = F_INC_NEQ_LOOP;
                      else
                	token = F_DEC_NEQ_LOOP;
                    }else{
                      if(inc)
                	token = F_INC_LOOP;
                      else
                	token = F_DEC_LOOP;
                    }

                    tmp1=CAR(*last);
                    ADD_NODE_REF(CAR(*last));
                    ADD_NODE_REF2(*arg1,
                    ADD_NODE_REF2(CADR(n),
                      tmp2 = mknode(token, mknode(F_VAL_LVAL, tmp3, *arg1), CADR(n));
                    ));

                    tmp1 = mknode(F_COMMA_EXPR, mkcastnode(void_type_string,
                					   mknode(inc ? F_DEC : F_INC, tmp1, 0)), tmp2);
                    goto use_tmp1;
                  }
                 next_for_opt:
                  ;
                }
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_DEC) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == ':') {
          if (!CADR(n)) {
            if (!CDDR(n)) {
            } else {
              if ((!(CDDR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_FOR{1153}(F_DEC{1154}(0 = *{1155}, *), ':'{1157}(-{1158}, 1 = +{1159}[!(CDDR(n)->tree_info & OPT_CONTINUE)]))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_DEC_NEQ_LOOP{1160}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CDDR(n),
                    tmp1 = mknode(F_DEC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(0), CAAR(n)), CDDR(n));
                  ));
                  goto use_tmp1;
                }
              }
            }
          } else if (!CDDR(n)) {
            if (!CADR(n)) {
            } else {
              if ((!(CADR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_FOR{1165}(F_DEC{1166}(0 = *{1167}, *), ':'{1169}(1 = +{1170}[!(CADR(n)->tree_info & OPT_CONTINUE)], -{1171}))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_DEC_NEQ_LOOP{1172}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CADR(n),
                    tmp1 = mknode(F_DEC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(0), CAAR(n)), CADR(n));
                  ));
                  goto use_tmp1;
                }
              }
            }
          } else {
            if ((!(CADR(n)->tree_info & OPT_CONTINUE))) {
              if (!CDDR(n)) {
              } else {
                if ((!(CDDR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_FOR{1177}(F_DEC{1178}(0 = *{1179}, *), ':'{1181}(1 = +{1182}[!(CADR(n)->tree_info & OPT_CONTINUE)], 2 = +{1183}[!(CDDR(n)->tree_info & OPT_CONTINUE)]))""\n");
                    }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "=> ""F_DEC_NEQ_LOOP{1184}""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                    ADD_NODE_REF2(CAAR(n),
                    ADD_NODE_REF2(CADR(n),
                    ADD_NODE_REF2(CDDR(n),
                      tmp1 = mknode(F_DEC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(0), CAAR(n)), mknode(F_COMMA_EXPR, CADR(n), CDDR(n)));
                    )));
                    goto use_tmp1;
                  }
                }
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_INC) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == ':') {
          if (!CADR(n)) {
            if (!CDDR(n)) {
            } else {
              if ((!(CDDR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_FOR{1057}(F_INC{1058}(0 = *{1059}, *), ':'{1061}(-{1062}, 1 = +{1063}[!(CDDR(n)->tree_info & OPT_CONTINUE)]))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_INC_NEQ_LOOP{1064}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CDDR(n),
                    tmp1 = mknode(F_INC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(0), CAAR(n)), CDDR(n));
                  ));
                  goto use_tmp1;
                }
              }
            }
          } else if (!CDDR(n)) {
            if (!CADR(n)) {
            } else {
              if ((!(CADR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_FOR{1069}(F_INC{1070}(0 = *{1071}, *), ':'{1073}(1 = +{1074}[!(CADR(n)->tree_info & OPT_CONTINUE)], -{1075}))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_INC_NEQ_LOOP{1076}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CADR(n),
                    tmp1 = mknode(F_INC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(0), CAAR(n)), CADR(n));
                  ));
                  goto use_tmp1;
                }
              }
            }
          } else {
            if ((!(CADR(n)->tree_info & OPT_CONTINUE))) {
              if (!CDDR(n)) {
              } else {
                if ((!(CDDR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_FOR{1081}(F_INC{1082}(0 = *{1083}, *), ':'{1085}(1 = +{1086}[!(CADR(n)->tree_info & OPT_CONTINUE)], 2 = +{1087}[!(CDDR(n)->tree_info & OPT_CONTINUE)]))""\n");
                    }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "=> ""F_INC_NEQ_LOOP{1088}""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                    ADD_NODE_REF2(CAAR(n),
                    ADD_NODE_REF2(CADR(n),
                    ADD_NODE_REF2(CDDR(n),
                      tmp1 = mknode(F_INC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(0), CAAR(n)), mknode(F_COMMA_EXPR, CADR(n), CDDR(n)));
                    )));
                    goto use_tmp1;
                  }
                }
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_POST_DEC) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == ':') {
          if (!CADR(n)) {
            if (!CDDR(n)) {
            } else {
              if ((!(CDDR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_FOR{1201}(F_POST_DEC{1202}(0 = *{1203}, *), ':'{1205}(-{1206}, 1 = +{1207}[!(CDDR(n)->tree_info & OPT_CONTINUE)]))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_DEC_NEQ_LOOP{1208}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CDDR(n),
                    tmp1 = mknode(F_DEC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(-1), CAAR(n)), CDDR(n));
                  ));
                  goto use_tmp1;
                }
              }
            }
          } else if (!CDDR(n)) {
            if (!CADR(n)) {
            } else {
              if ((!(CADR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_FOR{1213}(F_POST_DEC{1214}(0 = *{1215}, *), ':'{1217}(1 = +{1218}[!(CADR(n)->tree_info & OPT_CONTINUE)], -{1219}))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_DEC_NEQ_LOOP{1220}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CADR(n),
                    tmp1 = mknode(F_DEC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(-1), CAAR(n)), CADR(n));
                  ));
                  goto use_tmp1;
                }
              }
            }
          } else {
            if ((!(CADR(n)->tree_info & OPT_CONTINUE))) {
              if (!CDDR(n)) {
              } else {
                if ((!(CDDR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_FOR{1225}(F_POST_DEC{1226}(0 = *{1227}, *), ':'{1229}(1 = +{1230}[!(CADR(n)->tree_info & OPT_CONTINUE)], 2 = +{1231}[!(CDDR(n)->tree_info & OPT_CONTINUE)]))""\n");
                    }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "=> ""F_DEC_NEQ_LOOP{1232}""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                    ADD_NODE_REF2(CAAR(n),
                    ADD_NODE_REF2(CADR(n),
                    ADD_NODE_REF2(CDDR(n),
                      tmp1 = mknode(F_DEC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(-1), CAAR(n)), mknode(F_COMMA_EXPR, CADR(n), CDDR(n)));
                    )));
                    goto use_tmp1;
                  }
                }
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_POST_INC) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == ':') {
          if (!CADR(n)) {
            if (!CDDR(n)) {
            } else {
              if ((!(CDDR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_FOR{1105}(F_POST_INC{1106}(0 = *{1107}, *), ':'{1109}(-{1110}, 1 = +{1111}[!(CDDR(n)->tree_info & OPT_CONTINUE)]))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_INC_NEQ_LOOP{1112}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CDDR(n),
                    tmp1 = mknode(F_INC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(1), CAAR(n)), CDDR(n));
                  ));
                  goto use_tmp1;
                }
              }
            }
          } else if (!CDDR(n)) {
            if (!CADR(n)) {
            } else {
              if ((!(CADR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_FOR{1117}(F_POST_INC{1118}(0 = *{1119}, *), ':'{1121}(1 = +{1122}[!(CADR(n)->tree_info & OPT_CONTINUE)], -{1123}))""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_INC_NEQ_LOOP{1124}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CADR(n),
                    tmp1 = mknode(F_INC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(1), CAAR(n)), CADR(n));
                  ));
                  goto use_tmp1;
                }
              }
            }
          } else {
            if ((!(CADR(n)->tree_info & OPT_CONTINUE))) {
              if (!CDDR(n)) {
              } else {
                if ((!(CDDR(n)->tree_info & OPT_CONTINUE))) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_FOR{1129}(F_POST_INC{1130}(0 = *{1131}, *), ':'{1133}(1 = +{1134}[!(CADR(n)->tree_info & OPT_CONTINUE)], 2 = +{1135}[!(CDDR(n)->tree_info & OPT_CONTINUE)]))""\n");
                    }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "=> ""F_INC_NEQ_LOOP{1136}""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                    ADD_NODE_REF2(CAAR(n),
                    ADD_NODE_REF2(CADR(n),
                    ADD_NODE_REF2(CDDR(n),
                      tmp1 = mknode(F_INC_NEQ_LOOP, mknode(F_VAL_LVAL, mkintnode(1), CAAR(n)), mknode(F_COMMA_EXPR, CADR(n), CDDR(n)));
                    )));
                    goto use_tmp1;
                  }
                }
              }
            }
          }
        }
      }
    }
    if (CDR(n)->token == ':') {
      if (!CADR(n)) {
        if (!CDDR(n)) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_FOR{1029}(0 = *{1030}, ':'{1031}(-{1032}, -{1033}))""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""F_FOR{1034}""\n");
            }
#endif /* PIKE_DEBUG */
          {
            ADD_NODE_REF2(CAR(n),
              tmp1 = mknode(F_FOR, CAR(n), 0);
            );
            goto use_tmp1;
          }
        }
      } else if (!CDDR(n)) {
      } else {
        if (CDDR(n)->token == F_CAST) {
          if ((CDDR(n)->type == void_type_string)) {
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "Match: ""F_FOR{1239}(0 = *{1240}, ':'{1241}(1 = *{1242}, F_CAST{1243}[CDDR(n)->type == void_type_string](2 = *{1244}, *)))""\n");
              }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "=> ""F_FOR{1246}""\n");
              }
#endif /* PIKE_DEBUG */
            {
              ADD_NODE_REF2(CAR(n),
              ADD_NODE_REF2(CADR(n),
              ADD_NODE_REF2(CADDR(n),
                tmp1 = mknode(F_FOR, CAR(n), mknode(':', CADR(n), CADDR(n)));
              )));
              goto use_tmp1;
            }
          }
        }
      }
    }
    if ((node_is_false(CAR(n)))) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_FOR{1037}(+{1038}[node_is_false(CAR(n))], *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""-{1040}""\n");
        }
#endif /* PIKE_DEBUG */
      goto zap_node;
    }
  }
  break;

case F_FOREACH:
  if (!CAR(n)) {
  } else if (!CDR(n)) {
    if (!CAR(n)) {
    } else {
      if (CAR(n)->token == F_VAL_LVAL) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_FOREACH{955}(F_VAL_LVAL{956}(0 = *{957}, *), -{959})""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""F_POP_VALUE{960}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CAAR(n),
            tmp1 = mknode(F_POP_VALUE, CAAR(n), 0);
          );
          goto use_tmp1;
        }
      }
    }
  } else {
    if (CAR(n)->token == F_VAL_LVAL) {
      if (!CAAR(n)) {
      } else if (!CDAR(n)) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_FOREACH{981}(F_VAL_LVAL{982}(0 = *{983}, -{984}), 1 = *{985})""\n");
          }
#endif /* PIKE_DEBUG */
        {
            
          ADD_NODE_REF2(CDR(n),
          ADD_NODE_REF2(CAAR(n),
            tmp1 = mknode(F_LOOP, mkefuncallnode("sizeof", CAAR(n)), CDR(n));
          ));
          goto use_tmp1;
          }
      } else {
        if (CAAR(n)->token == F_APPLY) {
          if (!CAAAR(n)) {
          } else {
            if (CAAAR(n)->token == F_CONSTANT) {
              if ((CAAAR(n)->u.sval.type == T_FUNCTION) &&
                  (CAAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                  (CAAAR(n)->u.sval.u.efun->function == f_divide)) {
                if (!CDAAR(n)) {
                } else {
                  if (CDAAR(n)->token == F_ARG_LIST) {
                    if (!CADAAR(n)) {
                    } else {
                      if ((pike_types_le(CADAAR(n)->type, string_type_string))) {
                        if (!CDDAAR(n)) {
                        } else {
                          if (CDDAAR(n)->token == F_CONSTANT) {
                            if ((CDDAAR(n)->u.sval.type == T_STRING) &&
                                (CDDAAR(n)->u.sval.u.string->len == 1)) {
#ifdef PIKE_DEBUG
                                if (l_flag > 4) {
                                  fprintf(stderr, "Match: ""F_FOREACH{986}(F_VAL_LVAL{987}(F_APPLY{988}(F_CONSTANT{989}[CAAAR(n)->u.sval.type == T_FUNCTION][CAAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAAR(n)->u.sval.u.efun->function == f_divide], F_ARG_LIST{990}(0 = +{991}[pike_types_le(CADAAR(n)->type, string_type_string)], 1 = F_CONSTANT{992}[CDDAAR(n)->u.sval.type == T_STRING][CDDAAR(n)->u.sval.u.string->len == 1])), 2 = *{993}), 3 = *{994})""\n");
                                }
#endif /* PIKE_DEBUG */
                              {
                                extern struct program *string_split_iterator_program;
                                node *vars;
                                p_wchar2 split = index_shared_string(CDDAAR(n)->u.sval.u.string, 0);

                                ADD_NODE_REF2(CDAR(n),
                                  if (CDAR(n)->token == ':') {
                                    vars = CDAR(n);
                                  } else {
                                    /* Old-style. Convert to new-style. */
                                    vars = mknode(':', NULL, CDAR(n));
                                  }
                                );

                                
                                ADD_NODE_REF2(CDR(n),
                                ADD_NODE_REF2(CADAAR(n),
                                  tmp1 = mknode(F_FOREACH,
                              	      mknode(F_VAL_LVAL,
                              		     mkapplynode(mkprgnode(string_split_iterator_program),
                              				 mknode(F_ARG_LIST, CADAAR(n),
                              					mkintnode(split))),
                              		     vars),
                              	      CDR(n));
                                ));
                                goto use_tmp1;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              if ((CAAAR(n)->u.sval.type == T_FUNCTION) &&
                  (CAAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                  (CAAAR(n)->u.sval.u.efun->function == f_minus)) {
                if (!CDAAR(n)) {
                } else {
                  if (CDAAR(n)->token == F_ARG_LIST) {
                    if (!CADAAR(n)) {
                    } else {
                      if (CADAAR(n)->token == F_APPLY) {
                        if (!CAADAAR(n)) {
                        } else {
                          if (CAADAAR(n)->token == F_CONSTANT) {
                            if ((CAADAAR(n)->u.sval.type == T_FUNCTION) &&
                                (CAADAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                                (CAADAAR(n)->u.sval.u.efun->function == f_divide)) {
                              if (!CDADAAR(n)) {
                              } else {
                                if (CDADAAR(n)->token == F_ARG_LIST) {
                                  if (!CADADAAR(n)) {
                                  } else {
                                    if ((pike_types_le(CADADAAR(n)->type, string_type_string))) {
                                      if (!CDDADAAR(n)) {
                                      } else {
                                        if (CDDADAAR(n)->token == F_CONSTANT) {
                                          if ((CDDADAAR(n)->u.sval.type == T_STRING) &&
                                              (CDDADAAR(n)->u.sval.u.string->len == 1)) {
                                            if (!CDDAAR(n)) {
                                            } else {
                                              if (CDDAAR(n)->token == F_APPLY) {
                                                if (!CADDAAR(n)) {
                                                } else {
                                                  if (CADDAAR(n)->token == F_CONSTANT) {
                                                    if ((CADDAAR(n)->u.sval.type == T_FUNCTION) &&
                                                        (CADDAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                                                        (CADDAAR(n)->u.sval.u.efun->function == f_allocate)) {
                                                      if (!CDDDAAR(n)) {
                                                      } else {
                                                        if (CDDDAAR(n)->token == F_ARG_LIST) {
                                                          if (!CADDDAAR(n)) {
                                                          } else {
                                                            if (CADDDAAR(n)->token == F_CONSTANT) {
                                                              if ((CADDDAAR(n)->u.sval.type == T_INT) &&
                                                                  (CADDDAAR(n)->u.sval.u.integer == 1)) {
                                                                if (!CDDDDAAR(n)) {
                                                                } else {
                                                                  if (CDDDDAAR(n)->token == F_CONSTANT) {
                                                                    if ((CDDDDAAR(n)->u.sval.type == T_STRING) &&
                                                                        (CDDDDAAR(n)->u.sval.u.string->len == 0)) {
#ifdef PIKE_DEBUG
                                                                        if (l_flag > 4) {
                                                                          fprintf(stderr, "Match: ""F_FOREACH{1008}(F_VAL_LVAL{1009}(F_APPLY{1010}(F_CONSTANT{1011}[CAAAR(n)->u.sval.type == T_FUNCTION][CAAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAAR(n)->u.sval.u.efun->function == f_minus], F_ARG_LIST{1012}(F_APPLY{1013}(F_CONSTANT{1014}[CAADAAR(n)->u.sval.type == T_FUNCTION][CAADAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAADAAR(n)->u.sval.u.efun->function == f_divide], F_ARG_LIST{1015}(0 = +{1016}[pike_types_le(CADADAAR(n)->type, string_type_string)], 1 = F_CONSTANT{1017}[CDDADAAR(n)->u.sval.type == T_STRING][CDDADAAR(n)->u.sval.u.string->len == 1])), F_APPLY{1018}(F_CONSTANT{1019}[CADDAAR(n)->u.sval.type == T_FUNCTION][CADDAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CADDAAR(n)->u.sval.u.efun->function == f_allocate], F_ARG_LIST{1020}(F_CONSTANT{1021}[CADDDAAR(n)->u.sval.type == T_INT][CADDDAAR(n)->u.sval.u.integer == 1], F_CONSTANT{1022}[CDDDDAAR(n)->u.sval.type == T_STRING][CDDDDAAR(n)->u.sval.u.string->len == 0])))), 2 = *{1023}), 3 = *{1024})""\n");
                                                                        }
#endif /* PIKE_DEBUG */
                                                                      {
                                                                        extern struct program *string_split_iterator_program;
                                                                        node *vars;
                                                                        p_wchar2 split = index_shared_string(CDDADAAR(n)->u.sval.u.string, 0);

                                                                        ADD_NODE_REF2(CDAR(n),
                                                                          if (CDAR(n)->token == ':') {
                                                                            vars = CDAR(n);
                                                                          } else {
                                                                            /* Old-style. Convert to new-style. */
                                                                            vars = mknode(':', NULL, CDAR(n));
                                                                          }
                                                                        );

                                                                        
                                                                        ADD_NODE_REF2(CDR(n),
                                                                        ADD_NODE_REF2(CADADAAR(n),
                                                                          tmp1 = mknode(F_FOREACH,
                                                                      	      mknode(F_VAL_LVAL,
                                                                      		     mkapplynode(mkprgnode(string_split_iterator_program),
                                                                      				 mknode(F_ARG_LIST, CADADAAR(n),
                                                                      					mknode(F_ARG_LIST,
                                                                      					       mkintnode(split),
                                                                      					       mkintnode(1)))),
                                                                      		     vars),
                                                                      	      CDR(n));
                                                                        ));
                                                                        goto use_tmp1;
                                                                      }
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              } else if (CDDAAR(n)->token == F_CONSTANT) {
                                                if ((CDDAAR(n)->u.sval.type == T_ARRAY) &&
                                                    (CDDAAR(n)->u.sval.u.array->size == 1) &&
                                                    (CDDAAR(n)->u.sval.u.array->item[0].type == T_STRING) &&
                                                    (CDDAAR(n)->u.sval.u.array->item[0].u.string->len == 0)) {
#ifdef PIKE_DEBUG
                                                    if (l_flag > 4) {
                                                      fprintf(stderr, "Match: ""F_FOREACH{995}(F_VAL_LVAL{996}(F_APPLY{997}(F_CONSTANT{998}[CAAAR(n)->u.sval.type == T_FUNCTION][CAAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAAR(n)->u.sval.u.efun->function == f_minus], F_ARG_LIST{999}(F_APPLY{1000}(F_CONSTANT{1001}[CAADAAR(n)->u.sval.type == T_FUNCTION][CAADAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAADAAR(n)->u.sval.u.efun->function == f_divide], F_ARG_LIST{1002}(0 = +{1003}[pike_types_le(CADADAAR(n)->type, string_type_string)], 1 = F_CONSTANT{1004}[CDDADAAR(n)->u.sval.type == T_STRING][CDDADAAR(n)->u.sval.u.string->len == 1])), F_CONSTANT{1005}[CDDAAR(n)->u.sval.type == T_ARRAY][CDDAAR(n)->u.sval.u.array->size == 1][CDDAAR(n)->u.sval.u.array->item[0].type == T_STRING][CDDAAR(n)->u.sval.u.array->item[0].u.string->len == 0])), 2 = *{1006}), 3 = *{1007})""\n");
                                                    }
#endif /* PIKE_DEBUG */
                                                  {
                                                    extern struct program *string_split_iterator_program;
                                                    node *vars;
                                                    p_wchar2 split = index_shared_string(CDDADAAR(n)->u.sval.u.string, 0);

                                                    ADD_NODE_REF2(CDAR(n),
                                                      if (CDAR(n)->token == ':') {
                                                        vars = CDAR(n);
                                                      } else {
                                                        /* Old-style. Convert to new-style. */
                                                        vars = mknode(':', NULL, CDAR(n));
                                                      }
                                                    );

                                                    
                                                    ADD_NODE_REF2(CDR(n),
                                                    ADD_NODE_REF2(CADADAAR(n),
                                                      tmp1 = mknode(F_FOREACH,
                                                  	      mknode(F_VAL_LVAL,
                                                  		     mkapplynode(mkprgnode(string_split_iterator_program),
                                                  				 mknode(F_ARG_LIST, CADADAAR(n),
                                                  					mknode(F_ARG_LIST,
                                                  					       mkintnode(split),
                                                  					       mkintnode(1)))),
                                                  		     vars),
                                                  	      CDR(n));
                                                    ));
                                                    goto use_tmp1;
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        if (( pike_types_le(CAAR(n)->type, array_type_string) )) {
          if (!CDAR(n)) {
          } else {
            if (CDAR(n)->token == ':') {
              if (!CADAR(n)) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_FOREACH{969}(F_VAL_LVAL{970}(0 = +{971}[ pike_types_le(CAAR(n)->type, array_type_string) ], ':'{972}(-{973}, 1 = *{974})), 2 = *{975})""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_FOREACH{976}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CDDAR(n),
                  ADD_NODE_REF2(CDR(n),
                    tmp1 = mknode(F_FOREACH, mknode(F_VAL_LVAL, CAAR(n), CDDAR(n)), CDR(n));
                  )));
                  goto use_tmp1;
                }
              }
            }
          }
        }
      }
    }
  }
  break;

case F_GE:
  if (!CAR(n)) {
  } else {
    if (CAR(n)->token == F_APPLY) {
      if (!CAAR(n)) {
      } else {
        if (CAAR(n)->token == F_CONSTANT) {
          if ((CAAR(n)->u.sval.type == T_FUNCTION) &&
              (CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
              (CAAR(n)->u.sval.u.efun->function == f_search)) {
            if (!CDAR(n)) {
            } else {
              if (CDAR(n)->token == F_ARG_LIST) {
                if (!CADAR(n)) {
                } else {
                  if (CADAR(n)->token == F_APPLY) {
                    if (!CAADAR(n)) {
                    } else {
                      if (CAADAR(n)->token == F_CONSTANT) {
                        if ((CAADAR(n)->u.sval.type == T_FUNCTION) &&
                            (CAADAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                            (CAADAR(n)->u.sval.u.efun->function == f_indices)) {
                          if (!CDADAR(n)) {
                          } else {
                            if ((pike_types_le(CDADAR(n)->type, mapping_type_string))) {
                              if (!CDR(n)) {
                              } else {
                                if (CDR(n)->token == F_CONSTANT) {
                                  if ((CDR(n)->u.sval.type == T_INT) &&
                                      (!CDR(n)->u.sval.u.integer)) {
#ifdef PIKE_DEBUG
                                      if (l_flag > 4) {
                                        fprintf(stderr, "Match: ""F_GE{81}(F_APPLY{82}(F_CONSTANT{83}[CAAR(n)->u.sval.type == T_FUNCTION][CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAR(n)->u.sval.u.efun->function == f_search], F_ARG_LIST{84}(F_APPLY{85}(F_CONSTANT{86}[CAADAR(n)->u.sval.type == T_FUNCTION][CAADAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAADAR(n)->u.sval.u.efun->function == f_indices], 0 = +{87}[pike_types_le(CDADAR(n)->type, mapping_type_string)]), 1 = *{88})), F_CONSTANT{89}[CDR(n)->u.sval.type == T_INT][!CDR(n)->u.sval.u.integer])""\n");
                                      }
#endif /* PIKE_DEBUG */
                                    {
                                      
                                      ADD_NODE_REF2(CDDAR(n),
                                      ADD_NODE_REF2(CDADAR(n),
                                        tmp1 = mknode(F_NOT,mkefuncallnode("zero_type",mknode(F_ARG_LIST, CDADAR(n), CDDAR(n))),0);
                                      ));
                                      goto use_tmp1;
                                    }
                                  }
                                  if ((CDR(n)->u.sval.type == T_INT) &&
                                      (CDR(n)->u.sval.u.integer == -1)) {
#ifdef PIKE_DEBUG
                                      if (l_flag > 4) {
                                        fprintf(stderr, "Match: ""F_GE{90}(F_APPLY{91}(F_CONSTANT{92}[CAAR(n)->u.sval.type == T_FUNCTION][CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAR(n)->u.sval.u.efun->function == f_search], F_ARG_LIST{93}(F_APPLY{94}(F_CONSTANT{95}[CAADAR(n)->u.sval.type == T_FUNCTION][CAADAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAADAR(n)->u.sval.u.efun->function == f_indices], 0 = +{96}[pike_types_le(CDADAR(n)->type, mapping_type_string)]), 1 = *{97})), F_CONSTANT{98}[CDR(n)->u.sval.type == T_INT][CDR(n)->u.sval.u.integer == -1])""\n");
                                      }
#endif /* PIKE_DEBUG */
                                    {
                                      
                                      ADD_NODE_REF2(CDDAR(n),
                                      ADD_NODE_REF2(CDADAR(n),
                                        tmp1 = mknode(F_NOT,mkefuncallnode("zero_type",mknode(F_ARG_LIST, CDADAR(n), CDDAR(n))),0);
                                      ));
                                      goto use_tmp1;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  break;

case F_GT:
#ifdef PIKE_DEBUG
    if (l_flag > 4) {
      fprintf(stderr, "Match: ""F_GT{60}(0 = *{61}, 1 = *{62})""\n");
    }
#endif /* PIKE_DEBUG */
  {
      
    ADD_NODE_REF2(CDR(n),
    ADD_NODE_REF2(CAR(n),
      tmp1 = mkopernode("`<", CAR(n), CDR(n));
    ));
    goto use_tmp1;
    }
  break;

case F_INC_LOOP:
  if (!CAR(n)) {
  } else if (!CDR(n)) {
    if (!CAR(n)) {
    } else {
      if (CAR(n)->token == F_VAL_LVAL) {
        if (!CAAR(n)) {
        } else {
          if (( !(CAAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|
						OPT_CASE|OPT_CONTINUE|
						OPT_BREAK|OPT_RETURN)) )) {
            if (!CDAR(n)) {
            } else {
              if (( !(CDAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|
						OPT_CASE|OPT_CONTINUE|
						OPT_BREAK|OPT_RETURN)) )) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_INC_LOOP{1263}(F_VAL_LVAL{1264}(0 = +{1265}[ !(CAAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|\n"
                "\t\t\t\t\t\tOPT_CASE|OPT_CONTINUE|\n"
                "\t\t\t\t\t\tOPT_BREAK|OPT_RETURN)) ], 1 = +{1266}[ !(CDAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|\n"
                "\t\t\t\t\t\tOPT_CASE|OPT_CONTINUE|\n"
                "\t\t\t\t\t\tOPT_BREAK|OPT_RETURN)) ]), -{1267})""\n");
                  }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "=> ""F_POP_VALUE{1268}""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  ADD_NODE_REF2(CDAR(n),
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CAAR(n),
                  ADD_NODE_REF2(CDAR(n),
                    tmp1 = mknode(F_POP_VALUE, mknode('?', mknode(F_LT, mknode(F_INC, CDAR(n), 0), CAAR(n)), mknode(':', mknode(F_ASSIGN, CAAR(n), CDAR(n)), 0)), 0);
                  ))));
                  goto use_tmp1;
                }
              }
            }
          }
        }
      }
    }
  } else {
    if (CAR(n)->token == F_VAL_LVAL) {
      if (!CAAR(n)) {
      } else {
        if (( !(CAAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|
						OPT_CASE|OPT_CONTINUE|
						OPT_BREAK|OPT_RETURN)) )) {
          if (!CDAR(n)) {
          } else {
            if (( !(CDAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|
						OPT_CASE|OPT_CONTINUE|
						OPT_BREAK|OPT_RETURN)) )) {
              if (!CDR(n)) {
              } else {
                if (( !(CDR(n)->tree_info & (OPT_BREAK|OPT_CONTINUE)) ) &&
                    ( !depend2_p(CDR(n), CDAR(n)) )) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_INC_LOOP{1258}(F_VAL_LVAL{1259}(0 = +{1260}[ !(CAAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|\n"
                  "\t\t\t\t\t\tOPT_CASE|OPT_CONTINUE|\n"
                  "\t\t\t\t\t\tOPT_BREAK|OPT_RETURN)) ], 1 = +{1261}[ !(CDAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|\n"
                  "\t\t\t\t\t\tOPT_CASE|OPT_CONTINUE|\n"
                  "\t\t\t\t\t\tOPT_BREAK|OPT_RETURN)) ]), 2 = +{1262}[ !(CDR(n)->tree_info & (OPT_BREAK|OPT_CONTINUE)) ][ !depend2_p(CDR(n), CDAR(n)) ])""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                      
                    ADD_NODE_REF2(CDAR(n),
                    ADD_NODE_REF2(CDAR(n),
                    ADD_NODE_REF2(CDR(n),
                    ADD_NODE_REF2(CAAR(n),
                    ADD_NODE_REF2(CAAR(n),
                      tmp1 = mknode(F_COMMA_EXPR,
                  		mknode(F_LOOP,
                  		       mkefuncallnode("`+",
                  				      mknode(F_ARG_LIST,
                  					     mkefuncallnode("`-", CDAR(n)),
                  					     mknode(F_ARG_LIST,
                  						    mkintnode(-1),
                  						    CAAR(n)))), CDR(n)),
                  		mkcastnode(void_type_string, mknode(F_ASSIGN, CAAR(n), CDAR(n))));
                    )))));
                    goto use_tmp1;
                    }
                }
              }
            }
          }
        }
      }
    }
  }
  break;

case F_INDEX:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_INDEX{919}(-{920}, *)""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""F_COMMA_EXPR{922}""\n");
      }
#endif /* PIKE_DEBUG */
    {
      ADD_NODE_REF2(CDR(n),
        tmp1 = mknode(F_COMMA_EXPR, mknode(F_POP_VALUE, CDR(n), 0), mkintnode(0));
      );
      goto use_tmp1;
    }
  } else {
    if (( !match_types(CAR(n)->type, object_type_string) )) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == F_CONSTANT) {
          if ((CDR(n)->u.sval.type == T_STRING)) {
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "Match: ""F_INDEX{927}(0 = +{928}[ !match_types(CAR(n)->type, object_type_string) ], 1 = F_CONSTANT{929}[CDR(n)->u.sval.type == T_STRING])""\n");
              }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "=> ""F_ARROW{930}""\n");
              }
#endif /* PIKE_DEBUG */
            {
              ADD_NODE_REF2(CAR(n),
              ADD_NODE_REF2(CDR(n),
                tmp1 = mknode(F_ARROW, CAR(n), CDR(n));
              ));
              goto use_tmp1;
            }
          }
        }
      }
    }
  }
  break;

case F_LAND:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_LAND{640}(-{641}, 0 = *{642})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""-{643}""\n");
      }
#endif /* PIKE_DEBUG */
    goto zap_node;
  } else if (!CDR(n)) {
    if (!CAR(n)) {
    } else {
      if ((node_is_false(CAR(n)))) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_LAND{700}(0 = +{701}[node_is_false(CAR(n))], *)""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""0 = *{703}""\n");
          }
#endif /* PIKE_DEBUG */
        goto use_car;
      }
      if ((node_is_true(CAR(n)))) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_LAND{696}(+{697}[node_is_true(CAR(n))], 0 = *{698})""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""0 = *{699}""\n");
          }
#endif /* PIKE_DEBUG */
        goto use_cdr;
      }
    }
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_LAND{644}(0 = *{645}, -{646})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""F_COMMA_EXPR{647}""\n");
      }
#endif /* PIKE_DEBUG */
    {
      ADD_NODE_REF2(CAR(n),
        tmp1 = mknode(F_COMMA_EXPR, CAR(n), mkintnode(0));
      );
      goto use_tmp1;
    }
  } else {
    if (CAR(n)->token == F_APPLY) {
      if (!CAAR(n)) {
      } else {
        if (CAAR(n)->token == F_CONSTANT) {
          if ((CAAR(n)->u.sval.type == T_FUNCTION) &&
              (CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
              (CAAR(n)->u.sval.u.efun->function == f_not)) {
            if (!CDR(n)) {
            } else {
              if (CDR(n)->token == F_APPLY) {
                if (!CADR(n)) {
                } else {
                  if (CADR(n)->token == F_CONSTANT) {
                    if ((CADR(n)->u.sval.type == T_FUNCTION) &&
                        (CADR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                        (CADR(n)->u.sval.u.efun->function == f_not)) {
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "Match: ""F_LAND{650}(F_APPLY{651}(0 = F_CONSTANT{652}[CAAR(n)->u.sval.type == T_FUNCTION][CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAR(n)->u.sval.u.efun->function == f_not], 1 = *{653}), F_APPLY{654}(F_CONSTANT{655}[CADR(n)->u.sval.type == T_FUNCTION][CADR(n)->u.sval.subtype == FUNCTION_BUILTIN][CADR(n)->u.sval.u.efun->function == f_not], 2 = *{656}))""\n");
                        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "=> ""F_APPLY{657}""\n");
                        }
#endif /* PIKE_DEBUG */
                      {
                        ADD_NODE_REF2(CAAR(n),
                        ADD_NODE_REF2(CDAR(n),
                        ADD_NODE_REF2(CDDR(n),
                          tmp1 = mknode(F_APPLY, CAAR(n), mknode(F_LOR, CDAR(n), CDDR(n)));
                        )));
                        goto use_tmp1;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_ASSIGN) {
      if (!CDAR(n)) {
      } else {
        if ((node_is_false(CDAR(n)))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_LAND{680}(0 = F_ASSIGN{681}(*, +{683}[node_is_false(CDAR(n))]), *)""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{685}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
        if ((node_is_true(CDAR(n)))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_LAND{672}(0 = F_ASSIGN{673}(*, +{675}[node_is_true(CDAR(n))]), 2 = *{676})""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""F_COMMA_EXPR{677}""\n");
            }
#endif /* PIKE_DEBUG */
          {
            ADD_NODE_REF2(CAR(n),
            ADD_NODE_REF2(CDR(n),
              tmp1 = mknode(F_COMMA_EXPR, CAR(n), CDR(n));
            ));
            goto use_tmp1;
          }
        }
      }
    } else if (CAR(n)->token == F_COMMA_EXPR) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_LAND{686}(F_COMMA_EXPR{687}(0 = *{688}, 1 = *{689}), 2 = *{690})""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""F_COMMA_EXPR{691}""\n");
        }
#endif /* PIKE_DEBUG */
      {
        ADD_NODE_REF2(CAAR(n),
        ADD_NODE_REF2(CDAR(n),
        ADD_NODE_REF2(CDR(n),
          tmp1 = mknode(F_COMMA_EXPR, CAAR(n), mknode(F_LAND, CDAR(n), CDR(n)));
        )));
        goto use_tmp1;
      }
    } else if (CAR(n)->token == F_LAND) {
      if (!CDAR(n)) {
      } else {
        if ((node_is_false(CDAR(n)))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_LAND{704}(0 = F_LAND{705}(*, +{707}[node_is_false(CDAR(n))]), *)""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{709}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    }
    if (CDR(n)->token == F_LAND) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_LAND{662}(0 = *{663}, F_LAND{664}(1 = *{665}, 2 = *{666}))""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""F_LAND{667}""\n");
        }
#endif /* PIKE_DEBUG */
      {
        ADD_NODE_REF2(CAR(n),
        ADD_NODE_REF2(CADR(n),
        ADD_NODE_REF2(CDDR(n),
          tmp1 = mknode(F_LAND, mknode(F_LAND, CAR(n), CADR(n)), CDDR(n));
        )));
        goto use_tmp1;
      }
    }
    if ((node_is_false(CAR(n)))) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_LAND{700}(0 = +{701}[node_is_false(CAR(n))], *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{703}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    }
    if ((node_is_true(CAR(n)))) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_LAND{696}(+{697}[node_is_true(CAR(n))], 0 = *{698})""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{699}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_cdr;
    }
  }
  break;

case F_LOOP:
  if (!CAR(n)) {
  } else if (!CDR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_LOOP{963}(0 = *{964}, -{965})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""F_POP_VALUE{966}""\n");
      }
#endif /* PIKE_DEBUG */
    {
      ADD_NODE_REF2(CAR(n),
        tmp1 = mknode(F_POP_VALUE, CAR(n), 0);
      );
      goto use_tmp1;
    }
  } else {
    if (CDR(n)->token == F_POP_VALUE) {
      if (!CDDR(n)) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_LOOP{1299}(0 = *{1300}, F_POP_VALUE{1301}(1 = *{1302}, -{1303}))""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""F_LOOP{1304}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CAR(n),
          ADD_NODE_REF2(CADR(n),
            tmp1 = mknode(F_LOOP, CAR(n), CADR(n));
          ));
          goto use_tmp1;
        }
      }
    }
    if (( !depend_p(CAR(n), CAR(n)) )) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == F_DEC) {
          if (( pike_types_le(CDR(n)->type, int_type_string) )) {
            if (!CDDR(n)) {
              if (!CADR(n)) {
              } else {
                if (( !depend_p(CADR(n), CADR(n)) )) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_LOOP{1327}(0 = +{1328}[ !depend_p(CAR(n), CAR(n)) ], F_DEC{1329}[ pike_types_le(CDR(n)->type, int_type_string) ](1 = +{1330}[ !depend_p(CADR(n), CADR(n)) ], -{1331}))""\n");
                    }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "=> ""F_POP_VALUE{1332}""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                    ADD_NODE_REF2(CADR(n),
                    ADD_NODE_REF2(CAR(n),
                      tmp1 = mknode(F_POP_VALUE, mknode(F_SUB_EQ, CADR(n), CAR(n)), 0);
                    ));
                    goto use_tmp1;
                  }
                }
              }
            }
          }
        } else if (CDR(n)->token == F_INC) {
          if (( pike_types_le(CDR(n)->type, int_type_string) )) {
            if (!CDDR(n)) {
              if (!CADR(n)) {
              } else {
                if (( !depend_p(CADR(n), CADR(n)) )) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_LOOP{1307}(0 = +{1308}[ !depend_p(CAR(n), CAR(n)) ], F_INC{1309}[ pike_types_le(CDR(n)->type, int_type_string) ](1 = +{1310}[ !depend_p(CADR(n), CADR(n)) ], -{1311}))""\n");
                    }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "=> ""F_POP_VALUE{1312}""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                    ADD_NODE_REF2(CADR(n),
                    ADD_NODE_REF2(CAR(n),
                      tmp1 = mknode(F_POP_VALUE, mknode(F_ADD_EQ, CADR(n), CAR(n)), 0);
                    ));
                    goto use_tmp1;
                  }
                }
              }
            }
          }
        } else if (CDR(n)->token == F_POST_DEC) {
          if (( pike_types_le(CDR(n)->type, int_type_string) )) {
            if (!CDDR(n)) {
              if (!CADR(n)) {
              } else {
                if (( !depend_p(CADR(n), CADR(n)) )) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_LOOP{1337}(0 = +{1338}[ !depend_p(CAR(n), CAR(n)) ], F_POST_DEC{1339}[ pike_types_le(CDR(n)->type, int_type_string) ](1 = +{1340}[ !depend_p(CADR(n), CADR(n)) ], -{1341}))""\n");
                    }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "=> ""F_POP_VALUE{1342}""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                    ADD_NODE_REF2(CADR(n),
                    ADD_NODE_REF2(CAR(n),
                      tmp1 = mknode(F_POP_VALUE, mknode(F_SUB_EQ, CADR(n), CAR(n)), 0);
                    ));
                    goto use_tmp1;
                  }
                }
              }
            }
          }
        } else if (CDR(n)->token == F_POST_INC) {
          if (( pike_types_le(CDR(n)->type, int_type_string) )) {
            if (!CDDR(n)) {
              if (!CADR(n)) {
              } else {
                if (( !depend_p(CADR(n), CADR(n)) )) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_LOOP{1317}(0 = +{1318}[ !depend_p(CAR(n), CAR(n)) ], F_POST_INC{1319}[ pike_types_le(CDR(n)->type, int_type_string) ](1 = +{1320}[ !depend_p(CADR(n), CADR(n)) ], -{1321}))""\n");
                    }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "=> ""F_POP_VALUE{1322}""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                    ADD_NODE_REF2(CADR(n),
                    ADD_NODE_REF2(CAR(n),
                      tmp1 = mknode(F_POP_VALUE, mknode(F_ADD_EQ, CADR(n), CAR(n)), 0);
                    ));
                    goto use_tmp1;
                  }
                }
              }
            }
          }
        }
      }
    }
    if (( !depend_p(CAR(n), CAR(n))) &&
        ( !(CAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|
			    OPT_CASE|OPT_CONTINUE|
			    OPT_BREAK|OPT_RETURN)) )) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == F_ADD_EQ) {
          if (!CADR(n)) {
          } else {
            if (( !depend_p(CADR(n), CADR(n)) )) {
              if (!CDDR(n)) {
              } else {
                if (( !depend_p(CDDR(n), CADR(n)) ) &&
                    ( !depend_p(CDDR(n), CDDR(n)) ) &&
                    ( !(CDDR(n)->tree_info & OPT_SIDE_EFFECT) ) &&
                    ( (((CDDR(n)->type == int_type_string) &&
		    (CADR(n)->type == int_type_string)) ||
		   pike_types_le(CDDR(n)->type, string_type_string)) )) {
#ifdef PIKE_DEBUG
                    if (l_flag > 4) {
                      fprintf(stderr, "Match: ""F_LOOP{1347}(0 = +{1348}[ !depend_p(CAR(n), CAR(n))][ !(CAR(n)->tree_info & (OPT_SIDE_EFFECT|OPT_ASSIGNMENT|\n"
                  "\t\t\t    OPT_CASE|OPT_CONTINUE|\n"
                  "\t\t\t    OPT_BREAK|OPT_RETURN)) ], F_ADD_EQ{1349}(1 = +{1350}[ !depend_p(CADR(n), CADR(n)) ], 2 = +{1351}[ !depend_p(CDDR(n), CADR(n)) ][ !depend_p(CDDR(n), CDDR(n)) ][ !(CDDR(n)->tree_info & OPT_SIDE_EFFECT) ][ (((CDDR(n)->type == int_type_string) &&\n"
                  "\t\t    (CADR(n)->type == int_type_string)) ||\n"
                  "\t\t   pike_types_le(CDDR(n)->type, string_type_string)) ]))""\n");
                    }
#endif /* PIKE_DEBUG */
                  {
                      
                    ADD_NODE_REF2(CAR(n),
                    ADD_NODE_REF2(CAR(n),
                    ADD_NODE_REF2(CADR(n),
                    ADD_NODE_REF2(CDDR(n),
                      tmp1 = mknode(F_POP_VALUE,
                  		mknode('?',
                  		       mkefuncallnode("`>",
                  				      mknode(F_ARG_LIST,
                  					     CAR(n), mkintnode(0))),
                  		       mknode(':',
                  			      mknode(F_ADD_EQ,
                  				     CADR(n),
                  				     mkefuncallnode("`*",
                  						    mknode(F_ARG_LIST,
                  							   CDDR(n), CAR(n)))),
                  			      mkintnode(0))),
                  		NULL);
                    ))));
                    goto use_tmp1;
                    }
                }
              }
            }
          }
        }
      }
    }
  }
  break;

case F_LOR:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_LOR{572}(-{573}, 0 = *{574})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""0 = *{575}""\n");
      }
#endif /* PIKE_DEBUG */
    goto use_cdr;
  } else if (!CDR(n)) {
    if (!CAR(n)) {
    } else {
      if ((node_is_false(CAR(n)))) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_LOR{626}(+{627}[node_is_false(CAR(n))], 0 = *{628})""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""0 = *{629}""\n");
          }
#endif /* PIKE_DEBUG */
        goto use_cdr;
      }
      if ((node_is_true(CAR(n)))) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_LOR{630}(0 = +{631}[node_is_true(CAR(n))], *)""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""0 = *{633}""\n");
          }
#endif /* PIKE_DEBUG */
        goto use_car;
      }
    }
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_LOR{576}(0 = *{577}, -{578})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""0 = *{579}""\n");
      }
#endif /* PIKE_DEBUG */
    goto use_car;
  } else {
    if (CAR(n)->token == F_APPLY) {
      if (!CAAR(n)) {
      } else {
        if (CAAR(n)->token == F_CONSTANT) {
          if ((CAAR(n)->u.sval.type == T_FUNCTION) &&
              (CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
              (CAAR(n)->u.sval.u.efun->function == f_not)) {
            if (!CDR(n)) {
            } else {
              if (CDR(n)->token == F_APPLY) {
                if (!CADR(n)) {
                } else {
                  if (CADR(n)->token == F_CONSTANT) {
                    if ((CADR(n)->u.sval.type == T_FUNCTION) &&
                        (CADR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                        (CADR(n)->u.sval.u.efun->function == f_not)) {
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "Match: ""F_LOR{580}(F_APPLY{581}(0 = F_CONSTANT{582}[CAAR(n)->u.sval.type == T_FUNCTION][CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAR(n)->u.sval.u.efun->function == f_not], 1 = *{583}), F_APPLY{584}(F_CONSTANT{585}[CADR(n)->u.sval.type == T_FUNCTION][CADR(n)->u.sval.subtype == FUNCTION_BUILTIN][CADR(n)->u.sval.u.efun->function == f_not], 2 = *{586}))""\n");
                        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                        if (l_flag > 4) {
                          fprintf(stderr, "=> ""F_APPLY{587}""\n");
                        }
#endif /* PIKE_DEBUG */
                      {
                        ADD_NODE_REF2(CAAR(n),
                        ADD_NODE_REF2(CDAR(n),
                        ADD_NODE_REF2(CDDR(n),
                          tmp1 = mknode(F_APPLY, CAAR(n), mknode(F_LAND, CDAR(n), CDDR(n)));
                        )));
                        goto use_tmp1;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_ASSIGN) {
      if (!CDAR(n)) {
      } else {
        if ((node_is_false(CDAR(n)))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_LOR{602}(0 = F_ASSIGN{603}(*, +{605}[node_is_false(CDAR(n))]), 2 = *{606})""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""F_COMMA_EXPR{607}""\n");
            }
#endif /* PIKE_DEBUG */
          {
            ADD_NODE_REF2(CAR(n),
            ADD_NODE_REF2(CDR(n),
              tmp1 = mknode(F_COMMA_EXPR, CAR(n), CDR(n));
            ));
            goto use_tmp1;
          }
        }
        if ((node_is_true(CDAR(n)))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_LOR{620}(0 = F_ASSIGN{621}(*, +{623}[node_is_true(CDAR(n))]), *)""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{625}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    } else if (CAR(n)->token == F_COMMA_EXPR) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_LOR{610}(F_COMMA_EXPR{611}(0 = *{612}, 1 = *{613}), 2 = *{614})""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""F_COMMA_EXPR{615}""\n");
        }
#endif /* PIKE_DEBUG */
      {
        ADD_NODE_REF2(CAAR(n),
        ADD_NODE_REF2(CDAR(n),
        ADD_NODE_REF2(CDR(n),
          tmp1 = mknode(F_COMMA_EXPR, CAAR(n), mknode(F_LOR, CDAR(n), CDR(n)));
        )));
        goto use_tmp1;
      }
    } else if (CAR(n)->token == F_LOR) {
      if (!CDAR(n)) {
      } else {
        if ((node_is_true(CDAR(n)))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_LOR{634}(0 = F_LOR{635}(*, +{637}[node_is_true(CDAR(n))]), *)""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{639}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    }
    if (CDR(n)->token == F_LOR) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_LOR{592}(0 = *{593}, F_LOR{594}(1 = *{595}, 2 = *{596}))""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""F_LOR{597}""\n");
        }
#endif /* PIKE_DEBUG */
      {
        ADD_NODE_REF2(CAR(n),
        ADD_NODE_REF2(CADR(n),
        ADD_NODE_REF2(CDDR(n),
          tmp1 = mknode(F_LOR, mknode(F_LOR, CAR(n), CADR(n)), CDDR(n));
        )));
        goto use_tmp1;
      }
    }
    if ((node_is_false(CAR(n)))) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_LOR{626}(+{627}[node_is_false(CAR(n))], 0 = *{628})""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{629}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_cdr;
    }
    if ((node_is_true(CAR(n)))) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_LOR{630}(0 = +{631}[node_is_true(CAR(n))], *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{633}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    }
  }
  break;

case F_LT:
  if (!CAR(n)) {
  } else {
    if (CAR(n)->token == F_APPLY) {
      if (!CAAR(n)) {
      } else {
        if (CAAR(n)->token == F_CONSTANT) {
          if ((CAAR(n)->u.sval.type == T_FUNCTION) &&
              (CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
              (CAAR(n)->u.sval.u.efun->function == f_search)) {
            if (!CDAR(n)) {
            } else {
              if (CDAR(n)->token == F_ARG_LIST) {
                if (!CADAR(n)) {
                } else {
                  if (CADAR(n)->token == F_APPLY) {
                    if (!CAADAR(n)) {
                    } else {
                      if (CAADAR(n)->token == F_CONSTANT) {
                        if ((CAADAR(n)->u.sval.type == T_FUNCTION) &&
                            (CAADAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
                            (CAADAR(n)->u.sval.u.efun->function == f_indices)) {
                          if (!CDADAR(n)) {
                          } else {
                            if ((pike_types_le(CDADAR(n)->type, mapping_type_string))) {
                              if (!CDR(n)) {
                              } else {
                                if (CDR(n)->token == F_CONSTANT) {
                                  if ((CDR(n)->u.sval.type == T_INT) &&
                                      (!CDR(n)->u.sval.u.integer)) {
#ifdef PIKE_DEBUG
                                      if (l_flag > 4) {
                                        fprintf(stderr, "Match: ""F_LT{63}(F_APPLY{64}(F_CONSTANT{65}[CAAR(n)->u.sval.type == T_FUNCTION][CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAR(n)->u.sval.u.efun->function == f_search], F_ARG_LIST{66}(F_APPLY{67}(F_CONSTANT{68}[CAADAR(n)->u.sval.type == T_FUNCTION][CAADAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAADAR(n)->u.sval.u.efun->function == f_indices], 0 = +{69}[pike_types_le(CDADAR(n)->type, mapping_type_string)]), 1 = *{70})), F_CONSTANT{71}[CDR(n)->u.sval.type == T_INT][!CDR(n)->u.sval.u.integer])""\n");
                                      }
#endif /* PIKE_DEBUG */
                                    {
                                      
                                      ADD_NODE_REF2(CDDAR(n),
                                      ADD_NODE_REF2(CDADAR(n),
                                        tmp1 = mkefuncallnode("zero_type",mknode(F_ARG_LIST, CDADAR(n), CDDAR(n)));
                                      ));
                                      goto use_tmp1;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
#ifdef PIKE_DEBUG
    if (l_flag > 4) {
      fprintf(stderr, "Match: ""F_LT{57}(0 = *{58}, 1 = *{59})""\n");
    }
#endif /* PIKE_DEBUG */
  {
      
    ADD_NODE_REF2(CDR(n),
    ADD_NODE_REF2(CAR(n),
      tmp1 = mkopernode("`<", CAR(n), CDR(n));
    ));
    goto use_tmp1;
    }
  break;

case F_LVALUE_LIST:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_LVALUE_LIST{534}(-{535}, 0 = *{536})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""0 = *{537}""\n");
      }
#endif /* PIKE_DEBUG */
    goto use_cdr;
  } else if (!CDR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_LVALUE_LIST{538}(0 = *{539}, -{540})""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""0 = *{541}""\n");
      }
#endif /* PIKE_DEBUG */
    goto use_car;
  } else {
    if (CAR(n)->token == F_BREAK) {
      if (!CDR(n)) {
      } else {
        if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_LVALUE_LIST{550}(0 = F_BREAK{551}, +{552}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{553}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    } else if (CAR(n)->token == F_COMMA_EXPR) {
      if (!CDAR(n)) {
      } else {
        if (CDAR(n)->token == F_BREAK) {
          if (!CDR(n)) {
          } else {
            if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_LVALUE_LIST{566}(0 = F_COMMA_EXPR{567}(*, F_BREAK{569}), +{570}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""0 = *{571}""\n");
                }
#endif /* PIKE_DEBUG */
              goto use_car;
            }
          }
        } else if (CDAR(n)->token == F_CONTINUE) {
          if (!CDR(n)) {
          } else {
            if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_LVALUE_LIST{560}(0 = F_COMMA_EXPR{561}(*, F_CONTINUE{563}), +{564}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""0 = *{565}""\n");
                }
#endif /* PIKE_DEBUG */
              goto use_car;
            }
          }
        } else if (CDAR(n)->token == F_RETURN) {
          if (!CDR(n)) {
          } else {
            if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_LVALUE_LIST{554}(0 = F_COMMA_EXPR{555}(*, F_RETURN{557}), +{558}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""0 = *{559}""\n");
                }
#endif /* PIKE_DEBUG */
              goto use_car;
            }
          }
        }
      }
    } else if (CAR(n)->token == F_CONTINUE) {
      if (!CDR(n)) {
      } else {
        if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_LVALUE_LIST{546}(0 = F_CONTINUE{547}, +{548}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{549}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    } else if (CAR(n)->token == F_RETURN) {
      if (!CDR(n)) {
      } else {
        if ((!(CDR(n)->tree_info & OPT_CASE))) {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_LVALUE_LIST{542}(0 = F_RETURN{543}, +{544}[!(CDR(n)->tree_info & OPT_CASE)])""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""0 = *{545}""\n");
            }
#endif /* PIKE_DEBUG */
          goto use_car;
        }
      }
    }
  }
  break;

case F_NOT:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_NOT{827}(-{828}, *)""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""1 = T_INT{830}""\n");
      }
#endif /* PIKE_DEBUG */
    {
      tmp1 = mkintnode(1);
      goto use_tmp1;
    }
  } else {
    if (CAR(n)->token == F_EQ) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_NOT{863}(F_EQ{864}(0 = *{865}, 1 = *{866}), *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""F_NE{868}""\n");
        }
#endif /* PIKE_DEBUG */
      {
        ADD_NODE_REF2(CAAR(n),
        ADD_NODE_REF2(CDAR(n),
          tmp1 = mknode(F_NE, CAAR(n), CDAR(n));
        ));
        goto use_tmp1;
      }
    } else if (CAR(n)->token == F_GE) {
      if (!CAAR(n)) {
      } else {
        if ((pike_types_le(CAAR(n)->type, int_type_string))) {
          if (!CDAR(n)) {
          } else {
            if ((pike_types_le(CDAR(n)->type, int_type_string))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_NOT{855}(F_GE{856}(0 = +{857}[pike_types_le(CAAR(n)->type, int_type_string)], 1 = +{858}[pike_types_le(CDAR(n)->type, int_type_string)]), *)""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""F_LT{860}""\n");
                }
#endif /* PIKE_DEBUG */
              {
                ADD_NODE_REF2(CAAR(n),
                ADD_NODE_REF2(CDAR(n),
                  tmp1 = mknode(F_LT, CAAR(n), CDAR(n));
                ));
                goto use_tmp1;
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_GT) {
      if (!CAAR(n)) {
      } else {
        if ((pike_types_le(CAAR(n)->type, int_type_string))) {
          if (!CDAR(n)) {
          } else {
            if ((pike_types_le(CDAR(n)->type, int_type_string))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_NOT{839}(F_GT{840}(0 = +{841}[pike_types_le(CAAR(n)->type, int_type_string)], 1 = +{842}[pike_types_le(CDAR(n)->type, int_type_string)]), *)""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""F_LE{844}""\n");
                }
#endif /* PIKE_DEBUG */
              {
                ADD_NODE_REF2(CAAR(n),
                ADD_NODE_REF2(CDAR(n),
                  tmp1 = mknode(F_LE, CAAR(n), CDAR(n));
                ));
                goto use_tmp1;
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_LE) {
      if (!CAAR(n)) {
      } else {
        if ((pike_types_le(CAAR(n)->type, int_type_string))) {
          if (!CDAR(n)) {
          } else {
            if ((pike_types_le(CDAR(n)->type, int_type_string))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_NOT{847}(F_LE{848}(0 = +{849}[pike_types_le(CAAR(n)->type, int_type_string)], 1 = +{850}[pike_types_le(CDAR(n)->type, int_type_string)]), *)""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""F_GT{852}""\n");
                }
#endif /* PIKE_DEBUG */
              {
                ADD_NODE_REF2(CAAR(n),
                ADD_NODE_REF2(CDAR(n),
                  tmp1 = mknode(F_GT, CAAR(n), CDAR(n));
                ));
                goto use_tmp1;
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_LT) {
      if (!CAAR(n)) {
      } else {
        if ((pike_types_le(CAAR(n)->type, int_type_string))) {
          if (!CDAR(n)) {
          } else {
            if ((pike_types_le(CDAR(n)->type, int_type_string))) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_NOT{831}(F_LT{832}(0 = +{833}[pike_types_le(CAAR(n)->type, int_type_string)], 1 = +{834}[pike_types_le(CDAR(n)->type, int_type_string)]), *)""\n");
                }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "=> ""F_GE{836}""\n");
                }
#endif /* PIKE_DEBUG */
              {
                ADD_NODE_REF2(CAAR(n),
                ADD_NODE_REF2(CDAR(n),
                  tmp1 = mknode(F_GE, CAAR(n), CDAR(n));
                ));
                goto use_tmp1;
              }
            }
          }
        }
      }
    } else if (CAR(n)->token == F_NE) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_NOT{871}(F_NE{872}(0 = *{873}, 1 = *{874}), *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""F_EQ{876}""\n");
        }
#endif /* PIKE_DEBUG */
      {
        ADD_NODE_REF2(CAAR(n),
        ADD_NODE_REF2(CDAR(n),
          tmp1 = mknode(F_EQ, CAAR(n), CDAR(n));
        ));
        goto use_tmp1;
      }
    }
  }
  break;

case F_POP_VALUE:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_POP_VALUE{135}(-{136}, *)""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""-{138}""\n");
      }
#endif /* PIKE_DEBUG */
    goto zap_node;
  } else {
    if (CAR(n)->token == '?') {
      if (!CDAR(n)) {
      } else {
        if (CDAR(n)->token == ':') {
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "Match: ""F_POP_VALUE{223}('?'{224}(0 = *{225}, ':'{226}(1 = *{227}, 2 = *{228})), *)""\n");
            }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr,
                      "Setting line position to %s:%d\n",
                      CAR(n)->current_file->str,
                      CAR(n)->line_number);
            }
#endif /* PIKE_DEBUG */
          c->lex.current_line = CAR(n)->line_number;
          c->lex.current_file = CAR(n)->current_file;
#ifdef PIKE_DEBUG
            if (l_flag > 4) {
              fprintf(stderr, "=> ""'?'{230}""\n");
            }
#endif /* PIKE_DEBUG */
          {
            ADD_NODE_REF2(CAAR(n),
            ADD_NODE_REF2(CADAR(n),
            ADD_NODE_REF2(CDDAR(n),
              tmp1 = mknode('?', CAAR(n), mknode(':', mknode(F_POP_VALUE, CADAR(n), 0), mknode(F_POP_VALUE, CDDAR(n), 0)));
            )));
            goto use_tmp1;
          }
        }
      }
    } else if (CAR(n)->token == F_BREAK) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{175}(0 = F_BREAK{176}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{178}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_CASE) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{179}(0 = F_CASE{180}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{182}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_CASE_RANGE) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{183}(0 = F_CASE_RANGE{184}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{186}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_CONSTANT) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{143}(F_CONSTANT{144}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""-{146}""\n");
        }
#endif /* PIKE_DEBUG */
      goto zap_node;
    } else if (CAR(n)->token == F_CONTINUE) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{171}(0 = F_CONTINUE{172}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{174}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_DEC_LOOP) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{203}(0 = F_DEC_LOOP{204}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{206}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_DEC_NEQ_LOOP) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{211}(0 = F_DEC_NEQ_LOOP{212}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{214}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_DEFAULT) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{187}(0 = F_DEFAULT{188}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{190}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_EXTERNAL) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{159}(F_EXTERNAL{160}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""-{162}""\n");
        }
#endif /* PIKE_DEBUG */
      goto zap_node;
    } else if (CAR(n)->token == F_FOR) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{191}(0 = F_FOR{192}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{194}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_FOREACH) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{195}(0 = F_FOREACH{196}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{198}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_GET_SET) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{163}(F_GET_SET{164}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""-{166}""\n");
        }
#endif /* PIKE_DEBUG */
      goto zap_node;
    } else if (CAR(n)->token == F_GLOBAL) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{155}(F_GLOBAL{156}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""-{158}""\n");
        }
#endif /* PIKE_DEBUG */
      goto zap_node;
    } else if (CAR(n)->token == F_INC_LOOP) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{199}(0 = F_INC_LOOP{200}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{202}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_INC_NEQ_LOOP) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{207}(0 = F_INC_NEQ_LOOP{208}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{210}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_LOCAL) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{151}(F_LOCAL{152}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""-{154}""\n");
        }
#endif /* PIKE_DEBUG */
      goto zap_node;
    } else if (CAR(n)->token == F_LOOP) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{215}(0 = F_LOOP{216}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{218}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_POP_VALUE) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{139}(0 = F_POP_VALUE{140}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{142}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_RETURN) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{167}(0 = F_RETURN{168}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{170}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    } else if (CAR(n)->token == F_SWITCH) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{219}(0 = F_SWITCH{220}, *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""0 = *{222}""\n");
        }
#endif /* PIKE_DEBUG */
      goto use_car;
    }
    if ((node_is_tossable(CAR(n)))) {
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "Match: ""F_POP_VALUE{147}(+{148}[node_is_tossable(CAR(n))], *)""\n");
        }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
        if (l_flag > 4) {
          fprintf(stderr, "=> ""-{150}""\n");
        }
#endif /* PIKE_DEBUG */
      goto zap_node;
    }
  }
  break;

case F_PUSH_ARRAY:
  if (!CAR(n)) {
  } else {
    if (CAR(n)->token == F_APPLY) {
      if (!CAAR(n)) {
      } else {
        if (CAAR(n)->token == F_CONSTANT) {
          if ((CAAR(n)->u.sval.type == T_FUNCTION) &&
              (CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN) &&
              (CAAR(n)->u.sval.u.efun->function == debug_f_aggregate)) {
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "Match: ""F_PUSH_ARRAY{20}(F_APPLY{21}(F_CONSTANT{22}[CAAR(n)->u.sval.type == T_FUNCTION][CAAR(n)->u.sval.subtype == FUNCTION_BUILTIN][CAAR(n)->u.sval.u.efun->function == debug_f_aggregate], 0 = *{23}), *)""\n");
              }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "=> ""0 = *{25}""\n");
              }
#endif /* PIKE_DEBUG */
            {
              ADD_NODE_REF2(CDAR(n),
                tmp1 = CDAR(n);
              );
              goto use_tmp1;
            }
          }
        }
      }
    } else if (CAR(n)->token == F_CONSTANT) {
      if ((CAR(n)->u.sval.type == T_ARRAY)) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_PUSH_ARRAY{26}(0 = F_CONSTANT{27}[CAR(n)->u.sval.type == T_ARRAY], *)""\n");
          }
#endif /* PIKE_DEBUG */
        {
            struct array *a = CAR(n)->u.sval.u.array;
            node *res = NULL;
            int i;
            for(i=0; i<a->size; i++) {
              if (res) {
        	res = mknode(F_ARG_LIST, res, mksvaluenode(a->item+i));
              } else {
        	/* i is always 0 here. */
        	res = mksvaluenode(a->item);
              }
            }
            
          tmp1 = res;
          goto use_tmp1;
          }
      }
    }
  }
  break;

case F_RANGE:
  if (!CDR(n)) {
  } else {
    if (CDR(n)->token == ':') {
      if (!CADR(n)) {
      } else if (!CDDR(n)) {
      } else {
        if (CADR(n)->token == F_RANGE_FROM_BEG) {
          if (!CAADR(n)) {
          } else {
            if (CAADR(n)->token == F_CONSTANT) {
              if ((CAADR(n)->u.sval.type == T_INT)) {
                if (!CDDR(n)) {
                } else {
                  if (CDDR(n)->token == F_RANGE_FROM_BEG) {
                    if (!CADDR(n)) {
                    } else {
                      if (CADDR(n)->token == F_CONSTANT) {
                        if ((CADDR(n)->u.sval.type == T_INT) &&
                            (CADDR(n)->u.sval.u.integer < CAADR(n)->u.sval.u.integer)) {
#ifdef PIKE_DEBUG
                            if (l_flag > 4) {
                              fprintf(stderr, "Match: ""F_RANGE{277}(0 = *{278}, ':'{279}(F_RANGE_FROM_BEG{280}(1 = F_CONSTANT{281}[CAADR(n)->u.sval.type == T_INT], *), F_RANGE_FROM_BEG{283}(F_CONSTANT{284}[CADDR(n)->u.sval.type == T_INT][CADDR(n)->u.sval.u.integer < CAADR(n)->u.sval.u.integer], *)))""\n");
                            }
#endif /* PIKE_DEBUG */
                          {
                            /* Objects may want to use the range for obscure purposes. */
                            if (!match_types(object_type_string, CAR(n)->type)) {
                              yywarning("Range is always empty.");
                              if (pike_types_le(CAR(n)->type, string_type_string)) {
                                
                            tmp1 = mkstrnode(empty_pike_string);
                            goto use_tmp1;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        } else if (CADR(n)->token == F_RANGE_OPEN) {
          if (!CDDR(n)) {
          } else {
            if (CDDR(n)->token == F_RANGE_OPEN) {
#ifdef PIKE_DEBUG
                if (l_flag > 4) {
                  fprintf(stderr, "Match: ""F_RANGE{272}(0 = *{273}, ':'{274}(F_RANGE_OPEN{275}, F_RANGE_OPEN{276}))""\n");
                }
#endif /* PIKE_DEBUG */
              {
                /* Objects may want to use the range for obscure purposes. */
                if (!match_types(object_type_string, CAR(n)->type)) {
                  yywarning("Redundant range operator.");
                  
                ADD_NODE_REF2(CAR(n),
                  tmp1 = CAR(n);
                );
                goto use_tmp1;
                }
              }
            }
          }
        }
        if (CDDR(n)->token == F_RANGE_FROM_BEG) {
          if (!CADDR(n)) {
          } else {
            if (CADDR(n)->token == F_CONSTANT) {
              if ((CADDR(n)->u.sval.type == T_FLOAT) &&
                  (CADDR(n)->u.sval.u.float_number < 0.0)) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_RANGE{265}(*, ':'{267}(*, F_RANGE_FROM_BEG{269}(F_CONSTANT{270}[CADDR(n)->u.sval.type == T_FLOAT][CADDR(n)->u.sval.u.float_number < 0.0], *)))""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  yywarning("Range end is negative.");
                }
              }
              if ((CADDR(n)->u.sval.type == T_INT) &&
                  (CADDR(n)->u.sval.u.integer < 0)) {
#ifdef PIKE_DEBUG
                  if (l_flag > 4) {
                    fprintf(stderr, "Match: ""F_RANGE{258}(*, ':'{260}(*, F_RANGE_FROM_BEG{262}(F_CONSTANT{263}[CADDR(n)->u.sval.type == T_INT][CADDR(n)->u.sval.u.integer < 0], *)))""\n");
                  }
#endif /* PIKE_DEBUG */
                {
                  yywarning("Range end is negative.");
                }
              }
            }
          }
        }
      }
    }
  }
  break;

case F_RETURN:
  if (!CAR(n)) {
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "Match: ""F_RETURN{14}(-{15}, *)""\n");
      }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
      if (l_flag > 4) {
        fprintf(stderr, "=> ""F_RETURN{17}""\n");
      }
#endif /* PIKE_DEBUG */
    {
      tmp1 = mknode(F_RETURN, mkintnode(0), 0);
      goto use_tmp1;
    }
  }
  break;

case F_SSCANF:
  if (!CAR(n)) {
  } else {
    if (CAR(n)->token == ':') {
      if (!CAAR(n)) {
      } else {
        if (!CDAR(n)) {
        } else {
          if (CDAR(n)->token == F_ARG_LIST) {
            if (!CADAR(n)) {
            } else {
              if (CADAR(n)->token == F_RANGE) {
                if (!CAADAR(n)) {
                } else {
                  if (( pike_types_le(CAADAR(n)->type,
						     string_type_string))) {
                    if (!CDADAR(n)) {
                    } else {
                      if (CDADAR(n)->token == ':') {
                        if (!CADADAR(n)) {
                        } else {
                          if (CADADAR(n)->token == F_RANGE_FROM_BEG) {
                            if (!CDADADAR(n)) {
                              if (!CAADADAR(n)) {
                              } else {
                                if (( pike_types_le(CAADADAR(n)->type,
									  int_type_string))) {
                                  if (!CDDADAR(n)) {
                                  } else {
                                    if (CDDADAR(n)->token == F_RANGE_OPEN) {
                                      if (!CDDAR(n)) {
                                      } else {
#ifdef PIKE_DEBUG
                                          if (l_flag > 4) {
                                            fprintf(stderr, "Match: ""F_SSCANF{99}(':'{100}(4 = +{101}, F_ARG_LIST{102}(F_RANGE{103}(0 = +{104}[ pike_types_le(CAADAR(n)->type,\n"
                                        "\t\t\t\t\t\t     string_type_string)], ':'{105}(F_RANGE_FROM_BEG{106}(1 = +{107}[ pike_types_le(CAADADAR(n)->type,\n"
                                        "\t\t\t\t\t\t\t\t\t  int_type_string)], -{108}), F_RANGE_OPEN{109})), 2 = +{110})), 3 = *{111})""\n");
                                          }
#endif /* PIKE_DEBUG */
                                        {
                                          struct pike_string *percent_star_string;
                                          struct pike_string *s_string;
                                          MAKE_CONST_STRING(percent_star_string, "%*!");
                                          MAKE_CONST_STRING(s_string, "s");
                                          
                                          ADD_NODE_REF2(CDR(n),
                                          ADD_NODE_REF2(CDDAR(n),
                                          ADD_NODE_REF2(CAADAR(n),
                                          ADD_NODE_REF2(CAAR(n),
                                          ADD_NODE_REF2(CAADADAR(n),
                                            tmp1 = mkopernode("`-",
                                        		  mknode(F_LOR,
                                        			 mknode(F_SSCANF,
                                        				mknode(':', CAAR(n),
                                        				mknode(F_ARG_LIST, CAADAR(n),
                                        				       mkopernode("`+",
                                        						  mknode(F_ARG_LIST,
                                        							 mkstrnode(percent_star_string),
                                        							 CAADADAR(n)),
                                        						  mknode(F_ARG_LIST,
                                        							 mkstrnode(s_string),
                                        							 CDDAR(n))))),
                                        				CDR(n)),
                                        			 mkintnode(1)),
                                        		  mkintnode(1));
                                          )))));
                                          goto use_tmp1;
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  break;

case F_SUB_EQ:
  if (!CAR(n)) {
  } else if (!CDR(n)) {
  } else {
    if (CDR(n)->token == F_CONSTANT) {
      if ((CDR(n)->u.sval.type == T_INT) &&
          ((CDR(n)->u.sval.u.integer) == -1)) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_SUB_EQ{905}(0 = *{906}, 1 = F_CONSTANT{907}[CDR(n)->u.sval.type == T_INT][(CDR(n)->u.sval.u.integer) == -1])""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""F_INC{908}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CAR(n),
            tmp1 = mknode(F_INC, CAR(n), 0);
          );
          goto use_tmp1;
        }
      }
      if ((CDR(n)->u.sval.type == T_INT) &&
          ((CDR(n)->u.sval.u.integer) == 1)) {
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "Match: ""F_SUB_EQ{899}(0 = *{900}, 1 = F_CONSTANT{901}[CDR(n)->u.sval.type == T_INT][(CDR(n)->u.sval.u.integer) == 1])""\n");
          }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
          if (l_flag > 4) {
            fprintf(stderr, "=> ""F_DEC{902}""\n");
          }
#endif /* PIKE_DEBUG */
        {
          ADD_NODE_REF2(CAR(n),
            tmp1 = mknode(F_DEC, CAR(n), 0);
          );
          goto use_tmp1;
        }
      }
    }
    if (( CAR(n)->token != F_AUTO_MAP_MARKER )) {
      if (!CDR(n)) {
      } else {
        if (CDR(n)->token == F_CONSTANT) {
          if ((CDR(n)->u.sval.type == T_INT) &&
              (!(CDR(n)->u.sval.u.integer))) {
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "Match: ""F_SUB_EQ{895}(0 = +{896}[ CAR(n)->token != F_AUTO_MAP_MARKER ], 1 = F_CONSTANT{897}[CDR(n)->u.sval.type == T_INT][!(CDR(n)->u.sval.u.integer)])""\n");
              }
#endif /* PIKE_DEBUG */
#ifdef PIKE_DEBUG
              if (l_flag > 4) {
                fprintf(stderr, "=> ""0 = *{898}""\n");
              }
#endif /* PIKE_DEBUG */
            goto use_car;
          }
        }
      }
    }
  }
  break;

