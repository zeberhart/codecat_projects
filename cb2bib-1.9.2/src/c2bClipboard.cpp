/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#include "c2bClipboard.h"
#ifdef C2B_USE_CBPOLL
#include "clipboardPoll.h"
#endif

#include <QApplication>


c2bClipboard::c2bClipboard(QObject* parento) : QObject(parento)
{
    // Connecting clipboard
    _cb = QApplication::clipboard();
    _supports_selection = _cb->supportsSelection();
    _is_connected = false;
    _cb_text = _cb->text(QClipboard::Clipboard);
    if (_supports_selection)
        _cbs_text = _cb->text(QClipboard::Selection);

    // Setting timer
    _interval = 1000;
#ifdef C2B_USE_CBPOLL
    _poll = new clipboardPoll();
    connect(_poll, SIGNAL(clipboardChanged(bool)), this, SLOT(newClipboardData(bool)));
#else
#ifdef Q_OS_UNIX
    _timer = new QTimer(this);
    connect(_timer, SIGNAL(timeout()), this, SLOT(checkData()));
#endif
#endif
}

c2bClipboard::~c2bClipboard()
{
#ifdef C2B_USE_CBPOLL
    delete _poll;
#endif
}


void c2bClipboard::checkData()
{
    if (_supports_selection)
    {
        if (_cb_text != _cb->text(QClipboard::Clipboard))
        {
            _cb_text = _cb->text(QClipboard::Clipboard);
            _cbs_text = _cb->text(QClipboard::Selection);
            emit cbDataChanged(_cb_text);
        }
        else if (_cbs_text != _cb->text(QClipboard::Selection))
        {
            _cbs_text = _cb->text(QClipboard::Selection);
            emit cbDataChanged(_cbs_text);
        }
    }
    else
    {
        if (_cb_text != _cb->text(QClipboard::Clipboard))
        {
            _cb_text == _cb->text(QClipboard::Clipboard);
            emit cbDataChanged(_cb_text);
        }
    }
}

void c2bClipboard::dataChanged()
{
    _cb_text = _cb->text(QClipboard::Clipboard);
    emit cbDataChanged(_cb_text);
}

void c2bClipboard::selectionChanged()
{
    _cbs_text = _cb->text(QClipboard::Selection);
    emit cbDataChanged(_cbs_text);
}

void c2bClipboard::setConnected(bool zconn)
{
    _is_connected = zconn;
    if (_is_connected)
    {
        _cb_text = _cb->text(QClipboard::Clipboard);
        connect(_cb, SIGNAL(dataChanged()), this, SLOT(dataChanged()));
        if (_supports_selection)
        {
            _cbs_text = _cb->text(QClipboard::Selection);
            connect(_cb, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));
        }
#ifdef C2B_USE_CBPOLL
        _poll->startT(_interval);
#else
#ifdef Q_OS_UNIX
        _timer->start(_interval);
#endif
#endif
    }
    else
    {
#ifdef C2B_USE_CBPOLL
        _poll->stopT();
#else
#ifdef Q_OS_UNIX
        _timer->stop();
#endif
#endif
        disconnect(_cb, SIGNAL(dataChanged()), this, SLOT(dataChanged()));
        if (_supports_selection)
            disconnect(_cb, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));
    }
}

const QString c2bClipboard::text()
{
    return _cb->text(QClipboard::Clipboard);
}

void c2bClipboard::newClipboardData(bool selectionMode)
{
    if (selectionMode)
        checkSelectionData();
    else
        checkClipboardData();
}

void c2bClipboard::checkSelectionData()
{
    if (_supports_selection)
        if (_cbs_text != _cb->text(QClipboard::Selection))
        {
            _cbs_text = _cb->text(QClipboard::Selection);
            emit cbDataChanged(_cbs_text);
        }
}

void c2bClipboard::checkClipboardData()
{
    if (_cb_text != _cb->text(QClipboard::Clipboard))
    {
        _cb_text = _cb->text(QClipboard::Clipboard);
        emit cbDataChanged(_cb_text);
    }
}
