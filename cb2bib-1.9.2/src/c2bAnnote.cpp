/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#include "c2bAnnote.h"

#include "c2b.h"
#include "c2bSettings.h"
#include "c2bUtils.h"
#include "c2bWebBrowser.h"

#include <texToHtml.h>

#include <QFileSystemWatcher>
#include <QLabel>
#include <QMessageBox>
#include <QShortcut>
#include <QStatusBar>


c2bAnnote::c2bAnnote(QWidget* parentw) : QMainWindow(parentw)
{
    _view_port = new c2bWebBrowser(this);
    setCentralWidget(_view_port);
    QLabel* informationLabel = new QLabel(this);
    statusBar()->addWidget(informationLabel, 1);
    connect(_view_port, SIGNAL(titleChanged(const QString&)), this, SLOT(setWindowTitle(const QString&)));
    connect(_view_port, SIGNAL(statusMessage(const QString&)), this, SLOT(showMessage(const QString&)));

    // Creating cb2Bib global resources
    c2b* c2b_resources = new c2b(this, this);
    connect(c2b_resources, SIGNAL(statusMessage(const QString&)), this, SLOT(showMessage(const QString&)));

    QAction* act = new QAction(QIcon(QString::fromUtf8(":/icons/icons/edit.png")), tr("Edit Note"), this);
    act->setShortcut(QKeySequence(Qt::Key_E));
    act->setStatusTip(tr("Edit TeX note"));
    c2bUtils::addSeparator(_view_port);
    _view_port->addAction(act);
    connect(act, SIGNAL(triggered()), this, SLOT(edit()));

    act = new QAction(QIcon(QString::fromUtf8(":/icons/icons/configure.png")), tr("Configure"), this);
    act->setShortcut(QKeySequence(Qt::Key_C));
    act->setStatusTip(tr("Configure cb2Bib Annote"));
    c2bUtils::addSeparator(_view_port);
    _view_port->addAction(act);
    connect(act, SIGNAL(triggered()), this, SLOT(configure()));

    _settingsP = c2bSettingsP;
    resize(_settingsP->value("c2bAnnote/size", size()).toSize());
    move(_settingsP->value("c2bAnnote/position", pos()).toPoint());
    loadSettings();
    connect(_settingsP, SIGNAL(newSettings()), this, SLOT(loadSettings()));

    _t2h = new texToHtml;
    _fsw = new QFileSystemWatcher(this);
    connect(_fsw, SIGNAL(fileChanged(const QString&)), this, SLOT(annote()));
    disconnect(_view_port->viewReloadAction, 0, 0, 0);
    connect(_view_port->viewReloadAction, SIGNAL(triggered()), this, SLOT(update()));

    QShortcut* help_shortcut = new QShortcut(QKeySequence(QKeySequence::HelpContents), this);
    connect(help_shortcut, SIGNAL(activated()), this, SLOT(help()));
}

c2bAnnote::~c2bAnnote()
{
    delete _t2h;
    _settingsP->setValue("c2bAnnote/position", pos());
    _settingsP->setValue("c2bAnnote/size", size());
}


bool c2bAnnote::show()
{
    _annote_filename = QDir::cleanPath(_settingsP->cl_annote_filename);
    const bool is_html = _annote_filename.endsWith(".html");
    if (is_html)
        _annote_filename.remove(QRegExp("\\.html$"));
    if (!QFileInfo(_annote_filename).exists())
    {
        QMessageBox::warning(this, tr("Warning - cb2Bib"),
                             tr("Unable to open the file %1 for reading.\nError: '%2'.").
                             arg(QDir::toNativeSeparators(_annote_filename)).arg("File does not exist"), QMessageBox::Ok);
        return false;
    }
    _html_filename = _annote_filename + ".html";
    if (!is_html || !QFileInfo(_html_filename).exists())
        _t2h->toHtml(c2bUtils::fileToString(_annote_filename), _html_filename);
    _view_port->setHomePage(_html_filename);
    _fsw->addPath(_annote_filename);
    QMainWindow::show();
    return true;
}

void c2bAnnote::annote()
{
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    _t2h->toHtml(c2bUtils::fileToString(_annote_filename), _html_filename);
    if (_html_filename == QDir::cleanPath(_view_port->url().toLocalFile()))
        _view_port->reload();
    QApplication::restoreOverrideCursor();
}

void c2bAnnote::update()
{
    if (_html_filename == QDir::cleanPath(_view_port->url().toLocalFile()))
        annote();
    else
        _view_port->reload();
}

void c2bAnnote::edit()
{
    c2bUtils::openFile(_annote_filename, this);
}

void c2bAnnote::configure()
{
    // Attention: Currently c2bAnnote is page 0. Update here if c2bConfigure changes page ordering.
    c2b::configure(0);
}

void c2bAnnote::setWindowTitle(const QString& title)
{
    if (title.isEmpty())
        QMainWindow::setWindowTitle("Annote - cb2Bib");
    else
        QMainWindow::setWindowTitle(title + " - cb2Bib");
    if (title == "New Note")
        showMessage(tr("Press E to Edit and %1 for Help.").arg(QKeySequence(QKeySequence::HelpContents).toString()));
}

void c2bAnnote::showMessage(const QString& ms)
{
    statusBar()->showMessage(ms, C2B_MESSAGE_TIME);
}

void c2bAnnote::loadSettings()
{
    _view_port->setFont(_settingsP->value("c2bAnnote/Font").value<QFont>());
    _view_port->setFixedFont(_settingsP->value("c2bAnnote/FixedFont").value<QFont>());
}

void c2bAnnote::help()
{
    c2bUtils::displayHelp("http://www.molspaces.com/cb2bib/annote.tex.html");
}
