/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: 9c352ee2d551bf4c56f050d50e1a432afb511758 $
*/

/*
 * This file is provided for CVS's sake, and to define a simple string to be
 * used as the module's release version
 */

#define MSQLMOD_VERSION "mSQL/1.1.1"
