#ifndef __RESOURCE_network_H__
#define __RESOURCE_network_H__

#include <gio/gio.h>

extern GResource *network_get_resource (void);
#endif
