/*	@(#)imap.h 1.2 02/03/11 falk	*/
/*	$Id: imap.h,v 1.1.1.1 2002/03/12 05:38:52 efalk Exp $	*/


	/* info known about a imap folder. */

typedef	struct {
	  int flags ;
	} ImapMsg ;

#define	IM_SEEN		0x1
#define	IM_DELETED	0x2
#define	IM_RECENT	0x4
#define	IM_FETCHED	0x8

typedef	struct {
	  int fd ;
	  int seq ;
	  enum {NONAUTH, AUTH, SELECT, LOGOUT} state ;
	  int nmsg ;
	  ImapMsg *messages ;
	  int timeout ;
	  char *capabilities ;
	} ImapFolder ;





#ifdef	__STDC__
extern	ImapFolder *openImap( char *hostname, char *user, char *pw, int to) ;
extern	int	imapFolder(ImapFolder *, char *folder) ;
extern	int	imapGetHeader( ImapFolder *, MailMessage *, int msgno, int to);
extern	int	imapGetMessage( MailMessage *, MsgState);
extern	int	imapDeleteMessage( ImapFolder *p3, MailMessage *, int timeout) ;
extern	void	closeImap( ImapFolder *p3) ;
#else
extern	ImapFolder *openImap() ;
extern	int	imapFolder() ;
extern	int	imapGetHeader();
extern	int	imapGetMessage();
extern	int	imapDeleteMessage() ;
extern	void	closeImap() ;
#endif
