

/* Generated from /Users/hww3/pikebuild/src/peep.in by mkpeep.pike
   2014-05-01 02:28:45
*/

INLINE static int _asm_peep_1(void)
{
  switch(opcode(0))
  {
  case F_BRANCH_WHEN_EQ:
    /* ASSIGN_LOCAL BRANCH_WHEN_ZERO 2_LOCALS(,$1a) BRANCH_WHEN_EQ : 
       ASSIGN_LOCAL($1a) BRANCH_AND_POP_WHEN_ZERO($2a) LOCAL($3a) 
       BRANCH_WHEN_EQ($4a) */
    if(F_ASSIGN_LOCAL==opcode(3) && F_BRANCH_WHEN_ZERO==opcode(2) && 
       (argument(3))==argument2(1))
    {
      do_optimization(4, 4, 2, F_BRANCH_WHEN_EQ, argument(0), 2, F_LOCAL, 
                      argument(1), 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(2), 
                      2, F_ASSIGN_LOCAL, argument(3), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_NE:
    /* ASSIGN_LOCAL BRANCH_WHEN_ZERO 2_LOCALS(,$1a) BRANCH_WHEN_NE : 
       ASSIGN_LOCAL($1a) BRANCH_AND_POP_WHEN_ZERO($2a) LOCAL($3a) 
       BRANCH_WHEN_NE($4a) */
    if(F_ASSIGN_LOCAL==opcode(3) && F_BRANCH_WHEN_ZERO==opcode(2) && 
       (argument(3))==argument2(1))
    {
      do_optimization(4, 4, 2, F_BRANCH_WHEN_NE, argument(0), 2, F_LOCAL, 
                      argument(1), 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(2), 
                      2, F_ASSIGN_LOCAL, argument(3), 0);
      return 1;
    }
    break;

  case F_INDEX:
    /* 2_LOCALS INDEX: LOCAL_LOCAL_INDEX($1b,$1a) */
    {
      do_optimization(2, 1, 3, F_LOCAL_LOCAL_INDEX, argument2(1), argument(1), 
                      0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* 2_LOCALS MARK_X [$2a>0] : LOCAL($1a) MARK_X($2a-1) LOCAL($1b) */
    if(argument(0)>0)
    {
      do_optimization(2, 3, 2, F_LOCAL, argument2(1), 2, F_MARK_X, 
                      argument(0)-1, 2, F_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_SIZEOF:
    /* 2_LOCALS SIZEOF: LOCAL($1a) SIZEOF_LOCAL ($1b) */
    {
      do_optimization(2, 2, 2, F_SIZEOF_LOCAL, argument2(1), 2, F_LOCAL, 
                      argument(1), 0);
      return 1;
    }
    break;

  case F_SWITCH:
    /* 2_LOCALS SWITCH: LOCAL($1a) SWITCH_ON_LOCAL($1b,$2a) */
    {
      do_optimization(2, 2, 3, F_SWITCH_ON_LOCAL, argument2(1), argument(0), 
                      2, F_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_2(void)
{
  switch(opcode(2))
  {
  case F_LTOSVAL:
    /* LTOSVAL ADD ASSIGN : ADD_TO */
    if(F_ASSIGN==opcode(0))
    {
      do_optimization(3, 1, 1, F_ADD_TO, 0);
      return 1;
    }
    break;

  case F_LTOSVAL2_AND_FREE:
    /* LTOSVAL2_AND_FREE ADD ASSIGN : ADD_TO */
    if(F_ASSIGN==opcode(0))
    {
      do_optimization(3, 1, 1, F_ADD_TO, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_3(void)
{
  switch(opcode(2))
  {
  case F_LTOSVAL:
    /* LTOSVAL ADD_FLOATS ASSIGN : ADD_TO */
    if(F_ASSIGN==opcode(0))
    {
      do_optimization(3, 1, 1, F_ADD_TO, 0);
      return 1;
    }
    break;

  case F_LTOSVAL2_AND_FREE:
    /* LTOSVAL2_AND_FREE ADD_FLOATS ASSIGN : ADD_TO */
    if(F_ASSIGN==opcode(0))
    {
      do_optimization(3, 1, 1, F_ADD_TO, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_4(void)
{
  switch(opcode(0))
  {
  case F_GLOBAL:
    /* GLOBAL ADD_INT GLOBAL($1a) : GLOBAL($1a) DUP ADD_INT($2a) SWAP */
    if(F_GLOBAL==opcode(2) && (argument(2))==argument(0))
    {
      do_optimization(3, 4, 1, F_SWAP, 2, F_ADD_INT, argument(1), 1, F_DUP, 2, 
                      F_GLOBAL, argument(2), 0);
      return 1;
    }
    break;

  case F_LOCAL:
    /* LOCAL ADD_INT LOCAL($1a) : LOCAL($1a) DUP ADD_INT($2a) SWAP */
    if(F_LOCAL==opcode(2) && (argument(2))==argument(0))
    {
      do_optimization(3, 4, 1, F_SWAP, 2, F_ADD_INT, argument(1), 1, F_DUP, 2, 
                      F_LOCAL, argument(2), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_5(void)
{
  switch(opcode(2))
  {
  case F_LTOSVAL:
    /* LTOSVAL ADD_INTS ASSIGN : ADD_TO */
    if(F_ASSIGN==opcode(0))
    {
      do_optimization(3, 1, 1, F_ADD_TO, 0);
      return 1;
    }
    break;

  case F_LTOSVAL2_AND_FREE:
    /* LTOSVAL2_AND_FREE ADD_INTS ASSIGN : ADD_TO */
    if(F_ASSIGN==opcode(0))
    {
      do_optimization(3, 1, 1, F_ADD_TO, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_6(void)
{
  switch(opcode(0))
  {
  case F_GLOBAL:
    /* GLOBAL ADD_NEG_INT GLOBAL($1a) : GLOBAL($1a) DUP ADD_NEG_INT($2a) SWAP
       */
    if(F_GLOBAL==opcode(2) && (argument(2))==argument(0))
    {
      do_optimization(3, 4, 1, F_SWAP, 2, F_ADD_NEG_INT, argument(1), 1, 
                      F_DUP, 2, F_GLOBAL, argument(2), 0);
      return 1;
    }
    break;

  case F_LOCAL:
    /* LOCAL ADD_NEG_INT LOCAL($1a) : LOCAL($1a) DUP ADD_NEG_INT($2a) SWAP */
    if(F_LOCAL==opcode(2) && (argument(2))==argument(0))
    {
      do_optimization(3, 4, 1, F_SWAP, 2, F_ADD_NEG_INT, argument(1), 1, 
                      F_DUP, 2, F_LOCAL, argument(2), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_7(void)
{
  switch(opcode(0))
  {
  case F_POP_VALUE:
    /* APPLY POP_VALUE: APPLY_AND_POP($1a) */
    {
      do_optimization(2, 1, 2, F_APPLY_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* APPLY [ check_tailrecursion() ] RETURN : APPLY_AND_RETURN($1a) */
    if( check_tailrecursion() )
    {
      do_optimization(2, 1, 2, F_APPLY_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_8(void)
{
  switch(opcode(3))
  {
  case F_BRANCH_WHEN_ZERO:
    /* LOCAL_ARROW($1a, $1b) ARROW($2a) ARROW($3a) BRANCH_WHEN_ZERO 
       LOCAL_ARROW($1a, $1b) ARROW($2a) ARROW($3a) : LOCAL_ARROW($1a, $1b) 
       ARROW($2a) ARROW($3a) BRANCH_AND_POP_WHEN_ZERO ( $4a ) */
    if(F_LOCAL_ARROW==opcode(6) && F_ARROW==opcode(5) && F_ARROW==opcode(4) && 
       F_LOCAL_ARROW==opcode(2) && (argument(6))==argument(2) && 
       (argument2(6))==argument2(2) && (argument(5))==argument(1) && 
       F_ARROW==opcode(0) && (argument(4))==argument(0))
    {
      do_optimization(7, 4, 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(3), 2, 
                      F_ARROW, argument(4), 2, F_ARROW, argument(5), 3, 
                      F_LOCAL_ARROW, argument(6), argument2(6), 0);
      return 1;
    }
    break;

  case F_LOCAL_ARROW:
    switch(opcode(4))
    {
    case F_BRANCH_WHEN_ZERO:
      /* LOCAL_ARROW($1a, $1b) ARROW($2a) ARROW($3a) ARROW($4a) 
         BRANCH_WHEN_ZERO LOCAL_ARROW($1a, $1b) ARROW($2a) ARROW($3a) 
         ARROW($4a) : LOCAL_ARROW($1a, $1b) ARROW($2a) ARROW($3a) ARROW($4a) 
         BRANCH_AND_POP_WHEN_ZERO ( $5a ) */
      if(F_LOCAL_ARROW==opcode(8) && F_ARROW==opcode(7) && F_ARROW==opcode(6) 
         && F_ARROW==opcode(5) && (argument(8))==argument(3) && 
         (argument2(8))==argument2(3) && F_ARROW==opcode(2) && 
         (argument(7))==argument(2) && (argument(6))==argument(1) && 
         F_ARROW==opcode(0) && (argument(5))==argument(0))
      {
        do_optimization(9, 5, 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(4), 2, 
                        F_ARROW, argument(5), 2, F_ARROW, argument(6), 2, 
                        F_ARROW, argument(7), 3, F_LOCAL_ARROW, argument(8), 
                        argument2(8), 0);
        return 1;
      }
      break;

    case F_MARK:
      /* LOCAL_ARROW($1a, $1b) ARROW($2a) ARROW($3a) ARROW($4a) 
         BRANCH_WHEN_ZERO MARK LOCAL_ARROW($1a, $1b) ARROW($2a) ARROW($3a) 
         ARROW($4a) : LOCAL_ARROW($1a, $1b) ARROW($2a) ARROW($3a) ARROW($4a) 
         BRANCH_AND_POP_WHEN_ZERO ( $5a ) MARK_X(1) */
      if(F_LOCAL_ARROW==opcode(9) && F_ARROW==opcode(8) && F_ARROW==opcode(7) 
         && F_ARROW==opcode(6) && F_BRANCH_WHEN_ZERO==opcode(5) && 
         (argument(9))==argument(3) && (argument2(9))==argument2(3) && 
         F_ARROW==opcode(2) && (argument(8))==argument(2) && 
         (argument(7))==argument(1) && F_ARROW==opcode(0) && 
         (argument(6))==argument(0))
      {
        do_optimization(10, 6, 2, F_MARK_X, 1, 2, F_BRANCH_AND_POP_WHEN_ZERO, 
                        argument(5), 2, F_ARROW, argument(6), 2, F_ARROW, 
                        argument(7), 2, F_ARROW, argument(8), 3, 
                        F_LOCAL_ARROW, argument(9), argument2(9), 0);
        return 1;
      }
      break;

    }
    break;

  case F_MARK:
    /* LOCAL_ARROW($1a, $1b) ARROW($2a) ARROW($3a) BRANCH_WHEN_ZERO MARK 
       LOCAL_ARROW($1a, $1b) ARROW($2a) ARROW($3a) : LOCAL_ARROW($1a, $1b) 
       ARROW($2a) ARROW($3a) BRANCH_AND_POP_WHEN_ZERO ( $4a ) MARK_X(1) */
    if(F_LOCAL_ARROW==opcode(7) && F_ARROW==opcode(6) && F_ARROW==opcode(5) && 
       F_BRANCH_WHEN_ZERO==opcode(4) && F_LOCAL_ARROW==opcode(2) && 
       (argument(7))==argument(2) && (argument2(7))==argument2(2) && 
       (argument(6))==argument(1) && F_ARROW==opcode(0) && 
       (argument(5))==argument(0))
    {
      do_optimization(8, 5, 2, F_MARK_X, 1, 2, F_BRANCH_AND_POP_WHEN_ZERO, 
                      argument(4), 2, F_ARROW, argument(5), 2, F_ARROW, 
                      argument(6), 3, F_LOCAL_ARROW, argument(7), 
                      argument2(7), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_9(void)
{
  switch(opcode(0))
  {
  case F_COMPL:
    switch(opcode(2))
    {
    case F_CONST0:
      /* CONST0 ASSIGN_GLOBAL COMPL : CONST0 ASSIGN_GLOBAL_AND_POP($2a) CONST_1
         */
      {
        do_optimization(3, 3, 1, F_CONST_1, 2, F_ASSIGN_GLOBAL_AND_POP, 
                        argument(1), 1, F_CONST0, 0);
        return 1;
      }
      break;

    case F_CONST_1:
      /* CONST_1 ASSIGN_GLOBAL COMPL : CONST_1 ASSIGN_GLOBAL_AND_POP($2a) 
         CONST0 */
      {
        do_optimization(3, 3, 1, F_CONST0, 2, F_ASSIGN_GLOBAL_AND_POP, 
                        argument(1), 1, F_CONST_1, 0);
        return 1;
      }
      break;

    }
    break;

  case F_POP_VALUE:
    /* ASSIGN_GLOBAL POP_VALUE : ASSIGN_GLOBAL_AND_POP($1a) */
    {
      do_optimization(2, 1, 2, F_ASSIGN_GLOBAL_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_10(void)
{
  switch(opcode(0))
  {
  case F_DEC_AND_POP:
    switch(opcode(2))
    {
    case F_CONST0:
      /* GLOBAL_LVALUE CONST0 ASSIGN_GLOBAL_AND_POP($1a) DEC_AND_POP : CONST_1 
         ASSIGN_GLOBAL_AND_POP($1a) */
      if(F_GLOBAL_LVALUE==opcode(3) && (argument(3))==argument(1))
      {
        do_optimization(4, 2, 2, F_ASSIGN_GLOBAL_AND_POP, argument(3), 1, 
                        F_CONST_1, 0);
        return 1;
      }
      break;

    case F_CONST1:
      /* GLOBAL_LVALUE CONST1 ASSIGN_GLOBAL_AND_POP($1a) DEC_AND_POP : CONST0 
         ASSIGN_GLOBAL_AND_POP($1a) */
      if(F_GLOBAL_LVALUE==opcode(3) && (argument(3))==argument(1))
      {
        do_optimization(4, 2, 2, F_ASSIGN_GLOBAL_AND_POP, argument(3), 1, 
                        F_CONST0, 0);
        return 1;
      }
      break;

    case F_NEG_NUMBER:
      /* GLOBAL_LVALUE NEG_NUMBER ASSIGN_GLOBAL_AND_POP($1a) DEC_AND_POP : 
         NEG_NUMBER($2a) ADD_NEG_INT(1) ASSIGN_GLOBAL_AND_POP($1a) */
      if(F_GLOBAL_LVALUE==opcode(3) && (argument(3))==argument(1))
      {
        do_optimization(4, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(3), 2, 
                        F_ADD_NEG_INT, 1, 2, F_NEG_NUMBER, argument(2), 0);
        return 1;
      }
      break;

    case F_NUMBER:
      /* GLOBAL_LVALUE NUMBER ASSIGN_GLOBAL_AND_POP($1a) DEC_AND_POP : 
         NUMBER($2a) ADD_NEG_INT(1) ASSIGN_GLOBAL_AND_POP($1a) */
      if(F_GLOBAL_LVALUE==opcode(3) && (argument(3))==argument(1))
      {
        do_optimization(4, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(3), 2, 
                        F_ADD_NEG_INT, 1, 2, F_NUMBER, argument(2), 0);
        return 1;
      }
      break;

    }
    break;

  case F_GLOBAL:
    /* ASSIGN_GLOBAL_AND_POP GLOBAL($1a) : ASSIGN_GLOBAL($1a) */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 1, 2, F_ASSIGN_GLOBAL, argument(1), 0);
      return 1;
    }
    break;

  case F_GLOBAL_LVALUE:
    switch(opcode(2))
    {
    case F_CONST0:
      /* CONST0 ASSIGN_GLOBAL_AND_POP GLOBAL_LVALUE: GLOBAL_LVALUE($3a) CONST0 
         ASSIGN_GLOBAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(1), 1, 
                        F_CONST0, 2, F_GLOBAL_LVALUE, argument(0), 0);
        return 1;
      }
      break;

    case F_CONST1:
      /* CONST1 ASSIGN_GLOBAL_AND_POP GLOBAL_LVALUE: GLOBAL_LVALUE($3a) CONST1 
         ASSIGN_GLOBAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(1), 1, 
                        F_CONST1, 2, F_GLOBAL_LVALUE, argument(0), 0);
        return 1;
      }
      break;

    case F_CONST_1:
      /* CONST_1 ASSIGN_GLOBAL_AND_POP GLOBAL_LVALUE: GLOBAL_LVALUE($3a) 
         CONST_1 ASSIGN_GLOBAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(1), 1, 
                        F_CONST_1, 2, F_GLOBAL_LVALUE, argument(0), 0);
        return 1;
      }
      break;

    case F_NEG_NUMBER:
      /* NEG_NUMBER ASSIGN_GLOBAL_AND_POP GLOBAL_LVALUE: GLOBAL_LVALUE($3a) 
         NEG_NUMBER($1a) ASSIGN_GLOBAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(1), 2, 
                        F_NEG_NUMBER, argument(2), 2, F_GLOBAL_LVALUE, 
                        argument(0), 0);
        return 1;
      }
      break;

    case F_NUMBER:
      /* NUMBER ASSIGN_GLOBAL_AND_POP GLOBAL_LVALUE: GLOBAL_LVALUE($3a) 
         NUMBER($1a) ASSIGN_GLOBAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(1), 2, 
                        F_NUMBER, argument(2), 2, F_GLOBAL_LVALUE, 
                        argument(0), 0);
        return 1;
      }
      break;

    }
    break;

  case F_INC_AND_POP:
    switch(opcode(2))
    {
    case F_CONST0:
      /* GLOBAL_LVALUE CONST0 ASSIGN_GLOBAL_AND_POP($1a) INC_AND_POP : CONST1 
         ASSIGN_GLOBAL_AND_POP($1a) */
      if(F_GLOBAL_LVALUE==opcode(3) && (argument(3))==argument(1))
      {
        do_optimization(4, 2, 2, F_ASSIGN_GLOBAL_AND_POP, argument(3), 1, 
                        F_CONST1, 0);
        return 1;
      }
      break;

    case F_CONST_1:
      /* GLOBAL_LVALUE CONST_1 ASSIGN_GLOBAL_AND_POP($1a) INC_AND_POP : CONST0 
         ASSIGN_GLOBAL_AND_POP($1a) */
      if(F_GLOBAL_LVALUE==opcode(3) && (argument(3))==argument(1))
      {
        do_optimization(4, 2, 2, F_ASSIGN_GLOBAL_AND_POP, argument(3), 1, 
                        F_CONST0, 0);
        return 1;
      }
      break;

    case F_NEG_NUMBER:
      /* GLOBAL_LVALUE NEG_NUMBER ASSIGN_GLOBAL_AND_POP($1a) INC_AND_POP : 
         NEG_NUMBER($2a) ADD_INT(1) ASSIGN_GLOBAL_AND_POP($1a) */
      if(F_GLOBAL_LVALUE==opcode(3) && (argument(3))==argument(1))
      {
        do_optimization(4, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(3), 2, 
                        F_ADD_INT, 1, 2, F_NEG_NUMBER, argument(2), 0);
        return 1;
      }
      break;

    case F_NUMBER:
      /* GLOBAL_LVALUE NUMBER ASSIGN_GLOBAL_AND_POP($1a) INC_AND_POP : 
         NUMBER($2a) ADD_INT(1) ASSIGN_GLOBAL_AND_POP($1a) */
      if(F_GLOBAL_LVALUE==opcode(3) && (argument(3))==argument(1))
      {
        do_optimization(4, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(3), 2, 
                        F_ADD_INT, 1, 2, F_NUMBER, argument(2), 0);
        return 1;
      }
      break;

    }
    break;

  case F_LOCAL_LVALUE:
    switch(opcode(2))
    {
    case F_CONST0:
      /* CONST0 ASSIGN_GLOBAL_AND_POP LOCAL_LVALUE: LOCAL_LVALUE($3a) CONST0 
         ASSIGN_GLOBAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(1), 1, 
                        F_CONST0, 2, F_LOCAL_LVALUE, argument(0), 0);
        return 1;
      }
      break;

    case F_CONST1:
      /* CONST1 ASSIGN_GLOBAL_AND_POP LOCAL_LVALUE: LOCAL_LVALUE($3a) CONST1 
         ASSIGN_GLOBAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(1), 1, 
                        F_CONST1, 2, F_LOCAL_LVALUE, argument(0), 0);
        return 1;
      }
      break;

    case F_CONST_1:
      /* CONST_1 ASSIGN_GLOBAL_AND_POP LOCAL_LVALUE: LOCAL_LVALUE($3a) CONST_1 
         ASSIGN_GLOBAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(1), 1, 
                        F_CONST_1, 2, F_LOCAL_LVALUE, argument(0), 0);
        return 1;
      }
      break;

    case F_NEG_NUMBER:
      /* NEG_NUMBER ASSIGN_GLOBAL_AND_POP LOCAL_LVALUE: LOCAL_LVALUE($3a) 
         NEG_NUMBER($1a) ASSIGN_GLOBAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(1), 2, 
                        F_NEG_NUMBER, argument(2), 2, F_LOCAL_LVALUE, 
                        argument(0), 0);
        return 1;
      }
      break;

    case F_NUMBER:
      /* NUMBER ASSIGN_GLOBAL_AND_POP LOCAL_LVALUE: LOCAL_LVALUE($3a) 
         NUMBER($1a) ASSIGN_GLOBAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_GLOBAL_AND_POP, argument(1), 2, 
                        F_NUMBER, argument(2), 2, F_LOCAL_LVALUE, argument(0), 
                        0);
        return 1;
      }
      break;

    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_11(void)
{
  switch(opcode(0))
  {
  case F_ASSIGN_LOCAL:
    /* ASSIGN_LOCAL ASSIGN_LOCAL($1a) : ASSIGN_LOCAL($1a) */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 1, 2, F_ASSIGN_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_COMPL:
    switch(opcode(2))
    {
    case F_CONST0:
      /* CONST0 ASSIGN_LOCAL COMPL : CLEAR_LOCAL($2a) CONST_1 */
      {
        do_optimization(3, 2, 1, F_CONST_1, 2, F_CLEAR_LOCAL, argument(1), 0);
        return 1;
      }
      break;

    case F_CONST_1:
      /* CONST_1 ASSIGN_LOCAL COMPL : CONST_1 ASSIGN_LOCAL_AND_POP($2a) CONST0
         */
      {
        do_optimization(3, 3, 1, F_CONST0, 2, F_ASSIGN_LOCAL_AND_POP, 
                        argument(1), 1, F_CONST_1, 0);
        return 1;
      }
      break;

    }
    break;

  case F_MARK_X:
    switch(opcode(2))
    {
    case F_CONST1:
      /* CONST1 ASSIGN_LOCAL MARK_X [$3a>0] : MARK_X($3a-1) CONST1 
         ASSIGN_LOCAL($2a) */
      if(argument(0)>0)
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL, argument(1), 1, F_CONST1, 2, 
                        F_MARK_X, argument(0)-1, 0);
        return 1;
      }
      break;

    case F_CONST_1:
      /* CONST_1 ASSIGN_LOCAL MARK_X [$3a>0] : MARK_X($3a-1) CONST_1 
         ASSIGN_LOCAL($2a) */
      if(argument(0)>0)
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL, argument(1), 1, F_CONST_1, 2, 
                        F_MARK_X, argument(0)-1, 0);
        return 1;
      }
      break;

    case F_NEG_NUMBER:
      /* NEG_NUMBER ASSIGN_LOCAL MARK_X [$3a>0] : MARK_X($3a-1) NEG_NUMBER($1a)
         ASSIGN_LOCAL($2a) */
      if(argument(0)>0)
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL, argument(1), 2, F_NEG_NUMBER, 
                        argument(2), 2, F_MARK_X, argument(0)-1, 0);
        return 1;
      }
      break;

    case F_NUMBER:
      /* NUMBER ASSIGN_LOCAL MARK_X [$3a>0] : MARK_X($3a-1) NUMBER($1a) 
         ASSIGN_LOCAL($2a) */
      if(argument(0)>0)
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL, argument(1), 2, F_NUMBER, 
                        argument(2), 2, F_MARK_X, argument(0)-1, 0);
        return 1;
      }
      break;

    }
    break;

  case F_NEGATE:
    switch(opcode(2))
    {
    case F_CONST1:
      /* CONST1 ASSIGN_LOCAL NEGATE: CONST1 ASSIGN_LOCAL_AND_POP($2a) CONST_1
         */
      {
        do_optimization(3, 3, 1, F_CONST_1, 2, F_ASSIGN_LOCAL_AND_POP, 
                        argument(1), 1, F_CONST1, 0);
        return 1;
      }
      break;

    case F_CONST_1:
      /* CONST_1 ASSIGN_LOCAL NEGATE: CONST_1 ASSIGN_LOCAL_AND_POP($2a) CONST1
         */
      {
        do_optimization(3, 3, 1, F_CONST1, 2, F_ASSIGN_LOCAL_AND_POP, 
                        argument(1), 1, F_CONST_1, 0);
        return 1;
      }
      break;

    case F_NEG_NUMBER:
      /* NEG_NUMBER ASSIGN_LOCAL NEGATE: NEG_NUMBER($1a) 
         ASSIGN_LOCAL_AND_POP($2a) NUMBER($1a) */
      {
        do_optimization(3, 3, 2, F_NUMBER, argument(2), 2, 
                        F_ASSIGN_LOCAL_AND_POP, argument(1), 2, F_NEG_NUMBER, 
                        argument(2), 0);
        return 1;
      }
      break;

    case F_NUMBER:
      /* NUMBER ASSIGN_LOCAL NEGATE: NUMBER($1a) ASSIGN_LOCAL_AND_POP($2a) 
         NEG_NUMBER($1a) */
      {
        do_optimization(3, 3, 2, F_NEG_NUMBER, argument(2), 2, 
                        F_ASSIGN_LOCAL_AND_POP, argument(1), 2, F_NUMBER, 
                        argument(2), 0);
        return 1;
      }
      break;

    }
    break;

  case F_POP_VALUE:
    /* ASSIGN_LOCAL POP_VALUE : ASSIGN_LOCAL_AND_POP($1a) */
    {
      do_optimization(2, 1, 2, F_ASSIGN_LOCAL_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* ASSIGN_LOCAL RETURN [!(Pike_compiler->compiler_frame->lexical_scope & 
       SCOPE_SCOPED)]: RETURN */
    if(!(Pike_compiler->compiler_frame->lexical_scope & SCOPE_SCOPED))
    {
      do_optimization(2, 1, 1, F_RETURN, 0);
      return 1;
    }
    break;

  case F_VOLATILE_RETURN:
    /* ASSIGN_LOCAL VOLATILE_RETURN 
       [!(Pike_compiler->compiler_frame->lexical_scope & SCOPE_SCOPED)]: 
       VOLATILE_RETURN */
    if(!(Pike_compiler->compiler_frame->lexical_scope & SCOPE_SCOPED))
    {
      do_optimization(2, 1, 1, F_VOLATILE_RETURN, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_12(void)
{
  switch(opcode(0))
  {
  case F_DEC_LOCAL:
    /* ASSIGN_LOCAL_AND_POP DEC_LOCAL($1a) : ADD_NEG_INT(1) ASSIGN_LOCAL($1a)
       */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 2, 2, F_ASSIGN_LOCAL, argument(1), 2, F_ADD_NEG_INT, 
                      1, 0);
      return 1;
    }
    break;

  case F_GLOBAL_LVALUE:
    switch(opcode(2))
    {
    case F_CONST1:
      /* CONST1 ASSIGN_LOCAL_AND_POP GLOBAL_LVALUE: GLOBAL_LVALUE($3a) CONST1 
         ASSIGN_LOCAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL_AND_POP, argument(1), 1, 
                        F_CONST1, 2, F_GLOBAL_LVALUE, argument(0), 0);
        return 1;
      }
      break;

    case F_CONST_1:
      /* CONST_1 ASSIGN_LOCAL_AND_POP GLOBAL_LVALUE: GLOBAL_LVALUE($3a) CONST_1
         ASSIGN_LOCAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL_AND_POP, argument(1), 1, 
                        F_CONST_1, 2, F_GLOBAL_LVALUE, argument(0), 0);
        return 1;
      }
      break;

    case F_NEG_NUMBER:
      /* NEG_NUMBER ASSIGN_LOCAL_AND_POP GLOBAL_LVALUE: GLOBAL_LVALUE($3a) 
         NEG_NUMBER($1a) ASSIGN_LOCAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL_AND_POP, argument(1), 2, 
                        F_NEG_NUMBER, argument(2), 2, F_GLOBAL_LVALUE, 
                        argument(0), 0);
        return 1;
      }
      break;

    case F_NUMBER:
      /* NUMBER ASSIGN_LOCAL_AND_POP GLOBAL_LVALUE: GLOBAL_LVALUE($3a) 
         NUMBER($1a) ASSIGN_LOCAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL_AND_POP, argument(1), 2, 
                        F_NUMBER, argument(2), 2, F_GLOBAL_LVALUE, 
                        argument(0), 0);
        return 1;
      }
      break;

    }
    break;

  case F_INC_LOCAL:
    /* ASSIGN_LOCAL_AND_POP INC_LOCAL($1a) : ADD_INT(1) ASSIGN_LOCAL($1a) */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 2, 2, F_ASSIGN_LOCAL, argument(1), 2, F_ADD_INT, 1, 
                      0);
      return 1;
    }
    break;

  case F_LOCAL:
    /* ASSIGN_LOCAL_AND_POP LOCAL($1a) : ASSIGN_LOCAL($1a) */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 1, 2, F_ASSIGN_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_LOCAL_LVALUE:
    switch(opcode(2))
    {
    case F_CONST1:
      /* CONST1 ASSIGN_LOCAL_AND_POP LOCAL_LVALUE: LOCAL_LVALUE($3a) CONST1 
         ASSIGN_LOCAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL_AND_POP, argument(1), 1, 
                        F_CONST1, 2, F_LOCAL_LVALUE, argument(0), 0);
        return 1;
      }
      break;

    case F_CONST_1:
      /* CONST_1 ASSIGN_LOCAL_AND_POP LOCAL_LVALUE: LOCAL_LVALUE($3a) CONST_1 
         ASSIGN_LOCAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL_AND_POP, argument(1), 1, 
                        F_CONST_1, 2, F_LOCAL_LVALUE, argument(0), 0);
        return 1;
      }
      break;

    case F_NEG_NUMBER:
      /* NEG_NUMBER ASSIGN_LOCAL_AND_POP LOCAL_LVALUE: LOCAL_LVALUE($3a) 
         NEG_NUMBER($1a) ASSIGN_LOCAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL_AND_POP, argument(1), 2, 
                        F_NEG_NUMBER, argument(2), 2, F_LOCAL_LVALUE, 
                        argument(0), 0);
        return 1;
      }
      break;

    case F_NUMBER:
      /* NUMBER ASSIGN_LOCAL_AND_POP LOCAL_LVALUE: LOCAL_LVALUE($3a) 
         NUMBER($1a) ASSIGN_LOCAL_AND_POP($2a) */
      {
        do_optimization(3, 3, 2, F_ASSIGN_LOCAL_AND_POP, argument(1), 2, 
                        F_NUMBER, argument(2), 2, F_LOCAL_LVALUE, argument(0), 
                        0);
        return 1;
      }
      break;

    }
    break;

  case F_MARK_AND_LOCAL:
    /* ASSIGN_LOCAL_AND_POP MARK_AND_LOCAL($1a) : ASSIGN_LOCAL($1a) MARK_X(1)
       */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 2, 2, F_MARK_X, 1, 2, F_ASSIGN_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_13(void)
{
  switch(opcode(0))
  {
  case F_POP_VALUE:
    /* CALL_BUILTIN POP_VALUE: CALL_BUILTIN_AND_POP($1a) */
    {
      do_optimization(2, 1, 2, F_CALL_BUILTIN_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* CALL_BUILTIN [ check_tailrecursion() ] RETURN : 
       CALL_BUILTIN_AND_RETURN($1a) */
    if( check_tailrecursion() )
    {
      do_optimization(2, 1, 2, F_CALL_BUILTIN_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_14(void)
{
  switch(opcode(0))
  {
  case F_BRANCH_WHEN_ZERO:
    /* CALL_BUILTIN1 [ 
       Pike_compiler->new_program->constants[$1a].sval.u.efun->function == 
       f_intp ] BRANCH_WHEN_ZERO : BRANCH_IF_TYPE_IS_NOT( T_INT ) POINTER($2a)
       */
    if( 
       Pike_compiler->new_program->constants[argument(1)].sval.u.efun->function
       == f_intp )
    {
      do_optimization(2, 2, 2, F_POINTER, argument(0), 2, 
                      F_BRANCH_IF_TYPE_IS_NOT, T_INT, 0);
      return 1;
    }
    /* CALL_BUILTIN1 [ 
       Pike_compiler->new_program->constants[$1a].sval.u.efun->function == 
       f_stringp ] BRANCH_WHEN_ZERO : BRANCH_IF_TYPE_IS_NOT( T_STRING ) 
       POINTER($2a) */
    if( 
       Pike_compiler->new_program->constants[argument(1)].sval.u.efun->function
       == f_stringp )
    {
      do_optimization(2, 2, 2, F_POINTER, argument(0), 2, 
                      F_BRANCH_IF_TYPE_IS_NOT, T_STRING, 0);
      return 1;
    }
    /* CALL_BUILTIN1 [ 
       Pike_compiler->new_program->constants[$1a].sval.u.efun->function == 
       f_arrayp ] BRANCH_WHEN_ZERO : BRANCH_IF_TYPE_IS_NOT( T_ARRAY ) 
       POINTER($2a) */
    if( 
       Pike_compiler->new_program->constants[argument(1)].sval.u.efun->function
       == f_arrayp )
    {
      do_optimization(2, 2, 2, F_POINTER, argument(0), 2, 
                      F_BRANCH_IF_TYPE_IS_NOT, T_ARRAY, 0);
      return 1;
    }
    /* CALL_BUILTIN1 [ 
       Pike_compiler->new_program->constants[$1a].sval.u.efun->function == 
       f_floatp ] BRANCH_WHEN_ZERO : BRANCH_IF_TYPE_IS_NOT( T_FLOAT ) 
       POINTER($2a) */
    if( 
       Pike_compiler->new_program->constants[argument(1)].sval.u.efun->function
       == f_floatp )
    {
      do_optimization(2, 2, 2, F_POINTER, argument(0), 2, 
                      F_BRANCH_IF_TYPE_IS_NOT, T_FLOAT, 0);
      return 1;
    }
    /* CALL_BUILTIN1 [ 
       Pike_compiler->new_program->constants[$1a].sval.u.efun->function == 
       f_mappingp ] BRANCH_WHEN_ZERO : BRANCH_IF_TYPE_IS_NOT( T_MAPPING ) 
       POINTER($2a) */
    if( 
       Pike_compiler->new_program->constants[argument(1)].sval.u.efun->function
       == f_mappingp )
    {
      do_optimization(2, 2, 2, F_POINTER, argument(0), 2, 
                      F_BRANCH_IF_TYPE_IS_NOT, T_MAPPING, 0);
      return 1;
    }
    /* CALL_BUILTIN1 [ 
       Pike_compiler->new_program->constants[$1a].sval.u.efun->function == 
       f_multisetp ] BRANCH_WHEN_ZERO : BRANCH_IF_TYPE_IS_NOT( T_MULTISET ) 
       POINTER($2a) */
    if( 
       Pike_compiler->new_program->constants[argument(1)].sval.u.efun->function
       == f_multisetp )
    {
      do_optimization(2, 2, 2, F_POINTER, argument(0), 2, 
                      F_BRANCH_IF_TYPE_IS_NOT, T_MULTISET, 0);
      return 1;
    }
    break;

  case F_POP_VALUE:
    /* CALL_BUILTIN1 POP_VALUE : CALL_BUILTIN1_AND_POP ($1a) */
    {
      do_optimization(2, 1, 2, F_CALL_BUILTIN1_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_15(void)
{
  switch(opcode(0))
  {
  case F_POP_VALUE:
    /* CALL_FUNCTION POP_VALUE: CALL_FUNCTION_AND_POP($1a) */
    {
      do_optimization(2, 1, 2, F_CALL_FUNCTION_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* CALL_FUNCTION [ check_tailrecursion() ] RETURN : 
       CALL_FUNCTION_AND_RETURN($1a) */
    if( check_tailrecursion() )
    {
      do_optimization(2, 1, 2, F_CALL_FUNCTION_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_16(void)
{
  switch(opcode(0))
  {
  case F_POP_VALUE:
    /* CALL_LFUN POP_VALUE: CALL_LFUN_AND_POP($1a) */
    {
      do_optimization(2, 1, 2, F_CALL_LFUN_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* CALL_LFUN [ check_tailrecursion() ] RETURN : CALL_LFUN_AND_RETURN($1a)
       */
    if( check_tailrecursion() )
    {
      do_optimization(2, 1, 2, F_CALL_LFUN_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_17(void)
{
  switch(opcode(0))
  {
  case F_POP_VALUE:
    /* CALL_OTHER POP_VALUE: CALL_OTHER_AND_POP($1a) */
    {
      do_optimization(2, 1, 2, F_CALL_OTHER_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* CALL_OTHER [ check_tailrecursion() ] RETURN : CALL_OTHER_AND_RETURN($1a)
       */
    if( check_tailrecursion() )
    {
      do_optimization(2, 1, 2, F_CALL_OTHER_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_18(void)
{
  switch(opcode(0))
  {
  case F_CLEAR_2_LOCAL:
    /* CLEAR_2_LOCAL CLEAR_2_LOCAL($1a+2) : CLEAR_4_LOCAL($1a) */
    if((argument(1)+2)==argument(0))
    {
      do_optimization(2, 1, 2, F_CLEAR_4_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_CLEAR_4_LOCAL:
    /* CLEAR_2_LOCAL CLEAR_4_LOCAL($1a+2) : CLEAR_4_LOCAL($1a) 
       CLEAR_2_LOCAL($2a+2) */
    if((argument(1)+2)==argument(0))
    {
      do_optimization(2, 2, 2, F_CLEAR_2_LOCAL, argument(0)+2, 2, 
                      F_CLEAR_4_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_19(void)
{
  switch(opcode(0))
  {
  case F_CLEAR_2_LOCAL:
    /* CLEAR_LOCAL CLEAR_2_LOCAL($1a+1) : CLEAR_2_LOCAL($1a) CLEAR_LOCAL($2a+1)
       */
    if((argument(1)+1)==argument(0))
    {
      do_optimization(2, 2, 2, F_CLEAR_LOCAL, argument(0)+1, 2, 
                      F_CLEAR_2_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_CLEAR_4_LOCAL:
    /* CLEAR_LOCAL CLEAR_4_LOCAL($1a+1) : CLEAR_4_LOCAL($1a) CLEAR_LOCAL($2a+3)
       */
    if((argument(1)+1)==argument(0))
    {
      do_optimization(2, 2, 2, F_CLEAR_LOCAL, argument(0)+3, 2, 
                      F_CLEAR_4_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_CLEAR_LOCAL:
    /* CLEAR_LOCAL CLEAR_LOCAL($1a) : CLEAR_LOCAL($1a) */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 1, 2, F_CLEAR_LOCAL, argument(1), 0);
      return 1;
    }
    /* CLEAR_LOCAL CLEAR_LOCAL($1a+1) : CLEAR_2_LOCAL($1a) */
    if((argument(1)+1)==argument(0))
    {
      do_optimization(2, 1, 2, F_CLEAR_2_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_DEC_LOCAL_AND_POP:
    /* CLEAR_LOCAL DEC_LOCAL_AND_POP($1a) : CONST_1 ASSIGN_LOCAL_AND_POP($1a)
       */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 2, 2, F_ASSIGN_LOCAL_AND_POP, argument(1), 1, 
                      F_CONST_1, 0);
      return 1;
    }
    break;

  case F_GLOBAL_LVALUE:
    /* CLEAR_LOCAL GLOBAL_LVALUE: GLOBAL_LVALUE($2a) CLEAR_LOCAL($1a) */
    {
      do_optimization(2, 2, 2, F_CLEAR_LOCAL, argument(1), 2, F_GLOBAL_LVALUE, 
                      argument(0), 0);
      return 1;
    }
    break;

  case F_INC_LOCAL_AND_POP:
    /* CLEAR_LOCAL INC_LOCAL_AND_POP($1a) : CONST1 ASSIGN_LOCAL_AND_POP($1a) */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 2, 2, F_ASSIGN_LOCAL_AND_POP, argument(1), 1, 
                      F_CONST1, 0);
      return 1;
    }
    break;

  case F_LOCAL:
    /* CLEAR_LOCAL LOCAL($1a) : CLEAR_LOCAL($1a) CONST0 */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 2, 1, F_CONST0, 2, F_CLEAR_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_LOCAL_LVALUE:
    /* CLEAR_LOCAL LOCAL_LVALUE: LOCAL_LVALUE($2a) CLEAR_LOCAL($1a) */
    {
      do_optimization(2, 2, 2, F_CLEAR_LOCAL, argument(1), 2, F_LOCAL_LVALUE, 
                      argument(0), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_20(void)
{
  switch(opcode(2))
  {
  case F_2_LOCALS:
    /* 2_LOCALS CLEAR_STRING_SUBTYPE INDEX: LOCAL_LOCAL_INDEX($1b,$1a) */
    if(F_INDEX==opcode(0))
    {
      do_optimization(3, 1, 3, F_LOCAL_LOCAL_INDEX, argument2(2), argument(2), 
                      0);
      return 1;
    }
    break;

  case F_LOCAL:
    /* LOCAL CLEAR_STRING_SUBTYPE INDEX: LOCAL_INDEX ($1a) */
    if(F_INDEX==opcode(0))
    {
      do_optimization(3, 1, 2, F_LOCAL_INDEX, argument(2), 0);
      return 1;
    }
    break;

  case F_MARK_AND_LOCAL:
    /* MARK_AND_LOCAL CLEAR_STRING_SUBTYPE INDEX: MARK LOCAL_INDEX ($1a) */
    if(F_INDEX==opcode(0))
    {
      do_optimization(3, 2, 2, F_LOCAL_INDEX, argument(2), 1, F_MARK, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_21(void)
{
  switch(opcode(0))
  {
  case F_ADD:
    /* CONST0 ADD: ADD_INT (0) */
    {
      do_optimization(2, 1, 2, F_ADD_INT, 0, 0);
      return 1;
    }
    break;

  case F_ADD_INT:
    /* CONST0 ADD_INT : NUMBER($2a) */
    {
      do_optimization(2, 1, 2, F_NUMBER, argument(0), 0);
      return 1;
    }
    break;

  case F_ADD_INTS:
    /* CONST0 ADD_INTS: ADD_INT (0) */
    {
      do_optimization(2, 1, 2, F_ADD_INT, 0, 0);
      return 1;
    }
    break;

  case F_ADD_NEG_INT:
    /* CONST0 ADD_NEG_INT : NEG_NUMBER($2a) */
    {
      do_optimization(2, 1, 2, F_NEG_NUMBER, argument(0), 0);
      return 1;
    }
    break;

  case F_ASSIGN_LOCAL:
    /* CONST0 ASSIGN_LOCAL: CLEAR_LOCAL($2a) CONST0 */
    {
      do_optimization(2, 2, 1, F_CONST0, 2, F_CLEAR_LOCAL, argument(0), 0);
      return 1;
    }
    break;

  case F_ASSIGN_LOCAL_AND_POP:
    /* CONST0 ASSIGN_LOCAL_AND_POP : CLEAR_LOCAL($2a) */
    {
      do_optimization(2, 1, 2, F_CLEAR_LOCAL, argument(0), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_LE:
    switch(opcode(2))
    {
    case F_SIZEOF:
      /* SIZEOF CONST0 BRANCH_WHEN_LE : SIZEOF BRANCH_WHEN_ZERO ($3a) */
      {
        do_optimization(3, 2, 2, F_BRANCH_WHEN_ZERO, argument(0), 1, F_SIZEOF, 
                        0);
        return 1;
      }
      break;

    case F_SIZEOF_LOCAL:
      /* SIZEOF_LOCAL CONST0 BRANCH_WHEN_LE : SIZEOF_LOCAL($1a) 
         BRANCH_WHEN_ZERO ($3a) */
      {
        do_optimization(3, 2, 2, F_BRANCH_WHEN_ZERO, argument(0), 2, 
                        F_SIZEOF_LOCAL, argument(2), 0);
        return 1;
      }
      break;

    }
    break;

  case F_BRANCH_WHEN_NON_ZERO:
    /* CONST0 BRANCH_WHEN_NON_ZERO: */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_ZERO:
    /* CONST0 BRANCH_WHEN_ZERO: BRANCH($2a) */
    {
      do_optimization(2, 1, 2, F_BRANCH, argument(0), 0);
      return 1;
    }
    break;

  case F_CLEAR_LOCAL:
    /* CONST0 CLEAR_LOCAL : CLEAR_LOCAL($2a) CONST0 */
    {
      do_optimization(2, 2, 1, F_CONST0, 2, F_CLEAR_LOCAL, argument(0), 0);
      return 1;
    }
    break;

  case F_CLEAR_STRING_SUBTYPE:
    /* CONST0 CLEAR_STRING_SUBTYPE: CONST0 */
    {
      do_optimization(2, 1, 1, F_CONST0, 0);
      return 1;
    }
    break;

  case F_COMPL:
    /* CONST0 COMPL : CONST_1 */
    {
      do_optimization(2, 1, 1, F_CONST_1, 0);
      return 1;
    }
    break;

  case F_INDEX:
    /* CONST0 INDEX: POS_INT_INDEX (0) */
    {
      do_optimization(2, 1, 2, F_POS_INT_INDEX, 0, 0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* CONST0 MARK_X [$2a>0] : MARK_X($2a-1) CONST0 */
    if(argument(0)>0)
    {
      do_optimization(2, 2, 1, F_CONST0, 2, F_MARK_X, argument(0)-1, 0);
      return 1;
    }
    break;

  case F_NEGATE:
    /* CONST0 NEGATE : CONST0 */
    {
      do_optimization(2, 1, 1, F_CONST0, 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* CONST0 RETURN: RETURN_0 */
    {
      do_optimization(2, 1, 1, F_RETURN_0, 0);
      return 1;
    }
    break;

  case F_SUBTRACT:
    /* CONST0 SUBTRACT: */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_VOLATILE_RETURN:
    /* CONST0 VOLATILE_RETURN: RETURN_0 */
    {
      do_optimization(2, 1, 1, F_RETURN_0, 0);
      return 1;
    }
    break;

  case F_XOR:
    /* CONST0 XOR: */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_22(void)
{
  switch(opcode(0))
  {
  case F_ADD:
    /* CONST1 ADD: ADD_INT (1) */
    {
      do_optimization(2, 1, 2, F_ADD_INT, 1, 0);
      return 1;
    }
    break;

  case F_ADD_INT:
    /* CONST1 ADD_INT [($2a+1) > 0] : NUMBER($2a+1) */
    if((argument(0)+1) > 0)
    {
      do_optimization(2, 1, 2, F_NUMBER, argument(0)+1, 0);
      return 1;
    }
    break;

  case F_ADD_INTS:
    /* CONST1 ADD_INTS: ADD_INT (1) */
    {
      do_optimization(2, 1, 2, F_ADD_INT, 1, 0);
      return 1;
    }
    break;

  case F_ADD_NEG_INT:
    /* CONST1 ADD_NEG_INT : NEG_NUMBER($2a-1) */
    {
      do_optimization(2, 1, 2, F_NEG_NUMBER, argument(0)-1, 0);
      return 1;
    }
    break;

  case F_ADD_TO:
    /* LOCAL CONST1 ADD_TO : INC_LOCAL($1a) */
    if(F_LOCAL==opcode(2))
    {
      do_optimization(3, 1, 2, F_INC_LOCAL, argument(2), 0);
      return 1;
    }
    break;

  case F_ADD_TO_AND_POP:
    /* LOCAL CONST1 ADD_TO_AND_POP : INC_LOCAL_AND_POP($1a) */
    if(F_LOCAL==opcode(2))
    {
      do_optimization(3, 1, 2, F_INC_LOCAL_AND_POP, argument(2), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_LT:
    switch(opcode(2))
    {
    case F_SIZEOF:
      /* SIZEOF CONST1 BRANCH_WHEN_LT : SIZEOF BRANCH_WHEN_ZERO ($3a) */
      {
        do_optimization(3, 2, 2, F_BRANCH_WHEN_ZERO, argument(0), 1, F_SIZEOF, 
                        0);
        return 1;
      }
      break;

    case F_SIZEOF_LOCAL:
      /* SIZEOF_LOCAL CONST1 BRANCH_WHEN_LT : SIZEOF_LOCAL($1a) 
         BRANCH_WHEN_ZERO ($3a) */
      {
        do_optimization(3, 2, 2, F_BRANCH_WHEN_ZERO, argument(0), 2, 
                        F_SIZEOF_LOCAL, argument(2), 0);
        return 1;
      }
      break;

    }
    break;

  case F_BRANCH_WHEN_NON_ZERO:
    /* CONST1 BRANCH_WHEN_NON_ZERO: BRANCH($2a) */
    {
      do_optimization(2, 1, 2, F_BRANCH, argument(0), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_ZERO:
    /* CONST1 BRANCH_WHEN_ZERO: */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_CLEAR_LOCAL:
    /* CONST1 CLEAR_LOCAL : CLEAR_LOCAL($2a) CONST1 */
    {
      do_optimization(2, 2, 1, F_CONST1, 2, F_CLEAR_LOCAL, argument(0), 0);
      return 1;
    }
    break;

  case F_CLEAR_STRING_SUBTYPE:
    /* CONST1 CLEAR_STRING_SUBTYPE: CONST1 */
    {
      do_optimization(2, 1, 1, F_CONST1, 0);
      return 1;
    }
    break;

  case F_INDEX:
    /* CONST1 INDEX: POS_INT_INDEX (1) */
    {
      do_optimization(2, 1, 2, F_POS_INT_INDEX, 1, 0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* CONST1 MARK_X [$2a>0] : MARK_X($2a-1) CONST1 */
    if(argument(0)>0)
    {
      do_optimization(2, 2, 1, F_CONST1, 2, F_MARK_X, argument(0)-1, 0);
      return 1;
    }
    break;

  case F_NEGATE:
    /* CONST1 NEGATE : CONST_1 */
    {
      do_optimization(2, 1, 1, F_CONST_1, 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* CONST1 RETURN: RETURN_1 */
    {
      do_optimization(2, 1, 1, F_RETURN_1, 0);
      return 1;
    }
    break;

  case F_SUBTRACT:
    /* NEGATE CONST1 SUBTRACT : COMPL */
    if(F_NEGATE==opcode(2))
    {
      do_optimization(3, 1, 1, F_COMPL, 0);
      return 1;
    }
    /* CONST1 SUBTRACT: ADD_NEG_INT (1) */
    {
      do_optimization(2, 1, 2, F_ADD_NEG_INT, 1, 0);
      return 1;
    }
    break;

  case F_VOLATILE_RETURN:
    /* CONST1 VOLATILE_RETURN: RETURN_1 */
    {
      do_optimization(2, 1, 1, F_RETURN_1, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_23(void)
{
  switch(opcode(0))
  {
  case F_CLEAR_LOCAL:
    /* CONSTANT CLEAR_LOCAL : CLEAR_LOCAL($2a) CONSTANT($1a) */
    {
      do_optimization(2, 2, 2, F_CONSTANT, argument(1), 2, F_CLEAR_LOCAL, 
                      argument(0), 0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* CONSTANT MARK_X [$2a>0] : MARK_X($2a-1) CONSTANT($1a) */
    if(argument(0)>0)
    {
      do_optimization(2, 2, 2, F_CONSTANT, argument(1), 2, F_MARK_X, 
                      argument(0)-1, 0);
      return 1;
    }
    break;

  case F_POP_VALUE:
    /* CONSTANT POP_VALUE : */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_24(void)
{
  switch(opcode(0))
  {
  case F_ADD:
    /* CONST_1 ADD: ADD_NEG_INT (1) */
    {
      do_optimization(2, 1, 2, F_ADD_NEG_INT, 1, 0);
      return 1;
    }
    break;

  case F_ADD_INT:
    /* CONST_1 ADD_INT : NUMBER($2a-1) */
    {
      do_optimization(2, 1, 2, F_NUMBER, argument(0)-1, 0);
      return 1;
    }
    break;

  case F_ADD_INTS:
    /* NEGATE CONST_1 ADD_INTS : COMPL */
    if(F_NEGATE==opcode(2))
    {
      do_optimization(3, 1, 1, F_COMPL, 0);
      return 1;
    }
    /* CONST_1 ADD_INTS: ADD_NEG_INT (1) */
    {
      do_optimization(2, 1, 2, F_ADD_NEG_INT, 1, 0);
      return 1;
    }
    break;

  case F_ADD_NEG_INT:
    /* CONST_1 ADD_NEG_INT [($2a+1) > 0] : NEG_NUMBER($2a+1) */
    if((argument(0)+1) > 0)
    {
      do_optimization(2, 1, 2, F_NEG_NUMBER, argument(0)+1, 0);
      return 1;
    }
    break;

  case F_ADD_TO:
    /* LOCAL CONST_1 ADD_TO : DEC_LOCAL($1a) */
    if(F_LOCAL==opcode(2))
    {
      do_optimization(3, 1, 2, F_DEC_LOCAL, argument(2), 0);
      return 1;
    }
    break;

  case F_ADD_TO_AND_POP:
    /* LOCAL CONST_1 ADD_TO_AND_POP : DEC_LOCAL_AND_POP($1a) */
    if(F_LOCAL==opcode(2))
    {
      do_optimization(3, 1, 2, F_DEC_LOCAL_AND_POP, argument(2), 0);
      return 1;
    }
    break;

  case F_ASSIGN_GLOBAL_AND_POP:
    /* MARK CONST_1 ASSIGN_GLOBAL_AND_POP: CONST_1 ASSIGN_GLOBAL_AND_POP($3a) 
       MARK */
    if(F_MARK==opcode(2))
    {
      do_optimization(3, 3, 1, F_MARK, 2, F_ASSIGN_GLOBAL_AND_POP, 
                      argument(0), 1, F_CONST_1, 0);
      return 1;
    }
    break;

  case F_ASSIGN_LOCAL_AND_POP:
    /* MARK CONST_1 ASSIGN_LOCAL_AND_POP: CONST_1 ASSIGN_LOCAL_AND_POP($3a) 
       MARK */
    if(F_MARK==opcode(2))
    {
      do_optimization(3, 3, 1, F_MARK, 2, F_ASSIGN_LOCAL_AND_POP, argument(0), 
                      1, F_CONST_1, 0);
      return 1;
    }
    break;

  case F_CLEAR_LOCAL:
    /* CONST_1 CLEAR_LOCAL : CLEAR_LOCAL($2a) CONST_1 */
    {
      do_optimization(2, 2, 1, F_CONST_1, 2, F_CLEAR_LOCAL, argument(0), 0);
      return 1;
    }
    break;

  case F_CLEAR_STRING_SUBTYPE:
    /* CONST_1 CLEAR_STRING_SUBTYPE: CONST_1 */
    {
      do_optimization(2, 1, 1, F_CONST_1, 0);
      return 1;
    }
    break;

  case F_COMPL:
    /* CONST_1 COMPL : CONST0 */
    {
      do_optimization(2, 1, 1, F_CONST0, 0);
      return 1;
    }
    break;

  case F_INDEX:
    /* CONST_1 INDEX: NEG_INT_INDEX (1) */
    {
      do_optimization(2, 1, 2, F_NEG_INT_INDEX, 1, 0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* CONST_1 MARK_X [$2a>0] : MARK_X($2a-1) CONST_1 */
    if(argument(0)>0)
    {
      do_optimization(2, 2, 1, F_CONST_1, 2, F_MARK_X, argument(0)-1, 0);
      return 1;
    }
    break;

  case F_NEGATE:
    /* CONST_1 NEGATE : CONST1 */
    {
      do_optimization(2, 1, 1, F_CONST1, 0);
      return 1;
    }
    break;

  case F_SUBTRACT:
    /* CONST_1 SUBTRACT: ADD_INT (1) */
    {
      do_optimization(2, 1, 2, F_ADD_INT, 1, 0);
      return 1;
    }
    break;

  case F_XOR:
    /* CONST_1 XOR: COMPL */
    {
      do_optimization(2, 1, 1, F_COMPL, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_25(void)
{
  switch(opcode(0))
  {
  case F_LOCAL:
    /* DEC_LOCAL_AND_POP LOCAL ($1a) : DEC_LOCAL ($1a) */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 1, 2, F_DEC_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_MARK_AND_LOCAL:
    /* DEC_LOCAL_AND_POP MARK_AND_LOCAL ($1a) : DEC_LOCAL ($1a) MARK_X(1) */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 2, 2, F_MARK_X, 1, 2, F_DEC_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_26(void)
{
  switch(opcode(0))
  {
  case F_MARK_X:
    /* DUP MARK_X [$2a>0] : MARK_X($2a-1) DUP */
    if(argument(0)>0)
    {
      do_optimization(2, 2, 1, F_DUP, 2, F_MARK_X, argument(0)-1, 0);
      return 1;
    }
    break;

  case F_POP_N_ELEMS:
    /* DUP POP_N_ELEMS [$2a > 0]: POP_N_ELEMS($2a-1) */
    if(argument(0) > 0)
    {
      do_optimization(2, 1, 2, F_POP_N_ELEMS, argument(0)-1, 0);
      return 1;
    }
    break;

  case F_POP_VALUE:
    /* DUP POP_VALUE: */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_27(void)
{
  switch(opcode(0))
  {
  case F_BRANCH_WHEN_NON_ZERO:
    /* EQ BRANCH_WHEN_NON_ZERO: BRANCH_WHEN_EQ ($2a) */
    {
      do_optimization(2, 1, 2, F_BRANCH_WHEN_EQ, argument(0), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_ZERO:
    /* EQ BRANCH_WHEN_ZERO: BRANCH_WHEN_NE ($2a) */
    {
      do_optimization(2, 1, 2, F_BRANCH_WHEN_NE, argument(0), 0);
      return 1;
    }
    break;

  case F_LAND:
    /* EQ LAND: EQ_AND ($2a) */
    {
      do_optimization(2, 1, 2, F_EQ_AND, argument(0), 0);
      return 1;
    }
    break;

  case F_LOR:
    /* EQ LOR: EQ_OR ($2a) */
    {
      do_optimization(2, 1, 2, F_EQ_OR, argument(0), 0);
      return 1;
    }
    break;

  case F_NOT:
    /* EQ NOT: NE */
    {
      do_optimization(2, 1, 1, F_NE, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_28(void)
{
  switch(opcode(0))
  {
  case F_ADD_INTS:
    /* CONST0 GLOBAL ADD_INTS : GLOBAL($2a) */
    if(F_CONST0==opcode(2))
    {
      do_optimization(3, 1, 2, F_GLOBAL, argument(1), 0);
      return 1;
    }
    break;

  case F_ARROW:
    switch(opcode(2))
    {
    case F_BRANCH_WHEN_ZERO:
      /* GLOBAL($1a) ARROW ($2a) BRANCH_WHEN_ZERO GLOBAL($1a) ARROW ($2a) : 
         GLOBAL($1a) ARROW ($2a) BRANCH_AND_POP_WHEN_ZERO ( $3a ) */
      if(F_GLOBAL==opcode(4) && F_ARROW==opcode(3) && 
         (argument(4))==argument(1) && (argument(3))==argument(0))
      {
        do_optimization(5, 3, 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(2), 2, 
                        F_ARROW, argument(3), 2, F_GLOBAL, argument(4), 0);
        return 1;
      }
      break;

    case F_MARK:
      /* GLOBAL($1a) ARROW ($2a) BRANCH_WHEN_ZERO MARK GLOBAL($1a) ARROW ($2a) 
         : GLOBAL($1a) ARROW ($2a) BRANCH_AND_POP_WHEN_ZERO ( $3a ) MARK_X(1)
         */
      if(F_GLOBAL==opcode(5) && F_ARROW==opcode(4) && 
         F_BRANCH_WHEN_ZERO==opcode(3) && (argument(5))==argument(1) && 
         (argument(4))==argument(0))
      {
        do_optimization(6, 4, 2, F_MARK_X, 1, 2, F_BRANCH_AND_POP_WHEN_ZERO, 
                        argument(3), 2, F_ARROW, argument(4), 2, F_GLOBAL, 
                        argument(5), 0);
        return 1;
      }
      break;

    }
    break;

  case F_ASSIGN_GLOBAL_AND_POP:
    /* GLOBAL ASSIGN_GLOBAL_AND_POP($1a) : */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_ASSIGN_LOCAL_AND_POP:
    /* GLOBAL ASSIGN_LOCAL_AND_POP : GLOBAL_2_LOCAL($1a,$2a) */
    {
      do_optimization(2, 1, 3, F_GLOBAL_2_LOCAL, argument(1), argument(0), 0);
      return 1;
    }
    break;

  case F_GLOBAL:
    /* GLOBAL GLOBAL($1a): GLOBAL($1a) DUP */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 2, 1, F_DUP, 2, F_GLOBAL, argument(1), 0);
      return 1;
    }
    break;

  case F_LOCAL:
    switch(opcode(2))
    {
    case F_DEC_LOCAL_AND_POP:
      /* DEC_LOCAL_AND_POP GLOBAL LOCAL ($1a) : GLOBAL($2a) DEC_LOCAL ($1a) */
      if((argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_DEC_LOCAL, argument(2), 2, F_GLOBAL, 
                        argument(1), 0);
        return 1;
      }
      break;

    case F_INC_LOCAL_AND_POP:
      /* INC_LOCAL_AND_POP GLOBAL LOCAL ($1a) : GLOBAL($2a) INC_LOCAL ($1a) */
      if((argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_INC_LOCAL, argument(2), 2, F_GLOBAL, 
                        argument(1), 0);
        return 1;
      }
      break;

    }
    break;

  case F_LOCAL_INDEX:
    /* GLOBAL LOCAL_INDEX : GLOBAL_LOCAL_INDEX($1a,$2a) */
    {
      do_optimization(2, 1, 3, F_GLOBAL_LOCAL_INDEX, argument(1), argument(0), 
                      0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* GLOBAL MARK_X [$2a>0] : MARK_X($2a-1) GLOBAL($1a) */
    if(argument(0)>0)
    {
      do_optimization(2, 2, 2, F_GLOBAL, argument(1), 2, F_MARK_X, 
                      argument(0)-1, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_29(void)
{
  switch(opcode(0))
  {
  case F_DEC_AND_POP:
    /* ASSIGN_GLOBAL_AND_POP GLOBAL_LVALUE($1a) DEC_AND_POP : ADD_NEG_INT(1) 
       ASSIGN_GLOBAL_AND_POP($1a) */
    if(F_ASSIGN_GLOBAL_AND_POP==opcode(2) && (argument(2))==argument(1))
    {
      do_optimization(3, 2, 2, F_ASSIGN_GLOBAL_AND_POP, argument(2), 2, 
                      F_ADD_NEG_INT, 1, 0);
      return 1;
    }
    break;

  case F_INC_AND_POP:
    /* ASSIGN_GLOBAL_AND_POP GLOBAL_LVALUE($1a) INC_AND_POP : ADD_INT(1) 
       ASSIGN_GLOBAL_AND_POP($1a) */
    if(F_ASSIGN_GLOBAL_AND_POP==opcode(2) && (argument(2))==argument(1))
    {
      do_optimization(3, 2, 2, F_ASSIGN_GLOBAL_AND_POP, argument(2), 2, 
                      F_ADD_INT, 1, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_30(void)
{
  switch(opcode(0))
  {
  case F_LOCAL:
    /* INC_LOCAL_AND_POP LOCAL ($1a) : INC_LOCAL ($1a) */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 1, 2, F_INC_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_MARK_AND_LOCAL:
    /* INC_LOCAL_AND_POP MARK_AND_LOCAL ($1a) : INC_LOCAL ($1a) MARK_X(1) */
    if((argument(1))==argument(0))
    {
      do_optimization(2, 2, 2, F_MARK_X, 1, 2, F_INC_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_31(void)
{
  switch(opcode(0))
  {
  case F_CLEAR_2_LOCAL:
    /* BYTE ENTRY START_FUNCTION LABEL(0) CLEAR_2_LOCAL [$1a <= $5a] : 
       BYTE($1a) ENTRY START_FUNCTION LABEL(0) */
    if(F_BYTE==opcode(4) && F_ENTRY==opcode(3) && F_START_FUNCTION==opcode(2) 
       && (0)==argument(1) && argument(4) <= argument(0))
    {
      do_optimization(5, 4, 2, F_LABEL, 0, 1, F_START_FUNCTION, 1, F_ENTRY, 2, 
                      F_BYTE, argument(4), 0);
      return 1;
    }
    break;

  case F_CLEAR_4_LOCAL:
    /* BYTE ENTRY START_FUNCTION LABEL(0) CLEAR_4_LOCAL [$1a <= $5a] : 
       BYTE($1a) ENTRY START_FUNCTION LABEL(0) */
    if(F_BYTE==opcode(4) && F_ENTRY==opcode(3) && F_START_FUNCTION==opcode(2) 
       && (0)==argument(1) && argument(4) <= argument(0))
    {
      do_optimization(5, 4, 2, F_LABEL, 0, 1, F_START_FUNCTION, 1, F_ENTRY, 2, 
                      F_BYTE, argument(4), 0);
      return 1;
    }
    break;

  case F_CLEAR_LOCAL:
    /* BYTE ENTRY START_FUNCTION LABEL(0) CLEAR_LOCAL [$1a <= $5a] : BYTE($1a) 
       ENTRY START_FUNCTION LABEL(0) */
    if(F_BYTE==opcode(4) && F_ENTRY==opcode(3) && F_START_FUNCTION==opcode(2) 
       && (0)==argument(1) && argument(4) <= argument(0))
    {
      do_optimization(5, 4, 2, F_LABEL, 0, 1, F_START_FUNCTION, 1, F_ENTRY, 2, 
                      F_BYTE, argument(4), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_32(void)
{
  switch(opcode(0))
  {
  case F_ADD_INTS:
    /* CONST0 LOCAL ADD_INTS : LOCAL($2a) */
    if(F_CONST0==opcode(2))
    {
      do_optimization(3, 1, 2, F_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_ARROW:
    /* LOCAL ARROW : LOCAL_ARROW($2a,$1a) */
    {
      do_optimization(2, 1, 3, F_LOCAL_ARROW, argument(0), argument(1), 0);
      return 1;
    }
    break;

  case F_ASSIGN_GLOBAL_AND_POP:
    /* LOCAL ASSIGN_GLOBAL_AND_POP : LOCAL_2_GLOBAL($2a,$1a) */
    {
      do_optimization(2, 1, 3, F_LOCAL_2_GLOBAL, argument(0), argument(1), 0);
      return 1;
    }
    break;

  case F_ASSIGN_LOCAL_AND_POP:
    /* LOCAL ASSIGN_LOCAL_AND_POP : LOCAL_2_LOCAL($2a,$1a) */
    {
      do_optimization(2, 1, 3, F_LOCAL_2_LOCAL, argument(0), argument(1), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_NON_ZERO:
    /* LOCAL BRANCH_WHEN_NON_ZERO : BRANCH_IF_LOCAL($1a) POINTER($2a) */
    {
      do_optimization(2, 2, 2, F_POINTER, argument(0), 2, F_BRANCH_IF_LOCAL, 
                      argument(1), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_ZERO:
    /* LOCAL BRANCH_WHEN_ZERO : BRANCH_IF_NOT_LOCAL($1a) POINTER($2a) */
    {
      do_optimization(2, 2, 2, F_POINTER, argument(0), 2, 
                      F_BRANCH_IF_NOT_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_INDEX:
    /* LOCAL INDEX: LOCAL_INDEX ($1a) */
    {
      do_optimization(2, 1, 2, F_LOCAL_INDEX, argument(1), 0);
      return 1;
    }
    break;

  case F_LOCAL:
    switch(opcode(2))
    {
    case F_DEC_LOCAL_AND_POP:
      /* DEC_LOCAL_AND_POP LOCAL LOCAL ($1a) : LOCAL($2a) DEC_LOCAL ($1a) */
      if((argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_DEC_LOCAL, argument(2), 2, F_LOCAL, 
                        argument(1), 0);
        return 1;
      }
      break;

    case F_INC_LOCAL_AND_POP:
      /* INC_LOCAL_AND_POP LOCAL LOCAL ($1a) : LOCAL($2a) INC_LOCAL ($1a) */
      if((argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_INC_LOCAL, argument(2), 2, F_LOCAL, 
                        argument(1), 0);
        return 1;
      }
      break;

    }
    /* LOCAL LOCAL : 2_LOCALS ($1a,$2a) */
    {
      do_optimization(2, 1, 3, F_2_LOCALS, argument(1), argument(0), 0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* LOCAL MARK_X [$2a>0] : MARK_X($2a-1) LOCAL($1a) */
    if(argument(0)>0)
    {
      do_optimization(2, 2, 2, F_LOCAL, argument(1), 2, F_MARK_X, 
                      argument(0)-1, 0);
      return 1;
    }
    break;

  case F_POP_VALUE:
    /* LOCAL POP_VALUE : */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* LOCAL RETURN : RETURN_LOCAL($1a) */
    {
      do_optimization(2, 1, 2, F_RETURN_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_SIZEOF:
    /* LOCAL SIZEOF: SIZEOF_LOCAL ($1a) */
    {
      do_optimization(2, 1, 2, F_SIZEOF_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_SWITCH:
    /* LOCAL SWITCH: SWITCH_ON_LOCAL($1a,$2a) */
    {
      do_optimization(2, 1, 3, F_SWITCH_ON_LOCAL, argument(1), argument(0), 0);
      return 1;
    }
    break;

  case F_VOLATILE_RETURN:
    /* LOCAL VOLATILE_RETURN : RETURN_LOCAL($1a) */
    {
      do_optimization(2, 1, 2, F_RETURN_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_33(void)
{
  switch(opcode(0))
  {
  case F_ARROW:
    switch(opcode(2))
    {
    case F_BRANCH_WHEN_ZERO:
      /* LOCAL_ARROW($1a, $1b) ARROW($2a) BRANCH_WHEN_ZERO LOCAL_ARROW($1a, 
         $1b) ARROW($2a) : LOCAL_ARROW($1a, $1b) ARROW($2a) 
         BRANCH_AND_POP_WHEN_ZERO ( $3a ) */
      if(F_LOCAL_ARROW==opcode(4) && F_ARROW==opcode(3) && 
         (argument(4))==argument(1) && (argument2(4))==argument2(1) && 
         (argument(3))==argument(0))
      {
        do_optimization(5, 3, 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(2), 2, 
                        F_ARROW, argument(3), 3, F_LOCAL_ARROW, argument(4), 
                        argument2(4), 0);
        return 1;
      }
      break;

    case F_MARK:
      /* LOCAL_ARROW($1a, $1b) ARROW($2a) BRANCH_WHEN_ZERO MARK 
         LOCAL_ARROW($1a, $1b) ARROW($2a) : LOCAL_ARROW($1a, $1b) ARROW($2a) 
         BRANCH_AND_POP_WHEN_ZERO ( $3a ) MARK_X(1) */
      if(F_LOCAL_ARROW==opcode(5) && F_ARROW==opcode(4) && 
         F_BRANCH_WHEN_ZERO==opcode(3) && (argument(5))==argument(1) && 
         (argument2(5))==argument2(1) && (argument(4))==argument(0))
      {
        do_optimization(6, 4, 2, F_MARK_X, 1, 2, F_BRANCH_AND_POP_WHEN_ZERO, 
                        argument(3), 2, F_ARROW, argument(4), 3, 
                        F_LOCAL_ARROW, argument(5), argument2(5), 0);
        return 1;
      }
      break;

    }
    break;

  case F_BRANCH_WHEN_ZERO:
    /* LOCAL_ARROW BRANCH_WHEN_ZERO: BRANCH_IF_NOT_LOCAL_ARROW($1a,$1b) 
       POINTER($2a) */
    {
      do_optimization(2, 2, 2, F_POINTER, argument(0), 3, 
                      F_BRANCH_IF_NOT_LOCAL_ARROW, argument(1), argument2(1), 
                      0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_34(void)
{
  switch(opcode(0))
  {
  case F_DEC:
    /* LOCAL_LVALUE DEC : DEC_LOCAL ($1a) */
    {
      do_optimization(2, 1, 2, F_DEC_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_DEC_AND_POP:
    /* LOCAL_LVALUE DEC_AND_POP : DEC_LOCAL_AND_POP ($1a) */
    {
      do_optimization(2, 1, 2, F_DEC_LOCAL_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_INC:
    /* LOCAL_LVALUE INC : INC_LOCAL ($1a) */
    {
      do_optimization(2, 1, 2, F_INC_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_INC_AND_POP:
    /* LOCAL_LVALUE INC_AND_POP : INC_LOCAL_AND_POP ($1a) */
    {
      do_optimization(2, 1, 2, F_INC_LOCAL_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* LOCAL_LVALUE MARK_X [$2a>1] : MARK_X($2a-2) LOCAL_LVALUE($1a) */
    if(argument(0)>1)
    {
      do_optimization(2, 2, 2, F_LOCAL_LVALUE, argument(1), 2, F_MARK_X, 
                      argument(0)-2, 0);
      return 1;
    }
    break;

  case F_POST_DEC:
    /* LOCAL_LVALUE POST_DEC : POST_DEC_LOCAL ($1a) */
    {
      do_optimization(2, 1, 2, F_POST_DEC_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_POST_INC:
    /* LOCAL_LVALUE POST_INC : POST_INC_LOCAL ($1a) */
    {
      do_optimization(2, 1, 2, F_POST_INC_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_35(void)
{
  switch(opcode(0))
  {
  case F_APPLY:
    /* MARK APPLY : MARK_APPLY ($2a) */
    {
      do_optimization(2, 1, 2, F_MARK_APPLY, argument(0), 0);
      return 1;
    }
    break;

  case F_CALL_BUILTIN:
    /* MARK CALL_BUILTIN : MARK_CALL_BUILTIN ($2a) */
    {
      do_optimization(2, 1, 2, F_MARK_CALL_BUILTIN, argument(0), 0);
      return 1;
    }
    break;

  case F_CALL_LFUN:
    /* MARK CALL_LFUN : MARK_CALL_LFUN ($2a) */
    {
      do_optimization(2, 1, 2, F_MARK_CALL_LFUN, argument(0), 0);
      return 1;
    }
    break;

  case F_CLEAR_LOCAL:
    /* MARK CLEAR_LOCAL: CLEAR_LOCAL($2a) MARK */
    {
      do_optimization(2, 2, 1, F_MARK, 2, F_CLEAR_LOCAL, argument(0), 0);
      return 1;
    }
    break;

  case F_CONST0:
    /* MARK CONST0 : MARK_AND_CONST0 */
    {
      do_optimization(2, 1, 1, F_MARK_AND_CONST0, 0);
      return 1;
    }
    break;

  case F_CONST1:
    /* MARK CONST1 : MARK_AND_CONST1 */
    {
      do_optimization(2, 1, 1, F_MARK_AND_CONST1, 0);
      return 1;
    }
    break;

  case F_EXTERNAL:
    /* MARK EXTERNAL : MARK_AND_EXTERNAL($2a,$2b) */
    {
      do_optimization(2, 1, 3, F_MARK_AND_EXTERNAL, argument(0), argument2(0), 
                      0);
      return 1;
    }
    break;

  case F_GLOBAL:
    switch(opcode(2))
    {
    case F_ASSIGN_GLOBAL_AND_POP:
      /* ASSIGN_GLOBAL_AND_POP MARK GLOBAL($1a) : ASSIGN_GLOBAL($1a) MARK_X(1)
         */
      if((argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_MARK_X, 1, 2, F_ASSIGN_GLOBAL, argument(2), 
                        0);
        return 1;
      }
      break;

    case F_BRANCH_WHEN_NON_ZERO:
      switch(opcode(3))
      {
      case F_ASSIGN_GLOBAL:
        /* ASSIGN_GLOBAL BRANCH_WHEN_NON_ZERO MARK GLOBAL ($1a) : ASSIGN_GLOBAL
           ($1a) BRANCH_AND_POP_WHEN_NON_ZERO ($2a) MARK_X(1) */
        if((argument(3))==argument(0))
        {
          do_optimization(4, 3, 2, F_MARK_X, 1, 2, 
                          F_BRANCH_AND_POP_WHEN_NON_ZERO, argument(2), 2, 
                          F_ASSIGN_GLOBAL, argument(3), 0);
          return 1;
        }
        break;

      case F_GLOBAL:
        /* GLOBAL BRANCH_WHEN_NON_ZERO MARK GLOBAL ($1a) : GLOBAL ($1a) 
           BRANCH_AND_POP_WHEN_NON_ZERO ($2a) MARK_X(1) */
        if((argument(3))==argument(0))
        {
          do_optimization(4, 3, 2, F_MARK_X, 1, 2, 
                          F_BRANCH_AND_POP_WHEN_NON_ZERO, argument(2), 2, 
                          F_GLOBAL, argument(3), 0);
          return 1;
        }
        break;

      }
      break;

    case F_BRANCH_WHEN_ZERO:
      switch(opcode(3))
      {
      case F_ASSIGN_GLOBAL:
        /* ASSIGN_GLOBAL BRANCH_WHEN_ZERO MARK GLOBAL ($1a) : ASSIGN_GLOBAL 
           ($1a) BRANCH_AND_POP_WHEN_ZERO ($2a) MARK_X(1) */
        if((argument(3))==argument(0))
        {
          do_optimization(4, 3, 2, F_MARK_X, 1, 2, F_BRANCH_AND_POP_WHEN_ZERO, 
                          argument(2), 2, F_ASSIGN_GLOBAL, argument(3), 0);
          return 1;
        }
        break;

      case F_GLOBAL:
        /* GLOBAL BRANCH_WHEN_ZERO MARK GLOBAL ($1a) : GLOBAL ($1a) 
           BRANCH_AND_POP_WHEN_ZERO ($2a) MARK_X(1) */
        if((argument(3))==argument(0))
        {
          do_optimization(4, 3, 2, F_MARK_X, 1, 2, F_BRANCH_AND_POP_WHEN_ZERO, 
                          argument(2), 2, F_GLOBAL, argument(3), 0);
          return 1;
        }
        break;

      }
      break;

    }
    /* MARK GLOBAL: MARK_AND_GLOBAL ($2a) */
    {
      do_optimization(2, 1, 2, F_MARK_AND_GLOBAL, argument(0), 0);
      return 1;
    }
    break;

  case F_GLOBAL_LOCAL_INDEX:
    /* GLOBAL_LOCAL_INDEX($1a, $1b) BRANCH_WHEN_ZERO MARK 
       GLOBAL_LOCAL_INDEX($1a, $1b) : GLOBAL_LOCAL_INDEX($1a, $1b) 
       BRANCH_AND_POP_WHEN_ZERO ( $2a ) MARK_X(1) */
    if(F_GLOBAL_LOCAL_INDEX==opcode(3) && F_BRANCH_WHEN_ZERO==opcode(2) && 
       (argument(3))==argument(0) && (argument2(3))==argument2(0))
    {
      do_optimization(4, 3, 2, F_MARK_X, 1, 2, F_BRANCH_AND_POP_WHEN_ZERO, 
                      argument(2), 3, F_GLOBAL_LOCAL_INDEX, argument(3), 
                      argument2(3), 0);
      return 1;
    }
    break;

  case F_LOCAL:
    switch(opcode(2))
    {
    case F_BRANCH_WHEN_NON_ZERO:
      switch(opcode(3))
      {
      case F_ASSIGN_LOCAL:
        /* ASSIGN_LOCAL BRANCH_WHEN_NON_ZERO MARK LOCAL ($1a) : ASSIGN_LOCAL 
           ($1a) BRANCH_AND_POP_WHEN_NON_ZERO ($2a) MARK_X(1) */
        if((argument(3))==argument(0))
        {
          do_optimization(4, 3, 2, F_MARK_X, 1, 2, 
                          F_BRANCH_AND_POP_WHEN_NON_ZERO, argument(2), 2, 
                          F_ASSIGN_LOCAL, argument(3), 0);
          return 1;
        }
        break;

      case F_LOCAL:
        /* LOCAL BRANCH_WHEN_NON_ZERO MARK LOCAL ($1a) : LOCAL ($1a) 
           BRANCH_AND_POP_WHEN_NON_ZERO ($2a) MARK_X(1) */
        if((argument(3))==argument(0))
        {
          do_optimization(4, 3, 2, F_MARK_X, 1, 2, 
                          F_BRANCH_AND_POP_WHEN_NON_ZERO, argument(2), 2, 
                          F_LOCAL, argument(3), 0);
          return 1;
        }
        break;

      }
      break;

    case F_BRANCH_WHEN_ZERO:
      switch(opcode(3))
      {
      case F_ASSIGN_LOCAL:
        /* ASSIGN_LOCAL BRANCH_WHEN_ZERO MARK LOCAL ($1a) : ASSIGN_LOCAL ($1a) 
           BRANCH_AND_POP_WHEN_ZERO ($2a) MARK_X(1) */
        if((argument(3))==argument(0))
        {
          do_optimization(4, 3, 2, F_MARK_X, 1, 2, F_BRANCH_AND_POP_WHEN_ZERO, 
                          argument(2), 2, F_ASSIGN_LOCAL, argument(3), 0);
          return 1;
        }
        break;

      case F_LOCAL:
        /* LOCAL BRANCH_WHEN_ZERO MARK LOCAL ($1a) : LOCAL ($1a) 
           BRANCH_AND_POP_WHEN_ZERO ($2a) MARK_X(1) */
        if((argument(3))==argument(0))
        {
          do_optimization(4, 3, 2, F_MARK_X, 1, 2, F_BRANCH_AND_POP_WHEN_ZERO, 
                          argument(2), 2, F_LOCAL, argument(3), 0);
          return 1;
        }
        break;

      }
      break;

    case F_CLEAR_LOCAL:
      /* CLEAR_LOCAL MARK LOCAL [ $1a != $3a ] : MARK LOCAL($3a) 
         CLEAR_LOCAL($1a) */
      if( argument(2) != argument(0) )
      {
        do_optimization(3, 3, 2, F_CLEAR_LOCAL, argument(2), 2, F_LOCAL, 
                        argument(0), 1, F_MARK, 0);
        return 1;
      }
      /* CLEAR_LOCAL MARK LOCAL($1a) : MARK CLEAR_LOCAL($1a) CONST0 */
      if((argument(2))==argument(0))
      {
        do_optimization(3, 3, 1, F_CONST0, 2, F_CLEAR_LOCAL, argument(2), 1, 
                        F_MARK, 0);
        return 1;
      }
      break;

    }
    /* MARK LOCAL : MARK_AND_LOCAL ($2a) */
    {
      do_optimization(2, 1, 2, F_MARK_AND_LOCAL, argument(0), 0);
      return 1;
    }
    break;

  case F_LOCAL_ARROW:
    /* LOCAL_ARROW($1a, $1b) BRANCH_WHEN_ZERO MARK LOCAL_ARROW($1a, $1b) : 
       LOCAL_ARROW($1a, $1b) BRANCH_AND_POP_WHEN_ZERO ( $2a ) MARK_X(1) */
    if(F_LOCAL_ARROW==opcode(3) && F_BRANCH_WHEN_ZERO==opcode(2) && 
       (argument(3))==argument(0) && (argument2(3))==argument2(0))
    {
      do_optimization(4, 3, 2, F_MARK_X, 1, 2, F_BRANCH_AND_POP_WHEN_ZERO, 
                      argument(2), 3, F_LOCAL_ARROW, argument(3), 
                      argument2(3), 0);
      return 1;
    }
    break;

  case F_LOCAL_LOCAL_INDEX:
    /* LOCAL_LOCAL_INDEX($1a, $1b) BRANCH_WHEN_ZERO MARK LOCAL_LOCAL_INDEX($1a,
       $1b) : LOCAL_LOCAL_INDEX($1a, $1b) BRANCH_AND_POP_WHEN_ZERO ( $2a ) 
       MARK_X(1) */
    if(F_LOCAL_LOCAL_INDEX==opcode(3) && F_BRANCH_WHEN_ZERO==opcode(2) && 
       (argument(3))==argument(0) && (argument2(3))==argument2(0))
    {
      do_optimization(4, 3, 2, F_MARK_X, 1, 2, F_BRANCH_AND_POP_WHEN_ZERO, 
                      argument(2), 3, F_LOCAL_LOCAL_INDEX, argument(3), 
                      argument2(3), 0);
      return 1;
    }
    break;

  case F_MARK:
    /* MARK MARK: MARK2 */
    {
      do_optimization(2, 1, 1, F_MARK2, 0);
      return 1;
    }
    break;

  case F_POP_MARK:
    /* MARK POP_MARK: */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_STRING:
    /* MARK STRING : MARK_AND_STRING($2a) */
    {
      do_optimization(2, 1, 2, F_MARK_AND_STRING, argument(0), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_36(void)
{
  switch(opcode(0))
  {
  case F_ASSIGN_GLOBAL_AND_POP:
    /* MARK_AND_CONST1 ASSIGN_GLOBAL_AND_POP: CONST1 ASSIGN_GLOBAL_AND_POP($3a)
       MARK */
    {
      do_optimization(2, 3, 1, F_MARK, 2, F_ASSIGN_GLOBAL_AND_POP, 
                      argument(-1), 1, F_CONST1, 0);
      return 1;
    }
    break;

  case F_ASSIGN_LOCAL_AND_POP:
    /* MARK_AND_CONST1 ASSIGN_LOCAL_AND_POP: CONST1 ASSIGN_LOCAL_AND_POP($3a) 
       MARK */
    {
      do_optimization(2, 3, 1, F_MARK, 2, F_ASSIGN_LOCAL_AND_POP, 
                      argument(-1), 1, F_CONST1, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_37(void)
{
  switch(opcode(0))
  {
  case F_INDEX:
    /* MARK_AND_LOCAL INDEX: MARK LOCAL_INDEX ($1a) */
    {
      do_optimization(2, 2, 2, F_LOCAL_INDEX, argument(1), 1, F_MARK, 0);
      return 1;
    }
    break;

  case F_SIZEOF:
    /* MARK_AND_LOCAL SIZEOF: MARK SIZEOF_LOCAL ($1a) */
    {
      do_optimization(2, 2, 2, F_SIZEOF_LOCAL, argument(1), 1, F_MARK, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_38(void)
{
  switch(opcode(0))
  {
  case F_MARK_X:
    /* MARK_APPLY MARK_X [$2a>0] : MARK_X($2a-1) MARK_APPLY($1a) */
    if(argument(0)>0)
    {
      do_optimization(2, 2, 2, F_MARK_APPLY, argument(1), 2, F_MARK_X, 
                      argument(0)-1, 0);
      return 1;
    }
    break;

  case F_POP_VALUE:
    /* MARK_APPLY POP_VALUE: MARK_APPLY_AND_POP($1a) */
    {
      do_optimization(2, 1, 2, F_MARK_APPLY_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* MARK_APPLY [ check_tailrecursion() ] RETURN : MARK_APPLY_AND_RETURN($1a)
       */
    if( check_tailrecursion() )
    {
      do_optimization(2, 1, 2, F_MARK_APPLY_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_39(void)
{
  switch(opcode(0))
  {
  case F_POP_VALUE:
    /* MARK_CALL_BUILTIN POP_VALUE: MARK_CALL_BUILTIN_AND_POP($1a) */
    {
      do_optimization(2, 1, 2, F_MARK_CALL_BUILTIN_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* MARK_CALL_BUILTIN [ check_tailrecursion() ] RETURN : 
       MARK_CALL_BUILTIN_AND_RETURN($1a) */
    if( check_tailrecursion() )
    {
      do_optimization(2, 1, 2, F_MARK_CALL_BUILTIN_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_40(void)
{
  switch(opcode(0))
  {
  case F_POP_VALUE:
    /* MARK_CALL_LFUN POP_VALUE: MARK_CALL_LFUN_AND_POP($1a) */
    {
      do_optimization(2, 1, 2, F_MARK_CALL_LFUN_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* MARK_CALL_LFUN [ check_tailrecursion() ] RETURN : 
       MARK_CALL_LFUN_AND_RETURN($1a) */
    if( check_tailrecursion() )
    {
      do_optimization(2, 1, 2, F_MARK_CALL_LFUN_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_41(void)
{
  switch(opcode(0))
  {
  case F_POP_N_ELEMS:
    /* MARK_X POP_N_ELEMS [$1a >= $2a] : POP_N_ELEMS($2a) MARK_X($1a-$2a) */
    if(argument(1) >= argument(0))
    {
      do_optimization(2, 2, 2, F_MARK_X, argument(1)-argument(0), 2, 
                      F_POP_N_ELEMS, argument(0), 0);
      return 1;
    }
    break;

  case F_POP_VALUE:
    /* MARK_X [$1a>0] POP_VALUE : POP_VALUE MARK_X($1a-1) */
    if(argument(1)>0)
    {
      do_optimization(2, 2, 2, F_MARK_X, argument(1)-1, 1, F_POP_VALUE, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_42(void)
{
  switch(opcode(0))
  {
  case F_BRANCH_WHEN_NON_ZERO:
    /* NE BRANCH_WHEN_NON_ZERO: BRANCH_WHEN_NE ($2a) */
    {
      do_optimization(2, 1, 2, F_BRANCH_WHEN_NE, argument(0), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_ZERO:
    /* NE BRANCH_WHEN_ZERO: BRANCH_WHEN_EQ ($2a) */
    {
      do_optimization(2, 1, 2, F_BRANCH_WHEN_EQ, argument(0), 0);
      return 1;
    }
    break;

  case F_NOT:
    /* NE NOT: EQ */
    {
      do_optimization(2, 1, 1, F_EQ, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_43(void)
{
  switch(opcode(0))
  {
  case F_ADD_INTS:
    switch(opcode(2))
    {
    case F_GLOBAL:
      /* CONST0 GLOBAL NEGATE ADD_INTS : GLOBAL($2a) NEGATE */
      if(F_CONST0==opcode(3))
      {
        do_optimization(4, 2, 1, F_NEGATE, 2, F_GLOBAL, argument(2), 0);
        return 1;
      }
      break;

    case F_LOCAL:
      /* CONST0 LOCAL NEGATE ADD_INTS : LOCAL($2a) NEGATE */
      if(F_CONST0==opcode(3))
      {
        do_optimization(4, 2, 1, F_NEGATE, 2, F_LOCAL, argument(2), 0);
        return 1;
      }
      break;

    }
    break;

  case F_NEGATE:
    /* NEGATE NEGATE : */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_44(void)
{
  switch(opcode(0))
  {
  case F_ADD:
    /* NEG_NUMBER [$1a > 0] ADD : ADD_NEG_INT ($1a) */
    if(argument(1) > 0)
    {
      do_optimization(2, 1, 2, F_ADD_NEG_INT, argument(1), 0);
      return 1;
    }
    break;

  case F_ADD_INT:
    /* NEG_NUMBER ADD_INT [ !INT32_ADD_OVERFLOW(-$1a, $2a) ]: NUMBER(-$1a+$2a)
       */
    if( !INT32_ADD_OVERFLOW(-argument(1), argument(0)) )
    {
      do_optimization(2, 1, 2, F_NUMBER, -argument(1)+argument(0), 0);
      return 1;
    }
    break;

  case F_ADD_INTS:
    /* NEG_NUMBER [$1a > 0] ADD_INTS : ADD_NEG_INT ($1a) */
    if(argument(1) > 0)
    {
      do_optimization(2, 1, 2, F_ADD_NEG_INT, argument(1), 0);
      return 1;
    }
    break;

  case F_ADD_NEG_INT:
    /* NEG_NUMBER ADD_NEG_INT [ !INT32_SUB_OVERFLOW(-$1a, $2a) ]: 
       NUMBER(-$1a-$2a) */
    if( !INT32_SUB_OVERFLOW(-argument(1), argument(0)) )
    {
      do_optimization(2, 1, 2, F_NUMBER, -argument(1)-argument(0), 0);
      return 1;
    }
    break;

  case F_ASSIGN_GLOBAL_AND_POP:
    /* MARK NEG_NUMBER ASSIGN_GLOBAL_AND_POP: NEG_NUMBER($2a) 
       ASSIGN_GLOBAL_AND_POP($3a) MARK */
    if(F_MARK==opcode(2))
    {
      do_optimization(3, 3, 1, F_MARK, 2, F_ASSIGN_GLOBAL_AND_POP, 
                      argument(0), 2, F_NEG_NUMBER, argument(1), 0);
      return 1;
    }
    break;

  case F_ASSIGN_LOCAL_AND_POP:
    /* MARK NEG_NUMBER ASSIGN_LOCAL_AND_POP: NEG_NUMBER($2a) 
       ASSIGN_LOCAL_AND_POP($3a) MARK */
    if(F_MARK==opcode(2))
    {
      do_optimization(3, 3, 1, F_MARK, 2, F_ASSIGN_LOCAL_AND_POP, argument(0), 
                      2, F_NEG_NUMBER, argument(1), 0);
      return 1;
    }
    break;

  case F_CLEAR_LOCAL:
    /* NEG_NUMBER CLEAR_LOCAL : CLEAR_LOCAL($2a) NEG_NUMBER($1a) */
    {
      do_optimization(2, 2, 2, F_NEG_NUMBER, argument(1), 2, F_CLEAR_LOCAL, 
                      argument(0), 0);
      return 1;
    }
    break;

  case F_CLEAR_STRING_SUBTYPE:
    /* NEG_NUMBER CLEAR_STRING_SUBTYPE: NEG_NUMBER($1a) */
    {
      do_optimization(2, 1, 2, F_NEG_NUMBER, argument(1), 0);
      return 1;
    }
    break;

  case F_INDEX:
    /* NEG_NUMBER INDEX: NEG_INT_INDEX ($1a) */
    {
      do_optimization(2, 1, 2, F_NEG_INT_INDEX, argument(1), 0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* NEG_NUMBER MARK_X [$2a>0] : MARK_X($2a-1) NEG_NUMBER($1a) */
    if(argument(0)>0)
    {
      do_optimization(2, 2, 2, F_NEG_NUMBER, argument(1), 2, F_MARK_X, 
                      argument(0)-1, 0);
      return 1;
    }
    break;

  case F_NEGATE:
    /* NEG_NUMBER NEGATE : NUMBER ($1a) */
    {
      do_optimization(2, 1, 2, F_NUMBER, argument(1), 0);
      return 1;
    }
    break;

  case F_SUBTRACT:
    /* NEG_NUMBER [$1a > 0] SUBTRACT : ADD_INT ($1a) */
    if(argument(1) > 0)
    {
      do_optimization(2, 1, 2, F_ADD_INT, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_45(void)
{
  switch(opcode(0))
  {
  case F_BRANCH_WHEN_NON_ZERO:
    /* NOT BRANCH_WHEN_NON_ZERO: BRANCH_WHEN_ZERO($2a) */
    {
      do_optimization(2, 1, 2, F_BRANCH_WHEN_ZERO, argument(0), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_ZERO:
    /* NOT BRANCH_WHEN_ZERO: BRANCH_WHEN_NON_ZERO($2a) */
    {
      do_optimization(2, 1, 2, F_BRANCH_WHEN_NON_ZERO, argument(0), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_46(void)
{
  switch(opcode(0))
  {
  case F_ADD:
    /* NUMBER [$1a >= 0] ADD : ADD_INT ($1a) */
    if(argument(1) >= 0)
    {
      do_optimization(2, 1, 2, F_ADD_INT, argument(1), 0);
      return 1;
    }
    break;

  case F_ADD_INT:
    /* NUMBER ADD_INT [ !INT32_ADD_OVERFLOW($1a, $2a) ] : NUMBER($1a+$2a) */
    if( !INT32_ADD_OVERFLOW(argument(1), argument(0)) )
    {
      do_optimization(2, 1, 2, F_NUMBER, argument(1)+argument(0), 0);
      return 1;
    }
    break;

  case F_ADD_INTS:
    /* NUMBER [$1a >= 0] ADD_INTS : ADD_INT ($1a) */
    if(argument(1) >= 0)
    {
      do_optimization(2, 1, 2, F_ADD_INT, argument(1), 0);
      return 1;
    }
    break;

  case F_ADD_NEG_INT:
    /* NUMBER ADD_NEG_INT [ !INT32_SUB_OVERFLOW($1a, $2a) ]: NUMBER($1a-$2a) */
    if( !INT32_SUB_OVERFLOW(argument(1), argument(0)) )
    {
      do_optimization(2, 1, 2, F_NUMBER, argument(1)-argument(0), 0);
      return 1;
    }
    break;

  case F_ASSIGN_GLOBAL_AND_POP:
    /* MARK NUMBER ASSIGN_GLOBAL_AND_POP: NUMBER($2a) 
       ASSIGN_GLOBAL_AND_POP($3a) MARK */
    if(F_MARK==opcode(2))
    {
      do_optimization(3, 3, 1, F_MARK, 2, F_ASSIGN_GLOBAL_AND_POP, 
                      argument(0), 2, F_NUMBER, argument(1), 0);
      return 1;
    }
    break;

  case F_ASSIGN_LOCAL_AND_POP:
    /* MARK NUMBER ASSIGN_LOCAL_AND_POP: NUMBER($2a) ASSIGN_LOCAL_AND_POP($3a) 
       MARK */
    if(F_MARK==opcode(2))
    {
      do_optimization(3, 3, 1, F_MARK, 2, F_ASSIGN_LOCAL_AND_POP, argument(0), 
                      2, F_NUMBER, argument(1), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_NON_ZERO:
    /* NUMBER [$1a] BRANCH_WHEN_NON_ZERO: BRANCH($2a) */
    if(argument(1))
    {
      do_optimization(2, 1, 2, F_BRANCH, argument(0), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_ZERO:
    /* NUMBER [$1a] BRANCH_WHEN_ZERO: */
    if(argument(1))
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_CLEAR_LOCAL:
    /* NUMBER CLEAR_LOCAL : CLEAR_LOCAL($2a) NUMBER($1a) */
    {
      do_optimization(2, 2, 2, F_NUMBER, argument(1), 2, F_CLEAR_LOCAL, 
                      argument(0), 0);
      return 1;
    }
    break;

  case F_CLEAR_STRING_SUBTYPE:
    /* NUMBER CLEAR_STRING_SUBTYPE: NUMBER($1a) */
    {
      do_optimization(2, 1, 2, F_NUMBER, argument(1), 0);
      return 1;
    }
    break;

  case F_INDEX:
    /* NUMBER INDEX: POS_INT_INDEX ($1a) */
    {
      do_optimization(2, 1, 2, F_POS_INT_INDEX, argument(1), 0);
      return 1;
    }
    break;

  case F_LOCAL:
    /* ASSIGN_LOCAL_AND_POP NUMBER LOCAL($1a) : ASSIGN_LOCAL($1a) NUMBER($2a) 
       SWAP */
    if(F_ASSIGN_LOCAL_AND_POP==opcode(2) && (argument(2))==argument(0))
    {
      do_optimization(3, 3, 1, F_SWAP, 2, F_NUMBER, argument(1), 2, 
                      F_ASSIGN_LOCAL, argument(2), 0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* NUMBER MARK_X [$2a>0] : MARK_X($2a-1) NUMBER($1a) */
    if(argument(0)>0)
    {
      do_optimization(2, 2, 2, F_NUMBER, argument(1), 2, F_MARK_X, 
                      argument(0)-1, 0);
      return 1;
    }
    break;

  case F_NEGATE:
    /* NUMBER NEGATE : NEG_NUMBER($1a) */
    {
      do_optimization(2, 1, 2, F_NEG_NUMBER, argument(1), 0);
      return 1;
    }
    break;

  case F_POP_VALUE:
    /* NUMBER POP_VALUE : */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_SUBTRACT:
    /* NUMBER [$1a >= 0] SUBTRACT : ADD_NEG_INT ($1a) */
    if(argument(1) >= 0)
    {
      do_optimization(2, 1, 2, F_ADD_NEG_INT, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_47(void)
{
  switch(opcode(2))
  {
  case F_BRANCH_IF_LOCAL:
    /* BRANCH_IF_LOCAL POINTER LABEL($2a) : LABEL($2a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(3, 1, 2, F_LABEL, argument(1), 0);
      return 1;
    }
    break;

  case F_BRANCH_IF_NOT_LOCAL:
    /* BRANCH_IF_NOT_LOCAL POINTER LABEL($2a) : LABEL($2a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(3, 1, 2, F_LABEL, argument(1), 0);
      return 1;
    }
    break;

  case F_BRANCH_IF_NOT_LOCAL_ARROW:
    /* BRANCH_IF_NOT_LOCAL_ARROW POINTER LABEL ($2a) : LABEL($2a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(3, 1, 2, F_LABEL, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_48(void)
{
  switch(opcode(0))
  {
  case F_POP_N_ELEMS:
    /* POP_N_ELEMS POP_N_ELEMS : POP_N_ELEMS ($1a + $2a) */
    {
      do_optimization(2, 1, 2, F_POP_N_ELEMS, argument(1) + argument(0), 0);
      return 1;
    }
    break;

  case F_POP_TO_MARK:
    /* POP_N_ELEMS POP_TO_MARK : POP_TO_MARK */
    {
      do_optimization(2, 1, 1, F_POP_TO_MARK, 0);
      return 1;
    }
    break;

  case F_POP_VALUE:
    /* POP_N_ELEMS POP_VALUE : POP_N_ELEMS ($1a + 1) */
    {
      do_optimization(2, 1, 2, F_POP_N_ELEMS, argument(1) + 1, 0);
      return 1;
    }
    break;

  case F_RETURN_0:
    /* POP_N_ELEMS RETURN_0: RETURN_0 */
    {
      do_optimization(2, 1, 1, F_RETURN_0, 0);
      return 1;
    }
    break;

  case F_RETURN_1:
    /* POP_N_ELEMS RETURN_1: RETURN_1 */
    {
      do_optimization(2, 1, 1, F_RETURN_1, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_49(void)
{
  switch(opcode(0))
  {
  case F_POP_N_ELEMS:
    /* POP_VALUE POP_N_ELEMS : POP_N_ELEMS ($2a + 1) */
    {
      do_optimization(2, 1, 2, F_POP_N_ELEMS, argument(0) + 1, 0);
      return 1;
    }
    break;

  case F_POP_TO_MARK:
    /* POP_VALUE POP_TO_MARK : POP_TO_MARK */
    {
      do_optimization(2, 1, 1, F_POP_TO_MARK, 0);
      return 1;
    }
    break;

  case F_POP_VALUE:
    /* POP_VALUE POP_VALUE : POP_N_ELEMS (2) */
    {
      do_optimization(2, 1, 2, F_POP_N_ELEMS, 2, 0);
      return 1;
    }
    break;

  case F_RETURN_0:
    /* POP_VALUE RETURN_0: RETURN_0 */
    {
      do_optimization(2, 1, 1, F_RETURN_0, 0);
      return 1;
    }
    break;

  case F_RETURN_1:
    /* POP_VALUE RETURN_1: RETURN_1 */
    {
      do_optimization(2, 1, 1, F_RETURN_1, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_50(void)
{
  switch(opcode(0))
  {
  case F_POP_VALUE:
    /* RECUR POP_VALUE : RECUR_AND_POP($1a) */
    {
      do_optimization(2, 1, 2, F_RECUR_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_RETURN:
    /* RECUR RETURN [check_tailrecursion()] : TAIL_RECUR ($1a) */
    if(check_tailrecursion())
    {
      do_optimization(2, 1, 2, F_TAIL_RECUR, argument(1), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_51(void)
{
  switch(opcode(0))
  {
  case F_CLEAR_2_LOCAL:
    /* BYTE ENTRY START_FUNCTION CLEAR_2_LOCAL [$1a <= $4a] : BYTE($1a) ENTRY 
       START_FUNCTION */
    if(F_BYTE==opcode(3) && F_ENTRY==opcode(2) && argument(3) <= argument(0))
    {
      do_optimization(4, 3, 1, F_START_FUNCTION, 1, F_ENTRY, 2, F_BYTE, 
                      argument(3), 0);
      return 1;
    }
    break;

  case F_CLEAR_4_LOCAL:
    /* BYTE ENTRY START_FUNCTION CLEAR_4_LOCAL [$1a <= $4a] : BYTE($1a) ENTRY 
       START_FUNCTION */
    if(F_BYTE==opcode(3) && F_ENTRY==opcode(2) && argument(3) <= argument(0))
    {
      do_optimization(4, 3, 1, F_START_FUNCTION, 1, F_ENTRY, 2, F_BYTE, 
                      argument(3), 0);
      return 1;
    }
    break;

  case F_CLEAR_LOCAL:
    /* BYTE ENTRY START_FUNCTION CLEAR_LOCAL [$1a <= $4a] : BYTE($1a) ENTRY 
       START_FUNCTION */
    if(F_BYTE==opcode(3) && F_ENTRY==opcode(2) && argument(3) <= argument(0))
    {
      do_optimization(4, 3, 1, F_START_FUNCTION, 1, F_ENTRY, 2, F_BYTE, 
                      argument(3), 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_52(void)
{
  switch(opcode(0))
  {
  case F_CLEAR_STRING_SUBTYPE:
    /* STRING CLEAR_STRING_SUBTYPE: STRING ($1a) */
    {
      do_optimization(2, 1, 2, F_STRING, argument(1), 0);
      return 1;
    }
    break;

  case F_INDEX:
    /* STRING INDEX: STRING_INDEX ($1a) */
    {
      do_optimization(2, 1, 2, F_STRING_INDEX, argument(1), 0);
      return 1;
    }
    break;

  case F_LOCAL:
    /* ASSIGN_LOCAL_AND_POP STRING LOCAL($1a) : ASSIGN_LOCAL($1a) STRING($2a) 
       SWAP */
    if(F_ASSIGN_LOCAL_AND_POP==opcode(2) && (argument(2))==argument(0))
    {
      do_optimization(3, 3, 1, F_SWAP, 2, F_STRING, argument(1), 2, 
                      F_ASSIGN_LOCAL, argument(2), 0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* STRING MARK_X [$2a>0] : MARK_X($2a-1) STRING($1a) */
    if(argument(0)>0)
    {
      do_optimization(2, 2, 2, F_STRING, argument(1), 2, F_MARK_X, 
                      argument(0)-1, 0);
      return 1;
    }
    break;

  case F_POP_VALUE:
    /* STRING POP_VALUE : */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_53(void)
{
  switch(opcode(0))
  {
  case F_MARK_X:
    /* SWAP MARK_X : MARK_X($2a) SWAP */
    {
      do_optimization(2, 2, 1, F_SWAP, 2, F_MARK_X, argument(0), 0);
      return 1;
    }
    break;

  case F_SWAP:
    /* SWAP SWAP: */
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int _asm_peep_54(void)
{
  switch(argument(0))
  {
  case 0:
    /* POP_N_ELEMS (0) : */
    {
      do_optimization(1, 0, 0);
      return 1;
    }
    break;

  case 1:
    /* POP_N_ELEMS(1) : POP_VALUE */
    {
      do_optimization(1, 1, 1, F_POP_VALUE, 0);
      return 1;
    }
    break;

  }
  return 0;
}

INLINE static int low_asm_opt(void) {
  switch(opcode(1))
  {
  case F_2_LOCALS:
    if (_asm_peep_1())
      return 1;
    break;

  case F_ADD:
    if (_asm_peep_2())
      return 1;
    break;

  case F_ADD_FLOATS:
    if (_asm_peep_3())
      return 1;
    break;

  case F_ADD_INT:
    if (_asm_peep_4())
      return 1;
    break;

  case F_ADD_INTS:
    if (_asm_peep_5())
      return 1;
    break;

  case F_ADD_NEG_INT:
    if (_asm_peep_6())
      return 1;
    break;

  case F_ADD_TO:
    /* ADD_TO POP_VALUE : ADD_TO_AND_POP */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 1, F_ADD_TO_AND_POP, 0);
      return 1;
    }
    break;

  case F_APPLY:
    if (_asm_peep_7())
      return 1;
    break;

  case F_APPLY_AND_RETURN:
    /* APPLY_AND_RETURN ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && $2o != 
       F_POP_SYNCH_MARK && $2o != F_ENTRY] : APPLY_AND_RETURN($1a) */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 2, F_APPLY_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  case F_ARROW:
    if (_asm_peep_8())
      return 1;
    break;

  case F_ASSIGN:
    /* ASSIGN POP_VALUE : ASSIGN_AND_POP */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 1, F_ASSIGN_AND_POP, 0);
      return 1;
    }
    break;

  case F_ASSIGN_GLOBAL:
    if (_asm_peep_9())
      return 1;
    break;

  case F_ASSIGN_GLOBAL_AND_POP:
    if (_asm_peep_10())
      return 1;
    break;

  case F_ASSIGN_LOCAL:
    if (_asm_peep_11())
      return 1;
    break;

  case F_ASSIGN_LOCAL_AND_POP:
    if (_asm_peep_12())
      return 1;
    break;

  case F_BRANCH:
    switch(opcode(2))
    {
    case F_BRANCH_WHEN_EQ:
      /* BRANCH_WHEN_EQ BRANCH LABEL ($1a) : BRANCH_WHEN_NE($2a) LABEL($1a) */
      if(F_LABEL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_LABEL, argument(2), 2, F_BRANCH_WHEN_NE, 
                        argument(1), 0);
        return 1;
      }
      break;

    case F_BRANCH_WHEN_NE:
      /* BRANCH_WHEN_NE BRANCH LABEL ($1a) : BRANCH_WHEN_EQ($2a) LABEL($1a) */
      if(F_LABEL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_LABEL, argument(2), 2, F_BRANCH_WHEN_EQ, 
                        argument(1), 0);
        return 1;
      }
      break;

    case F_BRANCH_WHEN_NON_ZERO:
      /* BRANCH_WHEN_NON_ZERO BRANCH LABEL ($1a): BRANCH_WHEN_ZERO($2a) 
         LABEL($1a) */
      if(F_LABEL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_LABEL, argument(2), 2, F_BRANCH_WHEN_ZERO, 
                        argument(1), 0);
        return 1;
      }
      break;

    case F_BRANCH_WHEN_ZERO:
      /* BRANCH_WHEN_ZERO BRANCH LABEL ($1a): BRANCH_WHEN_NON_ZERO($2a) 
         LABEL($1a) */
      if(F_LABEL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_LABEL, argument(2), 2, 
                        F_BRANCH_WHEN_NON_ZERO, argument(1), 0);
        return 1;
      }
      break;

    }
    /* BRANCH ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && $2o != 
       F_POP_SYNCH_MARK && $2o != F_ENTRY] : BRANCH($1a) */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 2, F_BRANCH, argument(1), 0);
      return 1;
    }
    /* BRANCH LABEL($1a) : LABEL($1a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(2, 1, 2, F_LABEL, argument(1), 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_EQ:
    /* BRANCH_WHEN_EQ LABEL($1a) : POP_VALUE POP_VALUE LABEL($1a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(2, 3, 2, F_LABEL, argument(1), 1, F_POP_VALUE, 1, 
                      F_POP_VALUE, 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_GE:
    /* BRANCH_WHEN_GE LABEL($1a) : POP_VALUE POP_VALUE LABEL($1a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(2, 3, 2, F_LABEL, argument(1), 1, F_POP_VALUE, 1, 
                      F_POP_VALUE, 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_GT:
    /* BRANCH_WHEN_GT LABEL($1a) : POP_VALUE POP_VALUE LABEL($1a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(2, 3, 2, F_LABEL, argument(1), 1, F_POP_VALUE, 1, 
                      F_POP_VALUE, 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_LE:
    /* BRANCH_WHEN_LE LABEL($1a) : POP_VALUE POP_VALUE LABEL($1a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(2, 3, 2, F_LABEL, argument(1), 1, F_POP_VALUE, 1, 
                      F_POP_VALUE, 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_LT:
    /* BRANCH_WHEN_LT LABEL($1a) : POP_VALUE POP_VALUE LABEL($1a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(2, 3, 2, F_LABEL, argument(1), 1, F_POP_VALUE, 1, 
                      F_POP_VALUE, 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_NE:
    /* BRANCH_WHEN_NE LABEL($1a) : POP_VALUE POP_VALUE LABEL($1a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(2, 3, 2, F_LABEL, argument(1), 1, F_POP_VALUE, 1, 
                      F_POP_VALUE, 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_NON_ZERO:
    switch(opcode(2))
    {
    case F_ASSIGN_GLOBAL:
      /* ASSIGN_GLOBAL BRANCH_WHEN_NON_ZERO GLOBAL ($1a) : ASSIGN_GLOBAL ($1a) 
         BRANCH_AND_POP_WHEN_NON_ZERO ($2a) */
      if(F_GLOBAL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_BRANCH_AND_POP_WHEN_NON_ZERO, argument(1), 
                        2, F_ASSIGN_GLOBAL, argument(2), 0);
        return 1;
      }
      break;

    case F_ASSIGN_LOCAL:
      /* ASSIGN_LOCAL BRANCH_WHEN_NON_ZERO LOCAL ($1a) : ASSIGN_LOCAL ($1a) 
         BRANCH_AND_POP_WHEN_NON_ZERO ($2a) */
      if(F_LOCAL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_BRANCH_AND_POP_WHEN_NON_ZERO, argument(1), 
                        2, F_ASSIGN_LOCAL, argument(2), 0);
        return 1;
      }
      break;

    case F_GLOBAL:
      /* GLOBAL BRANCH_WHEN_NON_ZERO GLOBAL ($1a) : GLOBAL ($1a) 
         BRANCH_AND_POP_WHEN_NON_ZERO ($2a) */
      if(F_GLOBAL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_BRANCH_AND_POP_WHEN_NON_ZERO, argument(1), 
                        2, F_GLOBAL, argument(2), 0);
        return 1;
      }
      break;

    case F_LOCAL:
      /* LOCAL BRANCH_WHEN_NON_ZERO LOCAL ($1a) : LOCAL ($1a) 
         BRANCH_AND_POP_WHEN_NON_ZERO ($2a) */
      if(F_LOCAL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_BRANCH_AND_POP_WHEN_NON_ZERO, argument(1), 
                        2, F_LOCAL, argument(2), 0);
        return 1;
      }
      break;

    }
    /* BRANCH_WHEN_NON_ZERO LABEL($1a) : POP_VALUE LABEL($1a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(2, 2, 2, F_LABEL, argument(1), 1, F_POP_VALUE, 0);
      return 1;
    }
    break;

  case F_BRANCH_WHEN_ZERO:
    switch(opcode(2))
    {
    case F_ASSIGN_GLOBAL:
      /* ASSIGN_GLOBAL BRANCH_WHEN_ZERO GLOBAL ($1a) : ASSIGN_GLOBAL ($1a) 
         BRANCH_AND_POP_WHEN_ZERO ($2a) */
      if(F_GLOBAL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(1), 2, 
                        F_ASSIGN_GLOBAL, argument(2), 0);
        return 1;
      }
      break;

    case F_ASSIGN_LOCAL:
      /* ASSIGN_LOCAL BRANCH_WHEN_ZERO LOCAL ($1a) : ASSIGN_LOCAL ($1a) 
         BRANCH_AND_POP_WHEN_ZERO ($2a) */
      if(F_LOCAL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(1), 2, 
                        F_ASSIGN_LOCAL, argument(2), 0);
        return 1;
      }
      break;

    case F_GLOBAL:
      /* GLOBAL BRANCH_WHEN_ZERO GLOBAL ($1a) : GLOBAL ($1a) 
         BRANCH_AND_POP_WHEN_ZERO ($2a) */
      if(F_GLOBAL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(1), 2, 
                        F_GLOBAL, argument(2), 0);
        return 1;
      }
      break;

    case F_GLOBAL_LOCAL_INDEX:
      /* GLOBAL_LOCAL_INDEX($1a, $1b) BRANCH_WHEN_ZERO GLOBAL_LOCAL_INDEX($1a, 
         $1b) : GLOBAL_LOCAL_INDEX($1a, $1b) BRANCH_AND_POP_WHEN_ZERO ( $2a )
         */
      if(F_GLOBAL_LOCAL_INDEX==opcode(0) && (argument(2))==argument(0) && 
         (argument2(2))==argument2(0))
      {
        do_optimization(3, 2, 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(1), 3, 
                        F_GLOBAL_LOCAL_INDEX, argument(2), argument2(2), 0);
        return 1;
      }
      break;

    case F_LOCAL:
      /* LOCAL BRANCH_WHEN_ZERO LOCAL ($1a) : LOCAL ($1a) 
         BRANCH_AND_POP_WHEN_ZERO ($2a) */
      if(F_LOCAL==opcode(0) && (argument(2))==argument(0))
      {
        do_optimization(3, 2, 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(1), 2, 
                        F_LOCAL, argument(2), 0);
        return 1;
      }
      break;

    case F_LOCAL_ARROW:
      /* LOCAL_ARROW($1a, $1b) BRANCH_WHEN_ZERO LOCAL_ARROW($1a, $1b) : 
         LOCAL_ARROW($1a, $1b) BRANCH_AND_POP_WHEN_ZERO ( $2a ) */
      if(F_LOCAL_ARROW==opcode(0) && (argument(2))==argument(0) && 
         (argument2(2))==argument2(0))
      {
        do_optimization(3, 2, 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(1), 3, 
                        F_LOCAL_ARROW, argument(2), argument2(2), 0);
        return 1;
      }
      break;

    case F_LOCAL_LOCAL_INDEX:
      /* LOCAL_LOCAL_INDEX($1a, $1b) BRANCH_WHEN_ZERO LOCAL_LOCAL_INDEX($1a, 
         $1b) : LOCAL_LOCAL_INDEX($1a, $1b) BRANCH_AND_POP_WHEN_ZERO ( $2a ) */
      if(F_LOCAL_LOCAL_INDEX==opcode(0) && (argument(2))==argument(0) && 
         (argument2(2))==argument2(0))
      {
        do_optimization(3, 2, 2, F_BRANCH_AND_POP_WHEN_ZERO, argument(1), 3, 
                        F_LOCAL_LOCAL_INDEX, argument(2), argument2(2), 0);
        return 1;
      }
      break;

    }
    /* BRANCH_WHEN_ZERO LABEL($1a) : POP_VALUE LABEL($1a) */
    if(F_LABEL==opcode(0) && (argument(1))==argument(0))
    {
      do_optimization(2, 2, 2, F_LABEL, argument(1), 1, F_POP_VALUE, 0);
      return 1;
    }
    break;

  case F_CALL_BUILTIN:
    if (_asm_peep_13())
      return 1;
    break;

  case F_CALL_BUILTIN1:
    if (_asm_peep_14())
      return 1;
    break;

  case F_CALL_BUILTIN_AND_RETURN:
    /* CALL_BUILTIN_AND_RETURN ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && $2o
       != F_POP_SYNCH_MARK && $2o != F_ENTRY] : CALL_BUILTIN_AND_RETURN($1a) */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 2, F_CALL_BUILTIN_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  case F_CALL_FUNCTION:
    if (_asm_peep_15())
      return 1;
    break;

  case F_CALL_FUNCTION_AND_RETURN:
    /* CALL_FUNCTION_AND_RETURN ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && 
       $2o != F_POP_SYNCH_MARK && $2o != F_ENTRY] : 
       CALL_FUNCTION_AND_RETURN($1a) */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 2, F_CALL_FUNCTION_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  case F_CALL_LFUN:
    if (_asm_peep_16())
      return 1;
    break;

  case F_CALL_LFUN_AND_RETURN:
    /* CALL_LFUN_AND_RETURN ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && $2o !=
       F_POP_SYNCH_MARK && $2o != F_ENTRY] : CALL_LFUN_AND_RETURN($1a) */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 2, F_CALL_LFUN_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  case F_CALL_OTHER:
    if (_asm_peep_17())
      return 1;
    break;

  case F_CALL_OTHER_AND_RETURN:
    /* CALL_OTHER_AND_RETURN ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && $2o 
       != F_POP_SYNCH_MARK && $2o != F_ENTRY] : CALL_OTHER_AND_RETURN($1a) */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 2, F_CALL_OTHER_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  case F_CATCH:
    /* CATCH ESCAPE_CATCH: */
    if(F_ESCAPE_CATCH==opcode(0))
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_CLEAR_2_LOCAL:
    if (_asm_peep_18())
      return 1;
    break;

  case F_CLEAR_LOCAL:
    if (_asm_peep_19())
      return 1;
    break;

  case F_CLEAR_STRING_SUBTYPE:
    if (_asm_peep_20())
      return 1;
    break;

  case F_COMPL:
    /* COMPL COMPL : */
    if(F_COMPL==opcode(0))
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_CONST0:
    if (_asm_peep_21())
      return 1;
    break;

  case F_CONST1:
    if (_asm_peep_22())
      return 1;
    break;

  case F_CONSTANT:
    if (_asm_peep_23())
      return 1;
    break;

  case F_CONST_1:
    if (_asm_peep_24())
      return 1;
    break;

  case F_DEC:
    /* DEC POP_VALUE: DEC_AND_POP */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 1, F_DEC_AND_POP, 0);
      return 1;
    }
    break;

  case F_DEC_AND_POP:
    /* GLOBAL_LVALUE DEC_AND_POP GLOBAL($1a) : GLOBAL_LVALUE($1a) DEC */
    if(F_GLOBAL_LVALUE==opcode(2) && F_GLOBAL==opcode(0) && 
       (argument(2))==argument(0))
    {
      do_optimization(3, 2, 1, F_DEC, 2, F_GLOBAL_LVALUE, argument(2), 0);
      return 1;
    }
    /* GLOBAL_LVALUE DEC_AND_POP GLOBAL($1a): GLOBAL_LVALUE($1a) DEC */
    if(F_GLOBAL_LVALUE==opcode(2) && F_GLOBAL==opcode(0) && 
       (argument(2))==argument(0))
    {
      do_optimization(3, 2, 1, F_DEC, 2, F_GLOBAL_LVALUE, argument(2), 0);
      return 1;
    }
    break;

  case F_DEC_LOCAL:
    /* DEC_LOCAL POP_VALUE : DEC_LOCAL_AND_POP ($1a) */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 2, F_DEC_LOCAL_AND_POP, argument(1), 0);
      return 1;
    }
    /* DEC_LOCAL POP_VALUE : DEC_LOCAL_AND_POP($1a) */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 2, F_DEC_LOCAL_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_DEC_LOCAL_AND_POP:
    if (_asm_peep_25())
      return 1;
    break;

  case F_DUP:
    if (_asm_peep_26())
      return 1;
    break;

  case F_EQ:
    if (_asm_peep_27())
      return 1;
    break;

  case F_GE:
    /* GE BRANCH_WHEN_NON_ZERO: BRANCH_WHEN_GE ($2a) */
    if(F_BRANCH_WHEN_NON_ZERO==opcode(0))
    {
      do_optimization(2, 1, 2, F_BRANCH_WHEN_GE, argument(0), 0);
      return 1;
    }
    break;

  case F_GLOBAL:
    if (_asm_peep_28())
      return 1;
    break;

  case F_GLOBAL_LVALUE:
    if (_asm_peep_29())
      return 1;
    break;

  case F_GT:
    /* GT BRANCH_WHEN_NON_ZERO: BRANCH_WHEN_GT ($2a) */
    if(F_BRANCH_WHEN_NON_ZERO==opcode(0))
    {
      do_optimization(2, 1, 2, F_BRANCH_WHEN_GT, argument(0), 0);
      return 1;
    }
    break;

  case F_IDENTIFIER:
    /* IDENTIFIER POP_VALUE : */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_INC:
    /* INC POP_VALUE: INC_AND_POP */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 1, F_INC_AND_POP, 0);
      return 1;
    }
    break;

  case F_INC_AND_POP:
    /* GLOBAL_LVALUE INC_AND_POP GLOBAL($1a) : GLOBAL_LVALUE($1a) INC */
    if(F_GLOBAL_LVALUE==opcode(2) && F_GLOBAL==opcode(0) && 
       (argument(2))==argument(0))
    {
      do_optimization(3, 2, 1, F_INC, 2, F_GLOBAL_LVALUE, argument(2), 0);
      return 1;
    }
    /* GLOBAL_LVALUE INC_AND_POP GLOBAL($1a): GLOBAL_LVALUE($1a) INC */
    if(F_GLOBAL_LVALUE==opcode(2) && F_GLOBAL==opcode(0) && 
       (argument(2))==argument(0))
    {
      do_optimization(3, 2, 1, F_INC, 2, F_GLOBAL_LVALUE, argument(2), 0);
      return 1;
    }
    break;

  case F_INC_LOCAL:
    /* INC_LOCAL POP_VALUE : INC_LOCAL_AND_POP ($1a) */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 2, F_INC_LOCAL_AND_POP, argument(1), 0);
      return 1;
    }
    /* INC_LOCAL POP_VALUE : INC_LOCAL_AND_POP($1a) */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 2, F_INC_LOCAL_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_INC_LOCAL_AND_POP:
    if (_asm_peep_30())
      return 1;
    break;

  case F_INDEX:
    /* INDEX SWITCH: SWITCH_ON_INDEX($2a) */
    if(F_SWITCH==opcode(0))
    {
      do_optimization(2, 1, 2, F_SWITCH_ON_INDEX, argument(0), 0);
      return 1;
    }
    break;

  case F_LABEL:
    if (_asm_peep_31())
      return 1;
    break;

  case F_LE:
    /* LE BRANCH_WHEN_NON_ZERO: BRANCH_WHEN_LE ($2a) */
    if(F_BRANCH_WHEN_NON_ZERO==opcode(0))
    {
      do_optimization(2, 1, 2, F_BRANCH_WHEN_LE, argument(0), 0);
      return 1;
    }
    break;

  case F_LFUN:
    /* LFUN MARK_X [$2a>0] : MARK_X($2a-1) LFUN($1a) */
    if(F_MARK_X==opcode(0) && argument(0)>0)
    {
      do_optimization(2, 2, 2, F_LFUN, argument(1), 2, F_MARK_X, 
                      argument(0)-1, 0);
      return 1;
    }
    break;

  case F_LOCAL:
    if (_asm_peep_32())
      return 1;
    break;

  case F_LOCAL_ARROW:
    if (_asm_peep_33())
      return 1;
    break;

  case F_LOCAL_LVALUE:
    if (_asm_peep_34())
      return 1;
    break;

  case F_LT:
    /* LT BRANCH_WHEN_NON_ZERO: BRANCH_WHEN_LT ($2a) */
    if(F_BRANCH_WHEN_NON_ZERO==opcode(0))
    {
      do_optimization(2, 1, 2, F_BRANCH_WHEN_LT, argument(0), 0);
      return 1;
    }
    break;

  case F_LTOSVAL:
    /* LTOSVAL MARK_X [$2a>0] : MARK_X($2a-1) LTOSVAL */
    if(F_MARK_X==opcode(0) && argument(0)>0)
    {
      do_optimization(2, 2, 1, F_LTOSVAL, 2, F_MARK_X, argument(0)-1, 0);
      return 1;
    }
    break;

  case F_LTOSVAL_CALL_BUILTIN_AND_ASSIGN:
    /* LTOSVAL_CALL_BUILTIN_AND_ASSIGN POP_VALUE : 
       LTOSVAL_CALL_BUILTIN_AND_ASSIGN_POP($1a) */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 2, F_LTOSVAL_CALL_BUILTIN_AND_ASSIGN_POP, 
                      argument(1), 0);
      return 1;
    }
    break;

  case F_MARK:
    if (_asm_peep_35())
      return 1;
    break;

  case F_MARK_AND_CONST1:
    if (_asm_peep_36())
      return 1;
    break;

  case F_MARK_AND_LOCAL:
    if (_asm_peep_37())
      return 1;
    break;

  case F_MARK_AND_STRING:
    /* ASSIGN_LOCAL_AND_POP MARK_AND_STRING LOCAL($1a) : ASSIGN_LOCAL($1a) 
       STRING($2a) SWAP MARK_X(2) */
    if(F_ASSIGN_LOCAL_AND_POP==opcode(2) && F_LOCAL==opcode(0) && 
       (argument(2))==argument(0))
    {
      do_optimization(3, 4, 2, F_MARK_X, 2, 1, F_SWAP, 2, F_STRING, 
                      argument(1), 2, F_ASSIGN_LOCAL, argument(2), 0);
      return 1;
    }
    break;

  case F_MARK_APPLY:
    if (_asm_peep_38())
      return 1;
    break;

  case F_MARK_APPLY_AND_RETURN:
    /* MARK_APPLY_AND_RETURN ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && $2o 
       != F_POP_SYNCH_MARK && $2o != F_ENTRY] : MARK_APPLY_AND_RETURN($1a) */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 2, F_MARK_APPLY_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  case F_MARK_CALL_BUILTIN:
    if (_asm_peep_39())
      return 1;
    break;

  case F_MARK_CALL_BUILTIN_AND_RETURN:
    /* MARK_CALL_BUILTIN_AND_RETURN ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK 
       && $2o != F_POP_SYNCH_MARK && $2o != F_ENTRY] : 
       MARK_CALL_BUILTIN_AND_RETURN($1a) */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 2, F_MARK_CALL_BUILTIN_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  case F_MARK_CALL_LFUN:
    if (_asm_peep_40())
      return 1;
    break;

  case F_MARK_CALL_LFUN_AND_RETURN:
    /* MARK_CALL_LFUN_AND_RETURN ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && 
       $2o != F_POP_SYNCH_MARK && $2o != F_ENTRY] : 
       MARK_CALL_LFUN_AND_RETURN($1a) */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 2, F_MARK_CALL_LFUN_AND_RETURN, argument(1), 0);
      return 1;
    }
    break;

  case F_MARK_X:
    if (_asm_peep_41())
      return 1;
    break;

  case F_NE:
    if (_asm_peep_42())
      return 1;
    break;

  case F_NEGATE:
    if (_asm_peep_43())
      return 1;
    break;

  case F_NEG_NUMBER:
    if (_asm_peep_44())
      return 1;
    break;

  case F_NOT:
    if (_asm_peep_45())
      return 1;
    break;

  case F_NUMBER:
    if (_asm_peep_46())
      return 1;
    break;

  case F_POINTER:
    if (_asm_peep_47())
      return 1;
    break;

  case F_POP_N_ELEMS:
    if (_asm_peep_48())
      return 1;
    break;

  case F_POP_VALUE:
    if (_asm_peep_49())
      return 1;
    break;

  case F_POST_DEC:
    /* POST_DEC POP_VALUE: DEC_AND_POP */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 1, F_DEC_AND_POP, 0);
      return 1;
    }
    break;

  case F_POST_DEC_LOCAL:
    /* POST_DEC_LOCAL POP_VALUE : DEC_LOCAL_AND_POP ($1a) */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 2, F_DEC_LOCAL_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_POST_INC:
    /* POST_INC POP_VALUE: INC_AND_POP */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 1, F_INC_AND_POP, 0);
      return 1;
    }
    break;

  case F_POST_INC_LOCAL:
    /* POST_INC_LOCAL POP_VALUE : INC_LOCAL_AND_POP ($1a) */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 1, 2, F_INC_LOCAL_AND_POP, argument(1), 0);
      return 1;
    }
    break;

  case F_RECUR:
    if (_asm_peep_50())
      return 1;
    break;

  case F_RETURN:
    switch(opcode(2))
    {
    case F_GLOBAL:
      /* ASSIGN_GLOBAL BRANCH_WHEN_ZERO GLOBAL($1a) RETURN LABEL($2a): 
         ASSIGN_GLOBAL($1a) RETURN_IF_TRUE LABEL($2a) */
      if(F_ASSIGN_GLOBAL==opcode(4) && F_BRANCH_WHEN_ZERO==opcode(3) && 
         (argument(4))==argument(2) && F_LABEL==opcode(0) && 
         (argument(3))==argument(0))
      {
        do_optimization(5, 3, 2, F_LABEL, argument(3), 1, F_RETURN_IF_TRUE, 2, 
                        F_ASSIGN_GLOBAL, argument(4), 0);
        return 1;
      }
      break;

    case F_LOCAL:
      /* ASSIGN_LOCAL BRANCH_WHEN_ZERO LOCAL($1a) RETURN LABEL($2a): 
         ASSIGN_LOCAL($1a) RETURN_IF_TRUE LABEL($2a) */
      if(F_ASSIGN_LOCAL==opcode(4) && F_BRANCH_WHEN_ZERO==opcode(3) && 
         (argument(4))==argument(2) && F_LABEL==opcode(0) && 
         (argument(3))==argument(0))
      {
        do_optimization(5, 3, 2, F_LABEL, argument(3), 1, F_RETURN_IF_TRUE, 2, 
                        F_ASSIGN_LOCAL, argument(4), 0);
        return 1;
      }
      /* ASSIGN_LOCAL BRANCH_WHEN_ZERO LOCAL($1a) RETURN LABEL($2a) 
         [!(Pike_compiler->compiler_frame->lexical_scope & SCOPE_SCOPED)] : 
         RETURN_IF_TRUE ASSIGN_LOCAL($1a) LABEL($2a) */
      if(F_ASSIGN_LOCAL==opcode(4) && F_BRANCH_WHEN_ZERO==opcode(3) && 
         (argument(4))==argument(2) && F_LABEL==opcode(0) && 
         (argument(3))==argument(0) && 
         !(Pike_compiler->compiler_frame->lexical_scope & SCOPE_SCOPED))
      {
        do_optimization(5, 3, 2, F_LABEL, argument(3), 2, F_ASSIGN_LOCAL, 
                        argument(4), 1, F_RETURN_IF_TRUE, 0);
        return 1;
      }
      break;

    }
    /* RETURN ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && $2o != 
       F_POP_SYNCH_MARK && $2o != F_ENTRY] : RETURN */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 1, F_RETURN, 0);
      return 1;
    }
    break;

  case F_RETURN_0:
    /* RETURN_0 ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && $2o != 
       F_POP_SYNCH_MARK && $2o != F_ENTRY] : RETURN_0 */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 1, F_RETURN_0, 0);
      return 1;
    }
    break;

  case F_RETURN_1:
    /* RETURN_1 ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && $2o != 
       F_POP_SYNCH_MARK && $2o != F_ENTRY] : RETURN_1 */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 1, F_RETURN_1, 0);
      return 1;
    }
    break;

  case F_RETURN_LOCAL:
    /* RETURN_LOCAL ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && $2o != 
       F_POP_SYNCH_MARK && $2o != F_ENTRY] : RETURN_LOCAL($1a) */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 2, F_RETURN_LOCAL, argument(1), 0);
      return 1;
    }
    break;

  case F_START_FUNCTION:
    if (_asm_peep_51())
      return 1;
    break;

  case F_STRING:
    if (_asm_peep_52())
      return 1;
    break;

  case F_SWAP:
    if (_asm_peep_53())
      return 1;
    break;

  case F_TRAMPOLINE:
    /* TRAMPOLINE POP_VALUE : */
    if(F_POP_VALUE==opcode(0))
    {
      do_optimization(2, 0, 0);
      return 1;
    }
    break;

  case F_VOLATILE_RETURN:
    switch(opcode(2))
    {
    case F_GLOBAL:
      /* ASSIGN_GLOBAL BRANCH_WHEN_ZERO GLOBAL($1a) VOLATILE_RETURN LABEL($2a):
         ASSIGN_GLOBAL($1a) RETURN_IF_TRUE LABEL($2a) */
      if(F_ASSIGN_GLOBAL==opcode(4) && F_BRANCH_WHEN_ZERO==opcode(3) && 
         (argument(4))==argument(2) && F_LABEL==opcode(0) && 
         (argument(3))==argument(0))
      {
        do_optimization(5, 3, 2, F_LABEL, argument(3), 1, F_RETURN_IF_TRUE, 2, 
                        F_ASSIGN_GLOBAL, argument(4), 0);
        return 1;
      }
      break;

    case F_LOCAL:
      /* ASSIGN_LOCAL BRANCH_WHEN_ZERO LOCAL($1a) VOLATILE_RETURN LABEL($2a): 
         ASSIGN_LOCAL($1a) RETURN_IF_TRUE LABEL($2a) */
      if(F_ASSIGN_LOCAL==opcode(4) && F_BRANCH_WHEN_ZERO==opcode(3) && 
         (argument(4))==argument(2) && F_LABEL==opcode(0) && 
         (argument(3))==argument(0))
      {
        do_optimization(5, 3, 2, F_LABEL, argument(3), 1, F_RETURN_IF_TRUE, 2, 
                        F_ASSIGN_LOCAL, argument(4), 0);
        return 1;
      }
      /* ASSIGN_LOCAL BRANCH_WHEN_ZERO LOCAL($1a) VOLATILE_RETURN LABEL($2a) 
         [!(Pike_compiler->compiler_frame->lexical_scope & SCOPE_SCOPED)] : 
         RETURN_IF_TRUE ASSIGN_LOCAL($1a) LABEL($2a) */
      if(F_ASSIGN_LOCAL==opcode(4) && F_BRANCH_WHEN_ZERO==opcode(3) && 
         (argument(4))==argument(2) && F_LABEL==opcode(0) && 
         (argument(3))==argument(0) && 
         !(Pike_compiler->compiler_frame->lexical_scope & SCOPE_SCOPED))
      {
        do_optimization(5, 3, 2, F_LABEL, argument(3), 2, F_ASSIGN_LOCAL, 
                        argument(4), 1, F_RETURN_IF_TRUE, 0);
        return 1;
      }
      break;

    }
    /* VOLATILE_RETURN ? [ $2o != F_LABEL && $2o != F_SYNCH_MARK && $2o != 
       F_POP_SYNCH_MARK && $2o != F_ENTRY] : VOLATILE_RETURN */
    if(opcode(0) != -1 &&  opcode(0) != F_LABEL && opcode(0) != F_SYNCH_MARK &&
       opcode(0) != F_POP_SYNCH_MARK && opcode(0) != F_ENTRY)
    {
      do_optimization(2, 1, 1, F_VOLATILE_RETURN, 0);
      return 1;
    }
    break;

  }
  switch(opcode(0))
  {
  case F_2_LOCALS:
    /* 2_LOCALS [$1a == $1b]: LOCAL($1a) DUP */
    if(argument(0) == argument2(0))
    {
      do_optimization(1, 2, 1, F_DUP, 2, F_LOCAL, argument(0), 0);
      return 1;
    }
    break;

  case F_LOCAL_2_LOCAL:
    /* LOCAL_2_LOCAL [$1a == $1b] : */
    if(argument(0) == argument2(0))
    {
      do_optimization(1, 0, 0);
      return 1;
    }
    break;

  case F_MARK_X:
    /* MARK_X(0) : MARK */
    if((0)==argument(0))
    {
      do_optimization(1, 1, 1, F_MARK, 0);
      return 1;
    }
    break;

  case F_NEG_NUMBER:
    /* NEG_NUMBER(1) : CONST_1 */
    if((1)==argument(0))
    {
      do_optimization(1, 1, 1, F_CONST_1, 0);
      return 1;
    }
    break;

  case F_NOP:
    /* NOP : */
    {
      do_optimization(1, 0, 0);
      return 1;
    }
    break;

  case F_NUMBER:
    switch(argument(0))
    {
    case -1:
      /* NUMBER(-1) : CONST_1 */
      {
        do_optimization(1, 1, 1, F_CONST_1, 0);
        return 1;
      }
      break;

    case 0:
      /* NUMBER(0) : CONST0 */
      {
        do_optimization(1, 1, 1, F_CONST0, 0);
        return 1;
      }
      break;

    case 1:
      /* NUMBER(1) : CONST1 */
      {
        do_optimization(1, 1, 1, F_CONST1, 0);
        return 1;
      }
      break;

    }
    /* NUMBER (0x7fffffff) : BIGNUM */
    if((0x7fffffff)==argument(0))
    {
      do_optimization(1, 1, 1, F_BIGNUM, 0);
      return 1;
    }
    break;

  case F_POP_N_ELEMS:
    if (_asm_peep_54())
      return 1;
    break;

  }
  return 0;
}
