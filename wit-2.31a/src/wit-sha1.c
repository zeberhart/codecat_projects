#include <stdio.h>
#include <string.h>
#include <mhash.h>

#if 0
#include <openssl/sha.h>
unsigned char *WIT_SHA1_ssl(const unsigned char *d, size_t n, unsigned char *md)
{
        SHA_CTX c;
        static unsigned char m[20];

        if (md == NULL) md=m;
        if (!SHA1_Init(&c))
                return NULL;
        SHA1_Update(&c,d,n);
        SHA1_Final(md,&c);
        return(md);
}
#endif

unsigned char *WIT_SHA1(const unsigned char *d, size_t n, unsigned char *md)
{
	MHASH td;
	if ((td = mhash_init(MHASH_SHA1)) == MHASH_FAILED)
		return NULL;
	mhash(td, d, n);
	mhash_deinit(td, md);
	return(md);
}

#if 0
int main() {
	char *bleh = "22:40 → fritz09 [~Adium@port-5452.pppoe.wtnet.de] has joined #29c3";
	char md[1024] = { 0 };
	WIT_SHA1(bleh, strlen(bleh), md);
	printf("md = %s\n", md);
	memset(md, '\0', sizeof(md));
	WIT_SHA1_ssl(bleh, strlen(bleh), md);
	printf("md = %s\n", md);
}
#endif
