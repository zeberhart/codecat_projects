#ifndef __RESOURCE_summary_H__
#define __RESOURCE_summary_H__

#include <gio/gio.h>

extern GResource *summary_get_resource (void);
#endif
