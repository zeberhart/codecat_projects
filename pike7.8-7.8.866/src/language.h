/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TOK_ARROW = 258,
     TOK_CONSTANT = 259,
     TOK_FLOAT = 260,
     TOK_STRING = 261,
     TOK_NUMBER = 262,
     TOK_INC = 263,
     TOK_DEC = 264,
     TOK_RETURN = 265,
     TOK_EQ = 266,
     TOK_GE = 267,
     TOK_LE = 268,
     TOK_NE = 269,
     TOK_NOT = 270,
     TOK_LSH = 271,
     TOK_RSH = 272,
     TOK_LAND = 273,
     TOK_LOR = 274,
     TOK_SWITCH = 275,
     TOK_SSCANF = 276,
     TOK_CATCH = 277,
     TOK_FOREACH = 278,
     TOK_LEX_EOF = 279,
     TOK_ADD_EQ = 280,
     TOK_AND_EQ = 281,
     TOK_ARRAY_ID = 282,
     TOK_ATTRIBUTE_ID = 283,
     TOK_BREAK = 284,
     TOK_CASE = 285,
     TOK_CLASS = 286,
     TOK_COLON_COLON = 287,
     TOK_CONTINUE = 288,
     TOK_DEFAULT = 289,
     TOK_DEPRECATED_ID = 290,
     TOK_DIV_EQ = 291,
     TOK_DO = 292,
     TOK_DOT_DOT = 293,
     TOK_DOT_DOT_DOT = 294,
     TOK_ELSE = 295,
     TOK_ENUM = 296,
     TOK_EXTERN = 297,
     TOK_FLOAT_ID = 298,
     TOK_FOR = 299,
     TOK_FUNCTION_ID = 300,
     TOK_GAUGE = 301,
     TOK_GLOBAL = 302,
     TOK_IDENTIFIER = 303,
     TOK_RESERVED = 304,
     TOK_IF = 305,
     TOK_IMPORT = 306,
     TOK_INHERIT = 307,
     TOK_FACET = 308,
     TOK_INLINE = 309,
     TOK_LOCAL_ID = 310,
     TOK_FINAL_ID = 311,
     TOK_FUNCTION_NAME = 312,
     TOK_INT_ID = 313,
     TOK_LAMBDA = 314,
     TOK_MULTISET_ID = 315,
     TOK_MULTISET_END = 316,
     TOK_MULTISET_START = 317,
     TOK_LSH_EQ = 318,
     TOK_MAPPING_ID = 319,
     TOK_MIXED_ID = 320,
     TOK_MOD_EQ = 321,
     TOK_MULT_EQ = 322,
     TOK_NO_MASK = 323,
     TOK_OBJECT_ID = 324,
     TOK_OR_EQ = 325,
     TOK_PRIVATE = 326,
     TOK_PROGRAM_ID = 327,
     TOK_PROTECTED = 328,
     TOK_PREDEF = 329,
     TOK_PUBLIC = 330,
     TOK_RSH_EQ = 331,
     TOK_STATIC = 332,
     TOK_STRING_ID = 333,
     TOK_SUB_EQ = 334,
     TOK_TYPEDEF = 335,
     TOK_TYPEOF = 336,
     TOK_VARIANT = 337,
     TOK_VERSION = 338,
     TOK_VOID_ID = 339,
     TOK_WHILE = 340,
     TOK_XOR_EQ = 341,
     TOK_OPTIONAL = 342
   };
#endif
/* Tokens.  */
#define TOK_ARROW 258
#define TOK_CONSTANT 259
#define TOK_FLOAT 260
#define TOK_STRING 261
#define TOK_NUMBER 262
#define TOK_INC 263
#define TOK_DEC 264
#define TOK_RETURN 265
#define TOK_EQ 266
#define TOK_GE 267
#define TOK_LE 268
#define TOK_NE 269
#define TOK_NOT 270
#define TOK_LSH 271
#define TOK_RSH 272
#define TOK_LAND 273
#define TOK_LOR 274
#define TOK_SWITCH 275
#define TOK_SSCANF 276
#define TOK_CATCH 277
#define TOK_FOREACH 278
#define TOK_LEX_EOF 279
#define TOK_ADD_EQ 280
#define TOK_AND_EQ 281
#define TOK_ARRAY_ID 282
#define TOK_ATTRIBUTE_ID 283
#define TOK_BREAK 284
#define TOK_CASE 285
#define TOK_CLASS 286
#define TOK_COLON_COLON 287
#define TOK_CONTINUE 288
#define TOK_DEFAULT 289
#define TOK_DEPRECATED_ID 290
#define TOK_DIV_EQ 291
#define TOK_DO 292
#define TOK_DOT_DOT 293
#define TOK_DOT_DOT_DOT 294
#define TOK_ELSE 295
#define TOK_ENUM 296
#define TOK_EXTERN 297
#define TOK_FLOAT_ID 298
#define TOK_FOR 299
#define TOK_FUNCTION_ID 300
#define TOK_GAUGE 301
#define TOK_GLOBAL 302
#define TOK_IDENTIFIER 303
#define TOK_RESERVED 304
#define TOK_IF 305
#define TOK_IMPORT 306
#define TOK_INHERIT 307
#define TOK_FACET 308
#define TOK_INLINE 309
#define TOK_LOCAL_ID 310
#define TOK_FINAL_ID 311
#define TOK_FUNCTION_NAME 312
#define TOK_INT_ID 313
#define TOK_LAMBDA 314
#define TOK_MULTISET_ID 315
#define TOK_MULTISET_END 316
#define TOK_MULTISET_START 317
#define TOK_LSH_EQ 318
#define TOK_MAPPING_ID 319
#define TOK_MIXED_ID 320
#define TOK_MOD_EQ 321
#define TOK_MULT_EQ 322
#define TOK_NO_MASK 323
#define TOK_OBJECT_ID 324
#define TOK_OR_EQ 325
#define TOK_PRIVATE 326
#define TOK_PROGRAM_ID 327
#define TOK_PROTECTED 328
#define TOK_PREDEF 329
#define TOK_PUBLIC 330
#define TOK_RSH_EQ 331
#define TOK_STATIC 332
#define TOK_STRING_ID 333
#define TOK_SUB_EQ 334
#define TOK_TYPEDEF 335
#define TOK_TYPEOF 336
#define TOK_VARIANT 337
#define TOK_VERSION 338
#define TOK_VOID_ID 339
#define TOK_WHILE 340
#define TOK_XOR_EQ 341
#define TOK_OPTIONAL 342




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 185 "language.yacc"
{
  int number;
  FLOAT_TYPE fnum;
  struct node_s *n;
  char *str;
  void *ptr;
}
/* Line 1529 of yacc.c.  */
#line 231 "y.tab.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



