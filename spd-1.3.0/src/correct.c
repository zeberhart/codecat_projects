/*
 *   Project: The SPD Image correction and azimuthal regrouping
 *			http://forge.epn-campus.eu/projects/show/azimuthal
 *
 *   Copyright (C) 2001-2010 European Synchrotron Radiation Facility
 *                           Grenoble, France
 *
 *   Principal authors: P. Boesecke (boesecke@esrf.fr)
 *                      R. Wilcke (wilcke@esrf.fr)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   and the GNU Lesser General Public License  along with this program.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

/*
Update 23/10/2011 P. Boesecke (boesecke@esrf.fr)
                  DO_DARK default set to 0
                  strtok_r -> strlib_tok_r
Update 19/10/2011 P. Boesecke (boesecke@esrf.fr)
                  Various changes to allow intensity renormalization
                  during prerotation corrections. The following 
                  variables have been added: INVALID_TYP, SDMTYP,
                  TMPTYP, MINFILE, MOUTFILE, M_COR, NORM_PREROT, 
                  If NORM_PREROT is set the distortion corrected image 
                  is multiplied with the intensity correction array M_COR.
                  The array M_COR is always created together with the
                  displacement arrays X_COR and Y_COR, but it remains
                  always available.
                  The renormalization during prerotation correction can
                  be controlled with set_normprerot, set_moutfile defines
                  the output file name of the the intensity correction
                  array M_COR, if required.
                  The correct intensity normalization with and without
                  prerotations has been checked.
                  Other changes: 
                  - get_doprerot added to allow to read the
                    prerot values from outside this module. 
                  - upd_headval(): parameters out_type and mode added,
                    upd_headval must only be used for DO_PREROT!=2,

                  - normint_im(): partial normalizations to Intensity1 and 
                                  DOmega added. This can be controlled with
                                  set_normint(normint,factor), with normint
                      0: copy src_im to cor_im
                      1: full normalization (I/DOmega/Intensity1*NORMFACT)
                      2: normalization to Intensity1 (I/Intensity1*NORMFACT)
                      3: normalization to DOmega (I/DOmega*NORMFACT)
                  Originally, only the values 0 and 1 were possible.
Update 23/09/2011 P. Boesecke (boesecke@esrf.fr)
                  DO_SPD=4 added,
                  subtract_im: additional parameter fac added, to avoid 
                  usage of scale_im in scattering background subtraction.
                  old function renamed to subtract_drk
Update 22/09/2011 P. Boesecke (boesecke@esrf.fr)
                  DO_PREROT default is 0 (according to inout.c)
Update 14/09/2011 P. Boesecke (boesecke@esrf.fr)
                  Only a single place to write final XOUTFILE and YOUTFILE 
                  at the end of spd_corr (removed from spd_scorr and spd_fcorr).
                  correct_image: with SDX_PREROT it is not necessary
                  any more to check that the prerotation angles are zero when
                  DO_PREROT is not set.
Update 12/09/2011 P. Boesecke (boesecke@esrf.fr)
                  SDX_PREROT variable added.
Update 10/09/2011 P. Boesecke (boesecke@esrf.fr)
                  spd_rotcorr and undistort_im:
                  To allow the use of DSTRTVAL updated CORTYP header 
                  parameters inside spd_rotcorr the CORTYP and SDXTYP 
                  headers are updated with the new functions upd_headval 
                  calc_prerot and set_prerot_headval.
Update 08/09/2011 P. Boesecke (boesecke@esrf.fr)
                  default of overflow parameter IMAGE_OVER changed from
                  0xffff to 0 because it is detector dependent.
                  The overflow parameter is now an unsigned long 
                  (before unsigned short) to allow use with detectors 
                  having more than 2 bytes data per pixel.
Update 17/08/2011 P. Boesecke (boesecke@esrf.fr)
                  set_doprerot added (to do a rotation correction after 
                  the distortion correction)  
Update 22/07/2011 P. Boesecke (boesecke@esrf.fr)
                  rearrangement of code: andy_corr split into andy_corr
                  and andy_scorr, error labels added in andy_corr,
                  andy_scorr and andy_fcorr, no forward declaration,
                  corr_calc renamed to undistort_im, ANDY_CORR code 
                  collected in a single block, because it is - and was -
                  not possible any more to compile spd without 
                  ANDY_CORR defined this FLAG is not checked any more.
                  my_func -> spd_func, myfgets -> spd_fgets,
                  unloadspd -> spd_unloadspline,
                  loadspdfile -> spd_loadspline,
                  findkeyword -> spd_findkeyword,
                  readarray -> spd_readarray,
                  pxcorrgrid -> spd_calcspline
Update 03/06/2010 P. Boesecke (boesecke@esrf.fr)
                  angle_sum replaced by ang_sum to include rotations
                  azim_int: flag apro added, a0, da in radian,
                  parameter verbose added.
                  For azim_pro!=0 the key ProjectionType in the AZITYP header
                  is set according to azim_pro:IO_ProSaxs | IO_ProWaxs.
Update 06/10/2009 P. Boesecke (boesecke@esrf.fr)
                  azim_int(): azimuthal regrouping and averaging is now done
                  with functions angle_sum (angle.h) and project_1 (project.h),
                  unused variables removed.
Update 06/05/2009 R. Wilcke (wilcke@esrf.fr)
                  correct_image(): print information on corrections performed;
                  set_splinfil(): return without action if input buffer is NULL.
Update 13/02/2009 Peter Boesecke (boesecke@esrf.fr)
                  in map_imag: The displaced offsets of read xyfiles were 
                  twice as big as they should. They contained additionally 
                  the offset of the mapped region. This bug could only be 
                  observed for option psize_distort==2. Now, the displaced 
                  offsets are only divided by the binning factor.
Update 16/06/2008 Peter Boesecke (boesecke@esrf.fr) 
                  obsolete include "SaxsRoutine.h" removed.
Update 22/01/2008 R. Wilcke (wilcke@esrf.fr)
                  slightly modified many error messages in the program;
                  added routines interval_compare() and region_compare().
Update 20/12/2007 R. Wilcke (wilcke@esrf.fr)
                  set dimension of table "exptab" to USHRT_MAX + 1;
                  mark_overflow_nocorr(): scale the various limits (overflow,
                  minimum, maximum) with the exponentiation constant INPEXP if
                  it is set.
Update 17/12/2007 R. Wilcke (wilcke@esrf.fr)
                  scale_im(): add size of image to the argument list and change
                  code to use that value instead of XSIZE * YSIZE;
                  correct_image() and map_imag(): add image size to the
                  arguments of the scale_im() call.
Update 28/11/2007 R. Wilcke (wilcke@esrf.fr)
                  correct_image() and map_imag(): move linearity correction from
                  correct_image() to map_imag().
Update 26/11/2007 R. Wilcke (wilcke@esrf.fr)
                  expon_im() and set_inpexp(): replace exponential function by
                  a lookup table calculation for speed.
Update 10/04/2007 R. Wilcke (wilcke@esrf.fr)
                  add new global variable DO_DARK to suppress the dark image
                  correction;
                  add new function set_dodark() to set DO_DARK;
                  correct_image(): do dark image correction only if DO_DARK is
                  set.
Update 01/02/2007 P. Boesecke (boesecke@esrf.fr)
                  map_imag(): replace call to RebinFloat2d() with IpolRebin2().
Update 14/02/2007 R. Wilcke (wilcke@esrf.fr)
                  correct_image(): modify linearity correction if the source
                  or dark image are binned.
Update 12/02/2007 R. Wilcke (wilcke@esrf.fr)
                  set_dstrtval() and undistort_im(): for the update of the output
                  image header, do no longer use the special enumerated data,
                  but the general flags for the user data header.
Update 26/09/2006 R. Wilcke (wilcke@esrf.fr)
                  mark_overflow_nocorr(): add new argument containing the list
                  with the dummy pixels;
                  add new functions set_inpexp() to define a exponential
                  constant for the linearity correction and expon_im() to apply
                  this constant to an image;
                  correct_image(): add the exponentiation of the source image at
                  the very beginning of the corrections and equally modify code
                  for dark image subtraction to include exponentiation.
Update 10/03/2006 R. Wilcke (wilcke@esrf.fr)
                  set_dospd(): do no longer set LUT_INVALID in this routine.
Update 15/09/2005 R. Wilcke (wilcke@esrf.fr)
                  lut_calc(): make the two tests for MAX_PIXELSIZE identical.
Update 09/09/2005 R. Wilcke (wilcke@esrf.fr)
                  triangle_cutall(): modify the definition and thus also the
                  calculation of the cut lines; this also changes the returned
                  minimum and maximum grid lines in x and y;
                  andy_corr(): change code to calculate the coordinates along
                  the right and upper edges of the image as well;
                  andy_corr(): add Offset_1 and Offset_2 to the header flags of
                  the SDXTYP and SDYTYP buffers;
                  andy_fcorr(): write the displacement buffers to x and y files
                  if requested with XOUTFILE and YOUTFILE;
                  my_func(): allow calculations of corrected coordinates for the
                  right and upper edge of the image as well;
                  map_imag(): for displacement files (type SDXTYP and SDYTYP),
                  make output dimensions one bigger and set an offset of -0.5;
                  lut_calc(): for the loops over the source pixels, modify the
                  processing of their corners to include the edges of the image
                  in a consistent manner.
Update 24/08/2005 R. Wilcke (wilcke@esrf.fr)
                  lut_calc(): when compressing the program (in Step 5), allocate
                  a bigger buffer for the compressed LUT if necessary instead of
                  terminating the program when the buffer gets too small.
Update 19/08/2005 R. Wilcke (wilcke@esrf.fr)
                  replace multiplication by RELTABSIZE with shift by RELTABSH.
Update 13/04/2005 R. Wilcke (wilcke@esrf.fr)
                  lut_calc(): when creating multipixel target advances, check
                  that the counter does not overflow (unsigned short), and
                  create additional MULTIINC instructions if necessary.
Update 01/04/2005 R. Wilcke (wilcke@esrf.fr)
                  set_headval(): move from this routine to routine scanhead()
                  (file "inout.c") the initialization of header values that
                  are not set by the input data.
Update 24/03/2005 R. Wilcke (wilcke@esrf.fr)
                  azim_int(): change the way the "s" and average values are
                  preset for the averaged output array.
Update 25/01/2005 R. Wilcke (wilcke@esrf.fr)
                  map_imag(): handle the "displaced parameters" of the
                  distortion files;
                  undistort_im(): add Offset_1, Offset_2, BSize_1 and Bsize_2 to
                  the list of header values that will be updated.
Update 14/01/2005 R. Wilcke (wilcke@esrf.fr)
                  remove routine set_dolater() and global variable DO_LATER;
                  set_dospd(): add code for new functionality (possible values
                  now 0, 1, 2 or 3);
                  set_headval(): provide default values for the new header
                  members ProjTyp, DetRot_1, DetRot_2 and DetRot_3; 
                  undistort_im(): get the values for the new header members from
                  the distortion files if required;
                  azim_int(): remove input argument ave_waxs, add header flag
                  FL_PRO to the list of required header flags and use the header
                  members ProjTyp to determine Saxs / Waxs image type;
                  correct_image(): change code for processing the various
                  combinations of corrections and add the case DO_SPD == 3.
Update 21/09/2004 R. Wilcke (wilcke@esrf.fr)
                  change name and call of bin_imag() to map_imag().
Update 07/09/2004 R. Wilcke (wilcke@esrf.fr)
                  set_splinfil(): cleaned up code for the case of a new
                  distortion file;
                  andy_corr() and andy_free_buffers(): made x_buf and y_buf
                  local variables in andy_corr(), allocate and free them there
                  as well;
                  andy_corr(): allocation of cor_x and cor_y buffers is done by
                  get_buffer().
Update 31/08/2004 R. Wilcke (wilcke@esrf.fr)
                  divide_im() and divide_insito_im(): do no longer mark illegal
                  pixels of the flood field image with "Dummy" in the output
                  image;
                  mark_overflow_nocorr(): process also illegal pixels of the
                  flood field image;
                  correct_image(): fill temp_im buffer with 0. before marking
                  illegal pixels and add code to mark illegal pixels for the
                  flood field image;
                  normint_im(): use num_str2double() instead of atof() to get
                  value of header member Intens_1;
                  correct_image(): free structure members lut_d->xrel and
                  lut_d->yrel.
Update 30/08/2004 R. Wilcke (wilcke@esrf.fr)
                  set_imgbuf(): call to prepare_flood() for flood field images 
                  moved here from get_buffer() (file inout.c);
                  normint_im(): process all pixels, do not test for "Dummy";
                  prepare_flood(): return without action if NULL output buffer.
Update 25/08/2004 R. Wilcke (wilcke@esrf.fr)
                  azim_int(): add new input parameter and code to calculate the
                  scattering vector for an image that has been projected to the
                  Ewald sphere;
                  change name of global variable PSIZDIST to DSTRTVAL;
                  change name of routine set_psizdist() to set_dstrtval() and
                  change test for allowed values;
                  undistort_im(): add code to set center coordinates and sample
                  distance from the distortion files.
Update 24/08/2004 R. Wilcke (wilcke@esrf.fr)
                  trianglecutv_only(), trianglecuth_only(), andy_active_area(),
                  make_grid(), azim_int(), lut_calc(), debug_print():
                  remove unused variables;
                  trianglecuth_only(), triangle_cutall(), debutout(), my_func(),
                  andy_free_buffers(), andy_active_area(), make_grid(),
                  mark_overflow_nocorr(), undistort_im(), debug_print(), 
                  despair():
                  add "return(0)" at the end of the routine;
Update 18/08/2004 R. Wilcke (wilcke@esrf.fr)
                  replace call to rebin_float_2d() by RebinFloat2d() and remove
                  rebin_float_2d() function declaration.
Update 21/06/2004 R. Wilcke (wilcke@esrf.fr)
                  andy_loadspline(): add search for optional keyword "BINNING".
Update 18/06/2004 R. Wilcke (wilcke@esrf.fr)
                  bin_imag(): do not divide the binned pixels by the weight
                  factor for SRCTYP and DRKTYP.
Update 17/06/2004 R. Wilcke (wilcke@esrf.fr)
                  correct_image(): set temp_im pointer to NULL after pfree().
Update 29/03/2004 R. Wilcke (wilcke@esrf.fr)
                  set_headval(): set default values for header elements BSize_1
                  and BSize_2;
                  prepare_flood(): use size information from flood image if it
                  is defined;
                  remove conditional code for LOW_MEM (no longer used).
Update 19/03/2004 R. Wilcke (wilcke@esrf.fr)
                  add function bin_imag();
                  andy_corr(): add code for binning of the x- and y-distortion
                  files;
                  replace the functions set_bckgim(), set_drkim() and
                  set_floim() by function set_imgbuf().
Update 31/07/2003 R. Wilcke (wilcke@esrf.fr)
                  normint_im(): correct the multiplication factor for the pixel
                  values.
Update 21/07/2003 R. Wilcke (wilcke@esrf.fr)
                  add new global variable NORMFACT for the scattering intensity
                  normalization factor;
                  set_normint(): add the scattering intensity normalization
                  factor as second argument to the function and set NORMFACT;
                  normint_im(): multiply each pixel of the output image with
                  NORMFACT.
Update 11/03/2003 R. Wilcke (wilcke@esrf.fr)
                  correct_image(): explicitly initialize *temp_im to NULL.
Update 26/10/2002 R. Wilcke (wilcke@esrf.fr)
                  change default value of PSIZDIST from 1 to 0;
                  set_psizdist(): change comments to reflect the new possible
                  values of PSIZDIST;
                  set_psizdist(): change the function type from "void" to "int"
                  and return an error if the input value is illegal;
                  undistort_im(): add code to handle the new values of PSIZDIST;
                  mark_overflow_nocorr(): correct comments.
Update 24/10/2002 R. Wilcke (wilcke@esrf.fr)
                  andy_calcspline(): removed variable "buffer" (not needed);
                  andy_calcspline(): reorder output of BISPEV() routine to have x
                  coordinate index increase fastest;
                  my_func(): change the indexing algorithm for arrays X_COR and
                  Y_COR to agree with the new storage layout in andy_calcspline().
Update 26/09/2002 R. Wilcke (wilcke@esrf.fr)
                  move routine _prmsg() from "correct.c" to "inout.c";
                  remove routine set_verbose();
                  remove declaration of global variable "verbose";
                  lut_calc(): add flag PRERR to prmsg() call for "." (dot)
                  printing.
Update 17/09/2002 R. Wilcke (wilcke@esrf.fr)
                  andy_unloadspline() and area_only(): declare functions as "void";
                  subtract_im(), divide_insito_im() and divide_im(): return 0
                  for no error;
                  undistort_im(): return 0 if no error, -1 if error.
Update 16/09/2002 R. Wilcke (wilcke@esrf.fr)
                  _prmsg(): initialize "errmsg" = NULL, call strerror() only if
                  errno != 0, print MSG level messages if "verbose" >= 0;
                  reallocate the message levels for the prmsg() calls;
                  lut_calc(): do BOUND_CHECK tests independent of "verbose".
Update 05/09/2002 R. Wilcke (wilcke@esrf.fr)
                  set_headval() and normint_im(): change code to treat
                  "data_head" members "Intens_0" and "Intens_1" as string
                  instead of "float".
Update 03/09/2002 R. Wilcke (wilcke@esrf.fr)
                  change references from "background correction" to "dark image
                  correction", this includes changing all corresponding variable
                  names from "bkg...." to "drk....";
                  rename enumerated variable BKGTYP to DRKTYP;
                  rename BKG_CONST and BKG_IM to DRK_CONST and DRK_IM;
                  change function names set_bkgconst() and set_bkgim() to
                  set_drkconst() and set_drkim();
                  change variable names referring to "scattering background
                  correction" from "sca...." to "bckg....";
                  rename enumerated variable SCATYP to SBKTYP;
                  change function names set_scaconst(), set_scafact() and
                  set_scaim() to set_bckgconst(), set_bckgfact and set_bckgim();
                  rename SCACONST, SCAFACT and SCA_IM to BCKGCONST, BCKGFACT
                  and BCKG_IM;
Update 02/09/2002 R. Wilcke (wilcke@esrf.fr)
                  prmsg() and _prmsg(): print in _prmsg() all output to "stdout"
                  instead of "stderr". This in turn determines where the prmsg()
                  output goes to.
Update 30/05/2002 R. Wilcke (wilcke@esrf.fr)
                  mark_overflow(): mark all target pixels as "dummy" that have
                  no source pixels mapped onto them;
                  correct_image(): add INPFACT and INPCONST corrections for
                  the case that only intensity normalization or scattering
                  background correction are to be performed.
Update 29/05/2002 R. Wilcke (wilcke@esrf.fr)
                  correct_image(): modify the program flow to separate into
                  three clearly distinguished steps: corrections, normalization
                  and marking of invalid pixels.
Update 14/03/2002 R. Wilcke (wilcke@esrf.fr)
                  set_xycorin(): free the "spline" structure if it exists.
Update 13/03/2002 R. Wilcke (wilcke@esrf.fr)
                  andy_fcorr(): replace read_esrf_file() call by get_buffer();
                  andy_corr(): call andy_fcorr() before freeing buffers;
                  andy_corr(): reset SPLINE_INVALID only if the new spline
                  coefficients could be successfully obtained;
                  andy_fcorr(): return without action if SPLINE_INVALID is
                  false, and reset SPLINE_INVALID if the routine is successful;
                  andy_calcspline(): return with error if "spline" structure is NULL.
Update 12/03/2002 R. Wilcke (wilcke@esrf.fr)
                  use macro MAXTYP to declare the size of "img_head" and use the
                  C default value 0 to initialize the "init" members;
                  get_headval() and set_headval(): use 0 and MAXTYP to test for
                  valid values of the "type" input argument;
                  andy_corr() and andy_fcorr(): replace data type SPDTYP by the
                  two new data types SDXTYP and SDYTYP.
Update 05/03/2002 R. Wilcke (wilcke@esrf.fr)
                  lut_calc(): remove freeing "lut_d->temp_im" (no longer used).
Update 04/03/2002 R. Wilcke (wilcke@esrf.fr)
                  andy_fcorr(): test if the x and y pixel sizes are defined in
                  the files with the distortion values, and if the definitions
                  are identical in both files;
                  undistort_im(): test for illegal or non-defined pixel sizes;
                  andy_corr(): put the x and y pixel sizes in the files with the
                  distortion values;
                  andy_corr(): use put_buffer() instead of save_esrf_files() to
                  write the distortion values to file.
Update 01/03/2002 R. Wilcke (wilcke@esrf.fr)
                  andy_corr() and andy_fcorr(): free distortion buffers only in
                  andy_corr(), and call andy_fcorr() after this is done;
                  set_splinfil(): modify test for new file;
                  set_xycorin(): add test for new files.
Update 28/02/2002 R. Wilcke (wilcke@esrf.fr)
                  lut_calc():  test for fxmin and fymin "less or equal than 0"
                  instead of just "less than 0" to avoid overwriting boundaries
	          of array "offsets".
Update 25/02/2002 R. Wilcke (wilcke@esrf.fr)
                  lut_calc(): introduce additional variables xsize1, ysize1 and
                  xysize to avoid recalculating values.
Update 21/02/2002 R. Wilcke (wilcke@esrf.fr)
                  andy_fcorr(): set cols = XSIZE, rows = YSIZE to have a size
                  defined for the read_esrf_file() call;
                  rename global variable FILENAME to DISTFILE;
                  set_splinfil(), andy_loadspline() and gtest(): change variable
                  name "filename" to "distfile".
Update 23/01/2002 R. Wilcke (wilcke@esrf.fr)
                  mark_overflow(): return -1 in case of error;
                  lut_calc(): return NULL pointer in case of error;
                  correct_image(): return with error if calculation of look-up
                  table failed.
Update 22/01/2002 R. Wilcke (wilcke@esrf.fr)
                  andy_fcorr() and andy_corr(): test for read errors in the
                  input files and return -1 if there are any;
                  my_func(): return with error -1 if andy_corr() returns error.
Update 20/12/2001 R. Wilcke (wilcke@esrf.fr)
                  andy_corr(): remove last argument (title) in save_esrf_file()
                  call and add "SPDTYP" as new 5th argument.
Update 19/12/2001 R. Wilcke (wilcke@esrf.fr)
                  andy_corr(): remove the last input argument in the call to
                  save_esrf_file().
Update 13/12/2001 R. Wilcke (wilcke@esrf.fr)
                  changed the initialization value of FILENAME from
                  "spatial.dat" to NULL and modify set_splinfil() accordingly.
Update 03/12/2001 R. Wilcke (wilcke@esrf.fr)
                  _prmsg(): print system error messages only if "errno" has a
                  valid value.
Update 03/12/2001 R. Wilcke (wilcke@esrf.fr)
                  get_headval(), set_headval() and azim_int(): add code to
                  process the new header type AVETYP;
                  azim_int(): add the averaging buffer and the scale factor to
                  the input arguments and modify code to handle these arguments.
Update 28/11/2001 R. Wilcke (wilcke@esrf.fr)
                  azim_int(): add "SampleDistance" and "Wavelength" to the
                  required header values;
                  azim_int(): copy CORTYP header structure to AZITYP structure;
                  azim_int(): calculate "s" and averaged angle value for each
                  radial value;
Update 26/11/2001 R. Wilcke (wilcke@esrf.fr)
                  azim_int(): remove test for the range of the angular
                  integration;
                  azim_int(): change value for I0Offset_2;
                  azim_int(): change the input arguments by providing the radial
                  and angular dimensions instead of the end values, and change
                  code of the routine accordingly;
                  get_headval and set_headval(): type can now also be "AZITYP".
Update 22/11/2001 R. Wilcke (wilcke@esrf.fr)
                  increase dimension of structure img_head to 8.
Update 20/11/2001 R. Wilcke (wilcke@esrf.fr)
                  correct_image(): always allocate space for temporary image
                  if there is none yet.
Update 19/11/2001 R. Wilcke (wilcke@esrf.fr)
                  add new global variables INPCONST, INPFACT, SCACONST, SCAFACT
                  and functions set_inpconst(), set_inpfact, set_scaconst(),
                  set_scafact() to set their values;
                  add new function scale_im() to adjust the input image with
                  a multiplicative and an additive constant;
                  correct_image(): use new function scale_im() to adjust the
                  values of the input and the scattering background images.
Update 16/11/2001 R. Wilcke (wilcke@esrf.fr)
                  add new global variable PSIZDIST and a function set_psizdist()
                  to set its value;
                  undistort_im(): set pixel size for corrected image from
                  distortion files only if PSIZDIST is set.
Update 14/11/2001 R. Wilcke (wilcke@esrf.fr)
                  azim_int(): correct values and test for the range of the
                  angular integration.
Update 13/11/2001 R. Wilcke (wilcke@esrf.fr)
                  mark_overflow_nocorr(): test illegal values "IMAGE_OVER",
                  "INP_MIN" and "INP_MAX" only for source and dark current
                  images;
                  correct_image(): do test for illegal "Dummy" values after
                  performing the scattering background subtraction.
Update 12/11/2001 R. Wilcke (wilcke@esrf.fr)
                  mark_overflow_nocorr(): separate the tests for illegal values
                  "IMAGE_OVER" and "dummy" in two loops for better performance;
                  mark_overflow_nocorr(): add the 3. input argument "type" and
                  replace the fixed index "SRCTYP" by this new index "type";
                  mark_overflow_nocorr(): return without action if the input
                  image buffer is a NULL pointer;
                  correct_image(): add the 3. input argument "SRCTYP" to the
                  mark_overflow_nocorr() calls;
                  correct_image(): call mark_overflow_nocorr() with the dark
                  current image if there is one;
Update 09/11/2001 R. Wilcke (wilcke@esrf.fr)
                  change the scope of the first "#if ANDY_CORR" clause - as it
                  was, all global variable declarations were in it.
Update 08/11/2001 R. Wilcke (wilcke@esrf.fr)
                  andy_corr() and andy_fcorr(): save the x- and y-pixel sizes
                  when obtained from the (spline or coordinate) files;
                  undistort_im(): set the header values PSize_1 and PSize_2 for the
                  corrected image from the saved x- and y-pixel sizes;
                  add global variables INP_MAX and INP_MIN, which define the
                  maximum and minumum allowed values in the source and the dark
                  images, and add routines set_inpmax() and set_inpmin() to set
                  their values;
                  mark_overflow_nocorr(): add test for values less than INP_MIN
                  or greater than INP_MAX;
Update 05/11/2001 R. Wilcke (wilcke@esrf.fr)
                  azim_int(): add code to restrict integration to the range
                  given by either the input arguments or the dimension of the
                  input image.
Update 05/11/2001 R. Wilcke (wilcke@esrf.fr)
                  azim_int(): add 2 more new input arguments.
Update 31/10/2001 R. Wilcke (wilcke@esrf.fr)
                  azim_int(): add calculation of maximum radius for the
                  azimuthal integration.
Update 22/10/2001 R. Wilcke (wilcke@esrf.fr)
                  azim_int(): add 3 new input arguments for start radius and
                  angle as well as angular increment.
Update 18/10/2001 R. Wilcke (wilcke@esrf.fr)
                  set_dummy(): set value of "DDummy" to reflect new value of
                  "Dummy";
                  normint_im(): take "ddummy" from the CORTYP header structure;
                  add new routine azim_int() to integrate an image azimuthally.
Update 03/10/2001 R. Wilcke (wilcke@esrf.fr)
                  andy_corr(): change last argument in save_esrf_file() from
                  NULL to -1.
Update 02/10/2001 R. Wilcke (wilcke@esrf.fr)
                  andy_corr(): remove the last two arguments in the call to
                  save_esrf_file().
Update 14/09/2001 R. Wilcke (wilcke@esrf.fr)
                  correct_image(): return -2 if no buffer for corrected image
                  was given, -1 if any of the corrections failed, and 0 else.
Update 13/09/2001 R. Wilcke (wilcke@esrf.fr)
                  normint_im(): corrected test for the required header values.
Update 11/09/2001 R. Wilcke (wilcke@esrf.fr)
                  mark_overflow(): correct error in the processing of the last
                  pixel: in the "for" loop for "j", replace "YSIZE" by "XSIZE"
                  as it is the case in the loop for all other pixels;
                  lut_calc(): correct code for filling the lut_d->xrel and
                  lut_d->yrel arrays, because the input arrays x_trg and y_trg
                  have one pixel more in each dimension than xrel and yrel.
Update 20/08/2001 R. Wilcke (wilcke@esrf.fr)
                  add function get_headval() to obtain the values of the header
                  structures;
                  set_headval(): define default values for structure members
                  Dummy, DDummy, Offset_1, Offset_2, Orientat and Intens_1;
                  correct_image(): if no correction is to be done, just copy the
                  input file to the output file;
                  set_dummy(): put the new "Dummy" in the header structure of
                  the corrected image (type = CORTYP);
                  mark_overflow_nocorr(): write the output "Dummy" value into
                  the header of the corrected image;
                  correct_image(): allocate the space for the temporary image no
                  longer in the structure "lut_d", but in a separate static
                  variable "temp_im";
Update 17/08/2001 R. Wilcke (wilcke@esrf.fr)
                  set_headval(): do no longer set member "init" to 1 in the
                  routine, as "init" now contains the "OR"ed flags of all header
                  keywords that have been found;
                  normint_im(): test "init" member for the required keywords;
                  mark_overflow_nocorr() and prepare_flood(): test "init" member
                  for the keywords "Dummy" and "DDummy";
                  mark_overflow(): return immediately without action if
                  "image_over" is not a valid dummy value;
                  change dimension of structure array img_head from 6 to 7;
                  set_headval(): "type" can now also be "SPDTYP";
                  andy_fcorr(): fill the members PSize_1 and PSize_2 of the
                  header structure for the corrected image from the files for
                  the corrected x and y coordinates;
                  andy_fcorr() and andy_corr(): or the flags for PSize_1 and
                  PSize_2 into the "init" member of the corrected image header;
                  move the declarations of andy_active_area() and andy_fcorr()
                  to the beginning of the file;
                  correct_image(): free the members of structure "lut_d" only if
                  they do not contain NULL pointers.
Update 16/08/2001 R. Wilcke (wilcke@esrf.fr)
                  correct_image(): change code to process the marking of illegal
                  pixels correctly;
                  mark_overflow() and mark_overflow_nocorr(): use only "Dummy"
                  to mark illegal pixel values in the output;
                  prepare_flood(): use the value of the keyword "Dummy" in the
                  floodfield image header to find illegal pixels in the
                  floodfield image;
                  normint_im(): do not process pixels with illegal values;
                  set_actrad(): take new value only if different from old one.
Update 14/08/2001 R. Wilcke (wilcke@esrf.fr)
                  andy_fcorr(): add new fifth argument (type = SPDTYP) to the
                  read_esrf_file() call.
Update 13/08/2001 R. Wilcke (wilcke@esrf.fr)
                  rename structure variable "src_head" to "img_head", make it
                  an array with dimension 6 and initialize all their "init"
                  members to 0;
                  set_headval(): change the type of the function from "void" to
                  "int", add a second input argument to the function, change
                  code to copy the input header to particular element in the
                  header structure array, copy a SRCTYP input header also to
                  the CORTYP structure and return an error if the "type" of the
                  structure requested has an illegal value;
                  andy_corr(): put the x- and y-pixel sizes in the header
                  structure element CORTYP;
                  normint_im(): take the values from the header structure
                  element CORTYP;
                  prepare_flood(): add code to take the values for "dummy" and
                  "ddummy" from the FLOTYP header structure, if this has been
                  initialized.
Update 10/08/2001 R. Wilcke (wilcke@esrf.fr)
                  subtract_im(): always subtract the background constant;
                  correct_image(): do corrections even if only NORM_INT or
                  SCA_IM are specified;
                  normint_im(): add output buffer as second input argument;
Update 07/08/2001 R. Wilcke (wilcke@esrf.fr)
                  add global variable SCA_IM, which points to the scattering
                  background image, and routine set_scaim() to set its value;
                  subtract_im(): remove variable "negative";
                  subtract_im(): add fourth input argument for the background
                  constant and modify the references to BKG_CONST accordingly;
                  correct_image(): add forth input argument to the calls to
                  subtract_im();
                  correct_image(): correct error in the "distortion floodfield
                  (floodfield after distortion)" case: wrong source image for
                  undistort_im() call was specified;
                  correct_image(): add code to perform the scattering background
                  correction;
                  andy_corr(): divide the x- and y-pixel sizes by 1000000.
Update 06/08/2001 R. Wilcke (wilcke@esrf.fr)
                  andy_corr(): put the corrected x- and y-pixel sizes in the
                  data header structure.
Update 03/08/2001 R. Wilcke (wilcke@esrf.fr)
                  correct_image(): add variable "bkg_cor" to indicate if a
                  background correction is to be done;
                  add global variable NORM_INT for the normalization to absolute
                  scattering intensities and routine set_normint() to set its
                  value;
                  add struct src_head and routine set_headval() to set its
                  values;
                  add routine normint_im() to calculate the normalization to
                  absolute scattering intensities;
                  correct_image(): reorganize the logical structure of the
                  decision which corrections are to be made, and add the call
                  to normint_im() if NORM_INT is set.
Update 28/06/2001 R. Wilcke (wilcke@esrf.fr)
                  add routines set_bkgim() and set_floim() to set the values
                  of BKG_IM and FLO_IM, which point to the background image and
                  the floodfield image;
                  change routine correct_image() by eliminating the background
                  image and the floodfield image from the input arguments and
                  modifying the code to use the new global variables BKG_IM and
                  FLO_IM;
                  removed declaration of variable one_spline (not used);
                  andy_unloadspline(): return without action if spline == NULL.
Update 26/06/2001 R. Wilcke (wilcke@esrf.fr)
                  change the name of the variables XFILE and YFILE to XINFILE
                  and YINFILE;
                  add variables XOUTFILE and YOUTFILE for the names of files
                  where the distortion correction values can be written to;
                  andy_corr(): make the output of the distortion correction
                  values dependent on "XOUTFILE && YOUTFILE" instead of
                  "verbose == 2" and use the filenames in XOUTFILE and YOUTFILE
                  instead of the fixed names "cor_x.edf" and "cor_y.edf";
                  add new function set_xycorout() to set the value of XOUTFILE
                  and YOUTFILE.
Update 25/06/2001 R. Wilcke (wilcke@esrf.fr)
                  remove variable DO_XYFILE;
                  andy_corr(): replace test for DO_XYFILE with test for
                  "XFILE && YFILE";
                  move declaration of variable "verbose" here from "spd.h";
                  change type of BKG_CONST from "int" to "float";
                  remove declaration of the parameters for the geometrical
                  method (CURV_RADIUS, XCENTER and YCENTER) and all related
                  code. This method is no longer used;
                  make_grid(): set the variable CURV_RADIUS in this function to
                  the old default value of CURV_RADIUS = 270000/180;
                  add new functions:
                    - set_verbose() to set the value of "verbose";
                    - set_overflow() to set the value of IMAGE_OVER and
                      IMAGE_OVER_SET;
                    - set_dummy() to set the value of "Dummy";
                    - set_actrad() to set the value of ACTIVE_R;
                    - set_slinfil() to set the value of FILENAME;
                    - set_xycorin() to set the value of XFILE and YFILE;
                    - set_xysize() to set the value of XSIZE and YSIZE;
                    - get_xsize() to get the value of XSIZE;
                    - get_ysize() to get the value of YSIZE;
                    - set_bkgconst() to set the value of BKG_CONST;
                    - set_dospd() to set the value of DO_SPD;
                    - set_dolater() to set the value of DO_LATER;
                    - set_doflat() to set the value of DO_FLAT;
Update 12/06/2001 R. Wilcke (wilcke@esrf.fr)
                  change variable name "DUMMY" to "Dummy" to avoid name conflict
                  with macro DUMMY().
Update 14/05/2001 R. Wilcke (wilcke@esrf.fr)
                  andy_corr(): replace SHM_FLOAT by MFloat in the call to
                  save_esrf_file().
Update 03/05/2001 R. Wilcke (wilcke@esrf.fr)
                  andy_fcorr(): replace SHM_FLOAT by MFloat in the call to
                  read_esrf_file().
Update 26/04/2001 R. Wilcke (wilcke@esrf.fr)
                  prepare_flood(): add the possibility that the input image
                  is already of type "float", and just invert the image in this
                  case.
Update 14/02/2001 R. Wilcke (wilcke@esrf.fr)
                  divide_im() and divide_insito_im(): remove the code that tests
                  for pixel values >= IMAGE_OVER and replaces them with
                  IMAGE_OVER_SET. This is no longer needed for "float" arrays;
                  correct_image(): remove the calls to "mark_overflow_nocorr()"
                  for the cases where the floodfield correction is done before
                  before the distortion correction, and call "mark_overflow()"
                  with the original source image. This is a consequence of the
                  changes to "divide_im()" and "divide_insito_im()" described
                  above;
                  mark_overflow_nocorr() and mark_overflow(): to mark pixels
                  with overflow in the output images, use the value DUMMY if it
                  is set (i.e., != 0.), otherwise IMAGE_OVER_SET.
Update 13/02/2001 R. Wilcke (wilcke@esrf.fr)
                  subtract_im(): allow the background subtraction to yield
                  negative values in the output image (they were set =0 before),
                  and remove the diagnostic message about negative values;
                  clean up global variables: remove C_XSIZE, C_YSIZE;
                  added global variable DUMMY;
                  undistort_im(): set the value of empty pixels in the target 
                  image
                  to DUMMY (they were set to 0. before).
Update 06/02/2001 R. Wilcke (wilcke@esrf.fr)
                  changed the arguments to the "BISPEV" call (they must all be
                  pointers because of the FORTRAN calling convention).
Update 02/02/2001 R. Wilcke (wilcke@esrf.fr)
                  divide_insito_im() and divide_im(): change routines to work
                  with an input image of type "float": convert the corresponding
                  function arguments and local variables to type "float" and
                  change code to operate on floats;
                  substract_im(): change name to subtract_im() and convert
                  routine to work with an input image of type "float": convert
                  the corresponding function arguments and local variables to
                  type "float" and change code to operate on floats;
                  mark_overflow() and mark_overflow_nocorr(): change routines to
                  work with an input image of type "float": convert the
                  corresponding function arguments and local variables to type
                  "float" and change code to operate on floats;
                  undistort_im(): change routine to work with an input image of
                  type "float": convert the corresponding function arguments
                  and local variables to type "float" and change code to operate
                  on floats;
                  remove function undistort_im_plus();
                  correct_image(): change routine to work with an input image of
                  type "float": convert the corresponding function arguments
                  and allocate "float" memory space for "lut_d->temp_im";
Update 17/01/2001 R. Wilcke (wilcke@esrf.fr)
                  andy_calcspline(): replaced call to E02DEF() by call to 
                  BISPEV();
                  changed type of arguments accordingly from "double" to "float"
                  everywhere in the program.
Update 10/01/2001 R. Wilcke (wilcke@esrf.fr)
                  mark_overflow() and gtest(): replaced call to pxcorr() by
                  call to andy_calcspline();
                  removed routine pxcorr() and all references to E02DEF().
*/

#include "spd.h"

static char *DISTFILE = NULL;
static char *XINFILE = NULL;
static char *YINFILE = NULL;
static char *MINFILE = NULL;
static char *XOUTFILE = NULL;
static char *YOUTFILE = NULL;
static char *MOUTFILE = NULL;
static unsigned long IMAGE_OVER = 0;
static unsigned long IMAGE_OVER_SET = 0;
static int DO_DARK = 0;
static int DO_SPD = 1;
static int DO_PREROT = 0;
static int NORM_PREROT = 1;
static int DO_FLAT = 0;
static int NORM_INT = 0;
static int XSIZE = 1024;                   /* x-size of the src image */
static int YSIZE = 1024;                   /* y-size of the src image */
static unsigned long DSTRTVAL = 0;
static float ACTIVE_R = 0.;
static float DRK_CONST = 0.;
static float INPCONST = 0.;
static float INPEXP = 1.;
static float exptab[USHRT_MAX + 1];
static float INPFACT = 1.;
static float BCKGCONST = 0.;
static float BCKGFACT = 1.;
static float Dummy = 0.;
static float INP_MAX = 0.;
static float INP_MIN = 0.;
static float NORMFACT = 1.;
static float *DRK_IM = NULL;
static float *FLO_IM = NULL;
static float *BCKG_IM = NULL;

static float psizex = -1.,psizey = -1.; //+++++++++++ used????

/*
 * Declare structure for data header. The "init" member is by C default
 * initialized to 0, which indicates that the structure values are not set.
 */
static struct data_head img_head[MAXTYP];

static int LUT_INVALID = 1;
static int SPLINE_INVALID = 1;
static long *fldumlst = NULL;
static long *drdumlst = NULL;
static float *X_COR = NULL;
static float *Y_COR = NULL;
static float *M_COR = NULL;

/*
 * The variable SDX_PREROT keeps the type of the
 * distortion arrays. The value is set to
 * DO_PREROT when the distortion arrays are
 * created with spd_corr. When DO_SPD is set
 * and SDX_PREROT differs from DO_PREROT the 
 * look-up table is invalid and must be
 * recalculated.
 */
static int SDX_PREROT = -1;

/* ANDY_CORR definitions BEGIN */

#ifdef UNDERSCORE
#define BISPEV bispev_
void bispev_();
#else
#ifdef UPPERCASE
#define BISPEV BISPEV
void BISPEV();
#else
#define BISPEV bispev
void bispev();
#endif /* UPPERCASE */
#endif /* UNDERSCORE */

struct spd_spline {
  int dx_xknots;
  int dx_yknots;
  double xcor_size;
  double xcenter;
  float *xlambda;
  float *xmu;
  float *xpars;
  int dy_xknots;
  int dy_yknots;
  double ycor_size;
  double ycenter;
  float *ylambda;
  float *ymu;
  float *ypars;
  double grid_space;
  float *wrk;
  int *iwrk;
  int maxspace;
  double reg_x0; /* Valid region from to */
  double reg_x1;
  double reg_y0;
  double reg_y1;
  float bsize_1,bsize_2;
};

static struct spd_spline *spline = NULL;

/*==============================================================================
 * Reads a number of values from the input file and fills them into an array.
 *
 * Input : fp:  stream pointer referring to the input file
 *         no:  the number of values to read
 *         arr: pointer to the array where the values will be stored
 * Output: none
 * Return: 0  if requested number of values was read
 *         1  otherwise
 */

int spd_readarray(FILE *fp,int no,float *arr)
{
  int i, j, idx=0;
  char *line,*s;
  char dammed_minus_copy[512];

  while((line = spd_fgets(fp)) != NULL) {

    /*
     * Handles the special case that the input might contain tokens that are
     * separated by a "-" (minus) instead of a blank, but the "-" is actually
     * part of the next token (e.g. 2345-4567). Insert a blank before the "-".
     */
    for(i=0, j=0; i <= strlen(line); i++) {
      if(i && line[i] == '-' && line[i-1] != ' ')
        dammed_minus_copy[j++] = ' ';
      dammed_minus_copy[j++] = line[i];
    }
    line = dammed_minus_copy;

    for(i=0; i< VALUES_PERLINE; i++) {
      if((s = (char*) strlib_tok(line," ")) == NULL)  {
        prmsg(ERROR,("not enough values in file\n"));
        return(1);
      }
      line = NULL;
      arr[idx++] = atof(s);
      if(idx == no)
        return(0);
    }
  }
  prmsg(ERROR,("unexpected end of file\n"));
  return(1);
} /* spd_readarray */

/*==============================================================================
 * Searches the input file for a line containing a specified keyword.
 *
 * Input : fp:      stream pointer referring to the input file
 *         keyword: the keyword to search for
 * Output: none
 * Return: 0  if found
 *         1  otherwise
 */

int spd_findkeyword(FILE *fp,char *keyword)
{
  char *line;

  while((line = spd_fgets(fp)) != NULL) {
    if((char *)strstr(line,keyword) != NULL)
      return(0);
  }
  prmsg(ERROR,("missing keyword \"%s\" in file \n",keyword));
  return(1);
} /* spd_findkeyword */

/*==============================================================================
 * Reads the spline function parameters from a file and fills them into the
 * corresponding elements of a newly created spd_spline structure.
 *
 * Memory for the arrays of the structure is allocated by the function itself
 * and can be freed by a call to spd_unloadspline().
 *
 * Input : distfile: name of the file that contains the spline function
 *                   parameters
 * Output: none
 * Return: if successful: pointer to the created spline function structure
 *         else:          NULL
 */

struct spd_spline *spd_loadspline(char *distfile)
{
  FILE *fp;
  char *line;
  struct spd_spline *spline;
  int nopar,maxspace;

  if(distfile == NULL) {
    prmsg(ERROR,("spd_loadspline: file name empty\n"));
    return(NULL);
  }

  if((fp = fopen(distfile,"r")) == NULL) {
    prmsg(ERROR,("spd_loadspline: cannot read <%s> (open failed)\n",distfile));
    return(NULL);
  }

  if((spline = (struct spd_spline *)pmalloc(sizeof(struct spd_spline))) == NULL)
    goto memerror;

  /*
   * Read in binning size in pixels.
   * This is an optional keyword; if it is not found, its value is 1. Just
   * rewind the file and continue with the other keywords.
   */
  spline->bsize_1 = 1.;
  spline->bsize_2 = 1.;
  if(spd_findkeyword(fp,"BINNING")) {
    rewind(fp);
    prmsg(MSG,("Binning size set to default = 1.\n"));
  } else if(((line = spd_fgets(fp)) == NULL) || (sscanf(line,"%f %f",
    &(spline->bsize_1),&(spline->bsize_2)) != 2))
    goto readerror;

  /* Read in valid region in pixels */
  if(spd_findkeyword(fp,"VALID REGION"))
    goto readerror;

  if(((line = spd_fgets(fp)) == NULL) || (sscanf(line,"%lf %lf %lf %lf",
    &(spline->reg_x0),&(spline->reg_y0),&(spline->reg_x1),&(spline->reg_y1))
    != 4))
    goto readerror;

  if(spd_findkeyword(fp,"GRID SPACING"))
    goto readerror;

  if(((line = spd_fgets(fp)) == NULL) || (sscanf(line,"%lf %lf %lf",
    &(spline->grid_space),&(spline->xcor_size),&(spline->ycor_size)) != 3))
    goto readerror;

  /* Read in X Distortion data */
  if(spd_findkeyword(fp,"X-DISTORTION"))
    goto readerror;

  if(((line = spd_fgets(fp)) == NULL) ||
    (sscanf(line,"%d %d",&(spline->dx_xknots),&(spline->dx_yknots)) != 2))
    goto readerror;

  nopar = (spline->dx_yknots-4) * (spline->dx_xknots-4);

  if(!(spline->xpars = (float *)pmalloc(sizeof(float) * nopar)) ||
    !(spline->xmu = (float *)pmalloc(sizeof(float) * spline->dx_yknots)) ||
    !(spline->xlambda = (float *)pmalloc(sizeof(float) * spline->dx_xknots)))
    goto memerror;

  if(spd_readarray(fp,spline->dx_xknots,spline->xlambda))
    goto readerror;

  if(spd_readarray(fp,spline->dx_yknots,spline->xmu))
    goto readerror;

  if(spd_readarray(fp,nopar,spline->xpars))
    goto readerror;

  /* Read in Y Distortion data */
  if (spd_findkeyword(fp,"Y-DISTORTION"))
    goto readerror;

  if(((line = spd_fgets(fp)) == NULL) ||
    (sscanf(line,"%d %d",&(spline->dy_xknots),&(spline->dy_yknots)) != 2))
    goto readerror;

  nopar = (spline->dy_yknots-4) * (spline->dy_xknots-4);

  if(!(spline->ypars = (float *)pmalloc(sizeof(float) * nopar)) ||
    !(spline->ymu = (float *)pmalloc(sizeof(float) * spline->dy_yknots)) ||
    !(spline->ylambda = (float *)pmalloc(sizeof(float) * spline->dy_xknots)))
    goto memerror;

  maxspace = 3000 * 5 * 2;
  spline->maxspace = maxspace;

  if(!(spline->wrk = (float *)pmalloc(sizeof(float) * maxspace)) ||
    !(spline->iwrk = (int *)pmalloc(sizeof(int) * maxspace)))
    goto memerror;

  if(spd_readarray(fp,spline->dy_xknots,spline->ylambda))
    goto readerror;

  if(spd_readarray(fp,spline->dy_yknots,spline->ymu))
    goto readerror;

  if(spd_readarray(fp,nopar,spline->ypars))
    goto readerror;

  fclose(fp);
  return(spline);

 readerror:
  prmsg(ERROR,("spd_loadspline: error reading from file <%s>\n",distfile));
  fclose(fp);
  return(NULL);

 memerror:
  prmsg(ERROR,("spd_loadspline: cannot allocate memory\n"));
  fclose(fp);
  return(NULL);

} /* spd_loadspline */

/*==============================================================================
 * Free the buffers allocated for the input spline function structure.
 *
 * Input : spline: structure with the parameters for the spline function
 * Output: none
 * Return: none
 */

void spd_unloadspline(struct spd_spline *spline)
{
  if(!spline)
    return;
  if(spline->xlambda)
    pfree(spline->xlambda);
  if(spline->xmu)
    pfree(spline->xmu);
  if(spline->xpars)
    pfree(spline->xpars);
  if(spline->ylambda)
    pfree(spline->ylambda);
  if(spline->ymu)
    pfree(spline->ymu);
  if(spline->ypars)
    pfree(spline->ypars);
  if(spline->wrk)
    pfree(spline->wrk);
  if(spline->iwrk)
    pfree(spline->iwrk);
  if(spline)
    pfree(spline);
} /* spd_unloadspline */

/*==============================================================================
 * Computes the spline function values for x and y direction at the grid formed
 * by the input coordinate points. It will calculate dx, dy at all the
 * intersections of the px, py arrays. E.g. with px = [2,3] and py = [7,8] this
 * routine will calculate dx,dy for the 4 points ((2,7), (2,8), (3,7), (3,8)).
 *
 * Input : spline:   structure with the parameters of the x and y spline
 *                   functions
 *         nox, noy: number of points in x and y arrays (= size in x and in y)
 *         px, py:   arrays with the x and y coordinates
 * Output: dx, dy: arrays with the x and y spline values at the corresponding
 *                 (px,py) coordinate points (will be nox * noy points)
 * Return: 0  if no errors
 *         -1 if no valid "spline" structure or memory allocation error
 *         error code from BISPEV call otherwise
 */

int spd_calcspline(struct spd_spline *spline,int nox,int noy,float px[],float py[],
  float dx[],float dy[])
{
  int ifail=0,i,j;
  int lwrk = spline->maxspace;
  int liwrk = spline->maxspace;
  int ideg = 3;
  float *ddx,*ddx_p;
  float *dx_p = dx, *dy_p = dy;

  if(spline == NULL) {
    prmsg(ERROR,("no valid spline structure in pxcalcgrid\n"));
    return(-1);
  }

  if((ddx = (float *)pmalloc(nox * noy * sizeof(float))) == NULL) {
    prmsg(ERROR,("no memory in pxcalcgrid\n"));
    return(-1);
  }

  /*
   * Note that BISPEV() is a FORTRAN routine, and that the FORTRAN calling
   * convention expects all arguments to be pointers. Thus even the constants
   * used for the degree of the spline must be given as pointers to variables
   * (in this case the variable "ideg").
   *
   * Furthermore, the storing convention of BISPEV() is that the y coordinate
   * index is increased fastest, whereas the "correct" package increases the x
   * coordinate index fastest. Thus the BISPEV() output array "ddx" has to
   * be reordered to agree with the "correct" storage convention.
   */

  /*
   * Calculate corrections in x direction.
   */
  BISPEV(spline->xlambda,&spline->dx_xknots,spline->xmu,&spline->dx_yknots,
    spline->xpars,&ideg,&ideg,px,&nox,py,&noy,
    ddx,spline->wrk,&lwrk,spline->iwrk,&liwrk,&ifail);

  /*
   * Reorder BISPEV() output to have x coordinate index increase fastest.
   */
  for(j = 0; j < noy; j++)
    for(i = 0, ddx_p = ddx + j; i < nox; i++, ddx_p += noy)
      *dx_p++ = *ddx_p;

  if(ifail)
    prmsg(ERROR,("error %d in BISPEV\n",ifail));

  /*
   * Calculate corrections in y direction.
   */
  BISPEV(spline->ylambda,&spline->dy_xknots,spline->ymu,&spline->dy_yknots,
    spline->ypars,&ideg,&ideg,px,&nox,py,&noy,
    ddx,spline->wrk,&lwrk,spline->iwrk,&liwrk,&ifail);

  /*
   * Reorder BISPEV() output to have x coordinate index increase fastest.
   */
  for(j = 0; j < noy; j++)
    for(i = 0, ddx_p = ddx + j; i < nox; i++, ddx_p += noy)
      *dy_p++ = *ddx_p;

  pfree(ddx);

  if(ifail)
    prmsg(ERROR,("error %d in BISPEV\n",ifail));

  return(ifail);
} /* spd_calcspline */

/*==============================================================================
 */
char *spd_fgets(FILE *fp)
{
  static char buf[1024];
  return fgets(buf,1024,fp);
} /* spd_fgets */

/*==============================================================================
 * Free the x and y buffers as well as the buffers for the corrected
 * coordinates.
 *
 * Buffers that have not been allocated will be silently ignored.
 *
 * Input : cor_x, cor_y: pointers to the buffers for the corrected coordinates
 * Output: none
 * Return: 0
 */

int spd_free_buffers(float *cor_x,float *cor_y)
{
#if WASTE4_FORSPEED
  spd_unloadspline(spline);
  spline = NULL;
#endif /* WASTE4_FORSPEED */

  if(cor_x) {
    pfree((void *)cor_x);
    cor_x = NULL;
  }
  if(cor_y) {
    pfree((void *)cor_y);
    cor_y = NULL;
  }
  return(0);
}

/*==============================================================================
 * Sets new values for the pixels outside the active area. The active area is
 * a circle with radius "r", its center is at (xcen, ycen).
 *
 * The pixels in the image belong to one of the following five categories:
 * - pixels that are inside the active area;
 * - pixels that are in their x-coordinate outside the active area, without
 *   regard to their y-coordinate (called "pixels to the left or right of the
 *   active area");
 * - pixels that are in their y-coordinate outside the active area, but in
 *   their x-coordinate inside the active area (called "pixels above or below
 *   the active area").
 *
 * The values of the pixels inside the active area are not changed.
 *
 * All pixels to the left (right) of the active area are assigned the pixel
 * value of the leftmost (rightmost) pixel of the active area, i.e. the pixel
 * with the coordinates (xcen - r, 0) (or, for the right side, (xcen + r, 0)).
 *
 * All pixels above (below) the active area are assigned the pixel value of
 * that pixel on the border of the active area that is exactly below (above)
 * them, i.e. the pixel that has the same x-coordinate and is situated on
 * the circumference of the arcive area circle below (above).
 *
 * Input : image:      array with the corrected coordinate values (x or y) for
 *                     the input image
 *         xcen, ycen: x and y coordinates of the center of the active area
 *         r:          radius of the active area
 * Output: image: input array with redefined values for pixels outside the
 *                active area
 * Return: 0
 */

int spd_active_area(float *image,int xcen,int ycen,int r)
{
  int x,y;
  float r2 = r * r;
  float dx,dy;

  prmsg(DMSG,("Setting pixel outside active area (r = %d) \n",r));

  for(x = 0; x < XSIZE; x++) {
    dx = x - xcen;
    if(dx * dx < r2) {
      /*
       * Set pixel values above and below the active area.
       */
      dy = sqrt(r2 - dx * dx);
      for(y = 0; y < (int)(ycen - dy); y++)
        image[x + y * XSIZE] = image[x + ((int)(ycen - dy)) * XSIZE];
      for(y = (int)(ycen + dy); y < YSIZE; y++)
        image[x + y * XSIZE] = image[x + ((int)(ycen + dy)) * XSIZE];
    } else {
      /*
       * Set pixel values left and right of the active area.
       */
      if(dx > 0)
        for(y = 0; y < YSIZE; y++)
          image[x + y * XSIZE]= image[xcen + r + ycen * XSIZE];
      else
        for(y = 0; y < YSIZE; y++)
          image[x + y * XSIZE]= image[xcen - r + ycen * XSIZE];
    }
  }
  return(0);
} /* spd_active_area */

/*==============================================================================
 * Obtains for all pixel points and for all lines the corrected coordinates 
 * from a set of spline function parameters that are read from the spatial 
 * distortion correction file.
 *
 * If any of the requested input files cannot be found, an error is returned.
 *
 * Output: cor_x, cor_y: arrays with the corrected coordinate values for the
 *                       input image
 * Return:  0  if successful
 *         -1  else
 */

int spd_scorr(float **cor_x,float **cor_y)
{
  static float *x_buf = NULL,*y_buf = NULL;
  int ix,iy,xreg,yreg;
  float *px,*py,*pcor_xnw,*pcor_ynw;
  struct data_head *sdx_head = &img_head[SDXTYP],*sdy_head = &img_head[SDYTYP];

  prmsg(DMSG,("spd_fcorr: create displacements from splines\n"));

  /*
   * Get a new set of spline function parameters, if necessary. This includes
   * freeing the old buffers in the spline function structure, allocating new
   * buffers and reading the parameters from a file into the structure elements.
   *
   * This has to be done at the first pass through the program and also every
   * time the spatial distortion file is changed (then SPLINE_INVALID == 1).
   */
  if(SPLINE_INVALID || spline == NULL) {
    if(spline != NULL) {
      spd_unloadspline(spline);
      spline = NULL;
    }
    bench(NULL);
    if((spline = spd_loadspline(DISTFILE)) == NULL)
      goto spd_scorr_error;
    SPLINE_INVALID = 0;

  /*
   * Save the corrected x- and y-pixel sizes.
   *
   * They will be put (by undistort_im()) in the data header structure for all
   * corrected images that are produced with this set of spline coefficients.
   *
   * Note that the units for these sizes are micro-meter in the spatial
   * distortion file, and meter in the data header.
   */
    psizex = spline->xcor_size / 1000000.;
    psizey = spline->ycor_size / 1000000.;

    prmsg(DMSG,("XDist: XKnots: %d YKnots: %d YDist: XKnots: %d YKnots: %d\n",
      spline->dx_xknots,spline->dx_yknots,spline->dy_xknots,spline->dy_yknots));
    prmsg(DMSG,("Size: [%4.0f %4.0f] [%4.0f %4.0f]\n",spline->reg_x0,
      spline->reg_y0,spline->reg_x1,spline->reg_y1));
  }

  /*
   * Allocate buffers for the input x- and y-coordinate arrays. Make them one
   * element bigger than the "Size" to allow for the calculation of the left
   * and right (lower and upper) corners for each pixel.
   */
  xreg = spline->reg_x1 - spline->reg_x0 + 1;
  yreg = spline->reg_y1 - spline->reg_y0 + 1;

  if((x_buf = (float *)pmalloc(xreg * sizeof(float))) == NULL ||
    (y_buf = (float *)pmalloc(yreg * sizeof(float))) == NULL)
    prmsg(FATAL,("no memory for buffers in spd_scorr\n"));

  /*
   * Fill the x coordinate array with the x-values of the input image (all
   * integer values between the smallest and largest x).
   *
   * Then do the same for y.
   */
  for(px = x_buf,ix = spline->reg_x0; ix <= spline->reg_x1; ix++)
    *(px++) = (float)ix;
  for(py = y_buf,iy = spline->reg_y0; iy <= spline->reg_y1; iy++)
    *(py++) = (float)iy;

  /*
   * Allocate buffers for the corrected x- and y-coordinate arrays.
   */
  get_buffer(-1,NULL,(void **)cor_x,&yreg,&xreg,SDXTYP);
  sdx_head->PSize_1 = spline->xcor_size / 1000000.;
  sdx_head->PSize_2 = spline->ycor_size / 1000000.;
  sdx_head->BSize_1 = spline->bsize_1;
  sdx_head->BSize_2 = spline->bsize_2;
  sdx_head->Offset_1 = spline->reg_x0 - 0.5;
  sdx_head->Offset_2 = spline->reg_y0 - 0.5;
  sdx_head->init = FL_PSIZ1 | FL_PSIZ2 | FL_BSIZ1 | FL_BSIZ2 | FL_OFFS1 |
    FL_OFFS2;
  set_headval(*sdx_head,SDXTYP);

  get_buffer(-1,NULL,(void **)cor_y,&yreg,&xreg,SDYTYP);
  *sdy_head = *sdx_head;

  /*
   * For all points of the image (i.e. all combinations of values in x_buf and
   * y_buf) obtain the displacement values for the x and y coordinates using
   * spline functions (one for the x and the other for the y coordinate) and
   * return them in cor_x and cor_y.
   */
  spd_calcspline(spline,xreg,yreg,x_buf,y_buf,*cor_x,*cor_y);
  pfree(x_buf);
  pfree(y_buf);
  x_buf = y_buf = NULL;

  /*
   * If there is an active area of the detector defined, do not correct the
   * coordinates for the pixels outside the active area.
   *
   * More precisely, set the coordinate corrections for all those pixels
   * identical to the corrections for the pixels on the edge of the active
   * area. This means that there is a steady transition from the coordinates
   * inside to the ones outside the active area.
   */
  if(ACTIVE_R) {
    spd_active_area(*cor_x,XSIZE/2,YSIZE/2,ACTIVE_R);
    spd_active_area(*cor_y,XSIZE/2,YSIZE/2,ACTIVE_R);
  }

  /*
   * Map the buffers and headers of the displacement values so that their
   * geometry is identical to the one for the input source image.
   */
  if(map_imag(*cor_x,(void **)&pcor_xnw,-1.,-1.,SDXTYP) >= 0 &&
    map_imag(*cor_y,(void **)&pcor_ynw,-1.,-1.,SDYTYP) >= 0) {
    clean_buffer((void **)cor_x,SDXTYP,1);
    clean_buffer((void **)cor_y,SDYTYP,1);
    *cor_x = pcor_xnw;
    *cor_y = pcor_ynw;
  } else {
    prmsg(ERROR,("error mapping x- or y-distortion buffer\n"));
    goto spd_scorr_error;
  }

  return(0);

spd_scorr_error:
  return(-1);

} /* spd_scorr */

/*==============================================================================
 * Gets for all pixel points in the input image the corrected coordinates from
 * two input files (one for the x, the other for the y coordinates).
 *
 * If any of the two input files cannot be read correctly, the routine returns
 * an error.
 *
 * Input : x0, x1, y0, y1: minimal and maximal x and y coordinates of the input
 *                         image (x0 = left border, x1 = right border,...)
 * Output: cor_x, cor_y: arrays with the corrected coordinate values for the
 *                       input image
 * Return:  0  if successful
 *         -1  else
 */

int spd_fcorr(float **cor_x,float **cor_y,int x0,int x1,int y0,int y1)
{
  unsigned long reqflg = FL_PSIZ1 | FL_PSIZ2;
  int cols = x1 - x0 + 1,rows = y1 - y0 + 1;

  prmsg(DMSG,("spd_fcorr: read displacements from files\n"));

  /*
   * Read the x and y distortion values from the corresponding files, if they
   * are defined.
   *
   * Get also the corrected x- and y-pixel sizes from these files.
   * They will be put (by undistort_im()) in the data header structure for all
   * corrected images that are produced with this set of correction values.
   */
  if(XINFILE && YINFILE) {
    if(get_buffer(-1,XINFILE,(void **)cor_x,&rows,&cols,SDXTYP) == -1) {
      prmsg(ERROR,("error reading file %s\n",XINFILE));
      goto spd_fcorr_error;
    }
    if((img_head[SDXTYP].init & reqflg) != reqflg)
      psizex = psizey = -1.;
    else {
      psizex = img_head[SDXTYP].PSize_1;
      psizey = img_head[SDXTYP].PSize_2;
    }

    if(get_buffer(-1,YINFILE,(void **)cor_y,&rows,&cols,SDYTYP) == -1) {
      prmsg(ERROR,("error reading file %s\n",YINFILE));
      goto spd_fcorr_error;
    }
    if((img_head[SDYTYP].init & reqflg) != reqflg)
      psizex = psizey = -1.;

    if(psizex != img_head[SDYTYP].PSize_1 || psizey != img_head[SDYTYP].PSize_2)
      psizex = psizey = -1.;

  } else {
    prmsg(ERROR,("no file(s) specified for x or y distortion values\n"));
    goto spd_fcorr_error;
  }

  SPLINE_INVALID = 0;

  return(0);

spd_fcorr_error:
  return(-1);

} /* spd_fcorr */

/*==============================================================================
 * Read the multiplication factors for each pixel after distortion correction
 * from a file.
 *
 * If MINFILE is set the multiplication array is read from a file, otherwise
 * it is allocated and initialized with 1.
 *
 * Input : x0, x1, y0, y1: minimal and maximal x and y coordinates of the input
 *                         image (x0 = left border, x1 = right border,...)
 * Output:          cor_m: multiplication array from file or initialized with 1.
 * Return:  0  if successful
 *         -1  else
 */

int spd_mcorr(float **cor_m,int x0,int x1,int y0,int y1)
{
  int cols = x1 - x0,rows = y1 - y0;

  /*
   * Create the multiplication array or read it from a file.
   */
  if(MINFILE) {
    if(get_buffer(-1,MINFILE,(void **)cor_m,&rows,&cols,SDMTYP) == -1) {
      prmsg(ERROR,("spd_mcorr: error reading file %s\n",MINFILE));
      goto spd_mcorr_error;
    }
  } else {

    float *pbuf, *pend; 
    if(get_buffer(-1,NULL,(void **)cor_m,&rows,&cols,SDMTYP) == -1) {
      prmsg(ERROR,("spd_mcorr: error creating multiplication array\n"));
      goto spd_mcorr_error;
    }
    pbuf = (float *) *cor_m;
    pend = pbuf + (cols * rows);
   /*
    * fill buffer with 1.
    */
    while (pbuf<pend) *(pbuf++)=1.;
  }

  return(0);

spd_mcorr_error:
  return(-1);

} /* spd_mcorr */

/*==============================================================================
 * Adds the prerotation displacements to cor_x and cor_y. 
 *
 * Input: cor_x, cor_y: x- and y shift-arrays for the input image coordinates
 *                      or NULL
 *               cor_m: intensity multiplication factors for the corrected image
 *
 * Output: cor_x, cor_y: incremented x- and y-shift arrays including the
 *                       prerotation displacements, allocated if needed
 *         cor_m: array with multiplication factors for the corrected image
 *                The new multiplication factors are multiplied to the input
 *
 * Return:  0  if successful
 *         -1  else
 */

int spd_rotcorr(float **cor_x,float **cor_y, float **cor_m,
                int x0, int x1, int y0, int y1)
{
  int cols = x1 - x0 + 1,rows = y1 - y0 + 1;
  struct data_head *sdx_head = &img_head[SDXTYP];
  struct data_head *sdm_head = &img_head[SDMTYP];
  unsigned long paramsneeded, paramsgiven;

  prmsg(DMSG,("spd_rotcorr: calculate prerotation displacements\n"));

  /* 
   * if NULL, allocate and clear buffer *cor_x and *cor_y 
   */ 
  if(*cor_x == NULL) {
    if (get_buffer(-1,NULL,(void **)cor_x,&rows,&cols, SDXTYP) == -1) {
      prmsg(ERROR,("spd_rotcorr: error creating prerotation array x\n"));
      goto spd_rotcorr_error;
    }
    memset(*cor_x,0,sizeof(float) * rows * cols);
  }
  if(*cor_y == NULL) {
    if (get_buffer(-1,NULL,(void **)cor_y,&rows,&cols, SDYTYP) == -1) {
      prmsg(ERROR,("spd_rotcorr: error creating prerotation array x\n"));
      goto spd_rotcorr_error;
    }
    memset(*cor_y,0,sizeof(float) * rows * cols);
  }

  if ( set_prerot_headval( SDXTYP ) )
    goto spd_rotcorr_error;

  if(*cor_m == NULL) {
    /*
     * create multiplication array cor_m
     */
    if (spd_mcorr(cor_m,x0,x1,y0,y1))
      goto spd_rotcorr_error;
  }

  paramsneeded = FL_PREROT1 | FL_PREROT2 | FL_PREROT3 |
                 FL_PRECEN1 | FL_PRECEN2 | FL_PREDIS |
                 FL_OFFS1 | FL_OFFS2 | FL_PSIZ1 | FL_PSIZ2;
  paramsgiven = sdx_head->init & paramsneeded;

  if ( paramsgiven == paramsneeded ) {

    /*
     * The sdx and sdy images are mapped to the edges of the
     * source image (Dim = Dim_src+1, Offset = Offset_src-0.5)
     */

    float *px,*py;

    /* Pixel indices */
    long  i_10, i_20;
    float f_10, f_20;
    long  i_11, i_21;
    float f_11, f_21;

    /* Array reference system */
    float I0Off_1,I0Ps_1;
    float I0Off_2,I0Ps_2;
    float I1Off_1,I1Ps_1;
    float I1Off_2,I1Ps_2;

    /* Tangens reference system */
    float Off_10, Off_20;
    float Off_11, Off_21;
    float Ps_10, Ps_20;
    float Ps_11, Ps_21;

    /* Header parameters */
    float I0Offset_1,I0PSize_1,I0Center_1,I0Distance;
    float I0Offset_2,I0PSize_2,I0Center_2;

    long  I1Dim_1, I1Dim_2;
    float I1Offset_1,I1PSize_1,I1Center_1,I1Distance;
    float I1Offset_2,I1PSize_2,I1Center_2;
    float I1Rot_1, I1Rot_2, I1Rot_3;

    /* Waxs parameters */
    WParams I0wparams, I1wparams;
    struct sx_params sx;

    int err;
    char errbuf[1024];

    I1Dim_1    = sdx_head->Dim_1;
    I1Dim_2    = sdx_head->Dim_2;
    I1Offset_1 = sdx_head->Offset_1;
    I1Offset_2 = sdx_head->Offset_2;

    I1PSize_1  = sdx_head->PSize_1;
    I1PSize_2  = sdx_head->PSize_2;

    I1Center_1 = sdx_head->PreCenter_1;
    I1Center_2 = sdx_head->PreCenter_2;
    I1Distance = sdx_head->PreSamplDis;

    I1Rot_1    = sdx_head->PreDetRot_1;
    I1Rot_2    = sdx_head->PreDetRot_2;
    I1Rot_3    = sdx_head->PreDetRot_3;

    I0Offset_1 = I1Offset_1;
    I0Offset_2 = I1Offset_2;
    I0PSize_1  = I1PSize_1;
    I0PSize_2  = I1PSize_2;

    sx_init( &sx );
    sx.pix1.V = I1PSize_1;  sx.pix1.I = 1;
    sx.pix2.V = I1PSize_2;  sx.pix2.I = 1;
    sx.cen1.V = I1Center_1; sx.cen1.I = 1;
    sx.cen2.V = I1Center_2; sx.cen2.I = 1;
    sx.dis.V  = I1Distance; sx.dis.I  = 1;

    sx.rot1.V = I1Rot_1; sx.rot1.I = 1;
    sx.rot2.V = I1Rot_2; sx.rot2.I = 1;
    sx.rot3.V = I1Rot_3; sx.rot3.I = 1;

    if (sx_tf_params ( &sx, &sx, 0 , 0, &err )) {
      if (sx.bcen1.I && sx.bcen2.I && sx.bdis.I ) {
        I0Center_1 = sx.bcen1.V;
        I0Center_2 = sx.bcen2.V;
        I0Distance = sx.bdis.V;
      } else {
        prmsg(ERROR,("spd_rotcorr: Cannot calculate %s %s %s.\n",
          sx.bcen1.I?"":"BeamCenter_1",
          sx.bcen2.I?"":"BeamCenter_2",
          sx.bdis.I?"":"BeamDistance"));
        goto spd_rotcorr_error;
      }
    } else {
      prmsg(ERROR,("spd_rotcorr: sx_tf_params %s.\n",
        sx_errval2str ( errbuf, 1024, err )));
      goto spd_rotcorr_error;
    }

    TANGENSREF(Off_10,Ps_10,I0Offset_1,I0PSize_1,I0Center_1,I0Distance);
    TANGENSREF(Off_20,Ps_20,I0Offset_2,I0PSize_2,I0Center_2,I0Distance);

    TANGENSREF(Off_11,Ps_11,I1Offset_1,I1PSize_1,I1Center_1,I1Distance);
    TANGENSREF(Off_21,Ps_21,I1Offset_2,I1PSize_2,I1Center_2,I1Distance);

    // TANGENSREF => K=1; 
    waxs_Init ( &I0wparams, 1.0, 0.0, 0.0, 0.0 );
    waxs_Init ( &I1wparams, 1.0, I1Rot_1, I1Rot_2, I1Rot_3 );

    // The displacements are expressed in array coordinates
    // calculate I0 and I1 array coordinates
    ARRAYREF(I0Off_1,I0Ps_1);
    ARRAYREF(I0Off_2,I0Ps_2);
    ARRAYREF(I1Off_1,I1Ps_1);
    ARRAYREF(I1Off_2,I1Ps_2);

    // loop over the input image 

    px = *cor_x;
    py = *cor_y;
    for (i_21=0;i_21<I1Dim_2;i_21++) {

      for (i_11=0;i_11<I1Dim_1;i_11++) {
        WaxsCoord W1, W0;

        // add x-and y-shifts to get corrected coordinates
        f_11 = i_11+*px; 
        f_21 = i_21+*py; 

        W1.s_1 = (double) WORLD( f_11, Off_11, Ps_11 );
        W1.s_2 = (double) WORLD( f_21, Off_21, Ps_21 );

        W0=waxs_S2S(&I1wparams, &I0wparams, W1);

        if (!W0.status) {
          f_10 = INDEX(W0.s_1, Off_10, Ps_10);
          f_20 = INDEX(W0.s_2, Off_20, Ps_20);

          // new displacements
          *px = WORLD(f_10, I0Off_1, I0Ps_1) - WORLD(i_11, I1Off_1, I1Ps_1);
          *py = WORLD(f_20, I0Off_2, I0Ps_2) - WORLD(i_21, I1Off_2, I1Ps_2);
        } // if (!W0.status) 
        px++;
        py++;
      }
    }

    if(*cor_m != NULL) {
      /* 
       * Multiply intensity corrections to SDMTYP (*cor_m). The correction is
       * DOmega(prerotated)/DOmega(unrotated)
       * The sdm array is mapped to the source image and in each direction
       * one pixel smaller than the displacment arrays.
       */

      const float eps = 1e-30;

      float Off_1, Off_2;
      float Ps_1, Ps_2;

      long  Dim_1, Dim_2;

      float tmp;
 
      Dim_1    = sdm_head->Dim_1;
      Dim_2    = sdm_head->Dim_2;

      Off_1 = Off_10+0.5; Ps_1  = Ps_10; // Tangens reference system
      Off_2 = Off_20+0.5; Ps_2  = Ps_20; // Tangens reference system


      tmp = I1Distance*I1PSize_1*I1PSize_2;
      if (fabs(tmp)>eps) {
       /*
        * image after prerotation correction 
        */
        float I0rd20, I0rd2, I0rd3;
        float I0factor, I0fac;
        float I0N_1, I0N_2;

       /*
        * image before prerotation correction
        */
        float I1rd20, I1rd2, I1rd3;
        float I1factor, I1fac;
        float I1N_1, I1N_2;

        float *pcor_m=*cor_m;

        I1fac= 1./tmp;

        tmp = I0Distance*I1PSize_1*I1PSize_2;
        if (fabs(tmp)>eps) {
          I0fac= 1./tmp;

          // loop over the output image
          for (i_20=0;i_20<Dim_2;i_20++) {
            WaxsCoord W1, W0;

            pcor_m = ABSPTR(*cor_m,Dim_1,Dim_2,0,i_20);

            W0.s_2 = (double) WORLD( i_20, Off_2, Ps_2 );
           /*
            * T2PSIZE converts Tangens coordinates into Normal coordinates
            */
            I0N_2 = T2PSIZE(W0.s_2,I0Distance);
            I0rd20 = I0Distance*I0Distance + I0N_2*I0N_2;

            for (i_10=0;i_10<=Dim_1;i_10++) {

              W0.s_1 = (double) WORLD( i_10, Off_1, Ps_1 );
             /*
              * T2PSIZE converts Tangens coordinates into Normal coordinates
              */
              I0N_1 = T2PSIZE(W0.s_1,I0Distance);
              I0rd2 = I0rd20 + I0N_1*I0N_1;
              I0rd3 = I0rd2*sqrt(I0rd2);

              I0factor = I0fac*I0rd3;

             /* 
              * Calculate Tangens Coordinate W1 of original image
              */
              W1=waxs_S2S(&I0wparams, &I1wparams, W0);

              if (!W1.status) {
               /*
                * T2PSIZE converts Tangens coordinates into Normal coordinates
                */
                I1N_1 =  T2PSIZE(W1.s_1,I1Distance);
                I1N_2 =  T2PSIZE(W1.s_2,I1Distance);

                I1rd20 = I1Distance*I1Distance + I1N_2*I1N_2;
                I1rd2 = I1rd20 + I1N_1*I1N_1;
                I1rd3 = I1rd2*sqrt(I1rd2);

                I1factor = I1fac*I1rd3;

                if (fabs(I0factor)>eps) {
                  *pcor_m *= I1factor / I0factor;
                } // else invalid number

              } // if (!W1.status) // else invalid number

              pcor_m++;
            } /* for i_10 ... */

          } /* for i_20 ... */

        } // else all invalid

      } // else all invalid

    } /* if(*cor_m != NULL) */

  } else {
    long unsigned missing = paramsneeded & ~paramsgiven;
    prmsg(ERROR,("spd_rotcorr: Missing required prerotation parameters (%x)\n",
      missing));
    pr_headval(stdout, SDXTYP);
    goto spd_rotcorr_error;
  }

  return(0);

spd_rotcorr_error:
  return(-1);

} /* spd_rotcorr */

/*==============================================================================
 * Obtains for all pixel points between x0 and x1 (included) and for all
 * lines from y0 to y1 (included) the corrected coordinates.
 *
 * The corrected coordinates are either directly obtained from 2 input files
 * that contain for each pixel the correction values in the x- and in the y-
 * direction, or they are calculated from a set of spline function parameters
 * that are read from the spatial distortion correction file.
 * In addition, the multiplication array cor_m is always created and updated 
 * together with cor_x and cor_y.
 *
 * If any of the requested input files cannot be found, an error is returned.
 *
 * DO_SPD controls the distortion/prerotation correction
 *        0: no distortion/prerotation
 *       >0: distortion/prerotation correction
 *
 * DO_PREROT  controls the rotation correction:
 *        0: no prerotation correction
 *        1: prerotation correction after distortion correction
 *        2: prerotation correction without distortion correction
 *
 * Input : x0, x1, y0, y1: minimal and maximal x and y coordinates of the input
 *                         image
 *                         Note: these input arguments are not used if the
 *                         correction is calculated from the spline function
 *                         parameters; then the spatial distortion file provides
 *                         this information.
 * Output: cor_x, cor_y: arrays with the corrected coordinate values for the
 *                       input image
 *         cor_m: array with multiplication factors for the corrected image
 *                image. If DO_SPD is set cor_m is always created. cor_m remains
 *                valid as long as cor_x and cor_y are valid.
 * Return:  0  if successful
 *         -1  else
 */

int spd_corr(float **cor_x,float **cor_y,float **cor_m, 
             int x0,int x1,int y0,int y1)
{
  if(*cor_x)
    pfree(*cor_x);
  if(*cor_y)
    pfree(*cor_y);
  if(*cor_m)
    pfree(*cor_m);
  *cor_x = *cor_y = *cor_m = NULL;

  /*
   * If the corrected image coordinates are to be determined from files which
   * contain directly the x- and y-direction displacement values, use 
   * spd_fcorr(), otherwise calculate them from splines and use spd_scorr().
   * Both routines do their own freeing and allocating of buffers.
   */
  if (DO_SPD) {
    /*
     * create multiplication array cor_m
     */
    if (spd_mcorr(cor_m,x0,x1,y0,y1))
      goto spd_corr_error;

    /*
     * skip distortion correction when DO_PREROT is 2
     */
    if (DO_PREROT!=2) {
      if(XINFILE && YINFILE) {
        if (spd_fcorr(cor_x,cor_y,x0,x1,y0,y1))
          goto spd_corr_error;
      } else {
        if (spd_scorr(cor_x,cor_y))
          goto spd_corr_error;
      }
    }

    /*
     * pre-rotation correction
     */
    if (DO_PREROT) {
      if (spd_rotcorr(cor_x,cor_y,cor_m,x0,x1,y0,y1))
        goto spd_corr_error;
    }
    SDX_PREROT = DO_PREROT;

    /*
     * Save the final correction values in x and y direction to the two files
     * the names of which are in XOUTFILE and YOUTFILE. These files can then 
     * be read and the correction values used by the correction program at 
     * the next execution, or they can be inspected with a program to read 
     * edf files (e.g. dis).
     */
    if(XOUTFILE && YOUTFILE) {
      put_buffer(XOUTFILE,(void **)cor_x,
                 img_head[SDXTYP].Dim_2,img_head[SDXTYP].Dim_1,SDXTYP);
      put_buffer(YOUTFILE,(void **)cor_y,
                 img_head[SDYTYP].Dim_2,img_head[SDYTYP].Dim_1,SDYTYP);
    }
    SDX_PREROT = DO_PREROT;
    if(MOUTFILE) {
      put_buffer(MOUTFILE,(void **)cor_m,
                 img_head[SDMTYP].Dim_2,img_head[SDMTYP].Dim_1,SDMTYP);
    }

  } /* if (DO_SPD) */

  return(0);

spd_corr_error:
  return(-1);

} /* spd_corr */

/*==============================================================================
 * Obtains the corrected (resx,resy) coordinate values for the (ix,iy) input
 * image coordinate point.
 *
 * To speed up the calculation, this routine will calculate the whole image at
 * the first call and for all subsequent calls just get the calculated values.
 *
 * If the corrected values cannot be obtained, the routine returns an error.
 *
 * Input : ix, iy: x and y coordinate of a pixel in the input image
 * Output: resx, resy: corrected x and y coordinate of the input pixel
 * Return:  0  if successful
 *         -1  else
 */

int spd_func(int ix,int iy,float *resx,float *resy)
{
  int idx;
  float xcor = 0,ycor = 0;
  double x,y;

  x = (double)ix; y = (double)iy;

  if(ix > XSIZE)
    ix = XSIZE;
  else if(ix < 0)
    ix = 0;

  if(iy > YSIZE)
    iy = YSIZE;
  else if(iy < 0)
    iy = 0;

  /*
   * Calculate the arrays with the corrected x and y image coordinates if
   * necessary, then get the requested values from the arrays.
   */
  if(X_COR == NULL)
    if(spd_corr(&X_COR,&Y_COR,&M_COR,0,XSIZE,0,YSIZE) != 0)
      goto spd_func_error;

  idx = iy * (XSIZE + 1) + ix;
  xcor = X_COR[idx];
  ycor = Y_COR[idx];
  *resx = x + xcor;
  *resy = y + ycor;
  return(0);

spd_func_error:
  return(-1);

} /* spd_func */

/* ANDY_CORR definitions END */

/*==============================================================================
 * Gets the values of the user-definable header parameters' structure.
 *
 * The image header structure selected by the input parameter "type" is returned
 * in the structure pointed to by the argument "outhead". The member "init" of
 * the structure contains the "OR"ed flags of all the header keywords that have
 * been initialized.
 *
 * Default value if this structure array element has never been initialized by
 * the user (i.e., if set_headval() has never been called for the structure
 * with index "type"): "init" = 0. However, the CORTYP structure needs not to be
 * initialized by the user, as its values will be set from the SRCTYP structure.
 * Thus the CORTYP structure is initialized if the SRCTYP structure has been
 * initialized.
 *
 * See the declaration of the "img_head" structure array for the values that
 * "type" can have.
 *
 * Input : type: index of the structure array element to be returned
 * Output: outhead: structure with the values of the structure "img_head[type]"
 * Return: -1  if "type" has an illegal value
 *          0  otherwise
 */

int get_headval(struct data_head *outhead,int type)
{
  if(type <= 0 || type >= MAXTYP)
    return(-1);

  *outhead = img_head[type];

  return(0);
}

/*==============================================================================
 * Print the initialized values of the user-definable header parameters' structure.
 *
 * Input :  out: output file pointer
           type: index of the structure array element to be returned
 * Return: -1  if "type" has an illegal value
 *          0  otherwise
 */

int pr_headval(FILE *out, int type)
{ int status=0;
  struct data_head user_head;

  if ( status=get_headval(&user_head, type) )
    return(status);

  fprintf(out,"Header of %s image\n",typestr[type]);

  if (user_head.init & FL_ORIEN) 
    fprintf(out,"FL_ORIEN=%ld\n",user_head.Orientat);
  if (user_head.init & FL_DUMMY)
    fprintf(out,"FL_DUMMY=%.4g\n",user_head.Dummy);
  if (user_head.init & FL_DDUMM)
    fprintf(out,"FL_DDUMM=%.4g\n",user_head.DDummy);
  if (user_head.init & FL_OFFS1)
    fprintf(out,"FL_OFFS1=%.4g\n",user_head.Offset_1);
  if (user_head.init & FL_OFFS2)
    fprintf(out,"FL_OFFS2=%.4g\n",user_head.Offset_2);
  if (user_head.init & FL_PSIZ1)
    fprintf(out,"FL_PSIZ1=%.4g\n",user_head.PSize_1);
  if (user_head.init & FL_PSIZ2)
    fprintf(out,"FL_PSIZ2=%.4g\n",user_head.PSize_2);
  if (user_head.init & FL_INTE0)
    fprintf(out,"FL_INTE0=%s\n",user_head.Intens_0);
  if (user_head.init & FL_INTE1)
    fprintf(out,"FL_INTE1=%s\n",user_head.Intens_1);
  if (user_head.init & FL_CENT1)
    fprintf(out,"FL_CENT1=%.4g\n",user_head.Center_1);
  if (user_head.init & FL_CENT2)
    fprintf(out,"FL_CENT2=%.4g\n",user_head.Center_2);
  if (user_head.init & FL_SAMDS)
    fprintf(out,"FL_SAMDS=%.4g\n",user_head.SamplDis);
  if (user_head.init & FL_WAVLN)
    fprintf(out,"FL_WAVLN=%.4g\n",user_head.WaveLeng);
  if (user_head.init & FL_TITLE)
    fprintf(out,"FL_TITLE=%s\n",user_head.Title);
  if (user_head.init & FL_TIME)
    fprintf(out,"FL_TIME=%s\n",user_head.Time);
  if (user_head.init & FL_EXTIM)
    fprintf(out,"FL_EXTIM=%s\n",user_head.ExpTime);
  if (user_head.init & FL_BSIZ1)
    fprintf(out,"FL_BSIZ1=%.4g\n",user_head.BSize_1);
  if (user_head.init & FL_BSIZ2)
    fprintf(out,"FL_BSIZ2=%.4g\n",user_head.BSize_2);
  if (user_head.init & FL_DIM1)
    fprintf(out,"FL_DIM1=%d\n",user_head.Dim_1);
  if (user_head.init & FL_DIM2)
    fprintf(out,"FL_DIM2=%d\n",user_head.Dim_2);
  if (user_head.init & FL_PRO)
    fprintf(out,"FL_PRO=%s\n",user_head.ProjTyp);
  if (user_head.init & FL_ROT1)
    fprintf(out,"FL_ROT1=%.4g\n",user_head.DetRot_1);
  if (user_head.init & FL_ROT2)
    fprintf(out,"FL_ROT2=%.4g\n",user_head.DetRot_2);
  if (user_head.init & FL_ROT3)
    fprintf(out,"FL_ROT3=%.4g\n",user_head.DetRot_3);
  if (user_head.init & FL_PRECEN1)
    fprintf(out,"FL_PRECEN1=%.4g\n",user_head.PreCenter_1);
  if (user_head.init & FL_PRECEN2)
    fprintf(out,"FL_PRECEN2=%.4g\n",user_head.PreCenter_2);
  if (user_head.init & FL_PREDIS)
    fprintf(out,"FL_PREDIS=%.4g\n",user_head.PreSamplDis);
  if (user_head.init & FL_PREROT1)
    fprintf(out,"FL_PREROT1=%.4g\n",user_head.PreDetRot_1);
  if (user_head.init & FL_PREROT2)
    fprintf(out,"FL_PREROT2=%.4g\n",user_head.PreDetRot_2);
  if (user_head.init & FL_PREROT3)
    fprintf(out,"FL_PREROT3=%.4g\n",user_head.PreDetRot_3);

  return(status);

} /* pr_headval */

/*==============================================================================
 * Gets the value of the user-definable variable XSIZE. This is the number of
 * pixels in the x-direction.
 *
 * Default value if not defined by the user: 1024
 *
 * Input : none
 * Output: none
 * Return: present value of variable XSIZE
 */

int get_xsize(void)
{
  return(XSIZE);
}

/*==============================================================================
 * Gets the value of the user-definable variable YSIZE. This is the number of
 * pixels in the y-direction.
 *
 * Default value if not defined by the user: 1024
 *
 * Input : none
 * Output: none
 * Return: present value of variable YSIZE
 */

int get_ysize(void)
{
  return(YSIZE);
}

/*==============================================================================
 * Sets the user-definable variable ACTIVE_R. If set, the spatial correction is
 * only done inside a circular area with radius ACTIVE_R in the image.
 *
 * Default value if not defined by the user: 0. (i.e., no circular area defined)
 *
 * Input : arad: new value for variable ACTIVE_R
 * Output: none
 * Return: none
 */

void set_actrad(float arad)
{
  if(arad != ACTIVE_R) {
    ACTIVE_R = arad;
    LUT_INVALID = 1;
  }
}

/*==============================================================================
 * Sets the user-definable variable DRK_CONST. If set, this value is subtracted
 * from every pixel in the source image.
 *
 * For details, see routine subtract_drk().
 *
 * Default value if not defined by the user: 0.
 *
 * Input : drkconst: new value for variable DRK_CONST
 * Output: none
 * Return: none
 */

void set_drkconst(float drkconst)
{
  DRK_CONST = drkconst;
}

/*==============================================================================
 * Sets the user-definable variable DO_DARK. If this variable is set to 0, no
 * dark image correction will be performed.
 *
 * Default value if not defined by the user: 0
 *
 * Input : dodark: new value for variable DO_DARK
 * Output: none
 * Return: none
 */

void set_dodark(int dodark)
{
  DO_DARK = dodark;
}

/*==============================================================================
 * Sets the user-definable variable DO_FLAT. If this variable is set, the target
 * image will be normalized to a flat image.
 *
 * Default value if not defined by the user: 0
 *
 * Input : doflat: new value for variable DO_FLAT
 * Output: none
 * Return: none
 */

void set_doflat(int doflat)
{
  if(DO_FLAT != doflat) {
    LUT_INVALID = 1;
    DO_FLAT = doflat;
  }
}

/*==============================================================================
 * Sets the user-definable variable DO_SPD, which decides if a distortion/prerotation
 * correction will be performed:
 * DO_SPD = 0  no distortion/prerotation correction
 *        = 1  distortion/prerotation correction after dark image, before 
 *             floodfield
 *        = 2  distortion/prerotation correction after floodfield, before 
 *             normalization
 *        = 3  distortion/prerotation correction after normalization
 *        = 4  distortion/prerotation correction after background subtraction
 *
 * Default value if not defined by the user: 1
 *
 * Input : dospd: new value for variable DO_SPD
 * Output: none
 * Return: none
 */

void set_dospd(int dospd)
{
  if(dospd < 0 || dospd > 4)
    return;
  DO_SPD = dospd;
}

/*==============================================================================
 * Sets the user-definable variable DO_PREROT, which decides if a rotation
 * correction will be done after the distortion correction:
 * DO_PREROT = 0  no rotation correction
 *           = 1  rotation correction after distortion correction
 *
 * Default value if not defined by the user: 1
 *
 * Input : doprerot: new value for variable DO_PREROT
 * Output: none
 * Return: none
 */

void set_doprerot(int doprerot)
{
  if(doprerot < 0 || doprerot > 2)
    return;
  DO_PREROT = doprerot;
}

int get_doprerot( void )
{
  return (DO_PREROT);
}

/*==============================================================================
 * Sets the user-definable variable NORM_PREROT, which decides whether
 * a multiplication with the spd multiplication array M_COR is done after
 * a spatial distortion correction.
 * NORM_PREROT = 0 no multiplication with M_COR
 *             = 1 multiplication with M_COR
 *
 * Default value if not defined by the user: 0
 *
 * Input : normprerot: new value for variable NORM_PREROT
 * Output: none
 * Return: none
 */

void set_normprerot(int normprerot)
{
  if(normprerot < 0 || normprerot > 1)
    return;
  NORM_PREROT = normprerot;
}

/*==============================================================================
 * Sets the user-definable variable "Dummy" which is used to mark invalid pixels
 * in the output image.
 *
 * Default value if not defined by the user: 0.
 *
 * The value of "DDummy" is also redefined to reflect the new value of "Dummy":
 *         DDummy = DDSET(Dummy)
 *
 * Input : dummy_in: new value for variable Dummy
 * Output: none
 * Return: none
 */

void set_dummy(float dummy_in)
{
  Dummy = dummy_in;
  img_head[CORTYP].Dummy = Dummy;
  img_head[CORTYP].init |= FL_DUMMY;
  img_head[CORTYP].init |= FL_DDUMM;
  img_head[CORTYP].DDummy = DDSET(Dummy);
}

/*==============================================================================
 * Sets the values of the user-definable header parameters' structure.
 *
 * The input structure is copied to the "data_head" type structure with the
 * array index given by the input parameter "type". The member "init" of that
 * header structure contains then the "OR"ed flags of all the header keywords
 * that are initialized.
 *
 * Default value if this structure array element has never been initialized by
 * the user (i.e., if set_headval() has never been called for the structure
 * with index "type"): "init" = 0
 *
 * If the input header is of type SRCTYP, copy it into the CORTYP structure as
 * well.
 *
 * See the declaration of the "img_head" structure array for the values that
 * "type" can have.
 *
 * Input : inhead: structure with the new values for the structure
 *                 "img_head[type]"
 *         type  : index of the structure array element to be initialized
 * Output: none
 * Return: -1  if "type" has an illegal value
 *          0  otherwise
 */

int set_headval(struct data_head inhead,int type)
{
  if(type <= 0 || type >= MAXTYP)
    return(-1);

  img_head[type] = inhead;

  if(type == SRCTYP)
    img_head[CORTYP] = img_head[SRCTYP];

  return(0);
}

/*==============================================================================
 * Sets pointers to several types of user-definable image buffers:
 *
 * - type = DRKTYP defines the dark image (variable DRK_IM);
 * - type = FLOTYP defines the flood field image (variable FLO_IM);
 * - type = SBKTYP defines the scattering background image (variable BCKG_IM);
 *
 * Default values for all buffers if not defined by the user: NULL pointer.
 *
 * If "type" has not one of the allowed values, an error message is displayed
 * and the routine returns without further action.
 *
 * Input : pbuf: pointer to image of type "type"
 *         type: type of image
 * Output: none
 * Return: none
 */

void set_imgbuf(void *pbuf,int type)
{
  switch(type) {
  case DRKTYP:
    DRK_IM = (float *)pbuf;
    break;
  case FLOTYP:

    /*
     * Invert the floodfield image. This makes later processing easier, as the
     * source image is then multiplied by the floodfield image, which avoids
     * the problems of division by 0.
     */
    FLO_IM = (float *)pbuf;
    prepare_flood((unsigned short *)NULL,FLO_IM);
    break;
  case SBKTYP:
    BCKG_IM = (float *)pbuf;
    break;
  default:
    prmsg(ERROR,("illegal file type %d in set_imgbuf\n",type));
    return;
  }
}

/*==============================================================================
 * Sets the user-definable variable INPCONST. If set, this value is added to
 * every pixel in the input image.
 *
 * For details, see routine scale_im().
 *
 * Default value if not defined by the user: 0.
 *
 * Input : inpconst: new value for variable INPCONST
 * Output: none
 * Return: none
 */

void set_inpconst(float inpconst)
{
  INPCONST = inpconst;
}

/*==============================================================================
 * Sets the user-definable variable INPEXP. If set, every pixel in the input
 * image is exponentiated with this value.
 *
 * As the exponential function is rather slow, the calculation will be done with
 * a lookup-table that contains the exponentiated values for all integers
 * between 0 and USHRT_MAX (i.e. the range of the source image data). This table
 * is prepared here every time the value of the input exponent INPEXP changes. 
 *
 * Note that this means that the table has to have dimension USHRT_MAX + 1.
 *
 * For more details, see routine expon_im().
 *
 * Default value if not defined by the user: 1.
 *
 * Input : inpexp: new value for variable INPEXP
 * Output: none
 * Return: none
 */

void set_inpexp(float inpexp)
{
  int index;
  if(inpexp != INPEXP)
  {
    INPEXP = inpexp;
    for(index = 0; index <= USHRT_MAX; index++)
      *(exptab + index) = pow((double)index,(double)inpexp);
  }
}

/*==============================================================================
 * Sets the user-definable variable INPFACT. If set, every pixel in the input
 * image is multiplied with this value.
 *
 * For details, see routine scale_im().
 *
 * Default value if not defined by the user: 1.
 *
 * Input : inpfact: new value for variable INPFACT
 * Output: none
 * Return: none
 */

void set_inpfact(float inpfact)
{
  INPFACT = inpfact;
}

/*==============================================================================
 * Sets the user-definable variable "INP_MAX" which is used to define the
 * maximum allowed value in the input and the dark current image.
 *
 * The value 0. is used to signal that no maximum allowed value is set.
 *
 * Default value if not defined by the user: 0. (i.e., not set).
 *
 * Input : inpmax_in: new value for variable INP_MAX
 * Output: none
 * Return: none
 */

void set_inpmax(float inpmax_in)
{
  INP_MAX = inpmax_in;
}

/*==============================================================================
 * Sets the user-definable variable "INP_MIN" which is used to define the
 * minimum allowed value in the input and the dark current image.
 *
 * The value 0. is used to signal that no minimum allowed value is set.
 *
 * Default value if not defined by the user: 0. (i.e., not set).
 *
 * Input : inpmin_in: new value for variable INP_MIN
 * Output: none
 * Return: none
 */

void set_inpmin(float inpmin_in)
{
  INP_MIN = inpmin_in;
}

/*==============================================================================
 * Sets the user-definable variable NORM_INT. If this variable is set, the
 * target image will be normalized to absolute scattering intensities.
 *
 * Default value if not defined by the user: 0 (i.e., not set)
 *
 * Input : normint: new value for variable NORM_INT
 * Output: none
 * Return: none
 */

void set_normint(int normint,float normfact)
{
  NORM_INT = normint;
  NORMFACT = normfact;
}

/*==============================================================================
 * Sets the user-definable variables IMAGE_OVER and IMAGE_OVER_SET which are
 * used to mark pixels with overflow values in the images.
 *
 * Default value for both variables if not defined by the user: 0xffff
 *
 * Input : overfl: new value for variables IMAGE_OVER and IMAGE_OVER_SET
 * Output: none
 * Return: none
 */

void set_overflow(unsigned long overf)
{
  IMAGE_OVER = overf;
  IMAGE_OVER_SET = overf;
}

/*==============================================================================
 * Sets the user-definable variable DSTRTVAL. If this variable is set, the pixel
 * sizes, center coordinates, coordinate offsets, sample distance, bin sizes,
 * projection type and detector rotations for the corrected image are taken from
 * the distortion files, otherwise the values of the source image header are
 * retained.
 *
 * For more details, see routine undistort_im().
 *
 * Default value if not defined by the user: 0
 *
 * Input : dstrtval: new value for variable DSTRTVAL
 * Output: none
 * Return: 0 always
 */

int set_dstrtval(int dstrtval)
{
  DSTRTVAL = dstrtval;
  return(0);
}

/*==============================================================================
 * Sets the user-definable variable BCKGCONST. If set, this value is added to
 * every pixel in the scattering background image.
 *
 * For details, see routine scale_im().
 *
 * Default value if not defined by the user: 0.
 *
 * Input : bckgconst: new value for variable BCKGCONST
 * Output: none
 * Return: none
 */

void set_bckgconst(float bckgconst)
{
  BCKGCONST = bckgconst;
}

/*==============================================================================
 * Sets the user-definable variable BCKGFACT. If set, every pixel in the
 * scattering background image is multiplied with this value.
 *
 * For details, see routine scale_im().
 *
 * Default value if not defined by the user: 1.
 *
 * Input : bckgfact: new value for variable BCKGFACT
 * Output: none
 * Return: none
 */

void set_bckgfact(float bckgfact)
{
  BCKGFACT = bckgfact;
}

/*==============================================================================
 * Sets the user-definable variable DISTFILE. The corresponding file contains
 * the spline function coefficients that are used to calculate the distortion
 * correction values.
 *
 * Default value if not defined by the user: NULL pointer
 *
 * Input : distfile: new value for variable DISTFILE
 * Output: none
 * Return: none
 */

void set_splinfil(char *distfile)
{
  int dflen,istat;
  static int dist_mtime = 0;
  struct stat stat_buf;

  if(distfile == NULL || (dflen = strlen(distfile)) == 0)
    return;

  if(XINFILE) {
    pfree(XINFILE);
    XINFILE = NULL;
  }
  if(YINFILE) {
    pfree(YINFILE);
    YINFILE = NULL;
  }

  /*
   * Check if this is a new distortion file. This is the case if any of the
   * following is true:
   * - there was no previous distortion file;
   * - the file names of the previous and the present file are different;
   * - the modification time of the file has changed.
   */
  if((istat = stat(distfile,&stat_buf)) != 0)
    prmsg(DMSG,("Cannot access distortion file %s\n",distfile));

  if(DISTFILE == NULL || strcmp(DISTFILE,distfile) != 0 || dist_mtime == 0 ||
    istat == 0 && dist_mtime != (int)stat_buf.st_mtime) {

    if(DISTFILE) {
      prmsg(DMSG,("Distortion file name changed from %s to %s\n",DISTFILE,
        distfile));
      pfree(DISTFILE);
    } else
      prmsg(DMSG,("Distortion file name set to %s\n",distfile));

    if(istat == 0)
      dist_mtime = (int)stat_buf.st_mtime;

    SPLINE_INVALID = 1;
    LUT_INVALID = 1;
    DISTFILE = (char *)pmalloc((dflen + 1) * sizeof(char));
    strcpy(DISTFILE,distfile);
  }
}

/*==============================================================================
 * Sets the user-definable variables XINFILE and YINFILE. The corresponding
 * files contain for each pixel of the source image the spatial distortion
 * correction values in x direction (XINFILE) and y direction (YINFILE).
 *
 * Default value for both variables if not defined by the user: NULL pointer
 *
 * Input : xfile: new value for variable XINFILE
 *         yfile: new value for variable YINFILE
 * Output: none
 * Return: none
 */

void set_xycorin(char *xfile,char *yfile)
{
  int ixstat,iystat,xflen,yflen,newxfile = 0,newyfile = 0;
  static int x_mtime = 0,y_mtime = 0;
  struct stat xstat_buf,ystat_buf;

  if((xflen = strlen(xfile)) == 0 || (yflen = strlen(yfile)) == 0)
    return;

  if(DISTFILE) {
    pfree(DISTFILE);
    DISTFILE = NULL;
  }
  if(spline != NULL) {
    spd_unloadspline(spline);
    spline = NULL;
  }

  /*
   * Check if the distortion correction files are new. This is the case if any
   * of the following is true:
   * - there was no previous distortion file;
   * - the file names of the previous and the present file are different;
   * - the modification time of the file has changed.
   */
  if((ixstat = stat(xfile,&xstat_buf)) != 0)
    prmsg(DMSG,("Cannot access x distortion value file %s\n",xfile));
  if(XINFILE == NULL || strcmp(XINFILE,xfile) != 0 || x_mtime == 0 ||
    ixstat == 0 && x_mtime != (int)xstat_buf.st_mtime)
    newxfile = 1;

  if((iystat = stat(yfile,&ystat_buf)) != 0)
    prmsg(DMSG,("Cannot access y distortion value file %s\n",yfile));
  if(YINFILE == NULL || strcmp(YINFILE,yfile) != 0 || y_mtime == 0 ||
    iystat == 0 && y_mtime != (int)ystat_buf.st_mtime)
    newyfile = 1;

  if(newxfile) {
    if(XINFILE) {
      prmsg(DMSG,("X distortion value file name changed from %s to %s\n",
        XINFILE,xfile));
      pfree(XINFILE);
    } else
      prmsg(DMSG,("X distortion file name set to %s\n",xfile));

    if(ixstat == 0)
      x_mtime = (int)xstat_buf.st_mtime;

    XINFILE = (char *)pmalloc((xflen + 1) * sizeof(char));
    strcpy(XINFILE,xfile);
  }

  if(newyfile) {
    if(YINFILE) {
      prmsg(DMSG,("Y distortion value file name changed from %s to %s\n",
        YINFILE,yfile));
      pfree(YINFILE);
    } else
      prmsg(DMSG,("Y distortion file name set to %s\n",yfile));

    if(iystat == 0)
      y_mtime = (int)ystat_buf.st_mtime;

    YINFILE = (char *)pmalloc((yflen + 1) * sizeof(char));
    strcpy(YINFILE,yfile);
  }

  if(newxfile || newyfile) {
    SPLINE_INVALID = 1;
    LUT_INVALID = 1;
  }

} /* set_xycorin */

/*==============================================================================
 * Sets the user-definable variables XOUTFILE and YOUTFILE. If both are set, the
 * spatial distortion correction values for each pixel of the source image will
 * be saved in XOUTFILE (for x direction) and YOUTFILE (for y direction).
 *
 * Default value for both variables if not defined by the user: NULL pointer
 * (i.e., not set)
 *
 * Input : xfile: new value for variable XOUTFILE
 *         yfile: new value for variable YOUTFILE
 * Output: none
 * Return: none
 */

void set_xycorout(char *xfile,char *yfile)
{
  int xflen,yflen;

  if((xflen = strlen(xfile)) == 0 || (yflen = strlen(yfile)) == 0)
    return;

  if(XOUTFILE) {
    pfree(XOUTFILE);
    XOUTFILE = NULL;
  }
  if(YOUTFILE) {
    pfree(YOUTFILE);
    YOUTFILE = NULL;
  }

  XOUTFILE = (char *)pmalloc((xflen + 1) * sizeof(char));
  strcpy(XOUTFILE,xfile);
  YOUTFILE = (char *)pmalloc((yflen + 1) * sizeof(char));
  strcpy(YOUTFILE,yfile);

} /* set_xycorout */

/*==============================================================================
 * Sets the user-definable variable MOUTFILE. If set, the prerotation
 * renormalization image will be saved in MOUTFILE.
 *
 * Default value: NULL pointer
 * (i.e., not set)
 *
 * Input : mfile: new value for variable MOUTFILE
 * Output: none
 * Return: none
 */
void set_moutfile(char *mfile)
{
  int mflen;

  if((mflen = strlen(mfile)) == 0 )
    return;

  if(MOUTFILE) {
    pfree(MOUTFILE);
    MOUTFILE = NULL;
  }

  MOUTFILE = (char *)pmalloc((mflen + 1) * sizeof(char));
  strcpy(MOUTFILE,mfile);

} /* set_moutfile */

/*==============================================================================
 * Sets the user-definable variables XSIZE and YSIZE. These are the number of
 * pixels in the x- and y-direction.
 *
 * The x-direction is the "fast-moving" index in a multidimensional C array,
 * the y-direction is the "slow-moving" one.
 *
 * Default value for both variables if not defined by the user: 1024
 *
 * Input : xsize: new value for variable XSIZE
 *         ysize: new value for variable YSIZE
 * Output: none
 * Return: none
 */

void set_xysize(int xsize,int ysize)
{
  XSIZE = xsize;
  YSIZE = ysize;
} /* set_xysize */

/*==============================================================================
 * A routine used to get either a perfect grid image or a distorted grid
 * with Gauss peaks for tests purposes. The first peak center is at (15,35)
 * and then every 50 pixels in x and y direction there is another one.
 *
 * Input : image:         pointer to the image memory which has to exist
 *         height, width: height and width of the individual peaks.
 *         distorted:     outputs the peaks not as a perfect grid but
 *                        distorted with a simple algorithm (projection
 *                        of points on a ball)
 * Output: image: filled with image data
 * Return: 0
 */

int make_grid(unsigned short *image,float height,float width,int distorted)
{
  float x,y,r,xcen,ycen;
  float CURV_RADIUS = 270000/180;
  int i,j;
  int i0,i1,j0,j1;
  prmsg(DMSG, ("Grid (%dx%d) with peak height : %f and width %f\n",
    XSIZE,YSIZE,height,width));
  memset(image,0,sizeof(short) * XSIZE * YSIZE);
  for(x = 15; x < XSIZE; x += 50)
    for(y = 35; y < YSIZE; y += 50) {
      xcen = x; ycen = y;
      if(distorted) {
        double dx,dy,atn,d2;
        dx = x - XSIZE / 4; dy = y - YSIZE / 3;
        d2 = CURV_RADIUS * sin (sqrt (dx * dx + dy * dy) / CURV_RADIUS);
        if(fabs(dx) > 1E-5 || fabs(dy) < 1E-5) {
          atn = atan2(dy,dx);
          xcen = XSIZE/4.0 + d2 * cos(atn);
          ycen = YSIZE/3.0 + d2 * sin(atn);
        }
      }
      i0 = xcen - width * 2; i1 = xcen + width * 2 + 1;
      j0 = ycen - width * 2; j1 = ycen + width * 2 + 1;
      if(i0 >=0 && j0 >= 0 && i1 < XSIZE && j1 < YSIZE)
        for(i = i0; i < i1; i++)
          for(j = j0; j < j1; j++) {
            r = sqrt (pow(xcen - (float)i,2) + pow(ycen - (float)j,2));
            image[i + j * XSIZE] = height * exp(-r / pow(width/3,2));
          }
    }
  return(0);
} /* make_grid */

/* TRIANGLE CALCULATION BEGIN */

/*==============================================================================
 * Cuts the input triangle along a vertical line into a (smaller) triangle and
 * an irregular quadrangle. The new coordinates of the triangle are stored in
 * the old input triangle. The quadrangle is divided into two new triangles,
 * and their coordinates are stored in "new_triangles".
 *
 * Under particular circumstances there may be only one or even no new triangle
 * created (if the cut line goes through one of the corners or coincides with
 * one of the sides of the input triangle).
 *
 * Input : in_tri: structure with the x and y coordinates of a triangle
 *         v:      x coordinate of the vertical cut line
 * Output: new_triangles: structures with the x and y coordinates of the new
 *                        triangles created
 *         no_new:        number of the new triangles created (normally 2)
 * Return: 0
 */

int trianglecutv_only(struct triangle *in_tri,float v,
  struct triangle new_triangles[],int *no_new)
{
  float U1,U2,U3;
  int i,next,afternext;
  float py,ndx;
  struct triangle *new = new_triangles;
  /*
   * Set pointers to the old (input) and the new (to be created) triangle:
   * - tri, x and y point to the old triangle;
   * - nx and ny point to the new triangle.
   */
  struct triangle *tri = in_tri;
  float *x = tri->x, *y = tri->y;
  float *nx = new->x, *ny = new->y;


  /*
   * Define first point as "previous point".
   * Is this previous point to the right of the cut line?
   */
  *no_new = 0;
  U1 = x[0] > v;
  for(i = 0; i < 3; i++) {

    /*
     * Determine next point.
     * Is this next point to the right of the cut line?
     */
    next = (i == 2) ? 0 : (i + 1);
    U2 = x[next] > v;

    /*
     * If the previous and the next point are not on the same side of the cut
     * line, then cut the triangle at the cut line between previous point and
     * next point.
     *
     * The resulting two triangles will consist of the following points:
     * - old triangle: previous point, cut point, and "afternext" point;
     * - new triangle: cut point, next point, and "afternext" point.
     *
     * The new triangle will thus share two points with the old one (cutpoint
     * and afternext).
     *
     * If the previous and the next point are on the same side of the cut line,
     * then the cut will not happen between these two points. Just continue the
     * loop and test the next pair of points.
     */
    if(U1 != U2) {

      /*
       * Calculate cut point.
       */
      ndx = v - x[i];
      py = y[i] + ndx * (y[next] - y[i]) / (x[next] - x[i]);

      /*
       * Copy old triangle to new triangle.
       */
      memcpy(new,tri,sizeof(struct triangle));

      /*
       * Put cut point as new point into the old and the new triangle.
       */
      x[next] = nx[i] = v;
      y[next] = ny[i] = py;

      if(new == new_triangles) {

        /*
         * If this was the first cut, then there are now two triangles: the
         * modified input triangle and the newly created one.
         *
         * One of the two will be entirely on one side of the cut line, the
         * other needs to be cut again along the cut line.
         *
	 * Find out which triangle needs to be cut.
         */
	afternext = (next == 2) ? 0 : (next + 1);
	U3 = x[afternext] > v;

	/*
         * If the new triangle has to be cut:
         *
         * - cycle all triangle pointers: the second triangle will become the
         *   old one, and the (to be created) third will be the new triangle
         *   (i.e., "tri", "x" and "y" will point to the second, "nx" and "ny"
         *   to the third triangle);
         *
         * - then just apply the same algorithm again: this will cut the second
         *   triangle in the same way as the first one was cut before, with the
         *   new cut point stored in the coordinates of the second and third
         *   triangle.
         *
         * If the old (input) triangle has to be cut:
         *
         * - cycle just the pointers to the new triangle: the (to be created)
         *   third triangle will be the new triangle, but the first triangle
         *   will remain the old one (i.e., "tri", "x" and "y" will point to the
         *   first, "nx" and "ny" to the third triangle);
         *
         * - then just apply the same algorithm again: this will cut the first
         *   triangle again in the same way as it was cut before, with the new
         *   cut point stored in the coordinates of the first and third
         *   triangle.
         */
	if(U3 == U1) {
	  tri = new;
	  x = nx;
	  y = ny;
	}
	new++;
	nx = new->x;
	ny = new->y;
	*no_new = 1;

      } else {
        /*
         * If this was not the first cut, then the task is finished. Just
         * increase the triangle count, terminate the loop and return.
         */
	*no_new = 2;
	break;
      }
    }
    U1 = U2;
  }
  return(0);
} /* trianglecutv_only */

/*==============================================================================
 * Does the same as routine trianglecutv_only(), but cuts along a horizontal
 * line.
 *
 * See routine trianglecutv_only() for details of how the routine works.
 *
 * Input : in_tri: structure with the x and y coordinates of a triangle
 *         v:      y coordinate of the horizontal cut line
 * Output: new_triangles: structures with the x and y coordinates of the new
 *                        triangles created
 *         no_new:        number of the new triangles created (normally 2)
 * Return: 0
 */

int trianglecuth_only(struct triangle *in_tri,float v,
  struct triangle new_triangles[],int *no_new)
{
  float U1,U2,U3;
  int i,next,afternext;
  float px,ndy;
  struct triangle *new = new_triangles;
  struct triangle *tri = in_tri;
  float *x = tri->x,*y = tri->y;
  float *nx = new->x,*ny = new->y;

  *no_new = 0;
  U1 = y[0] > v;

  for(i = 0; i < 3; i++) {
    next = (i == 2) ? 0 : (i + 1);
    U2 = y[next] > v;

    if(U1 != U2) {
      ndy = v - y[i];
      px = x[i] + ndy * (x[next] - x[i]) / (y[next] - y[i]);
      memcpy(new,tri,sizeof(struct triangle));

      x[next] = nx[i] = px;
      y[next] = ny[i] = v;

      if(new == new_triangles) {
	afternext = (next == 2) ? 0 : (next + 1);

	U3 = y[afternext] > v;
	if (U3 == U1) {
	  tri = new;
	  x = nx;
	  y = ny;
	}
	new++;
	nx = new->x;
	ny = new->y;
	*no_new = 1;

      } else {
	*no_new = 2;
	break;
      }
    }
    U1 = U2;
  }
  return(0);
} /* trianglecuth_only */

/*==============================================================================
 * Calculates the area of the triangle formed by the three coordinate pairs in
 * the input arrays x and y. It uses Heron's formula
 *
 *      area = sqrt(s * (s-a) * (s-b) * (s-c))
 *
 *      where a,b,c are the three sides of the triangle and s = (a+b+c) / 2
 *
 * Input : x, y: arrays with the three x and y coordinates of the triangle
 * Output: area: the area of the triangle
 * Return: none
 */

void area_only(float *x,float *y,float *area)
{
  float dx,dy;
  float sum = 0;
  float len1,len2,len3;
  float insum;

  dx = x[0] - x[2];
  dy = y[0] - y[2];
  sum = len1 = sqrt (dx * dx + dy * dy);
  dx = x[1] - x[0];
  dy = y[1] - y[0];
  len2 = sqrt (dx * dx + dy * dy);
  sum += len2;
  dx = x[2] - x[1];
  dy = y[2] - y[1];
  len3 = sqrt (dx * dx + dy * dy);
  sum += len3;

  sum /= 2;
  insum = sum * (sum - len1) * (sum - len2) * (sum - len3);
  if(insum >= 0)
    *area = sqrt (sum * (sum - len1) * (sum - len2) * (sum - len3));
  else {
    *area = 0;
  }
} /* area_only */

/*==============================================================================
 * Cuts the input triangle into several new ones such that each of them is
 * fully contained in one of the squares of an integer valued x and y grid.
 *
 * Expressed in terms of image pixels, the triangle is cut up in such a way that
 * each part is fully contained in one pixel. The "ixmin" to "iymax" return
 * arguments give the ranges of the x and y indices of the pixels that contain
 * the input triangle ("min" and "max" both included).
 *
 * Input : triangles[0]: structure with the x and y coordinates of a triangle
 * Output: triangles: structures with the x and y coordinates of the new
 *                    triangles created. These are the integer-truncated values
 *                    of the triangle's center (the (smallest x, smallest y)
 *                    corner of the grid square the triangle is in)
 *         n:         number of the new triangles created
 *         ixmin:     the x index of the vertical grid line just to the left of
 *                    the input triangle
 *         ixmax:     the x index of the rightmost vertical grid line that still
 *                    has part of the input triangle to its right
 *         iymin:     the y index of the horizontal grid line just below the
 *                    input triangle
 *         iymax:     the y index of the highest horizontal grid line that still
 *                    has part of the input triangle above it
 * Return: 0
 */

int triangle_cutall(struct triangle triangles[],int *n,int *ixmin,int *ixmax,
  int *iymin,int *iymax)
{
  int i,j;
  int no_add,no_tri,free,total_add;
  int cutxmin,cutxmax,cutymin,cutymax;
  float *x = triangles[0].x, *y = triangles[0].y;
  float xmin,xmax,ymin,ymax;

  /*
   * Get the smallest and largest x and y value of the input triangle.
   */
  xmin = x[0] < x[1] ? x[0] : x[1];
  if(xmin > x[2])
    xmin = x[2];
  ymin = y[0] < y[1] ? y[0] : y[1];
  if(ymin > y[2])
    ymin = y[2];
  xmax = x[0] > x[1] ? x[0] : x[1];
  if(xmax < x[2])
    xmax = x[2];
  ymax = y[0] > y[1] ? y[0] : y[1];
  if(ymax < y[2])
    ymax = y[2];

  /*
   * Determine the leftmost and the rightmost vertical cut line, as well as
   * the lowest and highest horizontal cut line.
   *
   * A valid vertical cut line has a non-empty part of the triangle to the left
   * and another non-empty part to the right. Thus the leftmost vertical cut
   * line is the one with the smallest x index that still has a non-empty part
   * of the triangle to its left.
   *
   * Therefore if a grid line goes through the leftmost point of the triangle,
   * then the leftmost vertical cut line is not this one, but the next one to
   * the right.
   *
   * The rightmost vertical cut line is determined in an analog way by selecting
   * the vertical cut line with the largest x value that still has a non-empty
   * part of the triangle to its right.
   *
   * The same philosophy is then applied to select the lowest and highest
   * horizontal cut line.
   */
  cutxmin = ceil(xmin);
  if(cutxmin == xmin)
    cutxmin++;
  cutxmax = floor(xmax);
  if(cutxmax == xmax)
    cutxmax--;
  cutymin = ceil(ymin);
  if(cutymin == ymin)
    cutymin++;
  cutymax = floor(ymax);
  if(cutymax == ymax)
    cutymax--;

  /*
   * Cut the input triangle along the vertical grid (formed by all integer
   * values between the leftmost and rightmost vertical cut line) into several
   * triangles in such a way that each triangle is entirely contained between
   * two grid lines.
   *
   * no_tri counts the number of triangles, the first cut triangle will replace
   * the input triangle, and the other ones are stored into subsequent
   * locations in the structure array triangles.
   */
  no_tri = 1;
  free = 1;

  for(i = cutxmin; i <= cutxmax; i++) {
    total_add = 0;
    for(j = 0; j < no_tri; j++) {
      trianglecutv_only(triangles + j,(float)i,triangles + free,&no_add);
      free += no_add;
      if(free >= MAX_TRIANGLES)
	prmsg(ERROR,("too many triangles (%d) \n",free));
      total_add += no_add;
    }
    no_tri += total_add;
  }

  /*
   * Now cut all these created triangles along the horizontal grid (formed by
   * the integer values between the lowest and highest horizontal cut line)
   * into several triangles in such a way that each triangle is entirely
   * contained in the square between adjacent grid lines (i.e., each triangle is
   * entirely contained in a single pixel).
   */
  for(i = cutymin; i <= cutymax; i++) {
    total_add = 0;
    for(j = 0; j < no_tri; j++) {
      trianglecuth_only(triangles + j,(float)i,triangles + free,&no_add);
      free += no_add;
      if(free >= MAX_TRIANGLES)
	prmsg(ERROR,("too many triangles (%d) \n",free));
      total_add += no_add;
    }
    no_tri += total_add;
  }

  /*
   * Determine the grid coordinates for each triangle. These are the
   * integer-truncated values of the triangle's center (i.e., the
   * (smallest x, smallest y) corner of the grid square the triangle is in).
   */
  for(i = 0; i < no_tri; i++) {
    triangles[i].ypos = (int)
      ((triangles[i].y[0] + triangles[i].y[1] + triangles[i].y[2]) / 3);
    triangles[i].xpos = (int)
      ((triangles[i].x[0] + triangles[i].x[1] + triangles[i].x[2]) / 3);
  }

  /*
   * Determine the minimum and maximum pixel indices in x and y.
   */
  *ixmin = cutxmin - 1;
  *iymin = cutymin - 1;
  *ixmax = cutxmax;
  *iymax = cutymax;
  *n = no_tri;

  return(0);
} /* triangle_cutall */

/*==============================================================================
 * Cuts the input pixel into segments along an integer-values horizontal and
 * vertical grid. It determines the area of the input pixel that falls into
 * each grid square and the minimal and maximal grid values in x and y.
 *
 * To achieve this, the input pixel is cut in two triangles, which in turn are
 * cut into more triangles in such a way that at the end each of the created
 * triangles is fully contained in one square of the grid.
 *
 * Input : x, y:  arrays with the (four) x and y coordinates of a pixel
 *         debug: debugging flag: produce debugging output if != 0
 * Output: parts: array that gives for each grid square the area of the input
 *                pixel it contains
 *         txmin: the x coordinate of the vertical grid line just to the left
 *                of the input pixel
 *         txmax: the x coordinate of the rightmost vertical grid line that
 *                still has part of the input pixel to its right
 *         tymin: the y coordinate of the horizontal grid line just below the
 *                input pixel
 *         tymax: the y coordinate of the highest horizontal grid line that
 *                still has part of the input pixel above it
 *         total: the area of the input pixel
 * Return: number of triangles created
 */

int calcparts(float x[],float y[],int debug,float parts[],int *txmin,int *txmax,
  int *tymin,int *tymax,float *total)
{
  int xmin,xmax,ymin,ymax;
  int xmin2,xmax2,ymin2,ymax2;
  struct triangle triangles[MAX_TRIANGLES];
  float totalarea;
  float totalarea2;
  int no_tri,no_tri2;
  int n,i,j,idx;

  /*
   * Divide the pixel into two triangles along the line from point "0" to
   * point "2":
   *
   *                 0             1
   *                  -------------
   *                  |           |
   *                  |           |
   *                  |           |
   *                  |           |
   *                  -------------
   *                 3             2
   *
   * Calculate the areas of these two triangles, then cut each into several
   * triangles along an integer-valued horizontal and vertical grid in such a
   * way that each of those new triangles is entirely contained between
   * neighboring grid lines.
   */
  triangles[0].x[0] = x[0]; triangles[0].y[0] = y[0];
  triangles[0].x[1] = x[1]; triangles[0].y[1] = y[1];
  triangles[0].x[2] = x[2]; triangles[0].y[2] = y[2];
  area_only(triangles[0].x,triangles[0].y,&totalarea);
  triangle_cutall(triangles,&no_tri,&xmin,&xmax,&ymin,&ymax);

  triangles[no_tri].x[0] = x[2]; triangles[no_tri].y[0] = y[2];
  triangles[no_tri].x[1] = x[3]; triangles[no_tri].y[1] = y[3];
  triangles[no_tri].x[2] = x[0]; triangles[no_tri].y[2] = y[0];

  area_only(triangles[no_tri].x,triangles[no_tri].y,&totalarea2);
  triangle_cutall(triangles + no_tri,&no_tri2,&xmin2,&xmax2,&ymin2,&ymax2);

  /*
   * Get the total number of triangles, the area and the smallest and biggest
   * grid values in x and y.
   */
  no_tri += no_tri2;
  totalarea += totalarea2;
  if(xmin2 < xmin)
    xmin = xmin2;
  if(ymin2 < ymin)
    ymin = ymin2;
  if(xmax2 > xmax)
    xmax = xmax2;
  if(ymax2 > ymax)
    ymax = ymax2;

  /*
   * Calculate for each grid square how much of its area the input pixel covers,
   * and store it in the array "parts". This array contains "number of x grid
   * squares" times "number of y grid squares" elements, and the grid squares
   * are stored in row-fashion (x increases fastest).
   *
   * Expressed in pixel indices, this determines for the input source pixel the
   * target pixels it is mapped to, and how much of each target pixel the source
   * pixel covers.
   */
  n = (xmax - xmin + 1) * (ymax - ymin + 1);
  if(n > MAX_PARTS) {
    prmsg(ERROR,("too many parts (%d) - pixel too big\n",n));
    exit(-1);
  }

  for(i = 0; i < n; i++)
    parts[i] = 0.;

  for(j = 0; j < no_tri; j++) {
    area_only(triangles[j].x,triangles[j].y,&triangles[j].area);
    idx = (triangles[j].xpos - xmin) +
      (triangles[j].ypos - ymin) * (xmax - xmin + 1);
    parts[idx] += triangles[j].area;
  }

  if(debug)
    debugout(triangles,no_tri,totalarea,parts,xmin,xmax,ymin,ymax);

  *txmin = xmin; *txmax = xmax; *tymin = ymin; *tymax = ymax;
  *total = totalarea;
  return(no_tri);
} /* calcparts */

/*==============================================================================
 * Print for debugging purposes the values calculated by "calcparts()".
 *
 * Input : triangles: structure array with the triangles created
 *         no_tri:    number of triangles created
 *         total:     the area of the pixel
 *         parts:     array that gives for each grid square the area of the
 *                    input pixel it contains
 *         xmin:      the x coordinate of the vertical grid line just to the
 *                    left of the pixel
 *         xmax:      the x coordinate of the rightmost vertical grid line that
 *                    still has part of the pixel to its right
 *         ymin:      the y coordinate of the horizontal grid line just below
 *                    the pixel
 *         ymax:      the y coordinate of the highest horizontal grid line that
 *                    still has part of the pixel above it
 * Output: none
 * Return: 0
 */

int debugout(struct triangle triangles[],int no_tri,float total,float parts[],
  int xmin,int xmax,int ymin,int ymax)
{
  FILE *file;
  float sumparts;
  int i,j,idx = 0;
  float fxmin,fxmax,fymin,fymax;

  /*
   * Print smallest and largest grid value in x and y.
   */
  printf("Min Max: X %d %d Y %d %d\n",xmin,xmax,ymin,ymax);

  /*
   * Print for each triangle the coordinates of the corners, the area and the
   * grid coordinates.
   *
   * Then test if the triangle is fully contained in its grid square, and
   * print an error message if not.
   */
  for(i = 0; i < no_tri; i++) {
    fxmin = triangles[i].x[0];
    fymin = triangles[i].y[0];
    fxmax = triangles[i].x[0];
    fymax = triangles[i].y[0];
    printf("Triangle %d:\n",i);
    for(j = 0; j < 3; j++) {
      printf("x[%d]=%7.2f, y[%d]=%7.2f\n",j,triangles[i].x[j],
        j,triangles[i].y[j]);
      if(fxmin > triangles[i].x[j])
	fxmin = triangles[i].x[j];
      if(fymin > triangles[i].y[j])
	fymin = triangles[i].y[j];
      if(fxmax < triangles[i].x[j])
	fxmax = triangles[i].x[j];
      if(fymax < triangles[i].y[j])
	fymax = triangles[i].y[j];
    }
    printf("     area = %7.2f, xpos = %d, ypos = %d\n",
      triangles[i].area,triangles[i].xpos,triangles[i].ypos);
    if(fxmin < (float)triangles[i].xpos ||
      fxmax > (float)triangles[i].xpos + 1 ||
      fymin < (float)triangles[i].ypos ||
      fymax > (float)triangles[i].ypos + 1) {
      printf("Error: x[%f:%f] y[%f:%f]\n",fxmin,fxmax,fymin,fymax);
    }

  }

  /*
   * Print the area of the input pixel contained in each grid square.
   */
  for(j = ymin; j <= ymax; j++) {
    for(i = xmin; i <= xmax; i++) {
      printf("%6.2f ",parts[idx++]);
    }
    printf("\n");
  }

  /*
   * Print the total area of the input pixel, determined as the sum over all
   * the triangle areas.
   */
  sumparts = 0;
  for(i = 0; i < no_tri; i++)
    sumparts += triangles[i].area;
  printf("============  %f (total) = %f (sum of parts)\n",total,sumparts);

  /*
   * Write all the triangle coordinates to the file "/tmp/triangles".
   */
  file = fopen("/tmp/triangles","w");
  for(i = 0; i < no_tri; i++) {
    fprintf(file,"%f %f 1\n%f %f\n%f %f\n%f %f\n",
      triangles[i].x[0],triangles[i].y[0],triangles[i].x[1],triangles[i].y[1],
      triangles[i].x[2],triangles[i].y[2],triangles[i].x[0],triangles[i].y[0]);
  }
  fclose(file);
  return(0);
} /* debugout */

/* TRIANGLE CALCULATION END */

/*==============================================================================
 * Put the pixel sizes, center coordinates, coordinate offsets, sample
 * distance, bin sizes, projection type and detector rotations of the
 * distortion files in the image header structure type, if this has been
 * selected by the user with the global variable DSTRTVAL (everything else 
 * than prerotation parameters)
 *
 * In detail: the value of DSTRTVAL is compared (with a "bitwise and") to
 * several flags:
 * - if the comparison is true, then the corresponding value is taken from 
 *   the distortion file headers (SDXTYP, SDYTYP).
 * - else the values of the image header are retained for the value.
 *
 * Input : type: type of header to be updated 
 *         out_type: 0, header type is updated depending on moded
 *                   1, output header is a copy of type
 *                      and updated depending on mode 
 *         mode: 0 Copy only type to out_type
 *         mode: 1 update output header from SDXTYP and SDYTYP
 * Return: 0  no errors
 *         -1 invalid header type
 */

int upd_headval( int type, int out_type, int mode )
{ struct data_head *user_head, *out_head;

  if (type <= 0) goto upd_headval_error;
  user_head = &(img_head[type]);

  if ( ( out_type>INVALID_TYP ) && ( type != out_type ) ) {
    prmsg(DMSG,("upd_headval: copy %s header to %s.\n", typestr[type],typestr[out_type]));
    if (out_type <= 0) goto upd_headval_error;
    out_head = &(img_head[out_type]);
    /* 
     * copy type header to output header
     */

    *out_head = *user_head;

  } else {
    /*
     * output header is identical to input header
     */
    prmsg(DMSG,("upd_headval: update %s header.\n",typestr[type]));

    out_head = user_head; 
  }

  if (mode==1) {
    /*
     * update from SDX SDY headers
     */
    if(DSTRTVAL) {
      if(DSTRTVAL & FL_OFFS1) {
        if(img_head[SDXTYP].Dspinit & FL_OFFS1) {
          out_head->Offset_1 = img_head[SDXTYP].DspOffset_1;
          out_head->init |= FL_OFFS1;
        } else if(img_head[SDXTYP].init & FL_OFFS1) {
          out_head->Offset_1 = img_head[SDXTYP].Offset_1;
          out_head->init |= FL_OFFS1;
        }
      }
      if(DSTRTVAL & FL_OFFS2) {
        if(img_head[SDYTYP].Dspinit & FL_OFFS2) {
          out_head->Offset_2 = img_head[SDYTYP].DspOffset_2;
          out_head->init |= FL_OFFS2;
        } else if(img_head[SDYTYP].init & FL_OFFS2) {
          out_head->Offset_2 = img_head[SDYTYP].Offset_2;
          out_head->init |= FL_OFFS2;
        }
      }
      if(DSTRTVAL & FL_PSIZ1) {
        if(img_head[SDXTYP].Dspinit & FL_PSIZ1) {
          out_head->PSize_1 = img_head[SDXTYP].DspPSize_1;
          out_head->init |= FL_PSIZ1;
        } else if(img_head[SDXTYP].init & FL_PSIZ1) {
          out_head->PSize_1 = img_head[SDXTYP].PSize_1;
          out_head->init |= FL_PSIZ1;
        }
      }
      if(DSTRTVAL & FL_PSIZ2) {
        if(img_head[SDYTYP].Dspinit & FL_PSIZ2) {
          out_head->PSize_2 = img_head[SDYTYP].DspPSize_2;
          out_head->init |= FL_PSIZ2;
        } else if(img_head[SDYTYP].init & FL_PSIZ2) {
          out_head->PSize_2 = img_head[SDYTYP].PSize_2;
          out_head->init |= FL_PSIZ2;
        }
      }
      if(DSTRTVAL & FL_CENT1) {
        if(img_head[SDXTYP].Dspinit & FL_CENT1) {
          out_head->Center_1 = img_head[SDXTYP].DspCenter_1;
          out_head->init |= FL_CENT1;
        } else if(img_head[SDXTYP].init & FL_CENT1) {
          out_head->Center_1 = img_head[SDXTYP].Center_1;
          out_head->init |= FL_CENT1;
        }
      }
      if(DSTRTVAL & FL_CENT2) {
        if(img_head[SDYTYP].Dspinit & FL_CENT2) {
          out_head->Center_2 = img_head[SDYTYP].DspCenter_2;
          out_head->init |= FL_CENT2;
        } else if(img_head[SDYTYP].init & FL_CENT2) {
          out_head->Center_2 = img_head[SDYTYP].Center_2;
          out_head->init |= FL_CENT2;
        }
      }
      if(DSTRTVAL & FL_SAMDS) {
        if(img_head[SDXTYP].Dspinit & FL_SAMDS) {
          out_head->SamplDis = img_head[SDXTYP].DspSamplDis;
          out_head->init |= FL_SAMDS;
        } else if(img_head[SDXTYP].init & FL_SAMDS) {
          out_head->SamplDis = img_head[SDXTYP].SamplDis;
          out_head->init |= FL_SAMDS;
        }
      }
      if(DSTRTVAL & FL_BSIZ1) {
        if(img_head[SDXTYP].Dspinit & FL_BSIZ1) {
          out_head->BSize_1 = img_head[SDXTYP].DspBSize_1;
          out_head->init |= FL_BSIZ1;
        } else if(img_head[SDXTYP].init & FL_BSIZ1) {
          out_head->BSize_1 = img_head[SDXTYP].BSize_1;
          out_head->init |= FL_BSIZ1;
        }
      }
      if(DSTRTVAL & FL_BSIZ2) {
        if(img_head[SDYTYP].Dspinit & FL_BSIZ2) {
          out_head->BSize_2 = img_head[SDYTYP].DspBSize_2;
          out_head->init |= FL_BSIZ2;
        } else if(img_head[SDYTYP].init & FL_BSIZ2) {
          out_head->BSize_2 = img_head[SDYTYP].BSize_2;
          out_head->init |= FL_BSIZ2;
        }
      }
      if(DSTRTVAL & FL_PRO) {
        if(img_head[SDXTYP].Dspinit & FL_PRO) {
          strcpy(out_head->ProjTyp,img_head[SDXTYP].DspProjTyp);
          out_head->init |= FL_PRO;
        } else if(img_head[SDXTYP].init & FL_PRO) {
          strcpy(out_head->ProjTyp,img_head[SDXTYP].ProjTyp);
          out_head->init |= FL_PRO;
        }
      }
      if(DSTRTVAL & FL_ROT1) {
        if(img_head[SDXTYP].Dspinit & FL_ROT1) {
          out_head->DetRot_1 = img_head[SDXTYP].DspDetRot_1;
          out_head->init |= FL_ROT1;
        } else if(img_head[SDXTYP].init & FL_ROT1) {
          out_head->DetRot_1 = img_head[SDXTYP].DetRot_1;
          out_head->init |= FL_ROT1;
        }
      }
      if(DSTRTVAL & FL_ROT2) {
        if(img_head[SDXTYP].Dspinit & FL_ROT2) {
          out_head->DetRot_2 = img_head[SDXTYP].DspDetRot_2;
          out_head->init |= FL_ROT2;
        } else if(img_head[SDXTYP].init & FL_ROT2) {
          out_head->DetRot_2 = img_head[SDXTYP].DetRot_2;
          out_head->init |= FL_ROT2;
        }
      }
      if(DSTRTVAL & FL_ROT3) {
        if(img_head[SDXTYP].Dspinit & FL_ROT3) {
          out_head->DetRot_3 = img_head[SDXTYP].DspDetRot_3;
          out_head->init |= FL_ROT3;
        } else if(img_head[SDXTYP].init & FL_ROT3) {
          out_head->DetRot_3 = img_head[SDXTYP].DetRot_3;
          out_head->init |= FL_ROT3;
        }
      }
    }
  }

  return( 0 );

upd_headval_error:

  prmsg(ERROR,("upd_headval: invalid input header type %s\n",typestr[type]));
  return( -1 );

} /* upd_headval */

/*==============================================================================
 * This function calculates the actual prerotation values for the header
 * type. The function sx_tf_params is used for calculation. The prerotations 
 * and the detector orientation are always read from the source header:
 * PreDetRot_1, PreDetRot_2, PreDetRot_3.
 * The parameters PreCenter_1, PreCenter_2, PreSamplDis, can only be 
 * returned if they are either explicitely defined or if they can be
 * calculated from header values of Center_1, Center_2 and SamplDis.
 * A calculation of prerotation parameters is necessary when parameters 
 * are mixed, e.g. center, distance and prerotations, but not precenter,
 * predistance and prerotations. The distance and center values can change
 * between different images.
 *
 * The return value is only zero when all parameters have been
 * successfully calculated.
 *
 * int type      : CORTYP, SDXTYP, SDYTYP
 * int do_prerot : 0 no prerotations, *prot1, *prot2, *prot3 are set to 0
 *                 otherwise the source header values are returned
 *
 * return value:  0 no error, all parameters are updated
 *               >0 error: number of missing (not updated) parameters
 *               -1 calculation error
 */
int calc_prerot( int type, int do_prerot,
                 float *pix1,  float *pix2,
                 float *pcen1, float *pcen2, float *pdis,
                 float *prot1, float *prot2, float *prot3 )
{
  struct sx_params sx;
  struct data_head *src_head = &img_head[SRCTYP],*user_head; 

  int err=0, iret=0;

  user_head = &img_head[type];
 
  sx_init( &sx );
  if ( user_head->init & FL_PSIZ1 ) {
    sx.pix1.V = user_head->PSize_1;
    sx.pix1.I = 1;
  }
  if ( user_head->init & FL_PSIZ2 ) {
    sx.pix2.V = user_head->PSize_2;
    sx.pix2.I = 1;
  }
  if ( user_head->init & FL_CENT1 ) {
    sx.bcen1.V = user_head->Center_1;
    sx.bcen1.I = 1;
  }
  if ( user_head->init & FL_CENT2 ) {
    sx.bcen2.V = user_head->Center_2;
    sx.bcen2.I = 1;
  }
  if ( user_head->init & FL_SAMDS ) {
    sx.bdis.V = user_head->SamplDis;
    sx.bdis.I  = 1;
  }

  if (do_prerot) {
    sx.rot1.V  = src_head->PreDetRot_1; sx.rot1.I  = 1;
    sx.rot2.V  = src_head->PreDetRot_2; sx.rot2.I  = 1;
    sx.rot3.V  = src_head->PreDetRot_3; sx.rot3.I  = 1;
  } else { 
    sx.rot1.V  = 0.0; sx.rot1.I  = 1;
    sx.rot2.V  = 0.0; sx.rot2.I  = 1;
    sx.rot3.V  = 0.0; sx.rot3.I  = 1;
  }

  if ( src_head->init & FL_PRECEN1 ) {
    sx.cen1.V = src_head->PreCenter_1;
    sx.cen1.I = 1;
  }
  if ( src_head->init & FL_PRECEN2 ) {
    sx.cen2.V = src_head->PreCenter_2;
    sx.cen2.I = 1;
  }
  if ( src_head->init & FL_PREDIS ) {
    sx.dis.V = src_head->PreSamplDis;
    sx.dis.I = 1;
  }

  if (sx_tf_params ( &sx, &sx, src_head->Orientat , 0, &err )) {
    iret=8;
    *pix1  = sx.pix1.V; iret-=sx.pix1.I;
    *pix2  = sx.pix2.V; iret-=sx.pix2.I;

    *pcen1 = sx.cen1.V; iret-=sx.cen1.I;
    *pcen2 = sx.cen2.V; iret-=sx.cen2.I;
    *pdis  = sx.dis.V;  iret-=sx.dis.I;

    *prot1 = sx.rot1.V; iret-=sx.rot1.I;
    *prot2 = sx.rot2.V; iret-=sx.rot2.I;
    *prot3 = sx.rot3.V; iret-=sx.rot3.I;

    if (iret > 0) {
      prmsg(ERROR,("calc_prerot: missing %d prerotation parameters.\n",iret));
      goto calc_prerot_error;
    }
     
  } else {
    iret=-1;
    prmsg(ERROR,("calc_prerot: sx parameter transformation error %d.\n",err));
    goto calc_prerot_error;
  }

  return( 0 );

calc_prerot_error:

  return(iret);

} /* calc_prerot */

/*==============================================================================
 * The CORTYP header is updated with DSTRTVAL values using 
 * upd_headval. Because the prerotations will be applied
 * after the distortion correction to the corrected image 
 * the actual pixel size is copied from CORTYP to SDXTYP
 * and all prerotation parameters are copied from the 
 * (previously mapped) SRCTYP header to SDXTYP. If not all 
 * required prerotation parameters are now available in 
 * SDXTYP it is tried to calculate them in agreement with 
 * the following CORTYP header values:
 *
 *   Center_1, Center_2, SamplDis
 *
 * The values of PreCenter_1, PreCenter_2 and PreSamplDis
 * are then updated with the calculated values.
 *
 * After prerotation the detector is perpendicular to the
 * beam, Center_1, Center_2 are equal to the beam center,
 * and SampleDistance is equal to the beam distance.
 *
 * int type    : SDXTYP or SDYTYP
 * return value:  0 no error
 *               -1 error
 */
 
int set_prerot_headval( int type )
{
  struct data_head *user_head;

  user_head = &img_head[type];

  if (DO_PREROT!=2)
    upd_headval( CORTYP, TMPTYP, 1 );
  else upd_headval( CORTYP, TMPTYP, 0 );

  /* 
   * calculate all prerotation parameters for user_head type
   */

  if (calc_prerot( TMPTYP, 1,
        &(user_head->PSize_1), 
        &(user_head->PSize_2),
        &(user_head->PreCenter_1), 
        &(user_head->PreCenter_2),
        &(user_head->PreSamplDis),
        &(user_head->PreDetRot_1),
        &(user_head->PreDetRot_2),
        &(user_head->PreDetRot_3)) )
    goto set_prerot_headval_error;

  user_head->init |= FL_PSIZ1;
  user_head->init |= FL_PSIZ2;
  user_head->init |= FL_PRECEN1;
  user_head->init |= FL_PRECEN2;
  user_head->init |= FL_PREDIS;
  user_head->init |= FL_PREROT1;
  user_head->init |= FL_PREROT2;
  user_head->init |= FL_PREROT3;

  return(0);

set_prerot_headval_error:

  return(-1);

} /* set_prerot_headval */

/*==============================================================================
 * Gets for all pixel points in the input image the corrected coordinates.
 *
 * There are several types of correction. Any combination of them can be
 * performed according to the options chosen by the user at program startup:
 *
 * - distortion correction
 * - dark image subtraction
 * - floodfield correction
 * - intensity normalization
 * - scattering background subtraction
 *
 * The buffer for the output values must have been allocated before calling
 * this routine in order to get any values back. If this buffer is a NULL
 * pointer, the routine will just free all buffers of the look-up-table
 * structure and return without any further action.
 *
 * If there are images for the dark image, floodfield or scattering background
 * corrections, then the corresponding buffers have to be set up before calling
 * this routine by calls to set_imgbuf(). These calls set the buffer pointers
 * DRK_IM, FLO_IM and BCKG_IM that are used in this routine.
 *
 * If any of the required corrections fails, the image will not be processed any
 * further and the routine returns with an error.
 *
 * Input : cor_im: (empty) buffer for the corrected input pixel values
 *         src_im: buffer with the input image
 * Output: cor_im: buffer with the corrected pixel values for the input image
 * Return: -1  if any of the required corrections failed
 *         -2  if there is no output buffer given
 *          0  else
 */

int correct_image(float *cor_im,float *src_im)
{
  static int temp_siz = 0;
  static float drkconst;
  static float *temp_im = NULL;
  static struct lut_descript *lut_d = NULL;
  int drk_cor = DO_DARK && (DRK_IM || DRK_CONST != 0);
  float *ptemp,*inbuf,*outbuf;
  int lut_invalid = 0;

  /*
   * Free the buffers of the look-up-table structure if
   * - there is no buffer for the output, or
   * - the existing look-up table is no longer valid (then LUT_INVALID = 1,
   *   this is in particular at program startup).
   *
   * If there is no output buffer, the routine just returns afterwards with
   * return status -2.
   */

  if (LUT_INVALID || cor_im == NULL) {
    lut_invalid = 1;
  } else {
    if (img_head[SDXTYP].init) {

      if ( (DO_SPD) && (SDX_PREROT!=DO_PREROT) ) {
        lut_invalid = 1;
      } else if ( img_head[SDXTYP].Dim_1 != img_head[SRCTYP].Dim_1 + 1 ||
           img_head[SDXTYP].Dim_2 != img_head[SRCTYP].Dim_2 + 1 ||
           img_head[SDXTYP].BSize_1 != img_head[SRCTYP].BSize_1 ||
           img_head[SDXTYP].BSize_2 != img_head[SRCTYP].BSize_2 ||
           img_head[SDXTYP].Offset_1 != img_head[SRCTYP].Offset_1 - 0.5 ||
           img_head[SDXTYP].Offset_2 != img_head[SRCTYP].Offset_2 - 0.5 ) {
        lut_invalid = 1;
      } else if (DO_PREROT) {
        float PSize_1, PSize_2, PreDetRot_1, PreDetRot_2, PreDetRot_3;
        float PreCenter_1, PreCenter_2, PreSamplDis;
        /* upd_headval reads from SDXTYP header. When it was never
         * read the flags are still 0 and nothing is done. If the shift 
         * file was changed LUT_INVALID was already set by set_splinfil or 
         * set_xycorin and upd_headval is not called here.
         * If upd_headval fails it is an error and the program 
         * could be stopped. A return is avoided here to keep 
         * this part of the program transparent to earlier versions.
         */

        if (DO_PREROT!=2)
          upd_headval( CORTYP, TMPTYP, 1 );
        else upd_headval( CORTYP, TMPTYP, 0 );

        if ( calc_prerot( TMPTYP, DO_PREROT,
                    &PSize_1, &PSize_2,
                    &PreCenter_1, &PreCenter_2, &PreSamplDis,
                    &PreDetRot_1, &PreDetRot_2, &PreDetRot_3 ) ) {
          lut_invalid = 1;
        } else if ( img_head[SDXTYP].PSize_1 != PSize_1 ||
                    img_head[SDXTYP].PSize_2 != PSize_2 ||
                    img_head[SDXTYP].PreCenter_1 != PreCenter_1 ||
                    img_head[SDXTYP].PreCenter_2 != PreCenter_2 ||
                    img_head[SDXTYP].PreSamplDis != PreSamplDis ||
                    img_head[SDXTYP].PreDetRot_1 != PreDetRot_1 ||
                    img_head[SDXTYP].PreDetRot_2 != PreDetRot_2 ||
                    img_head[SDXTYP].PreDetRot_3 != PreDetRot_3 ) {
          lut_invalid = 1;
        }
      }
    }
  }

  if (lut_invalid) {
    if(lut_d != NULL) {
      if(lut_d->lut)
	pfree(lut_d->lut);
      if(lut_d->prog)
	pfree(lut_d->prog);
      if(lut_d->offset_tab)
	pfree(lut_d->offset_tab);
      if(lut_d->rel_tab)
	pfree(lut_d->rel_tab);
      if(lut_d->relend_tab)
	pfree(lut_d->relend_tab);
      if(lut_d->abs_src)
	pfree(lut_d->abs_src);
      if(lut_d->xrel)
        pfree(lut_d->xrel);
      if(lut_d->yrel)
        pfree(lut_d->yrel);
      pfree(lut_d);
      lut_d = NULL;
    }
    if(cor_im == NULL)
      return(-2);
    LUT_INVALID = 1;
  }

  bench(NULL);

  /*
   * Calculate a new set of tables for the distortion correction, if necessary.
   */
  if(LUT_INVALID && DO_SPD) {
    prmsg(DMSG,("Calculating look-up table: %d bit\n",LUT_BYTE ? 8 : 16));
    if((lut_d = lut_calc()) == NULL)
      return(-1);
    LUT_INVALID = 0;
    bench("look-up-table calculation");
  }

  /*
   * Allocate space for temporary image, if necessary.
   *
   * If there is already space allocated for it, check whether its size is
   * correct. Free and re-allocate if not.
   */
  if(temp_im != NULL && (XSIZE * YSIZE * sizeof(float)) != temp_siz) {
    pfree(temp_im);
    temp_im = NULL;
  }

  if(temp_im == NULL)
    if((temp_im = (float *)pmalloc(XSIZE * YSIZE * sizeof(float))) == NULL)
      prmsg(FATAL,("no memory in correct_calc for temp src\n"));
    else
      temp_siz = XSIZE * YSIZE * sizeof(float);

  /*
   * Start the corrections:
   * - dark image;
   * - floodfield;
   * - distortion;
   * - intensity normalization;
   * - scattering background.
   *
   * All corrections are optional. If the corresponding files or parameters
   * are not defined, the correction will be skipped.
   *
   * First is the dark image correction. The image resulting from the dark image
   * correction can be scaled with a multiplicative and an additive input
   * scaling factor.
   *
   * If the user-defined flag DO_DARK is 0, the dark image correction is
   * suppressed.
   *
   * Afterwards is the floodfield correction, then intensity normalization and
   * last scattering background subtraction.
   *
   * The distortion correction can be done at various points during the
   * processing, depending on the DO_SPD flag set by the user:
   * - DO_SPD = 0  no distortion correction
   *          = 1  after the dark subtraction, before flat field division
   *          = 2  after the flat field division, before the normalization
   *          = 3  after the normalization
   *
   * All corrections can be done in situ, i.e. the input and output buffer can
   * (but need not) be identical, with the exception of the distortion
   * correction, where the input and output buffer must be different.
   *
   * Therefore, in the following code, the idea is that if there is no
   * distortion correction, then the first correction copies the image from the
   * input to the output buffer, and all subsequent routines then work on the
   * output buffer. As there is always at least the input scaling correction
   * done, this guarantees that there is a valid image in the output buffer when
   * this routine returns.
   *
   * If there is a distortion correction, then the first correction (which might
   * be the input scaling correction) copies the input buffer to a temporary
   * buffer, all subsequent corrections before the distortion correction work in
   * this buffer, the distortion correction copies the temporary buffer to the
   * output buffer, and any corrections after that work on the output buffer.
   */
  inbuf = src_im;
  outbuf = (DO_SPD != 0) ? temp_im : cor_im;

 /*
  * Dark image subtraction and scaling of input image.
  */
  if(drk_cor) {
    drkconst = DRK_CONST;
    /*
     * Apply the dark image correction to the source image, then scale the
     * resulting image.
     */
    prmsg(DMSG,("Correcting: dark image subtraction\n"));
    // ++++++++++ subtract_im(inbuf,DRK_IM,outbuf,0.,drkconst);
    subtract_drk(inbuf,DRK_IM,outbuf,drkconst);
    inbuf = outbuf;
  }

  scale_im(inbuf,outbuf,INPFACT,INPCONST,XSIZE * YSIZE);
  inbuf = outbuf;

 /*
  * Distortion and floodfield corrections (distortion before or after
  * floodfield).
  */
  if(DO_SPD == 1) {
    prmsg(DMSG,("Correcting: distortion correction\n"));
    outbuf = cor_im;
    undistort_im(outbuf,inbuf,lut_d);
  }

  if(FLO_IM) {
    prmsg(DMSG,("Correcting: floodfield correction\n"));
    divide_insito_im(outbuf,FLO_IM);
  }

  if(DO_SPD == 2) {
    prmsg(DMSG,("Correcting: distortion correction\n"));
    inbuf = outbuf;
    outbuf = cor_im;
    undistort_im(outbuf,inbuf,lut_d);
  }

  /*
   * Intensity normalization, distortion after normalization, scattering
   * background corrections and distortion after background subtraction.
   */
  if(NORM_INT) {
    prmsg(DMSG,("Correcting: intensity normalization\n"));
    if(normint_im(outbuf,outbuf,NORM_INT) != 0)
      return(-1);
  }
  if(DO_SPD == 3) {
    prmsg(DMSG,("Correcting: distortion correction\n"));
    inbuf = outbuf;
    outbuf = cor_im;
    undistort_im(outbuf,inbuf,lut_d);
  }
  if(BCKG_IM) {
    prmsg(DMSG,("Correcting: scattering background subtraction\n"));
    // ++++++++++++++scale_im(BCKG_IM,temp_im,BCKGFACT,BCKGCONST,XSIZE * YSIZE);
    // ++++++++++++++subtract_drk(cor_im,temp_im,cor_im,0.);
    subtract_im(outbuf,BCKG_IM,outbuf,BCKGFACT,BCKGCONST);
  }
  if(DO_SPD == 4) {
    prmsg(DMSG,("Correcting: distortion correction\n"));
    inbuf = outbuf;
    outbuf = cor_im;
    undistort_im(outbuf,inbuf,lut_d);
  }

  /*
   * Mark invalid pixels in the output image.
   *
   * Invalid pixels are those where at least one of the following conditions
   * is true:
   * 1) the value of the pixel in the source, dark current or scattering
   *    background image is equal to the value of the "Dummy" keyword in the
   *    corresponding image header;
   * 2) the value of the pixel in the source or dark current image is equal to
   *    the value of the "overflow" command line argument;
   * 3) the value of the pixel in the source or dark current image is less
   *    than the value of the "inp_min" command line argument or greater than
   *    the value of the "inp_max" command line argument;
   * 4) the value of the pixel in the floodfield image is 0. or equal to the
   *    value of the "Dummy" keyword in the floodfield image header.
   *
   * Pixels that correspond to conditions 1 to 3 for the source or dark
   * current image will be marked with the output image "Dummy" value by
   * mark_overflow_nocorr(). The marking is done in a temporary copy of the
   * input image.
   *
   * Pixels that correspond to condition 1 for the scattering background
   * image are marked directly in the corrected image.
   *
   * If the floodfield correction is done before the distortion correction, then
   * the pixels corresponding to condition 4 are marked in the temporary copy
   * of the input image mentioned above. If the floodfield correction is done
   * after the distortion correction, the corresponding pixels are marked
   * directly in the corrected image.
   *
   * Therefore, in this temporary copy of the input image, all invalid pixels
   * are now marked with the output image "Dummy" value.
   *
   * mark_overflow() takes this temporary copy of the input image, transfers
   * the illegal pixels to the corresponding distortion corrected pixels in
   * the corrected image and sets their values to the output image "Dummy"
   * value. Note that one input pixel may correspond to more than one output
   * pixel, in which case all the corresponding output pixels are set to
   * "Dummy".
   */

  if ( DO_SPD != 0 ) {
    /* 
     * avoid loop, because temp_in is not used for DO_SPD == 0 
     */
    for(ptemp = temp_im + XSIZE * YSIZE - 1; ptemp >= temp_im; ptemp--)
      *ptemp = 0.;
    outbuf = temp_im;
  } else outbuf = cor_im;

  mark_overflow_nocorr(src_im,outbuf,NULL,SRCTYP);
  mark_overflow_nocorr(DRK_IM,outbuf,drdumlst,DRKTYP);

  if(DO_SPD == 1) {
    outbuf = cor_im;
    mark_overflow(temp_im,outbuf,lut_d,Dummy);
  }
  mark_overflow_nocorr(FLO_IM,outbuf,fldumlst,FLOTYP);
  if(DO_SPD == 2 || DO_SPD == 3) {
    outbuf = cor_im;
    mark_overflow(temp_im,outbuf,lut_d,Dummy);
  }

  if(BCKG_IM)
    mark_overflow_nocorr(BCKG_IM,outbuf,NULL,SBKTYP);

  if(DO_SPD == 4) {
    outbuf = cor_im;
    mark_overflow(temp_im,outbuf,lut_d,Dummy);
  }

  bench("image correction");
  return(0);
} /* correct_image */

/*==============================================================================
 * Exponentiates all values in the input image with an exponential constant and
 * stores the result in the output image.
 *
 * If the exponential constant is 1., the input image is just copied to the
 * output image.
 *
 * The calculation is done for each pixel of the image with the formula
 *
 *      output = pow(input, exponent)
 *
 * As this calculation is slow, a lookup table "exptab" is used for all pixel
 * values between 0 and USHRT_MAX. The values in this table are calculated
 * (in the routine set_inpexp()) with the formula
 *
 *      exptab(index) = pow(index, exponent)
 *
 * i.e., the exponential function is approximated in the value interval between
 * "index" and "index + 1" by the exponentiated value of the lower border of the
 * interval. The exponentiation of the input pixel values is then simply done by
 *
 *      index  = input
 *      output = exptab(index)
 *
 * As the pixel values in the detector are unsigned short integers, this should
 * be a good approximation. Observation showed it to be correct to at least 5
 * decimal digits.
 * 
 * For pixel values outside this range, the real formula is used. In principle
 * there should be none, as the input image pixels should have the range of an
 * "unsigned short" integer.
 *
 * Input : src_im: buffer with the input image (float)
 *         expconst: constant exponent for the values of the input image (float)
 * Output: out_im: buffer with output image (= corrected input) (float)
 * Return: -1  if src_im or out_im are NULL pointers
 *          0  else
 */

int expon_im(float *src_im,float *out_im,float expconst,int imgsize)
{
  register int index;
  register float *tempptr,*templast;
  int count = 0;
  float *srcptr;

  if(src_im == NULL || out_im == NULL)
    return(-1);

  if(expconst != 1.) {
    /*
     * There is an exponential constant. Apply it to the input image.
     */
    srcptr = src_im;
    tempptr = out_im;
    templast = out_im + imgsize - 1;
    while(tempptr <= templast)
    {
      index = *srcptr;
      if(index < 0 || index > USHRT_MAX)
      {
	*tempptr++ = pow((double)*srcptr,expconst);
	count++;
      }
      else
	*tempptr++ = *(exptab + index);
      srcptr++;
    }
    if(count != 0)
      prmsg(WARNING,("%d pixel values < 0 or > USHRT_MAX\n",count));
  } else if(out_im != src_im) {
    /*
     * The exponential constant is 1., i.e. no exponentiation is to be done.
     * Just copy the input image to the output image.
     */
    memcpy(out_im,src_im,imgsize * sizeof(float));
  }
  return(0);
} /* expon_im */

/*==============================================================================
 * Adjusts all values in the input image with an additive and a multiplicative
 * constant and stores the result into the output image.
 *
 * The adjustment is done for each pixel of the image with the formula
 *
 *      output  =  input * multiplicative + additive
 *
 * It is perfectly possible for this to yield a negative value for any given
 * pixel, which is then written as such (i.e. negative) in the output image.
 *
 * Input : src_im: buffer with the input image (float)
 *         mulconst: constant to be multiplied with the input image (float)
 *         addconst: constant to be added to the input image (float)
 * Output: out_im: buffer with output image (= corrected input) (float)
 * Return: -1  if src_im or out_im are NULL pointers
 *          0  else
 */

int scale_im(float *src_im,float *out_im,register float mulconst,
  register float addconst,int imgsize)
{
  register float *srcptr,*tempptr,*templast;

  if(src_im == NULL || out_im == NULL)
    return(-1);

  srcptr = src_im;
  tempptr = out_im;
  templast = out_im + imgsize - 1;

  if(addconst) {
    if(mulconst != 1.) {
      /*
       * There is an additive and a multiplicative constant. Multiply the input
       * image with the multiplicative constant, then add the additive constant.
       */
      while(tempptr <= templast)
        *tempptr++ = *srcptr++ * mulconst + addconst;
    }
    else
    {
      /*
       * There is an additive constant, but the multiplicative constant is 1.
       * Add the additive constant to the input image.
       */
      while(tempptr <= templast)
        *tempptr++ = *srcptr++ + addconst;
    }
  } else if(mulconst != 1.) {
    /*
     * There is no additive constant, but there is a multiplicative constant.
     * Multiply the the input image with the multiplicative constant.
     */
    while(tempptr <= templast)
      *tempptr++ = *srcptr++ * mulconst;
  } else if(out_im != src_im) {
    /*
     * There is neither an additive nor a multiplicative constant. Just copy the
     * input image to the output image.
     */
    memcpy(out_im,src_im,imgsize * sizeof(float));
  }
  return(0);
} /* scale_im */

/*==============================================================================
 * Performs a dark image subtraction for each pixel of the input image and
 * stores the result into the output image.
 *
 * It is perfectly possible for this subtraction to yield a negative value
 * for any given pixel, which is then written as such (i.e. negative) in the
 * output image.
 *
 * The background can be defined by the user at program startup on a pixel
 * basis and / or on a global basis. For the former, a background image has
 * to be specified that contains the pixel values for the background. For the
 * latter, a background constant has to be specified in the input arguments.
 *
 * There are three possibilities:
 *
 * 1) there is a background image (i.e., "drk_im" is not NULL), and the
 *    background constant is 0. Then for each pixel the background values of
 *    the background image are subtracted from the corresponding values in the
 *    input image;
 * 2) there is no background image. Then the background constant is subtracted
 *    from each pixel in the input image;
 * 3) there is a background image, but the background constant is not 0. Then
 *    for each pixel in the input image first the background constant and then
 *    the corresponding pixel value of the background image are subtracted from
 *    it.
 *
 * Input : src_im: buffer with the input image (float)
 *         drk_im: buffer with the dark image (float)
 *         drkcin: dark image constant
 * Output: out_im: buffer with output image (= corrected input) (float)
 * Return: 0  if no errors (always at present)
 */

int subtract_drk(float *src_im,float *drk_im,float *out_im,float drkcin)
{
  register float *drkptr,*srcptr,*tempptr,*templast;
  register float drk_const = drkcin;

  srcptr = src_im;
  tempptr = out_im;
  templast = out_im + XSIZE * YSIZE - 1;
  if(drk_im) {
    drkptr = drk_im;
    if(drk_const == 0.)
    {
      /*
       * There is a background image, but the global background variable is 0.
       * Subtract the background image from the input image.
       */
      while(tempptr <= templast) {
        *tempptr++ = *srcptr++ - *drkptr++;
      }
    }
    else
    {
      /*
       * There is a background image, and the global background variable is not
       * 0. Subtract the global background variable and the background image
       * from the input image.
       */
      while(tempptr <= templast) {
        *tempptr++ = *srcptr++ - drk_const - *drkptr++;
      }
    }
  } else {
    /*
     * There is no background image. Subtract the global background variable
     * from the input image.
     */
    while(tempptr <= templast) {
      *tempptr++ = *srcptr++ - drk_const;
    }
  }
  return(0);
} /* subtract_drk */

/*==============================================================================
 * Performs a pixel by pixel subtraction of sub_im from src_im and
 * writes the result into out_im. The values of sub_im are multiplied with 
 * fac and con is added.
 *
 * It is perfectly possible for this subtraction to yield a negative value
 * for any given pixel, which is then written as such (i.e. negative) in the
 * output image.
 *
 * There are three possibilities:
 *
 * 1) sub_im != NULL, fac == 1., con == 0.
 * 2) sub_im != NULL
 * 3) sub_im == NULL
 *
 * 1) there is an image to subtract (i.e., "sub_im" is not NULL), fac is 1.
 *    and con is 0. Then each pixel of the sub image is subtracted from
 *    the input image and written to the output image.
 * 2) there is no image to subtract. Then con is subtracted from each pixel 
 *    in the input image and written to the output image.
 * 3) there is an image to subtract, the factor is not 1. or the constant 
 *    is not 0. Then each pixel of the sub image is multiplied with fac and 
 *    subtracted together with con from the input image. The values are
 *    written to the output image.
 *
 * Input : src_im: buffer with the input image (float)
 *         sub_im: buffer to be subtracted (float)
 *         fac:    multiplication factor
 *         con:    added constant
 * Output: out_im: buffer with output image (float)
 * Return: 0  if no errors (always at present)
 */

int subtract_im(float *src_im,float *sub_im,float *out_im,float fac,float con)
{
  register float *subptr,*srcptr,*tempptr,*templast;
  register float factor, constant;

  srcptr = src_im;
  tempptr = out_im;
  templast = out_im + XSIZE * YSIZE - 1;
  if(sub_im) {
    subptr = sub_im;
    if((factor == 1.)&&(constant == 0.))
    {
      /*
       * Subtract sub_im from src_im.
       */
      while(tempptr <= templast) {
        *tempptr++ = *srcptr++ - *subptr++;
      }
    }
    else
    {
      /*
       * Subtract fac*sub_im+con from src_im
       */
      while(tempptr <= templast) {
        *tempptr++ = *srcptr++ - constant - *subptr++*factor;
      }
    }
  } else {
    /*
     * There is no image to subtract. Subtract the global background variable
     * from the input image.
     */
    while(tempptr <= templast) {
      *tempptr++ = *srcptr++ - constant;
    }
  }
  return(0);
} /* subtract_im */

/*==============================================================================
 * Perform the floodfield (also called flatfield) correction and store the
 * result back into the input image.
 *
 * This takes into account the fact that a sample with absolutely uniform
 * scattering response does not necessarily produce a flat image (e.g. because
 * of a non-uniform detector response). If one takes a image of a real sample,
 * one has to correct for this non-uniformity.
 *
 * This is done by taking an image from an uniformly scattering probe (the
 * floodfield image) and then dividing the real image by the floodfield image.
 *
 * This routine multiplies each pixel of the input image with the value of the
 * corresponding pixel of the inverted floodfield image and stores the result
 * back into the input image.
 *
 * This routine is identical in functionality to the routine divide_im(),
 * except that this routine stores the result back into the input array.
 *
 * Input : src_im: buffer with the input image (float)
 *         flo_im: buffer with the (inverted) floodfield image (float)
 * Output: src_im: buffer with corrected input image
 * Return: 0  if no errors (always at present)
 */

int divide_insito_im(float *src_im,float *flo_im)
{
  register float *srclast,*srcptr,*floptr;

  srclast = src_im + XSIZE * YSIZE - 1;
  srcptr = src_im;
  floptr = flo_im;

  while(srcptr <= srclast) {
    *srcptr = *srcptr * *floptr++;
    srcptr++;
  }
  return(0);
} /* divide_insito_im */

/*==============================================================================
 * Perform the floodfield (also called flatfield) correction and store the
 * result into the output image.
 *
 * This takes into account the fact that a sample with absolutely uniform
 * scattering response does not necessarily produce a flat image (e.g. because
 * of a non-uniform detector response). If one takes a image of a real sample,
 * one has to correct for this non-uniformity.
 *
 * This is done by taking an image from an uniformly scattering probe (the
 * floodfield image) and then dividing the real image by the floodfield image.
 *
 * This routine multiplies each pixel of the input image with the value of the
 * corresponding pixel of the inverted floodfield image and stores the result
 * into the output image.
 *
 * Pixels with an invalid value in the floodfield image are marked as invalid in
 * the output image by setting the value in the output image to "Dummy". For
 * details, see routine "prepare_flood()".
 *
 * This routine is identical in functionality to the routine divide_insito_im(),
 * except that this routine stores the result in a separate output array.
 *
 * Input : src_im: buffer with the input image (float)
 *         flo_im: buffer with the floodfield image (float)
 * Output: cor_im: buffer with output image (=corrected input) (float)
 * Return: 0  if no errors (always at present)
 */

int divide_im(float *src_im,float *flo_im,float *cor_im)
{
  register float *srcptr,*tempptr,*templast,*floptr;

  tempptr = cor_im;
  templast = cor_im + XSIZE * YSIZE - 1;
  srcptr = src_im;
  floptr = flo_im;

  while (tempptr <= templast) {
    *tempptr++ = *srcptr++ * *floptr++;
  }
  return(0);
} /* divide_im */

/*==============================================================================
 * Normalize the input image to absolute scattering intensities, then multiply
 * each pixel value with the user-defined scattering intensity normalization
 * factor NORMFACT and store the result into the output image.
 *
 * The values for this calculation are taken from the data header parameters'
 * structure "img_head" for the corrected image (type == CORTYP). If this
 * structure does not contain all needed values, the routine returns with an
 * error.
 *
 * Input : src_im:  buffer with the input image (float)
 *         normint: 0: copy src_im to cor_im
 *                  1: full normalization (I/DOmega/Intensity1*NORMFACT)
 *                  2: normalization to Intensity1 (I/Intensity1*NORMFACT)
 *                  3: normalization to DOmega (I/DOmega*NORMFACT)
 * Output: cor_im:  buffer with corrected input image
 * Return:  0  if no errors
 *         -1  if header parameter structure does not contain all needed values
 */

int normint_im(float *src_im,float *cor_im,int normint)
{
  register int i1;
  register float *srcptr,*corptr;
  register float offcen_1,psize_1;
  register double rd2,rd2h,i2r_1,fact1;
  int i2,err;
  unsigned long reqflg=0;
  float offcen_2,psize_2;
  double dis,dis2,i2r_2,intens1,tmp;

  switch (normint) {
    case 1: 
      reqflg = FL_PSIZ1 | FL_PSIZ2 | FL_OFFS1 | FL_OFFS2 | FL_CENT1 | FL_CENT2 |
        FL_INTE1 | FL_SAMDS;
      break;
    case 2:
      reqflg = FL_INTE1;
      break;
    case 3:
      reqflg = FL_PSIZ1 | FL_PSIZ2 | FL_OFFS1 | FL_OFFS2 | FL_CENT1 | FL_CENT2 |
        FL_SAMDS;
      break;
  }

  if((img_head[CORTYP].init & reqflg) != reqflg) {
    prmsg(ERROR,
      ("needed header parameters not set - cannot normalize intensities\n"));
    pr_headval(stdout, CORTYP);
    goto normint_im_error;
  }

  srcptr = src_im;
  corptr = cor_im;

  fact1 = NORMFACT;
  intens1 = 1.0;

  switch (normint) {
    case 1: // full normalization
      intens1 = num_str2double((img_head[CORTYP].Intens_1),NULL,&err);

    case 3: // normalize to DOmega
      psize_1 = img_head[CORTYP].PSize_1;
      psize_2 = img_head[CORTYP].PSize_2;
      offcen_1 = img_head[CORTYP].Offset_1 - img_head[CORTYP].Center_1;
      offcen_2 = img_head[CORTYP].Offset_2 - img_head[CORTYP].Center_2;
      dis = img_head[CORTYP].SamplDis;
      tmp = dis * psize_1 * psize_2 * intens1;
      if (tmp!=0.0) fact1 /= tmp;
      else fact1 = 0.0; // nan
      dis2 = dis * dis;

      for(i2 = 0; i2 < YSIZE; i2++) {
        i2r_2 = INDEX2R(i2,offcen_2,psize_2);
        rd2h = dis2 + i2r_2 * i2r_2;
        for(i1 = 0; i1 < XSIZE; i1++, corptr++, srcptr++) {
          i2r_1 = INDEX2R(i1,offcen_1,psize_1);
          rd2 = rd2h + i2r_1 * i2r_1;
          *corptr = *srcptr * sqrt(rd2) * rd2 * fact1;
        }
      }

      break;

    case 2: // normalize to Intensity1
      intens1 = num_str2double((img_head[CORTYP].Intens_1),NULL,&err);
      if (intens1!=0.0)
        fact1 /= intens1;
      else fact1 = 0.0; // nan

    default: // just copy
      for(i2 = 0; i2 < YSIZE; i2++) {
        for(i1 = 0; i1 < XSIZE; i1++, corptr++, srcptr++) {
          *corptr = *srcptr * fact1;
        }
      }
  }

  return(0);

normint_im_error:

  return(-1);

} /* normint_im */

/*==============================================================================
 * Scans the input image for pixels marked as illegal (value = dummy) and marks
 * the corresponding pixels in the distortion corrected output image.
 *
 * It also marks all pixels in the corrected output image that have no
 * corresponding pixels in the input image.
 *
 * The value used for marking these pixels is "Dummy". This value can be defined
 * with the command line argument "dummy", otherwise the value of the "Dummy"
 * keyword in the source image header is taken. The default (if neither is
 * specified) is 0.
 *
 * The routine returns an error if any of the following conditions is true:
 *
 * - "dummy" is not a valid dummy value (i.e., DUMMYDEFINED() for "dummy"
 *   returns "False"). The routine returns immediately without action;
 * - the pixels of the corrected output image cannot be determined (i.e.,
 *   spd_func() returns an error).
 *
 * Input : src_im: buffer with the input image (float)
 *         lut_d:  structure with the look-up-table for the distortion
 *                 corrections
 *         dummy:  value used to mark illegal pixels in the input image
 * Output: trg_im: buffer for the distortion corrected output image with all
 *                 pixels marked that correspond to illegal pixels in the
 *                 input image (float)
 * Return:  0  if successful
 *         -1  else
 */

int mark_overflow(float *src_im,float *trg_im,struct lut_descript * lut_d,
  float dummy)
{
  unsigned long starttidx = lut_d->starttidx;
  register unsigned char instruct;
  unsigned char *prog_ptr = lut_d->prog;
  float savepix;
  float *lastpix = src_im + XSIZE * YSIZE - 1;
  register float *srcptr,*trgptr;
  register float srcpix;
  short *xrel,*yrel;
  int i,j,multi,maxxpixel,maxypixel,sx,sy,x,y,xmin,xmax,ymin,ymax;
  int idx;
  float ddummy;
#if !defined(WASTE4_FORSPEED)
  float dsx,dsy,dx,dy;
  float fx,fy;
  double active2 = ACTIVE_R * ACTIVE_R;
  double d;
#endif

  prmsg(DMSG,("Set illegal pixels to dummy = %f\n",dummy));
  
  ddummy = DDSET(dummy);
  if(!DUMMYDEFINED(dummy,ddummy))
    return(-1);

  savepix = *lastpix;
  *lastpix = dummy;
  srcptr = src_im;
  maxxpixel = (lut_d->maxxpixel > 3) ? 2 : ((lut_d->maxxpixel > 1) ? 1 : 0);
  maxypixel = (lut_d->maxypixel > 3) ? 2 : ((lut_d->maxypixel > 1) ? 1 : 0);
  xrel = lut_d->xrel;
  yrel = lut_d->yrel;

  /*
   * Set all target pixels to "Dummy" that have no source pixel mapped onto
   * them. This has in principle already done by undistort_im(), but may have been
   * destroyed by divide_insito_im(), thus it needs to be redone here.
   */
  trgptr = trg_im;
  for(i=0; i<starttidx; i++)
    *trgptr++ = Dummy;
  /*
   * Endless loop through the instructions of the look-up-table program.
   * The loop is terminated by a special instruction PROGEND.
   */
  for(;;) {
    /*
     * Get the next instruction.
     *
     * Of interest is really only the exception instruction INCTARGET, but
     * several other instruction also increase the program counter and have
     * thus be processed.
     *
     * If the program instruction is != 0, we have a non-exception instruction.
     * Just increase the program pointer.
     */
    instruct = *prog_ptr++;
    if(instruct != 0)
      prog_ptr++;

    /*
     * If the program instruction is 0, then we have to treat an exception. Get
     * the next instruction and see what kind of exception it is.
     */
    else {
      instruct = *prog_ptr++;
      if(instruct & INCTARGET) {
        /*
         * Exception INCTARGET: there is one or several (if the MULTIINC flag
         * is set as well) target pixels that will not receive a contribution
         * from any source pixel. Just set the target pixel values to Dummy.
         */
	if(instruct & MULTIINC) {
	  multi  = *prog_ptr++;
	  multi += *prog_ptr++ * 256;
	  for(i = multi; i; i--)
            *trgptr++ = Dummy;
	} else
          *trgptr++ = Dummy;
	continue;
      }
      if(instruct & UNCOMPRESSED)
        /*
         * Exception UNCOMPRESSED: increase the program counter while it is != 0
         */
        while(*prog_ptr++);

      else if(instruct & ABSSRC)
        /*
         * Exception ABSSRC: increase the program counter
         */
        prog_ptr++;

      else if(instruct & PROGEND)
        /*
         * Exception PROGEND: terminate the loop over the program instructions.
         */
	break;
    }
      /*
       * Increase target image pointer (except for INCTARGET and PROGEND).
       */
      trgptr++;
  }

  /*
   * End of processing for target pixels without corresponding source pixels.
   *
   * Endless loop over the pixels in the input image to find the "dummy" values.
   */
  for(;;) {
    srcpix = *srcptr;
    if(DUMMY(srcpix,dummy,ddummy))
      /*
       * Terminate loop over input pixels.
       */
      if(srcptr == lastpix)
	break;
      else {
        /*
         * Calculate x and y coordinates (in input image space) of the pixel.
         */
	idx = srcptr - src_im;
	sx = idx % XSIZE; sy = idx / XSIZE;
        /*
         * Obtain the corresponding corrected x and y coordinates (in output
         * image space).
         */
#if WASTE4_FORSPEED
	x = sx + xrel[idx]; y = sy + yrel[idx];
#else /* WASTE4_FORSPEED */
	/* We go to a lower level to make things faster ATTENTION */
	dsx = sx; dsy = sy;

 	/*
         * Don't do anything with illegal pixels outside the active area.
         */
	if(ACTIVE_R) {
	  d = (dsx - XSIZE/2) * (dsx - XSIZE/2) +
	    (dsy - YSIZE/2) * (dsy - YSIZE/2);
	  prmsg(DMSG,("Pixel distance test: Is %f (%f,%f) > %f \n",
	    d,dsx,dsy,active2));
	  if(d > active2) {
	    srcptr++;
	    continue;
	  }
	}

	spd_calcspline(spline,1,1,&dsx,&dsy,&dx,&dy);
	x = dsx + dx + .5; y = dsy + dy + .5;
#endif /* WASTE4_FORSPEED */
/*
	prmsg(DMSG,("Invalid pixel: [%d][%d] (%.1f) -> [%d][%d] (%.1f)\n",
	  sx,sy,dummy,x,y,Dummy));
*/
        /*
         * One pixel in input image space can correspond to more than one pixel
         * in the corrected image space. Mark all corresponding pixels in the
         * corrected image space as "illegal".
         */
	xmin = x - maxxpixel; if(xmin < 0) xmin = 0;
	xmax = x + maxxpixel; if(xmax >= XSIZE) xmax = XSIZE - 1;
	ymin = y - maxypixel; if(ymin < 0) ymin = 0;
	ymax = y + maxypixel; if(ymax >= YSIZE) ymax = YSIZE - 1;
	for(i = xmin; i <= xmax; i++)
	  for(j = ymin * XSIZE; j <= ymax * XSIZE; j += XSIZE)
	    *(trg_im + i + j) = Dummy;
      }
    srcptr++;
  }

  /*
   * This is just a repetition of the above code for the last pixel. Treating
   * this as a special case allows to make the above loop faster by having less
   * comparisons to make.
   */
  *lastpix = savepix;

  if(DUMMY(*lastpix,dummy,ddummy)) {
    /* Same as above */
    idx = srcptr - src_im;
    sx = XSIZE - 1; sy = YSIZE - 1;
#if WASTE4_FORSPEED
    x = sx + xrel[idx]; y = sy + yrel[idx];
#else /* WASTE4_FORSPEED */
    dsx = sx; dsy = sy;

    /* Don't do anything with illegal pixels outside the active area */
    if(ACTIVE_R) {
      d = (dsx - XSIZE/2) * (dsx - XSIZE/2) + (dsy - YSIZE/2) * (dsy - YSIZE/2);
      prmsg(DMSG,("Pixel distance test : Is %f (%f,%f) > %f \n",
	d,dsx,dsy,active2));
      if(d > active2) {
        return(0);
      }
    }

    spd_calcspline(spline,1,1,&dsx,&dsy,&dx,&dy);
    x = dsx + dx + .5; y = dsy + dy + .5;
#endif /* WASTE4_FORSPEED */
/*
    prmsg(DMSG,("Invalid pixel: [%d][%d] (%.1f) -> [%d][%d] (%.1f)\n",
      sx,sy,dummy,x,y,Dummy));
*/
    xmin = x - maxxpixel; if (xmin < 0) xmin = 0;
    xmax = XSIZE - 1;
    ymin = y - maxypixel; if (ymin < 0) ymin = 0;
    ymax = YSIZE - 1;
    for(i = xmin; i <= xmax; i++)
      for(j = ymin * XSIZE; j <= ymax * XSIZE; j += XSIZE)
	trg_im[i + j] = Dummy;
  }
  return(0);
} /* mark_overflow */

/*==============================================================================
 * Scans the input image for pixels with illegal values and marks the
 * corresponding pixels in the output image.
 *
 * The input image can be a source, dark current, scattering background, mask or
 * floodfield image (type = SRCTYP, DRKTYP, SBKTYP, MSKTYP or FLOTYP).
 *
 * If the input image is empty (NULL pointer), the routine returns without any
 * action. This is not an error condition.
 *
 * Illegal values are:
 * - "dummy" (pixels marked as dummy);
 * - "IMAGE_OVER" (pixels marked as overflow);
 * - all values smaller than "INP_MIN";
 * - all values greater than "INP_MAX";
 * - all values equal to zero ("0.").
 *
 * The illegal values "IMAGE_OVER", "INP_MIN" and "INP_MAX" are tested only for
 * source and dark current images.
 *
 * Values equal to zero are tested only for floodfield images.
 *
 * "dummy" is the value of the "Dummy" keyword in the image header of the input
 * image, if this is defined, otherwise it is 0.
 *
 * "IMAGE_OVER" is the value of the command line argument "overflow". The
 * default value is 0., meaning that there is no "IMAGE_OVER" value set.
 *
 * "INP_MIN" is the value of the command line argument "inp_min". The default
 * value is 0., meaning that there is no "INP_MIN" value set.
 *
 * "INP_MAX" is the value of the command line argument "inp_max". The default
 * value is 0., meaning that there is no "INP_MAX" value set.
 *
 * The value used for marking these pixels is "Dummy". This value can be defined
 * with the command line argument "dummy", otherwise the value of the "Dummy"
 * keyword in the source image header is taken. The default (if neither is
 * specified) is 0.
 *
 * If the exponentiation constant INPEXP is set, the various limits (overflow,
 * minimum, maximum) have to be scaled accordingly.
 *
 * Input : src_im: buffer with the input image (float)
 *         type:   type of input image
 * Output: trg_im: output image buffer with overflow pixels marked (float)
 * Return: 0  always
 */

int mark_overflow_nocorr(float *src_im,float *trg_im,long *dumlst,int type)
{
  float savepix;
  float *lastpix = src_im + XSIZE * YSIZE - 1;
  register float *srcptr;
  register int idx;
  float image_over = IMAGE_OVER;
  float inp_max = INP_MAX, inp_min = INP_MIN;
  float dimage_over,dummy,ddummy;

  if(src_im == NULL)
    return(0);

  /*
   * Write the (possibly new) "Dummy" value into the header for the corrected
   * output image.
   */
  img_head[CORTYP].Dummy = Dummy;
  img_head[CORTYP].init |= FL_DUMMY;

  /*
   * Define the "dummy" and "ddummy" values.
   */
  if(img_head[type].init & FL_DUMMY)
    dummy = img_head[type].Dummy;
  else
    dummy = 0.;
  if(img_head[type].init & FL_DDUMM)
    ddummy = img_head[type].DDummy;
  else
    ddummy = DDSET(dummy);

  /*
   * For the floodfield image, a list of the pixels that have an invalid value
   * has been created in "prepare_flood()". Just mark the corresponding pixels
   * in the output image as "invalid" as well.
   */
  if(type == FLOTYP) {
    if(dumlst != NULL)
    {
      register long *pdumlst;
      for(pdumlst = dumlst; *pdumlst != -1; pdumlst++)
        *(trg_im + *pdumlst) = Dummy;
    }
    return(0);
  }

  /*
   * Save the value of the last pixel, then set the last pixel to "dummy". 
   *
   * Now test the image for the illegal value "dummy" (unless "dummy" is 0.,
   * which indicates that it is not to be used).
   *
   * As the last pixel has been set to "dummy", the loop will terminate there.
   * This eliminates the need to test for the end of the image at each iteration
   * of the loop.
   */
  savepix = *lastpix;

  if(DUMMYDEFINED(dummy,ddummy)) {
    *lastpix = dummy;
    srcptr = src_im;
  
    for(;;) {
      if(DUMMY(*srcptr,dummy,ddummy))
        if(srcptr == lastpix)
	  break;
        else {
	  idx = srcptr - src_im;
	  *(trg_im + idx) = Dummy;
        }
      srcptr++;
    }
  }

  /*
   * The other tests are done only for source and dark current data.
   *
   * Scale the various limits (overflow, minimum, maximum) with the
   * exponentiation constant INPEXP if it is set.
   */
  if(type == SRCTYP || type == DRKTYP) {
    if(INPEXP != 1.) {
      image_over = pow((double)image_over,INPEXP);
      inp_max = pow((double)inp_max,INPEXP);
      inp_min = pow((double)inp_min,INPEXP);
    }

    /*
     * If "IMAGE_OVER" is set, test the image for pixels with this value.
     *
     * For terminating the loop the same trick as above is used.
     */
    dimage_over = DDSET(image_over);
    if(image_over != 0.) {
      *lastpix = image_over;
      srcptr = src_im;
    
      for(;;) {
        if(DUMMY(*srcptr,image_over,dimage_over))
          if(srcptr == lastpix)
  	  break;
          else {
  	  idx = srcptr - src_im;
	  *(trg_im + idx) = Dummy;
          }
        srcptr++;
      }
    }
  
    /*
     * If "INP_MIN" is set, test the image for pixels with a smaller value.
     *
     * For terminating the loop the same trick as above is used.
     */
    if(inp_min != 0.) {
      *lastpix = inp_min - 0.1 * fabs((double)inp_min);
      srcptr = src_im;
    
      for(;;) {
        if(*srcptr < inp_min)
          if(srcptr == lastpix)
  	  break;
          else {
  	  idx = srcptr - src_im;
	  *(trg_im + idx) = Dummy;
          }
        srcptr++;
      }
    }
  
    /*
     * If "INP_MAX" is set, test the image for pixels with a greater value.
     *
     * For terminating the loop the same trick as above is used.
     */
    if(inp_max != 0.) {
      *lastpix = inp_max + 0.1 * fabs((double)inp_max);
      srcptr = src_im;
    
      for(;;) {
        if(*srcptr > inp_max)
          if(srcptr == lastpix)
  	  break;
          else {
  	  idx = srcptr - src_im;
	  *(trg_im + idx) = Dummy;
          }
        srcptr++;
      }
    }
  }

  /*
   * Restore the value of the last pixel and test it.
   */
  *lastpix = savepix;

  if(DUMMY(*lastpix,dummy,ddummy) || (type == SRCTYP || type == DRKTYP) &&
    (DUMMY(*lastpix,image_over,dimage_over) ||
    (inp_min != 0. && *lastpix < inp_min) ||
    (inp_max != 0. && *lastpix > inp_max))) {
    idx = srcptr - src_im;
    *(trg_im + idx) = Dummy;
  }
  return(0);
} /* mark_overflow_nocorr */

/*==============================================================================
 * Map the geometry of the input image to the required form. This includes
 * binning in horizontal and vertical direction and calculation of the offset
 * and center coordinate values.
   *
   * The linearity correction is done first. For this, all values of the source
   * image are exponentiated with a constant.
 *
 * Input : inbuff: buffer with the input image
 *         bin_1 : binning factor in x-direction
 *         bin_2 : binning factor in y-direction
 *         type  : type of image
 * Output: outbuff: buffer with the mapped output image
 * Return:  0  if no errors
 *         -1  else
 */

int map_imag(void *inbuff,void **outbuff,double bin_1,double bin_2,int type)
{
  int i_1,i_2,iDim_1,iDim_2,oDim_1,oDim_2;
  unsigned long dispinit;
  float WReal1_1,WReal3_1,WReal1_2,WReal3_2,f1_1,f3_1,f1_2,f3_2;
  float idummy,iddummy,iOffset_1,iOffset_2,iBSize_1,iBSize_2,iPSize_1,iPSize_2,
    iCenter_1,iCenter_2,iPreCenter_1,iPreCenter_2;
  float odummy,oddummy,oOffset_1,oOffset_2,oBSize_1,oBSize_2,oPSize_1,oPSize_2,
    oCenter_1,oCenter_2,oPreCenter_1,oPreCenter_2;
  float iOff_1,iOff_2,iPs_1,iPs_2,oOff_1,oOff_2,oPs_1,oPs_2;
  float value,sum,weight,*pdata,*data;
  double oB_iB_1,oB_iB_2;
  struct data_head *pimghead;

  float none=0.; /* just a dummy */

  pimghead = &img_head[type];
  dispinit = pimghead->Dspinit;
  iDim_1 = pimghead->Dim_1;
  iDim_2 = pimghead->Dim_2;
  iOffset_1 = pimghead->Offset_1;
  iOffset_2 = pimghead->Offset_2;
  iBSize_1 = pimghead->BSize_1;
  iBSize_2 = pimghead->BSize_2;
  iPSize_1 = pimghead->PSize_1;
  iPSize_2 = pimghead->PSize_2;
  iCenter_1 = pimghead->Center_1;
  iCenter_2 = pimghead->Center_2;
  idummy = pimghead->Dummy;
  iddummy = pimghead->DDummy;
  odummy = idummy;
  oddummy = iddummy;

  iPreCenter_1 = pimghead->PreCenter_1;
  iPreCenter_2 = pimghead->PreCenter_2;

  if(type == SRCTYP || type == DRKTYP) {
    if(bin_1 <= 0. || bin_2 <= 0.) {
      prmsg(ERROR,("%s image: bin size <= 0.: x = %f, y = %f\n",typestr[type],
        bin_1,bin_2));
      return(-1);
    } else {
      /*
       * Apply the linearity correction to the image. The new pixel values are:
       *
       *   pix_new = pix_old ^ inp_exp
       */
      expon_im(inbuff,inbuff,INPEXP,iDim_1 * iDim_2);
      if(bin_1 == 1. && bin_2 == 1.) {
	*outbuff = inbuff;
	return(0);
      } else
	prmsg(DMSG,("mapping %s image: x-bin = %f, y-bin = %f\n",typestr[type],
        bin_1,bin_2));
    }

    oDim_1 = iDim_1 / bin_1;
    oDim_2 = iDim_2 / bin_2;
    oOffset_1 = iOffset_1;
    oOffset_2 = iOffset_2;
    oBSize_1 = iBSize_1;
    oBSize_2 = iBSize_2;
    oPSize_1 = iPSize_1;
    oPSize_2 = iPSize_2;
    oCenter_1 = iCenter_1;
    oCenter_2 = iCenter_2;
    AREBIN(oOffset_1,oBSize_1,oPSize_1,oCenter_1,bin_1);
    AREBIN(oOffset_2,oBSize_2,oPSize_2,oCenter_2,bin_2);

    oPreCenter_1 = iPreCenter_1;
    oPreCenter_2 = iPreCenter_2;
    AREBIN(none,none,none,oPreCenter_1,bin_1);
    AREBIN(none,none,none,oPreCenter_2,bin_2);
  } else {
    /* REM PB: AREBIN is designed for integer rebinning and
               can only be used for bin>=1. Therefore, it is
               not used here */
    oDim_1 = img_head[SRCTYP].Dim_1;
    oDim_2 = img_head[SRCTYP].Dim_2;
    oOffset_1 = img_head[SRCTYP].Offset_1;
    oOffset_2 = img_head[SRCTYP].Offset_2;
    oBSize_1 = img_head[SRCTYP].BSize_1;
    oBSize_2 = img_head[SRCTYP].BSize_2;
    oB_iB_1 = oBSize_1 / iBSize_1;
    oB_iB_2 = oBSize_2 / iBSize_2;
    oPSize_1 = iPSize_1 * oB_iB_1;
    oPSize_2 = iPSize_2 * oB_iB_2;
    oCenter_1 = iCenter_1 / oB_iB_1;
    oCenter_2 = iCenter_2 / oB_iB_2;
    oPreCenter_1 = iPreCenter_1 / oB_iB_1;
    oPreCenter_2 = iPreCenter_2 / oB_iB_2;

    if(type == SDXTYP || type == SDYTYP) {
      if(dispinit & FL_OFFS1)
        pimghead->DspOffset_1 /= oB_iB_1;
      if(dispinit & FL_OFFS2)
        pimghead->DspOffset_2 /= oB_iB_2;
      if(dispinit & FL_BSIZ1)
        pimghead->DspBSize_1 *= oB_iB_1;
      if(dispinit & FL_BSIZ2)
        pimghead->DspBSize_2 *= oB_iB_2;
      if(dispinit & FL_PSIZ1)
        pimghead->DspPSize_1 *= oB_iB_1;
      if(dispinit & FL_PSIZ2)
        pimghead->DspPSize_2 *= oB_iB_2;
      if(dispinit & FL_CENT1)
        pimghead->DspCenter_1 /= oB_iB_1;
      if(dispinit & FL_CENT2)
        pimghead->DspCenter_2 /= oB_iB_2;
      if(dispinit & FL_PRECEN1)
        pimghead->DspPreCenter_1 /= oB_iB_1;
      if(dispinit & FL_PRECEN2)
        pimghead->DspPreCenter_2 /= oB_iB_2;
      /*
       * For displacement files, the dimensions have to be one bigger than those
       * of the source image, to store the displacement values of the right and
       * upper edge of the image.
       *
       * Also, the displacement files thus have an artificial offset of -0.5.
       */
      oDim_1++;
      oDim_2++;
      oOffset_1 -= 0.5;
      oOffset_2 -= 0.5;
    }
  }

  if((data = (float *)pmalloc(oDim_1 * oDim_2 * sizeof(float))) == NULL) {
    prmsg(ERROR,("no memory for mapped %s image in map_imag\n",type));
    return(-1);
  }

  if(type == SRCTYP || type == DRKTYP) {
    int tmp1,tmp2;
    tmp1 = iDim_1;
    tmp2 = iDim_2;
    IpolRebin2(inbuff,iDim_1,iDim_2,data,&tmp1,&tmp2,idummy,iddummy,bin_1,
      bin_2,0);
  } else {
    REALREF(iOff_1,iPs_1,iOffset_1,iBSize_1);
    REALREF(iOff_2,iPs_2,iOffset_2,iBSize_2);
    REALREF(oOff_1,oPs_1,oOffset_1,oBSize_1);
    REALREF(oOff_2,oPs_2,oOffset_2,oBSize_2);
  
    for(i_1 = 0; i_1 < oDim_1; i_1++) {
      WReal1_1 = WORLD(LOWERBORDER + i_1,oOff_1,oPs_1);
      WReal3_1 = WORLD(LOWERBORDER + i_1 + 1,oOff_1,oPs_1);
      f1_1 = INDEX(WReal1_1,iOff_1,iPs_1);
      f3_1 = INDEX(WReal3_1,iOff_1,iPs_1);
  
      for(i_2 = 0; i_2 < oDim_2; i_2++) {
        WReal1_2 = WORLD(LOWERBORDER + i_2,oOff_2,oPs_2);
        WReal3_2 = WORLD(LOWERBORDER + i_2 + 1,oOff_2,oPs_2);
        f1_2 = INDEX(WReal1_2,iOff_2,iPs_2);
        f3_2 = INDEX(WReal3_2,iOff_2,iPs_2);
  
        Isum2ldw(inbuff,iDim_1,iDim_2,idummy,iddummy,f1_1,f1_2,f3_1,f3_2,&sum,
          &weight);
          value = weight == 0. ? odummy : sum / weight;
        pdata = ABSPTR(data,oDim_1,oDim_2,i_1,i_2);
        *pdata = value;
      }
    }
  }

  pimghead->Dim_1 = oDim_1;
  pimghead->Dim_2 = oDim_2;
  pimghead->Offset_1 = oOffset_1;
  pimghead->Offset_2 = oOffset_2;
  pimghead->BSize_1 = oBSize_1;
  pimghead->BSize_2 = oBSize_2;
  pimghead->PSize_1 = oPSize_1;
  pimghead->PSize_2 = oPSize_2;
  pimghead->Center_1 = oCenter_1;
  pimghead->Center_2 = oCenter_2;
  pimghead->Dummy = odummy;
  pimghead->DDummy = oddummy;

  pimghead->PreCenter_1 = oPreCenter_1;
  pimghead->PreCenter_2 = oPreCenter_2;

  /*
   * Divide the displacement files by the mapping factor (as the image sizes are
   * now different by "mapping factor", the displacements have to be scaled
   * correspondingly as well).
   */
  if(type == SRCTYP)
    set_headval(img_head[type],SRCTYP);
  else if(type == SDXTYP && iBSize_1 != oBSize_1)
    scale_im(data,data,iBSize_1 / oBSize_1,0.,oDim_1 * oDim_2);
  else if(type == SDYTYP && iBSize_2 != oBSize_2)
    scale_im(data,data,iBSize_2 / oBSize_2,0.,oDim_1 * oDim_2);

  *outbuff = data;

  return(0);
} /* map_imag */

/*==============================================================================
 * Name
 * interval_compare
 *
 * Synopis
 * int interval_compare( float R1, float R3, float S1, float S3 )
 *
 * Arguments
 * [R1, R3]: coordinate interval of correction image (e.g. interval covered
 *           by spatial distortion correction, flatfield, background),
 * [S1, S3]: coordinate interval of source image (must be smaller or equal)
 *
 * Return value
 *  0: OK, interval R fully covers interval S
 * -1: BAD, interval R does not fully cover interval S
 *     (some pixels in S cannot be corrected and must be replaced with dummies)
 */

int interval_compare(float R1,float R3,float S1,float S3)
{
  float r1, r3, s1, s3;
  int value;

  // order coordinates
  if(R1<=R3) { r1 = R1; r3 = R3; } else { r1 = R3; r3 = R1; }
  if(S1<=S3) { s1 = S1; s3 = S3; } else { s1 = S3; s3 = S1; }

  // [r1,r3] must cover [s1,s3]
  if((r1<=s1)&&(s3<=r3)) value=0; else value = -1;
  
  return(value);
} /* interval_compare */

/*==============================================================================
 * Name
 * region_compare
 *
 * Description
 * The return value is only 0 if the region coordinates of all pixels in
 * image R ("region R") fully cover the region coordinates of all pixels in
 * image S ("region S"). Image S is the image that should be corrected, i.e.
 * the source image.
 *
 * The test in the calling program should look like
 *
 * ...
 * if ( region_compare( ROffset_1, RBSize_1, RDim_1,
 *                       ... ) ) { printf("WARNING: ...\n"); }
 * ...
 *
 * This check should be applied to all correction images, whether they are
 * mapped or not.
 *
 * This check should also be applied to the distortion images that have been 
 * calculated by a spatial distortion spline. For undefined Offset (0.0) and 
 * BSize (1.0) values default values (in parentheses) should be used. 
 *
 * Synopis
 * #include <reference.h>
 * int region_compare(int type, float ROffset_1, float RBSize_1, float RDim_1,
 *                              float ROffset_2, float RBSize_2, float RDim_2,
 *                              float SOffset_1, float SBSize_1, float SDim_1,
 *                              float SOffset_2, float SBSize_2, float SDim_2);
 *
 * Arguments
 *
 * int type: type of correction image
 *
 * float ROffset_1, float RBRize_1, float RDim_1,
 * float ROffset_2, float RBRize_2, float RDim_2: 
 *    Offset, BSize and Dim of correction image (e.g. distortion images (either
 *       calculated from spatial distortion spline or read from distortion
 *       file), flatfield, background)
 *
 * float SOffset_1, float SBSize_1, float SDim_1,
 * float SOffset_2, float SBSize_2, float SDim_2: 
 *    Offset, BSize and Dim of source image
 *
 * [R1, R3]: region of correction image
 * [S1, S3]: region of source image
 *
 * Return value
 *  0: OK, region R fully covers region S
 * -1: BAD, region R does not fully cover region S
 *     (some pixels cannot be corrected and will be replaced with dummies)
 */

int region_compare(int type,float ROffset_1,float RBSize_1,float RDim_1,
                            float ROffset_2,float RBSize_2,float RDim_2,
                            float SOffset_1,float SBSize_1,float SDim_1,
                            float SOffset_2,float SBSize_2,float SDim_2)
{
  float R1_1,R1_2,R3_1,R3_2,S1_1,S1_2,S3_1,S3_2;
  float value=-1;

  /* correction image (to be tested) */
  R1_1=INDEX2R(LOWERBORDER,ROffset_1,RBSize_1);
  R3_1=INDEX2R(LOWERBORDER+RDim_1,ROffset_1,RBSize_1);

  R1_2=INDEX2R(LOWERBORDER,ROffset_2,RBSize_2);
  R3_2=INDEX2R(LOWERBORDER+RDim_2,ROffset_2,RBSize_2);

  /* input image */
  S1_1=INDEX2R(LOWERBORDER,SOffset_1,SBSize_1);
  S3_1=INDEX2R(LOWERBORDER+SDim_1,SOffset_1,SBSize_1);

  S1_2=INDEX2R(LOWERBORDER,SOffset_2,SBSize_2);
  S3_2=INDEX2R(LOWERBORDER+SDim_2,SOffset_2,SBSize_2);

  if(!(interval_compare(R1_1,R3_1,S1_1,S3_1) ||
    interval_compare(R1_2,R3_2,S1_2,S3_2)))
    value=0;
  else
  {
    prmsg(ERROR,("inconsistent coordinate ranges\n"));
    prmsg(MSG,
      ("%s region coord. 1: [%6.1f, %6.1f], coord. 2: [%6.1f, %6.1f]\n",
      *(typestr + type),R1_1,R3_1,R1_2,R3_2));
    prmsg(MSG,
      ("source region coord. 1: [%6.1f, %6.1f], coord. 2: [%6.1f, %6.1f]\n",
      S1_1,S3_1,S1_2,S3_2));
  }

  return(value);

} /* region_compare */

/*==============================================================================
 * Integrate and average the input image azimuthally.
 *
 * The values for this calculation are taken from the data header parameters'
 * structure "img_head" for the corrected image (type == CORTYP). If this
 * structure does not contain all needed values, the routine returns with an 
 * error.
 *
 * Input : I1Data  : buffer with the input image (float)
 *         r0      : minimum radius for azimuthal integration (in meter)
 *         rstep   : dimension of the output array in radial direction
 *         a0      : start angle for azimuthal integration (in radian)
 *         da      : angular interval for azimuthal integration (in radian)
 *         astep   : dimension of the output array in angular direction
 *         apro    : Projection type of azimuthally regrouped image if 
 *                   different from 0 (IO_NoPro).
 *         avesfac : scale factor for "s" values of the averaged image
 *         verbose : verbose level
 * Output: I0Data : buffer with the azimuthal integrated image
 *         avedata: buffer with the averaged image
 * Return:  0  if no errors
 *         -1  if header parameter structure does not contain all needed values
 *         -2  if illegal parameters for integration are given in the input
 */

int azim_int(float *I1Data,float *I0Data,float *avedata,float r0,int rstep,
  float a0,float da,int astep,int apro, float avesfac, int verbose)
{
  int i_1,I0Dim_1,i_2,I0Dim_2,I1Dim_1,I1Dim_2;

  unsigned long reqflg;

  float *pI0Data, *pavedata = avedata;
  float *pavevardata=NULL; //avedata and avevardata have the same size
  float I0Offset_1,I0Offset_2,I1Offset_1,I1Offset_2;
  float I0PSize_1,I0PSize_2,I1PSize_1,I1PSize_2;
  float I0Center_1,I0Center_2,I1Center_1,I1Center_2;
  float I1DetRot1, I1DetRot2, I1DetRot3;
  float I0Dummy,I0DDummy,I1Dummy,I1DDummy;
  double samdis,wavlen=WaveLength0;

  int I1Pro, I0Pro;

  float Angle0, Angle1;
  float AngleMin, AngleMax;

  float dr;

  float *E0Data=NULL,*pE0Data;
  float *E1Data=NULL;

  int vsum=0, ave=1; // no sum, calculate azimuthal average
  int status;

  reqflg = FL_PSIZ1 | FL_PSIZ2 | FL_OFFS1 | FL_OFFS2 | FL_CENT1 | FL_CENT2 |
    FL_SAMDS | FL_WAVLN | FL_PRO;
  if((img_head[CORTYP].init & reqflg) != reqflg) {
    prmsg(ERROR,
      ("header parameters incomplete - cannot integrate over azimuth\n"));
    return(-1);
  }

  if(r0 < 0. || rstep <= 0 || da <= 0. || astep <= 0) {
    prmsg(ERROR,("wrong integration parameters r0 = %f, rstep = %d\n",r0,
      rstep));
    prmsg(ERROR,("                             da = %f, astep = %d\n",da,
      astep));
    return(-2);
  }

  bench(NULL);
  img_head[AZITYP] = img_head[CORTYP];

   /*
    * Variables starting with "I1" are for the input, those starting with "I0"
    * for the output image.
    */
  I1Dim_1 = XSIZE;
  I1Dim_2 = YSIZE;
  I1Offset_1 = img_head[CORTYP].Offset_1;
  I1Offset_2 = img_head[CORTYP].Offset_2;
  I1PSize_1 = img_head[CORTYP].PSize_1;
  I1PSize_2 = img_head[CORTYP].PSize_2;
  I1Center_1 = img_head[CORTYP].Center_1;
  I1Center_2 = img_head[CORTYP].Center_2;
  I1Dummy = img_head[CORTYP].Dummy;
  I1DDummy = img_head[CORTYP].DDummy;
  samdis = img_head[CORTYP].SamplDis;
  wavlen = img_head[CORTYP].WaveLeng;
  I1DetRot1 = img_head[CORTYP].DetRot_1;
  I1DetRot2 = img_head[CORTYP].DetRot_2;
  I1DetRot3 = img_head[CORTYP].DetRot_3;

  if(strlib_ncasecmp("Saxs",img_head[CORTYP].ProjTyp,4) == 0)
    I1Pro = IO_ProSaxs;
  else { 
    I1Pro = IO_ProWaxs;
    strcpy(img_head[AZITYP].ProjTyp,"Waxs");
  }

  dr = PSIZE2N(MIN2(fabs(I1PSize_1),fabs(I1PSize_2)));
  I0Dim_1 = rstep;
  I0Dim_2 = astep;
  img_head[AZITYP].PSize_1 = I0PSize_1 = R2PSIZE(dr);
  img_head[AZITYP].PSize_2 = I0PSize_2 = R2PSIZE(da);
  img_head[AZITYP].BSize_1 = 1.;
  img_head[AZITYP].BSize_2 = 1.;
  img_head[AZITYP].Offset_1 = I0Offset_1 = 0.;
  img_head[AZITYP].Offset_2 = I0Offset_2 = 0.;
  img_head[AZITYP].Center_1 = I0Center_1 = -R2CENTER(r0,I0PSize_1);
  img_head[AZITYP].Center_2 = I0Center_2 = -R2CENTER(N2PSIZE(a0),I0PSize_2);
  img_head[AZITYP].Dummy = I0Dummy = I1Dummy;
  img_head[AZITYP].DDummy = I0DDummy = DDSET(I0Dummy);

  /* 
   * Use input projection I1Pro as default for projection I0Pro of
   * azimuthally regrouped image (modification with command line
   * option azim_pro)
   */
  if (apro) I0Pro = apro;
  else I0Pro = I1Pro;

  strcpy(img_head[AZITYP].ProjTyp,(I0Pro==IO_ProSaxs)?"Saxs":"Waxs");

  img_head[AVETYP] = img_head[AZITYP];

  // preset output arrays with dummies
  pI0Data=I0Data;
  for(i_2=0; i_2 < I0Dim_2; i_2++)
    for(i_1=0; i_1 < I0Dim_1; i_1++)  *(pI0Data++)=I0Dummy;

  if (E0Data) {
    pE0Data=E0Data;
    for(i_2=0; i_2 < I0Dim_2; i_2++)
      for(i_1=0; i_1 < I0Dim_1; i_1++)  *(pE0Data++)=VarDummy;
  }

  // azimuthal regrouping
  /*
   * angular reference system is always Normal
   */
   Angle0=INDEX2N(LOWERBORDER,I0Offset_2,I0PSize_2,I0Center_2);
   Angle1=INDEX2N(LOWERBORDER+I0Dim_2,I0Offset_2,I0PSize_2,I0Center_2);

   AngleMin = MIN2(Angle0,Angle1);
   AngleMax = MAX2(Angle0,Angle1);

  /*
   * angular regrouping, use radial reference system Saxs to regroup image
   */
   ang_sum ( IO_Saxs,
             I0Data,   E0Data,
             I0Dim_1,  I0Dim_2,
             I0Offset_1, I0PSize_1, I0Center_1,
             I0Offset_2, I0PSize_2, I0Center_2,
             samdis, wavlen,
             I0Pro,
             I0Dummy,  I0DDummy,
             I1Data,   E1Data,
             I1Dim_1,  I1Dim_2,
             I1Offset_1, I1PSize_1, I1Center_1,
             I1Offset_2, I1PSize_2, I1Center_2,
             samdis, wavlen,
             I1DetRot1,  I1DetRot2, I1DetRot3,
             I1Pro,
             I1Dummy,    I1DDummy,
             AngleMin,   AngleMax,
             0.0,    0.0,
             vsum, ave, (verbose>=2)?1:0, &status ); // print debug info

  // azimuthal averaging
    // 1st row labels (modulus of scattering vector)
  for(i_1=0; i_1 < I0Dim_1; i_1++) {
    /*
     * "s" value calculation depends on projection type I0Pro:
     * - "Saxs"  normal image
     * - "Waxs"  image that has been projected to the Ewald sphere
     */
    if(I0Pro == IO_ProSaxs)
      *pavedata = 2. * sin(0.5 * atan2((double)INDEX2N(i_1,I0Offset_1,
        I0PSize_1,I0Center_1),samdis)) * WAVENUMBER(wavlen) * avesfac;
    else
      *pavedata = INDEX2S(i_1,I0Offset_1,I0PSize_1,I0Center_1,samdis,wavlen) *
        avesfac;
    pavedata++;
  }
    // 2nd row data (angular average)

  /* angular averaging */
  project_1    ( pavedata, pavevardata, I0Dim_1,
                 0, I0Dim_1-1, I0Dummy, 1.0,
                 I0Data, E0Data, I0Dim_1, I0Dim_2,
                 A2INDEX(0.0),A2INDEX(I0Dim_1),1.0,
                 A2INDEX(0.0),A2INDEX(I0Dim_2),
                 I0Dummy, I0DDummy, ave );

  // next row
  pavedata += I0Dim_1;
  pavevardata += I0Dim_1;

  bench("azimuthal regrouping");
  return(0);

} /* azim_int */

/*==============================================================================
 * Performs the distortion correction on the input image.
 *
 * Determines for each pixel in the output image which pixels in the input
 * image contribute to it, and which fraction of the area of each of those
 * input pixels is mapped on the output pixel being processed. It then uses
 * these fractions to calculate the pixel value of the output pixel: for each
 * contributing input pixel, it multiplies the pixel's value with the
 * corresponding fraction, and sums these values over all contributing input
 * pixels.
 *
 * Input : cor_im: (empty) buffer for the corrected input pixel values
 *         src_im: buffer with the input image
 *         lut_d:  structure with the look-up-table for the distortion
 *                 corrections
 * Output: cor_im: buffer with the corrected pixel values for the input image
 * Return:  0  if no errors
 */

int undistort_im(float *cor_im,float *src_im,struct lut_descript *lut_d)
{
  /*
   * starttidx  contains the number of empty pixels at the beginning of the
   *            target image, i.e. it is the index of the first non-empty pixel
   *            in the target image array.
   * startsidx  is the index of the corresponding pixel in the source image
   *            array
   */
  unsigned long startsidx = lut_d->startsidx;
  unsigned long starttidx = lut_d->starttidx;
  unsigned char *prog = lut_d->prog;
  LUT_TYPE *lut = lut_d->lut;
  unsigned short *asrc = lut_d->abs_src;
  unsigned char *prog_ptr = prog;
  LUT_TYPE *lut_ptr = lut;
  int multi;

  int i = 0;
  register int *rel_tab,**relend_tab;
  register int *offset_tab;
  register int *r_ptr;
  register int *relend;
  register float *src_p;
  register float *cor_p;
  unsigned short *abs_ptr = asrc;
  register float add = 0.;
  register float fullscale = FULLSCALE;
  register float dummy = Dummy;
  register unsigned char instruct;
  offset_tab = lut_d->offset_tab;
  rel_tab = lut_d->rel_tab;
  relend_tab = lut_d->relend_tab;

  /*
   * Determine the starting point for processing in the source and target
   * images, skipping over all empty pixels at the beginning of the images.
   *
   * Set the value of these empty pixels in the target image to Dummy.
   */
  src_p = src_im + startsidx;

  cor_p = cor_im;
  for(i=0; i<starttidx; i++)
    *cor_p++ = dummy;

  /*
   * Endless loop through the instructions of the look-up-table program.
   * The loop is terminated by a special instruction PROGEND.
   */
  for(;;) {
    /*
     * Get the next instruction.
     *
     * If it is != 0, then we have to process the "normal" case: a predefined
     * sequence of source pixels, starting with a pixel that is "near" the
     * source pixel that was processed last (i.e., it is in one of the 256
     * positions around the last one).
     *
     * If it is 0, then we have to treat an exception - see below.
     */
    instruct = *prog_ptr++;
    if(instruct != 0) {
      /*
       * Use the offset table to get the address of the first source image
       * pixel to be processed, and calculate that part of the pixel value
       * that goes into the corresponding target pixel. This percentage is
       * contained in the array lut_ptr.
       */
      src_p += offset_tab[instruct];
      add = (*lut_ptr++ * *src_p);

      /*
       * There is usually more than one source image pixel that will contribute
       * to the target pixel being processed. Loop over all these source image
       * pixels, get their addresses (always relative to the previous pixel -
       * see description of rel_tab in lut_calc()) and add their contribution
       * to the value already calculated for the first source pixel above.
       *
       * This loop ends when the sequence end pointer (from relend_tab) has
       * been reached.
       */
      instruct = *prog_ptr++;

      relend = relend_tab[instruct];
      for(r_ptr = rel_tab + (instruct << RELTABSH); r_ptr < relend; r_ptr++) {
	src_p += *r_ptr;
	add += (*lut_ptr++ * *src_p);
      }
    } else {
      /*
       * The program instruction is 0: we have to treat an exception. Get the
       * next instruction and see what kind of exception it is.
       */
      instruct = *prog_ptr++;
      if(instruct & INCTARGET) {
        /*
         * Exception INCTARGET: there is one or several (if the MULTIINC flag
         * is set as well) target pixels that will not receive a contribution
         * from any source pixel. Just set the target pixel values to Dummy.
         */
	if(instruct & MULTIINC) {
	  multi  = *prog_ptr++;
	  multi += *prog_ptr++ * 256;
	  for(i = multi; i; i--)
	    *cor_p++ = dummy;
	} else
	  *cor_p++ = dummy;
	continue;
      }
      if(instruct & ABSSRC) {
        /*
         * Exception ABSSRC: the next source pixel is not in one of the 256
         * positions around the present one and therefore its address cannot be
         * expressed as an offset relative to the present source pixel. Get the
         * absolute x and y coordinates of the pixel from the array abs_ptr,
         * obtain the absolute address using the coordinates, and then
         * calculate its contribution to the target pixel value.
         */
	int x = *abs_ptr++;
	int y = *abs_ptr++;
	src_p = src_im + (x + y * XSIZE);
	add = *abs_ptr++ * *src_p;

        /*
         * The following pixels can either be a predefined sequence of move-
         * ments as above, or it is a sequence where each subsequent source
         * pixel coordinate is obtained from its own look-up-table program
         * instruction (case UNCOMPRESSED).
         *
         * For more details on the first case, see above; for more details on
         * the second case, see below under "Exception UNCOMPRESSED".
         */
	if(instruct & UNCOMPRESSED) {
	  while(instruct = *prog_ptr++) {
	    src_p += offset_tab[instruct];
	    add += (*lut_ptr++ * *src_p);
	  }
	} else {
	  instruct = *prog_ptr++;
	  for(r_ptr = rel_tab + (instruct << RELTABSH);
	       r_ptr < relend_tab[instruct]; r_ptr++) {
	    src_p += *r_ptr;
	    add += (*lut_ptr++ * *src_p);
	  }
	}
      } else if(instruct & UNCOMPRESSED) {
        /*
         * Exception UNCOMPRESSED: the source pixels contributing to this target
         * pixel could not be expressed as a predefined sequence of movements
         * relative to the first source pixel. Therefore for each source pixel
         * there is an instruction in the look-up-table program that gives the
         * corresponding index for the offset table, and this offset is used to
         * calculate the source pixel address. Then the contributions to the
         * target pixel are calculated and summed as before. The loop over the
         * contributing source pixels ends when there is a 0 instruction in the
         * look-up-table program.
         */
	add = 0.;
	while(instruct = *prog_ptr++) {
	  src_p += offset_tab[instruct];
	  add += (*lut_ptr++ * *src_p);
	}
      } else if(instruct & PROGEND)
        /*
         * Exception PROGEND: terminate the loop over the program instructions.
         */
	break;
    }

    /*
     * The variable "add" contains the target image values multiplied by
     * FULLSCALE, as this is the way percentages are expressed in "lut_d->lut"
     * (default type "unsigned short").
     *
     * Now they have to be divided by this value to yield the correct image
     * values.
     *
     * The default value is: FULLSCALE = 0x800.
     */

     *cor_p++ = add / fullscale;
  }

  if ( NORM_PREROT && DO_PREROT && (M_COR != NULL) ) {
    /*
     * multiply cor_im with M_COR (prerotation normalization)
     */
    float *pcor_m=M_COR;
    float *pcor_im, *pend;

    prmsg(DMSG,("undistort_im: prerotation normalization\n"));
 
    pcor_im = cor_im;
    pend = pcor_im + (XSIZE * YSIZE);

    while (pcor_im<pend) {
      *pcor_im *= *pcor_m;
      pcor_im++; pcor_m++;
    }
  } else prmsg(DMSG,("undistort_im: no prerotation normalization\n"));

  if (DO_PREROT!=2)
    upd_headval( CORTYP, 0, 1 );
  else upd_headval( CORTYP, 0, 0 );

  return( 0 );

} /* undistort_im */

/*==============================================================================
 * This function is used as the "comparison function" for one of the qsort()
 * calls in lut_calc().
 *
 * It compares the second element in each of the two input fields and returns
 * -1, 0 or 1 if the one in the first field is greater than, equal to or
 * smaller than the one in the second field.
 *
 * This will cause the fields to be sorted in descending order.
 *
 * Input : el1, el2: pointers to the two fields to be compared
 * Output: none
 * Return: -1  if the first input field is greater than the second one
 *          0  if the first input field is equal to the second one
 *          1  if the first input field is smaller than the second one
 */

int histcompare_count(const void *el1,const void *el2)
{
  register unsigned int i1,i2;
  i1 = *((unsigned int*)el1 + 1);
  i2 = *((unsigned int*)el2 + 1);

  if(i1 < i2)
    return(1);
  else if(i1 != i2)
    return(-1);

  return(0);
} /* histcompare_count */

/*==============================================================================
 * This function is used as the "comparison function" for one of the qsort()
 * calls in lut_calc().
 *
 * It compares the first element in each of the two input fields and returns
 * -1, 0 or 1 if the one in the first field is smaller than, equal to or
 * greater than the one in the second field.
 *
 * This will cause the fields to be sorted in ascending order.
 *
 * Input : el1, el2: pointers to the two fields to be compared
 * Output: none
 * Return: -1  if the first input field is than smaller the second one
 *          0  if the first input field is equal to the second one
 *          1  if the first input field is greater than the second one
 */

int histcompare_idx(const void *el1,const void *el2)
{
  register unsigned int i1,i2;
  i1 = *(unsigned int*)(el1);
  i2 = *(unsigned int*)(el2);

  if(i1 < i2)
    return(-1);
  else if(i1 != i2)
    return(1);

  return(0);
} /* histcompare_idx */

/*==============================================================================
 * Calculates the variables and tables that will be used for the distortion
 * correction.
 *
 * The image received with a two-dimensional detector is often distorted (e.g.
 * through the use of an image intensifier). The consequence of this distortion
 * is that lines in a grid that were straight and crossing at right angles are
 * no longer straight and cross at angles different from 90 degrees in the
 * image received from the detector. Also, not only the shape but also the size
 * of the grid areas will be modified by this distortion.
 *
 * Before analyzing image data from such a detector for the scientific
 * information that they contain, it is therefore necessary to correct for this
 * distortion. The purpose of this correction is to reconstruct the original
 * undistorted image (also called "corrected image" or "target image") from
 * the distorted one (also called "source image").
 *
 * Both the target and the source image have the same number of pixels (e.g.
 * 1024 * 1024 for a given CCD camera), but because of the distortion the area
 * of a given pixel in the target image will usually correspond to parts of the
 * areas of several pixels in the source image.
 *
 * Once it is established (for all target pixels) which source pixels
 * contribute with which fraction of their area to a given target pixel, the
 * pixel values of the target pixels can be calculated:
 *
 *    target(i) = "sum over j" ( fraction(i,j) * source(j) )
 *
 * where  target(i)      is the value of the target pixel i,
 *        source(j)      is the value of the source pixel j,
 *        fraction(i,j)  is the area fraction of source pixel j that
 *                       contributes to target pixel i
 *
 * In principle, this sum is over all pixels in the source image, but in
 * reality for a given target pixel most of the "fraction(i,j)" values will
 * be 0. It is therefore better to sum only over the source pixels that have
 * a fraction different from 0. These will be called "contributing source
 * pixels".
 *
 * To do the distortion correction, two pieces of information are therefore
 * needed for each target pixel:
 * - a list of the contributing source pixels;
 * - a list with the fractional area contributions of each of these source
 *   pixels to the target pixel being processed.
 *
 * This routine calculates those two lists:
 * - the "look-up-table program" (table "prog" in the structure "lut_descript")
 *   contains the list of the contributing source pixels for each target pixel;
 * - the "look-up table" (table "lut" in the structure "lut_descript") contains
 *   for each target pixel the fractional contributions of each contributing
 *   source pixel.
 *
 * In principle, this is fairly simple and straightforward. For technical
 * reasons, the setup of these two lists is more complicated, and there are
 * additional lists used.
 *
 * The structure "lut_descript" is given below:
 *
 *    struct lut_descript {
 *      LUT_TYPE *lut;
 *      unsigned char *prog;
 *      int prog_length;
 *      unsigned int starttidx;
 *      unsigned int startsidx;
 *      int *offset_tab;
 *      int *rel_tab;
 *      int **relend_tab;
 *      unsigned short *abs_src;
 *      int maxxpixel;
 *      int maxypixel;
 *      short *xrel;
 *      short *yrel;
 *    }
 *
 * In order to make the program faster and also not to consume an excessive
 * amount of memory, the list of contributing source pixels in the
 * look-up-table program is given in several different ways depending on the
 * number and distribution of these source pixels:
 *
 * 1) the most general way to give the address of a source pixel is to specify
 *    its x and y coordinate. For this method of absolute addressing the
 *    additional source coordinate table "abs_src" in the structure
 *    "lut_descript" is used. For each source pixel contained in this table
 *    there are three entries at consecutive table addresses: the x coordinate
 *    (column number), the y coordinate (row number) and the area fraction that
 *    this source pixel contributes to the target pixel being processed.
 *
 *    From the row and column number, the pixel's index into the source image
 *    array can be calculated
 *
 *      index = "column number" + "row number" * "total number of columns"
 *
 * 2) this general addressing is, however, only needed at the beginning of a
 *    new target pixel (and even there not always). The distortion changes the
 *    shape and size of the pixels in the undistorted image, but it will not
 *    change the continuity of a pixel area. This means that all source pixels
 *    contributing to a given target pixel occupy a continuous area, i.e. every
 *    source pixel of this target pixel is adjacent to at least one other
 *    source pixel of the same target pixel. Once the first source pixel for
 *    a given target pixel has been found, the addresses of all subsequent
 *    source pixels can be given relative to the previous source pixel.
 *
 *    For this method of relative addressing the additional index offset table
 *    "offset_tab" in the structure "lut_descript" is used. It contains 256
 *    index offsets for the source image array relative to the index of the
 *    previously processed source pixel. The positions thus reached go from the
 *    "upper left" pixel at offset (-8 - 8 * XSIZE) to the "lower right" pixel
 *    at offset (7 + 7 * XSIZE).
 *
 *    In other words, offset_tab[0] points to the pixel that is 8 rows to the
 *    left and 8 lines above the previous pixel. The subsequent elements then
 *    move in this upper line to the right until offset_tab[15], which points
 *    to the pixel that is 7 rows to the right and (still) 8 lines above the
 *    previous pixel. offset_tab[16] then moves one line further down and back
 *    to the leftmost row: 8 rows to the left and 7 lines above the previous
 *    pixel. This continues like that, and offset_tab[255] finally points to
 *    the pixel that is 7 rows to the right and 7 rows below the previous
 *    pixel.
 *
 *    This relative addressing can also be used to specify the first source
 *    pixel of a new target pixel: if the index offset of this first source
 *    pixel relative to the index of the last source pixel of the previous
 *    target pixel fits the -8 to +7 condition for x and y described above,
 *    then the index of this new source pixel is specified this way with the
 *    offset table instead of using the source coordinate table.
 *
 * 3) the principle of relative addressing can be pushed even further. The
 *    addressing scheme as described above describes every source pixel
 *    individually by specifying its distance to the previous source pixel.
 *    This allows for a great variety in the pattern of the source pixels
 *    that belong to a given target pixel.
 *
 *    However, in many cases the image will not be strongly distorted. This
 *    means that the positions of the source pixels that contribute to a given
 *    target pixel will be near that pixel's position. Also, the correction
 *    function that maps the source pixel coordinates to the target pixel
 *    coordinates is not likely to vary a lot from one pixel to the next.
 *
 *    Therefore, the group of source pixels that contribute to a given target
 *    pixel is likely to have a similar shape as one moves from one target
 *    pixel to the next; or, expressed in a different way, the sequence of
 *    source pixel movements is likely to show a similar pattern.
 *
 *    This can be used to compact ("compress") the relative addressing scheme.
 *    The idea is to define a certain number of possible movement sequences and
 *    then look at all target pixels to find out whether the corresponding
 *    source pixel combination is described by one of these defined sequences.
 *    If this is the case, the individual description of the distance between a
 *    given source pixel and the previous one can be replaced by a reference to
 *    the corresponding movement sequence.
 *
 *    For this method of addressing the additional compressed addressing tables
 *    "rel_tab" and "relend_tab" in the structure "lut_descript" are used.
 *    "rel_tab" contains (at most) 256 predefined source pixel movement
 *    sequences of at most 18 pixels each. These 18 pixels are mainly "below
 *    and to the right" of the present pixel.
 *
 *    More precisely, if the present pixel is marked by "x", then all the
 *    pixels marked with a "." in the following drawing can be reached:
 *
 *                  x . . .           (the three pixels to the right)
 *                . . . . .           (line below: 1 left to 3 right)
 *                . . . . .           (two lines below: 1 left to 3 right)
 *                . . . . .           (three lines below: 1 left to 3 right)
 *
 *    The element 0 of each sequence contains the position of the next pixel
 *    relative to the present pixel. All subsequent elements of the sequence
 *    then contain the position of the next pixel relative to the previous one.
 *
 *    relend_tab contains the actual length for each sequence stored in rel_tab
 *    in the following way:
 *
 *      for the sequence "i" starting in rel_tab[i << RELTABSH]
 *
 *      relend_tab[i] = pointer to the end location of that sequence in rel_tab
 *
 * 4) it is also possible that no source pixels contribute to the given target
 *    pixel. Then obviously no source pixel indices and fractions need be
 *    given, it is only necessary to indicate the existence of such "empty"
 *    target pixels.
 *
 *    A special case are empty target pixels at the beginning of the image.
 *    They can easily be skipped over by starting the image processing only at
 *    the first non-empty target pixel. For this purpose, the member
 *    "starttidx" of the structure "lut_descript" contains the index of the
 *    first non-empty target pixel, and the member "startsidx" contains the
 *    index of the first source pixel for this target pixel.
 *
 * The use of these tables in the calculation of the distortion correction is
 * as follows:
 *
 * 1) processing starts at the target pixel identified by "starttidx", and the
 *    first of the corresponding source pixels is in "startsidx". The pointer
 *    into the look-up-table program "prog" is set to the beginning of "prog";
 *
 * 2) the instruction (i.e., the value contained in the program pointer) is
 *    examined:
 *
 * 2.1) if it is not 0, then the compressed source pixel addressing is used
 *      (case 3 above), and the instruction contains the relevant index into
 *      the compressed addressing tables "rel_tab" and "relend_tab".
 *
 *      A loop is performed over the source pixels given by "rel_tab" until
 *      the end location defined in "relend_tab" is reached. The look-up table
 *      "lut" contains the corresponding fractional contributions of the source
 *      pixels to the target pixel, i.e. there are for this target pixel as
 *      many values in the look-up table as there are source pixels that
 *      contribute to this target pixel.
 *
 *      The content of each source pixel is multiplied by the corresponding
 *      fractional contribution. The sum over all contributing source pixels
 *      gives the image value for the target pixel;
 *
 * 2.2) if it is 0, then an "exception" occurred, i.e. a different addressing
 *      scheme ist used. The type of the exception is contained in the next
 *      instruction of the look-up-table program. Possible exceptions are
 *      UNCOMPRESSED, ABSSRC, INCTARGET, MULTIINC and PROGEND and combinations
 *      of them. However, not all combinations are possible.
 *
 * 2.2.1) if the exception is just "UNCOMPRESSED", then the relative addressing
 *        scheme is used (case 2 above). The following lookup-table instruction
 *        starts a sequence of values, which is terminated by an instruction
 *        containing 0; the length of the sequence (without the terminating 0)
 *        is just the number of source pixels contributing to the target pixel.
 *
 *        These values are indices into the index offset table "offset_tab".
 *        The corresponding value in the offset table contains the index
 *        offset from the previous source pixel to the present (new) one; for
 *        the first source pixel of a particular target pixel the previous
 *        source pixel is the last source pixel of the previous target pixel.
 *
 *        The fractional contributions of the source pixels to the target pixel
 *        are in the look-up table "lut", and the value of the target pixel is
 *        calculated by a loop over the source pixels. This is the same
 *        procedure as described for the "compressed addressing scheme" above;
 *
 * 2.2.2) if the exception is just "ABSSRC", then the absolute addressing
 *        scheme is used (case 1 above). The x and y coordinates of the first
 *        source pixel contributing to this target pixel are stored in two
 *        consecutive locations in the source coordinate table "abs_src", and
 *        the fractional contribution of this source pixel to the target pixel
 *        is in the following (third) location of the source coordinate table.
 *        For each source pixel that is specified with this addressing scheme,
 *        there are therefore 3 values in the source coordinate table.
 *
 *        The following source pixels are specified by compressed addressing
 *        as described in 2.1 above: the next look-up-table program instruction
 *        contains the relevant index into the compressed addressing tables
 *        "rel_tab" and "relend_tab", and the fractional contributions of the
 *        source pixels to the target pixel are in the look-up table "lut".
 *
 *        Calculation of the target pixel value starts with multiplying the
 *        value of the first source pixel with its corresponding fractional
 *        contribution, and then adding the values of the other source pixels
 *        in a loop as described in step 2.1 above;
 *
 * 2.2.3) if the exception is "ABSSCR" combined with "UNCOMPRESSED", then the
 *        absolute addressing scheme is used for the first source pixel and
 *        the relative addressing scheme for the subsequent ones.
 *
 *        The values for the first source pixel are obtained as in 2.2.2 above,
 *        and the values for the subsequent ones as described in 2.2.1 above.
 *
 *        Calculation of the target pixel vlaue is as described in 2.2.2 above.
 *
 * 2.2.4) if the exception is just "INCTARGET" (note: this cannot happen for
 *        the first target pixel), then it is an empty target pixel as
 *        described in case 4 above.
 *
 *        The value of the target pixel is set to 0. No other action is taken;
 *
 * 2.2.5) if the exception is "INCTARGET" combined with "MULTIINC" (note: this
 *        cannot happen for the first target pixel), then it is sequence of
 *        empty target pixels. Empty target pixels are described in case 4
 *        above.
 *
 *        The two following look-up-table program instructions contain the LSB
 *        and the MSB of a 16-bit integer value that gives the number of empty
 *        target pixels in this sequence.
 *
 *        A loop is performed over all these target pixels, and the value of
 *        each of them is set to 0. No other action is taken;
 *
 * 2.2.6) if the exception is "PROGEND", then the target pixel processing is
 *        terminated;
 *
 * 3) during this processing the look-up-table program pointer is advanced
 *    every time an instruction from the program was used. With the value now
 *    reached, processing continues at step 2 above. This loop ends when the
 *    exception "PROGEND" is encountered (step 2.2.6).
 *
 * The calculation of these tables is done in several steps, and the tables are
 * modified several times:
 *
 * Step 1: determine for each target pixel the contributing source pixels and
 *         the fractional contributions of the source pixels to the target
 *         pixel. At the end of this step, the source coordinate table contains
 *         for all target pixels the first contributing source pixel, and the
 *         subsequent source pixels are given in the look-up-table program
 *         using the relative addressing scheme. The fractional contributions
 *         are stored in the look-up table. Each empty target pixel occupies
 *         one place in each of these tables. The beginning of a new target
 *         pixel is indicated by the flag BITMASK in the look-up table;
 *
 * Step 2: remove the empty target pixels at the beginning of the target image,
 *         and mark the other empty target pixels with the flag INCTARGET in
 *         the look-up table. The look-up table and the look-up-table-program
 *         are shortened according to the number of empty pixels removed;
 *
 * Step 3: optional: if the user-definable global variable "DO_FLAT" is set,
 *         the target image is normalized to a flat image. The structure of the
 *         tables is not changed;
 *
 * Step 4: the absolute addressing scheme for the first contributing source
 *         pixel is replaced by relative addressing whenever possible. Where it
 *         is not possible, the flag ABSSRC is set in the corresponding
 *         location in the look-up table.
 *
 *         At the end of this step, the source coordinate table has its final
 *         form as described above;
 *
 * Step 5: the relative addressing is replaced by the compressed addressing
 *         scheme whenever possible.
 *
 *         At the end of this step, all tables have their final form as
 *         described above.
 *
 * Input : none
 * Output: none
 * Return: if successful: structure with the variables and tables needed for the
 *                        distortion correction
 *         else:          NULL
 */

struct lut_descript *lut_calc()
{
#define MAXSTAT 20
  unsigned char *lut_prog,*prog_ptr,*new_prog_ptr,*prog_end,*new_inc;
  unsigned char instruct;
  unsigned short *abs_ptr,*abs_src,*new_abssrc;
  unsigned int part,psum,max_pixelpart;
  unsigned int xsize1 = XSIZE + 1,ysize1 = YSIZE + 1,xysize = XSIZE * YSIZE;
  int xmin,xmax,ymin,ymax,idx,pidx;
  int i,j,ix,iy;
  int max_xsize = 0, max_ysize = 0,count_parts = 0;
  int prog_length,lutsize,new_plen;
  int *offsets,*offset_p;
  int src_x,src_y,old_instruct;
  int flush_interval,flush_cnt;
  int offset,lastoffset;
  int first,noabs;
  int new_x,new_y,diff_x,diff_y;
  int max_ocount,max_oidx;
  unsigned long stat_case[MAXSTAT];
  unsigned long start_ff,start_tidx,tidx;
  unsigned long count_abs = 0,count_inc = 0,count_trg = 0,count_ff = 0;
  unsigned long count_deleted = 0;
  float parts[MAX_PARTS];
  float fx[4],fy[4],fxmin,fxmax,fymin,fymax;
  float min_xdist = 1E6,max_xdist = 0,min_ydist = 1E6,max_ydist = 0;
  float *x_trg,*y_trg;
  float total;
  LUT_TYPE *lut_tab,*lut_ptr,*new_lut_ptr,*max_pixellut;
  struct lut_descript *lut_d;

  /*
   *
   * Step 1: determine for each target pixel the contributing source pixels and
   *         the fractional contributions of the source pixels to the target
   *         pixel. Set up the look-up table, the look-up-table program and the
   *         source coordinate table.
   *
   * Allocate memory for the look-up table structure and initialize it to 0.
   */
  if((lut_d = (struct lut_descript *)pmalloc(sizeof(struct lut_descript)))
    == NULL)
    prmsg(FATAL,("no memory\n"));
  memset(lut_d,0,sizeof(struct lut_descript));

  /*
   * Allocate two arrays for the corrected x and y coordinate values.
   *
   * For an "n * m" image, each array has to have the size "(n+1) * (m+1)".
   * There is one array element more in each direction as for each pixel the
   * four corner points are calculated. For the last pixel "n" in a line that
   * leads to calculate the distortion for point "n+1".
   *
   * Fill the arrays with the corrected x and the corrected y coordinate values
   * for the four corners of each pixel of the image.
   *
   * See loop below "MARKER: IX+1, IY+1".
   */
  if((x_trg = (float *)pmalloc(xsize1 * ysize1 * sizeof(float))) == NULL ||
    (y_trg = (float *)pmalloc(xsize1 * ysize1 * sizeof(float))) == NULL) {
    prmsg(FATAL,("no memory in lut_calc for x_trg and y_trg\n"));
  }

  for(iy = 0; iy < ysize1; iy++) {
    for(ix = 0; ix < xsize1; ix++) {
      idx = ix + iy * xsize1;
      if(spd_func(ix,iy,x_trg + idx,y_trg + idx) != 0)
        return(NULL);
    }
  }

  bench("X Y array calculation");
  prmsg(DMSG,("X and Y target arrays calculated\n"));

  /*
   * Free SPD buffers, the M_COR array is still needed
   */
  spd_free_buffers(X_COR,Y_COR);
  X_COR = Y_COR = NULL;

  /*
   * Produce a table that specifies how many source pixels contribute to each
   * target pixel.
   *
   * For this, loop over all source pixels. For each source pixel, get its
   * corrected coordinates in the target image; this determines to which target
   * pixels this source pixel contributes.
   */
  if((offsets = (int *)pmalloc(xysize * sizeof(int))) == NULL) {
    prmsg(FATAL,("no memory in lut_calc for offsets\n"));
  }

  print_memsize();
  memset(offsets,0,xysize * sizeof(int));

  for(iy = 0; iy < YSIZE; iy++) {
    for(ix = 0; ix < XSIZE; ix++) {
      /*
       * Get the corrected (x,y) coordinate values (fx,fy) for the corners of
       * the square formed by the present point and the three neighboring ones
       * (i.e., the four corners of the present source pixel).
       *
       * The present point has index 0, the others are arranged as follows:
       *
       *                         0   1   |---> x
       *                                 |
       *                         3   2   V y
       *
       * MARKER: IX+1, IY+1
       */
      idx = ix + iy * xsize1;
      fx[0] = x_trg[idx];              fy[0] = y_trg[idx];
      fx[1] = x_trg[idx + 1];          fy[1] = y_trg[idx+1];
      fx[2] = x_trg[idx + xsize1 + 1]; fy[2] = y_trg[idx + xsize1 + 1];
      fx[3] = x_trg[idx + xsize1];     fy[3] = y_trg[idx + xsize1];

      /*
       * Determine the x and y size in corrected pixel indices (integers) of the
       * mapped square (= mapped source pixel).
       */
      minmax4(fx[0],fx[1],fx[2],fx[3],&fxmin,&fxmax);
      minmax4(fy[0],fy[1],fy[2],fy[3],&fymin,&fymax);

      if(fxmin < 0 || fymin < 0 || fxmax > XSIZE  || fymax > YSIZE)
	continue;

      xmin = floor(fxmin);
      xmax = ceil(fxmax);
      ymin = floor(fymin);
      ymax = ceil(fymax);

      /*
       * Determine the minimum and maximum distortion as well as the maximum
       * size of all pixel squares in the image.
       */
      if(xmax - xmin > MAX_PIXELSIZE || ymax - ymin > MAX_PIXELSIZE)
	continue;

      if(max_xdist < fabs(fx[0] - ix))
	max_xdist = fabs(fx[0] - ix);
      if(max_ydist < fabs(fy[0] - iy))
	max_ydist = fabs(fy[0] - iy);
      if(min_xdist > fabs(fx[0] - ix))
	min_xdist = fabs(fx[0] - ix);
      if(min_ydist > fabs(fy[0] - iy))
	min_ydist = fabs(fy[0] - iy);

      if(max_ysize < ymax - ymin)
	max_ysize = ymax - ymin;
      if(max_xsize < xmax - xmin)
	max_xsize = xmax - xmin;

      /*
       * Increase the contribution count for each target pixel to which this
       * source pixel contributes.
       *
       * At the end of the loop over the source pixels, "offsets" contains for
       * each target pixel the number of source pixels that contribute to it.
       */
      for(j = ymin; j < ymax; j++) {
	for(i = xmin; i < xmax; i++) {
	  tidx = i + XSIZE * j;
	  offsets[tidx]++;
	}
      }
    }
  }

  /*
   * Prepare the look-up table "lut_tab", the look-up-table program "lut_prog"
   * and the source coordinate array "abs_src".
   *
   * The look-up table will contain for every source pixel the part (fraction)
   * of that pixel's area that is mapped into each of the target pixels.
   *
   * The look-up-table program will contain for every target pixel the
   * information which source pixels contributed to it.
   *
   * The source coordinate array will contain for every target pixel the (x,y)
   * coordinates of the first source pixel that contributes to it.
   *
   * In detail:
   *
   * look-up table "lut_tab":
   * - the look-up table contains for each target pixel a sequence of numbers.
   *   The length of each sequence is the number of source pixels that are
   *   mapped into this target pixel. If there are no source pixels mapped into
   *   a particular target pixel, then the length is set to 1;
   * - each number in a given sequence corresponds to one of the source pixels.
   *   It gives, for target pixel "i" and source pixel "j", the part (fraction)
   *   of the area of "j" that maps into "i";
   * - the beginning of each sequence is given by the indices in the offset
   *   table "offsets" in the following form:
   *
   *      lut_tab[offsets[tidx]] = first element of the sequence
   *                               for target pixel "tidx"
   *
   * - the values (fractions) stored in the look-up table are constrained to be
   *   less than FULLSCALE (normally 0x8000);
   * - in the beginning of each sequence, the value to be stored is "OR"ed with
   *   BITMASK (normally 0x8000).
   *
   * look-up-table program "lut_prog":
   * - the look-up-table program contains for each target pixel a sequence of
   *   instructions. In this sequence, there is one instruction for every
   *   source pixel that is mapped into this target pixel. If there are no
   *   source pixels mapped into a particular target pixel, then there is
   *   still one instruction;
   * - from the instructions, the coordinates of the source pixels can be
   *   determined in the following way:
   *
   *   -- the instructions are 8 bit long;
   *   -- the information concerning the y coordinate is in the 4 high bits,
   *      the one for the x coordinate in the 4 low bits;
   *   -- the first instruction of a sequence is always 0x88, the coordinates
   *      of the first source pixel are obtained from the source coordinate
   *      array;
   *   -- the (x,y) coordinates of all subsequent source pixels are stored in
   *      the corresponding instructions as relative values to the last pixel.
   *      For pixel "i" of a sequence, the instruction value as a hexadecimal
   *      number "yx" is given by
   *            y = y(i-1) - y(i) + 8
   *            x = x(i-1) - x(i) + 8
   *  -- the maximum difference in x or y that is allowed by the program is
   *     +-7, i.e. the present pixel can be at most 7 pixels to the left or
   *     7 pixels to the right of the last one, and likewise 7 pixels above
   *     or 7 pixels below the present one. As an additional restriction, the
   *     difference in x and in y must not be +7 simultaneously (this is to
   *     prevent a valid instruction from having the initialization value
   *     0xff);
   *  -- if there is no source pixel mapped into a particular target pixel, the
   *     (one) corresponding program instruction has the initialization value
   *     0xff.
   *
   * - the beginning of each sequence is given by the indices in the offset
   *   table "offsets" in the following form:
   *
   *      lut_prog[offsets[tidx]] = first instruction of the sequence
   *                                for target pixel "tidx"
   *
   * source coordinate array "abs_src":
   * - the source coordinate array contains for each target pixel the x and
   *   the y coordinate of the first source pixel that contributes to it;
   * - if there is no source pixel that contributes to a given target pixel,
   *   then the coordinates are 0xffff (= initialization value);
   * - for target pixel "tidx", "abs_src[2 * tidx]" contains the x coordinate
   *   and "abs_src[2 * tidx + 1]" contains the y coordinate.
   *
   * First, fill the offset table.
   *
   * For each target pixel, the offset has to be incremented by the length of
   * its corresponding sequence in "lut_tab" and "lut_prog" (that is the number
   * of contributing source pixels, or one if there is no contributing source
   * pixel).
   *
   * The total length of all sequences thus is equal to the length of the
   * look-up table.
   */
  max_ocount = 0;
  max_oidx = 0;
  for(count_parts = 0, offset_p = offsets, i = 0; i < xysize; i++) {
    if(*offset_p == 0)
      count_parts += 1;
    else
      count_parts += *offset_p;
    if(max_ocount < *offset_p) {
      max_ocount = *offset_p;
      max_oidx = i;
    }
    *offset_p++ = count_parts;
  }

  prmsg(MSG,("Max Distortion: x = %f y = %f\n",max_xdist,max_ydist));
  prmsg(MSG,("Min Distortion: x = %f y = %f\n",min_xdist,min_ydist));
  prmsg(MSG,("Max Pixel size: x = %d y = %d\n",max_xsize,max_ysize));
  prmsg(MSG,("Max SRC Pixel in Target : %d at [%d,%d]\n",
    max_ocount,max_oidx % XSIZE,max_oidx / XSIZE));
  prmsg(MSG,("Parts Count total: %d\n",count_parts));

  lut_d->maxxpixel = max_xsize;
  lut_d->maxypixel = max_ysize;

  /*
   * Allocate memory for the look-up table, and initialize the parts sequences.
   *
   * The initialization is as follows:
   * - the beginning of each sequence is set to BITMASK;
   * - all other values are set to 0.
   */
  prog_length = count_parts;
  lut_ptr = lut_tab = (LUT_TYPE *)pmalloc(sizeof(LUT_TYPE) * count_parts);

  if(lut_tab == NULL)  {
    prmsg(FATAL,("no memory for lut\n"));
  }

  memset(lut_tab,0,prog_length * sizeof(LUT_TYPE));
  lut_tab[0] = BITMASK;
  for(offset_p = offsets, i = 0; i < xysize - 1; i++) {
    lut_tab[*offset_p++] = BITMASK;
  }

  /*
   * Allocate memory for the look-up-table program, and initialize all
   * instructions to 0xff.
   */
  if ((prog_ptr = lut_prog = (unsigned char *)
    pmalloc(sizeof(char) * prog_length)) == NULL) {
    prmsg(FATAL,("no memory for prog\n"));
  }

  memset(lut_prog,0xff,prog_length * sizeof(char));

  /*
   * Allocate memory for the source coordinate array, and initialize all
   * coordinates to 0xffff.
   */
  if((abs_src = (unsigned short *)
    pmalloc(2 * sizeof(short) * xysize)) == NULL) {
    prmsg(FATAL,("no memory for abs_src\n"));
  }

  memset(abs_src,0xff,2 * sizeof(short) * xysize);

  print_memsize();
  prmsg(MSG,("Going to produce the correction program\n"));

  flush_interval = YSIZE / 64;
  flush_cnt = 0;

  /*
   * Loop over all source pixels and get the corresponding corrected coordinate
   * values (target pixels).
   *
   * Then determine for every target pixel how many source pixels contribute to
   * it, and what fraction of each source pixel's area goes into that particular
   * target pixel.
   */
  for(iy = 0; iy < YSIZE; iy++) {
    if(flush_cnt++ > flush_interval) {
      flush_cnt = 0;
      prmsg(MSG | PRERR,("."));
    }
    for(ix = 0; ix < XSIZE; ix++) {
      /*
       * Get the corrected (x,y) coordinate values (fx,fy) for the corners of
       * the square formed by the present point and the three neighboring ones.
       *
       * The present point has index 0, the others are arranged as follows:
       *
       *                         0   1   |---> x
       *                                 |
       *                         3   2   V y
       */
      idx = ix + iy * xsize1;
      fx[0] = x_trg[idx];              fy[0] = y_trg[idx];
      fx[1] = x_trg[idx + 1];          fy[1] = y_trg[idx+1];
      fx[2] = x_trg[idx + xsize1 + 1]; fy[2] = y_trg[idx + xsize1 + 1];
      fx[3] = x_trg[idx + xsize1];     fy[3] = y_trg[idx + xsize1];

      minmax4(fx[0],fx[1],fx[2],fx[3],&fxmin,&fxmax);
      minmax4(fy[0],fy[1],fy[2],fy[3],&fymin,&fymax);

      if(fxmin < 0 || fymin < 0 || fxmax > XSIZE  || fymax > YSIZE)
	continue;

      /*
       * Determine for the source pixel whose corrected coordinates are in
       * (fx,fy) the target pixels that it is mapped on, and the area of the
       * source pixel that is mapped into each of the target pixels.
       *
       * On return from calcparts(), the array "parts" contains these areas. It
       * represents a two-dimensional array in target x and y pixel indices,
       * with x increasing fastest. The index ranges are xmin <= x <= xmax and
       * ymin <= y <= ymax.
       */
      xmin = floor(fxmin);
      xmax = ceil(fxmax);
      ymin = floor(fymin);
      ymax = ceil(fymax);
      if(xmax - xmin > MAX_PIXELSIZE || ymax - ymin > MAX_PIXELSIZE) {
	prmsg(WARNING,
          ("target size of pixel (%d,%d) too big (>%d): siz=(%d,%d)\n",ix,iy,
          MAX_PIXELSIZE,xmax - xmin,ymax - ymin));
	continue;
      } else {
	calcparts(fx,fy,0,parts,&xmin,&xmax,&ymin,&ymax,&total);
	if(total < 1E-6) {
	  prmsg(WARNING,("pixel [%f,%f], [%f,%f], [%f,%f], [%f,%f] is 0\n",
	    fx[0],fy[0],fx[1],fy[1],fx[2],fy[2],fx[3],fy[3]));
	  prmsg(WARNING,("        ix: %d iy: %d\n",ix,iy));
	  continue;
	}
      }

      max_pixelpart = 0;
      max_pixellut = NULL;
      pidx = 0;
      psum = 0;

#if BOUND_CHECK
      if(xmax >= XSIZE || xmin < 0 || ymax >= YSIZE || ymin < 0)
	prmsg(FATAL,("target pixel out of bounds %d %d %d %d\n",
	  xmin,xmax,ymin,ymax));
#endif /* BOUND_CHECK */

      /*
       * Loop over all target pixels that have contributions from the source
       * pixel being processed.
       */
      for(j = ymin; j <= ymax; j++) {
	for(i = xmin; i <= xmax; i++) {

	  /*
	   * Determine for the target pixel the start locations in the look-up
	   * table, the look-up-table program and the source coordinate array.
	   */
	  if(i == 0 && j == 0)
	    offset = 0;
	  else
	    offset = offsets[i + j * XSIZE - 1];
	  prog_ptr = lut_prog + offset;             /* start point program */
	  lut_ptr = lut_tab + offset;               /* start point LUT */
	  abs_ptr = abs_src + 2 * (i + j * XSIZE);  /* start point coor. arr. */

#if BOUND_CHECK
	  /*
	   * Determine end location in look-up-table program (to prevent
	   * overwriting of program).
	   */
	  if(i == XSIZE - 1 && j == YSIZE - 1)
	    lastoffset = prog_length - 1;
	  else
	    lastoffset = offsets[i + j * XSIZE] - 1;

	  if(!(*lut_ptr & BITMASK))
	    prmsg(ERROR,("target bit is unset for %d (*lut_ptr = 0x%x)\n",
	      offset,*lut_ptr));
#endif /* BOUND_CHECK */

          /*
           * The contributions of each source pixel to each target pixel are so
           * far expressed as absolute area values.
           *
           * Convert them into relative values by dividing each of those area
           * values by the total area that the corresponding source pixel
           * covers in the target space.
           *
           * Technical remark: the fraction is expressed as an integer on a
           * scale from 0 to FULLSCALE (normally 0x8000 = 32768).
           */
	  part = (parts[pidx] / (float)total * (float)FULLSCALE + .5);

          /*
           * Fill the source coordinate array, the look-up table, and the
           * look-up-table program with the values for this particular target
           * pixel.
           *
           * If there is no source pixel contributing to this target pixel,
           * then skip this part.
           */
	  if(part == 0) {
	    pidx++;
	    continue;
	  }

          /*
           * Fill the source coordinate array.
           *
           * If this is the first source pixel that contributes to this target
           * pixel, fill its x and y coordinates in the source coordinate
           * array.
           *
           * Otherwise, get the coordinates of the first contributing source
           * pixel from the source coordinate array.
           */
	  if(*abs_ptr == 0xffff) {
	    first = 1;
	    src_x = *abs_ptr++ = ix;
	    src_y = *abs_ptr   = iy;
	  } else {
	    first = 0;
	    src_x = *abs_ptr++;
	    src_y = *abs_ptr;
	  }

          /*
           * Fill the look-up-table program.
           *
           * Go through the instructions of the look-up-table program for this
           * target pixel and calculate the (x,y) coordinates of all source
           * pixels that have already been processed for this target pixel.
           *
           * This loop ends when encountering an instruction that has still the
           * initialization value 0xff (note that this cannot be a valid
           * instruction for coordinate calculations).
           *
           * At the end of the loop, src_x and src_y contain the (x,y)
           * coordinates of the last source pixel already processed for this
           * target pixel.
           */
	  while((old_instruct = *prog_ptr) != 0xff) {
	    prog_ptr++;
	    lut_ptr++;
	    src_x += (old_instruct & 0x0f) - 8;
	    src_y += ((old_instruct & 0xf0) >> 4) - 8;
	  }

#if BOUND_CHECK
	  if(src_x >= XSIZE || src_x < 0 || src_y >= YSIZE || src_y < 0)
	    prmsg(MSG,("Source pixel out of bounds in %d  %d %d (0x%x 0x%x)\n",
	      prog_ptr - lut_prog,src_x,src_y,instruct,part));
#endif /* BOUND_CHECK */

          /*
           * Calculate in x and y the difference between the last and the
           * present source pixel and store them in the instruction. For
           * details, see the description of the look-up-table program further
           * above.
           */
	  instruct = (ix - src_x + 8) + ((iy - src_y + 8) << 4);
	  if(abs(src_x - ix) > 7 || abs(src_y - iy) > 7 || instruct == 0xff) {
#if BOUND_CHECK
	    prmsg(MSG,("Source pixel area too big in %d  [%d,%d]  [%d,%d]\n",
	      prog_ptr - lut_prog,src_x,src_y,ix,iy));
#endif /* BOUND_CHECK */
	    count_deleted++;
	    pidx++;
	    if(first) {
	      prmsg(FATAL,("cannot happen"));
	    }
	    continue;
	  }

#if BOUND_CHECK
	  if(prog_ptr > lut_prog + lastoffset)
	    prmsg(FATAL,("about to overwrite LUT program in %d (>%d)\n",
	      prog_ptr - lut_prog,lastoffset));
#endif /* BOUND_CHECK */

	  *prog_ptr = instruct;

          /*
           * Fill the look-up table.
           *
           * The locations in the look-up table and the look-up-table program
           * must always be synchronized. The pointer for the look-up table
           * has been increased above simultaneously with the one for the
           * look-up-table program, thus it now points to the location in the
           * look-up table where the contribution from source pixel (ix,iy) to
           * target pixel (i,j) should go.
           *
           * Store this contribution in the look-up table. For details, see the
           * description of the look-up table further above.
           */
	  if(part > max_pixelpart) {
	    max_pixelpart = part;
	    max_pixellut  = lut_ptr;
	  }
	  psum += part;

	  if(part >= FULLSCALE)
	    part = 0;

	  if(first)
	    part |= BITMASK;

#if BOUND_CHECK
	  if(first && !(*lut_ptr & BITMASK))
	    prmsg(ERROR,("target bit is unset for %d (*lut_ptr = 0x%x)\n",
	      lut_ptr - lut_tab,*lut_ptr));
	  if(!first && (*lut_ptr & BITMASK))
	    prmsg(ERROR,("target bit is set for %d (*lut_ptr = 0x%x)\n",
	      lut_ptr - lut_tab,*lut_ptr));
#endif /* BOUND_CHECK */

	  *lut_ptr = part;

#if BOUND_CHECK
          /*
           * At the beginning of a sequence, the BITMASK flag must be set in
           * the look-up table and the corresponding look-up-table program
           * instruction must be 0x88. If not, exit with error.
           */
	  if(*lut_ptr & BITMASK)
	    if(*prog_ptr != 0x88)
	      prmsg(FATAL,("instruction != 0x88 but 0x%x (0x%x in %d)\n",
		*prog_ptr,*lut_ptr,prog_ptr - lut_prog));
	    else
              /*
               * count_ff only here for debugging
               */
	      count_ff++;
#endif /* BOUND_CHECK */

	  pidx++;
	}
      }

      /*
       * End of the loop processing the target pixels for this source pixel.
       *
       * The sum of all contributions (parts) of this source pixel should be
       * equal to FULLSCALE (because of the way the parts are normalized). If
       * this is not the case, then there have been rounding errors. Attribute
       * the difference (i.e., FULLSCALE - sum of all parts) to the target pixel
       * that has the largest contribution from this source pixel - that seems
       * to be the best choice. Note that this difference can be negative, then
       * the content of this target pixel will get smaller.
       *
       * Note that no value in the look-up table must be equal to FULLSCALE
       * because it would create confusion with the meaning of BITMASK.
       * Therefore, if a pixel has really the value FULLSCALE, hide it as 0.
       * Real target pixels cannot have a 0 contribution, because then they
       * would not have been included in the list of pixels that the source
       * pixel contributes to.
       */
      if(max_pixellut && psum != FULLSCALE) {
	int savelut;
	savelut = *max_pixellut & MAPSCALE;
	if(savelut == 0)
	  savelut = FULLSCALE;
	part = savelut + FULLSCALE - psum;
	if(part >= FULLSCALE)
	  part = 0;
	if(*max_pixellut & BITMASK)
	  *max_pixellut = part | BITMASK;
	else
	  *max_pixellut = part;
#if BOUND_CHECK
	if(*max_pixellut & BITMASK) {
	  instruct = lut_prog[max_pixellut - lut_tab];
          if(instruct != 0x88)
	    prmsg(FATAL,("prog != 0x88 max_p 0x%x (0x%x in %d) 0x%x %d\n",
	      instruct,*max_pixellut,prog_ptr - lut_prog,savelut,psum));
	  else
            /*
             * count_ff only here for debugging
             */
	    count_ff++;
	}
#endif /* BOUND_CHECK */
      }
    }
  }

  prmsg(MSG,("\n"));
  /*
   * At this point we have finished the look-up-table program "lut_prog" with
   * all the relative source pixel coordinates and the look-up table "lut_tab"
   * with the fractional contributions of the source pixels to the target
   * pixels. The values in the look-up table have the highest bit set if at the
   * beginning of a new target pixel sequence.
   *
   * Beginning of a new sequence:
   *
   *                look-up table          program
   *
   *              1vvv vvvv vvvv vvvv      10001000
   *              0vvv vvvv vvvv vvvv      yyyyxxxx
   *              0vvv vvvv vvvv vvvv      yyyyxxxx
   */

  if(count_deleted)
    prmsg(WARNING,
      ("\n%d target pixels deleted, too many source pixels contribute\n",
      count_deleted));

#if BOUND_CHECK
  /*
   * Test whether the arrays are correctly set up for the number of target
   * pixels in the image:
   *
   * - the number of entries in the look-up table that have the BITMASK flag
   *   set should be equal to the number of target pixels (BITMASK signals the
   *   begin of a new target pixel sequence);
   * - the number of entries in the source coordinate array that still have
   *   the initialization value should be equal to the number of target pixels
   *   that do not receive any contribution from any of the source pixels;
   * - all entries in the look-up table that correspond to an index value in
   *   the offset table should be at the beginning of a new sequence and thus
   *   have the BITMASK flag set.
   */
  {
    int count_case = 0, i, idx;
    prog_end = lut_prog + prog_length;
    lut_ptr = lut_tab;
    for(prog_ptr = lut_prog; prog_ptr != prog_end; prog_ptr++, lut_ptr++) {
      if(*lut_ptr & BITMASK)
	count_case++;
    }
    if(count_case != xysize)
      prmsg(ERROR,("lost some target pixels (now only %d)\n",count_case));

    for(count_case = 0, i = 0; i < xysize; i++)
      if(abs_src[2*i] == 0xffff && abs_src[2*i+1] == 0xffff)
	count_case++;
    prmsg(DMSG,("Empty targets via abs_src count: %d\n",count_case));

    for(offset_p = offsets, i = 0; i < xysize - 1; i++) {
      if(!(lut_tab[*offset_p++] & BITMASK)) {
	prmsg(ERROR,("target advance for %d lost\n",i));
	for(idx = *(offset_p-1); idx < *offset_p; idx++)
	  prmsg(DMSG,(" %d: 0x%4x 0x%2x\n",idx,lut_tab[idx],lut_prog[idx]));
      }
    }
  }
#endif /* BOUND_CHECK */

  /*
   *
   * Step 2: remove the empty target pixels at the beginning of the target
   *         image, and mark the other empty target pixels with the flag
   *         INCTARGET in the look-up table. The look-up table and the
   *         look-up-table-program are shortened according to the number of
   *         empty pixels removed.
   *
  */
  prmsg(MSG,("Delete untouched pixels\n"));

  /*
   * Go through the look-up table and the look-up-table program to find empty
   * target pixels, i.e. target pixels that do not receive a contribution from
   * any source pixel.
   *
   * Delete all consecutive empty target pixels at the beginning of the image,
   * and mark all other empty target pixels as an exception of type INCTARGET.
   */
  abs_ptr  = abs_src;
  prog_end = lut_prog + prog_length;
  lut_ptr  = lut_tab;
  start_ff = 1;
  start_tidx = 0;

  /*
   * Find all empty target pixels. These pixels still contain the
   * initialization value 0xff as look-up-table program instruction.
   *
   * Count how many consecutive empty target pixels are at the beginning of the
   * image array.
   *
   * Mark all other empty target pixels
   * - by setting the look-up-table program instruction to 0x0 (signifies
   *   exception),
   * - and by setting the flags BITMASK and INCTARGET as value in the look-up-
   *   table (signifies beginning of a new target sequence with an empty target
   *   pixel).
   */
  for(prog_ptr = lut_prog; prog_ptr != prog_end; prog_ptr++, lut_ptr++) {
    if(*prog_ptr == 0xff) { /* Nothing will be put in this target */
      if(*lut_ptr & BITMASK) {
	if(start_ff)
	  start_tidx++;
	else {
	  *prog_ptr = 0x0;
	  *lut_ptr  = BITMASK | INCTARGET;
	  count_inc++;
	}
      }
    } else
      start_ff = 0;
  }

  /*
   * Remove the consecutive empty target pixels from the beginning of the
   * look-up table and the look-up-table program.
   *
   * Then re-adjust the size of the look-up table and the look-up-table
   * program.
   */
  new_prog_ptr = lut_prog;
  new_lut_ptr = lut_tab;
  lut_ptr = lut_tab;
  for(prog_ptr = lut_prog; prog_ptr != prog_end; prog_ptr++, lut_ptr++) {
    *new_prog_ptr = *prog_ptr;
    *new_lut_ptr = *lut_ptr;
    if(*prog_ptr != 0xff) {
      new_prog_ptr++;
      new_lut_ptr++;
    }
  }

  prog_length = new_prog_ptr - lut_prog;

  if(count_parts != prog_length)
    prmsg(DMSG,("%d deleted - start offset %d\n",count_parts - prog_length,
      start_tidx));

  if((prog_ptr = lut_prog =
    (unsigned char *)prealloc(lut_prog,sizeof(char) * prog_length)) == NULL)
    prmsg(FATAL,("realloc failed\n"));

  if((lut_ptr = lut_tab =
    (LUT_TYPE *)prealloc(lut_tab,sizeof(LUT_TYPE) * prog_length)) == NULL)
    prmsg(FATAL,("realloc failed\n"));

  /*
   * At this point the empty target pixels have been marked in the look-up
   * table and the program in the following way:
   *
   *             program     look-up table
   *
   *             00000000    1000 0000 0000 0001  (= BITMASK | INCTARGET)
   *
   * This indicates a pure target advance, i.e. a target pixel that can be
   * skipped over.
   *
   * For non-empty target pixels, the contents of look-up table and program
   * have not changed.
   */

#if BOUND_CHECK
  {
    int count_case = 0,count_case1 = 0,count_case2 = 0;

    prog_end = lut_prog + prog_length;
    lut_ptr = lut_tab;

    /*
     * At the beginning of a sequence, there are now two possibilities:
     * - either the flag INCTARGET is set and the program instruction is 0,
     *   this marks an empty target pixel;
     * - or the program instruction is 0x88 as it should be at the beginning of
     *   a sequence with source pixels.
     *
     * Give a warning if this is not the case.
     */
    for(prog_ptr = lut_prog; prog_ptr < prog_end; prog_ptr++, lut_ptr++) {
      if(*lut_ptr & BITMASK) {
	if((*lut_ptr != (BITMASK | INCTARGET) || *prog_ptr != 0) &&
	  (*prog_ptr != 0x88))
	  prmsg(ERROR,("cannot be *lut_ptr = 0x%x *prog_ptr = 0x%x\n",
	    *lut_ptr,*prog_ptr));
	if(*prog_ptr == 0)
	  count_case1++;
	else
	  count_case2++;

      } else
	count_case++;
    }

    /*
     * Verify the program length:
     * - count_case1  contains the number of empty target pixel instructions 
     *                (INCTARGET exceptions), i.e. all target pixels (except
     *                those at the beginning) with no contributing source pixel;
     * - count_case2  contains all other instructions that mark the beginning
     *                of a new sequence, i.e. all target pixels that have at
     *                least one contributing source pixel;
     * - count_case   contains the number of all instructions that are not at
     *                the beginning of a new sequence.
     *
     * The empty target pixels at the beginning of the target image have already
     * been removed from the program and are thus not counted in "count_case1".
     *
     * The sum of the three should equal the program length. If not, give an
     * error message.
     *
     * Furthermore, the total number of target pixels should be
     *
     * count_case1 + count_case2 + (number of empty target pixels at beginning)
     */
    prmsg(DMSG,
      ("INCTARGET pix = %d other new pix = %d contributing parts = %d\n",
      count_case1,count_case2,count_case));
    if(count_case + count_case1 + count_case2 != prog_length)
      prmsg(ERROR,("lost some events somewhere\n"));

  }
#endif /* BOUND_CHECK */

  /*
   *
   * Step 3: optional: if the user-definable global variable "DO_FLAT" is set,
   *         the target image is normalized to a flat image. The structure of
   *         the tables (look-up table, etc.) is not changed.
   *
   * Going to normalize to flat target image. This means that when this code is
   * run a peak with 10000 pixels in the source image will not be 10000 pixels
   * in the target image any more. On the other hand a constant image will now
   * become another constant image.
   */
  if(DO_FLAT) {
    LUT_TYPE *lp,*start_lut;
    unsigned long partsum, newpartsum;
    unsigned long part;
    LUT_TYPE bitmask;
    int count_case = 0, count_case1 = 0, count_case2 = 0;

    prmsg(DMSG,("Normalizing for flat output image\n"));
    prog_end = lut_prog + prog_length;
    lut_ptr  = lut_tab;
    start_lut = lut_tab;

    /*
     * Loop through the look-up table, add for each target pixel all fractional
     * contributions from the source pixels, rescale them and store them back
     * in the look-up table.
     *
     * To do so, start with the first target pixel and skip forward through the
     * look-up-table program until the next target pixel is found. This defines
     * the start point "start_lut" and the end point "lut_ptr" of the summing
     * for the first target pixel.
     *
     * The following target pixels are processed in an analog manner.
     *
     * Note that the loop will go too far, as it has to go all the way to the
     * end of the program. "lut_ptr" does therefore not point to a valid
     * location of the look-up table on the last turn - careful.
     */
    for(prog_ptr = lut_prog; prog_ptr <= prog_end; prog_ptr++, lut_ptr++) {
      if(prog_ptr == prog_end || (*lut_ptr & BITMASK)) {

	/*
         * Calculate the sum of the fractional contributions in the look-up
         * table for this target pixel.
         *
         * As this sums over all source pixels that contribute to a given
         * target pixel, this sum can of course be different from
         * 100 % (= FULLSCALE), in particular it can also be bigger than
         * 100 %. On the other hand, the sum of all parts that a given source
         * pixel contributes to all target pixels should always be equal to
         * FULLSCALE (= 100 %).
         *
         * Note that a value of 0 in the look-up table really means a value
         * of FULLSCALE. For a detailed explanation, see above at the end of
         * the loop processing the target pixels for a given source pixel.
         */
	partsum = 0;
	max_pixelpart = 0;
	max_pixellut = NULL;

	for(lp = start_lut; lp < lut_ptr; lp++) {
	  if(*lp & MAPSCALE)
	    part = (*lp & MAPSCALE);
	  else
	    part = FULLSCALE;

	  if(part > max_pixelpart) {
	    max_pixelpart = part;
	    max_pixellut  = lp;
	  }
	  partsum += part;
	}

#if BOUND_CHECK
        /*
         * Because of the way the loop through the look-up table is processed,
         * "start_lut" can only point at locations that correspond to the
         * beginning of a new sequence.
         *
         * Give an error message if this is not the case.
         */
	if(start_lut != lut_tab && start_lut < lut_tab + prog_length &&
	  (*start_lut & BITMASK) == 0)
	  prmsg(ERROR,("cannot be: *start_lut is 0x%x *prog = 0x%x in %d\n",
	    *start_lut,*prog_ptr,prog_ptr - lut_prog));
#endif /* BOUND_CHECK */

        /*
	 * partsum should be approximately FULLSCALE on the average.
         * The value is normally not simply FULLSCALE as we are counting
         * here the sum of percentages of the source pixel contribution to
         * the target pixel (i.e. this could be 300 % if 3 source pixels
         * contribute each to 100 %).
         *
         * Rescale the fractional contributions to make partsum equal to
         * FULLSCALE.
         */
	bitmask = BITMASK;
	newpartsum = 0;
	for(lp = start_lut; lp < lut_ptr; lp++) {
	  part = *lp & MAPSCALE;
	  if(part == 0)
	    part = FULLSCALE;
	  part = (part << SHIFT) / partsum;

          /*
           * A contribution of 0 is not supposed to happen for a contributing
           * pixel. Thus, if the rescaling leads to a contribution of 0 (trough
           * truncating in the integer calculation), set the contribution to 1.
           */
	  if(part == 0) {
	    count_case++;
	    part = 1;
	  }

          /*
           * Store the rescaled values back into the look-up table. Set the
           * BITMASK flag for the first pixel of a sequence. Note that
           * "bitmask" is BITMASK for the starting pixel of a sequence and 0
           * for all other pixels.
           *
           * As before, a value of FULLSCALE is hidden in an artificial 0.
           */
	  newpartsum += part;
	  if(part >= FULLSCALE)
	    *lp = 0 | bitmask;
	  else
	    *lp = part | bitmask;
	  bitmask = 0;
	}

        /*
         * The sum of the rescaled contributions for this target pixel must be
         * equal to FULLSCALE. If this is not the case, then there have been
         * rounding errors. Attribute the difference (i.e., FULLSCALE - sum of
         * all parts) to that source pixel that gives the largest contribution
         * to this target pixel - that seems to be the best choice possible.
         */
	if(max_pixellut && newpartsum != FULLSCALE) {
	  int savelut;
	  savelut = *max_pixellut & MAPSCALE;
	  if(savelut == 0)
	    savelut = FULLSCALE;
	  part = savelut + FULLSCALE - newpartsum;
	  if(part >= FULLSCALE)
	    part = 0;
	  if(*max_pixellut & BITMASK)
	    *max_pixellut = part | BITMASK;
	  else
	    *max_pixellut = part;
	}

#if BOUND_CHECK
        /*
         * Test to see if the "start_lut" location still has the BITMASK flag
         * set after the rescaling.
         *
         * Give an error message if this is not the case.
         */
	if(start_lut != lut_tab && start_lut < lut_tab + prog_length &&
	  (*start_lut & BITMASK) == 0)
	  prmsg(ERROR,("cannot be: *start_lut is 0x%x *prog = 0x%x in %d\n",
	    *start_lut,*prog_ptr,prog_ptr - lut_prog));
#endif /* BOUND_CHECK */

        /*
         * Set the start pointer "start_lut" for the next target pixel. Usually
         * it will just be the end pointer "lut_ptr" of the present pixel, but
         * if the next target pixel(s) is (are) empty (INCTARGET flag set),
         * then it (they) will just be skipped.
	 */
	if(prog_ptr != prog_end && *prog_ptr == 0) {
#if BOUND_CHECK
          /*
           * A look-up-table instruction of 0 signals an exception. The only
           * type of exception defined so far is the INCTARGET exception, which
           * can only occur at the beginning of a new sequence. Give an error
           * message if this is not the case.
           */
	  if(*lut_ptr != (INCTARGET | BITMASK))
	    prmsg(ERROR,("cannot be: *lut_ptr is 0x%x\n",*lut_ptr));
	  count_case1++;
#endif /* BOUND_CHECK */
	  start_lut = lut_ptr + 1;           /* Ignore the empty target. */
	} else {
	  start_lut = lut_ptr;
#if BOUND_CHECK
	  count_case2++;
#endif /* BOUND_CHECK */
	}
      }
    }
    /*
     * Print statistics on the target pixels:
     * - count_case      is the number of source pixels that got a 0
     *                   contribution through the rescaling;
     * - count_case1     is the number of empty target pixels;
     * - count_case2 - 1 is the number of the non-empty target pixels
     *                   (-1 as the case prog_ptr == prog_end is counted also)
     */
    prmsg(DMSG,("Counted %d part == 0 (LOOP count %d %d)\n",
      count_case,count_case1,count_case2 - 1));
  }

  /*
   * Step 4: the absolute addressing scheme for the first contributing source
   *         pixel is replaced by relative addressing whenever possible. Where
   *         it is not possible, the flag ABSSRC is set in the corresponding
   *         location in the look-up table. At the end of this step, the source
   *         coordinate table has its final form.
   *
   * At present, the first program instruction for a new target pixel sequence
   * contains always 0x88. This location can be used to store the position of
   * the first source pixel in this target pixel sequence relative to the last
   * source pixel of the previous target pixel.
   *
   * The intention is to obtain the coordinates of the source pixels by
   * specifying the relative distance of the new source pixel with respect to
   * the source pixel treated last. This relative distance will then be stored
   * in the look-up-table program instruction for this source pixel.
   *
   * As the length of a program instruction is fixed (8 bit), there is a
   * maximum of 16 values that can be stored as difference for x and for y.
   * Moreover, the instruction values 0 and 0xff have a special meaning, thus
   * the difference can only have 15 values. These are chosen to be symmetric
   * around 0, i.e. the difference in x and in y must be between -7 and +7.
   *
   * If this maximum difference is exceeded, absolute source coordinates must
   * be used. A 0 will be stored in the look-up-table instruction to mark an
   * exception, and the type of the exception will be ABSSRC.
   *
   * starttidx  contains the number of empty pixels at the beginning of the
   *            target image, i.e. it is the index of the first non-empty pixel
   *            in the target image array.
   * abs_src  contains the x and y coordinates of the source image pixel that
   *          corresponds to the target image pixel being processed.
   *          The array is in principle a two-dimensional pixel array
   *          organized for speed as an one-dimensional array with line-order
   *          (i.e., the x coordinate increases fastest). For each pixel there
   *          are two short integers, the first with the x and the second
   *          with the y coordinate.
   * src_x and src_y  are the x and y coordinates of the source image pixel
   *                  that corresponds to the first non-empty target pixel.
   * startsidx  contains for this source image pixel the index into the source
   *            image array.
   */

  prmsg(DMSG,("Going to relative pixels\n"));

  lut_d->starttidx = start_tidx;
  src_x = abs_src[2 * start_tidx];
  src_y = abs_src[2 * start_tidx + 1];
  lut_d->startsidx = src_x +  XSIZE * src_y;

  /*
   * The first valid (non-empty) target pixel must be treated separately, as
   * for it there is no "last source pixel of the previous target pixel".
   * Mark it in the look-up table as if it was not the beginning of a new
   * sequence.
   * This will leave the instruction 0x88 in the corresponding look-up-table
   * program.
   */
  lut_tab[0] &= MAPSCALE;

  new_abssrc = abs_src;
  abs_ptr = abs_src + 2 * start_tidx + 2;
  count_trg = start_tidx + 1;

  lut_ptr = lut_tab;
  prog_end = lut_prog + prog_length;
  /*
   * Loop through the program and look for the beginnings of new target pixel
   * sequences.
   *
   * If one is found, then the look-up-table program instruction for the first
   * source pixel will be modified as described above.
   *
   * For the other source pixels in the program (subsequent pixels for an
   * already found target pixel), just calculate their source coordinates. Thus
   * (src_x, src_y) will contain the "coordinates of the last source pixel from
   * the previous target pixel".
   */
  for(prog_ptr = lut_prog; prog_ptr != prog_end; prog_ptr++, lut_ptr++){
    instruct = *prog_ptr;
    /*
     * Case 1: source pixel at the beginning of a new target pixel sequence.
     */
    if((part = *lut_ptr) & BITMASK) {
#if BOUND_CHECK
      /*
       * At the beginning of a new target pixel sequence, the look-up-table
       * program instruction must be either 0x88 for a normal sequence or 0 for
       * an exception.
       *
       * If this is not the case, give an error message.
       */
      if(instruct != 0x88  && instruct != 0) {
	prmsg(ERROR,("corrupted tables prog = 0x%x lut = 0x%x in %d\n",
	  instruct,part,prog_ptr - lut_prog));
	continue;
      }
#endif /* BOUND_CHECK */
      new_x = *abs_ptr++;
      new_y = *abs_ptr++;
#if BOUND_CHECK
      count_trg++;
#endif /* BOUND_CHECK */
      /*
       * Skip empty target pixels.
       */
      if(instruct == 0)
	continue;
#if BOUND_CHECK
      /*
       * For non-empty target pixels, the corresponding coordinates in the
       * source coordinate array must have a real value, not any longer the
       * initialization value.
       *
       * If they do, exit with error.
       */
      if(new_x == 0xffff)
	prmsg(FATAL,("no absolute src coordinate for %d in %d\n",
	  count_trg,prog_ptr - lut_prog));
#endif /* BOUND_CHECK */
      diff_x = new_x - src_x;
      diff_y = new_y - src_y;

      /*
       * The first source pixel for this new target pixel might be wrapped
       * around the end of the line with respect to the last source pixel of
       * the previous target pixel, e.g. (new_x,new_y) = (XSIZE - 1, 3) and
       * (src_x, src_y) = (1, 2).
       *
       * Adding or subtracting XSIZE to the x-difference and modifying the
       * y-difference accordingly allows these cases to be treated as described
       * above (maximum difference +-7). This way the use of absolute
       * coordinates can be avoided here as well.
       */
      if(diff_x >= 8) {
	diff_x -= XSIZE;
	diff_y++;
      }
      if(diff_x <= -8) {
	diff_x += XSIZE;
	diff_y--;
      }

      /*
       * Test if the difference between the new and the old source pixel
       * coordinates can be stored in the 8 bits of the look-up-table program
       * instruction, i.e. if it is between -7 and +7 for both x and y.
       *
       * If yes, store the difference in the look-up-table program instruction.
       *
       * If no, then absolute source coordinates must be used. Mark this event
       * as exception of type ABSSRC. Store the x and y source coordinates in
       * two subsequent locations in the source coordinate array. Store the
       * relative contribution of this source pixel to the target pixel also in
       * the source coordinate array, immediately behind the coordinates.
       */
      if(diff_x >= 8  || diff_x <= -8 || diff_y >= 8 || diff_y <= -8) {
	*prog_ptr = 0x0;
	*lut_ptr  = BITMASK | ABSSRC;
	*new_abssrc++ = new_x;
	*new_abssrc++ = new_y;
        /*
         * As the new set of data is written in the same array as the old
         * coordinates, make sure that this does not overwrite old coordinates
         * that have not been processed yet.
         *
         * This is not that likely to happen, as there should be room at the
         * beginning of the source coordinate array due to empty target pixels
         * at the beginning of the image.
         *
         * However, if it does happen, exit with error.
         */
	if(new_abssrc >= abs_ptr)
	  prmsg(FATAL,("about to overwrite source coordinate array\n"));
	*new_abssrc++ = part & MAPSCALE;
	count_abs++;
      } else {
	instruct = (diff_x + 8)  + ((diff_y + 8) << 4);
	*prog_ptr = instruct;
#if BOUND_CHECK
        /*
         * As the differences have been tested to be between -7 and +7, the
         * resulting instruction cannot be 0.
         *
         * If it is, exit with error.
         */
	if(*prog_ptr == 0)
	  prmsg(FATAL,("instruction is 0 - cannot be\n"));
#endif /* BOUND_CHECK */
      }

      src_x = new_x;
      src_y = new_y;
#if BOUND_CHECK
      if(src_x >= XSIZE || src_x < 0 || src_y >= YSIZE || src_y < 0)
	prmsg(DMSG,("src pixel out of bounds in %d a.) %d %d (0x%x 0x%x)\n",
	  prog_ptr - lut_prog,src_x,src_y,instruct,part));
#endif /* BOUND_CHECK */
    } else {
    /*
     * Case 2: source pixel that is not at the beginning of a new target pixel
     * sequence.
     */
      /* ??? can instruct == 0 happen here at all ??? */
      if(instruct != 0) {
	src_x += (instruct & 0x0f) - 8;
	src_y += (((int) (instruct & 0xf0)) >> 4) - 8;
#if BOUND_CHECK
	if(src_x >= XSIZE || src_x < 0 || src_y >= YSIZE || src_y < 0)
	  prmsg(DMSG,
            ("source pixel out of bounds in %d b.) %d %d (0x%x 0x%x)\n",
	    prog_ptr - lut_prog,src_x,src_y,instruct,part));
#endif /* BOUND_CHECK */
      }
    }
  }
#if BOUND_CHECK
  if(count_trg != xysize)
    prmsg(FATAL,("not all target pixels treated - cannot be %d\n",count_trg));
#endif /* BOUND_CHECK */
  prmsg(DMSG,("Treated: %d empty target increments %d abs. source indices\n",
    count_inc,count_abs));

  /*
   * Re-adjust the size of the source coordinate array. It is also possible
   * that this array is no longer needed, because no target pixel needs
   * absolute source coordinates. Then the source coordinate array is just
   * freed.
   */
  if(new_abssrc == abs_src) {
    pfree(abs_src);
    abs_src = lut_d->abs_src = NULL;
    noabs = 0;
  } else {
    if((abs_src = lut_d->abs_src = (unsigned short *)prealloc(abs_src,
      (noabs = new_abssrc - abs_src) * sizeof(short))) == NULL)
      prmsg(FATAL,("realloc fails\n"));
  }

  pfree(offsets);

#if WASTE4_FORSPEED
  /*
   * Save relative integer target position. This is used later for the overflow
   * correction. x_trg and y_trg contain the the target pixel coordinates.
   * We store here the differences in the short arrays lut_d->xrel and
   * lut_d->yrel. (i.e. if the pixel (100, 100) is distorted to position
   * (103.4, 101.6) then x_trg[100] is 103.4, y_trg[100] is 101.6,
   * spd_func(100,100,&x,&y) would return (103.4, 101.6) and lut_d->xrel
   * is 3, and lut_d->yrel is 2.)
   *
   * Note that x_trg, y_trg are floats and lut_d->xrel,yrel are shorts. The
   * reason for that is that we will only need approximate values for the
   * shift to blot out a square around overflow pixels.
   */
  if((lut_d->xrel = (short*)pmalloc(xysize * sizeof(short))) == NULL ||
    (lut_d->yrel = (short*)pmalloc(xysize * sizeof(short))) == NULL)
    prmsg(FATAL,("no memory for xrel and yrel\n"));

  for(idx = 0, j = 0; j < YSIZE; j++)
    for(i = 0; i < XSIZE; i++) {
      /*
       * Note that x_trg and y_trg have dimensions [XSIZE + 1,YSIZE + 1]
       */
      lut_d->xrel[idx] = x_trg[i + j * xsize1] - i;
      lut_d->yrel[idx] = y_trg[i + j * xsize1] - j;
      idx++;
    }
#endif /* WASTE4_FORSPEED */

#if !BOUND_SUPER
  pfree(x_trg);
  pfree(y_trg);
#endif /* !BOUND_SUPER */

  /*
   * Step 5: the relative addressing is replaced by the compressed addressing
   *         scheme whenever possible.
   *
   *         At the end of this step, all tables (look-up table, etc.) have
   *         their final form.
   */
  {
    unsigned char *start_ptr,*end_ptr,*c_ptr;
    int sx,sy,bito,compress,newmask;
    unsigned char *new_prog,*new_p;
    LUT_TYPE *start_lutptr;
    LUT_TYPE *newlut;
    int v;
    int oldidx;
    unsigned int *histo,*histo2;
    int count_hist,hist_max;
    unsigned int seq_value;
    int len,abs,old;
    int *rel_ptr,*rel_tab,**relend_tab;
    int *offset_tab;
    int seq_code;

    prmsg(DMSG,("Compressing program\n"));

    /*
     * At this point, the coordinates of a source pixel that contributes to a
     * given target pixel can be calculated in one of two ways:
     *
     * - relative: if the new source pixel is not too far away from the last
     *             source pixel processed, then the look-up-table program
     *             instruction for this new pixel contains the relative
     *             distance in x and y between the new and the previous
     *             source pixel;
     * - absolute: if the new source pixel is too far away, then the
     *             corresponding look-up-table program instruction contains a 0
     *             to mark an exception. The exception type is ABSSRC, and the
     *             coordinates of the new pixel are in the source coordinate
     *             array.
     *
     * However, in many cases the image will not be strongly distorted. This
     * means that the positions of the source pixels that contribute to a given
     * target pixel will be near that pixel's position. Also, the correction
     * function that maps the source pixel coordinates to the target pixel
     * coordinates is not likely to vary a lot from one pixel to the next.
     *
     * Therefore, the group of source pixels that contribute to a given target
     * pixel is likely to have a similar shape as one moves from one target
     * pixel to the next; or, expressed in a different way, the sequence of
     * source pixel movements is likely to show a similar pattern.
     *
     * This can be used to shorten the look-up-table program. The idea is to
     * define a certain number of these possible movement patterns and then
     * look at all target pixels to find out whether the corresponding source
     * pixels are described by one of the defined patterns. If this is the
     * case, the sequence of look-up-table program instructions that describe
     * the movement from one source pixel to the next can be replaced by a
     * reference to the corresponding pattern. As the hope is to end up with a
     * smaller program, the process is called "compressing the program".
     *
     * The technical implementation of this idea is described in the following.
     *
     * Scan all source pixel sequences in the look-up-table program for short
     * sequences in the immediate neighborhood of the starting source pixel.
     * "Short" is here defined as reaching any combination of the pixels that
     * are in one of 18 locations mainly "below and to the right" of the
     * starting pixel. More precisely, if the starting pixel is marked by "x",
     * then all the pixels marked with a "." in the following diagram can be
     * reached:
     *
     *               x . . .           (the three pixels to the right)
     *             . . . . .           (line below: 1 left to 3 right)
     *             . . . . .           (two lines below: 1 left to 3 right)
     *             . . . . .           (three lines below: 1 left to 3 right)
     *
     * Note that the order in which the pixels are reached is not important.
     * Note also that this is not restricting the general case. The starting
     * pixel is simply defined to be in the upper left corner of the pattern.
     * Because of this definition it is sometimes possible that there is a
     * pixel in the pattern which is one unit more to the left in the next
     * line, but almost never two units.
     *
     * There are (2 power 18) possible pixel sequences that can be constructed
     * with 18 pixels. Each sequence can thus be described unambiguously by an
     * 18-bit sequence mask pattern in that way that the bit "i" is set if the
     * pixel "i" is part of the sequence.
     *
     * Determine for each of these sequences the frequency with which it occurs
     * in the look-up-table program.
     *
     * First allocate (2 power MAXHIST) integers of memory for the frequency
     * counters (MAXHIST = 1 << 18).
     */
    if((histo = (unsigned int *)pmalloc(MAXHIST * sizeof(int))) == NULL)
      prmsg(FATAL,("no memory"));
    memset(histo,0,MAXHIST * sizeof(int));

    /*
     * Loop through the program and search for the the next target pixel (= end
     * point of the present source pixel sequence and starting point of the
     * next sequence). This is marked by the flag BITMASK being set.
     *
     * Note that the first program instruction is already at a target pixel,
     * thus there is a "present pixel sequence" at the start of the loop.
     */
    lut_ptr  = lut_tab;
    prog_end = lut_prog + prog_length;
    start_ptr = lut_prog;
    for(prog_ptr = lut_prog; prog_ptr != prog_end;) {
      start_ptr = prog_ptr;
      start_lutptr = lut_ptr;
      lut_ptr++;
      prog_ptr++;
      while(prog_ptr < prog_end && !(*lut_ptr & BITMASK)) {
	lut_ptr++;
	prog_ptr++;
      }
      /*
       * End of sequence found. Now analyze the pixels involved in the
       * sequence: do they all belong to the 18 positions defined above?
       *
       * If yes, the sequence is one of the short sequences that is being
       * looked for: increase the frequency count of the corresponding counter.
       *
       * If no, stop the analysis of this sequence and look for the next one.
       */
      end_ptr = prog_ptr - 1;
      compress = -1;
      if(start_ptr <= end_ptr) {
	sx = 0; sy = 0; compress = 0;
	for(c_ptr = start_ptr + 1; c_ptr <= end_ptr; c_ptr++) {
	  instruct = *c_ptr;
	  sx += (instruct & 0x0f) - 8;
	  sy += (((int)(instruct & 0xf0)) >> 4) - 8;
	  bito = (sy == 0) ? (sx - 1) : sy * 5 - 1 + sx; /* 0..17 */
	  if(sx < -1 || sx > 3 || sy < 0 || sy > 3 || bito < 0 || bito > 17
	    || (sy == 0 && sx < 1)) {
	    /*
             * It is not one of the short sequences. We cannot compress this
             * series.
             */
	    compress = -1;
	    break;
	  }
	  compress |= 1 << bito;
	}
      }

      /*
       * Ignore target pixels which have no corresponding source points (empty
       * source pixel sequence).
       */
      if(*start_ptr == 0 && (*start_lutptr & INCTARGET))
	continue;
#if BOUND_CHECK
      if(compress >= MAXHIST)
	prmsg(FATAL,("compress out of bounds\n"));
#endif /* BOUND_CHECK */
      if(compress >= 0)
	histo[compress]++;
    }

    /*
     * histo holds now for every possible sequence pattern the number of
     * occurrences of this pattern. Determine the total number of patterns that
     * are actually used.
     */

    count_hist = 0;
    for(i = 0; i < MAXHIST; i++)
      if(histo[i])
	count_hist++;

    /*
     * Allocate array histo2 and fill it with two pieces of information for
     * each sequence pattern found:
     * 1) the bitfield of the pattern;
     * 2) the number of occurrences for non 0 patterns.
     *
     * Then sort the array in descending order of the number of occurrences and
     * keep only the first 256 patterns (i.e., those 256 patterns that occur
     * most frequently).
     */
    if((histo2 = (unsigned int *)pmalloc(count_hist * 2 * sizeof(int))) == NULL)
	prmsg(FATAL,("no memory"));

    for(j=0, i = 0; i < MAXHIST; i++)
      if(histo[i]) {
	histo2[j++] = i;
	histo2[j++] = histo[i];
      }

    qsort(histo2,count_hist,2 * sizeof(int),histcompare_count);

    /*
     * Throw away excess patterns (if there are more than 256), and readjust
     * the total count of the number of patterns.
     */
    if(count_hist > 256)
      if((histo2 = (unsigned int *)prealloc(histo2,sizeof(int) * 2 * 256)) ==
        NULL) {
	prmsg(FATAL,("realloc failed\n"));
      }

    hist_max = count_hist > 256 ? 256 : count_hist;

    /*
     * Reuse the array histo2: the number of occurrences is no longer needed
     * and is replaced by the order number of the field. Remember that the
     * order in histo2 is the result of it being sorted in number of
     * occurrences, i.e., the sequence pattern with the highest number of
     * occurrences has order number 0, the one with the next highest number of
     * occurrences has order number 1, etc.
     *
     * histo2 thus contains now for each sequence pattern:
     * 1) the bitfield of the pattern;
     * 2) the order number of the pattern.
     */
    for(i=0; i<hist_max; i++)
      histo2[2 * i + 1] = i;

    /*
     * We build the relative transition tables here: offset_tab, rel_tab and
     * relend_tab.
     *
     * offset_tab contains the offsets for the 256 pixel positions around the
     * "present pixel", from the "upper left" pixel at offset (-8 - 8 * XSIZE)
     * to the "lower right" pixel at offset (7 + 7 * XSIZE).
     *
     * In other words, offset_tab[0] contains the pixel that is 8 rows to the
     * left and 8 lines above the present pixel. The subsequent elements then
     * move in this upper line to the right until offset_tab[15], which
     * contains the pixel that is 7 rows to the right and (still) 8 lines
     * above the present pixel. offset_tag[16] then moves one line further
     * down and back to the leftmost row: 8 rows to the left and 7 lines above
     * the present pixel. This continues like that, and offset_tab[255]
     * finally contains the pixel that is 7 rows to the right and 7 rows below
     * the present pixel.
     *
     * rel_tab contains sequences of predefined movements around the present
     * pixel. A sequence can be at most 18 movements long and can reach the 18
     * pixels that are mainly "below and to the right" of the present pixel.
     * More precisely, if the present pixel is marked by "x", then all the
     * pixels marked with a "." in the following drawing can be reached:
     *
     *    ----> x      x . . .         (the three pixels to the right)
     *    |          . . . . .         (line below: 1 left to 3 right)
     *    |          . . . . .         (two lines below: 1 left to 3 right)
     *    V y        . . . . .         (three lines below: 1 left to 3 right)
     *
     * The element 0 of each sequence contains the position of the next pixel
     * relative to the present pixel. All subsequent elements of the sequence
     * then contain the position of the next pixel relative to the previous
     * one.
     *
     * relend_tab contains the actual length for each sequence stored in
     * rel_tab in the following way:
     *
     *   for the sequence starting in rel_tab[i << RELTABSH]
     *
     *   relend_tab[i] = pointer to the end location of that sequence in
     *                   rel_tab
     */
    offset_tab =  (int *)pmalloc(sizeof(int) * 256);
    rel_tab =  (int *)pmalloc(sizeof(int *) * (hist_max << RELTABSH));
    relend_tab = (int **)pmalloc(sizeof(int **) * hist_max);

    for(i=0; i<256; i++)
      offset_tab[i] = ((i & 0x0f) - 8) + (((i & 0xf0) >> 4) - 8) * XSIZE;

    for(i=0; i < hist_max; i++) {
      rel_ptr = rel_tab + (i << RELTABSH);
      old = 0;
      len = 0;
      seq_code = histo2[2*i];
      for(j = 0; j< 18; j++)
	if(seq_code & (1<<j)) {
	  len++;
	  abs = (j < 3) ? j + 1 : (j - 3) % 5 - 1 + ((j - 3) / 5 + 1) * XSIZE;
	  *rel_ptr++ = abs - old;
	  old = abs;
	}
      relend_tab[i] = rel_tab + (i << RELTABSH) + len;
    }

    lut_d->relend_tab = relend_tab;
    lut_d->rel_tab = rel_tab;
    lut_d->offset_tab = offset_tab;

    qsort(histo2,hist_max,2 * sizeof(int),histcompare_idx);

    prmsg(DMSG,("Sequence histogrammed\n"));

    /*
     * Later on we want to find for an arbitrary compress value if we should
     * compress this sequence and what is the code (from 0 to 254). To avoid to
     * loop through the histo2 array at every sequence (and therefore at every
     * pixel) we set histo up in a way to simply do
     *         seq_value = histo[compress];
     *
     * The "qsort" call sorts "histo2" in ascending bitfield order number. This
     * avoids having to use 1 MB of memory (1 << MAXHIST integers = 4 * 256 KB)
     * and makes the filling of the table "histo" faster.
     */
    memset(histo,0,MAXHIST * sizeof(int));
    for(i = 0; i< hist_max; i++)
      histo[histo2[2*i]] = histo2[2*i+1]+1;
    /*
     * The second element of each "histo2" field holds the bitfield order
     * number --> histo holds for every bitfield "order number + 1" or 0 if
     * not set.
     */

    /*
     * Now use the above defined sequences of predefined movements to compress
     * the look-up-table program.
     *
     * Go through the program and see for every sequence of movements if it is
     * one of the predefined ones. If so, then replace the program instructions
     * for this sequence by a single one giving the corresponding index into
     * the rel_tab table ("compress the sequence").
     *
     * A new look-up-table program is thus built that will replace the old one.
     *
     * First allocate memory for the new look-up-table program.
     */

    if((new_prog = new_p = (unsigned char *)pmalloc(prog_length * sizeof(char)))
      == NULL)
      prmsg(FATAL,("no memory"));

    lut_ptr  = lut_tab;
    prog_end = lut_prog + prog_length;
    new_plen = prog_length;
    prmsg(DMSG,("Current prog length 1: %d\n",prog_end - lut_prog));
    for(i=0; i < MAXSTAT; i++)
      stat_case[i] = 0;
    for(prog_ptr = lut_prog; prog_ptr != prog_end;) {
      /*
       * Here we are at the start of a target pixel sequence. Find the end of
       * the sequence, i.e. the beginning of the next target pixel (or the end
       * of the look-up-table program).
       */
      start_ptr = prog_ptr;
      start_lutptr = lut_ptr;
      lut_ptr++;
      prog_ptr++;
      stat_case[0]++;
      while(prog_ptr < prog_end && !(*lut_ptr & BITMASK)) {
	lut_ptr++;
	prog_ptr++;
	stat_case[1]++;
      }
      end_ptr = prog_ptr - 1;
      /*
       * start_ptr points to the start of the target pixel sequence, end_ptr to
       * the end.
       */

      compress = -1;
      if(start_ptr <= end_ptr) {
	oldidx = 0; sx = 0; sy = 0; compress = 0;

	/*
         * Run with the pointer c_ptr through the target pixel sequence to
         * evaluate the relative moves.
         */
	for(c_ptr = start_ptr + 1; c_ptr <= end_ptr; c_ptr++) {
	  stat_case[2]++;
	  instruct = *c_ptr;
	  sx += (instruct & 0x0f) - 8;
	  sy += (((int) (instruct & 0xf0)) >> 4) - 8;

	  /*
           * Find out which bit (0..17) to set for this pixel in the sequence
           * mask.
           */
	  bito = (sy == 0) ? (sx - 1) : sy * 5 - 1 + sx;
	  if(sx < -1 || sx > 3 || sy < 0 || sy > 3 || bito < 0 || bito > 17
             || (sy == 0 && sx < 1)) {
	    /*
             * We cannot compress this sequence, as it does not fit the allowed
             * patterns (see description above).
             */
	    compress = -1;
	    stat_case[3]++;
	    break;
	  }
#if BOUND_CHECK
          /*
           * Test if the new pixel to be added is already contained in the
           * sequence mask. If yes, give error message.
           */
	  newmask = 1 << bito;
	  if(compress & newmask)
	    prmsg(FATAL,("source pixel referenced twice - cannot be\n"));
          /*
           * Test if the new pixel has a smaller array index for the source
           * image than the previous one. If yes, give error message.
           */
	  if(oldidx >= sx + sy * XSIZE)
	    prmsg(FATAL,("source not sorted - cannot be\n"));
	  oldidx = sx + sy * XSIZE;
#endif /* BOUND_CHECK */
	  /*
           * Build the sequence bitmask for all contributing source pixels.
           */
	  compress |= 1 << bito;
	}
      }

      /*
       * Test if the new program is about to overflow the allocated buffer.
       * If so, give warning and increase buffer size.
       */
      if(new_p > new_prog + new_plen - 20) {
	prmsg(WARNING,
	  ("allocated buffer for compressed program to small - increasing\n"));
	new_plen += prog_length;
	old = new_p - new_prog;
	if((new_prog = (unsigned char *)prealloc(new_prog,
	  new_plen * sizeof(char))) == NULL)
	  prmsg(FATAL,("no memory"));
	new_p = new_prog + old;
      }

      /*
       * If the look-up-table program instruction = 0 and the look-up-table
       * contains INCTARGET, then we have a target pixel without corresponding
       * source pixels.
       *
       * Set up an exception sequence in the look-up-table program:
       * - the first element of the sequence contains 0 to indicate an
       *   exception;
       * - the second element contains the type of exception, here INCTARGET.
       *
       * Then jump to the end of the "compress" loop.
       */
      if(*start_ptr == 0 && (*start_lutptr & INCTARGET)) {
	stat_case[4]++;

	/*
         * There should be only one element in the look-up-table program
         * -> no compress anyway.
         *
         * If there is more than one element, exit with error.
         */
#if BOUND_CHECK
	if(end_ptr != start_ptr)
	  prmsg(FATAL,("table is corrupted somehow\n"));
#endif /* BOUND_CHECK */
	*new_p++ = 0;
	*new_p++ = INCTARGET;
	continue;
      }

      /*
       * This is a sequence that can possibly be compressed. Get the
       * corresponding index into the rel_tab table. If it is 0, then the
       * sequence corresponds to one of those that were not included in the 256
       * possible entries of the rel_tab table - this should be a rare case.
       */
      if(compress >= 0) {
	seq_value = histo[compress];    /* get the index for this sequence */
	stat_case[5]++;
	if(seq_value == 0) {
	  compress = -1;
	  stat_case[6]++;
	} else {
	  seq_value--;                  /* restore "true" index */
#if BOUND_CHECK
	  if(seq_value >= hist_max || seq_value < 0)
	    prmsg(FATAL,("seq value > 256 cannot be\n"));
#endif /* BOUND_CHECK */
	}
      }

      if(compress >= 0) {
        /*
         * Now it is certain that this sequence can be compressed. There are
         * two possible cases:
         *
         * - the first pixel of the sequence is "near" the source pixel that
         *   was processed last (i.e., it is in one of the 256 positions
         *   around the last one).
         *
         *   Then the look-up-table program instruction is != 0 and contains
         *   the index into the offset table lut_d->offset_tab where the
         *   relative address of the first source image pixel can be found.
         *   This is the "normal" case;
         *
         * - the first pixel of the sequence is not "near" the last source
         *   pixel. Then the look-up-table program instruction is 0 to indicate
         *   an exception, the following instruction will contain ABSSRC to
         *   indicate the type of exception, and the address of the first
         *   source image pixel is in the table lut_d->abs_src.
         *
         * In both cases, the then next program instruction will contain the
         * index that points to the corresponding predefined sequence in the
         * lut_d->rel_tab table.
         */
	stat_case[7]++;
	*new_p++ = *start_ptr;
	if(*start_ptr == 0) {
	  *new_p++ = ABSSRC;
	  stat_case[8]++;
	}
	*new_p++ = seq_value & 0xff;
      } else {
        /*
         * This sequence cannot be compressed. There are the same two
         * possibilities for the address of the first pixel as in the
         * compressed case above:
         *
         * - it is near the last pixel processed. Then the present lookup-table
         *   instruction is != 0 and contains the index into the offset table.
         *
         *   This instruction will be replaced by a 0 to indicate an exception,
         *   followed by UNCOMPRESSED to indicate the type of exception, and
         *   then followed by the old "present" instruction (i.e., by the index
         *   into the offset table);
         *
         * - it is not "near" the last pixel. Then the look-up-table program
         *   instruction is 0 to indicate an exception, the following
         *   instruction will contain both the flags ABSSRC and UNCOMPRESSED to
         *   indicate the type of exception, and the address of the first source
         *   image pixel is in the absolute address table lut_d->abs_src.
         *
         * In both cases, the then following program instructions contain the
         * indices into the offset table lut_d->offset_tab where the relative
         * address of the following source image pixels can be found. This
         * sequence of source image pixels is ended by a program instruction
         * containing 0.
         */
	stat_case[9]++;
	if(*start_ptr == 0) {
	  stat_case[10]++;
	  *new_p++ = *start_ptr;
	  *new_p++ = ABSSRC | UNCOMPRESSED;
	} else {
	  stat_case[11]++;
	  *new_p++ = 0;
	  *new_p++ = UNCOMPRESSED;
	  *new_p++ = *start_ptr;
	}
	for(c_ptr = start_ptr + 1; c_ptr <= end_ptr; c_ptr++) {
	  stat_case[12]++;
	  *new_p++ = *c_ptr;
	}
	*new_p++ = 0; /* End uncompressed data with 0 */
      }
    }  /* End of the compression loop. */

    /*
     * Print statistical information on the compression:
     *  0: target pixels processed (instructions starting a new target pixel
     *     sequence)
     *  1: other contributing source pixel parts processed (instructions not
     *     starting a new target pixel sequence)
     *  2: total length of all source pixel sequences
     *  3: source sequences that cannot be compressed as they exceed the 18
     *     allowed positions
     *  4: target pixels with no corresponding source pixels
     *  5: source sequences that could be compressed as they are contained in
     *     the 18 allowed positions
     *  6: source sequences that could be compressed, but are not among the
     *     256 most frequent sequences and are thus not compressed
     *  7: source sequences that will be compressed
     *  8: source sequences that will be compressed and where the first source
     *     pixel is not near the last source pixel (exception ABSSCR)
     *  9: source sequences that will not be compressed
     * 10: source sequences that will not be compressed and where the first
     *     source pixel is not near the last source pixel (exceptions ABSSCR
     *     and UNCOMPRESSED)
     * 11: source sequences that will not be compressed and where the first
     *     source pixel is near the last source pixel (exception UNCOMPRESSED)
     * 12: total length of all UNCOMPRESSED source pixel sequences
     *
     * Note: the following relations should hold:
     *
     * - stat_case[0] = stat_case[3] + stat_case[4] + stat_case[5];
     * - stat_case[5] = stat_case[6] + stat_case[7];
     * - stat_case[9] = stat_case[10] + stat_case[11];
     * - stat_case[0] + start_tidx (from "Step 2") = output image size;
     * - stat_case[0] = count_case1 + count_case2 (both from "Step 2");
     * - stat_case[1] = count_case (from "Step 2")
     * - stat_case[4] = count_case1 (from "Step 2");
     * - stat_case[3] + stat_case[5] = count_case2 (from "Step 2");
     *
     * - new program length: new_p - new_prog =
     *       2*stat_case[4] + 2*stat_case[7] + stat_case[8] + stat_case[9] +
     *       2*stat_case[10] + 3*stat_case[11] + stat_case[12];
     */
    prmsg(DMSG,("Compress inter.: %d targ pix %d oth src part %d len src seq\n",
      stat_case[0],stat_case[1],stat_case[2]));
    prmsg(DMSG,("                 %d uncompr %d targ inc\n",
      stat_case[3],stat_case[4]));
    prmsg(DMSG,("     compressed: %d poss %d not freq %d real %d ABSSCR\n",
      stat_case[5],stat_case[6],stat_case[7],stat_case[8]));
    prmsg(DMSG,("   uncompressed: %d tot %d ABSSCR %d near %d total length\n",
      stat_case[9],stat_case[10],stat_case[11],stat_case[12]));

    /*
     * The look-up table entry corresponding to an "exception" instruction is
     * no longer needed and can be removed.
     *
     * Also, the BITMASK bit in the look-up table is no longer needed. The
     * value FULLSCALE had before been put as an artificial 0 in the table (to
     * avoid having a value creating the BITMASK meaning "start of a new
     * sequence").
     * Now the value FULLSCALE can be stored correctly in the look-up table.
     */
    prmsg(DMSG,("Current prog length 2: %d\n",prog_end - lut_prog));
    lut_ptr = newlut = lut_tab;
    for(prog_ptr = lut_prog; prog_ptr != prog_end; lut_ptr++, prog_ptr++) {
      if(*prog_ptr == 0)
	continue;
      v = *lut_ptr & MAPSCALE;
      *newlut++ = v ? v : FULLSCALE;
    }

    prmsg(DMSG,("Current prog length 3: %d\n",new_p - new_prog));
    count_inc = 0;
    for(i = 0; i < MAXSTAT; i++)
      stat_case[i] = 0;
    /*
     * Loop over the program.
     * 
     * Write the modified version over the old uncompressed one, which is no
     * longer needed. Normally the compressed version is smaller than the
     * uncompressed, thus this works fine. If not, provide a bigger buffer here.
     *
     * Also, modify the processing of target pixels without corresponding source
     * pixel. Up to this point each such target pixel has its own INCTARGET
     * exception. If there are successive target pixels without source pixel,
     * these sequence of exceptions will now be replaced by one single
     * "multipixel" exception.
     *
     * Finally, add a PROGEND exception at the end of the program.
     */
    if(prog_length < new_plen) {
      pfree(lut_prog);
      if((lut_prog = (unsigned char *)pmalloc(new_plen * sizeof(char))) == NULL)
	prmsg(FATAL,("no memory"));
    }
    for(prog_ptr = new_prog, new_inc = lut_prog; ;) {

      /*
       * Test for the end of a target advance sequence, and create the
       * corresponding exception if necessary.
       *
       * Target advance sequences are started by an "INCTARGET" exception, and
       * then the chain of uninterrupted following "INCTARGET" exceptions is
       * just counted. 
       *
       * The end of such a sequence is reached when something else than a target
       * advance instruction is encountered, or when the counter for multiple
       * target instructions has reached its maximum (0xffff for an unsigned
       * short).
       */
      if(prog_ptr >= new_p || *prog_ptr != 0 || !(*(prog_ptr + 1) & INCTARGET)
        || count_inc == 0xffff) {
	/*
         * End of target advance sequence.
	 *
	 * Terminate this target advance sequence properly before proceeding
         * further:
	 *
	 * - if it is a single pixel target advance, add a 0 instruction to
	 *   the program for an exception and then add an instruction with the
	 *   exception type INCTARGET;
	 *
	 * - if there is a target advance with several pixels, add a 0
	 *   instruction to the program for an exception, add an instruction
	 *   with the exception type flags INCTARGET and MULTIINC set, and
	 *   then add the number of pixels concerned in the next two
	 *   instructions (LSB first).
	 */
	if(count_inc > 1) {
	  stat_case[2]++;
	  *new_inc++ = 0;
	  *new_inc++ = INCTARGET | MULTIINC;
	  *new_inc++ = count_inc % 256;
	  *new_inc++ = count_inc / 256;
	  count_inc = 0;
	} else if(count_inc == 1) {
	  stat_case[3]++;
	  *new_inc++ = 0;
	  *new_inc++ = INCTARGET;
	  count_inc = 0;
	}

        /*
         * Terminate loop if end of program reached.
         */
        if(prog_ptr >= new_p)
          break;
      }

      /*
       * Now investigate the new instruction.
       *
       * Treat all exceptions (value of prog_ptr == 0) first.
       */
      if(*prog_ptr == 0) {
        if(*(prog_ptr + 1) & INCTARGET) {
	  /*
	   * Found a target advance instruction. Count the number of successive
           * occurrences, and skip the next two instructions (0 for exception
           * and INCTARGET for exception type).
	   */
 	  stat_case[1]++;
	  count_inc++;
	  prog_ptr += 2;

        } else if(*(prog_ptr + 1) & UNCOMPRESSED) {
          /*
           * Not a predefined source pixel sequence.
           *
           * Copy the present instruction (0 for exception), and all following
           * instructions up to and including the 0 that terminates the
           * uncompressed sequence.
           */
	  stat_case[4]++;
	  *new_inc++ = *prog_ptr++;
	  while(*new_inc++ = *prog_ptr++)
	    stat_case[5]++;

	} else if(*(prog_ptr + 1) & ABSSRC) {
          /*
           * Predefined source pixel sequence and first pixel is not near the
           * last one processed.
           *
           * Just copy the next three instructions (0 for exception, ABSSRC for
           * exception type and the index for the absolute address table.
           */
	  stat_case[6]++;
	  *new_inc++ = *prog_ptr++;
	  *new_inc++ = *prog_ptr++;
	  *new_inc++ = *prog_ptr++;
	}

      } else {
        /*
         * Normal case (i.e. no exception): predefined source pixel sequence and
         * first pixel near the last one processed.
         *
         * Just copy the next two instructions (offset index for first pixel
         * and sequence index).
         */
	stat_case[7]++;
	*new_inc++ = *prog_ptr++;
	*new_inc++ = *prog_ptr++;
      }
    }

    /*
     * End of the old program reached.
     *
     * Print statistical information on the final state of compression:
     * 1: target pixels with no corresponding source pixels (target advances)
     * 2: multipixel target advance sequences
     * 3: single pixel target advances
     * 4: source sequences that will not be compressed
     * 5: total length of uncompressed program sequences
     * 6: compressed source sequences where the first source pixel is not near
     *    the last source pixel (exception ABSSCR)
     * 7: compressed source sequences where the first source pixel is near the
     *    last source pixel
     *
     * The following relations should hold between the new stat_case values
     * (left side) and the ones calculated previously (right side):
     *
     * stat_case[1] = stat_case[4]
     * stat_case[4] = stat_case[9]
     * stat_case[5] = stat_case[10] + 2 * stat_case[11] + stat_case[12]
     * stat_case[6] = stat_case[8]
     * stat_case[7] = stat_case[7] - stat_case[8]
     */
    prmsg(DMSG,
      ("Compress final: %d total inc %d multi inc %d single inc\n",
      stat_case[1],stat_case[2],stat_case[3]));
    prmsg(DMSG,("                %d uncompr %d total length uncompr\n",
      stat_case[4],stat_case[5]));
    prmsg(DMSG,("                %d compr ABSSCR %d compr near\n",
      stat_case[6],stat_case[7]));
    /*
     * End of the compression.
     *
     * Mark the end of the look-up-table program by an exception of type
     * PROGEND.
     *
     * Re-allocate the tables for the look-up-table program and the
     * look-up-table (they are smaller now), then free the auxiliary buffers.
     */
    prog_length = new_inc - lut_prog + 2;
    prmsg(DMSG,("Current prog length 4: %d\n",prog_length));

    lut_prog[prog_length-2] = 0;
    lut_prog[prog_length-1] = PROGEND;

    if((prog_ptr = lut_prog = (unsigned char *)
      prealloc(lut_prog,sizeof(char) * prog_length)) == NULL) {
      prmsg(FATAL,("realloc failed\n"));
    }

    lutsize = newlut - lut_tab;
    if((lut_ptr = lut_tab = (LUT_TYPE *)
      prealloc(lut_tab,sizeof(LUT_TYPE) * lutsize)) == NULL) {
      prmsg(FATAL,("realloc failed\n"));
    }

    pfree(new_prog);
    pfree(histo);
    pfree(histo2);

  }

#if BOUND_CHECK
  /*
   * Make a dry run to check for source and target pixel boundaries as well as
   * inconsistencies in the look-up table or the look-up-table program.
   */
  {
    register unsigned char compress,instruct;
    register int *r_ptr,*rel_tab,**relend_tab;
    register int cor_p,src_p;
    unsigned char *prog_ptr = lut_prog;
    unsigned char *lastprog = lut_prog + prog_length - 1;
    unsigned short *abs_ptr = abs_src;
    int *offset_tab;
    int count_abs1 = 0,count_abs2 = 0,count_unc = 0;
    unsigned long count_inc = 0,count_exc = 0,count_abs = 0;
    unsigned long multi,count_multi = 0,count_multi_t = 0;
    LUT_TYPE *lut_ptr = lut_tab;
    LUT_TYPE *lastlut = lut_tab + lutsize;

    lut_ptr = lut_tab;

    offset_tab = lut_d->offset_tab;
    rel_tab = lut_d->rel_tab;
    relend_tab = lut_d->relend_tab;

    src_p = lut_d->startsidx;
    cor_p = lut_d->starttidx;

    for(;;) {
      instruct = *prog_ptr++;
      if(instruct != 0) {
        /*
         * Begin of a normal target pixel, i.e. one with a compressed source
         * pixel sequence.
         *
         * Test if the first source pixel is outside the image boundaries, or if
         * there are attempts to exceed the range of the look-up table and the
         * look-up-table program.
         */
	src_p += offset_tab[instruct];
	lut_ptr++;
	if(src_p > xysize || src_p < 0)
	  prmsg(FATAL,("table corrupted 1 src out of bounds\n"));
	if(lut_ptr > lastlut)
	  prmsg(FATAL,("table corrupted 1 lut out of bounds\n"));
	if(prog_ptr > lastprog)
	  prmsg(FATAL,("table corrupted 1 prog out of bounds\n"));
#if BOUND_SUPER
	{
          /*
           * Test if the corrected target pixel coordinates agree with the ones
           * obtained from the target pixel index (within rounding errors).
           */
	  float x = x_trg[src_p % XSIZE + (src_p / XSIZE) * xsize1];
	  float y = y_trg[src_p % XSIZE + (src_p / XSIZE) * xsize1];

	  if(fabs(x - cor_p % XSIZE) > 1 || fabs(y - cor_p / XSIZE) > 1)
	    prmsg(DMSG,("a.) %3d %3d %3d %3d %3d %3d %3f %3f\n",
	      src_p % XSIZE,src_p / XSIZE,cor_p % XSIZE,cor_p / XSIZE,
	      src_p % XSIZE - cor_p % XSIZE,src_p / XSIZE - cor_p / XSIZE,x,y));
	}
#endif /* BOUND_SUPER */
        /*
         * Go through the subsequent source pixels of this sequence and do the
         * same tests as for the first source pixel.
         */
	compress = *prog_ptr++;
	for(r_ptr = rel_tab + (compress << RELTABSH);
	  r_ptr < relend_tab[compress]; r_ptr++) {
	  src_p += *r_ptr;
	  lut_ptr++;
	  if(src_p > xysize || src_p < 0)
	    prmsg(FATAL,("table corrupted 2 src out of bounds\n"));
	  if(cor_p > xysize || cor_p < 0)
	    prmsg(FATAL,("table corrupted 2 cor out of bounds\n"));
	  if(lut_ptr > lastlut)
	    prmsg(FATAL,("table corrupted 2 lut out of bounds\n"));
	  if(prog_ptr > lastprog)
	    prmsg(FATAL,("table corrupted 2 prog out of bounds\n"));
	}
      } else {
        /*
         * We have to treat an exception.
         */
	count_exc++;
	if(prog_ptr > lastprog)
	  prmsg(FATAL,("table corrupted 3 prog out of bounds\n"));
	instruct = *prog_ptr++;

        /*
         * Exception of type "empty target pixel" (one or several).
         *
         * Test if the target pixels are within the image boundaries.
         */
	if(instruct & INCTARGET) {
	  if(instruct & MULTIINC) {
	    count_multi++;
	    multi = *prog_ptr++;
	    multi += *prog_ptr++ * 256;
	    count_multi_t += multi;
	    cor_p += multi;
	  } else {
	    count_inc++;
	    cor_p++;
	  }
	  if(cor_p > xysize || cor_p < 0)
	    prmsg(FATAL,("table corrupted 3 cor out of bounds\n"));
	  continue;
	}

        /*
         * Exception of type "absolute source pixel address" (for compressed or
         * uncompressed pixel sequence).
         *
         * Test if the source pixels are within the image boundaries, or if
         * there are attempts to exceed the range of the look-up table.
         */
	if(instruct & ABSSRC) {
	  int x = *abs_ptr++;
	  int y = *abs_ptr++;
	  count_abs++;
	  src_p = (x + y * XSIZE);
	  abs_ptr++;
	  if(src_p > xysize || src_p < 0)
	    prmsg(FATAL,("table corrupted 3 src out of bounds\n"));

	  if(instruct & UNCOMPRESSED) {
	    count_abs1++;
	    while(instruct = *prog_ptr++) {
	      src_p += offset_tab[instruct];
	      lut_ptr++;
	      if(src_p > xysize || src_p < 0)
		prmsg(FATAL,("table corrupted 4 src out of bounds\n"));
	      if(lut_ptr > lastlut)
		prmsg(FATAL,("table corrupted 4 lut out of bounds\n"));
	    }
	  } else {
	    count_abs2++;
	    compress = *prog_ptr++;
	    for(r_ptr = rel_tab + (compress << RELTABSH);
	      r_ptr < relend_tab[compress]; r_ptr++) {
	      src_p += *r_ptr;
	      lut_ptr++;
	      if(src_p > xysize || src_p < 0)
		prmsg(FATAL,("table corrupted 5 src out of bounds\n"));
	      if(lut_ptr > lastlut)
		prmsg(FATAL,("table corrupted 5 lut out of bounds\n"));
	    }
	  }
	} else if(instruct & UNCOMPRESSED) {
        /*
         * Exception of type "uncompressed source pixel sequence" (with
         * relative pixel addressing).
         *
         * Test if the source pixels are within the image boundaries, or if
         * there are attempts to exceed the range of the look-up table.
         */

	  count_unc++;
	  while(instruct = *prog_ptr++) {
	    src_p += offset_tab[instruct];
	    lut_ptr++;
	    if(src_p > xysize || src_p < 0)
	      prmsg(FATAL,("table corrupted 6 src out of bounds\n"));
	    if(lut_ptr > lastlut)
	      prmsg(FATAL,("table corrupted 6 lut out of bounds\n"));
	  }
	} else if(instruct & PROGEND)
	  break;
      }
      cor_p++;

      /*
       * The target pixel index must not exceed the image boundaries.
       */
      if(cor_p > xysize || cor_p < 0)
	prmsg(FATAL,("table corrupted 6 cor_p out of bounds\n"));
    }

    /*
     * At the end, the target pixel index should point to the last pixel in the
     * image.
     */
    if(cor_p != xysize)
      prmsg(FATAL,("missed some target pixels, %d\n",cor_p));

    /*
     * There must be three entries for each pixel that is in the source
     * coordinate array (but not all pixels need to be in this array!).
     */
    if(count_abs != noabs/3)
      prmsg(FATAL,("missed some absolute coord, %d != %d\n",count_abs,noabs/3));

    /*
     * Print statistics on the exceptions.
     */
    prmsg(DMSG,
      ("Exceptions: %d (%d absolute source: %d uncompr., %d compr.),\n",
      count_exc,count_abs,count_abs1,count_abs2));
    prmsg(DMSG,
      ("            %d single inc, %d multi inc (with %d inc), %d uncompr.)\n",
      count_inc,count_multi,count_multi_t,count_unc));

  }
#endif /* BOUND_CHECK */

  print_memsize();

  lut_d->prog_length = prog_length;
  lut_d->prog = lut_prog;
  lut_d->lut = lut_tab;

#if BOUND_SUPER
  pfree(x_trg);
  pfree(y_trg);
#endif /* BOUND_SUPER */

  return(lut_d);
} /* lut_calc */

/*==============================================================================
 * Prints for debugging purposes various pieces of information from the look-up
 * table, the look-up table program and the source coordinate array.
 *
 * Printed are, for a selectable section of the look-up-table program, the
 * following items:
 * - the program instruction and the content of the look-up-table;
 * - the target pixel coordinates;
 * - the coordinates of the contributing source pixels and their area
 *   contributions to the target pixel.
 *
 * The section to be printed is selected by the input variables "start" and
 * "end".
 *
 * This routine is at present not called from within the program, but can be
 * called from the debugger.
 *
 * Note that this routine expects an "intermediate state" of the look-up table
 * and associated program, i.e. the state when the exceptions and the beginning
 * of a new target pixel sequence are still marked in the look-up table and not
 * in the look-up-table program.
 *
 * Input : prog:        the look-up-table program
 *         lut:         the look-up table
 *         asrc:        the source coordinate array
 *         prog_length: the length of the look-up-table program
 *         startsidx:   the pixel index of the first source pixel
 *         starttidx:   the pixel index of the first target pixel
 *         start:       the sequence number (= array index) of the lookup-table
 *                      program instruction where the printing is to start
 *         end:         the corresponding number for the end of printing
 * Output: none
 * Return: 0
 */

int debug_print(unsigned char *prog,LUT_TYPE *lut,unsigned short *asrc,
  int prog_length,int startsidx,int starttidx,int start,int end)
{
  LUT_TYPE *lut_ptr = lut;
  unsigned char* prog_ptr = prog;
  int i;
  unsigned char instruct;
  int trg_x,trg_y;
  int src_x, src_y;
  int tidx,part;
  unsigned short *abs_ptr = asrc;

  tidx = starttidx;
  src_x = startsidx % XSIZE;
  src_y = startsidx / XSIZE;

  for(i = 0; i < prog_length; i++, prog_ptr++, lut_ptr++) {
    /*
     * Print look-up-table program instruction and look-up table content.
     */
    if(i >= start && i <= end)
      prmsg(MSG,("0x%2x 0x%4x :",*prog_ptr,*lut_ptr));
    if((part = *lut_ptr) & BITMASK) {
      tidx++;
      if(*prog_ptr == 0 && (*lut_ptr & ABSSRC)) {
	src_x = *abs_ptr++;
	src_y = *abs_ptr++;
	instruct = 0x88;
	part  = *abs_ptr++;
      }
      trg_x = tidx % XSIZE;
      trg_y = tidx / XSIZE;
      /*
       * Print target pixel coordinates.
       */
      if(i >= start && i <= end)
	prmsg(MSG,("%4d %4d :",trg_x,trg_y));

      if(*prog_ptr == 0 && *lut_ptr & INCTARGET) {
        /*
         * Print source pixel coordinates for an empty target.
         * ??? they can not be very meaningful. Is this only to make sure that
         * they did not get messed up ???
         */
	part = 0;
	if(i >= start && i <= end)
	  prmsg(MSG,("%4d %4d 0x%4x\n",src_x,src_y,part));
	continue;
      }

      part &= MAPSCALE;
    } else
      if(i >= start && i <= end)
	prmsg(MSG,("           "));

    instruct = *prog_ptr;
    src_x += (instruct & 0x0f) - 8;
    src_y += (((int) (instruct & 0xf0)) >> 4) - 8;
    /*
     * Print source pixel coordinates and contributing area fraction.
     */
    if(i >= start && i <= end) {
      if(part == 0)
	part = FULLSCALE;
      prmsg(MSG,("%4d %4d 0x%4x\n",src_x,src_y,part));
    }
  }
  return(0);
} /* debug_print */

#if 0
/*==============================================================================
 * Tests the spline function parameters.
 *
 * The parameters are read in from the spline function parameter file, and the
 * spline function is evaluated at one arbitrary coordinate point. The
 * coordinates of this point and the calculated spline function values for x
 * and y are then printed.
 *
 * Note that this function is currently not called from within this package.
 *
 * Input : distfile: name of the file that contains the spline function
 *                   parameters
 * Output: none
 * Return: none
 */

gtest(char *distfile)
{
  /*
   * Test for an arbitrary point (358,230).
   */
  float xin=358, yin=230, xout, yout;
  spline = spd_loadspline(distfile);
  spd_calcspline(spline,1,1,&xin,&yin,&xout,&yout);
  printf("%f %f %f %f\n",xin,yin,xout,yout);
}

#endif

/*==============================================================================
 * Prepare the floodfield (also called flatfield) image for the floodfield
 * correction.
 *
 * The floodfield correction takes into account the fact that a sample with
 * absolutely uniform scattering response does not necessarily produce a flat
 * image (e.g. because of a non-uniform detector response). If one takes an
 * image of a real sample, one has to correct for this non-uniformity.
 *
 * This is done by taking an image from an uniformly scattering probe (the
 * floodfield image) and then dividing the real image by the floodfield image.
 *
 * To make the actual correction calculation in divide_insito_im() and
 * divide_im() faster, the floodfield image calculated here is inverted. Thus
 * the correction routines can multiply with instead of divide by the floodfield
 * image.
 *
 * The input floodfield image can be either of type "unsigned short" or of
 * type "float". If it is of type "unsigned short", then it is handed over
 * in the argument "in_im". If the input floodfield image is of type "float",
 * then "in_im" must be a NULL pointer, and the input floodfield image is handed
 * over in "out_im".
 *
 * The output floodfield image is always of type "float" and returned in the
 * argument "out_im".
 *
 * Invalid pixels in the floodfield image (those with pixel value == 0. or pixel
 * value == dummy) will be stored in a list and dealt with later when the actual
 * division by the floodfield image is made. "dummy" is either the value of the
 * "Dummy" keyword in the floodfield header, if this is defined, or the value of
 * the "dummy" command line argument. If the latter is not defined by the user,
 * its default value is 0.
 *
 * Input : in_im:  array with the input floodfield image (unsigned short)
 *         out_im: array with the input floodfield image (float)
 * Output: out_im: array with the inverted floodfield image (float)
 * Return: 0
 */

int prepare_flood(unsigned short *in_im,float *out_im)
{
  float *float_flood;
  unsigned short *short_flood,dim_1,dim_2;
  long *pfldum;
  int dummycnt = 0,i;
  float dummy,ddummy;
  struct data_head flo_head = img_head[FLOTYP];

  if(out_im == NULL)
    return(0);
  /*
   * Define the "dummy" and "ddummy" values.
   */
  dummy = flo_head.init & FL_DUMMY ? flo_head.Dummy : 0.;
  ddummy = flo_head.init & FL_DDUMM ? flo_head.DDummy : DDSET(dummy);

  dim_1 = flo_head.init & FL_DIM1 ? flo_head.Dim_1 : XSIZE;
  dim_2 = flo_head.init & FL_DIM2 ? flo_head.Dim_2 : YSIZE;

  /*
   * If the image is of type "unsigned short", copy it from the "in_im" buffer
   * to the (float-type) "out_im" buffer.
   */
  if(in_im != NULL) {
    short_flood = in_im + dim_1 * dim_2 - 1;
    float_flood = out_im + dim_1 * dim_2 - 1;
    for(; float_flood >= out_im; short_flood--, float_flood--)
      *float_flood = (float)*short_flood;
  }

  /*
   * Invert the image on a pixel-to-pixel basis.
   *
   * Count the pixels with invalid values (0. or "dummy"). Values of 0. are
   * replaced by "dummy".
   */
  float_flood = out_im + dim_1 * dim_2 - 1;
  for(; float_flood >= out_im; float_flood--)
    if(*float_flood == 0. || DUMMY(*float_flood,dummy,ddummy))
    {
      *float_flood = dummy;
      dummycnt++;
    }
    else
      *float_flood = 1. / *float_flood;
 
  if(fldumlst != NULL)
  {
    pfree(fldumlst);
    fldumlst = NULL;
  }

  if(dummycnt == 0)
    return(0);

  /*
   * Make the list with the pixel indices of the invalid pixels.
   */
  fldumlst = (long *)pmalloc((dummycnt + 1) * sizeof(long));
  pfldum = fldumlst;
  for(i = 0; i < dim_1 * dim_2; i++)
    if(*(out_im + i) == dummy)
      *pfldum++ = i;
  *pfldum = -1;

  return(0);
} /* prepare_flood */

/*==============================================================================
 * Prints for debugging purposes various pieces of information from the look-up
 * table and the look-up table program.
 *
 * Printed are the following items:
 * - if the look-up-table program instruction is 0, but it is not at the
 *   begining of a new target pixel: the contents of the look-up-table program
 *   and of the look-up table as well as the present address in the look-up-
 *   table program;
 * - if the look-up table contains 0xff: the content of the look-up-table
 *   program.
 *
 * The routine also counts
 * - the number of INCTARGET exceptions;
 * - the number of ABSSRC exceptions;
 * - the total number of exceptions (i.e., when program instruction == 0);
 * - the number of occurrences of 0xff in the look-up table.
 *   ??? what does this mean ???
 *
 * This routine is at present not called from within the program, but can be
 * called from the debugger.
 *
 * Note that this routine expects an "intermediate state" of the look-up table
 * and associated program, i.e. the state when the exceptions and the beginning
 * of a new target pixel sequence are still marked in the look-up table and not
 * in the look-up-table program.
 *
 * Input : prog:        the start address in the look-up-table program
 *         lut:         the start address in the look-up table
 *         prog_length: the length of the look-up-table program
 * Output: none
 * Return: 0
 */

int despair(unsigned char *lut_prog,LUT_TYPE *lut_tab,int prog_length)
{
  unsigned char *prog_end;
  unsigned char *prog_ptr;
  unsigned long count_abs = 0,count_ff = 0,count_null = 0,count_inc = 0;
  LUT_TYPE *lut_ptr;

  prog_end = lut_prog + prog_length;
  lut_ptr = lut_tab;
  for(prog_ptr = lut_prog; prog_ptr != prog_end; prog_ptr++, lut_ptr++) {
    if(*prog_ptr == 0 && !(*lut_ptr & BITMASK))
      prmsg(MSG,("%d %d %d \n",*prog_ptr,*lut_ptr,prog_ptr-lut_prog));
    if(*prog_ptr == 0 && (*lut_ptr & INCTARGET))
      count_inc++;
    if(*prog_ptr == 0 && (*lut_ptr & ABSSRC))
      count_abs++;
    /* ??? what does 0xff in the look-up table mean ??? */
    if(*lut_ptr == 0xff) {
      prmsg(MSG,("<0x%x>",*prog_ptr));
      count_ff++;
    }
    if(*prog_ptr == 0)
      count_null++;
  }
  prmsg(MSG,("\nInc Abs Null LUT==FF %d %d %d %d\n",count_inc,count_abs,
    count_null,count_ff));
  return(0);
} /* despair */
