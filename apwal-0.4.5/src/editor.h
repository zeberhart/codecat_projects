/* editor.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#ifndef EDITOR__H
#define EDITOR__H

#include "common.h"

// ----------------------------------------------------------------------------
typedef struct editor_t
{
  struct apwalapp_t *apwal;

  GtkWidget *image;

  gint       x;                 // x coord of the top left icon in the view
  gint       y;                 // y coord if the top left icon in the view
  gint       width;             // number of icons in a row
  gint       height;            // number of icons in a column
  gint       xwidth;            // resolution of the current X11 display
  gint       xheight;           //   ''

  GTimer    *dblclick_timer;
  gint       dblclick_last_x;
  gint       dblclick_last_y;

  gboolean   drag_in_progress;
  gint       drag_start_x;
  gint       drag_start_y;
  gint       drag_current_x;
  gint       drag_current_y;
  
  gboolean   refresh_needed;

} editor_t;

// ----------------------------------------------------------------------------
editor_t* editor_new(struct apwalapp_t *apwal);
void      editor_refresh(editor_t *l);
void      editor_goto(editor_t *e, int pos_x, int pos_y);

#endif /*EDITOR__H*/
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
