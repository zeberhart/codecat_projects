/*	@(#)sortmail.h 1.15 02/03/11 falk	*/
/*	$Id: sortmail.h,v 1.3 2003/08/21 15:59:26 efalk Exp $	*/


#include "config.h"

	/* sortmail exit codes */

#define	EXIT_OK		0		/* success */
#define	EXIT_NOMAIL	1		/* success, but inbox was empty */
#define	USER_ERR	2		/* user error */
#define	INITFILE_ERR	3		/* syntax error in .rc file */
#define	MEM_ERR		4		/* out of memory */
#define	UNAME_ERR	5		/* user name not found */
#define	UDIR_ERR	6		/* user home dir not found */
#define	TMPFILE_ERR	7		/* could not write a temp file */
#define	BOUNCEDB_ERR	8		/* error in bouncecheck database */
#define	INPUT_ERR	9		/* error reading input */
#define	POP_ERR		10		/* error reading imap or pop folder */
#define	INTERRUPTED	11		/* killed by interrupt */
#define	POP_TIMEOUT	12		/* network timeout */






	/* everything from here down is internal */


#ifdef	NOREGCOMP

typedef	int	regex_t ;
typedef	char	*regmatch_t ;

#define	REG_ICASE	1
#define	REG_NOSUB	0

#ifdef	__STDC__
extern	int	regcomp( regex_t *, char *, int ) ;
extern	int	regexec( regex_t *, char *, size_t, regmatch_t *, int ) ;
extern	void	regfree(regex_t *) ;
extern	size_t	regerror(int, regex_t *, char *, int) ;
#else
extern	int	regcomp() ;
extern	int	regexec() ;
extern	void	regfree() ;
extern	size_t	regerror() ;
#endif

#else

#include <regex.h>

#endif	/* NOREGCOMP */

#ifdef	DEBUG
#ifdef	__STDC__
extern	void	assfail(char *, char *, int) ;
#define	assert(e) ((void)((e) || (assfail(#e, __FILE__,__LINE__),0)))
#else
extern	void	assfail() ;
#define	assert(e) ((void)((e) || (assfail("e", __FILE__,__LINE__),0)))
#endif
#else
#define	assert(e) ((void)0)
#endif

#define	MAXLINE	1024

typedef	int	bool ;
#define	True	1
#define	False	0

	/* sortmail variables */

extern	char	*user ;		/* userid */
extern	char	*home ;		/* user's $HOME */
extern	char	*mailbox ;	/* $MAIL, default = /usr/spool/mail/$USER */
extern	char	*mailrc ;	/* $MAILRC, default = $HOME/.mailrc */
extern	char	*sortmailrc ;	/* initfile, default = $HOME/.sortmailrc */
extern	char	*deflt ;	/* where unclasifiable mail goes */
extern	char	*reject ;	/* where mailing list errors go */
extern	char	*folder ;	/* folder, default = $HOME/folders */
extern	char	*vacation ;	/* pipe through vacation(1) */
extern	char	*from ;		/* value for "From " line */
extern	char	*lines ;	/* message size in lines */
extern	char	*size ;		/* message size in bytes */
extern	char	*tmpdir ;	/* tmp directory name */
extern	char	*sendmail ;	/* sendmail command */

extern	int	verbose ;
extern	int	terse ;
extern	char	*logfilename ;
extern	FILE	*logfile ;
extern	int	delayTime ;
extern	FILE	*tempfile ;
extern	int	exitCode ;
extern	long	sizeLimit ;
extern	bool	keepAll ;
extern	bool	noApop ;
extern	int	timeout ;
extern	int	maxlines ;	/* max lines to download */




typedef	enum	{MSG_FILE, POP_ANY, IMAP, POP3, POP2} BoxType ;

typedef	int	MsgState ;
#define	HAVE_NONE	0	/* message state */
#define	HAVE_HEADER	1
#define	HAVE_PARTIAL	2
#define	HAVE_WHOLE	3

	/* This structure represents one mail message.  Messages may
	 * be a file, a segment of a mail folder or a message on a
	 * pop or imap server.
	 */

typedef	struct {
	  BoxType	type ;
	  char		*name ;		/* filename if appropriate */
	  FILE		*file ;		/* file if appropriate */
	  bool		tmpfile ;	/* this is a temp file */
	  int		n ;		/* message # */
	  long		start ;		/* message start */
	  long		size ;		/* message size */
	  int		lines ;		/* message size, lines */
	  MsgState	state ;		/* how much downloaded? */
	  char		*fromaddr ;	/* "From " line */
	  char		*fromline,	/* "From: " line */
			*toline,	/* "To, Cc, ...: " lines */
			*subjectline ;	/* "Subject: " line */
	  char		*msgid ;
	  bool		keep ;		/* leave on pop server */
	  bool		keepGoing ;	/* keep processing this message */
	  bool		disposed ;	/* this message has been handled */
	  /* parsing state info */
	  bool		inheader ;
	  enum {NONE, To, From, Subject} scanning ;
	  /* info specific to message readers */
	  FILE		*infile ;	/* input from a file */
	  void		*popinfo ;
	  int		error ;		/* i/o error during processing */
	} MailMessage ;



	/* common elements of command objects */
typedef	enum {PATTERN, IPPATTERN, INCLUDE, EXCLUDE, BCHECK,
		HEADER, REPLACE, VAR, EXPRESSION} cmdType ;

typedef	struct cmd {
	  struct cmd	*next ;
	  cmdType	type ;
	  int		lineno ;
	} Cmd ;

extern	Cmd	*cmds, *lastcmd ;

	/* pattern match and dispose */

#define	P_SUBJECT	0x01
#define	P_TO		0x02
#define	P_FROM		0x04
#define	P_HEADER	0x08
#define	P_ALL		0x10
#define	P_RECEIVED	0x20
#define	P_IGNORECASE	0x40
#define	P_ONCE		0x80
#define	P_SEARCH	(P_SUBJECT|P_TO|P_FROM|P_HEADER|P_RECEIVED|P_ALL)

typedef	struct {
	  Cmd	c ;
	  char	*filename ;
	} IncludeInfo ;

extern	int		need_include ;


typedef	struct {
	  Cmd	c ;
	  char	*header ;	/* header line to write */
	  int	len ;		/* length up to ':' */
	  bool	active ;	/* active yet? */
	  bool	done ;		/* written yet? */
	} HeaderInfo ;

extern	int	headers ;

typedef	struct {
	  Cmd	c ;
	  char	*string ;
	} Var ;


typedef	struct {
	  Cmd	c ;
	  struct expr *expr ;
	} ExprInfo ;




	/* Expression evaluation */

typedef	enum {E_EVAL, E_FREE} ExprOp ;

#ifdef	__STDC__
typedef	int	exprHandler(ExprOp, struct expr *, MailMessage *) ;
#else
typedef	int	exprHandler() ;
#endif

typedef struct expr {
	  char	type ;
	  exprHandler *handler ;
	} Expression ;

typedef	struct {
	  Expression e ;
	  int	value ;
	} ConstExpr ;

typedef	struct {
	  Expression e ;
	  char	*var ;
	} VarExpr ;

typedef	struct {
	  Expression e ;
	  char	*pattern ;	/* pattern to match */
	  regex_t preg ;	/* compiled pattern */
	  bool	compiled ;
	  int	flags ;
	  char	*command ;	/* what to do with matching messages */
	} PatExpr ;

typedef	struct {
	  Expression e ;
	  Expression *left ;
	} UnaryExpr ;

typedef	struct {
	  Expression e ;
	  Expression *left, *right ;
	} BinExpr ;

typedef	struct {
	  Expression e ;
	  Expression *expr ;	/* child expression */
	  char	*command ;	/* what to do with matching messages */
	} ParenExpr ;



	/* input context for parser */

typedef	struct {
	  char *ptr ;
	  char	line[MAXLINE] ;
	  FILE	*file ;
	  int	lineno, lineno0 ;
	} Ictx ;



	/* macro and function declarations */

#define	strmatch(a,b)	(!strncmp((a),(b),sizeof(b)-1))
#define	strcmatch(a,b)	(!strncasecmp((a),(b),sizeof(b)-1))
#define	MALLOC(t,n)	((t *)malloc(sizeof(t)*(n)))

#define	NA(a)	(sizeof(a)/sizeof((a)[0]))

#ifdef	__STDC__
extern	int	die(int xval, char *fmt, ...) ;
extern	int	getMessage(MailMessage *, MsgState) ;
extern	void	parseMessage(MailMessage *msg, char *line) ;
extern	void	parseMessageDone(MailMessage *msg) ;
extern	void	ExpressionAdd(Ictx *) ;
extern	int	ExpressionEvaluate( ExprInfo *expr, MailMessage *msg ) ;
extern	void	append_cmd(Cmd *) ;
extern	void	dispose(char *cmd, MailMessage *) ;
extern	void	advance( Ictx *in, int ) ;
extern	void	advanceNext( Ictx *in ) ;
extern	void	advanceLine( Ictx *in ) ;
#else
extern	int	die() ;
extern	void	parseMessage() ;
extern	void	parseMessageDone() ;
extern	void	ExpressionAdd() ;
extern	int	ExpressionEvaluate() ;
extern	void	append_cmd() ;
extern	int	dispose() ;
extern	void	advance() ;
extern	void	advanceNext() ;
extern	void	advanceLine() ;
#endif
