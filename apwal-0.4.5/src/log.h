/* log.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#ifndef LOG__H
#define LOG__H

#include <gtk/gtk.h>

void nop(void);
// ----------------------------------------------------------------------------
#ifdef APWAL_DEBUG
#define TRACE(format, args...) g_print("[%s:%d] %s() " format "\n", __FILE__, __LINE__, __FUNCTION__, ## args)
#else
#define TRACE(format, args...) nop()
#endif
// ----------------------------------------------------------------------------
#define WARN(format, args...) g_warning("[%s:%d] %s() " format, __FILE__, __LINE__, __FUNCTION__, ## args)
#define ERR(format, args...) g_error("[%s:%d] %s() " format, __FILE__, __LINE__, __FUNCTION__, ## args)

// ----------------------------------------------------------------------------
#endif /*LOG__H*/
