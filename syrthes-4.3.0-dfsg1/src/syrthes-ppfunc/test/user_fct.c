# include <stdio.h>
# include <stdlib.h>
# include <math.h>
#include "tree.h"
#include "bd.h"
#include "abs.h"
#include "option.h"
#include "const.h"
#include "proto.h"
#include "hmt_bd.h"
#include "hmt_proto.h"


/*******************************************************************/ 
void user_cini_fct(struct Maillage maillnodes,double *t)
/*******************************************************************/ 
{
  int i,j,ne,nr;
  double x,y,z,T,tt;

  for (i=0;i<maillnodes.nelem;i++) 
    { 
      data_element_moy(i,maillnodes,t,&nr,&x,&y,&z,&T);
      t[i]=22.+x; 
    } 

  for (i=0;i<maillnodes.nelem;i++) 
    { 
      data_element_moy(i,maillnodes,t,&nr,&x,&y,&z,&T);
      if (nr== 1 || nr== 4) {
        t[i]=24.+y; 
      } 
    } 

}
/*******************************************************************/ 
void user_cphyso_fct(struct Maillage maillnodes,
                     double *tmps,struct Prophy physol,double tempss)
/*******************************************************************/ 
{
  int i,j,ne,nr;
  double x,y,z,T,tt;

  tt=tempss;

  for (i=0;i<physol.nelem;i++) 
      { 
        data_element_moy(i,maillnodes,tmps,&nr,&x,&y,&z,&T);
        if (nr== 5) {
           physol.rho[i]=2460.; 
        } 
        else if (nr== 6 || nr== 8 || nr==12) {
           physol.rho[i]=2460.*2; 
        } 
        else if (nr== 2 || nr== 4 || nr==12) {
           physol.rho[i]=3098+y; 
        } 
        else if (nr==17 || nr==18) {
           physol.rho[i]=3099+y; 
        } 
        else if (nr==21 || nr==19) {
           physol.rho[i]=7000*5; 
        } 
      } 

  for (i=0;i<physol.nelem;i++) 
      { 
        data_element_moy(i,maillnodes,tmps,&nr,&x,&y,&z,&T);
        if (nr== 5) {
           physol.cp[i]=908.+1.3*T; 
        } 
        else if (nr== 6 || nr== 8 || nr==12) {
           physol.cp[i]=908.+1.3*T*2; 
        } 
        else if (nr== 2 || nr== 4 || nr==12) {
           physol.cp[i]=460*0.5; 
        } 
        else if (nr==17 || nr==18) {
           physol.cp[i]=461*0.6; 
        } 
        else if (nr==21 || nr==19) {
           physol.cp[i]=460*4; 
        } 
      } 

  for (i=0;i<physol.kiso.nelem;i++) 
      { 
        data_element_moy(physol.kiso.ele[i],maillnodes,tmps,&nr,&x,&y,&z,&T);
        if (nr== 5) {
           physol.kiso.k[i]=2.2*(1-0.0009*(T-20)); 
        } 
        else if (nr== 6 || nr== 8 || nr==12) {
           physol.kiso.k[i]=2.2*(1-0.0009*(T-20))*2; 
        } 
      } 

  for (i=0;i<physol.kortho.nelem;i++) 
      { 
        data_element_moy(physol.kortho.ele[i],maillnodes,tmps,&nr,&x,&y,&z,&T);
        if (nr== 2 || nr== 4 || nr==12) {
           physol.kortho.k11[i]=2.2*(1-0.0009*(T-20)); 
           physol.kortho.k22[i]=2.2*(1-0.0009*(T-20)); 
           physol.kortho.k33[i]=1.6*(1-0.0009*(T-20)); 
        } 
        else if (nr==17 || nr==18) {
           physol.kortho.k11[i]=0.9*pow(T+273.15,3); 
           physol.kortho.k22[i]=0.8*pow(T+273.15,3); 
           physol.kortho.k33[i]=0.6176*pow(T+273.15,3); 
        } 
      } 

  for (i=0;i<physol.kaniso.nelem;i++) 
      { 
        data_element_moy(physol.kaniso.ele[i],maillnodes,tmps,&nr,&x,&y,&z,&T);
        if (nr==21 || nr==19) {
           physol.kaniso.k11[i]=1+x; 
           physol.kaniso.k22[i]=2+y; 
           physol.kaniso.k33[i]=3+z; 
           physol.kaniso.k12[i]=12+x; 
           physol.kaniso.k13[i]=13+y; 
           physol.kaniso.k23[i]=23+z; 
        } 
      } 

}
/*******************************************************************/ 
void user_limfso_fct(struct Maillage maillnodes,struct MaillageCL maillnodeus,
	    double *var,struct Clim diric,struct Clim flux,
	    struct Clim echang,struct Clim rayinf,double tempss)
/*******************************************************************/ 
{
}
/*******************************************************************/ 
void user_cfluvs_fct(struct Maillage maillnodes,
                     double *t,struct Cvol fluxvol_t,double tempss)
/*******************************************************************/ 
{
  int i,j,ne,nr;
  double x,y,z,T,tt;

  tt=tempss;

  for (i=0;i<fluxvol_t.nelem;i++) 
    { 
     data_element_moy(fluxvol_t.nume[i],maillnodes,t,&nr,&x,&y,&z,&T);
     if (nr==15) {
        fluxvol_t.val1[i]=(369.5*exp(-0.02310491*tt/31557600.)+85.5*exp(-0.00160451*tt/31557600)+28.5*exp(-0.03850818*tt/31557600)+3.5*exp(-0.000094178*tt/31557600)+0.0*exp(-0.007876673*tt/31557600))/0.144086005464; 
     } 
    } 

}
/*******************************************************************/ 
void user_rescon_fct(struct Maillage maillnodes,struct MaillageCL maillnodeus,
	  	           double *t,double *tcor,struct Contact rescon,double tempss,
		           struct SDparall sdparall)
/*******************************************************************/ 
{
}
/************************************************************************/ 
void user_hmt_cini_fct(struct Maillage maillnodes,
		             double *t,double *pv,double *pt)
/************************************************************************/ 
{
}
/*******************************************************************/ 
void user_hmt_limfso_fct(struct Maillage maillnodes,struct MaillageCL maillnodeus,
	                       double *t,double *pv,double *pt,
	                       struct HmtClimhhh hmtclimhhh,struct Clim rayinf,double tempss)
/*******************************************************************/ 
{
}
/************************************************************************/ 
void user_hmt_cfluvs_fct(struct Maillage maillnodes,
			 double *t, struct Cvol fluxvol_t ,
			 double *pv,struct Cvol fluxvol_pv,
			 double *pt,struct Cvol fluxvol_pt,double tempss)
/************************************************************************/ 
{
}
/*******************************************************************/ 
void user_hmt_rescon_fct(struct Maillage maillnodes,struct MaillageCL maillnodeus,
		               double *t,double *pv,double *pt,
    	               double *tcor,double *pvcor,double *ptcor,
		               struct Contact rescon,double tempss,
		               struct SDparall sdparall)
/*******************************************************************/ 
{
}
/*******************************************************************/ 
void user_transfo_perio_fct(int ndim,
                            int nb1,int *iref,
                            double x,double y,double z,
                            double *xt, double *yt, double *zt)
/*******************************************************************/ 
{
}
/*******************************************************************/ 
void user_ray_ftc(struct Maillage maillnodray,
                  double *tmpray,struct ProphyRay phyray,
                  struct PropInfini *propinf,struct Clim fimpray,struct Vitre vitre,
                  double tempss)
/*******************************************************************/ 
{
}
/*******************************************************************/ 
void user_solaire_fct(double *fdirect,double *fdiffus, int nbande,double tempss)
/*******************************************************************/ 
{
}
/*******************************************************************/ 
void user_propincidence_fct(int n,int nbande,double teta,
                            double **e,double **r,double **a,double **t,
                            double **emissi,double **reflec,
                            double **absorb,double **transm)
/*******************************************************************/ 
{
}
