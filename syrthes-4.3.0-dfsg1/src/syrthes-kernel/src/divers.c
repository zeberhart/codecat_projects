/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>
#include <sys/times.h>
#include <time.h>
# include "syr_abs.h"
# include "syr_bd.h"
# include "syr_parall.h"
# include "syr_proto.h"

#ifdef _SYRTHES_MPI_
# include "mpi.h"
MPI_Status status;
#endif


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | syrban                                                               |
  |======================================================================| */
void syrban(int i)
{
  if (i==1)
    {
      printf("\n\n");

      printf("       SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS\n");
      printf("     SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS\n");
      printf("    SSSSSS                                                                  \n");
      printf("     SSSSSS                                                                \n");
      printf("      SSSSSSSS     YY    YY  RRRRRR  TTTTTTTT  HH   HH  EEEEEE          SSSSSS  \n");
      printf("           SSSSS    YY  YY   RR   RR    TT     HH   HH  EE          SSSSSSSSS     \n"); 
      printf("            SSSSS    YYYY    RR   RR    TT     HH   HH  EE       SSSSS     \n"); 
      printf("            SSSSS     YY     RRRRRR     TT     HHHHHHH  EEEEE   SSSSS  \n");   
      printf("          SSSSS       YY     RR RR      TT     HH   HH  EE      SSSSS  \n"); 
      printf("    SSSSSSSSS         YY     RR  RR     TT     HH   HH  EE        SSSSSSSS  \n"); 
      printf("   SSSSSSS            YY     RR   RR    TT     HH   HH  EEEEEE      SSSSSSSS  \n");
      printf("                                                                     SSSSS\n");
      printf("                                                                SSSSSSS\n");
      printf("                        SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS\n");
      printf("                               SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS\n");


      printf("                               VERSION 4.3                         \n"); 
      printf("\n\n");
    }
  else
    {
      printf("\n\n");
      printf("     ******************************************************************\n");
      printf("     *          S Y R T H E S - FIN NORMALE DU PROGRAMME              *\n"); 
      printf("     ******************************************************************\n");
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    caldtvar                                                          |
  |    ATTENTION A COMPLETER en //                                       |
  |                                                                      |
  |======================================================================| */
void caldtvar(struct PasDeTemps *pasdetemps,
	      int npoin,double *tmpsa,double *tmps,struct SDparall sdparall)
{
  int i,num,ng;
  double dd,dtm,dt,*tabdt;

  if (pasdetemps->dtauto.actif && !(pasdetemps->premier) && !(pasdetemps->suiteprem) )
    {
      for (i=0,dtm=0;i<npoin;i++) 
	dtm=max(dtm,fabs(*(tmpsa+i)-*(tmps+i)));
      dd=pasdetemps->rdtts * pasdetemps->dtauto.deltaT/dtm;
      dt=min(dd,pasdetemps->dtauto.dtlimit); 

      pasdetemps->rdtts=min_double_parall(dt);
    }

  else if (pasdetemps->dtmult.actif)
    {
      if (pasdetemps->ntsyr <= pasdetemps->dtmult.nbiter[0])
	num=0;
      else if (pasdetemps->ntsyr > pasdetemps->dtmult.nbiter[pasdetemps->dtmult.nb-1])
	num=pasdetemps->dtmult.nb-1;
      else
	for (i=0;i<pasdetemps->dtmult.nb-1;i++){
	  if (pasdetemps->dtmult.nbiter[i] < pasdetemps->ntsyr &&  pasdetemps->ntsyr <= pasdetemps->dtmult.nbiter[i+1]) {
	    num=i+1;
	    break;
	  }
	}
      
      pasdetemps->rdtts=pasdetemps->dtmult.dt[num];
    }
  
}

