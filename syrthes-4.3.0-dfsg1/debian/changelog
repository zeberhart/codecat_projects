syrthes (4.3.0-dfsg1-2) unstable; urgency=medium

  * Update patch setup.patch after openmpi went mutiarch (closes:
    #848747)

 -- Gilles Filippini <pini@debian.org>  Tue, 20 Dec 2016 18:29:18 +0100

syrthes (4.3.0-dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches:
    - buildflags.patch
    - setup.patch
  * New patch no-cx-freeze.patch: cx_Freeze isn't available in Debian
    and we wouldn't want to use it anyway.

  * d/rules: enable parallel build.

 -- Gilles Filippini <pini@debian.org>  Tue, 14 Jul 2015 14:24:34 +0200

syrthes (4.1.1-dfsg1-5) unstable; urgency=medium

  * Disable libsyrthes_cfd on s390x where openmpi isn't available.

 -- Gilles Filippini <pini@debian.org>  Wed, 24 Sep 2014 00:18:03 +0200

syrthes (4.1.1-dfsg1-4) unstable; urgency=medium

  * Enable libsyrthes_cfd to allow coupling with code-saturne. It needs
    libple develoment files provided by code-saturne-bin and
    code-saturne-include.
  * Add code-saturne to syrthes's Suggests field.
  * Install syrthes-tools binaries into SYRTHES4_HOME/bin, because
    syrthes-gui expects to find them there. Add symlinks to have them
    into /usr/bin as well.

  * Bump Standards-Version to 3.9.6 (no change).
  * Upload to unstable.

 -- Gilles Filippini <pini@debian.org>  Mon, 22 Sep 2014 11:35:40 +0200

syrthes (4.1.1-dfsg1-3) experimental; urgency=medium

  * Fix setup.patch to support the hdf5 1.8.13 new packaging layout.

 -- Gilles Filippini <pini@debian.org>  Wed, 30 Jul 2014 20:16:50 +0200

syrthes (4.1.1-dfsg1-2) experimental; urgency=medium

  * d/rules: display ERREUR.log in case of installation failure.
  * New patch Wformat.patch: fix printf format warnings causing false
    positives in ERREUR.log.

 -- Gilles Filippini <pini@debian.org>  Sun, 16 Mar 2014 23:53:04 +0100

syrthes (4.1.1-dfsg1-1) experimental; urgency=low

  * New upstream release.
  * New binary packages:
    * syrthes-tools (pre/post-processors, converters)
    * syrthes-gui (new GUI writen in python)
    * syrthes-tests (test cases).

  * New manpages for the new upstream tools.

 -- Gilles Filippini <pini@debian.org>  Thu, 23 Jan 2014 15:30:55 +0100

syrthes (3.4.3-dfsg1-7) unstable; urgency=low

  * debian/rules: define MED_API_23 for the convert2syrthes build
    (closes: #730901).

 -- Gilles Filippini <pini@debian.org>  Mon, 09 Dec 2013 13:57:37 +0100

syrthes (3.4.3-dfsg1-6) unstable; urgency=low

  * d/rules:
    + support for any default mpi implementation (Closes: #666417) 

  * d/copyright:
    + Update to the version 1.0 of the Machine-readable debian/copyright
      format specification
  * d/README.source:
    + Drop the quilt section, no needed anymore with source format
      "3.0 (quilt)"
  * d/control:
    + Bump Standards-Version to 3.9.3 (no change needed)

 -- Gilles Filippini <pini@debian.org>  Sun, 01 Apr 2012 15:43:12 +0200

syrthes (3.4.3-dfsg1-5) unstable; urgency=low

  * d/rules:
    + MED 3.0 support (Closes: #646466)
    + Minor cleanning

 -- Gilles Filippini <pini@debian.org>  Tue, 01 Nov 2011 23:22:43 +0100

syrthes (3.4.3-dfsg1-4) unstable; urgency=low

  * New patch as-needed from Ubuntu Dev Ilya Barygin: fixes FTBFS with
    linker flag --as-needed (closes: #632521)
  * debian/control:
    + Build-Depends: drop quilt
    + Standards-Version: 3.9.2 (no change needed)

 -- Gilles Filippini <pini@debian.org>  Mon, 04 Jul 2011 22:52:36 +0200

syrthes (3.4.3-dfsg1-3) unstable; urgency=low

  * debian/rules: ensure proper MPI configuration using the information
    provided by mpi-default-dev in /usr/share/mpi-default-dev/debian_defaults

 -- Gilles Filippini <pini@debian.org>  Sun, 20 Feb 2011 21:02:21 +0100

syrthes (3.4.3-dfsg1-2) unstable; urgency=low

  * Build-Depends: replace libhdf5-openmpi-dev with libhdf5-mpi-dev which
    pulls the default MPI implementation of libhdf5 depending on the
    architecture.

 -- Gilles Filippini <pini@debian.org>  Sun, 20 Feb 2011 15:26:41 +0100

syrthes (3.4.3-dfsg1-1) unstable; urgency=low

  * New upstream release:
    + new convert2syrthes util to convert mesh files from GAMBIT, GMSH or MED
      to the SYRTHES format

  * Quit hard coding the version number in the install path (closes: #571022)
  * Drop naming the binary package after the version when the
    SCIENCE_EXPLICIT_VERSION variable is set at build time
  * move to source format 3.0 (quilt)
  * debian/control:
    + drop DM-Upload-Allowed and set Uploaders to my debian address
    + Build-Depends: libmedc-dev, libhdf5-openmpi-dev (for convert2syrthes)
    + versioned Build-Depends on debhelper for source format 3.0 (quilt)
  * debian/rules: switch to the dh sequencer
  * New patch:
    + libmed-mesgerr: the macro MESGERR has to be defined explicitly
  * debian/convert2syrthes.1: manpage for convert2syrthes written after
    the convert2syrthes README.txt file

 -- Gilles Filippini <pini@debian.org>  Tue, 15 Feb 2011 23:53:29 +0100

syrthes (3.4.2-dfsg1-3) unstable; urgency=low

  * New patch:
    + f2c: Add a test against __GNUC__ to match gcc whatever the arch
      is (Closes: #548491) 

 -- Gilles Filippini <gilles.filippini@free.fr>  Sun, 27 Sep 2009 23:04:17 +0200

syrthes (3.4.2-dfsg1-2) unstable; urgency=low

  * debian/patches/syrthes_env:
    + Remove useless patch's part against syrthes.profile file.
  * debian/rules:
    + Fix files' permissions on install.
    + Add get-orig-source target.
  * debian/syrthes2ensight.1:
    + Escape minus signs.
  * debian/README.source:
    + Updated upstream source handling from uscan to git-import-orig.

 -- Gilles Filippini <gilles.filippini@free.fr>  Sun, 27 Sep 2009 18:18:52 +0200

syrthes (3.4.2-dfsg1-1) unstable; urgency=low

  * Initial release (Closes: #531902)

 -- Gilles Filippini <gilles.filippini@free.fr>  Tue, 15 Sep 2009 17:02:31 +0200
