<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC
   "-//OASIS//DTD DocBook XML V4.4//EN"
   "docbook/docbookx.dtd">
<refentry id='gif2png.1' lang="de">
  <refentryinfo>
    <title>gif2png</title>
    <productname>GIF2PNG</productname>
    <authorgroup>
      <author>
        <firstname>Alexander</firstname>
        <surname>Lehmann</surname>
        <contrib>Schrieb das Programm.</contrib>
        <address><email>alex@hal.rhein-main.de</email></address>
      </author>
      <author>
        <firstname>Greg</firstname>
        <surname>Roelofs</surname>
        <contrib>Auto-Interlace-Umwandlung und tRNS Optimierung.</contrib>
        <address><email>newt@pobox.com</email></address>
      </author>
      <author>
        <firstname>Eric S.</firstname>
        <surname>Raymond</surname>
        <contrib>Man page, Optionen "-O" und "-w" und Paketierung.</contrib>
        <address><email>esr@thyrsus.com</email></address>
      </author>
      <author>
        <firstname>Steve</firstname>
        <surname>Ward</surname>
        <contrib>Option "-m".</contrib>
      </author>
      <author>
        <firstname>Erik</firstname>
        <surname>Schanze</surname>
        <contrib>�bersetzte und �berarbeitete die Manpages f�r die Debian-Distribution.</contrib>
        <address><email>eriks@debian.org</email></address>
      </author>
    </authorgroup>
    <copyright>
      <year>1995</year>
      <holder>Alexander Lehmann</holder>
    </copyright>
    <copyright>
      <year>1999</year>
      <holder>Greg Roelofs</holder>
    </copyright>
    <copyright>
      <year>1999</year>
      <holder>Eric S. Raymond</holder>
    </copyright>
    <copyright>
      <year>2012</year>
      <holder>Steve Ward</holder>
    </copyright>
    <copyright>
      <year>2004</year>
      <holder>Erik Schanze</holder>
    </copyright>
  </refentryinfo>
  <refmeta>
    <refentrytitle>gif2png</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='date'>02. Juni 2012</refmiscinfo>
    <refmiscinfo class='source'>gif2png</refmiscinfo>
    <refmiscinfo class='version'>2.5.8</refmiscinfo>
    <refmiscinfo class='manual'>User manual</refmiscinfo>
  </refmeta>
<refnamediv id='name'>
  <refname>gif2png</refname>
  <refpurpose>wandelt GIFs in PNGs um</refpurpose>
</refnamediv>
<refsect1 id='synopsis'><title>&Uuml;BERSICHT</title>

<cmdsynopsis>
  <!--  gif2png  [ \-bdfghinprsvwO ]   [ datei.[gif] ...  ]  -->
  <command>gif2png</command>  <arg choice='opt'>-bdfghinprsvwO</arg>
  <arg choice='opt' rep='repeat'><replaceable>datei[.gif]</replaceable></arg>
</cmdsynopsis>

</refsect1>

<refsect1 id='description'><title>BESCHREIBUNG</title>

<para>Das Programm <command>gif2png</command> wandelt Dateien aus dem
veralteten und patent-belasteten Graphic Interchange Format (GIF) ins
Portable Network Graphics (PNG) Format um, einen offenen W3C-Standard.</para>

<para>Normalerweise wandelt <command>gif2png</command> jede auf der Kommandozeile
�bergebene Datei um und l�sst das Original unver�ndert. Wenn der Dateiname
keine Endung .gif hat, wird zuerst der unver�nderte Name versucht und dann
der Name mit Endung .gif. F�r jede Datei, die `foo.gif' hei�t, wird eine Datei
foo.png angelegt.</para>

<para>Wenn eine GIF-Datei foo.gif mit mehreren Bildern in sich umgewandelt wird,
erstellt <command>gif2png</command> mehrere PNG-Dateien, jede enth�lt ein Bild;
Die Namen werden foo.png, foo.p01, foo.p02 usw. sein.</para>

<para>Wenn keine Dateien angegeben wurden und die Standardeingabe ist ein
Terminal, wird <command>gif2png</command> einen Hilfe-Text ausgeben und die
Versionsnummer ausgeben und sich beendet.</para>

<para>Wenn keine Dateien angegeben wurden und die Standardeingabe ist ein
Ger�t oder eine Pipe, werden die Daten von dort in noname.png umgewandelt.
(Das Programm kann kein normaler Stdin-zu-Stdout sein, weil ein GIF mehrere
Bilder enthalten kann.)</para>

<para>Wenn der Filter-Modus (mit -f) erzwungen wird, wandelt 
<command>gif2png</command> die Daten der Standardeingabe um und schickt sie
auf die Standardausgabe oder gibt eine Fehlernummer zur�ck, wenn das GIF
mehrere Bilder enth�lt.</para>

<para>Das Programm wird die Informationen, die die GIF-Datei enth�lt,
so gut wie m�glich �bernehmen, incl. GIF-Kommentaren und Erweiterungsbl�cken
mit Anwendungsdaten. Alle grafischen Daten (Pixel, RGB-Farbtabellen) werden
verlustfrei umgewandelt. Transparenz wird auch �bernommen. Eine Ausnahme
gibt es: GIF-Klartext-Erweiterungen werden ausgelassen.</para>

<para>Das Programm wandelt automatisch sich aufbauende (interlaced) GIFs
in sich aufbauende PNGs um. Es erkennt Bilder, in denen alle Farben Graut�ne
(gleiche R-, G- und B-Werte) sind und wandelt solche Bilder in ein
PNG-Graustufen-Bild um. Doppelte Farbeintr�ge in der Palette werden still
�bernommen. Ungenutzte Farb-Paletteneintr�ge verursachen eine Fehlermeldung.
</para>

</refsect1>

<refsect1 id='options'><title>OPTIONEN</title>
<variablelist remap='TP'>
<varlistentry>
<term>
  <option>-b</option> {#}RRGGBB
</term>
<listitem>
<para>Hintergrund.  Ersetzt transparente Bildpunkte mit dem �bergebenen RGB-Wert,
6 hexadezimale Stellen, dabei werden je zwei Stellen als Wert f�r rot, gr�n und
blau gedeutet. Der Wert kann auch mit einem f�hrenden # geschrieben werden, wie
bei HTML.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-d </option>
</term>
<listitem>
<para>L�sche das GIF-Bild nach erfolgreicher Umwandlung.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-f </option>
</term>
<listitem>
<para>Filter-Modus.  Wandelt GIFs an Stdin in PNGs an Stdout und gibt eine
Fehlernummer zur�ck, wenn das GIF mehrere Bilder enth�lt.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-g </option>
</term>
<listitem>
<para>Schreibt Gamma=1/2.2 und sRGB-Bl�cke in das PNG.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-h </option>
</term>
<listitem>
<para>Erstellt Farb-Frequenz-Histogramme (hIST chunks) in den umgewandelten
Farbbildern.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-i </option>
</term>
<listitem>
<para>Erzwinge Umwandlung in sich aufbauende PNG-Bilder.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-m </option>
</term>
<listitem>
<para>Bewahrt die �nderungszeit der Datei. Die PNG-Ausgabedatei erh�lt den Zeitstempel
der Ursprungsdatei, nicht die Zeit ihrer Umwandlung.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-n </option>
</term>
<listitem>
<para>Erzwinge Umwandlung in sich nicht aufbauende PNG-Bilder.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-p </option>
</term>
<listitem>
<para>Anzeige des Fortschritts der PNG-Umwandlung.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-r </option>
</term>
<listitem>
<para>Versucht, Daten defekter GIF-Dateien wieder herzustellen.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-s </option>
</term>
<listitem>
<para>�bertrage keine Information der verwendeten GIF-Software in das PNG.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-v </option>
</term>
<listitem>
<para>Verbose-Modus; zeigt Zusammenfassung, <option>-vv</option> zeigt Statistik
der Umwandlung und Meldungen f�r die Fehlersuche.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option>-w </option>
</term>
<listitem>
<para>Modus Web-Probe; Schreibt die Dateinamen der GIFs auf die Standardausgabe,
die nicht mehrere Bilder enthalten. GIFs, die diesen Filter
nicht passieren, verursachen eine Fehlermeldung auf der Standard-Fehlerausgabe.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>
  <option> -O </option>
</term>
<listitem>
<para>Optimierung; entfernt ungenutzte Farbeintr�ge in der Tabelle. Normalerweise
verursachen diese eine Fehlermeldung und deaktivieren die Option 
<option>-d</option> (aber es wird komplett umgewandelt). Es wird die
zlib-Komprimierungsstufe 9 (beste Kompression) anstatt der normalen Stufe
verwendet. Der Wiederherstellungsmodus (Option <option>-r</option>) funktioniert
so: ungenutzte Eintr�ge der Farbtabelle l�sen keine Fehlermeldung aus, wie
sie das sonst tun, und bleiben erhalten. Wenn gleichzeitig die Option
<option>-O</option> verwendet wird, werden sie gel�scht. Fehlende Farbtabellen
werden durch eine Standardtabelle ersetzt, bei der Schwarz den Index 0, Wei� 1
hat und Rot, Gr�n, Blau, Gelb, Violett, T�rkis werden den verbleibenden 
Farbwerten zugeteilt.  Fehlende Bildpunkte werden auf 0 gesetzt.  Nicht erkannte
oder fehlerhafte Erweiterungen werden nicht �bernommen.</para>
</listitem>
</varlistentry>
</variablelist>

</refsect1>

<refsect1 id='problems'><title>PROBLEME</title>

<para>Einfaches Umwandeln aller Ihrer GIFs auf einmal mit
<command>gif2png</command> k�nnte nicht zu den erwarteten Ergebnissen f�hren.
Das Problem ist nicht PNG oder <command>gif2png</command>, sondern liegt
an der schlechten bis nicht vorhandenen Unterst�tzung von Transparenz und
Animation in PNG der meisten Browser.</para>

<para>Der Modus Web-Probe kann in Skripten f�r die Umwandlung ganzer
Webseiten benutzt werden. Alle PNGs, die aus den zur�ckgegebenen Dateien
erzeugt werden, stellen Netscape Navigator 4.04+, Internet Explorer 4.0b1+
und alle anderen aktuellen Browser ordentlich dar.
Beachten Sie: in zuk�nftigen Versionen von <command>gif2png</command> kann
sich die Bedeutung dieser Option (-w) anhand der F�higkeiten der verbreiteten
Browser �ndern.</para>

</refsect1>

<refsect1 id='patent_issues'><title>PATENT-LAST</title> 
<para>Das Format GIF ist von einem Patent der Firma Unisys belastet
(siehe &lt;<ulink url='http://www.delphion.com/details'>http://www.delphion.com/details</ulink>?&amp;pn10=US04464650&gt;)
wegen des Lempel-Ziv-Welch-Kompressionsalgorithmuses. Die Benutzung eines
Programms, das GIF-Bilder erzeugt und nicht von Unisys lizenziert wurde, kann
strafrechtlich verfolgt werden. Unisys verweigert offensichtlich eine 
Lizenzierung der Nutzung von LZW in Open-Source-Software und stellte 1999 klar,
dass weiterhin von Webseiten, die GIF-Bilder verwenden, die mit unlizenzierten
Programmen erzeugt wurden, eine Lizenzabgabe von $5000 zu fordern ist, selbst
wenn es gemeinn�tzige Webseiten sind, die mit freier Software erstellt wurden.
Siehe &lt;<ulink url='http://corp2.unisys.com/LeadStory/lzw-license.html'>http://corp2.unisys.com/LeadStory/lzw-license.html</ulink>&gt;
f�r Details.</para>

<para>Das Patent umfasst h�chstwahrscheinlich nicht LZW-Dekompressoren, wie
auch <command>gif2png</command> einen benutzt; die Rechtsauffassung ist
dahingehend uneinig, es gab dazu noch keinen Rechtsstreit und Unisys lehnt es
ab etwas zu best�tigen.  Es ist m�glich, dass Sie deswegen f�r die Verbreitung
von <command>gif2png</command> in einem kommerziellen Programm oder auf einer
gewinnbringenden Basis verantwortlich gemacht werden k�nnten.</para>

<para>Zur Geschichte der GIF-Patent-Debatte lesen Sie bitte &lt;<ulink
url='http://lpf.ai.mit.edu/Patents/Gif/Gif.html'>
http://lpf.ai.mit.edu/Patents/Gif/Gif.html</ulink>&gt;.
Um rechtliche Probleme zu umgehen, sollten Sie unverz�glich alle GIFs
auf Ihrer Webseite und anderswo in PNGs umwandeln. Diskussionen dazu &lt;<ulink
url='http://burnallgifs.org/'>http://burnallgifs.org/</ulink>&gt;.</para>

</refsect1>

<refsect1 id='standards_and_specifications'><title>STANDARD UND SPEZIFIKATION</title>

<para>Kopien der Spezifikation von GIF89 sind �berall im Internet zu finden;
suchen Sie nach "GRAPHICS INTERCHANGE FORMAT".  Das Graphics
Interchange Format(c) ist copyright-gesch�tzt von CompuServe
Incorporated. GIF(sm) ist eine Service-Marke von CompuServe
Incorporated.</para>

<para>Die PNG-Homepage unter &lt;<ulink
url='http://www.libpng.org/pub/png/'>http://www.libpng.org/pub/png/</ulink>&gt;
h�lt �beraus umfangreiche Informationen zum PNG-Standard, PNG-Bibliotheken und
PNG-Werkzeugen bereit.</para>

</refsect1>

<refsect1 id='see_also'><title>SIEHE AUCH</title>
<para>web2png(1)</para>

</refsect1>
</refentry>
