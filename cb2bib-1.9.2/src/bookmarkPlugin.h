/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#ifndef BOOKMARKPLUGIN_H
#define BOOKMARKPLUGIN_H

#include <QMenu>

class QFileSystemWatcher;


class bookmarkPlugin : public QMenu
{

    Q_OBJECT

public:
    explicit bookmarkPlugin(QWidget* parentw = 0);
    ~bookmarkPlugin();


signals:
    void editBookmarks();
    void openFile(const QString& fn);


private:
    QFileSystemWatcher* _fsw;
    QString _bookmark_file;
    QString iconProvider(const QString& fn, const QString& iconFile);


private slots:
    void init();
    void openBookmark();
    void parseBookmarks();

};

#endif
