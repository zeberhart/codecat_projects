/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 *                        All rights reserved
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Id: Mri.c,v 1.2 2000/04/26 09:44:43 amai Exp $
 */

#include <stdlib.h>
#include <Xm/Xm.h>
#include <Wc/WcCreate.h>
#include <Xbae/Matrix.h>
#include <Xbae/Caption.h>

#define RCO(name, func)		WcRegisterConstructor(app, name, func)
#define RCN(name, class)	WcRegisterClassName(app, name, class)
#define RCP(name, class)	WcRegisterClassPtr(app, name, class)

extern void MriRegisterMotif();

int
main(int argc, char *argv[])
{
    Widget toplevel;
    XtAppContext app;

    toplevel = XtAppInitialize(&app, "Mri",
			       NULL, 0,
			       &argc, argv,
			       NULL,
			       NULL, 0);

    MriRegisterMotif(app);
    
    RCN("XbaeMatrix", xbaeMatrixWidgetClass);
    RCP("xbaeMatrixWidgetClass", xbaeMatrixWidgetClass);
    RCN("XbaeCaption", xbaeCaptionWidgetClass);
    RCP("xbaeCaptionWidgetClass", xbaeCaptionWidgetClass);

    WcWidgetCreation(toplevel);
    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    exit(0);
}
