/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_abs.h"
#include "syr_tree.h"
#include "syr_const.h"
#include "syr_proto.h"

/* #include "mpi.h" */


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | mamass                                                               |
  |======================================================================| */

void mamass(struct PasDeTemps pasdetemps,struct Maillage maillnodes,
	    double *coeff,double *res)
{
  /* rq : pour T, on a coeff=rho*cp */

  int i,nca;
  int ne0,ne1,ne2;    
  double s3rindts,s12rindts,s4rindts;
  double r1,r2,r3;
  double rc;
  int nel,**nod,*ip0,*ip1,*ip2,*ip3;
  double **coords,xx;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  s3rindts=1./3./pasdetemps.rdtts, s12rindts=1./12./pasdetemps.rdtts;
  s4rindts=0.25/pasdetemps.rdtts;
  nel=maillnodes.nelem; 
  nod=maillnodes.node;
  coords=maillnodes.coord;


  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
    {
      for (i=0,ip0=*nod,ip1=*(nod+1),ip2=*(nod+2); i<nel; i++,ip0++,ip1++,ip2++){
	xx=maillnodes.volume[i]*coeff[i];
	res[*ip0]+=xx;
	res[*ip1]+=xx;
	res[*ip2]+=xx;
      }
      for (i=0;i<maillnodes.npoin;i++) res[i]*=s3rindts;
    }  
  else if (maillnodes.ndim==2)
    {
      for (i=0,ip0=*nod,ip1=*(nod+1),ip2=*(nod+2); i<nel; i++,ip0++,ip1++,ip2++)
	{
	  rc= coeff[i]*maillnodes.volume[i];
	  r1 = fabs(*(*(coords+nca)+*ip0));
	  r2 = fabs(*(*(coords+nca)+*ip1));
	  r3 = fabs(*(*(coords+nca)+*ip2));
	  res[*ip0]+= rc*(2*r1+r2+r3);
	  res[*ip1]+= rc*(r1+2*r2+r3);
	  res[*ip2]+= rc*(r1+r2+2*r3);
	}   
      for (i=0;i<maillnodes.npoin;i++) res[i]*=s12rindts;      
    }
  else
    {
      for (i=0,ip0=*nod,ip1=*(nod+1),ip2=*(nod+2),ip3=*(nod+3) ; i<nel; i++,ip0++,ip1++,ip2++,ip3++)
	{
	  xx=maillnodes.volume[i]*coeff[i];
	  res[*ip0]+=xx;
	  res[*ip1]+=xx;
	  res[*ip2]+=xx;
	  res[*ip3]+=xx;
	}
      for (i=0;i<maillnodes.npoin;i++) res[i]*=s4rindts;   
    }
  
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | madif_isotro                                                         |
  |======================================================================| */

void madif_isotro(struct Maillage maillnodes,
		  struct Iso kiso,double *res,double *xdms)
{
  int i,nca,nume;
  double r1,r2,r3;
  int np,nel;
  int ne0,ne1,ne2,ne3,**nodes;
  double s4=0.25, s12=1./12., s36=1./36.;
  double x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4;
  double xgrad1,ygrad1,zgrad1,xgrad2,ygrad2,zgrad2;
  double xgrad3,ygrad3,zgrad3,xgrad4,ygrad4,zgrad4;
  double xk;
  double *c0,*c1,*c2,**coords;
  double dx0,dx1,dx2,dx3,dx02,dx12,dx22,dx32;
  double dy0,dy1,dy2,dy3,dy02,dy12,dy22,dy32;
  double dz0,dz1,dz2,dz3,dz02,dz12,dz22,dz32;
  double x01,x02,x03,x12,x13,x23;
  double y01,y02,y03,y12,y13,y23;
  double z01,z02,z03,z12,z13,z23;
  double coeff;
  int **nar;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  np=maillnodes.npoin;
  nel=maillnodes.nelem;
  nodes=maillnodes.node;
  coords=maillnodes.coord;
  nar=maillnodes.eltarete;

  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
    {
      for (i=0; i<kiso.nelem; i++)
	{
	  nume=kiso.ele[i];
	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  xk=kiso.k[i];

	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  coeff=s4/maillnodes.volume[nume]*xk;
	  
	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2;

	  res[ne0]+= coeff*(dx02+dy02);
	  res[ne1]+= coeff*(dx12+dy12);
	  res[ne2]+= coeff*(dx22+dy22);
	  xdms[nar[0][nume]]+=coeff*(dx0*dx1+dy0*dy1);   
	  xdms[nar[2][nume]]+=coeff*(dx0*dx2+dy0*dy2);   
	  xdms[nar[1][nume]]+=coeff*(dx1*dx2+dy1*dy2); 
	}
    }
  else if (maillnodes.ndim==2)
    {
      for (i=0; i<kiso.nelem; i++)
	{
	  nume=kiso.ele[i];
	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  xk=kiso.k[i];

	  r1 = fabs(*(*(coords+nca)+ne0));
	  r2 = fabs(*(*(coords+nca)+ne1));
	  r3 = fabs(*(*(coords+nca)+ne2));

	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  coeff=s12/maillnodes.volume[nume]*xk*(r1+r2+r3);
	  
	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2;

	  res[ne0]+= coeff*(dx02+dy02); 
	  res[ne1]+= coeff*(dx12+dy12); 
	  res[ne2]+= coeff*(dx22+dy22); 
	  xdms[nar[0][nume]]+=coeff*(dx0*dx1+dy0*dy1);  
	  xdms[nar[2][nume]]+=coeff*(dx0*dx2+dy0*dy2);    
	  xdms[nar[1][nume]]+=coeff*(dx1*dx2+dy1*dy2);    	 
	}
    }     
  else
    {

      for (i=0; i<kiso.nelem; i++)
	{

	  nume=kiso.ele[i];
	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];ne3=nodes[3][nume];
	  xk=kiso.k[i];

	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];ne3=nodes[3][nume];
	  c0=*coords; c1=*(coords+1); c2=*(coords+2);
	  x01 =*(c0+ne1)-*(c0+ne0); y01 =*(c1+ne1)-*(c1+ne0); z01 =*(c2+ne1)-*(c2+ne0);
	  x02 =*(c0+ne2)-*(c0+ne0); y02 =*(c1+ne2)-*(c1+ne0); z02 =*(c2+ne2)-*(c2+ne0);
	  x03 =*(c0+ne3)-*(c0+ne0); y03 =*(c1+ne3)-*(c1+ne0); z03 =*(c2+ne3)-*(c2+ne0);
	  x12 =*(c0+ne2)-*(c0+ne1); y12 =*(c1+ne2)-*(c1+ne1); z12 =*(c2+ne2)-*(c2+ne1);
          x13 =*(c0+ne3)-*(c0+ne1); y13 =*(c1+ne3)-*(c1+ne1); z13 =*(c2+ne3)-*(c2+ne1);
	  x23 =*(c0+ne3)-*(c0+ne2); y23 =*(c1+ne3)-*(c1+ne2); z23 =*(c2+ne3)-*(c2+ne2);

	  dx0=y13*z12-z13*y12; dy0=-x13*z12+z13*x12; dz0=x13*y12-y13*x12;
	  dx1=-y23*z02+z23*y02; dy1=x23*z02-z23*x02; dz1=-x23*y02+y23*x02;
	  dx2=y13*z03-z13*y03; dy2=-x13*z03+z13*x03; dz2=x13*y03-y13*x03;
	  dx3=y01*z02-z01*y02; dy3=-x01*z02+z01*x02; dz3=x01*y02-y01*x02;

	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2; dx32=dx3*dx3;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2; dy32=dy3*dy3;
	  dz02=dz0*dz0; dz12=dz1*dz1; dz22=dz2*dz2; dz32=dz3*dz3;

	  coeff=s36/maillnodes.volume[nume]*xk;

	  res[ne0]+=coeff*(dx02+dy02+dz02);
	  res[ne1]+=coeff*(dx12+dy12+dz12);    
	  res[ne2]+=coeff*(dx22+dy22+dz22); 
	  res[ne3]+=coeff*(dx32+dy32+dz32);  

	  xdms[nar[0][nume]]+=coeff*(dx0*dx1+dy0*dy1+dz0*dz1);
	  xdms[nar[2][nume]]+=coeff*(dx0*dx2+dy0*dy2+dz0*dz2);
	  xdms[nar[3][nume]]+=coeff*(dx0*dx3+dy0*dy3+dz0*dz3);
	  xdms[nar[1][nume]]+=coeff*(dx1*dx2+dy1*dy2+dz1*dz2);
	  xdms[nar[4][nume]]+=coeff*(dx1*dx3+dy1*dy3+dz1*dz3);
	  xdms[nar[5][nume]]+=coeff*(dx2*dx3+dy2*dy3+dz2*dz3);
	}
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | madif_orthotro                                                       |
  |======================================================================| */

void madif_orthotro(struct Maillage maillnodes,
		    struct Ortho kortho,double *res,double *xdms)
{
  int i,nca,nume;
  double s36=1./36.,s4=0.25,s12=1./12.,coeff;
  double r1,r2,r3;
  int np,nel,**nodes;
  int ne0,ne1,ne2,ne3;
  double xk,yk,zk;
  double dx0,dx1,dx2,dx3,dx02,dx12,dx22,dx32;
  double dy0,dy1,dy2,dy3,dy02,dy12,dy22,dy32;
  double dz0,dz1,dz2,dz3,dz02,dz12,dz22,dz32;
  double x01,x02,x03,x12,x13,x23;
  double y01,y02,y03,y12,y13,y23;
  double z01,z02,z03,z12,z13,z23;
  double *c0,*c1,*c2,**coords;
  int **nar;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  np=maillnodes.npoin;
  nel=maillnodes.nelem;
  nodes=maillnodes.node;
  coords=maillnodes.coord;
  nar=maillnodes.eltarete;

  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
    {
      for (i=0; i<kortho.nelem; i++)
	{

	  nume=kortho.ele[i];
	  xk=kortho.k11[i]; yk=kortho.k22[i];

	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  coeff=s4/maillnodes.volume[nume];

	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2;

	  res[ne0]+= coeff*(dx02*xk+dy02*yk);
	  res[ne1]+= coeff*(dx12*xk+dy12*yk);
	  res[ne2]+= coeff*(dx22*xk+dy22*yk);
	  xdms[nar[0][nume]]+=coeff*(dx0*dx1*xk+dy0*dy1*yk);   
	  xdms[nar[2][nume]]+=coeff*(dx0*dx2*xk+dy0*dy2*yk);   
	  xdms[nar[1][nume]]+=coeff*(dx1*dx2*xk+dy1*dy2*yk);    
	}
    }
  else if (maillnodes.ndim==2)
    {
      for (i=0; i<kortho.nelem; i++)
	{	  
	  nume=kortho.ele[i];
	  xk=kortho.k11[i]; yk=kortho.k22[i];

	  coeff=s12/maillnodes.volume[nume];

	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  r1=fabs(*(*(coords+nca)+ne0)) * coeff;
	  r2=fabs(*(*(coords+nca)+ne1)) * coeff;
	  r3=fabs(*(*(coords+nca)+ne2)) * coeff;

	  xk=  (r1+r2+r3)*xk;
	  yk=  (r1+r2+r3)*yk;

	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2;

	  res[ne0]+= dx02*xk+dy02*yk; 
	  res[ne1]+= dx12*xk+dy12*yk;  
	  res[ne2]+= dx22*xk+dy22*yk;  	  
	  xdms[nar[0][nume]]+=dx0*dx1*xk+dy0*dy1*yk;     
	  xdms[nar[2][nume]]+=dx0*dx2*xk+dy0*dy2*yk;     
	  xdms[nar[1][nume]]+=dx1*dx2*xk+dy1*dy2*yk;    
	}
    }     
  else
    {

      for (i=0; i<kortho.nelem; i++)
	{
	  xk=kortho.k11[i]; yk=kortho.k22[i]; zk=kortho.k33[i];

	  nume=kortho.ele[i];
	  coeff=s36/maillnodes.volume[nume];

	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];ne3=nodes[3][nume];
	  c0=*coords; c1=*(coords+1); c2=*(coords+2);
	  x01 =*(c0+ne1)-*(c0+ne0); y01 =*(c1+ne1)-*(c1+ne0); z01 =*(c2+ne1)-*(c2+ne0);
	  x02 =*(c0+ne2)-*(c0+ne0); y02 =*(c1+ne2)-*(c1+ne0); z02 =*(c2+ne2)-*(c2+ne0);
	  x03 =*(c0+ne3)-*(c0+ne0); y03 =*(c1+ne3)-*(c1+ne0); z03 =*(c2+ne3)-*(c2+ne0);
	  x12 =*(c0+ne2)-*(c0+ne1); y12 =*(c1+ne2)-*(c1+ne1); z12 =*(c2+ne2)-*(c2+ne1);
          x13 =*(c0+ne3)-*(c0+ne1); y13 =*(c1+ne3)-*(c1+ne1); z13 =*(c2+ne3)-*(c2+ne1);
	  x23 =*(c0+ne3)-*(c0+ne2); y23 =*(c1+ne3)-*(c1+ne2); z23 =*(c2+ne3)-*(c2+ne2);

	  dx0=y13*z12-z13*y12; dy0=-x13*z12+z13*x12; dz0=x13*y12-y13*x12;
	  dx1=-y23*z02+z23*y02; dy1=x23*z02-z23*x02; dz1=-x23*y02+y23*x02;
	  dx2=y13*z03-z13*y03; dy2=-x13*z03+z13*x03; dz2=x13*y03-y13*x03;
	  dx3=y01*z02-z01*y02; dy3=-x01*z02+z01*x02; dz3=x01*y02-y01*x02;

	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2; dx32=dx3*dx3;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2; dy32=dy3*dy3;
	  dz02=dz0*dz0; dz12=dz1*dz1; dz22=dz2*dz2; dz32=dz3*dz3;

	  res[ne0]+=coeff*(dx02*xk+dy02*yk+dz02*zk);
	  res[ne1]+=coeff*(dx12*xk+dy12*yk+dz12*zk);    
	  res[ne2]+=coeff*(dx22*xk+dy22*yk+dz22*zk); 
	  res[ne3]+=coeff*(dx32*xk+dy32*yk+dz32*zk);  	  
	  xdms[nar[0][nume]]+=coeff*(dx0*dx1*xk+dy0*dy1*yk+dz0*dz1*zk); 
	  xdms[nar[2][nume]]+=coeff*(dx0*dx2*xk+dy0*dy2*yk+dz0*dz2*zk);
	  xdms[nar[3][nume]]+=coeff*(dx0*dx3*xk+dy0*dy3*yk+dz0*dz3*zk);
	  xdms[nar[1][nume]]+=coeff*(dx1*dx2*xk+dy1*dy2*yk+dz1*dz2*zk);
	  xdms[nar[4][nume]]+=coeff*(dx1*dx3*xk+dy1*dy3*yk+dz1*dz3*zk); 
	  xdms[nar[5][nume]]+=coeff*(dx2*dx3*xk+dy2*dy3*yk+dz2*dz3*zk);  
	}
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | madif_anisotro                                                       |
  |======================================================================| */

void madif_anisotro(struct Maillage maillnodes,
		    struct Aniso kaniso,double *res,double *xdms)
{
  int i,nca,nume;
  double s4=0.25, s12=1./12.,s36=1./36.,coeff;
  double r1,r2,r3;
  int np,nel;
  int ne0,ne1,ne2,ne3,**nodes;
  double xk,yk,zk,xyk,xzk,yzk;
  double dx0,dx1,dx2,dx3,dx02,dx12,dx22,dx32;
  double dy0,dy1,dy2,dy3,dy02,dy12,dy22,dy32;
  double dz0,dz1,dz2,dz3,dz02,dz12,dz22,dz32;
  double x01,y01,z01,x02,y02,z02,x03,y03,z03;
  double x12,y12,z12,x13,y13,z13,x23,y23,z23;
  double kxx,kxy,kyy;
  double *c0,*c1,*c2,**coords;
  int **nar;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  np=maillnodes.npoin;   
  nel=maillnodes.nelem;
  nodes=maillnodes.node;
  coords=maillnodes.coord;
  nar=maillnodes.eltarete;




  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
    {
      for (i=0; i<kaniso.nelem; i++)
	{
	  nume=kaniso.ele[i];
	  xk=kaniso.k11[i]; yk=kaniso.k22[i]; xyk=kaniso.k12[i];

	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  coeff=s4/maillnodes.volume[nume];
	  
	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2;

	  res[ne0]+= coeff*(dx02*xk+2*dy0*dx0*xyk+dy02*yk);
	  res[ne1]+= coeff*(dx12*xk+2*dy1*dx1*xyk+dy12*yk);
	  res[ne2]+= coeff*(dx22*xk+2*dy2*dx2*xyk+dy22*yk);
	  xdms[nar[0][nume]]+=coeff*(dx0*dx1*xk+(dx0*dy1+dx1*dy0)*xyk+dy0*dy1*yk);   
	  xdms[nar[2][nume]]+=coeff*(dx0*dx2*xk+(dx0*dy2+dx2*dy0)*xyk+dy0*dy2*yk);   
	  xdms[nar[1][nume]]+=coeff*(dx1*dx2*xk+(dx1*dy2+dx2*dy1)*xyk+dy1*dy2*yk);
	}
    }
  else if (maillnodes.ndim==2)
    {
      for (i=0; i<kaniso.nelem; i++)
	{

	  nume=kaniso.ele[i];
	  xk=kaniso.k11[i]; yk=kaniso.k22[i]; xyk=kaniso.k12[i];

	  coeff=s12/maillnodes.volume[nume];

	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];
	  r1=fabs(*(*(coords+nca)+ne0)) * coeff;
	  r2=fabs(*(*(coords+nca)+ne1)) * coeff;
	  r3=fabs(*(*(coords+nca)+ne2)) * coeff;
	  
	  kxx=  xk* (r1+r2+r3);
	  kxy=  xyk*(r1+r2+r3);
	  kyy=  yk* (r1+r2+r3);

	  c0=*coords; c1=*(coords+1);
	  dx0=-(*(c1+ne2)-*(c1+ne1)); dy0=*(c0+ne2)-*(c0+ne1);
	  dx1=-(*(c1+ne0)-*(c1+ne2)); dy1=*(c0+ne0)-*(c0+ne2);
	  dx2=-(*(c1+ne1)-*(c1+ne0)); dy2=*(c0+ne1)-*(c0+ne0);

	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2;

	  res[ne0]+=dx02*kxx+2*dy0*dx0*kxy+dy02*kyy; 
	  res[ne1]+=dx12*kxx+2*dy1*dx1*kxy+dy12*kyy;   
	  res[ne2]+=dx22*kxx+2*dy2*dx2*kxy+dy22*kyy;   
	  xdms[nar[0][nume]]+=dx0*dx1*kxx+(dy0*dx1+dx0*dy1)*kxy+dy0*dy1*kyy;  
	  xdms[nar[2][nume]]+=dx0*dx2*kxx+(dy0*dx2+dx0*dy2)*kxy+dy0*dy2*kyy;  
	  xdms[nar[1][nume]]+=dx1*dx2*kxx+(dy2*dx1+dx2*dy1)*kxy+dy1*dy2*kyy;   
	}
    }     
  else
    {
      for (i=0; i<kaniso.nelem; i++)
	{
	  nume=kaniso.ele[i];
	   xk=kaniso.k11[i];   yk=kaniso.k22[i];   zk=kaniso.k33[i];
	  xyk=kaniso.k12[i];  xzk=kaniso.k13[i];  yzk=kaniso.k23[i];
	      
	  ne0=nodes[0][nume];ne1=nodes[1][nume];ne2=nodes[2][nume];ne3=nodes[3][nume];
	  c0=*coords; c1=*(coords+1); c2=*(coords+2);
	  x01 =*(c0+ne1)-*(c0+ne0); y01 =*(c1+ne1)-*(c1+ne0); z01 =*(c2+ne1)-*(c2+ne0);
	  x02 =*(c0+ne2)-*(c0+ne0); y02 =*(c1+ne2)-*(c1+ne0); z02 =*(c2+ne2)-*(c2+ne0);
	  x03 =*(c0+ne3)-*(c0+ne0); y03 =*(c1+ne3)-*(c1+ne0); z03 =*(c2+ne3)-*(c2+ne0);
	  x12 =*(c0+ne2)-*(c0+ne1); y12 =*(c1+ne2)-*(c1+ne1); z12 =*(c2+ne2)-*(c2+ne1);
          x13 =*(c0+ne3)-*(c0+ne1); y13 =*(c1+ne3)-*(c1+ne1); z13 =*(c2+ne3)-*(c2+ne1);
	  x23 =*(c0+ne3)-*(c0+ne2); y23 =*(c1+ne3)-*(c1+ne2); z23 =*(c2+ne3)-*(c2+ne2);

	  dx0=y13*z12-z13*y12; dy0=-x13*z12+z13*x12; dz0=x13*y12-y13*x12;
	  dx1=-y23*z02+z23*y02; dy1=x23*z02-z23*x02; dz1=-x23*y02+y23*x02;
	  dx2=y13*z03-z13*y03; dy2=-x13*z03+z13*x03; dz2=x13*y03-y13*x03;
	  dx3=y01*z02-z01*y02; dy3=-x01*z02+z01*x02; dz3=x01*y02-y01*x02;

	  dx02=dx0*dx0; dx12=dx1*dx1; dx22=dx2*dx2; dx32=dx3*dx3;
	  dy02=dy0*dy0; dy12=dy1*dy1; dy22=dy2*dy2; dy32=dy3*dy3;
	  dz02=dz0*dz0; dz12=dz1*dz1; dz22=dz2*dz2; dz32=dz3*dz3;

	  coeff=s36/maillnodes.volume[nume];

	  res[ne0]+=coeff*(dx02*xk+dy02*yk+dz02*zk +2*dy0*dx0*xyk +2*dz0*dx0*xzk +2*dz0*dy0*yzk); 
	  res[ne1]+=coeff*(dx12*xk+dy12*yk+dz12*zk +2*dy1*dx1*xyk +2*dz1*dx1*xzk +2*dz1*dy1*yzk); 
	  res[ne2]+=coeff*(dx22*xk+dy22*yk+dz22*zk +2*dy2*dx2*xyk +2*dz2*dx2*xzk +2*dz2*dy2*yzk); 
	  res[ne3]+=coeff*(dx32*xk+dy32*yk+dz32*zk +2*dy3*dx3*xyk +2*dz3*dx3*xzk +2*dz3*dy3*yzk); 	  
	  xdms[nar[0][nume]]+=coeff*(dx0*dx1*xk+dy0*dy1*yk+dz0*dz1*zk +(dx0*dy1+dx1*dy0)*xyk +(dx0*dz1+dx1*dz0)*xzk +(dy0*dz1+dy1*dz0)*yzk); 
	  xdms[nar[2][nume]]+=coeff*(dx0*dx2*xk+dy0*dy2*yk+dz0*dz2*zk +(dx0*dy2+dx2*dy0)*xyk +(dx0*dz2+dx2*dz0)*xzk +(dy0*dz2+dy2*dz0)*yzk);
	  xdms[nar[3][nume]]+=coeff*(dx0*dx3*xk+dy0*dy3*yk+dz0*dz3*zk +(dx0*dy3+dx3*dy0)*xyk +(dx0*dz3+dx3*dz0)*xzk +(dy0*dz3+dy3*dz0)*yzk);
	  xdms[nar[1][nume]]+=coeff*(dx1*dx2*xk+dy1*dy2*yk+dz1*dz2*zk +(dx1*dy2+dx2*dy1)*xyk +(dx1*dz2+dx2*dz1)*xzk +(dy1*dz2+dy2*dz1)*yzk);
	  xdms[nar[4][nume]]+=coeff*(dx1*dx3*xk+dy1*dy3*yk+dz1*dz3*zk +(dx1*dy3+dx3*dy1)*xyk +(dx1*dz3+dx3*dz1)*xzk +(dy1*dz3+dy3*dz1)*yzk); 
	  xdms[nar[5][nume]]+=coeff*(dx2*dx3*xk+dy2*dy3*yk+dz2*dz3*zk +(dx2*dy3+dx3*dy2)*xyk +(dx2*dz3+dx3*dz2)*xzk +(dy2*dz3+dy3*dz2)*yzk);  
	}
    }
}
