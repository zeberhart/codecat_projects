/*
     This file is part of GNUnet
     (C) 2005-2013 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/identity/gnunet-identity-gtk_advertise.h
 * @author LRN
 * @author Christian Grothoff
 */

#ifndef GNUNET_IDENTITY_GTK_ADVERTISE_H
#define GNUNET_IDENTITY_GTK_ADVERTISE_H

#include "gnunet_gtk.h"


/**
 * Shutdown advertisement subsystem, this process is terminating.
 */
void
GIG_advertise_shutdown_ (void);


/**
 * Run the dialog for advertising a namespace.
 *
 * @param priv private key of the namespace to advertise
 */
void
GIG_advertise_dialog_start_ (const struct GNUNET_CRYPTO_EcdsaPrivateKey *priv);

#endif
/* GNUNET_FS_GTK_NAMESPACE_MANAGER_H */

/* end of gnunet-identity-gtk_advertise.h */
