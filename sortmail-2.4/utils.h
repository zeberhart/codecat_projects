/*	@(#)utils.h 1.2 02/03/11 falk	*/
/*	$Id: utils.h,v 1.2 2002/12/07 18:00:11 efalk Exp $	*/




#ifdef __STDC__
extern	char	*variable_expand(char *, bool keep) ;
extern	void	filename_expand(char *dst, char *src) ;
extern	int	die(int xval, char *fmt, ...) ;
extern	void	logFile(char *fmt, ...) ;
extern	void	logFilev(int level, char *fmt, ...) ;
extern	void	assfail(char *expr, char *file, int line) ;
extern	long	getLimit(char *ptr) ;
extern	char	*firstNonBlank(char *) ;
extern	char	*firstBlank(char *) ;
extern	void	removeNL(char *) ;
extern	void	tolowerstr(char *) ;
extern	void	makeBase64(unsigned char *inbuf, char *outbuf, int n) ;
extern	int	readBase64(char *inbuf, unsigned char *outbuf, int max) ;
extern	FILE	*pathOpen(char *path, char *mode) ;
extern	FILE	*tmpOpen(char *tmpfilename) ;
extern	off_t	fileSize(char *name) ;
#else
extern	char	*variable_expand() ;
extern	void	filename_expand() ;
extern	int	die() ;
extern	void	logFile() ;
extern	void	logFilev() ;
extern	void	assfail() ;
extern	long	getLimit() ;
extern	char	*firstNonBlank() ;
extern	char	*firstBlank() ;
extern	void	removeNL() ;
extern	void	tolowerstr() ;
extern	void	makeBase64() ;
extern	int	readBase64() ;
extern	FILE	*pathOpen() ;
extern	FILE	*tmpOpen() ;
extern	off_t	fileSize() ;
#endif

#ifdef	NOSTRDUP
#ifdef __STDC__
extern	char	*strdup(const char *) ;
#else
extern	char	*strdup() ;
#endif
#endif

#ifndef	HSTRERROR
#ifdef __STDC__
extern	const	char	*hsterror(int err) ;
#else
extern	char	*hsterror() ;
#endif
#endif

#ifdef	NOSETENV
#ifdef __STDC__
extern	void	setenv(char *name, char *value, int overwrite) ;
extern	void	unsetenv(char *name) ;
#else
extern	void	setenv() ;
extern	void	unsetenv() ;
#endif
#endif

#ifdef	NOSTRCASECMP
#ifdef __STDC__
extern	int	strncasecmp(char *s1, char *s1, int len) ;
#else
extern	int	strncasecmp() ;
#endif
#endif
