/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <math.h>
# include "pp_usertype.h"
# include "pp_bd.h"
# include "pp_proto.h"

struct Performances perfo;

char nomgeom[200],nomdata[200];
char nomsuit[200]; /* inusite */

/*|======================================================================|
  | SYRTHES 4.3                                        COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | pp_pre_parall                                                        |
  |         Programme principal du pre-processeur de SYRTHES parallele   |
  |======================================================================| */

main(argc,argv)
     int argc;
     char *argv[];
{                                                                     
  FILE *fmaill,*fdata;

  char *s;
  char nompart[200];
  rp_int numarg,syrdata=0;
  rp_int nbrrc,nbrp;
  
  rp_int npart,*numdome,*numdomebord,nbeltmaxpart,visua=0,visub=0,nbliredom=0;
  rp_int tool_part=0; /* 0=scotch, 1=metis,  */
  char nomdom[200];

  struct Maillage maillnodes;
  struct MaillageBord maillnodebord;

  struct NodeCouple *listpairesrc=NULL;
  struct NodeCouple *listpairesperio=NULL;

  rp_int ndiele,ok_nompart,ok_nomgeom;
  rp_int i,j;


  /* initialisation des performances */
  /* ------------------------------- */
  perfo.cpu_deb=cpusyrt();
  perfo.mem_max=perfo.mem_max=0;


  /* ----------------------------------------------------------------------------------- */
  /*      Interpretation de la ligne de commande                                         */
  /* ----------------------------------------------------------------------------------- */

  s=(char*)malloc(10*sizeof(char));
  if (argc==1)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n pp_syrthes : pre-processeur pour la version parallele de SYRTHES\n");
	  printf(" --> taper << pp_syrthes -h >>  pour avoir des informations\n\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n pp_syrthes : pre-processor for SYRTHES parallel version\n");
	  printf(" --> type << pp_syrthes -h >>  for information\n\n");
	}
      exit(1);
    }

  numarg=0;
  npart=-1;

  strcpy(nompart,"\0"); ok_nompart=0;
  strcpy(nomgeom,"\0"); ok_nomgeom=0;

  while (++numarg < argc) {

    s = argv[numarg];

    if (strcmp (s, "-h") == 0) 
      {
	if (SYRTHES_LANG == FR)
	  {
	    printf("\n pp_syrthes : pre-processeur pour la version parallele de SYRTHES\n");
	    printf("                partitionnement du maillage initial\n\n");
	    printf("             Usage : pp_syrthes [-h]  -n nb_part|-e nb_elt    -m maillage  [-o maillage_decoupe]\n");
	    printf("                        -->  -h         : mode d'emploi\n");
	    printf("                        -->  -v         : creation d'un fichier Ensight ASCII de visualisation des partitions\n");
	    printf("                        -->  -b         : creation d'un fichier Ensight gold de visualisation des partitions\n");
	    printf("                        -->  -n nb_part : nombre de parttions a creer\n");
	    printf("                        -->  -e nb_elt  : nombre max d'elements par partition\n\n");
	    
	    printf("                        -->  -l nombre  : lecture du decoupage sur 'nombre' fichiers\n");
	    printf("                        -->  -f prefixe : prefixe des fichiers (dom.1 dom.2 dom.3 ...\n\n");
	    
	    printf("                        -->  -m maill   : nom du maillage a decouper (.syr)\n");
	    printf("                        -->  -o nompart : prefixe du nom des maillages decoupes\n\n");
	    printf("                        -->  -d syrthes.data : nom du fichier de donnees\n\n");
	    printf("                                               fichier indispensable en cas de presence de\n");
	    printf("                                               resistance de contact ou de periodicite\n");
	    printf("                                               Sinon, il n'est pas utilise\n\n");
	    printf("                        --> --part metis : partitionnement du maillage avec METIS 5\n");
	    printf("                        --> --part scotch: partitionnement du maillage avec SCOTCH (defaut)\n\n");
	    printf("           Exemple : pp_syrthes -n 3 -m carre.syr -o carre\n");
	    printf("                ou : pp_syrthes -n 3 -m carre.syr -d syrthes_data.syd -o carre\n");
	    printf("                        --> on obtient carre_00003part00000.syr\n");
	    printf("                                       carre_00003part00001.syr\n");
	    printf("                                       carre_00003part00002.syr\n");
	  }
	else if (SYRTHES_LANG == EN)
	  {
	    printf("\n pp_syrthes : pre-processor for SYRTHES parallel version\n");
	    printf("                splitting of the initial mesh\n\n");
	    printf("             Usage : pp_syrthes [-h]  -n nb_part|-e nb_elt    -m maillage  [-o maillage_decoupe]\n");
	    printf("                        -->  -h         : help\n");
	    printf("                        -->  -v         : create an Ensight ASCII vizualisation file with the partitions\n");
	    printf("                        -->  -b         :  create an Ensight gold vizualisation file with the partitions\n");
	    printf("                        -->  -n nb_part : number of partions to create\n");
	    printf("                        -->  -e nb_elt  : maximum number of element per partition\n\n");
	    
	    printf("                        -->  -l number  : read of number 'domain' files\n");
	    printf("                        -->  -f prefixe : prefix of the files (dom.1 dom.2 dom.3 ...\n\n");
	    
	    printf("                        -->  -m mesh    : name of the initial mesh file (.syr)\n");
	    printf("                        -->  -o nompart : prefix of the destination files\n\n");
	    printf("                        -->  -d syrthes_data.syd : name of the SYRTHES data file\n\n");
	    printf("                                               only if calculation needs periodicity or contact resistances\n\n");
	    printf("                        --> --toolpart metis : mesh partitioning thanks to METIS 5\n");
	    printf("                        --> --toolpart scotch: mesh partitioning thanks to SCOTCH (default)\n\n");
	    printf("           Example : pp_syrthes -n 3 -m carre.syr -o carre\n");
	    printf("                or : pp_syrthes -n 3 -m carre.syr -d syrthes.data -o carre\n");
	    printf("                        --> results :  carre_00003part00000.syr\n");
	    printf("                                       carre_00003part00001.syr\n");
	    printf("                                       carre_00003part00002.syr\n");
	  }
	exit(1);
      }
    else if (strcmp (s, "-v") == 0) 
      {
	visua=1;
      }
    else if (strcmp (s, "-b") == 0) 
      {
	visub=1;
      }
    else if (strcmp (s, "-n") == 0) 
      {
	s = argv[++numarg];
	npart=atoi(s);
	nbeltmaxpart=0;
      }
    else if (strcmp (s, "-e") == 0) 
      {
	s = argv[++numarg];
	nbeltmaxpart=atoi(s);
	npart=0;
      }
    else if (strcmp (s, "-l") == 0) 
      {
	s = argv[++numarg];
	nbliredom=atoi(s);
	npart=nbeltmaxpart=0;
      }
    else if (strcmp (s, "-f") == 0) 
      {
	s = argv[++numarg];
	strcpy(nomdom,argv[numarg]);
      }
    else if (strcmp (s, "-m") == 0) 
      {
	s = argv[++numarg];
	strcpy(nomgeom,argv[numarg]);
	if ((fmaill=fopen(argv[numarg],"r")) == NULL)
	  {
	    if (SYRTHES_LANG == FR) printf("Impossible d'ouvrir le fichier %s\n",argv[numarg]);	
	    else if (SYRTHES_LANG == EN) printf("Unable to open file : %s\n",argv[numarg]);	
	    exit(1) ;
	  }
	ok_nomgeom=1;
      }
    else if (strcmp (s, "-o") == 0) 
      {
	s = argv[++numarg];
	strcpy(nompart,argv[numarg]);
	ok_nompart=1;
      }
    else if (strcmp (s, "-d") == 0) 
      {
	s = argv[++numarg];
	strcpy(nomdata,argv[numarg]);
	if ((fdata=fopen(argv[numarg],"r")) == NULL)
	  {
	    if (SYRTHES_LANG == FR) printf("Impossible d'ouvrir le fichier %s\n",argv[numarg]);	
	    else if (SYRTHES_LANG == EN) printf("Unable to open file : %s\n",argv[numarg]);	
	    exit(1) ;
	  }
	syrdata=1;
      }
    else if ((strcmp (s, "-t") == 0) || (strcmp (s, "--toolpart") == 0))
      {
        if (numarg+1 < argc) {
          s = argv[++numarg];
	  if (strncmp(s,"scotch",6)==0) tool_part=0;
	  else if (strncmp(s,"metis",5)==0) tool_part=1;
	  else{
	    if (SYRTHES_LANG == FR) printf("Mauvaise option pour --part : [scotch] ou [metis] \n");	
	    else if (SYRTHES_LANG == EN) printf("Bad option for --part :  [scotch] or [metis] \n");	
	    exit(1) ;
	  }
	}
      }
  } /* fin du while */

  /* ------------------------------------------------------------------------ */
  /*                                  Banniere                                */
  /* ------------------------------------------------------------------------ */

  printf("    *****************************************************\n");
  printf("    *                                                   *\n");
  printf("    *   SSSS YY  YY RRRRR  TTTTTT HH   HH  EEEEE  SSSS  *\n");
  printf("    *   SS    YYYY  RR  RR   TT   HH   HH  EE     SS    *\n");
  printf("    *   SSS    YY   RRRRR    TT   HHHHHHH  EEE    SSS   *\n");
  printf("    *     SS   YY   RR  RR   TT   HH   HH  EE       SS  *\n");
  printf("    *   SSSS   YY   RR   RR  TT   HH   HH  EEEEE  SSSS  *\n");
  printf("    *                                                   *\n");
  printf("    *****************************************************\n");
  if (SYRTHES_LANG == FR)      
    printf("    *     PRE-PROCESSEUR  POUR  TRAITEMENT  PARALLELE   * \n");
  else if (SYRTHES_LANG == EN) 
    printf("    *      PRE-PROCESSOR  FOR  PARALLEL  COMPUTATION    * \n");
  printf("    *****************************************************\n\n\n");


  /* ------------------------------------------------------------------------ */
  /*                        verifications                                     */
  /* ------------------------------------------------------------------------ */
  if (!ok_nomgeom)
    {
      if (SYRTHES_LANG == FR)
        {
          printf("\n\n %%%% ERREUR : vous n'avez pas indique le nom du fichier de maillage\n");
          printf("             --> utilisez : -m nom_maillage\n");
        }
      else if (SYRTHES_LANG == EN)
        {
          printf("\n\n %%%% ERROR : the name of the mesh file is not defined\n");
          printf("             --> use :  -m mesh_file\n");
        }
      exit (1);
    }


  if (!ok_nompart)
    {
      if (SYRTHES_LANG == FR)
        {
          printf("\n\n %%%% ERREUR : vous n'avez pas indique le prefixe du nom des maillages decoupes\n");
          printf("             --> utilisez : -o nom_partitions\n");
        }
      else if (SYRTHES_LANG == EN)
        {
          printf("\n\n %%%% ERROR : the name of result partitioned mesh file is not defined\n");
          printf("             --> use :  -o partitioned_file\n");
        }
      exit (1);
    }

  if (npart<0)
    {
      if (SYRTHES_LANG == FR)
        {
          printf("\n\n %%%% ERREUR : le nombre de partitions soiuhaitees n'est pas indique\n");
          printf("             --> utilisez : -n nbre_partitions\n");
        }
      else if (SYRTHES_LANG == EN)
        {
          printf("\n\n %%%% ERROR : the number of partitions is not defined\n");
          printf("             --> use :  -n partitions_number\n");
        }
      exit (1);
    }


  /* ------------------------------------------------------------------------ */
  /*                        lecture du maillage solide                        */
  /* ------------------------------------------------------------------------ */


  if (!strncmp(nomgeom+strlen(nomgeom)-4, ".des",4))
    lire_simail (&maillnodes,&maillnodebord);

  else if (!strncmp(nomgeom+strlen(nomgeom)-4, ".unv",4))
    lire_ideas  (&maillnodes,&maillnodebord);

  else if (!strncmp(nomgeom+strlen(nomgeom)-5, ".syrb",5) ||
	   !strncmp(nomgeom+strlen(nomgeom)-4, ".syr",4))
      lire_syrthes(&maillnodes,&maillnodebord);

  if (maillnodes.ndim==2)
    {maillnodes.ndmat=3; maillnodes.nbface=3;maillnodes.ndiele=2;}
  else
    {maillnodes.ndmat=4; maillnodes.nbface=4;maillnodes.ndiele=3;}

  fclose(fmaill);

  fflush(stdout);


  /* decoupage du maillage volumique */
  /* ------------------------------- */
  ndiele=maillnodes.ndim;
  numdome=(rp_int*)malloc(maillnodes.nelem*sizeof(rp_int));

  if (npart>0)
    {
#if defined(_METIS_)
      if (tool_part==1) pp_metis(maillnodes,npart,numdome);
#endif

#if defined(_SCOTCH_)
      if (tool_part==0) pp_scotch(maillnodes,npart,numdome);
#endif
    }

  else if(nbeltmaxpart>0)
    pp_geopart(maillnodes.nelem,maillnodes.npoin,ndiele,maillnodes.coord,
	       maillnodes.node,nbeltmaxpart,&npart,numdome);
  else
    pp_lire_domaines(nomdom,nbliredom,maillnodes.nelem,numdome,&npart);
  
  /*   pp_gateau(maillnodes.nelem,npart,numdome); */

  /* decoupage du maillage de bord   */
  /* ------------------------------- */
  numdomebord=(rp_int*)malloc(maillnodebord.nelem*sizeof(rp_int));
  pp_decoupbord(maillnodes,maillnodebord,numdome,numdomebord);


  /* -------------------------------------------------------------------- */
  /* si besoin, ecriture d'un fichier ensight pour la visu des partitions */
  /* -------------------------------------------------------------------- */
  if (visua) 
    pp_ecrire_ensight(maillnodes,maillnodebord,ndiele,npart,numdome,numdomebord);
  else if (visub) 
    pp_ecrire_ensight_gold(maillnodes,maillnodebord,ndiele,npart,numdome,numdomebord);

  /* -------------------------------------------------------------------- */
  /* si besoin, lecture de qq donnees dans le fichier syrthes.data        */
  /* -------------------------------------------------------------------- */
  if (syrdata)
    { 
      nbrrc=nbrp=0;
      pp_exist_perio_rc(fdata,&nbrrc,&nbrp);

      if (nbrrc) /* il y a des resistances de contact a traiter */
	pp_contact(fdata,maillnodes,maillnodebord,&listpairesrc);

      if (nbrp) /* il y a de la periodicite a traiter */
	pp_perio(fdata,maillnodes,maillnodebord,&listpairesperio);
    }

  /* -------------------------------------------------------------------- */
  /*       creation des tables pour le parallelisme dans Syrthes          */
  /* -------------------------------------------------------------------- */
  pp_cree_tables_parall(maillnodes,maillnodebord,npart,numdome,numdomebord,nompart,
			listpairesrc,listpairesperio);

  printf("\n\n\n");
  printf("    *****************************************************\n");
  printf("    *                  S Y R T H E S                    *\n");
  printf("    *****************************************************\n");
  if (SYRTHES_LANG == FR)      
    printf("    *      FIN NORMALE DU PRE-PROCESSING PARALLELE      *\n");
  else if (SYRTHES_LANG == EN) 
    printf("    *   END OF PRE-PROCESSOR FOR PARALLEL COMPUTATION   * \n");
  printf("    *****************************************************\n\n");


  exit(0);

}  

