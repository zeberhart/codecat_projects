/* splash.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#ifndef PROGRESSWINDOW__H
#define PROGRESSWINDOW__H
#include "common.h"

typedef struct splash_t {
  struct apwalapp_t *apwal;
  GtkWidget *window;
  GtkWidget *progressbar;
  GdkCursor *busycursor;
  gboolean   showed;
} splash_t;

splash_t* splash_new(struct apwalapp_t *apwal);
void splash_show(splash_t *splash);
void splash_hide(splash_t *splash);
void splash_update(splash_t *splash);

#endif /* PROGRESSWINDOW__H */
// ----------------------------------------------------------------------------
