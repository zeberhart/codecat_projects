# SALOME_Selector.py

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import SIGNAL

class SALOME_Selector():
        
    def init_table(self, table, columnRef, columnComment) :
        self.table = table
        self.columnRef = columnRef
        self.columnComment = columnComment
        for i in range(self.table.rowCount()) :
            item0 = QtGui.QTableWidgetItem()
            item0.setText("")
            self.table.setItem(i, self.columnRef, item0)
            item = QtGui.QTableWidgetItem()
            item.setText("")
            self.table.setItem(i, self.columnComment, item)
        QtCore.QObject.connect(self.table, QtCore.SIGNAL("cellClicked(int,int)"), self.slotAddFromSalome)

    def slotAddFromSalome(self):
        """
        When SYRTHES GUI is embedded in the Salome desktop.
        """
        from SalomeHandlerGeneral import BoundaryGroup
        loc,ref = BoundaryGroup()
        print "1 SyrthesBoundary_conditions : loc = ",loc
        print "1 SyrthesBoundary_conditions : ref = ",ref
        listOfSelectedItems = self.table.selectedItems()
        print "listOfSelectedItems = ",listOfSelectedItems
        item = QtGui.QTableWidgetItem()
        print "SyrthesBoundary_conditions : self.table.rowCount() = ", self.table.rowCount()
        for item0 in listOfSelectedItems :
            if self.table.column(item0) == self.columnRef :
                print "SyrthesBoundary_conditions : ref = ",ref
                if self.table.item(item0.row(),self.columnRef) != None :
                    print "SyrthesBoundary_conditions :self.table.item(item0.row(),self.columnRef).text() = ",self.table.item(item0.row(),self.columnRef).text()
                    if ref not in str(self.table.item(item0.row(),self.columnRef).text()) :
                        ref  = str(self.table.item(item0.row(),self.columnRef).text())  + ' ' + ref
                    else :
                        return
                if self.table.item(item0.row(),self.columnComment) != None :
                    print "SyrthesBoundary_conditions :self.table.item(item0.row(),self.columnComment).text() = ",self.table.item(item0.row(),self.columnComment).text()
                    if loc not in str(self.table.item(item0.row(),self.columnComment).text()) :
                        loc  = str(self.table.item(item0.row(),self.columnComment).text()) + ' ' + loc
                    else :
                        return
                if ref != "" : item0.setText(ref)
                if loc != "" : item.setText(loc)
                print "SyrthesBoundary_conditions : item0.row() = ",item0.row()
                it = self.table.item(item0.row(),item0.column())
                print "SyrthesBoundary_conditions :it.text()= ",str(it.text())
                self.table.setItem(item0.row(),self.columnComment,item)
                #print "SyrthesBoundary_conditions :self.table(item.row(),item.column()).text()= ",str(self.table(item.row(),item.column()).text())

            if self.table.column(item0) == self.columnComment :
                print "SyrthesBoundary_conditions : loc = ",loc

                if self.table.item(item0.row(),self.columnComment) != None :
                    print "SyrthesBoundary_conditions :self.table.item(item0.row(),self.columnComment).text() = ",self.table.item(item0.row(),self.columnComment).text()
                    if loc not in str(self.table.item(item0.row(),self.columnComment).text()) :
                        loc  = str(self.table.item(item0.row(),self.columnComment).text())+ ' ' + loc
                    else :
                        return
                if self.table.item(item0.row(),self.columnRef) != None :
                    print "SyrthesBoundary_conditions :self.table.item(item0.row(),self.columnRef).text() = ",self.table.item(item0.row(),self.columnRef).text()
                    if ref not in str(self.table.item(item0.row(),self.columnRef).text()) :
                        ref  = str(self.table.item(item0.row(),self.columnRef).text()) + ' ' + ref
                    else :
                        return
                if loc != "" : item0.setText(loc)
                if ref != "" : item.setText(ref)
                print "SyrthesBoundary_conditions : item0.row() = ",item0.row()
                self.table.setItem(item0.row(),self.columnRef,item)
   