**************************************************************
  syrthes-pp : pre-processeur of parallel SYRTHES version
**************************************************************

syrthes-pp is a tool to split initial mesh. 



1. How to build syrthes-pp
==========================

If you want to split only with METIS  : 
     - edit the Makefile and put the path for your own METIS libraries 
     - type : make PART_METIS=yes

If you want to split only with SCOTCH  : 
     - edit the Makefile and put the path for your own SCOTCH libraries 
     - type : make PART_SCOTCH=yes
     
If you want to split with METIS or SCOTCH  : 
     - edit the Makefile and put the path for your own METIS and SCOTCH libraries 
     - type : make PART_METIS=yes PART_SCOTCH=yes 



2. How to use syrthes-pp
========================

  syrthes-pp [-h]  -n nb_part|-e nb_elt    -m maillage  [-o maillage_decoupe]
  
	     -->  -h	     : help
	     -->  -v	     : create an Ensight ASCII vizualisation file with the partitions
	     -->  -b	     : create an Ensight gold vizualisation file with the partitions
	     -->  -n nb_part : number of partions to create
	     -->  -e nb_elt  : maximum number of element per partition

	     -->  -l number  : read of number 'domain' files
	     -->  -f prefixe : prefix of the files (dom.1 dom.2 dom.3 ...)

	     -->  -m mesh    : name of the initial mesh file (.syr)
	     -->  -o nompart : prefix of the destination files
	     -->  -d syrthes.data : name of the SYRTHES data file
				    only if calculation needs periodicity or contact resistances
	     --> --toolpart metis : mesh partitioning thanks to METIS 5
	     --> --toolpart scotch: mesh partitioning thanks to SCOTCH (default)
	     
  Example : syrthes-pp -n 3 -m carre.syr -o carre
       or : syrthes-pp -n 3 -m carre.syr -d syrthes.data -o carre
	     
	     --> results :  carre_part00000.syr
			    carre_part00001.syr
			    carre_part00002.syr
