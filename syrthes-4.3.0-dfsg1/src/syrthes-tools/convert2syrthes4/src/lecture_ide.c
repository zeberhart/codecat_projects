/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 1988-2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/
/* Conversions de maillages */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "convert2syrthes.h"

/* longueur maximale d'une ligne du maillage a lire */
#define LONGUEUR_LIGNE 1001

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Ouverture du fichier                                                |
  |======================================================================| */
int ouvrir_ide(struct typ_maillage *maillage, /* INOUT structure de maillage */
               char *nomfich) {               /* IN nom de fichier maillage a lire */
   (*maillage).fichier_ext = fopen(nomfich,"r"); /* descripteur de fichier d'entree */
   if ( (*maillage).fichier_ext == NULL ) {
     printf("ERROR : unable to open file (%s)\n",nomfich);
     return 1;
   }
   printf("Ideas universal file opened : %s\n",nomfich); 
   return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Fermeture du fichier                                                |
  |======================================================================| */
int fermer_ide(struct typ_maillage *maillage) {
   if ( (*maillage).fichier_ext == NULL ) {
     printf("ERROR : unable to close the file .unv\n");
     return 1;
   }
   printf("Ideas universal file closed\n");
   fclose((*maillage).fichier_ext);
   return 0;
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture d'un maillage au format Ideas                                |
  |======================================================================| */
int lire_ideas (struct typ_maillage *maillage)
{
  int i,j,ne,n1,n2,n3,n4,nr,nn,nbnod,ok;
  int n,ne111,ne91,ne21;
  int fincoo,finnod111,finnod91,finnod21;
  int **node91,**node111,**node21,*nref111,*nref91,*nref21,*nref;
  double x,y,z,**coo;
  char ch[90],ch1[90],ch2[90],ch3[90];


  /* Allocations de base avant redimenssionnement en fonction de ce qui est lu */

  finnod111=finnod91=finnod21=10000;
  fincoo=10000;

  coo=(double**)malloc(3*sizeof(double*));
  for (i=0;i<3;i++)  {
    coo[i]=(double*)malloc(fincoo*sizeof(double)); 
    if (!coo[i]) {
      printf("\n %%%% ERROR lire_ideas : Memory allocation problem\n");
      return 1;} 
  }
  
  node111=(int**)malloc(4*sizeof(int*));
  for (i=0;i<4;i++) {
    node111[i]=(int*)malloc(finnod111*sizeof(int)); 
    if (!node111[i]) {
      printf("\n %%%% ERROR lire_ideas : Memory allocation problem\n");
      return 1;} 
  }
  
  node91=(int**)malloc(3*sizeof(int*));
  for (i=0;i<3;i++) {
    node91[i]=(int*)malloc(finnod91*sizeof(int)); 
    if (!node91[i]) {
      printf("\n %%%% ERROR lire_ideas : Memory allocation problem\n");
      return 1;} 
  }

  node21=(int**)malloc(2*sizeof(int*));
  for (i=0;i<2;i++) {
    node21[i]=(int*)malloc(finnod21*sizeof(int)); 
    if (!node21[i]) {
      printf("\n %%%% ERROR lire_ideas : Memory allocation problem\n");
      return 1;} 
  }

  nref111=(int*)malloc(finnod111*sizeof(int));   
  nref91=(int*)malloc(finnod91*sizeof(int));     
  nref21=(int*)malloc(finnod21*sizeof(int));     
  nref=(int*)malloc(fincoo*sizeof(int));     
  if (!nref111 || !nref91 || !nref21|| !nref) {
    printf("\n %%%% ERROR lire_ideas : Memory allocation problem\n");
    return 1;}

  /* lecture du maillage */
  n=ne111=ne91=ne21=0;

  while(fgets(ch1,90,(*maillage).fichier_ext))
    {
      sscanf(ch1,"%s",ch);
      if (strcmp(ch,"-1")==0)
	{
	  if (fgets(ch1,90,(*maillage).fichier_ext))
	    {
	      sscanf(ch1,"%s",ch);
	      if (strcmp(ch,"-1")==0)
		{
		  fgets(ch1,90,(*maillage).fichier_ext);
		  sscanf(ch1,"%s",ch);
		}
	      /* lecture des coordonnees des noeuds */
              /* ---------------------------------- */
	      if (strcmp(ch,"2411")==0 || strcmp(ch,"781")==0) 
		{
		  fgets(ch3,90,(*maillage).fichier_ext);
		  sscanf(ch3,"%s",ch1);
		  while (strcmp(ch1,"-1")!=0)
		    {
		      sscanf(ch3,"%10d%10d%10d%10d",&nn,&nn,&nn,&nr);
		      fgets(ch2,90,(*maillage).fichier_ext);
		      ch2[21]=ch2[46]=ch2[71]='e';
		      sscanf(ch2,"%lf%lf%lf",&x,&y,&z);
		      if (n>fincoo-1)
			{
                            /* le tableau est trop petit, on l'agrandit */
                            fincoo+=20000;
                            coo[0]=(double*)realloc(coo[0],fincoo*sizeof(double));
                            coo[1]=(double*)realloc(coo[1],fincoo*sizeof(double));
                            coo[2]=(double*)realloc(coo[2],fincoo*sizeof(double));
			    nref=(int*)realloc(nref,fincoo*sizeof(int));
                          }
		      coo[0][n]=x; coo[1][n]=y; coo[2][n]=z; nref[n]=nr;
		      n += 1;
		      fgets(ch3,90,(*maillage).fichier_ext);
		      sscanf(ch3,"%s",ch1);
		    }
		}
	      else if  (strcmp(ch,"2412")==0)
		/* lecture des elements */
		/* -------------------- */
		{
		  fgets(ch3,90,(*maillage).fichier_ext);
		  sscanf(ch3,"%s",ch1);
		  while (strcmp(ch1,"-1")!=0)
		    {
		      sscanf(ch3,"%10d%10d%10d%10d%10d%10d",&n1,&n2,&n3,&n4,&nr,&nbnod);
		      if (n2==111) /* tetraedres */
			{
			  if (ne111>finnod111-1)
			    {
			      finnod111+=20000;
			      for (i=0;i<nbnod;i++)
				node111[i]=(int*)realloc(node111[i],finnod111*sizeof(int));
			      nref111=(int*)realloc(nref111,finnod111*sizeof(int));
			    }
			  nref111[ne111]=nr;
			  fscanf((*maillage).fichier_ext,"%10d%10d%10d%10d",(node111[0]+ne111),(node111[1]+ne111),
				 (node111[2]+ne111),(node111[3]+ne111));
			  ne111++;
			}
		      else if (n2==91) /* triangles */
			{
			  if (ne91>finnod91-1)
			    {
			      finnod91+=20000;
			      for (i=0;i<nbnod;i++)
				node91[i]=(int*)realloc(node91[i],finnod91*sizeof(int));
			      nref91=(int*)realloc(nref91,finnod91*sizeof(int));
			    }
			  nref91[ne91]=nr;
			  fscanf((*maillage).fichier_ext,"%10d%10d%10d",(node91[0]+ne91),(node91[1]+ne91),(node91[2]+ne91));
			  ne91++;
			}
		      else if (n2==11 || n2==21) /* poutres */
			{
			  if (ne21>finnod21-1)
			    {
			      finnod21+=20000;
			      for (i=0;i<nbnod;i++)
				node21[i]=(int*)realloc(node21[i],finnod21*sizeof(int));
			      nref21=(int*)realloc(nref21,finnod21*sizeof(int));
			    }
			  nref21[ne21]=nr;
			  fgets(ch3,90,(*maillage).fichier_ext); /* on passe une ligne */
			  fscanf((*maillage).fichier_ext,"%10d%10d",(node21[0]+ne21),(node21[1]+ne21));
			  ne21++;
			}
		      else
			{
			  printf(" Ideas mesh file : wrong type of elements found (type=%d)\n",n2);
			  return 1;
			}
		      fgets(ch3,90,(*maillage).fichier_ext);fgets(ch3,90,(*maillage).fichier_ext);
		      sscanf(ch3,"%s",ch1);
		    }
		}
	    }
	}
    }

  /* la dimension est donnee en parametre (maillage->dimension) */
  ok=1;
  if ( (maillage->dimension==3 && ne111 && !ne91) ||
       (maillage->dimension==2 && ne91  && !ne21) ){
    printf(" Ideas mesh file : no boundary elements in the file\n\n");
    ok=0; 
  }
  if ( (maillage->dimension==3 && !ne111 && !ne91) ||
       (maillage->dimension==2 && !ne91  && !ne21) ){
    ok=0;
    printf(" Ideas mesh file : wrong type of elements used\n\n");
  }

  if (!ok)    return 1;



  if (ne111) /* s'il y a des tetra --> maillage vol tetra + bord tria */
    {
      maillage->dim_elem=3;
      maillage->nbnoeuds=n; 
      maillage->nbelem=ne111;
      maillage->nbnoeuds_par_elem=4;
      maillage->nbelem_de_bord=ne91;

      nref111=(int*)realloc(nref111,maillage->nbelem*sizeof(int)); 
      nref  =(int*)realloc(nref,maillage->nbnoeuds*sizeof(int));
      nref91=(int*)realloc(nref91,maillage->nbelem_de_bord*sizeof(int));

      maillage->xcoord=(double*)realloc(coo[0],maillage->nbnoeuds*sizeof(double));
      maillage->ycoord=(double*)realloc(coo[1],maillage->nbnoeuds*sizeof(double));
      maillage->zcoord=(double*)realloc(coo[2],maillage->nbnoeuds*sizeof(double));

      maillage->coul_noeud=nref;
      maillage->coul_elem=nref111;
      maillage->coul_elembord=nref91;

      /* pour la connectivite, il faut changer l'ordre du stockage */
      maillage->liste_elem=(int**)malloc(maillage->nbelem*sizeof(int*));
      for (i=0;i<maillage->nbelem;i++) {
	maillage->liste_elem[i]=(int*)malloc(maillage->nbnoeuds_par_elem*sizeof(int)); 
	if (!maillage->liste_elem[i]){
	  printf("ERROR : allocation error for the connectivity\n");
	  return 1;
	}
      }

      maillage->liste_elembord=(int**)malloc(maillage->nbelem_de_bord*sizeof(int*));
      for (i=0;i<maillage->nbelem_de_bord;i++) {
	maillage->liste_elembord[i]=(int*)malloc(maillage->dimension*sizeof(int)); 
	if (!maillage->liste_elembord[i]){
	  printf("ERROR : allocation error for the connectivity\n");
	  return 1;
	}
      }

      for (i=0;i<maillage->nbelem;i++)
	for (j=0;j<maillage->nbnoeuds_par_elem;j++)
	  maillage->liste_elem[i][j]=node111[j][i];
      
      for (i=0;i<maillage->nbelem_de_bord;i++)
	for (j=0;j<maillage->dimension;j++)
	  maillage->liste_elembord[i][j]=node91[j][i];

      for (j=0;j<4;j++) free(node111[j]);
      free(node111);
      for (j=0;j<3;j++) free(node91[j]);
      free(node91);


    }
  else if (ne91 && maillage->dimension==2) /* 2D maillage vol tria + bord segment */
    {        
      maillage->dim_elem=2;
      maillage->nbnoeuds=n; 
      maillage->nbelem=ne91;
      maillage->nbnoeuds_par_elem=3;
      maillage->nbelem_de_bord=ne21;

      maillage->xcoord=(double*)realloc(coo[0],maillage->nbnoeuds*sizeof(double));
      maillage->ycoord=(double*)realloc(coo[1],maillage->nbnoeuds*sizeof(double));
      free(coo[2]);  

      nref91=(int*)realloc(nref91,maillage->nbelem*sizeof(int));
      nref21=(int*)realloc(nref21,maillage->nbelem_de_bord*sizeof(int));
      nref  =(int*)realloc(nref,maillage->nbnoeuds*sizeof(int));

      maillage->coul_noeud=nref;
      maillage->coul_elem=nref91;
      maillage->coul_elembord=nref21;

      /* pour la connectivite, il faut changer l'ordre du stockage */
      maillage->liste_elem=(int**)malloc(maillage->nbelem*sizeof(int*));
      for (i=0;i<maillage->nbelem;i++) {
	maillage->liste_elem[i]=(int*)malloc(maillage->nbnoeuds_par_elem*sizeof(int)); 
	if (!maillage->liste_elem[i]){
	  printf("ERROR : allocation error for the connectivity\n");
	  return 1;
	}
      }

      maillage->liste_elembord=(int**)malloc(maillage->nbelem_de_bord*sizeof(int*));
      for (i=0;i<maillage->nbelem_de_bord;i++) {
	maillage->liste_elembord[i]=(int*)malloc(maillage->dimension*sizeof(int)); 
	if (!maillage->liste_elembord[i]){
	  printf("ERROR : allocation error for the connectivity\n");
	  return 1;
	}
      }

      for (i=0;i<maillage->nbelem;i++)
	for (j=0;j<maillage->nbnoeuds_par_elem;j++)
	  maillage->liste_elem[i][j]=node91[j][i];
      
      for (i=0;i<maillage->nbelem_de_bord;i++)
	for (j=0;j<maillage->dimension;j++)
	  maillage->liste_elembord[i][j]=node21[j][i];

      for (j=0;j<3;j++) free(node91[j]);
      free(node91);
      for (j=0;j<2;j++) free(node21[j]);
      free(node21);

    }
  else if (ne91 && maillage->dimension==3) /* s'il n'y a que des triangles en 3D --> pour le rayt */
    {         
      maillage->dim_elem=2;
      maillage->nbnoeuds=n; 
      maillage->nbelem=ne91;
      maillage->nbnoeuds_par_elem=3;
      maillage->nbelem_de_bord=0;

      nref  =(int*)realloc(nref,maillage->nbnoeuds*sizeof(int));
      nref91=(int*)realloc(nref91,maillage->nbelem*sizeof(int));

      maillage->xcoord=(double*)realloc(coo[0],maillage->nbnoeuds*sizeof(double));
      maillage->ycoord=(double*)realloc(coo[1],maillage->nbnoeuds*sizeof(double));
      maillage->zcoord=(double*)realloc(coo[2],maillage->nbnoeuds*sizeof(double));

      maillage->coul_noeud=nref;
      maillage->coul_elem=nref91;
      maillage->coul_elembord=NULL;

      /* pour la connectivite, il faut changer l'ordre du stockage */
      maillage->liste_elem=(int**)malloc(maillage->nbelem*sizeof(int*));
      for (i=0;i<maillage->nbelem;i++) {
	maillage->liste_elem[i]=(int*)malloc(maillage->nbnoeuds_par_elem*sizeof(int)); 
	if (!maillage->liste_elem[i]){
	  printf("ERROR : allocation error for the connectivity\n");
	  return 1;
	}
      }

      for (i=0;i<maillage->nbelem;i++)
	for (j=0;j<maillage->nbnoeuds_par_elem;j++)
	  maillage->liste_elem[i][j]=node91[j][i];
      
      for (j=0;j<3;j++) free(node91[j]);
      free(node91);
    }
  else if (ne21) /* s'il n'y a que des segments -->  maillage ray segment */
    {            
      maillage->dim_elem=1;
      maillage->nbnoeuds=n; 
      maillage->nbelem=ne21;
      maillage->nbnoeuds_par_elem=2;
      maillage->nbelem_de_bord=0;

      maillage->xcoord=(double*)realloc(coo[0],maillage->nbnoeuds*sizeof(double));
      maillage->ycoord=(double*)realloc(coo[1],maillage->nbnoeuds*sizeof(double));
      free(coo[2]);  

      nref21=(int*)realloc(nref21,maillage->nbelem*sizeof(int));
      nref  =(int*)realloc(nref,maillage->nbnoeuds*sizeof(int));

      maillage->coul_noeud=nref;
      maillage->coul_elem=nref21;
     
      /* pour la connectivite, il faut changer l'ordre du stockage */
      maillage->liste_elem=(int**)malloc(maillage->nbelem*sizeof(int*));
      for (i=0;i<maillage->nbelem;i++) {
	maillage->liste_elem[i]=(int*)malloc(maillage->nbnoeuds_par_elem*sizeof(int)); 
	if (!maillage->liste_elem[i]){
	  printf("ERROR : allocation error for the connectivity\n");
	  return 1;
	}
      }

      for (i=0;i<maillage->nbelem;i++)
	for (j=0;j<maillage->nbnoeuds_par_elem;j++)
	  maillage->liste_elem[i][j]=node21[j][i];
      
      for (j=0;j<2;j++) free(node21[j]);
      free(node21);
    }



  if ((maillage->dimension==3 && !ne111) || (maillage->dimension==2 && !ne91)){
    printf("\n\n *** IDEAS MESH :\n");
    printf("                           |--------------------|\n");
    printf("                           |    Surfacic mesh   |\n");
    printf("      ---------------------|--------------------|\n");
    printf("      | Dimension          |    %8d        |\n",maillage->dimension);
    printf("      | Nodes number       |    %8d        |\n",maillage->nbnoeuds);
    printf("      | Elements number    |    %8d        |\n",maillage->nbelem);
    printf("      | Nb nodes per elt   |    %8d        ||\n",maillage->nbnoeuds_par_elem);
    printf("      ---------------------|--------------------|\n");
  }
  else {
    printf("\n\n *** IDEAS MESH :\n");
    printf("                           |--------------------|------------------|\n");
    printf("                           |    Volumic mesh    |   Boundary mesh  |\n");
    printf("      ---------------------|--------------------|------------------|\n");
    printf("      | Dimension          |    %8d        |    %8d      |\n",maillage->dimension,maillage->dimension-1);
    printf("      | Nodes number       |    %8d        |    not used      |\n",maillage->nbnoeuds);
    printf("      | Elements number    |    %8d        |    %8d      |\n",maillage->nbelem,maillage->nbelem_de_bord);
    printf("      | Nb nodes per elt   |    %8d        |    %8d      |\n",maillage->nbnoeuds_par_elem,maillage->dimension);
    printf("      ---------------------|--------------------|------------------|\n");
  }

 return 0;

}

