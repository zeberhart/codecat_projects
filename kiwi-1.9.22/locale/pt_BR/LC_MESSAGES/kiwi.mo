��    @        Y         �  %   �  '   �  &   �     �  (         :     [     _     g     �     �     �  
   �     �     �     �     �     �  /        @     E     L     R  )   X  %   �     �  
   �  	   �     �     �     �  	   �     �     �     �     �            5     9   S     �  :   �  $   �     	     
	     	     	     	  	   #	  #   -	  ?   Q	  @   �	     �	     �	     �	     �	     �	     �	     �	     
     
     

     
  Y  
  1   l  (   �  (   �     �  -     %   =     c     l  %   t  &   �  )   �  
   �     �  	   �          	            (   7     `     m  	   v     �  =   �  5   �     �                    "     )     /  
   6     A  
   H     S     d     z  7   �  :   �     �  6     #   E     i     n     q     v     }     �  &   �  A   �  =   �  	   2     <     H     N     Q     T     ]     i     l     o     r        6                      *      ?       /      !       9   :         #   +                              8                     -       "   2       	       
   &             <   3          ;       4                  ,       '         @      (   $   .          )   =   0           7   1          %                 >       5    %s can not be converted to a currency %s could not be converted to an integer '%s' can not be converted to a boolean '%s' is not a valid object '%s' is not a valid value for this field A file named "%s" already exists Any Boolean Could not load image: %s Could not open file "%s" Could not select folder "%s" Currency Custom day Custom interval Date Date and Time Decimal Do you want to remove %s ? Do you wish to replace it with the current one? Enum Finish Float From: Inproperly placed thousand separators: %r Inproperly placed thousands separator Integer Last month Last week Long Object Open Password: Replace Save Search: Select folder Show more _details String The file "%s" could not be opened. Permission denied. The folder "%s" could not be selected. Permission denied. This field is mandatory This field requires a date of the format "%s" and not "%s" This field requires a number, not %r Time To: Today Total: Unicode Yesterday You cannot enter a year before 1900 You have a thousand separator to the right of the decimal point You have more than one decimal point ("%s")  in your number "%s" _Cancel _Select _Today dd hh hh:mm:ss hh:mm:ss LL mm ss yy yyyy Project-Id-Version: kiwi 1.9.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-05-28 17:38-0300
PO-Revision-Date: 2007-07-20 17:57-0300
Last-Translator: Ronaldo Maia <romaia@async.com.br>
Language-Team: Brazilian Portuguese <ldp-br@bazar.conectiva.com.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 %s não pode ser convertido para valor monetário %s não pode ser convertido para inteiro %s não pode ser convertido para boleano '%s' não é um objeto válido '%s' não é um valor válido para este campo Já existe um arquivo com o nome "%s" Qualquer Boleano Não foi possivel abrir o imagem "%s" Não foi possivel abrir o arquivo "%s" Não foi possivel selecionar a pasta "%s" Monetário Dia Intervalo Data Data e Hora Decimal Você deseja remover %s ? Deseja substituí-lo pelo arquivo atual? Enumeração Terminar Flutuante Para Separadores de milhares colocado em posições incorretas: %r Separador de milhares colocado em posição incorreta Inteiro Mês passado Semana passada Long Objeto Abrir Senha: Substituir Salvar Pesquisar: Selecionar pasta Exibir mais _detalhes String O arquivo "%s" não pode ser aberto. Permissão negada. A pasta "%s" não pode ser selecionada. Permissão negada. Esse campo é obrigatorio Este campo exige uma data no formato "%s", e não "%s" Este campo exige um numero, não %r Hora a: Hoje Total: Unicode Ontem Não pode inserir um ano antes do 1900 O numero tem um separador de milhares à direita do ponto decimal Você tem mais de um ponto decimal ("%s") em seu número "%s" _Cancelar _Selecionar _Hoje dd hh hh:mm:ss hh:mm:ss LL mm ss aa aaaa 