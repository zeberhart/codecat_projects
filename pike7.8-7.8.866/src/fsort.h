/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: 315ea218fee03f833d8eb836bfda2453805ef72a $
*/

#ifndef FSORT_H
#define FSORT_H

typedef int (*fsortfun)(const void *,const void *);

/* Prototypes begin here */
PMOD_EXPORT void fsort(void *base,
		       long elms,
		       long elmSize,
		       fsortfun cmpfunc);
/* Prototypes end here */


#endif
