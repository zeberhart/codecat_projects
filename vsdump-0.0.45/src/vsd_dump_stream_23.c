/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * vsdump: test program to dump and parse content of vsd file
 *
 * Copyright (C) 2006-2007	Valek Filippov (frob@df.ru)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 3 or later of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 */
#include <gsf/gsf-input.h>
#include <gsf/gsf-utils.h>
#include "vsdump.h"
#include "vsd_utils.h"
#include "vsd_dump_stream_23.h"

int
vsd_dump_stream_23(GByteArray *decomp, char *dirname, int *numico)
{
		guint i, j, size, nd, hex;
		char *tmpstr;
		FILE *fimage;
		guint8	palette[512];
		
		tmpstr = g_strdup_printf ("file%05d.ico", *numico);
 		fimage = vsd_openfd (dirname, tmpstr);
	
		size = decomp->data[6]*decomp->data[8]*5/8 + 0x68;
		
		fputc(0, fimage);		fputc(0, fimage);		fputc(1, fimage);		fputc(0, fimage);
		fputc(1, fimage);		fputc(0, fimage);		fputc(decomp->data[8], fimage);
		fputc(decomp->data[6], fimage);	fputc(0x10, fimage);		fputc(0, fimage);
		fputc(1, fimage);		fputc(0, fimage);	fputc(4, fimage);		fputc(0, fimage);
		
		GSF_LE_SET_GUINT32(&nd, size);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[j], 1, 1, fimage); 	/* file length */
		}
		hex = 0x16;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[j], 1, 1, fimage);
		}
		hex = 0x28;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[j], 1, 1, fimage);
		}
		
		hex = decomp->data[8];
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[j], 1, 1, fimage);
		}
		
		hex = 2*(decomp->data[6]);
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[j], 1, 1, fimage);
		}

		fputc(1, fimage);
		fputc(0, fimage);
		fputc(4, fimage);
		fputc(0, fimage);

		for(j=0;j<28;j++){
			fputc(0, fimage);
		}
	
		hex = 0x00008000;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}
		
		hex = 0x00800000;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}
		
		hex = 0x00808000;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		hex = 0x80000000;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		hex = 0x80008000;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}
		
		hex = 0x80800000;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		hex = 0xc0c0c000;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		hex = 0x80808000;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		hex = 0x0000ff00;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		hex = 0x00ff0000;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		hex = 0x00ffff00;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		hex = 0xff000000;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		hex = 0xff00ff00;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		hex = 0xffff0000;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		hex = 0xffffff00;
		GSF_LE_SET_GUINT32(&nd, hex);
		for(j=0;j<4;j++){
			fwrite(&((unsigned char *)&nd)[3-j], 1, 1, fimage);
		}

		j = 0;
		size = 0;
		hex = decomp->data[6]*decomp->data[8]/8; /* size of transparency mask */
		while	( (j + 19) < decomp->len){
			nd = decomp->data[18+j];
		
			if(nd&128){
				for(i=0; i< (nd-128);i++){
					if(size > hex - 1){
					fputc(decomp->data[19+j],fimage);
					}else{
					palette[size] = decomp->data[19+j];
					}
					size++;
				}
				j = j + 2;
			}else{
				for(i=0;i< nd;i++){
					if(size > hex - 1){
						fputc(decomp->data[19+j+i],fimage);
					}else{
						palette[size] = decomp->data[19+j+i];
					}
					size++;				
				}
				j = j + nd + 1;
			}
		}
		for(i=0;i<hex;i++){
			fputc(palette[i],fimage);
		}
		(*numico)++;
		fclose(fimage);
		
		return 0;
}
