/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 1988-2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/
/* Conversions de maillages */

/* Conversions de maillages */

#ifdef MED

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "convert2syrthes.h"

/* include MED */
#include <med.h>
#define MESGERR 12
#include <med_utils.h>

enum {VOLUME,FACE,EDGE,NODE};

/* ******************************************************************* */
int ouvrir_med(struct typ_maillage *maillage, /* INOUT structure de maillage */
               char *nomfich,                 /* IN nom de fichier maillage a lire */
               char *nomsyr) {                /* IN nom de fichier syrthes */
/* ******************************************************************* */

  /* ouverture du fichier en lecture seule */
  /* et verification de la conformite */
  
  med_idt fid;
  med_int majeurlib, mineurlib, releaselib;
  med_int majeurfic, mineurfic, releasefic;
  int erreur = 0;
  char *nomficfam = 0;
  med_bool hdfok,medok;
  med_err ret;

  fid = MEDfileOpen(nomfich,MED_ACC_RDONLY);
  (*maillage).fichier_ext = (FILE *) fid;
  if ( fid < 0) {
    printf("ERROR : unable to open MED file\n");
    return 1;
  }
  /*
   * Quelle version de la bibliotheque MED est utilisee ?
   */
  printf("\n");
  MEDlibraryNumVersion(&majeurlib, &mineurlib, &releaselib);
  fprintf(stdout,"- MED version used to read the file : %d.%d.%d \n",majeurlib,mineurlib,releaselib); 
  /*
   * Le fichier � lire est-il au bon format de fichier HDF ?
   */
  ret=MEDfileCompatibility(nomfich,&hdfok,&medok);
  if (! hdfok)
    fprintf(stdout,"- HDF format of the MED file in accordance with HDF format now used  \n");
  else {
    fprintf(stdout,"   WARNING : HDF format of the MED file not in accordance with HDF format now used\n");
    //erreur = 1;
  }
  
  /*
   * Le fichier a lire a-t-il �t� cr�� avec une version de la biblioth�que MED conforme avec celle utilise ?
   * (Num�ros majeur et mineur identiques).
   */
  if (! medok)
    fprintf(stdout,"- MED version of the file in accordance with MED version now used\n");
  else {
    fprintf(stdout,"   WARNING : MED version of the file not in accordance with MED version used now \n");
    //erreur = 1;
  }
  
  /*
   * Une fois le fichier ouvert on peut avoir acces au numero de version complet
   */
  if (MEDfileNumVersionRd(fid, &majeurfic, &mineurfic, &releasefic) < 0) {
    MESSAGE("ERROR while reading the library version ");
    return -1;
  }
  
  fprintf(stdout,"   This file has been created with MED %d.%d.%d \n",majeurfic,mineurfic,releasefic);
 /* 
  if(majeurlib < majeurfic || (majeurlib == majeurfic && mineurlib < mineurfic) || 
     (majeurlib == majeurfic && mineurlib == mineurfic && releaselib < releasefic) ) {
    fprintf(stdout,"ERROR : MED version of the file (%d.%d.%d) is higher than the currently used version of MED (%d.%d.%d)\n",
	    majeurfic,mineurfic,releasefic,majeurlib,mineurlib,releaselib);
    erreur=1;    
  }
*/
  if( majeurlib >= 3 && (majeurfic < 2 || (majeurfic == 2 && mineurfic <= 1)) ) {
    fprintf(stdout,"ERROR : This file has been created with a version of MED <= 2.1.");
    erreur=1;
  }

  if (erreur != 0) {
    fprintf(stdout,"ERROR : check $LD_LIBRARY_PATH?\n");
    return 1;
  } 
  printf("\n");
  printf("Name of the MED file : %s\n",nomfich); 

  if (!erreur) {
    /*
     * Ecriture de la table de correspondances nom familles MED, numeros syrthes
     */
    nomficfam = (char*) malloc((strlen(nomsyr) +10)*sizeof(char));
    sprintf(nomficfam, "%s%s",nomsyr,"_desc");
    (*maillage).fichier_desc = fopen(nomficfam,"w");
    if ( (*maillage).fichier_desc == NULL ) {
      printf("ERROR : unable to open file %s for writing\n",nomficfam);
      return 1;
    }
    printf("Name of the MED-SYRTHES correspondance file : %s\n",nomficfam);
  }
  return 0;
}

/* ******************************************************************* */
int fermer_med(struct typ_maillage *maillage) {
/* ******************************************************************* */
   /* fermeture du fichier .med */ 
   if (MEDfileClose((int) (*maillage).fichier_ext) < 0) {
     printf("ERROR : unable to close the file .med\n");
     return 1;
   }
   /* printf("MED file closed\n"); */
   return 0;
}

/* ******************************************************************* */
int lire_entete_med(struct typ_maillage *maillage) {
/* ******************************************************************* */
/* lecture des informations sur le maillage */
  med_idt fid = (int) (*maillage).fichier_ext;
  /* la dimension du maillage */
  med_int mdim;
  /* nom du maillage de longueur maxi MED_NAME_SIZE */
  char maa[MED_NAME_SIZE+1];
  char desc[MED_COMMENT_SIZE+1];
  med_mesh_type type;
  int tetraedreP2; /* presence de tetraedres P2 */
  int tetraedreP1; /* presence de tetraedres P1 */
  int triangleP2; /* presence de triangles P2 */
  int triangleP1; /* presence de triangles P1 */
  int segmentP2; /* presence de segments P2 */
  int segmentP1; /* presence de segments P1 */
  /* informations sur les familles */
  med_int nbfam, nufam, natt, ngro, *attide, *attval;
  med_int i;
  char *attdes, *gro;
  char nomfam[MED_NAME_SIZE+1];
  char typeofgroup[MED_NAME_SIZE+1];
  char *nommed = 0;
  med_err ret =0;

  char dtunit[MED_NAME_SIZE+1];
  med_sorting_type sortingtype;
  med_int nstep;
  med_axis_type axistype;
  med_int naxis;
  char * axisname;
  char * axisunit;
  med_int sdim;

  med_bool changement, transformation;

  printf("\nReading mesh information...\n");
  
  /* Lecture des infos concernant le premier maillage : les autres maillages sont ignores */
  
  naxis=MEDmeshnAxis(fid,1);
  axisname=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));
  axisunit=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));

  ret=MEDmeshInfo(fid,
		  1,
		  maa,
		  &sdim,
		  &mdim,
		  &type,
		  desc,
		  dtunit,
		  &sortingtype,
		  &nstep,
		  &axistype,
		  axisname,
		  axisunit);
  free(axisname);
  free(axisunit);
  //free(dtunit);

  if (ret < 0) {
    MESSAGE("ERROR while reading mesh information : ");SSCRUTE(maa);
    return 1;
  }
  printf("   Mesh name : %s - dimension : %d - espace : %d\n",maa,mdim,sdim);
  (*maillage).dimension = sdim;
  
  if (type != MED_UNSTRUCTURED_MESH) {
    printf("ERROR : mesh must be unstructured\n");
    return 1;
  }

  /* Combien de noeuds a lire ? */

  (*maillage).nbnoeuds = MEDmeshnEntity(fid,
					maa,
					MED_NO_DT,
					MED_NO_IT,
					MED_NODE,
					MED_NONE,
					MED_COORDINATE,
					MED_NODAL,
					&changement,
					&transformation);

  if ((*maillage).nbnoeuds <= 0) {
    printf("ERROR : wrong number of nodes : %i\n",(*maillage).nbnoeuds);
    return 1;
  }
  printf("   Number of nodes : %d \n",(*maillage).nbnoeuds);
  
  /* Combien d'elements a lire ? */
  tetraedreP2 = MEDmeshnEntity(fid,
			       maa,
			       MED_NO_DT,
			       MED_NO_IT,
			       MED_CELL,
			       MED_TETRA10,
			       MED_CONNECTIVITY,
			       MED_NODAL,
			       &changement,
			       &transformation);
  
  tetraedreP1 = MEDmeshnEntity(fid,
			       maa,
			       MED_NO_DT,
			       MED_NO_IT,
			       MED_CELL,
			       MED_TETRA4,
			       MED_CONNECTIVITY,
			       MED_NODAL,
			       &changement,
			       &transformation);

  triangleP2 = MEDmeshnEntity(fid,
			       maa,
			       MED_NO_DT,
			       MED_NO_IT,
			       MED_CELL,
			       MED_TRIA6,
			       MED_CONNECTIVITY,
			       MED_NODAL,
			       &changement,
			       &transformation);

  triangleP1 = MEDmeshnEntity(fid,
			       maa,
			       MED_NO_DT,
			       MED_NO_IT,
			       MED_CELL,
			       MED_TRIA3,
			       MED_CONNECTIVITY,
			       MED_NODAL,
			       &changement,
			       &transformation);

  segmentP2 = MEDmeshnEntity(fid,
			       maa,
			       MED_NO_DT,
			       MED_NO_IT,
			       MED_CELL,
			       MED_SEG3,
			       MED_CONNECTIVITY,
			       MED_NODAL,
			       &changement,
			       &transformation);

  segmentP1 = MEDmeshnEntity(fid,
			       maa,
			       MED_NO_DT,
			       MED_NO_IT,
			       MED_CELL,
			       MED_SEG2,
			       MED_CONNECTIVITY,
			       MED_NODAL,
			       &changement,
			       &transformation);
  
  if ( (segmentP2>0 || triangleP2>0 || tetraedreP2>0) && ((*maillage).version_syr == 4) ) {
    printf("ERROR :  quadratic elements found - This is not allowed with SYRTHES 4.0, Stop.\n");
    return 1;
  }

  if ( (tetraedreP2>0 || tetraedreP1>0) && ((*maillage).dimension != 3) ) {
    printf("ERROR : tetrahedra found in dimension : %i, Stop.\n", (*maillage).dimension);
    return 1;
  }

  if  ( tetraedreP1>0 ) { /* dimension de elements vaut 3 et le nombre de noeuds par element vaut 4*/
    (*maillage).dim_elem = 3;
    (*maillage).nbnoeuds_par_elem = 4;
    (*maillage).nbelem = tetraedreP1;
    (*maillage).nbelem_de_bord = triangleP1;
    printf("   Number of tetrahedra P1 : %i\n",tetraedreP1);
    printf("   Number of triangles P1 : %i\n",triangleP1);
  }
  else if  ( triangleP1>0 ) { /* dimension de elements vaut 2 et le nombre de noeuds par element vaut 3*/
    (*maillage).dim_elem = 2;
    (*maillage).nbnoeuds_par_elem = 3;
    (*maillage).nbelem = triangleP1;
    (*maillage).nbelem_de_bord = segmentP1;
    printf("   Number of triangles P1 : %i\n",triangleP1);
    printf("   Number of bars P1 : %i\n",segmentP1);
  }
  else if  ( segmentP1>0 ) { /* dimension de elements vaut 1 et le nombre de noeuds par element vaut 2*/
    (*maillage).dim_elem = 1;
    (*maillage).nbnoeuds_par_elem = 2;
    (*maillage).nbelem = segmentP1;
    (*maillage).nbelem_de_bord = 0;
    printf("   Number de segments P1 : %i\n",segmentP1);
  }
  else {
    printf("ERROR : no element to read !\n");
    return 1;
  }

  nbfam = MEDnFamily(fid,maa);
  maillage->entites_familles = (int *)calloc(nbfam, sizeof(int));
  for(i = 0 ; i < nbfam ; i++) maillage->entites_familles[i] = -1;

  maillage->numeros_familles = (int *)calloc(nbfam, sizeof(int));
  maillage->nbfam = nbfam;
  return 0;
}

/* ******************************************************************* */
void construction_entites_familles(int * couleurs, int nc, int *numeros_familles, int*entites_familles, int nbfam, int entite) {
/* ******************************************************************* */
  int i;
  int j;
  for(j=0; j< nbfam; j++)
    if (entites_familles[j]==-1)
      break;
  
  for(i=0 ; i < nc && j < nbfam; i++) {
    int k;
    int existe=0;
    for(k=0; k < j; k++)
      if( (numeros_familles[k] == couleurs[i] ) /*&& (entites_familles[k] == entite)*/)
	existe=1; 
    if(!existe) {
      numeros_familles[j] = couleurs[i];
      entites_familles[j] = entite;
      j++;
    }
  }
}
/* ******************************************************************* */
int ecrire_correspondance_syr_med(struct typ_maillage *maillage) {
/* ******************************************************************* */

/* lecture des informations sur le maillage */
  med_idt fid = (int) (*maillage).fichier_ext;
  /* la dimension du maillage */
  med_int mdim;
  /* nom du maillage de longueur maxi MED_NAME_SIZE */
  char maa[MED_NAME_SIZE+1];
  char desc[MED_COMMENT_SIZE+1];
  med_mesh_type type;
  /* informations sur les familles */
  med_int nbfam, nufam, natt, ngro, *attide, *attval;
  med_int i;
  char *attdes, *gro;
  char nomfam[MED_NAME_SIZE+1];
  char typeofgroup[MED_NAME_SIZE+1];
  char *nommed = 0;
  med_err ret =0;

  char dtunit[MED_NAME_SIZE+1];
  med_sorting_type sortingtype;
  med_int nstep;
  med_axis_type axistype;
  med_int naxis;
  char * axisname;
  char * axisunit;
  med_int sdim;

  int * numeros_familles; /* numero des familles */
  int * entites_familles; /* Entite des familles */

  /* Lecture des infos concernant le premier maillage : les autres maillages sont ignores */
  
  naxis=MEDmeshnAxis(fid,1);
  axisname=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));
  axisunit=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));

  ret=MEDmeshInfo(fid,
		  1,
		  maa,
		  &sdim,
		  &mdim,
		  &type,
		  desc,
		  dtunit,
		  &sortingtype,
		  &nstep,
		  &axistype,
		  axisname,
		  axisunit);
  free(axisname);
  free(axisunit);
  //free(dtunit);

  if (ret < 0) {
    MESSAGE("ERROR while reading mesh information : ");SSCRUTE(maa);
    return 1;
  }

  nbfam = MEDnFamily(fid,maa);

  /* printf("( "); */
  /* for(i = 0 ; i < nbfam ; i++) printf("%d, ",maillage->numeros_familles[i]); */
  /* printf(")\n"); */

  /* printf("( "); */
  /* for(i = 0 ; i < nbfam ; i++) printf("%d, ",maillage->entites_familles[i]); */
  /* printf(")\n"); */

  printf("\nNumber of MED families : %d\n", nbfam);
  for (i=0; i<nbfam; i++) {
    /* Lecture du nombre de groupe */
    if ((ngro = MEDnFamilyGroup(fid,maa,i+1)) < 0) {
      MESSAGE("ERROR reading number of groups in family: ");
      ISCRUTE(i+1);
      ret = -1;
    }
    
    /* Lecture du nombre d'attribut */
    if ((natt = MEDnFamily23Attribute(fid,maa,i+1)) < 0) {
      MESSAGE("ERROR reading number of attributes in family: ");
      ISCRUTE(i+1);
      ret = -1;
    }
    
    if (ret == 0)
      printf("\n   Family %d with %d attributes et %d groups \n",i+1,natt,ngro);
    
    /* Lecture des informations sur la famille */
    if (ret == 0) {
      /* Allocations memoire */
      attide = (med_int*) malloc(sizeof(med_int)*natt);
      attval = (med_int*) malloc(sizeof(med_int)*natt);
      attdes = (char *) malloc(MED_COMMENT_SIZE*natt+1);
      gro = (char*) malloc(MED_LNAME_SIZE*ngro+1);

      if ( MEDfamily23Info(fid, maa, i+1, nomfam, attide, attval, attdes, &nufam, gro)<0) {
	/*if (MEDfamilyInfo(fid, maa, i+1, nomfam, &nufam, gro) <0 ) {*/
        printf("ERROR reading informations about family %d\n", i+1);
        return 1;
      }

      /* Enlever ce qui precede le nom de groupe med : FAM_x_ */
      nommed = strchr(nomfam,'_');
      if (nommed)
        {
          nommed = nommed+1;
          nommed = strchr(nommed,'_');
        }
      if (nommed)
        nommed = nommed+1;
      else
        nommed = nomfam;
      printf("   Family name %s and number %d : \n",nomfam,nufam);
      printf("   Number of group in family : %d. Names of group : %s \n",ngro, gro);
      //printf("Group type : %s",typeofgroup);
      //if (nufam) fprintf((*maillage).fichier_desc, "%d    %s\n", abs(nufam), nommed);
      int k;
      for(k=0 ; k < nbfam ; k++) 
	if(maillage->numeros_familles[k] == nufam)
	  break;

      char * types_de_groupes;
      if(maillage->entites_familles[k] == VOLUME)
	types_de_groupes="group_of_volumes";
      else if(maillage->entites_familles[k] == EDGE)
	types_de_groupes="group_of_edges";
      else if(maillage->entites_familles[k] == FACE)
	types_de_groupes="group_of_faces";
      else if(maillage->entites_familles[k] == NODE)
	types_de_groupes="group_of_nodes";

      if (nufam) fprintf((*maillage).fichier_desc, "%s    %d    %s\n", types_de_groupes, abs(nufam), nommed);
      free(attide);
      free(attval);
      free(attdes);
      free(gro);
    }
  }
  fclose((*maillage).fichier_desc);
  /* printf("MED-SYRTHES correspondance file closed\n"); */

  return 0;
}

/* ******************************************************************* */
int lire_coord_med(struct typ_maillage *maillage) {
/* ******************************************************************* */
  med_idt fid = (int) (*maillage).fichier_ext;
  /* la dimension du maillage */
  med_int mdim;
  /* nom du maillage de longueur maxi MED_NAME_SIZE */
  char maa[MED_NAME_SIZE+1];
  /* table des coordonnees */
  med_float *coo;
  /* tables des noms et des unites des coordonnees 
     profil : (dimension*MED_SNAME_SIZE+1) */
  char * nomcoo;
  char * unicoo;
  /* tables des noms, numeros, numeros de familles des noeuds
     autant d'elements que de noeuds - les noms ont pout longueur
     MED_SNAME_SIZE */
  med_axis_type rep;
  char str[MED_SNAME_SIZE+1];
  int i,pb;
  char desc[MED_COMMENT_SIZE+1];
  med_mesh_type type;


  med_err ret=0;
  char dtunit[MED_NAME_SIZE+1];
  med_sorting_type sortingtype;
  med_int naxis;
  med_int sdim;
  med_int nstep;

  naxis=MEDmeshnAxis(fid,1);
  nomcoo=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));
  unicoo=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));

  ret=MEDmeshInfo(fid,
		  1,
		  maa,
		  &sdim,
		  &mdim,
		  &type,
		  desc,
		  dtunit,
		  &sortingtype,
		  &nstep,
		  &rep,
		  nomcoo,
		  unicoo);
  if (ret < 0) {
    MESSAGE("ERROR while reading mesh information : ");SSCRUTE(maa);
    return 1;
  }
  
  /* table des coordonnees 
      profil : (dimension * nombre de noeuds ) */
  coo = (med_float*) calloc(((*maillage).nbnoeuds)*sdim,sizeof(med_float));

  if (MEDmeshNodeCoordinateRd(fid, maa, MED_NO_DT, MED_NO_IT, MED_NO_INTERLACE, coo) < 0) {
    MESSAGE("ERROR while reading coordinates");
    return 1;
  }
  
  if (rep != MED_CARTESIAN) {
    printf("ERROR : axis must be cartesian\n");
    return 1;
  }
  
  for (pb=0,i=0;i<sdim;i++) {
    strncpy(str,unicoo+i*MED_SNAME_SIZE,MED_SNAME_SIZE);
    str[MED_SNAME_SIZE] = '\0';
    if ( str[0] != 'm' || str[1] != ' ' ) pb=1;
  }
  if (pb>0) printf("\nWARNING : make sure your mesh is using the meter unit.\n");

  free(nomcoo);
  free(unicoo);
  
  /* mise dans la structure */
  /* allocations et verifications */
  ((*maillage).xcoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
  if ((*maillage).xcoord == NULL) {
    printf("ERROR : allocation error for the x coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
    return 1;
  }
  ((*maillage).ycoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
  if ((*maillage).ycoord == NULL) {
    printf("ERROR : allocation error for the y coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
    return 1;
  }   
  if ((*maillage).dimension == 3) {
    ((*maillage).zcoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
    if ((*maillage).zcoord == NULL) {
      printf("ERROR : allocation error for the z coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
      return 1;
    }   
  }
  for (i=0;i<(*maillage).nbnoeuds;i++) (*maillage).xcoord[i] = coo[i];
  for (i=0;i<(*maillage).nbnoeuds;i++) (*maillage).ycoord[i] = coo[(*maillage).nbnoeuds+i];
  if ((*maillage).dimension == 3) {
    for (i=0;i<(*maillage).nbnoeuds;i++) (*maillage).zcoord[i] = coo[2 * (*maillage).nbnoeuds+i];
  }
  
    
  /* liberation de la memoire */
  free(coo);
  
  return 0;
}

/* ******************************************************************* */
int lire_noeuds_colores_med(struct typ_maillage *maillage) {
/* ******************************************************************* */
/* lecture des noeuds colores : rien a lire*/
/* allocation et initialisation du tableau des couleurs des noeuds */

  int i; /* indice de boucle */
  med_idt fid = (int) (*maillage).fichier_ext;
  char maa[MED_NAME_SIZE+1];
  char desc[MED_COMMENT_SIZE+1];
  med_mesh_type type;
  med_int mdim;
  
  med_err ret=0;
  char dtunit[MED_NAME_SIZE+1];
  med_sorting_type sortingtype;
  med_int nstep;
  med_axis_type axistype;
  med_int naxis;
  char * axisname;
  char * axisunit;
  med_int sdim;

  naxis=MEDmeshnAxis(fid,1);
  axisname=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));
  axisunit=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));

  ret=MEDmeshInfo(fid,
		  1,
		  maa,
		  &sdim,
		  &mdim,
		  &type,
		  desc,
		  dtunit,
		  &sortingtype,
		  &nstep,
		  &axistype,
		  axisname,
		  axisunit);
  if (ret < 0) {
    MESSAGE("ERROR while reading mesh information : ");SSCRUTE(maa);
    return 1;
  }

  free(axisname);
  free(axisunit);

  /* allocation */
  ((*maillage).coul_noeud) = (int *) malloc (sizeof(int) * ((*maillage).nbnoeuds));
  if ((*maillage).coul_noeud == NULL) {
    printf("ERROR : allocation error for the nodes color, needed size : %zu \n", sizeof(int) * ((*maillage).nbnoeuds));
    return 1;
  }   
  /* initialisation a zero */
  for (i=0;i<(*maillage).nbnoeuds;i++) (*maillage).coul_noeud[i] = 0;
  
  /* Lecture des numeros des familles pour les couleurs des noeuds*/

  ret=MEDmeshEntityFamilyNumberRd(fid,
				  maa,
				  MED_NO_DT,
				  MED_NO_IT,
				  MED_NODE,
				  MED_NONE,
				  (*maillage).coul_noeud);

  /*Couleur des noeuds*/
  printf("\nBuilding entity and family association for Nodes... \n");
  if (ret>=0){  /* sinon, c'est qu'il n'y a pas de familles de noeuds */
     construction_entites_familles(maillage->coul_noeud, maillage->nbnoeuds, maillage->numeros_familles, 
				  maillage->entites_familles, maillage->nbfam, NODE);
  }
  else
    printf("--> no family numbers for nodes\n");

  return 0;
}


/* ******************************************************************* */
int lire_elem_med(struct typ_maillage *maillage) {
/* ******************************************************************* */
  med_idt fid = (int) (*maillage).fichier_ext;
  /* la dimension du maillage */
  med_int mdim;
  /* nom du maillage de longueur maxi MED_NAME_SIZE */
  char maa[MED_NAME_SIZE+1];
  char str[MED_SNAME_SIZE+1];
  int i,j;
  char desc[MED_COMMENT_SIZE+1];
  med_mesh_type type;
  med_int *liste;


  med_err ret=0;
  char dtunit[MED_NAME_SIZE+1];
  med_sorting_type sortingtype;
  med_int nstep;
  med_axis_type axistype;
  med_int naxis;
  char * axisname;
  char * axisunit;
  med_int sdim;

  naxis=MEDmeshnAxis(fid,1);
  axisname=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));
  axisunit=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));

  ret=MEDmeshInfo(fid,
		  1,
		  maa,
		  &sdim,
		  &mdim,
		  &type,
		  desc,
		  dtunit,
		  &sortingtype,
		  &nstep,
		  &axistype,
		  axisname,
		  axisunit);
  if (ret < 0) {
    MESSAGE("ERROR while reading mesh information : ");SSCRUTE(maa);
    return 1;
  }

  free(axisname);
  free(axisunit);

  printf("Reading elements...\n");


  liste = (med_int*) malloc(sizeof(med_int)*((*maillage).nbnoeuds_par_elem)*((*maillage).nbelem));
  if (liste == NULL){
    printf("ERROR : allocation error for the connectivity\n");
    return 1;
  }
  
  /* selon le type d'elements a lire */   
  switch ((*maillage).nbnoeuds_par_elem) {
  case 10 : /* tetraedres P2 */
            /* Lecture de la connectivite */
            if ( MEDmeshElementConnectivityRd(fid, maa,
				      MED_NO_DT,
				      MED_NO_IT,
				      MED_CELL,
				      MED_TETRA10,
				      MED_NODAL,
				      MED_FULL_INTERLACE,
				      liste)< 0) {
	      MESSAGE("ERROR while reading connectivity, is it nodal connectivity ??");
	      return 1;
	    }  
           break;
  case 4 : /* tetraedres P1 */
           /* Lecture de la connectivite */
           if ( MEDmeshElementConnectivityRd(fid, maa,
				      MED_NO_DT,
				      MED_NO_IT,
				      MED_CELL,
				      MED_TETRA4,
				      MED_NODAL,
				      MED_FULL_INTERLACE,
				      liste) < 0) {
	     MESSAGE("ERROR while reading connectivity, is it nodal connectivity ??");
	     return 1;
	   }  
           break;
  case 6 : /* triangles P2 */
           /* Lecture de la connectivite */
           if ( MEDmeshElementConnectivityRd(fid, maa,
				      MED_NO_DT,
				      MED_NO_IT,
				      MED_CELL,
				      MED_TRIA6,
				      MED_NODAL,
				      MED_FULL_INTERLACE,
				      liste) < 0) {
	     MESSAGE("ERROR while reading connectivity, is it nodal connectivity ??");
	     return 1;
	   }  
           break;
  case 3 : /* triangles P1 ou segments P2 */
    if ((*maillage).dim_elem == 2) {
           /* Lecture de la connectivite */
           if ( MEDmeshElementConnectivityRd(fid, maa,
					MED_NO_DT,
					MED_NO_IT,
					MED_CELL,
					MED_TRIA3,
					MED_NODAL,
					MED_FULL_INTERLACE,
					liste) < 0) {
	     MESSAGE("ERROR while reading connectivity, is it nodal connectivity ??");
	     return 1;
	   }  
	   
    }
    else {
           /* Lecture de la connectivite */
           if ( MEDmeshElementConnectivityRd(fid, maa,
					MED_NO_DT,
					MED_NO_IT,
					MED_CELL,
					MED_SEG3,
					MED_NODAL,
					MED_FULL_INTERLACE,
					liste) < 0) {
	     MESSAGE("ERROR while reading connectivity, is it nodal connectivity ??");
	     return 1;
	   } 
    }
    break;
  case 2 : /* segments P1 */
           /* Lecture de la connectivite */
           if ( MEDmeshElementConnectivityRd(fid, maa,
				      MED_NO_DT,
				      MED_NO_IT,
				      MED_CELL,
				      MED_SEG2,
				      MED_NODAL,
				      MED_FULL_INTERLACE,
				      liste) < 0) {
	     MESSAGE("ERROR while reading connectivity, is it nodal connectivity ??");
	     return 1;
	   }  
	   break;
  default : 
    printf("ERROR : wrong number of nodes per element  %i\n",(*maillage).nbnoeuds_par_elem);
    return 1;
  }
  
  /* mise dans la structure */
  /* allocation et verification */
  (*maillage).liste_elem = (int **) malloc (sizeof(int *) * ((*maillage).nbelem));
  if ((*maillage).liste_elem == NULL) {
    printf("ERROR : allocation error for the connectivity, needed size : %zu \n", sizeof(int *) * ((*maillage).nbelem));
    return 1;
  }
  /* allocation du tableau de connectivite */
  for (i=0; i<(*maillage).nbelem; i++) (*maillage).liste_elem[i] = (int *) malloc (sizeof(int ) * ((*maillage).nbnoeuds_par_elem));
  /* verification de la derniere allocation seulement */
  if ((*maillage).liste_elem[(*maillage).nbelem-1] == NULL) {
    printf("ERROR : allocation error for the connectivity (verification of the last allocation)\n");
    return 1;
  }
  /* stockage selon le mode choisi MED_FULL_INTERLACE*/
  for (i=0;i<(*maillage).nbelem;i++) {
    for (j=0;j<(*maillage).nbnoeuds_par_elem;j++){
      (*maillage).liste_elem[i][j] = liste[i*((*maillage).nbnoeuds_par_elem) + j];
    }
  }
  
  /* liberation de la memoire */
  free(liste);
  
  return 0;
}

/* ******************************************************************* */
int lire_zones_med(struct typ_maillage *maillage) {
/* ******************************************************************* */

  int i ; /* indice de boucle */
  med_idt fid = (int) (*maillage).fichier_ext;
  /* la dimension du maillage */
  med_int mdim;
  /* nom du maillage de longueur maxi MED_NAME_SIZE */
  char maa[MED_NAME_SIZE+1];
  char desc[MED_COMMENT_SIZE+1];
  med_mesh_type type;

  med_err ret=0;
  char dtunit[MED_NAME_SIZE+1];
  med_sorting_type sortingtype;
  med_int nstep;
  med_axis_type axistype;
  med_int naxis;
  char * axisname;
  char * axisunit;
  med_int sdim;

  naxis=MEDmeshnAxis(fid,1);
  axisname=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));
  axisunit=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));

  ret=MEDmeshInfo(fid,
		  1,
		  maa,
		  &sdim,
		  &mdim,
		  &type,
		  desc,
		  dtunit,
		  &sortingtype,
		  &nstep,
		  &axistype,
		  axisname,
		  axisunit);
  if (ret < 0) {
    MESSAGE("ERROR while reading mesh information : ");SSCRUTE(maa);
    return 1;
  }

  free(axisname);
  free(axisunit);

  printf("Reading zones ...\n");
  
  /* allocation du tableau des couleurs d'elements et verification */
  (*maillage).coul_elem = (int *) malloc (sizeof(int) * ((*maillage).nbelem));
  if ((*maillage).liste_elem == NULL) {
    printf("ERROR : allocation error for nodes color, needed size : %zu \n", sizeof(int) * ((*maillage).nbelem));
    return 1;
  }
  /* initialisation a 0 des couleurs d'elements */
  
  for (i=0;i<(*maillage).nbelem;i++) (*maillage).coul_elem[i] = 0;
  
  /* selon le type d'elements a lire */   
  switch ((*maillage).nbnoeuds_par_elem) {
  case 4 : /* tetraedres P1 */
           /* Lecture des numeros des familles*/
           if (MEDmeshEntityFamilyNumberRd(fid,
				    maa,
				    MED_NO_DT,
				    MED_NO_IT,
				    MED_CELL,
				    MED_TETRA4,
				    (*maillage).coul_elem) < 0) {
	     MESSAGE("ERROR while reading family numbers for elements color");
	     return 1;
	   }
	   break;
  case 3 : /* triangles P1 */
           /* Lecture des numeros des familles*/
            if (MEDmeshEntityFamilyNumberRd(fid,
				      maa,
				      MED_NO_DT,
				      MED_NO_IT,
				      MED_CELL,
				      MED_TRIA3,
				      (*maillage).coul_elem) < 0) {
	     MESSAGE("ERROR while reading family numbers for element color");
	     return 1;
	    }	
	    break;
  case 2 : /* segments P1 */
           if (MEDmeshEntityFamilyNumberRd(fid,
				    maa,
				    MED_NO_DT,
				    MED_NO_IT,
				    MED_CELL,
				    MED_SEG2,
				    (*maillage).coul_elem) < 0) {
	     MESSAGE("ERROR while reading family numbers for element color");
	     return 1;
	   }	   
	   break;
  default : 
    printf("ERROR : wrong number of nodes per element %i\n",(*maillage).nbnoeuds_par_elem);
    return 1;
  }

  /*Couleur des elements*/
  int entite;
  printf("Building entity and family association for elements... \n");
  if(maillage->dim_elem == 2)
    entite=FACE;
  else if(maillage->dim_elem == 3)
    entite=VOLUME;

  construction_entites_familles(maillage->coul_elem, maillage->nbelem, maillage->numeros_familles, 
				maillage->entites_familles, maillage->nbfam, entite);

  /* convention MED, le num�ro des familles d'elements est negatif */
  for (i=0;i<(*maillage).nbelem;i++) (*maillage).coul_elem[i] = abs((*maillage).coul_elem[i]);

  return 0;
}

/* ******************************************************************* */
int lire_cl_med(struct typ_maillage *maillage) {
/* ******************************************************************* */
/* conditions limites */

  med_idt fid = (int) (*maillage).fichier_ext;
  /* la dimension du maillage */
  med_int mdim;
  /* nom du maillage de longueur maxi MED_NAME_SIZE */
  char maa[MED_NAME_SIZE+1];
  char str[MED_SNAME_SIZE+1];
  int i,j,k;
  char desc[MED_COMMENT_SIZE+1];
  med_mesh_type type;
  med_int *liste;
  int nbnoeuds_elembord=0; /* nombre de noeuds par element de bord */
  int nbfaces_par_elem; /* nomnre de faces par element */
  
  if ((*maillage).nbelem_de_bord > 0) {

   if (((*maillage).dimension == 3 && (*maillage).dim_elem == 3)
    || ((*maillage).dimension == 2 && (*maillage).dim_elem == 2) ) {
     med_err ret=0;
     char dtunit[MED_NAME_SIZE+1];
     med_sorting_type sortingtype;
     med_int nstep;
     med_axis_type axistype;
     med_int naxis;
     char * axisname;
     char * axisunit;
     med_int sdim;
     
     naxis=MEDmeshnAxis(fid,1);
     axisname=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));
     axisunit=malloc((naxis*MED_SNAME_SIZE+1)*sizeof(char));
     
     ret=MEDmeshInfo(fid,
		     1,
		     maa,
		     &sdim,
		     &mdim,
		     &type,
		     desc,
		     dtunit,
		     &sortingtype,
		     &nstep,
		     &axistype,
		     axisname,
		     axisunit);
     if (ret < 0) {
       MESSAGE("ERROR while reading mesh information : ");SSCRUTE(maa);
       return 1;
     }
     
     free(axisname);
     free(axisunit);
     
     
     printf("Reading boundary elements...\n");


     switch ((*maillage).nbnoeuds_par_elem) {
     case 4 :  /* 3 noeuds sommets (triangles)*/
             nbnoeuds_elembord = 3;
	     break;
     case 3 : /* 2 noeuds (aretes) */
             nbnoeuds_elembord = 2;
	     break;
     default :
       printf("ERROR : wrong number of nodes per element (%i)\n",(*maillage).nbnoeuds_par_elem);
       return 1;
     }
  
     liste = (med_int*) malloc(sizeof(med_int)*(nbnoeuds_elembord)*((*maillage).nbelem_de_bord));
     if (liste == NULL){
       printf("ERROR : allocation error for the boundary connectivity\n");
       return 1;
     }
     /* couleurs : allocation et initialisation */
     (*maillage).coul_elembord = (int *) malloc (sizeof(int) * (*maillage).nbelem_de_bord);
     if ((*maillage).coul_elembord == NULL) {
       printf("ERROR : allocation error for the boundary connectivity, needed size : %zu \n", sizeof(int) * (*maillage).nbelem_de_bord);
       return 1;
     }
     for (i=0;i<(*maillage).nbelem_de_bord;i++)  (*maillage).coul_elembord[i] = 0;   
   
     /* selon le type d'elements a lire */   
     switch ((*maillage).nbnoeuds_par_elem) {
     case 4 : /* tetraedres P1 -> triangles P1 */
              /* Lecture de la connectivite */
              if ( MEDmeshElementConnectivityRd(fid, maa,
				      MED_NO_DT,
				      MED_NO_IT,
				      MED_CELL,
				      MED_TRIA3,
				      MED_NODAL,
				      MED_FULL_INTERLACE,
				      liste) < 0) {
		MESSAGE("ERROR while reading connectivity, is it nodal connectivity ??");
		return 1;
	      }  
              /* Lecture des numeros des familles*/
	      if (MEDmeshEntityFamilyNumberRd(fid,
				      maa,
				      MED_NO_DT,
				      MED_NO_IT,
				      MED_CELL,
				      MED_TRIA3,
				      (*maillage).coul_elembord) < 0) {
		MESSAGE("ERROR while reading family numbers for the boundary elements");
		return 1;
	      }
	      break;
     case 3 : /* triangles P1 -> segments P1 */
              /* Lecture de la connectivite */
              if ( MEDmeshElementConnectivityRd(fid, maa,
				      MED_NO_DT,
				      MED_NO_IT,
				      MED_CELL,
				      MED_SEG2,
				      MED_NODAL,
				      MED_FULL_INTERLACE,
				      liste) < 0) {
		MESSAGE("ERROR while reading connectivity, is it nodal connectivity ??");
		return 1;
	      }  
              /* Lecture des numeros des familles*/
              if (MEDmeshEntityFamilyNumberRd(fid,
				      maa,
				      MED_NO_DT,
				      MED_NO_IT,
				      MED_CELL,
				      MED_SEG2,
				      (*maillage).coul_elembord) < 0) {
		MESSAGE("ERROR while reading family numbers for the boundary elements");
		return 1;
	      }
	      break;
     default : 
       printf("ERROR : wrong number of nodes per element %i\n",(*maillage).nbnoeuds_par_elem);
       return 1;
     }

     printf("Building entity and family association for boundary elements... \n");
     int entite;
     if(maillage->dimension == 2)
       entite=EDGE;
     else if(maillage->dimension == 3)
       entite=FACE;
     construction_entites_familles(maillage->coul_elembord, maillage->nbelem_de_bord, maillage->numeros_familles, 
				   maillage->entites_familles, maillage->nbfam, entite);

     /* convention MED, le num�ro des familles d'elements est negatif */
     for (i=0;i<(*maillage).nbelem_de_bord;i++)  (*maillage).coul_elembord[i] = abs((*maillage).coul_elembord[i]);   
     /* mise dans la structure */
     /* allocation et verification */
     (*maillage).liste_elembord = (int **) malloc (sizeof(int *) * ((*maillage).nbelem_de_bord));
     if ((*maillage).liste_elembord == NULL) {
       printf("ERROR : allocation error for the boundary connectivity, needed size : %zu \n", sizeof(int *) * ((*maillage).nbelem_de_bord));
       return 1;
     }
     /* allocation du tableau de connectivite */
     for (i=0; i<(*maillage).nbelem_de_bord; i++) (*maillage).liste_elembord[i] = (int *) malloc (sizeof(int ) * (nbnoeuds_elembord));
     /* verification de la derniere allocation seulement */
     if ((*maillage).liste_elembord[(*maillage).nbelem_de_bord-1] == NULL) {
       printf("ERROR : allocation error for the boundary connectivity (verification of the last allocation)\n");
       return 1;
     }
     /* stockage selon le mode choisi MED_FULL_INTERLACE*/
     for (i=0;i<(*maillage).nbelem_de_bord;i++) {
       for (j=0;j<nbnoeuds_elembord;j++){
         (*maillage).liste_elembord[i][j] = liste[i*(nbnoeuds_elembord) + j];
       }
     }
  
     /* liberation de la memoire */
     free(liste);
    }
    else {
      /* cas Rayonnement avec elements de bord trouves dans le maillage */
      (*maillage).nbelem_de_bord = 0;
    }
  }
  else { /* cas reffaces lorsque le nombre de d'elements de bord est nul */
  
    if ( ((*maillage).dimension == 3 && (*maillage).dim_elem == 3)
      || ((*maillage).dimension == 2 && (*maillage).dim_elem == 2) ) {
      
      printf("\nWARNING : 0 boundary element !!\n\n");
      
      if ((*maillage).version_syr == 3) {
    
        /* nombre de faces par element et nombre de noeuds sommets par element */
        if ((*maillage).dim_elem == 3) nbfaces_par_elem = 4;
        else nbfaces_par_elem = 3;
    
        /* allocation du tableau des references des faces */
        (*maillage).liste_reffaces = (int **) malloc (sizeof(int *) * ((*maillage).nbelem));
        if ((*maillage).liste_reffaces == NULL) {
              printf("ERROR : allocation error for face references, needed size : %zu \n", sizeof(int *) * ((*maillage).nbelem));
          return 1;
        }
        for  (i=0;i<(*maillage).nbelem;i++) (*maillage).liste_reffaces[i] = (int *) malloc (sizeof(int) * nbfaces_par_elem);
        /* verification de la derniere allocation */
        if ((*maillage).liste_reffaces[(*maillage).nbelem-1] == NULL) {
          printf("ERROR : allocation error for face references (last allocation)\n");
          return 1;
        }
        /* initialisation des references aux faces */
        for  (i=0;i<(*maillage).nbelem;i++) {
          for  (j=0;j<nbfaces_par_elem;j++) {
            (*maillage).liste_reffaces[i][j] = 0;
          }
        }
      }
    }
  }
  
  /* liberation du tableau de connectivites */
  for (i=0;i<(*maillage).nbelem;i++) free((*maillage).liste_elem[i]);
  free((*maillage).liste_elem);



  return 0;
}


#endif
