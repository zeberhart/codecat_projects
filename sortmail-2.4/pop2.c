#ifndef lint
static const char sccsid[] = "@(#)pop2.c 1.4 02/03/11 falk" ;
static const char rcsid[] = "$Id: pop2.c,v 1.3 2003/12/22 06:03:14 efalk Exp $" ;
#endif

/*	POP2.C	-- manage POP2 protocol
 *
 *
 * Pop2Folder *
 * openPop2(char *hostname, char *user, char *pw, int to)
 *	Open a network connection.  Return NULL on failure.
 *
 * int
 * pop2Folder(Pop2Folder *, char *folder, int to)
 *	Set input folder.  Return 0 on success, error code on failure.
 *
 * int
 * pop2GetMessage(MailMessage *msg, MsgState)
 *	Read one message from folder, copy to tempfile.  Return 0 on
 *	success, error code on failure.
 *
 * int
 * pop2FinishMessage(Pop2Folder *, int save, int to)
 *	Acknowledge receipt of a message, tell server to save or delete.
 *
 * void
 * closePop2(Pop2Folder *)
 *	Close connection.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <netinet/in.h>

#include "sortmail.h"
#include "netutils.h"
#include "pop2.h"
#include "utils.h"

#define	MAX_POP_CMD	512

static	Pop2Folder *Pop2Abort() ;

#ifndef	min
#define	min(a,b)	((a)<(b)?(a):(b))
#endif


	/* Initiate connection to host.  On success, connection
	 * is open and this function returns non-null.  On failure,
	 * there is no connection and this function returns null.
	 * This function reports all errors to logfile as appropriate.
	 */

Pop2Folder *
openPop2( char *hostname, char *user, char *pw, int to)
{
	int	fd ;
	char	buffer[513] ;
	Pop2Folder *rval ;

	if( strlen(user) + strlen(pw) > MAX_POP_CMD-10 ) {
	  logFile("username and/or password too long\n") ;
	  logFile("  username = %s\n", user) ;
	  return NULL ;
	}

	rval = MALLOC(Pop2Folder, 1) ;
	if( rval == NULL ) {
	  logFile("out of memory in openPop2\n") ;
	  return NULL ;
	}

	signal(SIGPIPE, SIG_IGN) ;

	if( (fd = openNet(hostname, "pop-2", 109)) < 0 ) {
	  free(rval) ;
	  return NULL ;
	}
	rval->fd = fd ;

	if( netReadln(fd, buffer, sizeof(buffer), to) <= 0 ) {
	  logFile("unable to connect to pop2 server %s, %s\n",
	  	hostname, strerror(errno)) ;
	  return Pop2Abort(rval) ;
	}

	if( buffer[0] != '+' ) {	/* connect failed */
	  removeNL(buffer) ;
	  logFile("unable to connect to pop2 server %s: %s\n",
	  	hostname, buffer) ;
	  return Pop2Abort(rval) ;
	}

	/* OK< we're connected */

	if( netWritef(fd, "HELO %s %s\r\n", user, pw) < 0  ||
	    netReadln(fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("unable to connect to pop2 server %s: %s\n",
	  	hostname, strerror(errno)) ;
	  return Pop2Abort(rval) ;
	}

	if( buffer[0] != '#' ) {	/* login failed */
	  removeNL(buffer) ;
	  logFile("pop2 open for user %s failed: %s\n", user, buffer) ;
	  return Pop2Abort(rval) ;
	}

	rval->nmsg = atoi(buffer+1) ;

	logFilev(1, "%d messages waiting\n", rval->nmsg) ;

	return rval ;
}


int
pop2Folder( Pop2Folder *p2, char *folder, int to)
{
	char	buffer[513] ;

	if( strlen(folder) > MAX_POP_CMD-10 ) {
	  logFile("folder name too long\n") ;
	  logFile("  %s\n", folder) ;
	  (void) Pop2Abort(p2) ;
	  return USER_ERR ;
	}

	if( netWritef(p2->fd, "FOLD %s\r\n", folder) < 0  ||
	    netReadln(p2->fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("pop2 fatal error setting folder %s: %s\n",
	  	folder, strerror(errno)) ;
	  (void) Pop2Abort(p2) ;
	  return POP_ERR ;
	}

	if( buffer[0] != '#' ) {	/* folder failed */
	  removeNL(buffer) ;
	  logFile("pop2 open for folder %s failed: %s\n", folder, buffer) ;
	  (void) Pop2Abort(p2) ;
	  return POP_ERR ;
	}

	p2->nmsg = atoi(buffer+1) ;

	return EXIT_OK ;
}


/* int
 * pop2GetMessage()
 *	Read one message from folder, copy to tempfile.  Return 0 on
 *	success, error code on failure.
 */

int
pop2GetMessage( MailMessage *msg, MsgState newstate)
{
	Pop2Folder *p2 = (Pop2Folder *) msg->popinfo ;
	int	msgno = msg->n ;
	int	to = p2->timeout ;
	char	buffer[1024] ;
	int	size ;
	int	i ;
	int	cancel = False ;
static	char	tmpfilename[MAXPATHLEN] ;

	assert( msgno <= p2->nmsg ) ;

	if( msg->state >= newstate )
	  return EXIT_OK ;

	/* Ignore newstate; we'll be reading the entire message
	 * at once.
	 */

	if( netWritef(p2->fd, "READ %d\r\n", msgno) < 0  ||
	    netReadln(p2->fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("pop2 fatal error reading message %d: %s\n",
	  	msgno, strerror(errno)) ;
	  (void) Pop2Abort(p2) ;
	  return POP_ERR ;
	}

	if( buffer[0] != '=' ) {	/* read failed */
	  removeNL(buffer) ;
	  logFile("pop2 fatal error reading message %d: %s\n", msgno, buffer) ;
	  (void) Pop2Abort(p2) ;
	  return POP_ERR ;
	}

	size = atoi(buffer+1) ;

	msg->type = POP2 ;
	msg->size = size ;


	/* Open temporary file.  If this fails, get out.  Messages will
	 * remain in remote mailbox.
	 */

#ifdef	COMMENT
	sprintf(tmpfilename, "%s/sortmail%d", tmpdir, getpid()) ;
#endif	/* COMMENT */
	msg->name = tmpfilename ;
	if( (msg->file = tmpOpen(tmpfilename)) == NULL )
	{
	  logFile(
	   "pop2 fatal error reading message %d, cannot open file %s, %s: %s\n",
	  	msgno, tmpfilename, strerror(errno));
	  (void) Pop2Abort(p2) ;
	  return TMPFILE_ERR ;
	}
	msg->tmpfile = True ;


	/* Copy input to tmp file.
	 * Scan for key headers lines while at it.
	 * Ignore case due to brain-damaged mailers.
	 */

	netWritef(p2->fd, "RETR\r\n") ;

	msg->inheader = True ;
	msg->scanning = NONE ;

	while( size > 0 )
	{
	  i = netReadln(p2->fd, buffer, min(size,sizeof(buffer)), to) ;
	  if( i <= 0 ) {
	    logFile("pop2 fatal error reading message %d: %s\n",
	  	msgno, strerror(errno)) ;
	    (void) Pop2Abort(p2) ;
	    return POP_ERR ;
	  }
	  ++msg->lines ;
	  removeCR(buffer) ;
	  parseMessage(msg, buffer) ;
	  if( !cancel && fputs(buffer, msg->file) == EOF ) {
	    logFile("pop2 fatal error, write to %s failed: %s\n",
	  	msg->name, strerror(errno));
	    (void) Pop2Abort(p2) ;
	    return POP_ERR ;
	  }
	  size -= i ;
	}

	parseMessageDone(msg) ;
	msg->state = HAVE_WHOLE ;

	return EXIT_OK ;
}


int
pop2FinishMessage( Pop2Folder *p2, int save, int to)
{
	char	buffer[1024] ;

	if( netWritef(p2->fd, save ? "ACKS\r\n": "ACKD\r\n" ) < 0  ||
	    netReadln(p2->fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("pop2 fatal error: %s\n", strerror(errno)) ;
	  (void) Pop2Abort(p2) ;
	  return POP_ERR ;
	}

	return EXIT_OK ;
}


void
closePop2(Pop2Folder *p2)
{
	netWritef(p2->fd, "QUIT\r\n") ;
	close(p2->fd) ;
	free(p2) ;
}



static	Pop2Folder *
Pop2Abort(Pop2Folder *p2)
{
	close(p2->fd) ;
	free(p2) ;
	return NULL ;
}
