/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

#include "syr_usertype.h"
#include "syr_tree.h"
#include "syr_bd.h"
#include "syr_proto.h"
#include "syr_abs.h"

extern struct Performances perfo;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | elague_tree                                                          |
  |         Faire du menage dans l'arbre                                 |
  |======================================================================| */
void elague_tree(struct node *pere)

{
  struct element *fa1,*fa2;
  struct child *f1;
  
  if (pere->lfils)
    {
      fa1=pere->lelement;
      pere->lelement=NULL;
      while(fa1)
	{
	  fa2=fa1;
	  fa1=fa1->suivant;
	  free(fa2);
	}
      
      f1=pere->lfils;
      while (f1)
	{
	  elague_tree(f1->fils);
	  f1 = f1->suivant;
	}
    }  
}	    
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | tuer_tree                                                            |
  |         Tuer l'arbre...                                              |
  |======================================================================| */
void tuer_tree(struct node *pere)

{
  struct element *fa1,*fa2;
  struct child *f1;
  
  /* destruction de la liste de facettes */
  fa1=pere->lelement;
  pere->lelement=NULL;
  while(fa1)
    {
      fa2=fa1;
      fa1=fa1->suivant;
      free(fa2);
    }
  /* destruction des fils */
  f1=pere->lfils;
  while (f1)
    {
      tuer_tree(f1->fils);
      f1 = f1->suivant;
    }

}	    

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | affiche_tree                                                         |
  |         Afficher un arbre                                            |
  |======================================================================| */
void affiche_tree(struct node *pere,int nbfils)

{
    int i,nbf;
    struct element *fa1;
    struct child *f1;


    fa1 = pere->lelement;
    if (fa1)
        if (nbfils==4)
	  if (SYRTHES_LANG == FR)
	    printf("centre %f %f taille x et y %f %f \n",
		   pere->xc,pere->yc,pere->sizx,pere->sizy);
	  else if (SYRTHES_LANG == EN)
	    printf("center %f %f size x et y %f %f \n",
		   pere->xc,pere->yc,pere->sizx,pere->sizy);
	  else
	    if (SYRTHES_LANG == FR)
	      printf("centre %f %f %f taille x et y %f %f %f \n",
		     pere->xc,pere->yc,pere->zc,
		     pere->sizx,pere->sizy,pere->sizz);
	    else if (SYRTHES_LANG == EN)
	      printf("center %f %f %f size x et y %f %f %f \n",
		     pere->xc,pere->yc,pere->zc,
		     pere->sizx,pere->sizy,pere->sizz);
    while (fa1) 
      {
	printf(" element  %d \n",fa1->num);
	fa1=fa1->suivant;
      }


    f1=pere->lfils;
    while (f1)
      {
	affiche_tree(f1->fils,nbfils);
	f1 = f1->suivant;
      }

}	    

