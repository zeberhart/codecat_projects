/*
 * Copyright(c) 1999 Andrew Lister
 *                        All rights reserved
 *
 * Copyright � 2001 by the LessTif Developers
 * Copyright � 2005 by the Xbae Developers
 *
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Id: multibyte.c,v 1.12 2005/03/30 06:31:43 tobiasoed Exp $
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif
#include <stdlib.h>

#include <locale.h>
#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif
#include <Xbae/Matrix.h>

#define USE_RENDER_TABLE 0

static String fallback[] = {
    "Multibyte*XbaeMatrix*columnWidths:    35",
#if USE_RENDER_TABLE
    "Multibyte*mw.renderTable:",
    "Multibyte*mw.renderTable.fontType:  FONT_IS_FONTSET",
    "Multibyte*mw.renderTable.fontName:  -*-*-*-*-*-*-*-*-*-*-*-*-iso8859-1,"
    "                                    -*-*-*-*-*-*-*-*-*-*-*-*-jisx0208.1983-0,"
    "                                    -*-*-*-*-*-*-*-*-*-*-*-*-jisx0201.1976-0",
#else
    "Multibyte*XbaeMatrix*fontList:		-*-*-*-*-*-*-*-*-*-*-*-*-iso8859-1;"
    "                                   -*-*-*-*-*-*-*-*-*-*-*-*-jisx0208.1983-0;"
    "                                   -*-*-*-*-*-*-*-*-*-*-*-*-jisx0201.1976-0: ",
#endif
    "Multibyte*XbaeMatrix*multiLineCell:    False",
    NULL
};

/*
 * Simple example of loaded Matrix
 */

String text[] =
{
    "�����Υ���ü���Ǥϡ��������ե饰�򻲾Ȥˤ� ��",
    "����������������ư�������Ǥ��ޤ��󡣤��Τ褦��ü���ǽ���",
    "���顼���ɤ��ˤϡ� e �ե饰����ꤹ��Ȥ��� t ��Ʊ���˻��ꤷ",
    "�ơ�����Ū�ˤ�ƤӽФ��Ƥ������������������β������",
    "��Ȥ�ȡ������ѡ�������ץ� (���դ�ʸ��) �ȥ��֥�����ץ� (",
    "���դ�ʸ��) �ν��Ϥ������Ȥ�ü�������ν��ϵ�ǽ����äƤ��Ƥ�",
    "�Բ�ǽ�ˤʤäƤ��ޤ��Ȥ�������������ޤ���ü�����ν�",
    "�� �ˤ�������ư��ʤ��ʤäƤ��ޤä��顢��Ȥ�",
};

int
main(int argc, char *argv[])
{
    Widget toplevel, mw;
    XtAppContext app;
    int row;

    setlocale(LC_ALL, "ja_JP");
    
    toplevel = XtVaAppInitialize(&app, "Multibyte",
				 NULL, 0,
				 &argc, argv,
				 fallback,
				 NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif

    mw = XtVaCreateManagedWidget("mw",
				 xbaeMatrixWidgetClass, toplevel,
				 XmNrows, sizeof(text)/sizeof(text[0]),
				 XmNcolumns, 1,
				 NULL);

    for (row = 0; row < sizeof(text)/sizeof(text[0]); row++) {
            XbaeMatrixSetCell(mw, row, 0, text[row]);
    }

    XtRealizeWidget(toplevel);

    XtAppMainLoop(app);
    
    /*NOTREACHED*/
    return 0;
}
