/*
 * This file is part of the Kvkbd project.
 * Copyright (C) 2007-2008 Todor Gyumyushev <yodor@developer.bg>
 * Copyright (C) 2008 Guillaume Martres <smarter@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "main.h"

#include <KDebug>

#include "mainwidget.h"

#include <QByteArray>
#include <QDBusConnection>

#include <KAboutData>
#include <KCmdLineArgs>
#include <KUser>
#include <KLocale>
#include <KUniqueApplication>

#include <stdio.h>

static const char description[] =
    I18N_NOOP("A virtual keyboard for KDE");

static const char version[] = "0.6.0";

int main(int argc, char **argv)
{
    KAboutData about("kvkbd", 0, ki18n("Kvkbd"), version, ki18n(description),
                     KAboutData::License_GPL, ki18n("(C) 2007-2008 The Kvkbd Developers"));
    about.addAuthor(ki18n("Todor Gyumyushev"), ki18n("Original Author"), "yodor@developer.bg");
    about.addAuthor(ki18n("Guillaume Martres"), ki18n("KDE4 port"), "smarter@ubuntu.com");
    about.setProgramIconName("preferences-desktop-keyboard");

    KCmdLineArgs::init(argc, argv, &about);
    KCmdLineOptions options;

    options.add("loginhelper", ki18n("Stand alone version for use with KDM or XDM.\n"
                                     "See Kvkbd Handbook for information on how to use this option."));
    KCmdLineArgs::addCmdLineOptions(options);
    KUniqueApplication::addCmdLineOptions();
    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

    // FIXME: kvkbd is hidden by KDM4
    // In loginhelper mode, settings can't be changed and there's no tray icon
    bool alone = false;
    if (args->isSet("loginhelper")) {
        alone = true;
    }
    args->clear();
    // TODO: find a better way to detect if we're launched from a display manager
    KUser user;
    if (alone && !user.isSuperUser()) {
        kError() << "LoginHelper mode can only be launched from a display manager!";
        return -1;
    }

    KvkbdApp app;
    MainWidget *widget = new MainWidget(&about, alone, 0);
    app.setActiveWindow(widget);
    return app.exec();
}

KvkbdApp::KvkbdApp() : KUniqueApplication()
{
}

KvkbdApp::~KvkbdApp()
{
}

// update keyboard mapping when it's changed by xmodmap/setxkbmap
bool KvkbdApp::x11EventFilter(XEvent *event)
{
    MainWidget *main = (MainWidget *)activeWindow();

    if (main) { //FIXME: is this check really necessary?
        if (event->type == MappingNotify) {
            XMappingEvent *e = (XMappingEvent *)event;
            if (e->request == MappingKeyboard) {
                main->mappingNotify(e);
            }
        }
    }
    return false;
}

// FIXME: What's this for?
//  int newInstance(){
//   MainWidget *main = (MainWidget *)mainWidget();
//   if (!main)
//   {
//    main = new MainWidget(const_cast<KAboutData *>(aboutData()),false, 0, "kvkbd");
//    setMainWidget(main);
//   }
//   return 0;
//  };
