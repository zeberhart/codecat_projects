#ifndef lint
static const char sccsid[] = "@(#)locking.c 1.4 02/03/11 falk" ;
static const char rcsid[] = "$Id: locking.c,v 1.6 2003/12/22 06:03:14 efalk Exp $" ;
#endif

/* Lock/unlock files:
 *
 * int lockFile(char *, int flags, mode_t, bool wait, LockInfo *lock);
 *
 *	open and lock a file.  Return fd on success, -1 on failure.
 *	Error codes:
 *		EWOULDBLOCK	file already locked and wait is False
 *		EPERM		unable to write <filename>.lock
 *		ETIMEDOUT	dot lock timed out
 *
 * void unLockFile(FileLock *)
 *
 *	unlock and close a file.
 *
 *
 *
 * There are four ways to lock a file, and we try all which are
 * available.
 *
 *	lockf()		old bsd, not in linux
 *	flock()		advisory lock
 *	fcntl()		"discretionary" lock
 *	filename.lock
 *
 * Courtesy of Mike Kupfer:
 *
 *
 * Here's something that Carl Gutekunst sent out a few years ago.  It's a
 * bit dated, but still useful, I think.
 * 
 * An overview of the mail locking protocols used by Sun and 
 * other companies (through the CDE partnetship).
 * 
 * Types of locks
 * --------------
 * 
 * There are two types of "locks" that can be applied against a mail
 * folder: "long term locks" and "short term locks".
 * 
 * A long term lock is meant to preclude multiple mail user agents (such
 * as mailtool or dtmail) from accessing the same mailbox concurrently.
 * It is held as long as a mail user agent has the mailbox open for any
 * purpose other than just to append data to the end of the mailbox. The
 * holder of a long term lock is assured the mailbox will never be reduced
 * in size, and that existing contents will never be changed. The holder
 * of a long term lock may not perform any actual reading or writing of
 * the mailbox.
 * 
 * A short term lock is meant to preclude multiple mail user or delivery
 * agents (such as mailtool, dtmail, sendmail, binmail) from performing
 * actual reading or writing operations on a mailbox. The short term lock
 * is held by the agent only as long as is necessary for the agent to
 * complete the reading or writing operation, after which the lock must be
 * immediately released.
 * 
 * An agent may hold one of three combinations of locks: long term only,
 * short term only, or both long term and short term.
 * 
 * If an agent holds only a long term lock on a mailbox, it is the sole
 * agent that may change the current contents of the mailbox. The agent is
 * the only process that may request a short term lock for the purposes of
 * changing data already in the mailbox. The agent is guaranteed
 * consistency in this regard.
 * 
 * If an agent holds only a short term lock on a mailbox, it is permitted
 * only to append data to the mailbox. Any data appended to the mailbox
 * must be in a proper "mailbox" format.
 * 
 * If an agent holds both a long term and a short term lock on a mailbox,
 * it has exclusive access to that mailbox. The agent may perform any
 * operations on the mailbox, including reordering and removal of data.
 * 
 * Long term locks: 
 * ----------------
 * 
 * Overview
 * 
 * 	Before a mail user agent opens a mailbox for the purposes of
 * 	reading and acting on the contents, it first must obtain a
 * 	long lock on that mailbox. If it cannot obtain the long
 * 	lock, this indicates that another mail user agent has the
 * 	mailbox open, and the contents are subject to any type of
 * 	change at any time. The mail user agent is forbidden from
 * 	doing the same as long as the long lock cannot be obtained.
 * 
 * 	When using tooltalk as a locking mechanism, there is a
 * 	procedure whereby one process can ask another process that
 * 	is holding a long term lock to "release" the lock.
 * 
 * Mailtool, Overview
 * 
 * 	OpenWindows mailtool uses one of two types of methods for
 * 	establishing a long term lock: tooltalk or lockf. If
 * 	tooltalk locking is enabled, it uses tooltalk. If tooltalk
 * 	locking is not enabled, it uses lockf only if the mailbox
 * 	is mounted on a local file system. If the mailbox is not
 * 	mounted on a local file system, NO LONG TERM LOCKING IS USED.
 * 
 * Mailtool, using tooltalk, applied to all folders:
 * 
 * 	Tooltalk locking within mailtool is disabled by default. The
 * 	.mailrc variable 'ttlock' must be set for tooltalk locking
 * 	to be used. If the variable is not set, lockf locking is
 * 	used.
 * 
 * 	send a tooltalk REQUEST, file scoped to the mail folder,
 * 	with an message operation of "tlock".
 * 
 * 	If it is "handled" (that is, if there is someone listening for
 * 	that message), then the file assumed to be currently being
 * 	used by another process.  If there was no handler, then the
 * 	file is unused.
 * 
 * 	If unused, register a pattern on the file (so we will be able
 * 	to see requests from other processes).
 * 
 * 	If the file is locked, optionally send a tooltalk NOTICE,
 * 	file scoped to the mail folder, with a message operation of
 * 	"rulock".  Sleep 5 seconds, and retry the lock.
 * 
 * Mailtool, using lockf (no tooltalk):
 * 
 * 	Mailtool will use lockf() to lock the mailbox only if the
 * 	mailbox is on a locally mounted file system. This is due to
 * 	problems with the lockf() protocol and the lockd() network
 * 	lock daemon which can result in indefinite hangs. The work
 * 	around, regrettably, is to do no locking at all on mailboxes
 * 	residing on network file system when tooltalk locking is not
 * 	available.
 * 
 * 	use lockf(fd, F_TLOCK,...) to lock the mail folder.  If
 * 	currently locked, use fcntl(fd, F_GETLK,...) to get the pid of
 * 	the process (if the locking process is local to the machine).
 * 	Optionally send that process a SIGUSR1 signal, which will
 * 	cause a mailtool process to do a "done" action.
 * 
 * Mailx:
 * 	Uses fcntl(fd, F_SETLKW, F_RDLCK or F_WRLCK), which blocks
 * 	until the lock is released.  However, since mailx makes a temporary
 * 	copy of the folder, this lock is in use only as long as the
 * 	copy of the file is being made.
 * 
 * 	In addition, the file $HOME/.Maillock is made whenever any
 * 	the spool file is opened.  This is used as a per-user semaphore
 * 	allowing only one folder per user to be locked (but it doesn't
 * 	help when two different users are editing the same folder.
 * 
 * dtmail, Overview
 * 
 * 	dtmail uses a similar scheme to that of OpenWindows
 * 	mailtool.  This is complicated somewhat in that the CDE
 * 	partners (HP and IBM) have changed the locking machinery
 * 	used by sendmail, and thus locking is done differently on
 * 	those systems.
 * 
 * 	On all systems, dtmail will first attempt to perform
 * 	tooltalk locking to establish a long term lock. This
 * 	behavior is enabled by default. If it is disabled <<how??>>
 * 	then lockf() locking is performed on Solaris systems only.
 * 
 * 	If the mailbox is not mounted on a local file system (on
 * 	Solaris systems) or the system is either HP or IBM, NO LONG
 * 	TERM LOCKING IS USED if tooltalk locking is disabled.
 * 
 * dtmail, using tooltalk:
 * 
 * 	Tooltalk locking within dtmail is enabled by default. The
 * 	.mailrc variable 'cdenotooltalklock' must be set for tooltalk
 * 	locking to be disabled.
 * 
 * 	Dtmail uses standard Desktop Messaging Set to perform file
 * 	locking. Dtmail will first express interest in a particular
 * 	file by calling ttdt_file_join() and then try to determine
 * 	whether other user mail agent is using this file by calling
 * 	ttdt_Get_Modified(). If this file is available, dtmail will
 * 	complete the locking process by sending out a TTDT_MODIFIED
 * 	event. Otherwise, if the file is being used by another user
 * 	mail agent, dtmail will send out a TTDT_SAVE event to request
 * 	the other process to save changes and release the lock.
 * 
 * 	After dtmail has done with the file, it will release the lock
 * 	by calling ttdt_file_quit() to unregister interest in the file.
 * 
 * 	In order to be backward compatible with OpenWindow's mailtool,
 * 	dtmail also sends out messages using mailtool's protocol.
 * 	Those messages are identical to mailtool's own execept that
 * 	they contain an extra user argument so dtmail can determine
 * 	the message's origin.
 * 
 * dtmail, using lockf()
 * 
 * 	Identical to mailtool, in that lockf(F_LOCK) is used to
 * 	lock the mailbox. If this fails (the lock is held by another
 * 	process), the lock is denied and dtmail refuses to access
 * 	the mailbox.
 * 
 * Roam, berkeley driver (I didn't see the solaris one):
 * 
 * 	Uses flock(fd, LOCK_EX|LOCK_NB) to lock the mailbox.
 * 	If this exclusive lock cannot be gotten, read the existing lock file
 * 	for the pid of the "holding" process (which is assumed to be
 * 	on the same machine).  Send this process a SIGUSR2.  Then
 * 	try and get the flock() to work once a second for 15 more
 * 	seconds.
 * 
 * 	The lock is held while the mailbox is open.
 * 
 * 	If imapd receives a SIGUSR2, and the stream's driver (only
 * 	one stream supported? Its a global) is "bezerk", "mbox", or
 * 	"mmdf", the stream is turned into readonly (what happens
 * 	to pending changes?)
 * 
 * 
 * Short term locks
 * ----------------
 * 
 * Overview
 * 
 * 	Before a mail user or deliver agent performs reading or
 * 	writing of a mailbox, it first must obtain a short term lock
 * 	on that mailbox.  If it cannot obtain the short term lock,
 * 	this indicates that another agent is currently performing an
 * 	i/o operation. The requesting agent should wait for up to 5
 * 	minutes periodically checking to see if the lock has been
 * 	released, after which it can try again to obtain the lock.
 * 
 * Sendmail:
 * 
 * 	Normally, sendmail uses /bin/mail to deliver messages.
 * 	In later version of Solaris, sendmail uses /usr/lib/mail.local
 * 	to deliver messages.
 * 
 * 	But if sendmail is delivering directly to a file, there
 * 	is no locking.
 * 
 * 	Note that sendmail acts differently on HP and IBM systems
 * 	than it does on Solaris systems.
 * 
 * mailx:
 * /bin/mail:
 * 
 * 	common code implemented by libmail.a (no dynamic lib available)
 * 
 * 	Only locks things in /var/mail.  No locking outside that dir.
 * 
 * 	Make a temp file in /var/mail.  Write a "0" into the file
 * 	(a comment says that this should be a process id, so other
 * 	processes can send you a signal; but that isn't done by the
 * 	library).
 * 
 * 	Attempt to link this temp file to "/var/mail/${USER}.lock"
 * 	If the link succeeds, remove the temp file.
 * 
 * 	If the link succeeds, remove the temp file; you now own the
 * 	mail file.
 * 
 * 	If the link fails, get the creation time of the temp file
 * 	and the existing lock.  If the lock file is over 5 minutes
 * 	older than the temp file, blow away the current lock
 * 	and try and lock again.
 * 
 * mailtool (uses mmap for reading file, so it really is reading for
 * 	the entire session...):
 * 
 * 	Tries to create ${mailfilename}.lock; if that works then
 * 	the lock is owned.
 * 
 * 	If the create fails then loop, trying to relock every
 * 	5 seconds.  If the lock file is over 5 minutes old then
 * 	ignore it.
 * 
 * dtmail, overview
 * 
 * 	Similar to mailtool but complicated because of differences
 * 	in partners (HP and IBM) implementations. It does mmap()
 * 	mailboxes for the purposes of reading, which means the
 * 	mailbox is "read" for the entire time dtmail has a long
 * 	lock on the mailbox. 
 * 
 * dtmail, Solaris systems
 * 
 * 	Identical to /bin/mail
 * 
 * dtmail, HP and IBM systems
 * 
 * 	These systems use lockf(F_LOCK) to serialize access to the 
 * 	mailbox.
 * 
 * Roam:
 * 	Uses mailbox.lock protocol, waiting for 5 minutes.  The
 * 	algorithm is subtly different (it looks at link counts (!)
 * 	to see if the lock succeeds, with some comments about NFS
 * 	brain damage causing these crimes.
 * 
 */


#include <stdio.h>
#include <stdlib.h>	/* defines getenv(3) */
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>		/* defines MAXPATHLEN */
#include <sys/file.h>

#include "sortmail.h"
#include "utils.h"
#include "locking.h"


extern	int	errno ;


#define	LOCKTIME	300

#ifndef	O_SYNC		/* OSX doesn't seem to have O_SYNC */
#define	O_SYNC	0
#endif

static	int	makelocknames() ;
static	int	trylock() ;
static	int	dotlock() ;


/* A word of explanation is in order here.  Under Solaris, and
 * quite possibly all versions of Unix, there is a race condition
 * in locking a mail folder.  When you open a file for append,
 * writing begins at the end of the file AT THE TIME IT WAS
 * OPENED.  However, the actual lock is not acquired until some time
 * *after* the file is opened. 
 *
 * So suppose that someone is viewing their mail in Sun's
 * mailtool, which locks the entire folder while it's being
 * viewed (bad idea, but there you have it.)  Further suppose
 * that the user has deleted some messages.  The deletes don't
 * take place until the user selects "done".  Thus, when sortmail
 * is executed, it will open the mailbox and note the
 * current size.  It then attempts to lock the mailbox and is
 * suspended while mailtool uses the box.
 *
 * When mailtool closes the mailbox, it truncates the file and
 * unlocks it.  Sortmail then continues, and attempts to write
 * data to the previous the end of the mailbox, which is now well
 * past the actual end.  This new data is lost.
 *
 * To fix the problem, we inquire the size of the mailbox before
 * and after it's locked, and re-lock if there was a change.
 *
 *
 * See the locking notes at the start of this file.  In most cases,
 * dotfile locking is the only kind that is required.
 */


int
lockFile(char	*filename,
	int	flags,
	mode_t	mode,
	int	wait,
	LockInfo *lock)
{
#if	LOCKFCNTL
	int	rval1=0, rval2=0, rval3=0 ;
	int	err2=0, err3=0 ;
#endif
	int	fd ;
	char	unique[MAXPATHLEN] ;
	char	lockname[MAXPATHLEN] ;
	off_t	size0, size1 ;

	lock->dotname = NULL ;
	lock->fd = -1 ;

	errno = 0 ;

	logFilev(2, "lock %s\n", filename) ;

	do
	{
	  if( (fd = open(filename,flags,mode)) == -1 )
	    return -1 ;

	  if( (size0 = fileSize(filename)) == -1 ) {
	    close(fd) ;
	    return -1 ;
	  }

#if	LOCKLOCKF
	  /* TODO */
	  /* Note: do not use on remotely-mounted mailboxes, due to deadlock */
#endif

#if	LOCKFLOCK
	  /* Use flock.  If fails with EWOULDBLOCK, that's a true failure.
	   * Else, keep going and try other locking techniques.
	   */

	  if( (rval2=flock(fd, wait ? LOCK_EX : LOCK_EX|LOCK_NB)) == -1 &&
	      (err2=errno) == EWOULDBLOCK )
	  {
	    close(fd) ;
	    return -1 ;
	  }
#endif

#if	LOCKFCNTL
	  {
	    /* use fcntl.  This will fail on some systems because flock()
	     * or lockf() already suceeded.  Just keep going.
	     */

	    struct flock l ;
	    l.l_type = F_WRLCK ;
	    l.l_whence = SEEK_SET ;
	    l.l_start = 0 ;
	    l.l_len = 0 ;
	    if( (rval3=fcntl(fd, wait ? F_SETLKW : F_SETLK, &l)) == -1  &&
		(err3=errno) == EACCES )
	      {
		close(fd) ;
		errno = EWOULDBLOCK ;
		return -1 ;
	      }
	  }
#endif

#if	LOCKDOTLOCK
	  if( makelocknames(filename, unique, lockname) == 0  &&
	      trylock(unique, lockname, wait, lock) == -1  &&
	      errno == EWOULDBLOCK  &&
	      wait )
	  {
	    /* force the lock */
	    if( verbose >= 1 ) {
	      FILE *ifile ;
	      int pid = 0 ;
	      if( (ifile = fopen(lockname, "r")) != NULL ) {
		fscanf(ifile, "%d", &pid) ;
		fclose(ifile) ;
	      }
	      logFilev(1, "overriding lock file %s, held by process %d\n",
		lockname, pid) ;
	    }
	    (void) unlink(lockname) ;
	    /* and try one more time */
	    if( trylock(unique, lockname, wait, lock) == -1  &&
		errno == EWOULDBLOCK )
	    {
	      close(fd) ;
	      return -1 ;
	    }
	  }
#endif


	  if( (size1 = fileSize(filename)) == -1 ) {
	    unLockFile(lock) ;
	    close(fd) ;
	    return -1 ;
	  }

	  if( size0 != size1 ) {
	    unLockFile(lock) ;
	    close(fd) ;
	  }

	} while( size0 != size1 ) ;

	/* TODO: what if *all* locking methods failed? */

	return lock->fd = fd ;
}





#if	LOCKDOTLOCK

	/* This is the most involved method, but possibly the most
	 * reliable.  Create the file <filename>.lock.  The traditional
	 * method is to simply create the file with open(O_CREAT|O_EXCL)
	 * which fails if the file already exists.
	 *
	 * Unfortunately, there is a race condition caused by NFS, so
	 * we need a more complex method.  The open(2) man page says to
	 * create a unique filename on the destination file system and
	 * ensure that the new file has a link count of only 1.  Then
	 * rename it to <filename>.lock
	 *
	 * Unique name must include host name and process id.  In a large
	 * NFS environment (multiple domains) we should use the fully-
	 * qualified domain name.
	 */


static	int
makelocknames(
	char	*filename,
	char	unique[MAXPATHLEN],
	char	lockname[MAXPATHLEN])
{
	char	hostname[MAXHOSTNAMELEN] ;
	char	*ptr ;
	int	filelen, pathlen, hostlen ;

	filelen = strlen(filename) ;

	if( gethostname(hostname, MAXHOSTNAMELEN) == -1 )
	  strcpy(hostname,"localhost") ;
	hostname[MAXHOSTNAMELEN-1] = '\0' ;
	hostlen = strlen(hostname) ;

	strncpy(lockname, filename, MAXPATHLEN) ;
	lockname[MAXPATHLEN-1] = '\0' ;
	if( (ptr = strrchr(lockname, '/')) == NULL )
	  ptr = lockname ;
	*ptr = '\0' ;
	pathlen = ptr-lockname ;

	/* sanity-check lengths now */

	if( filelen + sizeof(".lock") > MAXPATHLEN  ||
	    pathlen + sizeof(".sm") + hostlen + 20 > MAXPATHLEN )
	{
	  errno = ENAMETOOLONG ;
	  return -1 ;
	}

	sprintf(unique, "%s/sm.%s.%d", lockname, hostname, getpid()) ;
	sprintf(lockname, "%s.lock", filename) ;
	return 0 ;
}



	/* try the lock.  If wait is True, keep trying at 1-second
	 * intervals until timeout.
	 */

static	int
trylock(char *unique, char *lockname, bool wait, LockInfo *lock)
{
	int	rval=0 ;
	int	retries ;

	for( retries = wait ? LOCKTIME : 0 ;
	     (rval = dotlock(unique, lockname, lock)) == -1  &&
	     errno == EWOULDBLOCK  &&
	     --retries >= 0; )
	  sleep(1) ;

	if( rval == -1 && !terse ) {
	  logFilev(1, "unable to acquire lock file %s, %s, retry time = %d\n",
	    lockname, strerror(errno), retries = wait ? LOCKTIME : 0) ;
	}

	return rval ;
}



	/* make one attempt to obtain the lock */

static	int
dotlock( char *unique, char *lockname, LockInfo *lock)
{
	struct stat statbuf ;
	int	err ;
	int	fd ;
	char	buf[256] ;

	if( (fd = open(unique, O_WRONLY|O_CREAT|O_TRUNC|O_EXCL|O_SYNC, 0))
		== -1 )
	{
	  if( errno == EEXIST )
	    fprintf(stderr, "file %s already exists, should not happen\n",
	      unique) ;
	  return -1 ;
	}

	sprintf(buf, "%d\nlocked by sortmail, pid %d\n", getpid(), getpid()) ;
	(void) write(fd, buf, strlen(buf)) ;
	close(fd) ;

	if( link(unique, lockname) == -1 )
	{
	  (void) unlink(unique) ;
	  if( errno == EEXIST )
	    errno = EWOULDBLOCK ;
	  return -1 ;
	}

	if( stat(lockname, &statbuf) == -1 )
	{
	  err = errno ;
	  (void) unlink(unique) ;
	  (void) unlink(lockname) ;
	  errno = err ;
	  return -1 ;
	}

	if( statbuf.st_nlink > 2 )
	{
	  (void) unlink(unique) ;
	  errno = EWOULDBLOCK ;
	  return -1 ;
	}

	/* success */

	lock->dotname = strdup(lockname) ;
	(void) unlink(unique) ;
	return 0 ;
}
#endif



void
unLockFile( LockInfo *lock )
{
	/* flock, lockf, fcntl all release lock when file is closed.
	 * Only need to release dotlock here
	 */

#if	LOCKDOTLOCK
	if( lock->dotname != NULL ) {
	  logFilev(2, "unlock %s\n", lock->dotname) ;
	  (void) unlink(lock->dotname) ;
	  free(lock->dotname) ;
	  lock->dotname = NULL ;
	}
#endif
}
