/*
     This file is part of GNUnet.
     (C) 2010, 2011 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/setup/gnunet-setup-transport-https.c
 * @brief support for HTTPS configuration
 * @author Christian Grothoff
 */
#include "gnunet-setup.h"
#include <gnunet/gnunet_resolver_service.h>
#include <gnunet/gnunet_nat_lib.h>
#include "gnunet-setup-transport-test.h"


/**
 * Function called whenever the user wants to test the
 * HTTPS configuration.
 */
void
GNUNET_setup_transport_https_test_button_clicked_cb ()
{
  GNUNET_setup_transport_test ("transport-https_server", GNUNET_YES,
                               "GNUNET_setup_transport_https_test_success_image",
                               "GNUNET_setup_transport_https_test_fail_image");
}


/* end of gnunet-setup-transport-https.c */
