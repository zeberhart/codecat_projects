#ifndef __RESOURCE_language_H__
#define __RESOURCE_language_H__

#include <gio/gio.h>

extern GResource *language_get_resource (void);
#endif
