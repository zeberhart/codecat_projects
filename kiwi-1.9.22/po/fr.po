# vim: encoding=utf8
#
# French translation for the Kiwi project.
#
# Copyright (C) 2007 Johan Dahlin
# This file is distributed under the same license as the kiwi package.
# Benoit Myard <myardbenoit@gmail.com>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: kiwi 1.9.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-28 17:38-0300\n"
"PO-Revision-Date: 2007-04-03 12:41-0300\n"
"Last-Translator: Benoit Myard <myardbenoit@gmail.com>\n"
"Language-Team: French <myardbenoit@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../kiwi/currency.py:164
msgid "Currency"
msgstr "Devise"

#: ../kiwi/currency.py:180 ../kiwi/currency.py:197
#, python-format
msgid "%s can not be converted to a currency"
msgstr "%s n'a pu être converti vers une devise"

#: ../kiwi/datatypes.py:242
msgid "String"
msgstr "Chaîne"

#: ../kiwi/datatypes.py:256
msgid "Unicode"
msgstr "Unicode"

#: ../kiwi/datatypes.py:270
msgid "Integer"
msgstr "Entier"

#: ../kiwi/datatypes.py:298
#, python-format
msgid "%s could not be converted to an integer"
msgstr "%s n'a pu être converti en entier"

#: ../kiwi/datatypes.py:304
msgid "Long"
msgstr "Entier long"

#: ../kiwi/datatypes.py:309
msgid "Boolean"
msgstr "Booléen"

#: ../kiwi/datatypes.py:324
#, python-format
msgid "'%s' can not be converted to a boolean"
msgstr "'%s' n'a pu être converti en un booléen"

#: ../kiwi/datatypes.py:330
msgid "Float"
msgstr "Nombre en virgule flottante"

#: ../kiwi/datatypes.py:373 ../kiwi/datatypes.py:393
#, python-format
msgid "This field requires a number, not %r"
msgstr "Ce champ nécessite un nombre et non pas %r"

#: ../kiwi/datatypes.py:382
msgid "Decimal"
msgstr "Nombre décimal"

#: ../kiwi/datatypes.py:422 ../kiwi/datatypes.py:427
msgid "mm"
msgstr ""

#: ../kiwi/datatypes.py:423
msgid "yy"
msgstr ""

#: ../kiwi/datatypes.py:424
msgid "dd"
msgstr ""

#: ../kiwi/datatypes.py:425
msgid "yyyy"
msgstr ""

#: ../kiwi/datatypes.py:426
msgid "hh"
msgstr ""

#: ../kiwi/datatypes.py:428
msgid "ss"
msgstr ""

#: ../kiwi/datatypes.py:429
msgid "hh:mm:ss"
msgstr ""

#. FIXME: locale specific
#: ../kiwi/datatypes.py:431
msgid "hh:mm:ss LL"
msgstr ""

#: ../kiwi/datatypes.py:526 ../kiwi/datatypes.py:561
msgid "You cannot enter a year before 1900"
msgstr ""

#: ../kiwi/datatypes.py:555
#, python-format
msgid "This field requires a date of the format \"%s\" and not \"%s\""
msgstr "Ce champ nécessite une date de la forme \"%s\" et non pas \"%s\""

#: ../kiwi/datatypes.py:567
msgid "Time"
msgstr "Heure"

#: ../kiwi/datatypes.py:582
msgid "Date and Time"
msgstr "Date et Heure"

#: ../kiwi/datatypes.py:597
msgid "Date"
msgstr "Date"

#: ../kiwi/datatypes.py:612
msgid "Object"
msgstr "Objet"

#: ../kiwi/datatypes.py:620
msgid "Enum"
msgstr ""

#: ../kiwi/datatypes.py:689
msgid "You have a thousand separator to the right of the decimal point"
msgstr "Un séparateur des milliers se trouve dans la partie décimale"

#: ../kiwi/datatypes.py:701
msgid "Inproperly placed thousands separator"
msgstr "Séparateur des milliers mal placé"

#: ../kiwi/datatypes.py:706
#, python-format
msgid "Inproperly placed thousand separators: %r"
msgstr "Séparateur des milliers mal placé : %r"

#: ../kiwi/datatypes.py:724
#, python-format
msgid "You have more than one decimal point (\"%s\")  in your number \"%s\""
msgstr "Il y a plus d'un séparateur décimal (\"%s\") dans le nombre \"%s\""

#: ../kiwi/ui/listdialog.py:194
#, python-format
msgid "Do you want to remove %s ?"
msgstr ""

#: ../kiwi/ui/dateentry.py:74
msgid "_Today"
msgstr "_Aujourd'hui"

#: ../kiwi/ui/dateentry.py:75
msgid "_Cancel"
msgstr "_Annuler"

#: ../kiwi/ui/dateentry.py:76
msgid "_Select"
msgstr "_Séléctionner"

#: ../kiwi/ui/wizard.py:211
msgid "Finish"
msgstr "Terminer"

#: ../kiwi/ui/entry.py:599
#, python-format
msgid "'%s' is not a valid object"
msgstr "'%s' n'est pas un objet valide"

#: ../kiwi/ui/objectlist.py:2079
msgid "Total:"
msgstr "Total :"

#: ../kiwi/ui/search.py:66
msgid "Any"
msgstr ""

#: ../kiwi/ui/search.py:73
#, fuzzy
msgid "Today"
msgstr "_Aujourd'hui"

#: ../kiwi/ui/search.py:81
msgid "Yesterday"
msgstr ""

#: ../kiwi/ui/search.py:89
msgid "Last week"
msgstr ""

#: ../kiwi/ui/search.py:97
msgid "Last month"
msgstr ""

#: ../kiwi/ui/search.py:197
msgid "From:"
msgstr ""

#: ../kiwi/ui/search.py:207
#, fuzzy
msgid "To:"
msgstr "Total :"

#: ../kiwi/ui/search.py:218
msgid "Custom day"
msgstr ""

#: ../kiwi/ui/search.py:219
msgid "Custom interval"
msgstr ""

#: ../kiwi/ui/search.py:561
msgid "Search:"
msgstr ""

#: ../kiwi/ui/dialogs.py:101
msgid "Show more _details"
msgstr "Afficher plus de _détails"

#: ../kiwi/ui/dialogs.py:248
msgid "Open"
msgstr "Ouvrir"

#: ../kiwi/ui/dialogs.py:277
#, python-format
msgid "Could not open file \"%s\""
msgstr "Impossible d'ouvrir le fichier \"%s\""

#: ../kiwi/ui/dialogs.py:278
#, python-format
msgid "The file \"%s\" could not be opened. Permission denied."
msgstr "Impossible d'ouvrir le fichier \"%s\". Permission refusée."

#: ../kiwi/ui/dialogs.py:292
#, fuzzy
msgid "Select folder"
msgstr "_Séléctionner"

#: ../kiwi/ui/dialogs.py:315
#, fuzzy, python-format
msgid "Could not select folder \"%s\""
msgstr "Impossible d'ouvrir le fichier \"%s\""

#: ../kiwi/ui/dialogs.py:316
#, fuzzy, python-format
msgid "The folder \"%s\" could not be selected. Permission denied."
msgstr "Impossible d'ouvrir le fichier \"%s\". Permission refusée."

#: ../kiwi/ui/dialogs.py:323
#, python-format
msgid "A file named \"%s\" already exists"
msgstr "Un fichier nommé \"%s\" existe déjà"

#: ../kiwi/ui/dialogs.py:324
msgid "Do you wish to replace it with the current one?"
msgstr "Souhaitez-vous le remplacer par l'élément courant ?"

#: ../kiwi/ui/dialogs.py:330
msgid "Replace"
msgstr "Remplacer"

#: ../kiwi/ui/dialogs.py:336
msgid "Save"
msgstr "Enregistrer"

#: ../kiwi/ui/dialogs.py:391
msgid "Password:"
msgstr "Mot de passe :"

#: ../kiwi/ui/proxywidget.py:65
#, python-format
msgid "Could not load image: %s"
msgstr "Impossible de charger l'image : %s"

#: ../kiwi/ui/proxywidget.py:308
#, python-format
msgid "'%s' is not a valid value for this field"
msgstr "'%s' n'est pas une valeur autorisée pour ce champ"

#: ../kiwi/ui/proxywidget.py:342
msgid "This field is mandatory"
msgstr "Ce champ est obligatoire"
