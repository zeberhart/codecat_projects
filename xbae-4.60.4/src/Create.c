/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 * Copyright(c) 1995-99 Andrew Lister
 * Copyright � 1999, 2000, 2001, 2002, 2003, 2004, 2005 by the Xbae Developers.
 *
 *                        All rights reserved
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * MatrixWidget Author: Andrew Wason, Bellcore, aw@bae.bellcore.com
 *
 * $Id: Create.c,v 1.86 2006/01/19 18:43:47 tobiasoed Exp $
 */

/*
 * Create.c created by Andrew Lister (28 Jan, 1996)
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include <Xm/Xm.h>
#include <Xm/ScrollBar.h>

#include <Xbae/MatrixP.h>
#include <Xbae/Macros.h>
#include <Xbae/Utils.h>
#include <Xbae/Actions.h>
#include <Xbae/Create.h>

#include <XbaeDebug.h>

static Pixmap createInsensitivePixmap(XbaeMatrixWidget mw);

static Boolean xbaeEanableThinThickness(Widget widget) {
#if XmVersion >= 2
        Boolean enable_thin_thickness;
        Display *display = XtDisplay(widget);
        Widget xmDisplay = XmGetXmDisplay(display);

        XtVaGetValues(xmDisplay, 
            XmNenableThinThickness, &enable_thin_thickness, 
            NULL);

        return enable_thin_thickness;
#else
        return False
#endif
}

void xbaeCopyMarginSize(Widget widget, int offset, XrmValue *value)
{
        static const Dimension thin = 1;
        static const Dimension thick = 3;

        value->addr = (XtPointer) (xbaeEanableThinThickness(widget) ? &thin : &thick);
}

void xbaeCopyThickness(Widget widget, int offset, XrmValue *value)
{
        static const Dimension thin = 1;
        static const Dimension thick = 2;

        value->addr = (XtPointer) (xbaeEanableThinThickness(widget) ? &thin : &thick);
}

void xbaeCopySpace(Widget widget, int offset, XrmValue *value)
{
        static const Dimension thin = 2;
        static const Dimension thick = 4;

        value->addr = (XtPointer) (xbaeEanableThinThickness(widget) ? &thin : &thick);
}

void xbaeCopyBackground(Widget widget, int offset, XrmValue * value)
{
        value->addr = (XtPointer) & (widget->core.background_pixel);
}

void xbaeCopyForeground(Widget widget, int offset, XrmValue * value)
{
        value->addr = (XtPointer) & (((XmManagerWidget) widget)->manager.foreground);
}

void xbaeCopyDoubleClick(Widget widget, int offset, XrmValue * value)
{
        static int interval;

        interval = XtGetMultiClickTime(XtDisplay(widget));
        value->addr = (XtPointer) & interval;
}

/**************************************************************************************************/

static void *copyUnchecked(void *src, int n, size_t size) 
{
        void *copy = NULL;
        if (n) {
                copy = XtMalloc(n * size);
                memcpy(copy, src, n * size);
        }
        return copy;
}

void xbaeCopyColumnButtonLabels(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.column_button_labels = copyUnchecked(mw->matrix.column_button_labels, 
                                                        mw->matrix.columns, 
                                                        sizeof *mw->matrix.column_button_labels);
        xbaeObjectUnlock((Widget) mw);
}

void xbaeCopyRowButtonLabels(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.row_button_labels = copyUnchecked(mw->matrix.row_button_labels, 
                                                     mw->matrix.rows,
                                                     sizeof *mw->matrix.row_button_labels);
        xbaeObjectUnlock((Widget) mw);
}

void xbaeCopyShowColumnArrows(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.show_column_arrows = copyUnchecked(mw->matrix.show_column_arrows,
                                                      mw->matrix.columns,
                                                      sizeof *mw->matrix.show_column_arrows);
        xbaeObjectUnlock((Widget) mw);
}

void xbaeCopyColumnFontBold(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.column_font_bold = copyUnchecked(mw->matrix.column_font_bold,
                                                    mw->matrix.columns,
                                                    sizeof *mw->matrix.column_font_bold);
        xbaeObjectUnlock((Widget) mw);
}

void xbaeCopyRowUserData(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.row_user_data = copyUnchecked(mw->matrix.row_user_data,
                                                 mw->matrix.rows,
                                                 sizeof *mw->matrix.row_user_data);
        xbaeObjectUnlock((Widget) mw);
}

void xbaeCopyColumnUserData(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.column_user_data =  copyUnchecked(mw->matrix.column_user_data,
                                                     mw->matrix.columns,
                                                     sizeof *mw->matrix.column_user_data);
        xbaeObjectUnlock((Widget) mw);
}

/**************************************************************************************************/

static unsigned char *copyAlignments(Widget w, unsigned char *alignments, int n)
{
        unsigned char *copy = NULL;
        int i;
        Boolean bad = False;

        if (n) {
                copy = (unsigned char *) XtMalloc(n * sizeof(unsigned char));

                for (i = 0; i < n; i++) {
                        if (bad) {
                                copy[i] = XmALIGNMENT_BEGINNING;
                        } else if (alignments[i] == BAD_ALIGNMENT) {
                                copy[i] = XmALIGNMENT_BEGINNING;
                                bad = True;
                                XtAppWarningMsg(XtWidgetToApplicationContext(w),
                                                "copyAlignments", "tooShort", "XbaeMatrix",
                                                "XbaeMatrix: Column or column label alignments array is too short",
                                                NULL, 0);
                        } else {
                                copy[i] = alignments[i];
                        }
                }
        }
        
        return copy;
}

void xbaeCopyColumnAlignments(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.column_alignments = copyAlignments((Widget) mw, mw->matrix.column_alignments, mw->matrix.columns);
        xbaeObjectUnlock((Widget) mw);
}

void xbaeCopyColumnLabelAlignments(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.column_label_alignments = copyAlignments((Widget) mw, mw->matrix.column_label_alignments, mw->matrix.columns);
        xbaeObjectUnlock((Widget) mw);
}

/**************************************************************************************************/

static unsigned char *copyShadowTypes(Widget w, unsigned char *shadow_types, int n)
{
        unsigned char *copy = NULL;
        int i;
        Boolean bad = False;

        if (n) {
                copy = (unsigned char *) XtMalloc(n * sizeof(unsigned char));

                for (i = 0; i < n; i++) {
                        if (bad) {
                                copy[i] = 0;
                        } else if (shadow_types[i] == BAD_SHADOW) {
                                copy[i] = 0;
                                bad = True;
                                XtAppWarningMsg(XtWidgetToApplicationContext(w),
                                                "copyShadowTypes", "tooShort", "XbaeMatrix",
                                                "XbaeMatrix: Row or column shadowTypes array is too short",
                                                NULL, 0);
                        } else {
                                copy[i] = shadow_types[i];
                        }
                }
        }

        return copy;
}

void xbaeCopyRowShadowTypes(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.row_shadow_types = copyShadowTypes((Widget) mw, mw->matrix.row_shadow_types, mw->matrix.rows);
        xbaeObjectUnlock((Widget) mw);
}

void xbaeCopyColumnShadowTypes(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.column_shadow_types = copyShadowTypes((Widget) mw, mw->matrix.column_shadow_types, mw->matrix.columns);
        xbaeObjectUnlock((Widget) mw);
}

/**************************************************************************************************/

static short *copySizes(Widget w, short default_size, short *sizes, int n)
{
        short *copy = NULL;
        int i;
        Boolean bad = False;

        if (n) {
                copy = (short *) XtMalloc(n * sizeof(short));
                for (i = 0; i < n; i++) {
                        if (bad) {
                                copy[i] = default_size;
                        } else if (sizes[i] < 0) {
                                copy[i] = default_size;
                                bad = True;
                                XtAppWarningMsg(XtWidgetToApplicationContext(w),
                                                "copySizes", "tooShort", "XbaeMatrix",
                                                "XbaeMatrix: Row height or column width array is too short or contains illigal values",
                                                NULL, 0);
                        } else {
                                copy[i] = sizes[i];
                        }
                }
        }

        return copy;
}

void xbaeCopyColumnWidths(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.column_widths = copySizes((Widget) mw, DEFAULT_COLUMN_WIDTH(mw), mw->matrix.column_widths, mw->matrix.columns);
        xbaeObjectUnlock((Widget) mw);
}

void xbaeCopyRowHeights(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.row_heights = copySizes((Widget) mw, DEFAULT_ROW_HEIGHT(mw), mw->matrix.row_heights, mw->matrix.rows);
        xbaeObjectUnlock((Widget) mw);
}

/**************************************************************************************************/

void xbaeCopyColumnMaxLengths(XbaeMatrixWidget mw)
{
        int *copy = NULL;
        int i;
        Boolean bad = False;

        xbaeObjectLock((Widget) mw);

        if (mw->matrix.columns) {
                copy = (int *) XtMalloc(mw->matrix.columns * sizeof(int));

                for (i = 0; i < mw->matrix.columns; i++) {
                        if (bad) {
                                copy[i] = 0;
                        } else if (mw->matrix.column_max_lengths[i] < 0) {
                                copy[i] = 0;
                                bad = True;
                                XtAppWarningMsg(XtWidgetToApplicationContext((Widget) mw),
                                                "copyColumnMaxLengths", "tooShort", "XbaeMatrix",
                                                "XbaeMatrix: Column max lengths array is too short or contains illigal values",
                                                NULL, 0);
                        } else {
                                copy[i] = mw->matrix.column_max_lengths[i];
                        }
                }
        }
        mw->matrix.column_max_lengths = copy;

        xbaeObjectUnlock((Widget) mw);
}

/**************************************************************************************************/

static String *copyLabels(Widget w, String *labels, int n) 
{
        String *copy = NULL;
        int i;
        Boolean bad = False;
        
        if (n && labels) {
                copy = (String *) XtMalloc(n * sizeof(String));

                for (i = 0; i < n; i++) {
                        if (bad) {
                                copy[i] = NULL;
                        } else if (labels[i] == &xbaeBadString) {
                                copy[i] = NULL;
                                bad = True;
                                XtAppWarningMsg(XtWidgetToApplicationContext(w),
                                                "copyLabels", "tooShort", "XbaeMatrix",
                                                "XbaeMatrix: Row or column labels array is too short",
                                                NULL, 0);
                        } else {
                                copy[i] = (labels[i] == NULL) ? NULL : XtNewString(labels[i]);
                        }
                }
        }
        return copy;
}

static XmString *copyXmlabels(Widget w, XmString *xmlabels, int n)
{
        XmString *xmcopy = NULL;
        int i;

        if (n && xmlabels) {
                xmcopy = (XmString *) XtMalloc(n * sizeof(XmString));
                for (i = 0; i < n; i++) {
                        xmcopy[i] = (xmlabels[i] == NULL) ? NULL : XmStringCopy(xmlabels[i]);
                }
        }
        return xmcopy;
}

void xbaeCopyRowLabels(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.row_labels = copyLabels((Widget) mw, mw->matrix.row_labels, mw->matrix.rows);
        mw->matrix.xmrow_labels = copyXmlabels((Widget) mw, mw->matrix.xmrow_labels, mw->matrix.rows);
        xbaeObjectUnlock((Widget) mw);
}

void xbaeCopyColumnLabels(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        mw->matrix.column_labels = copyLabels((Widget) mw, mw->matrix.column_labels, mw->matrix.columns);
        mw->matrix.xmcolumn_labels = copyXmlabels((Widget) mw, mw->matrix.xmcolumn_labels, mw->matrix.columns);        
        xbaeObjectUnlock((Widget) mw);
}

/**************************************************************************************************/

void xbaeFreeLabels(String *labels, XmString *xmlabels, int n)
{
        int i;

        if (labels) {
                for (i = 0; i < n; i++) {
                        if (labels[i]) {
                                XtFree((char *) labels[i]);
                        }
                }
                XtFree((char *) labels);
        }

        if (xmlabels) {
                for (i = 0; i < n; i++) {
                        if (xmlabels[i]) {
                                XmStringFree(xmlabels[i]);
                        }
                }
                XtFree((char *) xmlabels);
        }
}

void xbaeFreeRowLabels(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        xbaeFreeLabels(mw->matrix.row_labels, mw->matrix.xmrow_labels, mw->matrix.rows);
        mw->matrix.row_labels = NULL;
        mw->matrix.xmrow_labels = NULL;
        xbaeObjectUnlock((Widget) mw);
}

void xbaeFreeColumnLabels(XbaeMatrixWidget mw)
{
        xbaeObjectLock((Widget) mw);
        xbaeFreeLabels(mw->matrix.column_labels, mw->matrix.xmcolumn_labels, mw->matrix.columns);
        mw->matrix.column_labels = NULL;
        mw->matrix.xmcolumn_labels = NULL;
        xbaeObjectUnlock((Widget) mw);
}

/**************************************************************************************************/

/*
 * A cache for pixmaps which remembers the screen a pixmap belongs to
 * (see bug #591306) and that permits clearing by screen when that
 * screen gets closed.
 */
static struct pcache {
        Pixmap pixmap;
        Screen *scr;

} *stipple_cache = NULL;
static int ncache = 0;          /* Allocated size of the array. */
                                /* Empty places have NULL screen */

static Pixmap PixmapFromCache(Screen * scr)
{
        int i;

        for (i = 0; i < ncache; i++)
                if (scr == stipple_cache[i].scr)
                        return stipple_cache[i].pixmap;
        return (Pixmap) 0;
}

static void AddPixmapToCache(Screen * scr, Pixmap p)
{
        int i, old;

        for (i = 0; i < ncache; i++)
                if (stipple_cache[i].scr == 0) {
                        stipple_cache[i].scr = scr;
                        stipple_cache[i].pixmap = p;
                        return;
                }

        /* Allocate more */
        if (ncache) {
                old = ncache;
                ncache *= 2;
                stipple_cache =
                    (struct pcache *) XtRealloc((char *) stipple_cache,
                                                ncache * sizeof(struct pcache));
                for (i = old; i < ncache; i++)
                        stipple_cache[i].scr = NULL;
                stipple_cache[old].scr = scr;
                stipple_cache[old].pixmap = p;
        } else {
                ncache = 16;    /* Some silly initial value */
                stipple_cache = (struct pcache *) XtMalloc(ncache * sizeof(struct pcache));
                for (i = 0; i < ncache; i++)
                        stipple_cache[i].scr = NULL;
                stipple_cache[0].scr = scr;
                stipple_cache[0].pixmap = p;
        }
}

/*
 * Remove the pixmaps with this screen from the cache
 */
static void RemovePixmapsFromScreen(Screen * scr)
{
        int i;
        for (i = 0; i < ncache; i++)
                if (stipple_cache[i].scr == scr) {
                        XFreePixmap(DisplayOfScreen(stipple_cache[i].scr), stipple_cache[i].pixmap);
                        stipple_cache[i].pixmap = (Pixmap) 0;
                        stipple_cache[i].scr = NULL;
                }
}

/*
 * Create a pixmap to be used for drawing the matrix contents when
 * XmNsensitive is set to False
 */
static Pixmap createInsensitivePixmap(XbaeMatrixWidget mw)
{
        static char stippleBits[] = { 0x01, 0x02 };
        Display *dpy = XtDisplay(mw);
        Screen *scr = XtScreen(mw);
        Pixmap p;

        xbaeObjectLock((Widget) mw);

        p = PixmapFromCache(XtScreen((Widget) mw));
        if (p) {
                xbaeObjectUnlock((Widget) mw);
                return p;
        }

        p = XCreatePixmapFromBitmapData(dpy, RootWindowOfScreen(scr), stippleBits, 2, 2, 0, 1, 1);
        AddPixmapToCache(scr, p);
        xbaeObjectUnlock((Widget) mw);
        return p;
}

/*
 * Make sure to know when our display connection dies.
 */
static void DisplayDied(Widget w, XtPointer client, XtPointer call)
{
        XtDestroyHookDataRec *p = (XtDestroyHookDataRec *) call;

        if (p == NULL || p->type != XtHdestroy)
                return;

        if (XtIsSubclass(p->widget, xmPrimitiveWidgetClass))
                RemovePixmapsFromScreen(XtScreen(p->widget));
}

void xbaeRegisterDisplay(Widget w)
{
        Display *d = XtDisplay(w);
        XtAddCallback(XtHooksOfDisplay(d), XtNdestroyHook, DisplayDied, NULL);
}

/**************************************************************************************************/

void xbaeCreateDrawGC(XbaeMatrixWidget mw)
{
        XGCValues values;
        unsigned long mask = GCStipple | GCFillStyle;

        xbaeObjectLock((Widget) mw);

        /*
         * GC for drawing cells. We create it instead of using a cached one,
         * since the foreground may change frequently.
         */
        values.stipple = createInsensitivePixmap(mw);
        values.fill_style = (XtIsSensitive((Widget) mw)) ? FillSolid : FillStippled;

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget) mw,
                            "xbaeCreateDrawGC(dpy %p win %p fg %d stip %p)\n",
                            XtDisplay(mw), GC_PARENT_WINDOW(mw),
                            values.foreground, values.stipple));

        mw->matrix.draw_gc = XCreateGC(XtDisplay(mw), GC_PARENT_WINDOW(mw), mask, &values);

        xbaeObjectUnlock((Widget) mw);
}

void xbaeCreatePixmapGC(XbaeMatrixWidget mw)
{
        XGCValues values;
        unsigned long mask = GCStipple | GCFillStyle | GCGraphicsExposures;

        xbaeObjectLock((Widget) mw);

        values.stipple = createInsensitivePixmap(mw);
        values.fill_style = (XtIsSensitive((Widget) mw)) ? FillSolid : FillStippled;
        values.graphics_exposures = False;

        mw->matrix.pixmap_gc = XCreateGC(XtDisplay(mw), GC_PARENT_WINDOW(mw), mask, &values);

        xbaeObjectUnlock((Widget) mw);
}

void xbaeCreateLabelGC(XbaeMatrixWidget mw)
{
        XGCValues values;
        unsigned long mask = GCStipple | GCFillStyle;

        xbaeObjectLock((Widget) mw);

        /*
         * GC for drawing labels
         */
        values.stipple = createInsensitivePixmap(mw);
        values.fill_style = (XtIsSensitive((Widget) mw)) ? FillSolid : FillStippled;
        
        /* Font id isn't used for fontsets */
        if (mw->matrix.label_font.type == XmFONT_IS_FONT) {
                mask |= GCFont;
                values.font = mw->matrix.label_font.id;
        }
        mw->matrix.label_gc = XCreateGC(XtDisplay(mw), GC_PARENT_WINDOW(mw), mask, &values);

        xbaeObjectUnlock((Widget) mw);
}

void xbaeGetGridLineGC(XbaeMatrixWidget mw)
{
        XGCValues values;
        XtGCMask mask = GCForeground | GCBackground;

        xbaeObjectLock((Widget) mw);

        /*
         * GC for drawing grid lines
         */
        values.foreground = mw->matrix.grid_line_color;
        values.background = mw->manager.foreground;

        /* Release the GC before getting another one */
        if (mw->matrix.grid_line_gc)
                XtReleaseGC((Widget) mw, mw->matrix.grid_line_gc);

        mw->matrix.grid_line_gc = XtGetGC((Widget) mw, mask, &values);

        xbaeObjectUnlock((Widget) mw);
}

void xbaeGetResizeTopShadowGC(XbaeMatrixWidget mw)
{
        XGCValues values;
        XtGCMask mask = GCForeground | GCBackground | GCFunction;

        xbaeObjectLock((Widget) mw);

        /*
         * GC for drawing the top shadow when resizing
         */
        values.foreground = mw->manager.top_shadow_color;
        values.background = mw->manager.foreground;
        values.function = GXxor;

        if (mw->manager.top_shadow_pixmap != XmUNSPECIFIED_PIXMAP) {
                mask |= GCFillStyle | GCTile;
                values.fill_style = FillTiled;
                values.tile = mw->manager.top_shadow_pixmap;
        }

        /* Release the GC before getting another one */
        if (mw->matrix.resize_top_shadow_gc)
                XtReleaseGC((Widget) mw, mw->matrix.resize_top_shadow_gc);

        mw->matrix.resize_top_shadow_gc = XtGetGC((Widget) mw, mask, &values);

        xbaeObjectUnlock((Widget) mw);
}

void xbaeGetResizeBottomShadowGC(XbaeMatrixWidget mw)
{
        XGCValues values;
        XtGCMask mask = GCForeground | GCBackground | GCFunction;

        xbaeObjectLock((Widget) mw);

        /*
         * GC for drawing the bottom shadow when resizing
         */
        values.foreground = mw->manager.bottom_shadow_color;
        values.background = mw->manager.foreground;
        values.function = GXxor;

        if (mw->manager.bottom_shadow_pixmap != XmUNSPECIFIED_PIXMAP) {
                mask |= GCFillStyle | GCTile;
                values.fill_style = FillTiled;
                values.tile = mw->manager.bottom_shadow_pixmap;
        }

        /* Release the GC before getting another one */
        if (mw->matrix.resize_bottom_shadow_gc)
                XtReleaseGC((Widget) mw, mw->matrix.resize_bottom_shadow_gc);

        mw->matrix.resize_bottom_shadow_gc = XtGetGC((Widget) mw, mask, &values);

        xbaeObjectUnlock((Widget) mw);
}

/**************************************************************************************************/

static short xbaeGetFontStructWidth(XFontStruct *font_struct)
{
        short width;
        unsigned long fp;
        unsigned char char_idx;

        /*
         *  From the XmText man page: If the em-space value is
         *  available, it is used. If not, the width of the  numeral  "0"
         *  is used. If this is not available, the maximum width is used.
         */
        if (XGetFontProperty(font_struct, XA_QUAD_WIDTH, &fp) && fp != 0) {
                width = (short) fp;
        } else {
                if (font_struct->min_char_or_byte2 <= '0' &&
                    font_struct->max_char_or_byte2 >= '0' && font_struct->per_char) {
                        char_idx = '0' - font_struct->min_char_or_byte2;
                        width = font_struct->per_char[char_idx].width;
                } else {
                        width = font_struct->max_bounds.width;
                }
        }

        /* last safety check */
        if (width <= 0) {
                width = 1;
        }

        return width;
}

static short xbaeGetFontSetWidth(XFontSet font_set)
{
        XFontStruct **font_struct_list;
        char **font_name_list;
        int n, i, max_width;
        
        n = XFontsOfFontSet(font_set, &font_struct_list, &font_name_list);

        if (n <= 0) {
                max_width = 1;
        } else {
                max_width = xbaeGetFontStructWidth(font_struct_list[0]);
                for (i = 1; i < n; i++) {
                        int width = xbaeGetFontStructWidth(font_struct_list[i]);
                        if (width > max_width) {
                                max_width = width;
                        }
                }
        }
        
        return max_width;
}

static void xbaeInitFontInfo(XtPointer fontp, XmFontType type, XbaeMatrixFontInfo *font)
{
        font->type = type;

        if (type == XmFONT_IS_FONTSET) {
                XFontSet font_set = (XFontSet) fontp;
                XFontSetExtents *extents;

                font->fontp.font_set = font_set;
                
                extents = XExtentsOfFontSet(font_set);

                font->width = xbaeGetFontSetWidth(font_set);
                font->height = extents->max_logical_extent.height;
                font->y = extents->max_logical_extent.y;
                font->id = 0;     /* not used for fontsets */

                DEBUGOUT(_XbaeDebug(__FILE__, NULL,
					"font is fontset of width %d\n", font->width));
        } else {
                XFontStruct *font_struct = (XFontStruct *) fontp;
        
                font->fontp.font_struct = font_struct;

                font->width = xbaeGetFontStructWidth(font_struct);
                font->height = font_struct->max_bounds.descent + font_struct->max_bounds.ascent;
                font->y = - font_struct->max_bounds.ascent;
                font->id = font_struct->fid;
                
                DEBUGOUT(_XbaeDebug(__FILE__, NULL,
					"font is font of width: %d\n", font->width));
        }
}

XmFontListEntry xbaeFontListGetEntry(XbaeMatrixWidget mw, XmFontList font_list, XmStringTag tag)
{
        XmFontContext context;
        XmFontListEntry font_list_entry = NULL;

        XmStringTag tags[] = {NULL, _MOTIF_DEFAULT_LOCALE, XmFONTLIST_DEFAULT_TAG};
        int n_tags = sizeof tags/sizeof *tags;
        int i;

        xbaeObjectLock((Widget) mw);

        tags[0] = tag;

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw, "xbaeFontListGetEntry\n"));

        /*
         * Find the first tag that exists in the font_list
         */
        for(i = (tag == NULL); i < n_tags && font_list_entry == NULL; i++) {
                /*
                 * Get a font context so we can loop over the XmFontListEntries
                 */
                if (!XmFontListInitFontContext(&context, font_list)) {
                        XtAppErrorMsg(XtWidgetToApplicationContext((Widget) mw),
                                      "xbaeFontListGetEntry", "badFont", "XbaeMatrix",
                                      "XbaeMatrix: XmFontListInitFontContext failed, bad fontList",
                                      NULL, 0);
                }

                /*
                 * Search the font list for an entry with a matching tag
                 */
                for(  font_list_entry = XmFontListNextEntry(context) 
                    ; font_list_entry != NULL
                    ; font_list_entry = XmFontListNextEntry(context)) {
                        XmStringTag entry_tag;

                        entry_tag = XmFontListEntryGetTag(font_list_entry);

                        if (strcmp(entry_tag, tags[i]) == 0) {
                                XtFree((XtPointer) entry_tag);
                                break;
                        }
                        XtFree((XtPointer) entry_tag);
                }

                XmFontListFreeFontContext(context);
        }
        
        if (font_list_entry == NULL) {
                /*
                 * No tag matches, fall back to the first font
                 */
                XtAppWarningMsg(XtWidgetToApplicationContext((Widget) mw),
                              "xbaeFontListGetEntry", "badTag", "XbaeMatrix",
                              "XbaeMatrix: Couldn't find tag in fontlist",
                              NULL, 0);

                if (!XmFontListInitFontContext(&context, font_list)) {
                        XtAppErrorMsg(XtWidgetToApplicationContext((Widget) mw),
                                      "xbaeFontListGetEntry", "badFont", "XbaeMatrix",
                                      "XbaeMatrix: XmFontListInitFontContext failed, bad fontList",
                                      NULL, 0);
                }

                font_list_entry = XmFontListNextEntry(context);
                XmFontListFreeFontContext(context);
        }

        xbaeObjectUnlock((Widget) mw);
        
        return font_list_entry;
}

void xbaeInitFontFromFontList(XbaeMatrixWidget mw, XmFontList font_list, XmStringTag tag, XbaeMatrixFontInfo *font)
{
        XmFontListEntry font_list_entry;
        XtPointer fontp = NULL;
        XmFontType type;

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget) mw, "InitFontFromFontlist\n"));

        font_list_entry = xbaeFontListGetEntry(mw, font_list, tag);

        if (font_list_entry == NULL) {
                XtAppErrorMsg(XtWidgetToApplicationContext((Widget) mw),
                              "xbaeInitFontFromFontList", "badFont", "XbaeMatrix",
                              "XbaeMatrix: No fontListEntry found", NULL, 0);
        }
        
        fontp = XmFontListEntryGetFont(font_list_entry, &type);
        
        if (fontp == NULL) {
                XtAppErrorMsg(XtWidgetToApplicationContext((Widget) mw),
                              "xbaeInitFontFromFontList", "badFont", "XbaeMatrix",
                              "XbaeMatrix: XmFontListEntryGetFont failed, bad fontListEntry", NULL, 0);
        }
        
        xbaeInitFontInfo(fontp, type, font);
}

#if XmVERSION >= 2
XmRendition xbaeRenderTableGetRendition(XbaeMatrixWidget mw, XmRenderTable render_table, XmStringTag tag) {

        XmRendition rendition = NULL;

        XmStringTag tags[] = {NULL, _MOTIF_DEFAULT_LOCALE, XmFONTLIST_DEFAULT_TAG};
        int n_tags = sizeof tags/sizeof *tags;
        int i;

        xbaeObjectLock((Widget) mw);

        tags[0] = tag;

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw, "xbaeRenderTableGetRendition\n"));

        /*
         * Find the first tag that exists in the render_table
         */
        for (i = (tag == NULL); i < n_tags && rendition == NULL; i++) {
                rendition = XmRenderTableGetRendition(render_table, tags[i]);
        }

        if (rendition == NULL) {
                /*
                 * No tag matches, fall back to the first rendition
                 */
                int i, n;
                XmStringTag *existing_tags;
                n = XmRenderTableGetTags(render_table, &existing_tags);

                XtAppWarningMsg(XtWidgetToApplicationContext((Widget) mw),
                              "xbaeRenderTableGetRendition", "badTag", "XbaeMatrix",
                              "XbaeMatrix: Couldn't find tag in renderTable",
                              NULL, 0);

                if (n) {
                        rendition = XmRenderTableGetRendition(render_table, existing_tags[0]);
                }
                
                for(i = 0; i < n; i++) {
                    XtFree(existing_tags[i]);
                }
                XtFree((XtPointer) existing_tags);
        }
        
        return rendition;
}

void xbaeInitFontFromRenderTable(XbaeMatrixWidget mw, XmRenderTable render_table, XmStringTag tag, XbaeMatrixFontInfo *font)
{
        XmRendition rendition = NULL;
        String fontname;
        XmFontType type;
        XtPointer fontp;
        Arg args[10];
        int m = 0;

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget) mw, "xbaeInitFontFromRenderTable(%p)\n", render_table));

        /*
         * Find the first tag that exists in the render_table
         */
        rendition = xbaeRenderTableGetRendition(mw, render_table, tag);
        
        if (rendition == NULL) {
                XtAppErrorMsg(XtWidgetToApplicationContext((Widget) mw),
                              "xbaeInitFontFromRenderTable", "badFont", "XbaeMatrix",
                              "XbaeMatrix: No rendition found", NULL, 0);
        }

        XtSetArg(args[m], XmNfontName, &fontname); m++;
        XtSetArg(args[m], XmNfont, &fontp); m++;
        XtSetArg(args[m], XmNfontType, &type); m++;

        XmRenditionRetrieve(rendition, args, m);

        if (fontp == NULL || fontp == (XtPointer) XmAS_IS) {
                XtAppErrorMsg(XtWidgetToApplicationContext((Widget) mw),
                              "xbaeFontFromRenderTable", "badFont", "XbaeMatrix",
                              "XbaeMatrix: The specified tag has no font loaded", NULL, 0);
        }

        xbaeInitFontInfo(fontp, type, font);

        XmRenditionFree(rendition);
}
#endif

void xbaeInitFonts(XbaeMatrixWidget mw) {
        #if XmVERSION >= 2
        if (mw->matrix.render_table) {
                xbaeInitFontFromRenderTable(mw, mw->matrix.render_table, NULL, &mw->matrix.cell_font);
        } else 
        #endif    
        {
                xbaeInitFontFromFontList(mw, mw->matrix.font_list, NULL, &mw->matrix.cell_font);
        }

        #if XmVERSION >= 2
        if (mw->matrix.render_table) {
                xbaeInitFontFromRenderTable(mw, mw->matrix.render_table, "labels", &mw->matrix.label_font);
        } else 
        #endif
        {
                if (mw->matrix.label_font_list) {
                        xbaeInitFontFromFontList(mw, mw->matrix.label_font_list, NULL, &mw->matrix.label_font);
                } else if (mw->matrix.font_list) {
                        xbaeInitFontFromFontList(mw, mw->matrix.font_list, "labels", &mw->matrix.label_font);
                }
        }
}

/**************************************************************************************************/

/*
 * ARCAD SYSTEMHAUS
 */
void xbaeFill_WithEmptyValues_PerCell(XbaeMatrixWidget mw, struct _XbaeMatrixPerCellRec *p)
{
	p->shadow_type = 0; /* 0 means to use matrix.cell_shadow_type */
	p->highlighted = HighlightNone;
	p->selected = False;
	p->underlined = False;
	p->user_data = NULL;
	p->background = XmUNSPECIFIED_PIXEL;
	p->color = XmUNSPECIFIED_PIXEL;
	p->widget = NULL;
	p->pixmap = XmUNSPECIFIED_PIXMAP;
	p->mask = XmUNSPECIFIED_PIXMAP;
	p->CellValue = NULL;
	p->qtag = NULLQUARK;
}

void xbaeCreatePerCell(XbaeMatrixWidget mw)
{
        /*
         * Create with empty values
         */
        struct _XbaeMatrixPerCellRec **copy = NULL;
        int i, j;

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw, "xbaeCreatePerCell(%d, %d)\n",
                 mw->matrix.rows, mw->matrix.columns));
        xbaeObjectLock((Widget) mw);

        /*
         * Malloc an array of row pointers
         */
        if (mw->matrix.rows && mw->matrix.columns) {
                copy = (struct _XbaeMatrixPerCellRec **) XtMalloc(mw->matrix.rows * sizeof *copy);
                for (i = 0; i < mw->matrix.rows; i++) {
                        copy[i] = (struct _XbaeMatrixPerCellRec *) XtMalloc(mw->matrix.columns *
                                                                      sizeof *copy[i]);
                        for (j = 0; j < mw->matrix.columns; j++)
                                xbaeFill_WithEmptyValues_PerCell(mw, &copy[i][j]);
                }
        }
        mw->matrix.per_cell = copy;
        xbaeObjectUnlock((Widget) mw);
}

void xbaeFreePerCellEntity(XbaeMatrixWidget mw, int row, int column) {
        /* Unmanage, unmap any cell widgets that might be visible */
        if (mw->matrix.per_cell[row][column].widget) {
                XtUnmanageChild(mw->matrix.per_cell[row][column].widget);
        }
        /* Free the cellValue string */
        if (mw->matrix.per_cell[row][column].CellValue) {
                XtFree(mw->matrix.per_cell[row][column].CellValue);
                mw->matrix.per_cell[row][column].CellValue = NULL;
        }
        /* Update the number of cells that are selected */
        if (mw->matrix.per_cell[row][column].selected) {
                mw->matrix.num_selected_cells--;
        }
}

void xbaeFreePerCell(XbaeMatrixWidget mw)
{
        int row;
        int column;

        /*
         * Free each row of XtPointer pointers
         */
        if (mw->matrix.per_cell) {

                xbaeObjectLock((Widget) mw);

                for (row = 0; row < mw->matrix.rows; row++) {
                        for (column = 0; column < mw->matrix.columns; column++) {
                                xbaeFreePerCellEntity(mw, row, column);
                        }
                        XtFree((XtPointer) mw->matrix.per_cell[row]);
                }

                XtFree((XtPointer) mw->matrix.per_cell);
                mw->matrix.per_cell = NULL;

                xbaeObjectUnlock((Widget) mw);
        }
}
