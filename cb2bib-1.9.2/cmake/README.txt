#-------------------------------------------------------------------------------
#  cb2bib cmake
#-------------------------------------------------------------------------------

To CONFIGURE cb2bib build, from the base directory, type
cd cmake/build
cmake ../..


To find out and eventually modify the build options, type
cmake -i ../..


To COMPILE cb2bib, type
make


To INSTALL cb2bib, type
make install/strip


For make options, type
make help


#-------------------------------------------------------------------------------

EXAMPLE: Configure, compile, and install to a non-default location
cd cmake/build
cmake ../.. -DCMAKE_INSTALL_PREFIX=/usr/local
make
make install/strip

#-------------------------------------------------------------------------------