<!doctype debiandoc system>

<!--
 Debian GAP Policy
 Copyright (C) 2004-2014 Bill Allombert.
 released under the terms of the GNU
 General Public License, version 2 or (at your option) any later.
 -->

<book>

<title>Debian GAP Policy
<author>Bill Allombert <email/ballombe@debian.org/
<version>version 0.1, <date>

<abstract>

This unofficial policy describes the conventions that Debian packages of GAP
(<httpsite>http://www.gap-system.org</>) components must follows to get along
nicely with the gap-core package and others.
</abstract>

<toc sect>

<chapt> Convention used in this policy
<p>
Since GAP and Debian have a different concept of package, we will
use the following conventions:
<enumlist>
<item><var/$pkg/ denote the name of the GAP package as stated on the
GAP website.
<item><var/$package/ denote the name of your Debian package.
</enumlist>

<chapt> Package name
<p>
Name of GAP packages (<var/$pkg/) are usually short and cryptic, like
<tt/ctbllib/.  You should choose a more verbose name (<var/$package/) like
gap-character-tables.
<enumlist>
<item> <var/$package/ must start by <tt/gap-/.
<item> Package must provide <tt/gap-pkg-$pkg/ so GAP users can do
<prgn/apt-get gap-pkg-$pkg/ without having to guess <var/$package/.
</enumlist>

<chapt> Installation
<p>
GAP packages should be installed in the <file>/usr/share/gap/pkg/$pkg</file>
directory.  Architecture dependent part of the package should go to
<file>/usr/lib/gap/pkg/$pkg</file>. You can provide symlinks if this is
necessary.
<p> If your package include large (say around 1MB) datasets (library of
groups, etc...), you should compress them (after checking GAP is able to
automatically uncompress them) with <prgn/gzip --best/ to save diskspace. Try
to not compress source code files (<tt/.g/ files) if possible.

<chapt> Global GAP workspace
<p> To allow GAP to start faster, Debian provides a global workspace file
in <file>/var/lib/gap/workspace.gz</file>. This file is managed by
<prgn/update-gap-workspace/. If this file exists, it will be automatically
updated whenever a Debian GAP package is installed or upgraded, through
a gap-core dpkg trigger.

<chapt> GAP2DEB
<p>
The GAP script <tt/gap2deb/ can be used to generate a basic <file>debian</file>
directory from a <file>PackageInfo.g</file> file. It must be run in the
package directory.

<chapt> Documentation
<p>
GAP packages come usually with extensive documentation.
<enumlist>
<item> TeX files used by the online help system must be shipped.
<item> Documentation must be shipped preferably in <tt/html/ and <tt/pdf/.
<item> Ship the file <file/manual.toc/ compressed. This enables
<tt/SetHelpviewer()/ to function properly.
<item> If you need to use the script <file>etc/convert.pl</file>, set the
environment variable <tt>GAP_CONVERT_DATE</tt> to the string appearing as
the date in the original manual
(e.g. <tt>GAP_CONVERT_DATE="Februar 2010"</tt>,
<tt>GAP_CONVERT_DATE</tt> is not interpolated).
</enumlist>

</book>
