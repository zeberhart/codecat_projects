/*
     This file is part of GNUnet.
     (C) 2010, 2012 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/setup/gnunet-setup-transport.h
 * @brief support for transport (NAT) configuration
 * @author Christian Grothoff
 */
#ifndef GNUNET_SETUP_TRANSPORT_H
#define GNUNET_SETUP_TRANSPORT_H
#include "gnunet-setup.h"
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_resolver_service.h>
#include <gnunet/gnunet_nat_lib.h>

/**
 * Handle for the autoconfig operation.
 */
struct GNUNET_SetupAutoContext;

/**
 * Function called upon completion of the operation.
 *
 * @param cls closure
 */
typedef void (*GNUNET_SetupAutoConfigFinished)(void *cls);


/**
 * User asked for autoconfiguration.  Try the full program.
 *
 * @param fin_cb function to call when done
 * @param fin_cb_cls closure for 'fin_cb'
 * @return handle for the operation
 */
struct GNUNET_SetupAutoContext *
GNUNET_setup_transport_autoconfig_start (GNUNET_SetupAutoConfigFinished fin_cb,
					 void *fin_cb_cls);


#endif
