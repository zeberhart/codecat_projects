/* common.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#ifndef COMMON__H
#define COMMON__H

// ----------------------------------------------------------------------------
#define ICON_WIDTH     48
#define ICON_HEIGHT    48
#define DBLCLICK      500
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

//#include <pthread.h> // later...
#include <gtk/gtk.h>
#include <stdlib.h>     // malloc, free, ...

#include "log.h"
#include "sysstuff.h"
#include "gtkstuff.h"
#include "splash.h"

#include "app.h"
#include "editor.h"
#include "property.h"
#include "iconlist.h"
#include "iconsel.h"
#include "filesel.h"
#include "apwalpref.h"
#include "apwalapp.h"
#include "xmlrc.h"
#include "about.h"

// ----------------------------------------------------------------------------
#endif /*COMMON__H*/
