#pike 7.3

//
// $Id: 72ef7ff071834f8492236a61c562af08c90c52c6 $
//
// Pike 7.2 backward compatibility layer.
//

inherit Thread;

#if constant(thread_create)

class Condition
{
  inherit Thread::Condition;

  // Old-style wait().
  void wait(void|Mutex m)
  {
    if (!m) {
      m = Mutex();
      m->lock();
    }
    ::wait(m);
  }
}

#endif /* !constant(thread_create) */
