/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TOK_ARROW = 258,
     TOK_CONSTANT = 259,
     TOK_FLOAT = 260,
     TOK_STRING = 261,
     TOK_NUMBER = 262,
     TOK_INC = 263,
     TOK_DEC = 264,
     TOK_RETURN = 265,
     TOK_EQ = 266,
     TOK_GE = 267,
     TOK_LE = 268,
     TOK_NE = 269,
     TOK_NOT = 270,
     TOK_LSH = 271,
     TOK_RSH = 272,
     TOK_LAND = 273,
     TOK_LOR = 274,
     TOK_SWITCH = 275,
     TOK_SSCANF = 276,
     TOK_CATCH = 277,
     TOK_FOREACH = 278,
     TOK_LEX_EOF = 279,
     TOK_ADD_EQ = 280,
     TOK_AND_EQ = 281,
     TOK_ARRAY_ID = 282,
     TOK_ATTRIBUTE_ID = 283,
     TOK_BREAK = 284,
     TOK_CASE = 285,
     TOK_CLASS = 286,
     TOK_COLON_COLON = 287,
     TOK_CONTINUE = 288,
     TOK_DEFAULT = 289,
     TOK_DEPRECATED_ID = 290,
     TOK_DIV_EQ = 291,
     TOK_DO = 292,
     TOK_DOT_DOT = 293,
     TOK_DOT_DOT_DOT = 294,
     TOK_ELSE = 295,
     TOK_ENUM = 296,
     TOK_EXTERN = 297,
     TOK_FLOAT_ID = 298,
     TOK_FOR = 299,
     TOK_FUNCTION_ID = 300,
     TOK_GAUGE = 301,
     TOK_GLOBAL = 302,
     TOK_IDENTIFIER = 303,
     TOK_RESERVED = 304,
     TOK_IF = 305,
     TOK_IMPORT = 306,
     TOK_INHERIT = 307,
     TOK_FACET = 308,
     TOK_INLINE = 309,
     TOK_LOCAL_ID = 310,
     TOK_FINAL_ID = 311,
     TOK_FUNCTION_NAME = 312,
     TOK_INT_ID = 313,
     TOK_LAMBDA = 314,
     TOK_MULTISET_ID = 315,
     TOK_MULTISET_END = 316,
     TOK_MULTISET_START = 317,
     TOK_LSH_EQ = 318,
     TOK_MAPPING_ID = 319,
     TOK_MIXED_ID = 320,
     TOK_MOD_EQ = 321,
     TOK_MULT_EQ = 322,
     TOK_NO_MASK = 323,
     TOK_OBJECT_ID = 324,
     TOK_OR_EQ = 325,
     TOK_PRIVATE = 326,
     TOK_PROGRAM_ID = 327,
     TOK_PROTECTED = 328,
     TOK_PREDEF = 329,
     TOK_PUBLIC = 330,
     TOK_RSH_EQ = 331,
     TOK_STATIC = 332,
     TOK_STRING_ID = 333,
     TOK_SUB_EQ = 334,
     TOK_TYPEDEF = 335,
     TOK_TYPEOF = 336,
     TOK_VARIANT = 337,
     TOK_VERSION = 338,
     TOK_VOID_ID = 339,
     TOK_WHILE = 340,
     TOK_XOR_EQ = 341,
     TOK_OPTIONAL = 342
   };
#endif
/* Tokens.  */
#define TOK_ARROW 258
#define TOK_CONSTANT 259
#define TOK_FLOAT 260
#define TOK_STRING 261
#define TOK_NUMBER 262
#define TOK_INC 263
#define TOK_DEC 264
#define TOK_RETURN 265
#define TOK_EQ 266
#define TOK_GE 267
#define TOK_LE 268
#define TOK_NE 269
#define TOK_NOT 270
#define TOK_LSH 271
#define TOK_RSH 272
#define TOK_LAND 273
#define TOK_LOR 274
#define TOK_SWITCH 275
#define TOK_SSCANF 276
#define TOK_CATCH 277
#define TOK_FOREACH 278
#define TOK_LEX_EOF 279
#define TOK_ADD_EQ 280
#define TOK_AND_EQ 281
#define TOK_ARRAY_ID 282
#define TOK_ATTRIBUTE_ID 283
#define TOK_BREAK 284
#define TOK_CASE 285
#define TOK_CLASS 286
#define TOK_COLON_COLON 287
#define TOK_CONTINUE 288
#define TOK_DEFAULT 289
#define TOK_DEPRECATED_ID 290
#define TOK_DIV_EQ 291
#define TOK_DO 292
#define TOK_DOT_DOT 293
#define TOK_DOT_DOT_DOT 294
#define TOK_ELSE 295
#define TOK_ENUM 296
#define TOK_EXTERN 297
#define TOK_FLOAT_ID 298
#define TOK_FOR 299
#define TOK_FUNCTION_ID 300
#define TOK_GAUGE 301
#define TOK_GLOBAL 302
#define TOK_IDENTIFIER 303
#define TOK_RESERVED 304
#define TOK_IF 305
#define TOK_IMPORT 306
#define TOK_INHERIT 307
#define TOK_FACET 308
#define TOK_INLINE 309
#define TOK_LOCAL_ID 310
#define TOK_FINAL_ID 311
#define TOK_FUNCTION_NAME 312
#define TOK_INT_ID 313
#define TOK_LAMBDA 314
#define TOK_MULTISET_ID 315
#define TOK_MULTISET_END 316
#define TOK_MULTISET_START 317
#define TOK_LSH_EQ 318
#define TOK_MAPPING_ID 319
#define TOK_MIXED_ID 320
#define TOK_MOD_EQ 321
#define TOK_MULT_EQ 322
#define TOK_NO_MASK 323
#define TOK_OBJECT_ID 324
#define TOK_OR_EQ 325
#define TOK_PRIVATE 326
#define TOK_PROGRAM_ID 327
#define TOK_PROTECTED 328
#define TOK_PREDEF 329
#define TOK_PUBLIC 330
#define TOK_RSH_EQ 331
#define TOK_STATIC 332
#define TOK_STRING_ID 333
#define TOK_SUB_EQ 334
#define TOK_TYPEDEF 335
#define TOK_TYPEOF 336
#define TOK_VARIANT 337
#define TOK_VERSION 338
#define TOK_VOID_ID 339
#define TOK_WHILE 340
#define TOK_XOR_EQ 341
#define TOK_OPTIONAL 342




/* Copy the first part of user declarations.  */
#line 118 "language.yacc"

/* This is the grammar definition of Pike. */

#include "global.h"
#ifdef HAVE_MEMORY_H
#include <memory.h>
#endif

#include "port.h"
#include "interpret.h"
#include "array.h"
#include "object.h"
#include "mapping.h"
#include "stralloc.h"
#include "las.h"
#include "interpret.h"
#include "program.h"
#include "pike_types.h"
#include "constants.h"
#include "pike_macros.h"
#include "pike_error.h"
#include "docode.h"
#include "pike_embed.h"
#include "opcodes.h"
#include "operators.h"
#include "bignum.h"

#define YYMAXDEPTH	1000

#ifdef PIKE_DEBUG
#ifndef YYDEBUG
/* May also be defined by machine.h */
#define YYDEBUG 1
#endif /* YYDEBUG */
#endif

/* Get verbose parse error reporting. */
#define YYERROR_VERBOSE	1

/* #define LAMBDA_DEBUG	1 */

static void yyerror_reserved(const char *keyword);
static struct pike_string *get_new_name(struct pike_string *prefix);
int add_local_name(struct pike_string *, struct pike_type *, node *);
int low_add_local_name(struct compiler_frame *,
		       struct pike_string *, struct pike_type *, node *);
static void mark_lvalues_as_used(node *n);
static node *lexical_islocal(struct pike_string *);
static node *safe_inc_enum(node *n);
static int call_handle_import(struct pike_string *s);

static int inherit_depth;
static struct program_state *inherit_state = NULL;

/*
 * Kludge for Bison not using prototypes.
 */
#ifndef __GNUC__
#ifndef __cplusplus
static void __yy_memcpy(char *to, const char *from,
			unsigned int count);
#endif /* !__cplusplus */
#endif /* !__GNUC__ */



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 185 "language.yacc"
{
  int number;
  FLOAT_TYPE fnum;
  struct node_s *n;
  char *str;
  void *ptr;
}
/* Line 193 of yacc.c.  */
#line 344 "y.tab.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */
#line 193 "language.yacc"

/* Need to be included after YYSTYPE is defined. */
#define INCLUDED_FROM_LANGUAGE_YACC
#include "lex.h"
#include "pike_compiler.h"
#line 200 "language.yacc"

/* Include <stdio.h> our selves, so that we can do our magic
 * without being disturbed... */
#include <stdio.h>
int yylex(YYSTYPE *yylval);
/* Bison is stupid, and tries to optimize for space... */
#ifdef YYBISON
#define short int
#endif /* YYBISON */



/* Line 216 of yacc.c.  */
#line 374 "y.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef /* short */ int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned /* short */ int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef /* short */ int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   8263

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  112
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  201
/* YYNRULES -- Number of rules.  */
#define YYNRULES  656
/* YYNRULES -- Number of states.  */
#define YYNSTATES  974

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   342

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,    98,    92,     2,
     105,   106,    97,    95,   104,    96,   111,    99,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   102,   101,
      94,    88,    93,    89,   110,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   107,     2,   108,    91,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   109,    90,   103,   100,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     8,    11,    14,    15,    17,    21,
      24,    27,    30,    31,    33,    35,    37,    43,    44,    47,
      53,    59,    65,    71,    76,    81,    86,    90,    94,    98,
     102,   106,   110,   114,   118,   120,   124,   129,   134,   139,
     144,   146,   148,   150,   152,   154,   156,   158,   159,   161,
     162,   164,   166,   168,   170,   171,   172,   173,   175,   176,
     177,   191,   200,   201,   213,   220,   222,   224,   226,   228,
     231,   234,   236,   239,   242,   245,   246,   252,   254,   256,
     257,   259,   261,   262,   266,   270,   272,   275,   277,   281,
     285,   287,   289,   291,   293,   295,   297,   299,   301,   303,
     305,   307,   309,   311,   313,   315,   317,   319,   321,   323,
     325,   327,   329,   331,   333,   335,   337,   339,   341,   343,
     345,   347,   349,   351,   353,   355,   357,   359,   361,   363,
     365,   367,   369,   371,   373,   375,   377,   379,   381,   383,
     385,   387,   389,   391,   393,   395,   397,   399,   401,   403,
     405,   407,   409,   411,   413,   415,   417,   419,   421,   423,
     424,   427,   433,   437,   439,   440,   443,   446,   447,   451,
     455,   457,   460,   462,   464,   467,   469,   472,   474,   476,
     478,   480,   484,   486,   490,   492,   494,   496,   498,   500,
     502,   505,   508,   511,   514,   517,   520,   523,   526,   533,
     539,   542,   547,   552,   554,   555,   557,   560,   561,   563,
     566,   568,   570,   571,   577,   581,   583,   584,   585,   590,
     591,   592,   601,   602,   604,   607,   609,   610,   615,   619,
     620,   621,   622,   623,   632,   633,   635,   639,   642,   645,
     646,   652,   657,   662,   667,   670,   673,   678,   683,   688,
     693,   695,   697,   701,   705,   706,   707,   708,   715,   717,
     719,   721,   723,   725,   726,   728,   733,   735,   740,   744,
     748,   752,   754,   758,   762,   766,   770,   774,   775,   778,
     781,   783,   785,   787,   789,   791,   793,   796,   799,   802,
     805,   808,   810,   812,   814,   816,   818,   820,   822,   824,
     826,   828,   829,   834,   836,   837,   840,   843,   845,   848,
     849,   850,   851,   852,   861,   867,   868,   874,   878,   879,
     886,   891,   897,   902,   904,   908,   912,   914,   917,   918,
     922,   926,   928,   930,   931,   932,   940,   942,   944,   945,
     948,   949,   952,   953,   955,   960,   962,   963,   964,   972,
     978,   979,   980,   990,   992,   994,   996,   997,  1000,  1002,
    1004,  1006,  1008,  1010,  1011,  1013,  1016,  1021,  1022,  1023,
    1033,  1042,  1048,  1053,  1055,  1057,  1058,  1059,  1072,  1073,
    1074,  1083,  1084,  1086,  1087,  1088,  1097,  1101,  1107,  1112,
    1114,  1116,  1118,  1120,  1123,  1127,  1128,  1130,  1132,  1133,
    1135,  1137,  1139,  1141,  1144,  1147,  1150,  1153,  1155,  1159,
    1161,  1164,  1166,  1170,  1174,  1178,  1184,  1188,  1192,  1196,
    1202,  1207,  1209,  1215,  1217,  1219,  1221,  1223,  1225,  1227,
    1229,  1231,  1233,  1235,  1236,  1238,  1239,  1242,  1244,  1248,
    1249,  1252,  1254,  1258,  1262,  1266,  1270,  1272,  1276,  1280,
    1284,  1288,  1292,  1296,  1300,  1304,  1308,  1312,  1316,  1320,
    1324,  1328,  1332,  1336,  1340,  1344,  1348,  1352,  1356,  1360,
    1364,  1368,  1372,  1376,  1380,  1384,  1388,  1392,  1396,  1400,
    1404,  1408,  1412,  1416,  1418,  1421,  1424,  1427,  1430,  1433,
    1436,  1439,  1441,  1444,  1447,  1448,  1449,  1456,  1462,  1468,
    1473,  1478,  1483,  1484,  1486,  1488,  1490,  1492,  1494,  1496,
    1498,  1500,  1503,  1506,  1508,  1510,  1515,  1520,  1527,  1532,
    1537,  1542,  1547,  1552,  1556,  1562,  1568,  1573,  1578,  1582,
    1586,  1590,  1594,  1599,  1604,  1609,  1614,  1619,  1624,  1629,
    1631,  1635,  1639,  1641,  1645,  1648,  1652,  1656,  1660,  1663,
    1666,  1670,  1674,  1676,  1678,  1682,  1686,  1690,  1694,  1697,
    1700,  1703,  1706,  1709,  1710,  1712,  1715,  1717,  1720,  1723,
    1728,  1733,  1738,  1743,  1748,  1752,  1756,  1760,  1764,  1768,
    1770,  1772,  1773,  1777,  1785,  1793,  1801,  1809,  1817,  1823,
    1829,  1835,  1841,  1846,  1851,  1856,  1861,  1863,  1867,  1870,
    1872,  1875,  1876,  1880,  1882,  1884,  1886,  1889,  1891,  1893,
    1895,  1897,  1899,  1901,  1903,  1905,  1907,  1909,  1911,  1913,
    1915,  1917,  1919,  1921,  1923,  1925,  1927,  1929,  1931,  1933,
    1935,  1937,  1939,  1941,  1943,  1945,  1947,  1949,  1951,  1953,
    1955,  1957,  1959,  1961,  1963,  1965,  1967,  1969,  1971,  1973,
    1975,  1977,  1979,  1981,  1983,  1985,  1987
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     113,     0,    -1,   114,    -1,   114,    24,    -1,   114,   137,
      -1,   114,   101,    -1,    -1,   310,    -1,   115,    95,   310,
      -1,   102,    48,    -1,   102,   311,    -1,   102,     1,    -1,
      -1,   115,    -1,   296,    -1,   117,    -1,    53,    48,   102,
     296,   101,    -1,    -1,   121,   117,    -1,   154,    52,   120,
     116,   101,    -1,   154,    52,   120,     1,   101,    -1,   154,
      52,   120,     1,    24,    -1,   154,    52,   120,     1,   103,
      -1,   154,    52,     1,   101,    -1,   154,    52,     1,    24,
      -1,   154,    52,     1,   103,    -1,    51,   296,   101,    -1,
      51,   310,   101,    -1,    51,     1,   101,    -1,    51,     1,
      24,    -1,    51,     1,   103,    -1,    48,    88,   250,    -1,
     311,    88,   250,    -1,     1,    88,   250,    -1,   124,    -1,
     125,   104,   124,    -1,   154,     4,   125,   101,    -1,   154,
       4,     1,   101,    -1,   154,     4,     1,    24,    -1,   154,
       4,     1,   103,    -1,   197,    -1,   101,    -1,    24,    -1,
       1,    -1,   165,    -1,   105,    -1,   106,    -1,    -1,   103,
      -1,    -1,   103,    -1,    24,    -1,   107,    -1,   108,    -1,
      -1,    -1,    -1,     4,    -1,    -1,    -1,   154,   157,   128,
     136,   158,    48,   135,   105,   138,   146,   130,   139,   127,
      -1,   154,   157,   128,   136,   158,    48,   135,     1,    -1,
      -1,   154,   157,   128,   136,   158,   311,   140,   105,   146,
     106,   127,    -1,   154,   157,   128,   136,   191,   101,    -1,
     122,    -1,   119,    -1,   123,    -1,   126,    -1,   154,   232,
      -1,   154,   240,    -1,   243,    -1,     1,    24,    -1,     1,
     101,    -1,     1,   103,    -1,    -1,   154,   109,   141,   114,
     132,    -1,    39,    -1,    38,    -1,    -1,    48,    -1,   311,
      -1,    -1,   164,   142,   143,    -1,   105,   146,   130,    -1,
     281,    -1,   147,   281,    -1,   144,    -1,   147,   104,   144,
      -1,   147,   102,   144,    -1,    68,    -1,    56,    -1,    77,
      -1,    42,    -1,    87,    -1,    71,    -1,    55,    -1,    75,
      -1,    73,    -1,    54,    -1,    82,    -1,    68,    -1,    56,
      -1,    77,    -1,    42,    -1,    71,    -1,    55,    -1,    75,
      -1,    73,    -1,    54,    -1,    87,    -1,    82,    -1,    84,
      -1,    65,    -1,    27,    -1,    28,    -1,    35,    -1,    64,
      -1,    60,    -1,    69,    -1,    45,    -1,    57,    -1,    72,
      -1,    78,    -1,    43,    -1,    58,    -1,    41,    -1,    80,
      -1,    50,    -1,    37,    -1,    44,    -1,    85,    -1,    40,
      -1,    23,    -1,    22,    -1,    46,    -1,    31,    -1,    29,
      -1,    30,    -1,     4,    -1,    33,    -1,    34,    -1,    53,
      -1,    51,    -1,    52,    -1,    59,    -1,    74,    -1,    10,
      -1,    21,    -1,    20,    -1,    81,    -1,    47,    -1,   149,
      -1,   150,    -1,   151,    -1,    48,    -1,    49,    -1,   152,
      -1,   155,    -1,    -1,   155,   148,    -1,    28,   105,   115,
     281,   106,    -1,    35,   105,   106,    -1,    35,    -1,    -1,
     157,   156,    -1,   158,    97,    -1,    -1,   129,   163,   106,
      -1,   133,   163,   108,    -1,   168,    -1,   161,    97,    -1,
     163,    -1,   172,    -1,   163,    97,    -1,   169,    -1,   164,
      97,    -1,   168,    -1,   168,    -1,   169,    -1,   172,    -1,
     168,    90,   170,    -1,   170,    -1,   169,    90,   170,    -1,
     171,    -1,   171,    -1,   172,    -1,    43,    -1,    84,    -1,
      65,    -1,    78,   177,    -1,    58,   176,    -1,    64,   187,
      -1,    45,   180,    -1,    69,   178,    -1,    72,   178,    -1,
      27,   186,    -1,    60,   186,    -1,    28,   105,   115,   104,
     164,   106,    -1,    28,   105,   115,     1,   106,    -1,    28,
       1,    -1,    35,   105,   164,   106,    -1,    35,   105,     1,
     106,    -1,   296,    -1,    -1,     7,    -1,    96,     7,    -1,
      -1,     7,    -1,    96,     7,    -1,    38,    -1,    39,    -1,
      -1,   105,   174,   175,   173,   106,    -1,   105,     1,   106,
      -1,   176,    -1,    -1,    -1,   179,   105,   118,   106,    -1,
      -1,    -1,   105,   181,   183,   142,   102,   182,   164,   106,
      -1,    -1,   281,    -1,   184,   281,    -1,   164,    -1,    -1,
     184,   104,   185,   164,    -1,   105,   164,   106,    -1,    -1,
      -1,    -1,    -1,   105,   188,   164,   102,   189,   164,   190,
     106,    -1,    -1,   192,    -1,   191,   104,   192,    -1,   158,
      48,    -1,   158,   311,    -1,    -1,   158,    48,    88,   193,
     278,    -1,   158,    48,    88,     1,    -1,   158,    48,    88,
      24,    -1,   158,   311,    88,   278,    -1,   158,    48,    -1,
     158,   311,    -1,   158,    48,    88,   278,    -1,   158,   311,
      88,   278,    -1,   158,    48,    88,     1,    -1,   158,    48,
      88,    24,    -1,    48,    -1,   311,    -1,    48,    88,   250,
      -1,   311,    88,   250,    -1,    -1,    -1,    -1,   109,   198,
     196,   199,   208,   200,    -1,   103,    -1,    24,    -1,   197,
      -1,     1,    -1,    24,    -1,    -1,   194,    -1,   203,   104,
     202,   194,    -1,   195,    -1,   204,   104,   202,   194,    -1,
      48,    88,   250,    -1,   311,    88,   250,    -1,     1,    88,
     250,    -1,   205,    -1,   206,   104,   205,    -1,     4,   206,
     101,    -1,     4,     1,   101,    -1,     4,     1,    24,    -1,
       4,     1,   103,    -1,    -1,   208,   211,    -1,   272,   257,
      -1,   209,    -1,   123,    -1,   244,    -1,   270,    -1,   207,
      -1,   197,    -1,   215,   257,    -1,   217,   257,    -1,     1,
     101,    -1,     1,    24,    -1,     1,   103,    -1,   101,    -1,
     210,    -1,   261,    -1,   256,    -1,   258,    -1,   253,    -1,
     265,    -1,   268,    -1,   216,    -1,   212,    -1,    -1,    48,
     213,   102,   211,    -1,    48,    -1,    -1,    29,   214,    -1,
      34,   102,    -1,    34,    -1,    33,   214,    -1,    -1,    -1,
      -1,    -1,    59,   196,   219,   218,   221,   145,   222,   201,
      -1,    59,   196,   219,   218,     1,    -1,    -1,    48,   218,
     145,   224,   201,    -1,    48,   218,     1,    -1,    -1,   158,
      48,   218,   145,   226,   201,    -1,   158,    48,   218,     1,
      -1,   154,   128,   158,   142,    48,    -1,   154,   128,   158,
     311,    -1,   227,    -1,   228,   104,   227,    -1,   228,   102,
     227,    -1,   281,    -1,   228,   281,    -1,    -1,   105,   229,
     130,    -1,   109,   114,   200,    -1,     1,    -1,    24,    -1,
      -1,    -1,    31,   196,   143,   233,   234,   230,   231,    -1,
      48,    -1,   311,    -1,    -1,    88,   250,    -1,    -1,   235,
     236,    -1,    -1,   237,    -1,   239,   104,   238,   237,    -1,
       1,    -1,    -1,    -1,    41,   241,   143,   109,   242,   239,
     200,    -1,   154,    80,   161,   235,   101,    -1,    -1,    -1,
      50,   245,   196,   246,   105,   274,   247,   211,   248,    -1,
     106,    -1,   103,    -1,    24,    -1,    -1,    40,   211,    -1,
     306,    -1,     1,    -1,   278,    -1,    24,    -1,     1,    -1,
      -1,   249,    -1,   104,   249,    -1,   101,   251,   101,   251,
      -1,    -1,    -1,    23,   254,   196,   255,   105,   278,   252,
     247,   211,    -1,    37,   196,   211,    85,   105,   274,   247,
     257,    -1,    37,   196,   211,    85,    24,    -1,    37,   196,
     211,    24,    -1,   101,    -1,    24,    -1,    -1,    -1,    44,
     259,   196,   260,   105,   271,   257,   264,   257,   271,   247,
     211,    -1,    -1,    -1,    85,   262,   196,   263,   105,   274,
     247,   211,    -1,    -1,   274,    -1,    -1,    -1,    20,   266,
     196,   267,   105,   274,   247,   211,    -1,    30,   274,   269,
      -1,    30,   274,   175,   273,   269,    -1,    30,   175,   274,
     269,    -1,   102,    -1,   101,    -1,   103,    -1,    24,    -1,
      10,   257,    -1,    10,   274,   257,    -1,    -1,   274,    -1,
     275,    -1,    -1,   274,    -1,   275,    -1,     1,    -1,   276,
      -1,   166,   203,    -1,   167,   204,    -1,   167,   223,    -1,
     166,   225,    -1,   278,    -1,   276,   104,   278,    -1,   278,
      -1,   110,   278,    -1,   279,    -1,   294,    88,   278,    -1,
     294,    88,     1,    -1,   312,    88,   278,    -1,   133,   307,
     108,    88,   278,    -1,   294,   280,   278,    -1,   294,   280,
       1,    -1,   312,   280,   278,    -1,   133,   307,   108,   280,
     278,    -1,   133,   307,   108,     1,    -1,   287,    -1,   287,
      89,   279,   102,   279,    -1,    26,    -1,    70,    -1,    86,
      -1,    63,    -1,    76,    -1,    25,    -1,    79,    -1,    67,
      -1,    66,    -1,    36,    -1,    -1,   104,    -1,    -1,   283,
     281,    -1,   277,    -1,   283,   104,   277,    -1,    -1,   285,
     281,    -1,   286,    -1,   285,   104,   286,    -1,   285,   104,
       1,    -1,   278,   269,   278,    -1,   278,   269,     1,    -1,
     288,    -1,   287,    19,   287,    -1,   287,    18,   287,    -1,
     287,    90,   287,    -1,   287,    91,   287,    -1,   287,    92,
     287,    -1,   287,    11,   287,    -1,   287,    14,   287,    -1,
     287,    93,   287,    -1,   287,    12,   287,    -1,   287,    94,
     287,    -1,   287,    13,   287,    -1,   287,    16,   287,    -1,
     287,    17,   287,    -1,   287,    95,   287,    -1,   287,    96,
     287,    -1,   287,    97,   287,    -1,   287,    98,   287,    -1,
     287,    99,   287,    -1,   287,    19,     1,    -1,   287,    18,
       1,    -1,   287,    90,     1,    -1,   287,    91,     1,    -1,
     287,    92,     1,    -1,   287,    11,     1,    -1,   287,    14,
       1,    -1,   287,    93,     1,    -1,   287,    12,     1,    -1,
     287,    94,     1,    -1,   287,    13,     1,    -1,   287,    16,
       1,    -1,   287,    17,     1,    -1,   287,    95,     1,    -1,
     287,    96,     1,    -1,   287,    97,     1,    -1,   287,    98,
       1,    -1,   287,    99,     1,    -1,   289,    -1,   159,   288,
      -1,   160,   288,    -1,     8,   294,    -1,     9,   294,    -1,
      15,   288,    -1,   100,   288,    -1,    96,   288,    -1,   294,
      -1,   294,     8,    -1,   294,     9,    -1,    -1,    -1,   109,
     196,   135,   291,   208,   200,    -1,   294,   129,   282,   106,
     290,    -1,   294,   129,     1,   106,   290,    -1,   294,   129,
       1,    24,    -1,   294,   129,     1,   101,    -1,   294,   129,
       1,   103,    -1,    -1,   310,    -1,     7,    -1,     5,    -1,
     303,    -1,   300,    -1,   301,    -1,   305,    -1,   220,    -1,
     293,   232,    -1,   293,   240,    -1,   295,    -1,   292,    -1,
     294,   133,    97,   108,    -1,   294,   133,   278,   108,    -1,
     294,   133,   299,   175,   299,   108,    -1,   294,   133,     1,
     108,    -1,   294,   133,     1,    24,    -1,   294,   133,     1,
     101,    -1,   294,   133,     1,   103,    -1,   294,   133,     1,
     106,    -1,   129,   276,   106,    -1,   129,   109,   282,   131,
     106,    -1,   129,   133,   284,   134,   106,    -1,    62,   196,
     282,    61,    -1,    62,   196,   282,   106,    -1,   129,     1,
     106,    -1,   129,     1,    24,    -1,   129,     1,   101,    -1,
     129,     1,   103,    -1,    62,   196,     1,    61,    -1,    62,
     196,     1,   106,    -1,    62,   196,     1,    24,    -1,    62,
     196,     1,   101,    -1,    62,   196,     1,   103,    -1,   294,
       3,   196,   153,    -1,   294,     3,   196,     1,    -1,   296,
      -1,    55,    32,    48,    -1,    55,    32,   311,    -1,   298,
      -1,   296,   111,    48,    -1,   111,    48,    -1,    47,   111,
      48,    -1,   296,   111,   311,    -1,   296,   111,     1,    -1,
      48,    32,    -1,    47,    32,    -1,   297,    48,    32,    -1,
     297,   311,    32,    -1,    48,    -1,    49,    -1,    74,    32,
      48,    -1,    74,    32,   311,    -1,    83,    32,    48,    -1,
      83,    32,   311,    -1,   297,    48,    -1,   297,   311,    -1,
     297,     1,    -1,    32,    48,    -1,    32,   311,    -1,    -1,
     275,    -1,    94,   275,    -1,    24,    -1,    94,    24,    -1,
      46,   302,    -1,    81,   105,   278,   106,    -1,    81,   105,
       1,   106,    -1,    81,   105,     1,   103,    -1,    81,   105,
       1,    24,    -1,    81,   105,     1,   101,    -1,   105,   275,
     106,    -1,   105,     1,   106,    -1,   105,     1,    24,    -1,
     105,     1,   103,    -1,   105,     1,   101,    -1,   197,    -1,
       1,    -1,    -1,    22,   304,   302,    -1,    21,   105,   278,
     104,   278,   308,   106,    -1,    21,   105,   278,   104,   278,
       1,   106,    -1,    21,   105,   278,   104,   278,     1,    24,
      -1,    21,   105,   278,   104,   278,     1,   103,    -1,    21,
     105,   278,   104,   278,     1,   101,    -1,    21,   105,   278,
       1,   106,    -1,    21,   105,   278,     1,    24,    -1,    21,
     105,   278,     1,   103,    -1,    21,   105,   278,     1,   101,
      -1,    21,   105,     1,   106,    -1,    21,   105,     1,    24,
      -1,    21,   105,     1,   103,    -1,    21,   105,     1,   101,
      -1,   294,    -1,   133,   307,   108,    -1,   162,    48,    -1,
     312,    -1,   306,   308,    -1,    -1,   104,   306,   308,    -1,
       6,    -1,    57,    -1,   309,    -1,   310,   309,    -1,   312,
      -1,    27,    -1,    28,    -1,    31,    -1,    35,    -1,    41,
      -1,    43,    -1,    45,    -1,    57,    -1,    58,    -1,    64,
      -1,    65,    -1,    60,    -1,    69,    -1,    72,    -1,    78,
      -1,    80,    -1,    84,    -1,    49,    -1,    54,    -1,    55,
      -1,    68,    -1,    74,    -1,    71,    -1,    73,    -1,    75,
      -1,    87,    -1,    82,    -1,    77,    -1,    42,    -1,    56,
      -1,    37,    -1,    40,    -1,    10,    -1,    51,    -1,    53,
      -1,    52,    -1,    22,    -1,    46,    -1,    59,    -1,    21,
      -1,    20,    -1,    81,    -1,    29,    -1,    30,    -1,    33,
      -1,    34,    -1,    44,    -1,    23,    -1,    50,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   380,   380,   381,   385,   386,   387,   390,   391,   404,
     405,   406,   407,   413,   438,   455,   466,   508,   508,   518,
     555,   561,   568,   574,   575,   580,   583,   590,   598,   599,
     604,   607,   680,   681,   684,   685,   688,   689,   690,   695,
     698,   703,   704,   705,   709,   722,   729,   735,   742,   744,
     749,   750,   756,   763,   765,   771,   790,   793,   802,   842,
     799,  1133,  1150,  1149,  1159,  1166,  1167,  1168,  1169,  1170,
    1171,  1172,  1173,  1179,  1185,  1193,  1191,  1204,  1205,  1210,
    1213,  1214,  1215,  1218,  1248,  1255,  1256,  1259,  1260,  1261,
    1269,  1277,  1278,  1279,  1280,  1281,  1282,  1283,  1284,  1285,
    1286,  1290,  1291,  1292,  1293,  1294,  1295,  1296,  1297,  1298,
    1299,  1300,  1304,  1305,  1306,  1307,  1308,  1309,  1310,  1311,
    1312,  1313,  1314,  1315,  1316,  1317,  1318,  1319,  1323,  1324,
    1325,  1326,  1327,  1328,  1329,  1330,  1331,  1332,  1333,  1334,
    1335,  1336,  1337,  1338,  1339,  1340,  1341,  1342,  1343,  1344,
    1345,  1346,  1349,  1349,  1349,  1351,  1351,  1352,  1360,  1367,
    1368,  1371,  1375,  1381,  1389,  1390,  1393,  1394,  1397,  1407,
    1417,  1418,  1427,  1427,  1429,  1436,  1439,  1446,  1449,  1462,
    1475,  1488,  1489,  1492,  1493,  1496,  1496,  1499,  1500,  1501,
    1502,  1503,  1504,  1505,  1506,  1507,  1508,  1509,  1510,  1515,
    1521,  1525,  1531,  1540,  1626,  1629,  1630,  1643,  1646,  1647,
    1659,  1660,  1667,  1670,  1709,  1716,  1722,  1723,  1723,  1758,
    1762,  1757,  1789,  1802,  1803,  1806,  1808,  1807,  1813,  1814,
    1818,  1821,  1824,  1817,  1829,  1837,  1838,  1841,  1855,  1857,
    1856,  1881,  1885,  1890,  1897,  1913,  1914,  1933,  1938,  1944,
    1953,  1965,  1966,  1981,  1985,  1992,  1997,  1991,  2017,  2018,
    2025,  2026,  2027,  2032,  2038,  2039,  2044,  2045,  2050,  2088,
    2089,  2092,  2093,  2096,  2097,  2098,  2103,  2107,  2108,  2114,
    2116,  2117,  2118,  2119,  2120,  2121,  2122,  2123,  2124,  2125,
    2132,  2139,  2142,  2146,  2147,  2148,  2149,  2150,  2151,  2152,
    2153,  2157,  2156,  2173,  2174,  2177,  2178,  2179,  2185,  2188,
    2196,  2204,  2212,  2203,  2348,  2367,  2366,  2494,  2510,  2509,
    2644,  2660,  2706,  2709,  2710,  2711,  2718,  2719,  2722,  2723,
    2732,  2733,  2734,  2742,  2818,  2741,  2998,  2999,  3002,  3003,
    3007,  3008,  3074,  3080,  3081,  3082,  3087,  3097,  3086,  3118,
    3139,  3143,  3138,  3163,  3164,  3165,  3172,  3173,  3176,  3187,
    3190,  3191,  3192,  3196,  3197,  3200,  3201,  3206,  3210,  3205,
    3233,  3241,  3248,  3257,  3258,  3266,  3270,  3265,  3290,  3294,
    3289,  3310,  3311,  3315,  3319,  3314,  3334,  3338,  3342,  3348,
    3349,  3353,  3357,  3364,  3376,  3382,  3383,  3386,  3388,  3389,
    3392,  3393,  3396,  3397,  3398,  3399,  3400,  3404,  3405,  3411,
    3412,  3414,  3415,  3416,  3417,  3418,  3427,  3428,  3429,  3430,
    3436,  3443,  3444,  3447,  3448,  3449,  3450,  3451,  3452,  3453,
    3454,  3455,  3456,  3459,  3459,  3461,  3462,  3466,  3467,  3470,
    3471,  3474,  3475,  3484,  3487,  3491,  3494,  3495,  3496,  3497,
    3498,  3499,  3500,  3501,  3502,  3503,  3504,  3505,  3506,  3507,
    3508,  3509,  3510,  3511,  3512,  3513,  3514,  3515,  3516,  3517,
    3518,  3519,  3520,  3521,  3522,  3523,  3524,  3525,  3526,  3527,
    3528,  3529,  3530,  3533,  3534,  3539,  3544,  3545,  3546,  3547,
    3548,  3551,  3552,  3553,  3575,  3579,  3576,  3671,  3677,  3683,
    3690,  3696,  3705,  3716,  3717,  3718,  3719,  3720,  3721,  3722,
    3723,  3724,  3725,  3726,  3727,  3728,  3734,  3740,  3747,  3753,
    3759,  3761,  3763,  3765,  3773,  3780,  3790,  3797,  3804,  3805,
    3810,  3811,  3812,  3813,  3817,  3822,  3823,  3824,  3830,  3833,
    3834,  3901,  3907,  3908,  3917,  3933,  3940,  3941,  3944,  3983,
    3991,  4021,  4024,  4050,  4058,  4074,  4078,  4135,  4140,  4186,
    4187,  4188,  4227,  4235,  4236,  4238,  4240,  4245,  4252,  4267,
    4290,  4291,  4292,  4297,  4300,  4301,  4302,  4307,  4308,  4309,
    4310,  4314,  4313,  4324,  4331,  4338,  4346,  4353,  4360,  4366,
    4373,  4379,  4385,  4386,  4391,  4392,  4395,  4396,  4402,  4413,
    4417,  4423,  4424,  4430,  4431,  4462,  4463,  4481,  4482,  4484,
    4486,  4488,  4490,  4492,  4494,  4496,  4498,  4500,  4502,  4504,
    4506,  4508,  4510,  4512,  4514,  4516,  4526,  4528,  4530,  4532,
    4534,  4536,  4538,  4540,  4542,  4544,  4546,  4548,  4550,  4552,
    4554,  4556,  4558,  4560,  4562,  4564,  4566,  4568,  4570,  4572,
    4574,  4576,  4578,  4580,  4582,  4584,  4586
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "TOK_ARROW", "TOK_CONSTANT", "TOK_FLOAT",
  "TOK_STRING", "TOK_NUMBER", "TOK_INC", "TOK_DEC", "TOK_RETURN", "TOK_EQ",
  "TOK_GE", "TOK_LE", "TOK_NE", "TOK_NOT", "TOK_LSH", "TOK_RSH",
  "TOK_LAND", "TOK_LOR", "TOK_SWITCH", "TOK_SSCANF", "TOK_CATCH",
  "TOK_FOREACH", "TOK_LEX_EOF", "TOK_ADD_EQ", "TOK_AND_EQ", "TOK_ARRAY_ID",
  "TOK_ATTRIBUTE_ID", "TOK_BREAK", "TOK_CASE", "TOK_CLASS",
  "TOK_COLON_COLON", "TOK_CONTINUE", "TOK_DEFAULT", "TOK_DEPRECATED_ID",
  "TOK_DIV_EQ", "TOK_DO", "TOK_DOT_DOT", "TOK_DOT_DOT_DOT", "TOK_ELSE",
  "TOK_ENUM", "TOK_EXTERN", "TOK_FLOAT_ID", "TOK_FOR", "TOK_FUNCTION_ID",
  "TOK_GAUGE", "TOK_GLOBAL", "TOK_IDENTIFIER", "TOK_RESERVED", "TOK_IF",
  "TOK_IMPORT", "TOK_INHERIT", "TOK_FACET", "TOK_INLINE", "TOK_LOCAL_ID",
  "TOK_FINAL_ID", "TOK_FUNCTION_NAME", "TOK_INT_ID", "TOK_LAMBDA",
  "TOK_MULTISET_ID", "TOK_MULTISET_END", "TOK_MULTISET_START",
  "TOK_LSH_EQ", "TOK_MAPPING_ID", "TOK_MIXED_ID", "TOK_MOD_EQ",
  "TOK_MULT_EQ", "TOK_NO_MASK", "TOK_OBJECT_ID", "TOK_OR_EQ",
  "TOK_PRIVATE", "TOK_PROGRAM_ID", "TOK_PROTECTED", "TOK_PREDEF",
  "TOK_PUBLIC", "TOK_RSH_EQ", "TOK_STATIC", "TOK_STRING_ID", "TOK_SUB_EQ",
  "TOK_TYPEDEF", "TOK_TYPEOF", "TOK_VARIANT", "TOK_VERSION", "TOK_VOID_ID",
  "TOK_WHILE", "TOK_XOR_EQ", "TOK_OPTIONAL", "'='", "'?'", "'|'", "'^'",
  "'&'", "'>'", "'<'", "'+'", "'-'", "'*'", "'%'", "'/'", "'~'", "';'",
  "':'", "'}'", "','", "'('", "')'", "'['", "']'", "'{'", "'@'", "'.'",
  "$accept", "all", "program", "string_constant",
  "optional_rename_inherit", "low_program_ref", "program_ref", "facet",
  "inherit_ref", "@1", "inheritance", "import", "constant_name",
  "constant_list", "constant", "block_or_semi", "type_or_error",
  "open_paren_with_line_info", "close_paren_or_missing",
  "close_brace_or_missing", "close_brace_or_eof",
  "open_bracket_with_line_info", "close_bracket_or_missing",
  "push_compiler_frame0", "optional_constant", "def", "@2", "@3", "@4",
  "@5", "optional_dot_dot_dot", "optional_identifier", "new_arg_name",
  "func_args", "arguments", "arguments2", "modifier", "magic_identifiers1",
  "magic_identifiers2", "magic_identifiers3", "magic_identifiers",
  "magic_identifier", "modifiers", "modifier_list", "attribute",
  "optional_attributes", "optional_stars", "cast", "soft_cast",
  "full_type", "type6", "type", "type7", "simple_type", "simple_type2",
  "simple_identifier_type", "type4", "type2", "type8", "basic_type",
  "identifier_type", "number_or_maxint", "number_or_minint",
  "expected_dot_dot", "opt_int_range", "opt_string_width",
  "opt_object_type", "@6", "opt_function_type", "@7", "@8",
  "function_type_list", "function_type_list2", "@9", "opt_array_type",
  "opt_mapping_type", "@10", "@11", "@12", "name_list", "new_name", "@13",
  "new_local_name", "new_local_name2", "line_number_info", "block", "@14",
  "@15", "end_block", "failsafe_block", "propagated_type",
  "local_name_list", "local_name_list2", "local_constant_name",
  "local_constant_list", "local_constant", "statements",
  "statement_with_semicolon", "normal_label_statement", "statement",
  "labeled_statement", "@16", "optional_label", "break", "default",
  "continue", "push_compiler_frame1", "implicit_identifier", "lambda",
  "@17", "@18", "local_function", "@19", "local_function2", "@20",
  "create_arg", "create_arguments2", "create_arguments",
  "optional_create_arguments", "failsafe_program", "class", "@21", "@22",
  "simple_identifier", "enum_value", "enum_def", "propagated_enum_value",
  "enum_list", "enum", "@23", "@24", "typedef", "cond", "@25", "@26",
  "end_cond", "optional_else_part", "safe_lvalue", "safe_expr0",
  "foreach_optional_lvalue", "foreach_lvalues", "foreach", "@27", "@28",
  "do", "expected_semicolon", "for", "@29", "@30", "while", "@31", "@32",
  "for_expr", "switch", "@33", "@34", "case", "expected_colon", "return",
  "unused", "unused2", "optional_comma_expr", "safe_comma_expr",
  "comma_expr", "comma_expr2", "expr00", "expr0", "expr01", "assign",
  "optional_comma", "expr_list", "expr_list2", "m_expr_list",
  "m_expr_list2", "assoc_pair", "expr1", "expr2", "expr3",
  "optional_block", "@35", "apply", "implicit_modifiers", "expr4",
  "idents2", "idents", "inherit_specifier", "low_idents", "range_bound",
  "gauge", "typeof", "catch_arg", "catch", "@36", "sscanf", "lvalue",
  "low_lvalue_list", "lvalue_list", "string_segment", "string",
  "bad_identifier", "bad_expr_ident", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,    61,    63,
     124,    94,    38,    62,    60,    43,    45,    42,    37,    47,
     126,    59,    58,   125,    44,    40,    41,    91,    93,   123,
      64,    46
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   112,   113,   113,   114,   114,   114,   115,   115,   116,
     116,   116,   116,   117,   117,   118,   119,   121,   120,   122,
     122,   122,   122,   122,   122,   122,   123,   123,   123,   123,
     123,   124,   124,   124,   125,   125,   126,   126,   126,   126,
     127,   127,   127,   127,   128,   129,   130,   130,   131,   131,
     132,   132,   133,   134,   134,   135,   136,   136,   138,   139,
     137,   137,   140,   137,   137,   137,   137,   137,   137,   137,
     137,   137,   137,   137,   137,   141,   137,   142,   142,   142,
     143,   143,   143,   144,   145,   146,   146,   147,   147,   147,
     148,   148,   148,   148,   148,   148,   148,   148,   148,   148,
     148,   149,   149,   149,   149,   149,   149,   149,   149,   149,
     149,   149,   150,   150,   150,   150,   150,   150,   150,   150,
     150,   150,   150,   150,   150,   150,   150,   150,   151,   151,
     151,   151,   151,   151,   151,   151,   151,   151,   151,   151,
     151,   151,   151,   151,   151,   151,   151,   151,   151,   151,
     151,   151,   152,   152,   152,   153,   153,   153,   154,   155,
     155,   156,   156,   156,   157,   157,   158,   158,   159,   160,
     161,   161,   162,   162,   163,   163,   164,   164,   165,   166,
     167,   168,   168,   169,   169,   170,   170,   171,   171,   171,
     171,   171,   171,   171,   171,   171,   171,   171,   171,   171,
     171,   171,   171,   172,   173,   173,   173,   174,   174,   174,
     175,   175,   176,   176,   176,   177,   178,   179,   178,   181,
     182,   180,   180,   183,   183,   184,   185,   184,   186,   186,
     188,   189,   190,   187,   187,   191,   191,   192,   192,   193,
     192,   192,   192,   192,   194,   194,   194,   194,   194,   194,
     195,   195,   195,   195,   196,   198,   199,   197,   200,   200,
     201,   201,   201,   202,   203,   203,   204,   204,   205,   205,
     205,   206,   206,   207,   207,   207,   207,   208,   208,   209,
     210,   210,   210,   210,   210,   210,   210,   210,   210,   210,
     210,   210,   211,   211,   211,   211,   211,   211,   211,   211,
     211,   213,   212,   214,   214,   215,   216,   216,   217,   218,
     219,   221,   222,   220,   220,   224,   223,   223,   226,   225,
     225,   227,   227,   228,   228,   228,   229,   229,   230,   230,
     231,   231,   231,   233,   234,   232,   235,   235,   236,   236,
     237,   237,   238,   239,   239,   239,   241,   242,   240,   243,
     245,   246,   244,   247,   247,   247,   248,   248,   249,   249,
     250,   250,   250,   251,   251,   252,   252,   254,   255,   253,
     256,   256,   256,   257,   257,   259,   260,   258,   262,   263,
     261,   264,   264,   266,   267,   265,   268,   268,   268,   269,
     269,   269,   269,   270,   270,   271,   271,   272,   273,   273,
     274,   274,   275,   275,   275,   275,   275,   276,   276,   277,
     277,   278,   278,   278,   278,   278,   278,   278,   278,   278,
     278,   279,   279,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   280,   281,   281,   282,   282,   283,   283,   284,
     284,   285,   285,   285,   286,   286,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   288,   288,   288,   288,   288,   288,   288,
     288,   289,   289,   289,   290,   291,   290,   292,   292,   292,
     292,   292,   293,   294,   294,   294,   294,   294,   294,   294,
     294,   294,   294,   294,   294,   294,   294,   294,   294,   294,
     294,   294,   294,   294,   294,   294,   294,   294,   294,   294,
     294,   294,   294,   294,   294,   294,   294,   294,   294,   295,
     295,   295,   296,   296,   296,   296,   296,   296,   297,   297,
     297,   297,   298,   298,   298,   298,   298,   298,   298,   298,
     298,   298,   298,   299,   299,   299,   299,   299,   300,   301,
     301,   301,   301,   301,   302,   302,   302,   302,   302,   302,
     302,   304,   303,   305,   305,   305,   305,   305,   305,   305,
     305,   305,   305,   305,   305,   305,   306,   306,   306,   306,
     307,   308,   308,   309,   309,   310,   310,   311,   311,   311,
     311,   311,   311,   311,   311,   311,   311,   311,   311,   311,
     311,   311,   311,   311,   311,   311,   312,   312,   312,   312,
     312,   312,   312,   312,   312,   312,   312,   312,   312,   312,
     312,   312,   312,   312,   312,   312,   312,   312,   312,   312,
     312,   312,   312,   312,   312,   312,   312
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     2,     2,     0,     1,     3,     2,
       2,     2,     0,     1,     1,     1,     5,     0,     2,     5,
       5,     5,     5,     4,     4,     4,     3,     3,     3,     3,
       3,     3,     3,     3,     1,     3,     4,     4,     4,     4,
       1,     1,     1,     1,     1,     1,     1,     0,     1,     0,
       1,     1,     1,     1,     0,     0,     0,     1,     0,     0,
      13,     8,     0,    11,     6,     1,     1,     1,     1,     2,
       2,     1,     2,     2,     2,     0,     5,     1,     1,     0,
       1,     1,     0,     3,     3,     1,     2,     1,     3,     3,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       2,     5,     3,     1,     0,     2,     2,     0,     3,     3,
       1,     2,     1,     1,     2,     1,     2,     1,     1,     1,
       1,     3,     1,     3,     1,     1,     1,     1,     1,     1,
       2,     2,     2,     2,     2,     2,     2,     2,     6,     5,
       2,     4,     4,     1,     0,     1,     2,     0,     1,     2,
       1,     1,     0,     5,     3,     1,     0,     0,     4,     0,
       0,     8,     0,     1,     2,     1,     0,     4,     3,     0,
       0,     0,     0,     8,     0,     1,     3,     2,     2,     0,
       5,     4,     4,     4,     2,     2,     4,     4,     4,     4,
       1,     1,     3,     3,     0,     0,     0,     6,     1,     1,
       1,     1,     1,     0,     1,     4,     1,     4,     3,     3,
       3,     1,     3,     3,     3,     3,     3,     0,     2,     2,
       1,     1,     1,     1,     1,     1,     2,     2,     2,     2,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     4,     1,     0,     2,     2,     1,     2,     0,
       0,     0,     0,     8,     5,     0,     5,     3,     0,     6,
       4,     5,     4,     1,     3,     3,     1,     2,     0,     3,
       3,     1,     1,     0,     0,     7,     1,     1,     0,     2,
       0,     2,     0,     1,     4,     1,     0,     0,     7,     5,
       0,     0,     9,     1,     1,     1,     0,     2,     1,     1,
       1,     1,     1,     0,     1,     2,     4,     0,     0,     9,
       8,     5,     4,     1,     1,     0,     0,    12,     0,     0,
       8,     0,     1,     0,     0,     8,     3,     5,     4,     1,
       1,     1,     1,     2,     3,     0,     1,     1,     0,     1,
       1,     1,     1,     2,     2,     2,     2,     1,     3,     1,
       2,     1,     3,     3,     3,     5,     3,     3,     3,     5,
       4,     1,     5,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     0,     1,     0,     2,     1,     3,     0,
       2,     1,     3,     3,     3,     3,     1,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     1,     2,     2,     2,     2,     2,     2,
       2,     1,     2,     2,     0,     0,     6,     5,     5,     4,
       4,     4,     0,     1,     1,     1,     1,     1,     1,     1,
       1,     2,     2,     1,     1,     4,     4,     6,     4,     4,
       4,     4,     4,     3,     5,     5,     4,     4,     3,     3,
       3,     3,     4,     4,     4,     4,     4,     4,     4,     1,
       3,     3,     1,     3,     2,     3,     3,     3,     2,     2,
       3,     3,     1,     1,     3,     3,     3,     3,     2,     2,
       2,     2,     2,     0,     1,     2,     1,     2,     2,     4,
       4,     4,     4,     4,     3,     3,     3,     3,     3,     1,
       1,     0,     3,     7,     7,     7,     7,     7,     5,     5,
       5,     5,     4,     4,     4,     4,     1,     3,     2,     1,
       2,     0,     3,     1,     1,     1,     2,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       6,     0,     0,     1,     0,     3,     0,     0,     5,    66,
      65,    67,    68,     4,   164,   158,    71,    72,    73,    74,
       0,   603,     0,     0,   552,   553,   604,     0,     0,     0,
       0,     0,   542,   605,     0,     0,     0,   254,   346,     0,
       0,    75,     0,    69,    70,    93,    99,    96,    91,    90,
      95,    98,    97,    92,   100,    94,   160,    29,    28,    30,
     640,   648,   647,   644,   655,   608,   609,   650,   651,   610,
     652,   653,   611,   638,   639,   612,   636,   613,   654,   614,
     645,   561,   625,   656,   641,   643,   642,   626,   627,   637,
     615,   616,   646,   619,   617,   618,   628,   620,   630,   621,
     631,   629,   632,   635,   622,   623,   649,   634,   624,   633,
     562,   607,   549,     0,   548,     0,     0,   544,    26,     0,
     560,   558,   559,    27,   606,     0,     0,     0,    34,     0,
       0,    82,    82,     0,     0,     0,   229,     0,     0,   187,
     222,   212,   229,   234,   189,   216,   216,   212,   188,     0,
     170,   182,   185,   186,   203,     6,     0,   163,    56,   165,
      44,   178,   545,   554,   555,   556,   557,   547,   543,   546,
     550,   551,     0,    38,     0,    37,    39,     0,    36,     0,
       0,    80,   333,    81,     0,    24,    23,    25,     0,     0,
       0,    13,    18,    14,     7,     0,   196,   200,     0,     0,
     219,   193,     0,   191,   197,   230,   192,   194,     0,   195,
     215,   190,   336,   171,     0,   337,     0,     0,     0,     0,
      57,   167,    16,   362,   505,   504,   502,   502,   502,   647,
     644,   361,     0,   627,   646,   254,   629,   649,   502,   502,
      45,    52,     0,   502,   502,   502,   510,    33,   360,   411,
     421,   446,   483,   514,     0,   491,   513,   539,   507,   508,
     506,   509,   503,     0,    31,     0,    35,    32,   334,   347,
      21,    20,    22,    11,     9,    10,    19,     0,     0,   177,
       0,     0,     0,   433,     0,   208,     0,     0,     0,     0,
     349,   181,    51,    50,    76,     0,   162,     0,     0,   235,
       0,   581,     0,     0,   254,     0,     0,   486,   487,     0,
     488,   491,     0,     0,   580,     0,   255,   579,   568,     0,
     310,     0,     0,   490,   489,     0,   435,   439,     0,   175,
     184,     0,   407,   502,     0,   172,   173,   596,   203,   601,
       0,   599,   484,   485,     0,     0,     0,     0,     0,     0,
       0,     0,   502,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   511,   512,   254,   492,   493,   428,   423,
     432,   426,   431,   430,   424,   427,   429,   425,     0,     0,
       0,     0,   502,   502,   328,     0,     8,   176,   228,     0,
       0,   202,   201,   434,   225,    79,   433,   223,   214,   209,
     210,   211,   204,     0,    15,     0,   434,     0,    55,   166,
     238,    64,   167,     0,     0,     0,   582,     0,   167,     0,
     179,   180,     0,   402,   254,   540,   541,   309,     0,   502,
     437,   409,     0,   433,     0,     0,   529,   530,   531,   528,
      49,   502,     0,    54,   433,   441,   491,   599,   174,   168,
       0,   502,   523,   172,     0,   598,   169,   502,   600,     0,
     470,   452,   473,   455,   475,   457,   471,   453,   476,   458,
     477,   459,   466,   448,   465,   447,     0,   467,   449,   468,
     450,   469,   451,   472,   454,   474,   456,   478,   460,   479,
     461,   480,   462,   481,   463,   482,   464,     0,   413,   412,
       0,     0,     0,   566,   502,     0,   564,   407,     0,   417,
     416,   414,   418,   159,     0,   345,   338,   343,     0,   199,
       0,    78,    77,     0,   226,   224,   205,     0,     0,   231,
     218,   161,     0,     0,   502,     0,     0,   236,   593,   595,
     594,   592,     0,   502,   576,   578,   577,   575,     0,   264,
     403,   406,   250,   266,   404,   405,   251,   574,   256,     0,
     534,   532,   535,   536,   533,   410,   526,   527,   434,   436,
     572,   573,   571,   570,   569,    48,     0,     0,   392,   390,
     389,   391,     0,    53,     0,     0,   440,   183,   408,   597,
     601,   420,   502,   502,   502,   538,   139,   147,   149,   148,
     134,   133,   114,   115,   137,   138,   136,   140,   141,   116,
     129,   132,   126,   104,   124,   130,   120,   135,   151,   155,
     156,   128,   143,   144,   142,   109,   106,   102,   121,   125,
     145,   118,   117,   113,   101,   119,   105,   122,   108,   146,
     107,   103,   123,   127,   150,   111,   112,   131,   110,   152,
     153,   154,   157,   537,   499,   500,   501,   494,   494,   519,
     520,   521,   522,   518,   567,   565,   515,   516,   502,     0,
     323,   433,    47,   326,   331,   332,     6,   335,     0,   341,
     259,   258,   342,   348,   198,   220,     0,   206,   213,     0,
     241,   242,   502,    61,    58,   243,   433,   237,   238,   589,
     591,   590,   588,     0,   244,   245,   263,     0,     0,   263,
       0,   277,   314,     0,   438,   524,     0,   445,   444,   525,
     443,   442,   602,   415,   419,   422,   254,   498,   497,     0,
     167,   159,   159,   327,    46,   329,     0,   339,   340,     0,
     227,   232,   240,   433,    87,     0,   433,    79,    85,     0,
       0,     0,     0,   502,   167,   252,   317,   433,   315,   167,
     253,     0,   312,    55,   517,    79,   325,   324,   330,   344,
       0,     0,    47,     0,     0,   434,    86,    82,   585,   587,
     586,   584,   583,   248,   249,   246,   320,   318,   247,     0,
     265,    47,     0,   267,     0,     0,     0,   648,   655,   650,
       0,   652,   307,   254,   654,   552,   656,     0,   378,   291,
     281,   285,   257,   284,   280,   292,   278,   300,     0,   299,
       0,   282,   296,   294,   295,   293,   297,   298,   283,     0,
     397,     0,   495,     0,   322,   221,   233,    59,    43,    42,
      41,    63,    40,    89,    88,    83,     0,   244,    84,   261,
     262,   260,   316,   289,   288,   290,     0,     0,   271,     0,
       0,   401,   374,   373,   393,     0,   400,   254,   254,   303,
     305,     0,     0,   308,   306,     0,   254,     0,   254,   254,
     286,   287,   279,   313,   277,   321,     0,   319,   275,     0,
     274,   276,     0,   273,     0,     0,   394,   384,   368,     0,
       0,   386,     0,   376,     0,   351,   379,     0,    60,   270,
     268,     0,   272,   269,     0,     0,   388,     0,   399,   372,
       0,     0,   302,     0,     0,   496,     0,   502,   387,   371,
       0,     0,     0,     0,     0,     0,     0,     0,   396,     0,
       0,   355,   354,   353,     0,     0,     0,     0,     0,     0,
       0,     0,   385,   359,   364,     0,   358,   365,     0,   370,
       0,   382,   356,   380,     0,   369,     0,     0,   352,   366,
       0,   357,     0,   377
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     2,   191,   190,   192,   405,     9,   134,   135,
      10,   810,   128,   129,    12,   841,   158,   242,   735,   576,
     294,   243,   584,   533,   221,    13,   743,   886,   535,   155,
     523,   182,   744,   758,   745,   746,    56,   649,   650,   651,
     652,   653,    14,    15,   159,    42,   789,   244,   245,   149,
     334,   453,   747,   160,   418,   419,   279,   420,   151,   330,
     421,   528,   287,   402,   203,   211,   207,   208,   201,   283,
     739,   395,   396,   686,   196,   206,   288,   689,   771,   298,
     299,   692,   549,   553,   320,   811,   424,   711,   683,   852,
     754,   550,   554,   858,   859,   813,   761,   814,   815,   816,
     817,   877,   870,   818,   819,   820,   559,   427,   246,   713,
     831,   555,   792,   551,   846,   670,   671,   672,   514,   677,
      43,   268,   384,   516,   679,   517,   738,   518,    44,   132,
     385,    16,   821,   878,   923,   944,   968,   954,   247,   955,
     947,   822,   868,   915,   823,   864,   824,   876,   921,   825,
     879,   924,   960,   826,   867,   914,   827,   582,   828,   937,
     829,   917,   938,   866,   423,   430,   332,   249,   381,   748,
     432,   433,   443,   444,   445,   250,   251,   252,   727,   884,
     253,   254,   255,   256,   257,    31,    32,   508,   258,   259,
     318,   260,   313,   261,   339,   340,   458,    33,   262,   183,
     263
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -765
static const yytype_int16 yypact[] =
{
    -765,    66,   949,  -765,   555,  -765,   306,    22,  -765,  -765,
    -765,  -765,  -765,  -765,   243,   836,  -765,  -765,  -765,  -765,
     569,  -765,  7762,   211,    60,  -765,  -765,    76,    91,    41,
     147,  7110,  -765,  -765,   234,   171,  7179,  -765,  -765,   398,
    7636,  -765,  7690,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,   233,  -765,  7831,  7900,  -765,  -765,  7248,
    -765,   302,   307,  -765,  -765,   360,   382,   277,  -765,   149,
     283,  7969,  7969,   591,   255,   467,   305,    78,   317,  -765,
     354,   374,   305,   395,  -765,   401,   401,   374,  -765,  6667,
     420,  -765,  -765,  -765,   421,  -765,   110,   429,   536,  -765,
    -765,   420,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,   286,  -765,  2959,  -765,  -765,  2959,  -765,  7317,
    2959,  -765,  -765,  -765,   442,  -765,  -765,  -765,   631,  7386,
     452,   474,  -765,   421,   262,  7636,  -765,  -765,   262,  5310,
    -765,  -765,   357,  -765,  -765,  -765,  -765,  -765,   468,  -765,
    -765,  -765,  -765,  -765,   475,  -765,  7636,  3900,   262,  5256,
    -765,  -765,  -765,  -765,  -765,  -765,  6186,  6186,  6206,   477,
      71,  -765,  5488,   553,   123,  -765,    76,   481,  6206,  6206,
    -765,  -765,  2054,  5899,  6206,  6206,  -765,  -765,  -765,  -765,
    6478,  -765,  -765,  -765,   370,  6313,  -765,   421,  -765,  -765,
    -765,  -765,   262,  1112,  -765,   500,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,   262,   170,   420,
     204,   507,   270,  7582,   510,  -765,   624,   479,  7636,   467,
    -765,  -765,  -765,  -765,  -765,   197,  -765,  6740,   196,  -765,
     477,  -765,    87,   553,  -765,   481,  3066,   181,   181,  4109,
    -765,   224,  3387,    87,  -765,  2161,  -765,  -765,  -765,  8038,
    -765,  2637,  3494,  -765,  -765,   335,  5992,  5700,   272,   544,
    -765,   323,  -765,  5899,   594,   188,  -765,   181,  5441,   540,
     539,  -765,  -765,  -765,  4072,  4101,  4208,  4237,  4344,  4373,
    4480,  4509,  6206,  4616,  4645,  4752,  4781,  4888,  4917,  5024,
    5053,  5160,  5189,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  3601,  2744,
    1411,  3708,  6099,  6099,   556,  6401,   262,  -765,  -765,   543,
    7636,  -765,  -765,  -765,   567,   490,   564,  -765,  -765,  -765,
    -765,  -765,   218,   -12,  -765,   563,  7636,   565,   419,  -765,
     -27,  -765,  -765,   188,   359,    57,  -765,   381,  -765,  8107,
     544,  -765,   568,   572,  -765,  -765,  -765,  -765,   245,  6099,
    -765,  -765,   -11,   573,   432,   575,  -765,  -765,  -765,  -765,
     581,  5899,   424,   587,   585,  -765,  6267,  1112,  -765,  -765,
    7636,  6099,  -765,   600,   603,  -765,  -765,  5899,  -765,   687,
    -765,   605,  -765,   528,  -765,   528,  -765,   605,  -765,   620,
    -765,   620,  -765,  6495,  -765,  5402,   618,  -765,  6504,  -765,
    5389,  -765,  4178,  -765,   528,  -765,   528,  -765,   450,  -765,
     450,  -765,  -765,  -765,  -765,  -765,  -765,  7041,  -765,  -765,
     462,   616,   312,  -765,  5807,   621,  -765,   622,   479,  -765,
    -765,  -765,  -765,    59,    70,  -765,   638,  -765,   269,  -765,
     326,  -765,  -765,   629,   347,  -765,  -765,   729,   632,  -765,
    -765,  -765,  3173,   124,  6099,   642,  6813,  -765,  -765,  -765,
    -765,  -765,   535,  6099,  -765,  -765,  -765,  -765,  6886,  -765,
     645,  -765,   237,  -765,   648,  -765,   671,  -765,  -765,   129,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  5992,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,   655,   656,  -765,  -765,
    -765,  -765,  3815,  -765,   659,  2851,  -765,  -765,  -765,  -765,
     540,  -765,  6099,  6099,  6206,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,   658,   658,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  5593,  7636,
    -765,   239,   670,  -765,  -765,  -765,  -765,  -765,  2959,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  7636,  -765,  -765,  7636,
    -765,  -765,  6099,  -765,  -765,  -765,  7582,   691,   692,  -765,
    -765,  -765,  -765,   117,   240,   693,  -765,  2959,   219,  -765,
    2959,  -765,  -765,   677,  -765,  -765,   476,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,   676,
    -765,  -765,    51,  -765,  -765,  -765,  3988,  -765,  8176,  7636,
     567,   567,  -765,  7582,  -765,   680,   409,    25,  -765,   547,
     682,  3280,   235,  6099,  -765,  -765,  -765,  7582,  -765,  -765,
    -765,  1304,  -765,  -765,  -765,  6594,  -765,  -765,  -765,  -765,
     355,   683,   670,    73,  7636,  7636,  -765,  7969,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  6959,
    -765,   670,   102,  -765,   684,  7455,  1046,   686,   688,   265,
    1196,   265,   797,  5243,   690,    15,   694,  5360,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,     6,  -765,
       6,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,     6,
    -765,   102,  -765,   744,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,   102,   708,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,   551,   709,  -765,   353,
     710,  -765,  -765,  -765,  -765,     6,  -765,  -765,  -765,  -765,
    -765,  2268,   703,  -765,  -765,  1519,  -765,   700,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,    73,  -765,  -765,  2959,
    -765,  -765,  2959,  -765,  7524,  2959,  -765,  -765,  -765,   424,
    1626,  -765,    11,  -765,  1519,  -765,  -765,  1304,  -765,  -765,
    -765,   715,  -765,  -765,   705,   706,  -765,   424,  -765,  -765,
     167,   707,  -765,   711,   713,  -765,  2268,  6099,  -765,  -765,
    2268,  1840,  2268,  2268,   336,   362,   336,     6,  -765,   336,
     336,  -765,  -765,  -765,  1519,  2452,  2544,   336,     6,  1947,
    1519,  1519,  -765,  -765,  -765,   714,  -765,  -765,  1519,  -765,
       6,  -765,   774,  -765,  2360,  -765,  1733,  1519,  -765,  -765,
     336,  -765,  1519,  -765
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -765,  -765,  -146,  -150,  -765,   530,  -765,  -765,  -765,  -765,
    -765,    -1,   647,  -765,  -765,   -65,   155,  -202,  -707,  -765,
    -765,   250,  -765,    67,  -765,  -765,  -765,  -765,  -765,  -765,
    -692,  -126,  -381,  -632,  -491,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -501,  -765,  -765,  -765,  -194,  -765,  -765,  -765,
    -765,  -199,  -119,  -765,  -765,  -765,   -32,  -240,  -193,   131,
     -29,  -765,  -765,  -482,   685,  -765,   695,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,   696,  -765,  -765,  -765,  -765,  -765,
     417,  -765,  -668,  -765,   -33,  -201,  -765,  -765,  -719,  -764,
     127,  -765,  -765,   -57,  -765,  -765,   -44,  -765,  -765,  -285,
    -765,  -765,    46,  -765,  -765,  -765,  -523,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -103,  -765,  -765,  -765,  -765,
     588,  -765,  -765,   699,  -765,   111,  -765,  -765,   596,  -765,
    -765,  -765,  -765,  -765,  -765,  -261,  -765,   -95,   -64,  -112,
    -765,  -765,  -765,  -765,  -765,  -643,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -441,  -765,  -113,
    -765,  -765,  -551,   -36,   120,   287,  -118,  -324,  -247,  -238,
    -275,  -765,  -765,  -765,   271,  6178,   252,  -765,   199,  -765,
    -765,  -765,  -206,  -765,    -6,  -765,  -765,   186,  -765,  -765,
     545,  -765,  -765,  -765,  -442,  -256,  -544,   -16,     1,    18,
     -17
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -654
static const yytype_int16 yytable[] =
{
      30,    11,   329,   329,   131,   111,   184,    34,   150,   217,
     161,   153,   669,   153,   111,   590,   383,   768,   124,   111,
     307,   308,   311,   291,   306,   306,   668,   297,   476,   708,
     862,   317,   311,   311,   154,   919,   154,   337,   311,   311,
     110,   306,   812,   328,   335,   397,   722,   114,   280,   122,
     566,   440,  -434,   379,   130,   777,   248,   407,   542,   248,
    -433,   534,   248,   521,   522,   837,     3,   883,   295,   329,
      35,   674,  -581,   833,   838,  -434,   278,   454,   -62,   197,
     282,   762,   887,  -433,   848,   387,   790,   329,   314,   117,
     529,   793,   114,   329,   675,   567,   920,   839,   111,   111,
     282,   317,   111,   849,   501,   379,   379,   863,   115,   379,
     413,   197,   317,   264,   111,   111,   267,  -301,   749,   172,
     787,   446,   387,   116,  -254,   693,   850,   337,   335,   193,
     712,   306,   111,   164,   166,   379,   194,   169,   311,   311,
     311,   311,   311,   311,   311,   311,   311,   311,   311,   311,
     311,   311,   311,   311,   311,   311,   311,  -434,   525,   750,
    -434,   543,   111,   393,   394,  -433,   153,   215,  -433,   403,
     153,   152,   111,   152,   840,   880,  -581,   881,   124,   676,
    -581,   752,   316,   198,   365,   577,   882,   153,   925,   154,
     153,   929,   315,   154,   415,   569,   316,   130,   389,   194,
     383,   329,   321,   431,   435,   389,   586,   275,   431,   442,
     154,   316,   593,   154,   336,   218,    11,   329,   536,   194,
     756,   457,   896,  -601,   548,   526,   341,   365,  -254,   694,
     669,   669,   366,   367,  -311,   337,   786,   338,  -309,   306,
      21,  -309,   335,   112,   379,   865,   124,    36,   118,   872,
     178,   337,   772,   179,   153,   306,   188,   587,   119,   153,
     499,   431,   507,   510,   511,   512,   791,   387,    21,   560,
     725,   520,   930,   125,    37,   673,   388,   154,   386,   422,
     111,   162,   154,   193,    38,   448,   240,   520,   241,  -304,
     194,    26,   277,   680,   949,    39,   456,   411,   336,   277,
     412,   406,   111,  -433,   336,   959,   561,    20,   390,   338,
     447,   565,    21,   869,   527,   410,   341,   966,   871,    26,
     899,   338,   113,    40,   757,   707,   152,   338,   751,   240,
     152,   241,   497,   588,   170,   123,   659,   426,    22,   171,
     757,   731,  -309,   732,   506,  -309,   562,   152,   563,   918,
     152,   564,    41,    23,    24,    25,   -12,   189,   284,   436,
     941,   153,   331,    26,   285,   177,  -304,   387,   111,   448,
     124,   180,   681,   682,   338,   934,   392,   153,   449,   936,
      27,   939,   940,   538,   154,  -434,  -434,   222,   311,    28,
     900,   558,    22,   843,   844,  -207,  -207,   119,   961,   133,
     154,    37,   111,   215,   -17,   544,   173,    23,    24,    25,
     195,    38,   336,   660,   152,   661,   695,    29,   662,   152,
     663,   153,   199,   387,   341,   703,   331,   451,   336,   452,
     -17,   901,   684,   733,    27,   338,   437,   556,   438,   942,
     341,   439,   943,    28,   154,   -17,   -17,   -17,   578,  -434,
     431,   338,   387,   286,   893,   -17,   570,   894,   916,   200,
     539,   835,   540,   945,   718,   541,   946,   442,   665,   593,
     174,    29,   -17,    21,   723,   724,   928,   591,   309,   202,
     310,   -17,   545,   175,   546,   176,   654,   547,   309,   309,
     323,   324,   327,   333,   309,   309,   342,   343,   338,    22,
     205,   368,   369,   956,   956,   380,  -217,   532,   776,   -17,
     216,   774,   370,   775,    23,    24,    25,   400,   401,   111,
    -237,   152,   956,  -237,    26,   579,   580,   581,   521,   522,
     736,   111,   119,   571,   219,   572,   765,   152,   573,   371,
     220,    27,   372,   373,   348,   349,   374,   360,   361,   362,
      28,   269,   375,   276,   698,   376,   327,   380,   380,   699,
     248,   380,   377,   655,   592,   656,   705,   740,   657,   277,
     741,   778,   842,   289,   742,   888,   290,   441,    29,    17,
    -597,   152,   312,   333,  -597,   319,   322,   380,   174,   248,
     902,   851,   248,    57,   309,   309,   309,   309,   309,   309,
     309,   309,   309,   309,   309,   309,   309,   309,   309,   309,
     309,   309,   309,   391,   737,   185,   398,   345,   346,   922,
     770,   348,   349,   358,   359,   360,   361,   362,   766,   767,
     851,   399,   506,   785,   450,   788,   700,   161,   701,   889,
     153,   702,   455,   755,   457,   851,   760,   459,   779,   519,
     780,   845,   890,   781,   891,   270,    18,   153,    19,   952,
     153,   513,   338,   154,   387,   962,   963,   153,   524,   530,
      58,   531,    59,   965,   557,   948,   451,   568,   950,   951,
     154,   574,   971,   154,   575,   842,   958,   973,   591,   585,
     154,   333,   186,   763,   187,   583,   380,   448,   356,   357,
     358,   359,   360,   361,   362,   329,   329,   333,   853,   972,
     153,   589,   368,   369,   153,   358,   359,   360,   361,   362,
     594,   111,   658,   370,   329,   830,   678,   578,   153,   666,
     667,   685,   271,   154,   272,    11,   687,   154,   688,   337,
     337,   400,   401,   306,   306,   153,   153,   696,   111,   706,
     371,   154,   709,   372,   373,   338,   215,   374,   337,   710,
     111,   715,   306,   375,   716,   719,   376,   726,   154,   154,
     875,   248,   111,   377,   248,   592,   734,   248,   111,   532,
     534,   753,   757,   834,   764,   854,   773,   855,   782,   836,
     338,  -383,   885,  -367,   338,  -375,   751,   892,   895,  -350,
     152,    30,   904,   889,   579,   580,   581,   705,    34,   935,
     926,   927,   931,   860,   967,   964,   932,   152,   933,   404,
     152,   908,  -653,  -653,   730,   909,   266,   152,   910,   537,
     832,   913,   210,  -653,   897,   898,   759,   912,   204,   830,
     907,   209,   363,   903,   309,   905,   906,   873,   214,   769,
     364,   957,   969,   970,   729,   714,   721,   728,   416,     0,
    -653,     0,     0,  -653,  -653,   338,     0,  -653,   830,   338,
     152,   830,     0,  -653,   152,     0,  -653,   111,    45,     0,
       0,     0,     0,  -653,     0,  -653,     0,     0,   152,     0,
      46,    47,    48,     0,   338,     0,     0,     0,   338,   874,
       0,   338,     0,     0,    49,   152,   152,    50,   830,    51,
       0,    52,   860,    53,   830,   830,   336,   336,    54,     0,
     338,     0,   830,    55,   338,   338,   338,   338,   341,   341,
       0,   830,     0,     0,     0,   336,   830,     0,   338,   338,
     338,     0,     0,   338,   338,   338,     0,   341,     0,    -2,
       4,     0,   338,  -159,     0,     0,     0,     0,   338,     0,
     338,   338,     0,     0,     0,     0,   338,     0,     0,     0,
       0,     0,     0,     5,     0,     0,  -159,  -159,     0,     0,
    -159,  -159,     0,     0,  -159,     0,     0,     0,     0,     0,
    -159,  -159,  -159,     0,  -159,     0,  -159,  -159,  -159,     0,
       6,  -159,     7,  -159,  -159,  -159,     0,  -159,     0,  -159,
       0,     0,     0,  -159,  -159,     0,     0,  -159,  -159,     0,
    -159,  -159,  -159,  -159,  -159,     0,  -159,  -159,     0,  -159,
       0,  -159,  -159,  -159,     0,     0,  -159,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   861,     0,     0,
       8,   224,    21,   225,   226,   227,    60,     0,  -159,     0,
    -159,   228,     0,     0,     0,     0,    61,   229,   230,    64,
     862,  -640,  -640,   136,   137,    67,    68,  -502,    22,    70,
      71,   138,  -640,    73,     0,     0,    74,  -502,    76,   139,
      78,   140,   232,    23,    24,    25,    83,    84,    85,    86,
      87,   233,    89,    26,   141,   234,   142,     0,   235,  -640,
     143,   144,  -640,  -640,    96,   145,  -640,    98,   146,   100,
     236,   102,  -640,   103,   147,  -640,     0,   237,   107,    28,
     148,     0,  -640,   109,  -640,     0,     0,   368,   369,     0,
       0,     0,   238,     0,     0,     0,   239,   863,   370,     0,
       0,   240,     0,   241,     0,     0,     0,    29,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   371,     0,     0,   372,   373,
       0,     0,   374,     0,     0,     0,     0,     0,   375,     0,
       0,   376,     0,     0,     0,   333,   333,   861,   377,     0,
     382,   224,    21,   225,   226,   227,    60,     0,     0,     0,
       0,   228,     0,     0,   333,     0,    61,   229,   230,    64,
       0,  -651,  -651,   136,   137,    67,    68,  -502,    22,    70,
      71,   138,  -651,    73,   400,   401,    74,  -502,    76,   139,
      78,   140,   232,    23,    24,    25,    83,    84,    85,    86,
      87,   233,    89,    26,   141,   234,   142,     0,   235,  -651,
     143,   144,  -651,  -651,    96,   145,  -651,    98,   146,   100,
     236,   102,  -651,   103,   147,  -651,     0,   237,   107,    28,
     148,     0,  -651,   109,  -651,     0,     0,     0,     0,     0,
       0,     0,   238,     0,     0,     0,   239,     0,     0,     0,
       0,   240,     0,   241,     0,   794,     0,    29,   795,   224,
      21,   225,   226,   227,   796,     0,     0,     0,     0,   228,
       0,     0,     0,     0,   797,   229,   230,   798,   680,     0,
       0,   136,   137,   799,   800,  -502,    22,   801,   802,   138,
       0,   803,     0,     0,    74,  -502,    76,   139,   804,   140,
     232,    23,   805,    25,   806,   807,    85,    86,    87,   233,
      89,    26,   141,   234,   142,     0,   235,     0,   143,   144,
       0,     0,    96,   145,     0,    98,   146,   100,   236,   102,
       0,   103,   147,     0,     0,   237,   107,    28,   148,   808,
       0,   109,     0,     0,     0,     0,     0,     0,     0,     0,
     238,     0,     0,     0,   239,   809,     0,   681,     0,   240,
       0,   241,   502,   316,     0,    29,   224,    21,   225,   226,
     227,    60,     0,     0,     0,     0,   228,     0,     0,     0,
       0,    61,   229,   230,    64,   503,     0,     0,   136,   137,
      67,    68,  -502,    22,    70,    71,   138,     0,    73,  -563,
    -563,    74,  -502,    76,   139,    78,   140,   232,    23,    24,
      25,    83,    84,    85,    86,    87,   233,    89,    26,   141,
     234,   142,     0,   235,     0,   143,   144,     0,     0,    96,
     145,     0,    98,   146,   100,   236,   102,     0,   103,   147,
       0,     0,   237,   107,    28,   148,     0,     0,   109,     0,
       0,     0,     0,     0,     0,   504,     0,   238,   505,     0,
       0,   239,     0,     0,     0,     0,   240,     0,   241,     0,
     794,     0,    29,   795,   224,    21,   225,   226,   227,   796,
       0,     0,     0,     0,   228,     0,     0,     0,     0,   797,
     229,   230,   798,     0,     0,     0,   136,   137,   799,   800,
    -502,    22,   801,   802,   138,     0,   803,     0,     0,    74,
    -502,    76,   139,   804,   140,   232,    23,   805,    25,   806,
     807,    85,    86,    87,   233,    89,    26,   141,   234,   142,
       0,   235,     0,   143,   144,     0,     0,    96,   145,     0,
      98,   146,   100,   236,   102,     0,   103,   147,     0,     0,
     237,   107,    28,   148,   808,     0,   109,     0,     0,     0,
       0,     0,     0,     0,     0,   238,     0,     0,     0,   239,
     809,     0,     0,     0,   240,     0,   241,   861,   316,     0,
      29,   224,    21,   225,   226,   227,    60,     0,     0,     0,
       0,   228,     0,     0,     0,     0,    61,   229,   230,    64,
    -398,     0,     0,   136,   137,    67,    68,  -502,    22,    70,
      71,   138,     0,    73,     0,     0,    74,  -502,    76,   139,
      78,   140,   232,    23,    24,    25,    83,    84,    85,    86,
      87,   233,    89,    26,   141,   234,   142,     0,   235,     0,
     143,   144,     0,     0,    96,   145,     0,    98,   146,   100,
     236,   102,     0,   103,   147,     0,     0,   237,   107,    28,
     148,     0,     0,   109,     0,     0,     0,     0,     0,     0,
       0,     0,   238,     0,     0,     0,   239,  -398,  -398,  -398,
       0,   240,     0,   241,   861,     0,     0,    29,   224,    21,
     225,   226,   227,    60,     0,     0,     0,     0,   228,     0,
       0,     0,     0,    61,   229,   230,    64,  -395,     0,     0,
     136,   137,    67,    68,  -502,    22,    70,    71,   138,     0,
      73,     0,     0,    74,  -502,    76,   139,    78,   140,   232,
      23,    24,    25,    83,    84,    85,    86,    87,   233,    89,
      26,   141,   234,   142,     0,   235,     0,   143,   144,     0,
       0,    96,   145,     0,    98,   146,   100,   236,   102,     0,
     103,   147,     0,     0,   237,   107,    28,   148,     0,     0,
     109,     0,     0,     0,     0,     0,     0,     0,     0,   238,
       0,     0,     0,   239,     0,     0,  -395,     0,   240,  -395,
     241,   861,     0,     0,    29,   224,    21,   225,   226,   227,
      60,     0,     0,     0,     0,   228,     0,     0,     0,     0,
      61,   229,   230,    64,  -395,     0,     0,   136,   137,    67,
      68,  -502,    22,    70,    71,   138,     0,    73,     0,     0,
      74,  -502,    76,   139,    78,   140,   232,    23,    24,    25,
      83,    84,    85,    86,    87,   233,    89,    26,   141,   234,
     142,     0,   235,     0,   143,   144,     0,     0,    96,   145,
       0,    98,   146,   100,   236,   102,     0,   103,   147,     0,
       0,   237,   107,    28,   148,     0,     0,   109,     0,     0,
       0,     0,     0,     0,     0,     0,   238,     0,     0,     0,
     239,  -395,     0,     0,     0,   240,     0,   241,   861,     0,
       0,    29,   224,    21,   225,   226,   227,    60,     0,     0,
       0,     0,   228,     0,     0,     0,     0,    61,   229,   230,
      64,  -381,     0,     0,   136,   137,    67,    68,  -502,    22,
      70,    71,   138,     0,    73,     0,     0,    74,  -502,    76,
     139,    78,   140,   232,    23,    24,    25,    83,    84,    85,
      86,    87,   233,    89,    26,   141,   234,   142,     0,   235,
       0,   143,   144,     0,     0,    96,   145,     0,    98,   146,
     100,   236,   102,     0,   103,   147,     0,     0,   237,   107,
      28,   148,     0,     0,   109,     0,     0,     0,     0,     0,
       0,     0,     0,   238,     0,     0,     0,   239,  -381,     0,
       0,     0,   240,     0,   241,   325,     0,     0,    29,   224,
      21,   225,   226,   227,    60,     0,     0,     0,     0,   228,
       0,     0,     0,     0,    61,   229,   230,    64,     0,     0,
       0,   136,   137,    67,    68,  -502,    22,    70,    71,   138,
       0,    73,     0,     0,    74,  -502,    76,   139,    78,   140,
     232,    23,    24,    25,    83,    84,    85,    86,    87,   233,
      89,    26,   141,   234,   142,     0,   235,     0,   143,   144,
       0,     0,    96,   145,     0,    98,   146,   100,   236,   102,
       0,   103,   147,     0,     0,   237,   107,    28,   148,     0,
       0,   109,     0,     0,     0,     0,     0,     0,     0,     0,
     238,     0,     0,     0,   239,     0,     0,     0,     0,   240,
       0,   241,   417,   326,     0,    29,   224,    21,   225,   226,
     227,    60,     0,     0,     0,     0,   228,     0,     0,     0,
       0,    61,   229,   230,    64,     0,     0,     0,   136,   137,
      67,    68,  -502,    22,    70,    71,   138,     0,    73,     0,
       0,    74,  -502,    76,   139,    78,   140,   232,    23,    24,
      25,    83,    84,    85,    86,    87,   233,    89,    26,   141,
     234,   142,     0,   235,     0,   143,   144,     0,     0,    96,
     145,     0,    98,   146,   100,   236,   102,     0,   103,   147,
       0,     0,   237,   107,    28,   148,     0,     0,   109,     0,
       0,     0,     0,     0,     0,     0,     0,   238,     0,     0,
       0,   239,     0,     0,     0,     0,   240,     0,   241,   861,
       0,     0,    29,   224,    21,   225,   226,   227,    60,     0,
       0,     0,     0,   228,     0,     0,     0,     0,    61,   229,
     230,    64,     0,     0,     0,   136,   137,    67,    68,  -502,
      22,    70,    71,   138,     0,    73,     0,     0,    74,  -502,
      76,   139,    78,   140,   232,    23,    24,    25,    83,    84,
      85,    86,    87,   233,    89,    26,   141,   234,   142,     0,
     235,     0,   143,   144,     0,     0,    96,   145,     0,    98,
     146,   100,   236,   102,     0,   103,   147,     0,     0,   237,
     107,    28,   148,     0,     0,   109,     0,     0,     0,     0,
       0,   953,     0,     0,   238,   224,    21,   225,   239,     0,
      60,     0,     0,   240,     0,   241,     0,     0,     0,    29,
      61,   229,   230,    64,  -363,     0,     0,   136,   137,    67,
      68,  -502,    22,    70,    71,   138,     0,    73,     0,     0,
      74,  -502,    76,   139,    78,   140,   232,    23,    24,    25,
      83,    84,    85,    86,    87,   233,    89,    26,   141,   234,
     142,     0,   235,     0,   143,   144,     0,     0,    96,   145,
       0,    98,   146,   100,   236,   102,     0,   103,   147,     0,
       0,   237,   107,    28,   148,     0,     0,   109,     0,     0,
       0,     0,     0,   953,     0,     0,     0,   224,    21,   225,
       0,     0,    60,  -363,     0,   240,  -363,   241,     0,     0,
       0,    29,    61,   229,   230,    64,     0,     0,     0,   136,
     137,    67,    68,  -502,    22,    70,    71,   138,     0,    73,
       0,     0,    74,  -502,    76,   139,    78,   140,   232,    23,
      24,    25,    83,    84,    85,    86,    87,   233,    89,    26,
     141,   234,   142,     0,   235,     0,   143,   144,     0,     0,
      96,   145,     0,    98,   146,   100,   236,   102,     0,   103,
     147,     0,     0,   237,   107,    28,   148,     0,     0,   109,
       0,     0,     0,     0,     0,   953,     0,     0,     0,   224,
      21,   225,     0,  -363,    60,     0,     0,   240,     0,   241,
       0,     0,     0,    29,    61,   229,   230,    64,     0,     0,
       0,   136,   137,    67,    68,  -502,    22,    70,    71,   138,
       0,    73,     0,     0,    74,  -502,    76,   139,    78,   140,
     232,    23,    24,    25,    83,    84,    85,    86,    87,   233,
      89,    26,   141,   234,   142,     0,   235,     0,   143,   144,
       0,     0,    96,   145,     0,    98,   146,   100,   236,   102,
       0,   103,   147,     0,     0,   237,   107,    28,   148,     0,
       0,   109,     0,     0,     0,     0,     0,     0,   428,     0,
       0,     0,   224,    21,   225,   226,   227,    60,     0,   240,
       0,   241,   228,     0,     0,    29,     0,    61,   229,   230,
      64,     0,     0,     0,     0,     0,    67,    68,  -502,    22,
      70,    71,     0,     0,    73,     0,     0,    74,  -502,    76,
       0,    78,     0,   232,    23,    24,    25,    83,    84,    85,
      86,    87,   233,    89,    26,     0,   234,     0,  -435,   235,
       0,     0,     0,     0,     0,    96,     0,     0,    98,     0,
     100,   236,   102,     0,   103,     0,     0,     0,   237,   107,
      28,     0,     0,     0,   109,     0,     0,     0,     0,     0,
       0,     0,     0,   238,     0,     0,     0,   239,     0,     0,
       0,     0,   240,  -435,   241,   500,     0,   429,    29,   224,
      21,   225,   226,   227,    60,     0,     0,     0,     0,   228,
       0,     0,     0,     0,    61,   229,   230,    64,     0,     0,
       0,     0,     0,    67,    68,  -502,    22,    70,    71,     0,
       0,    73,     0,     0,    74,  -502,    76,     0,    78,     0,
     232,    23,    24,    25,    83,    84,    85,    86,    87,   233,
      89,    26,     0,   234,     0,     0,   235,     0,     0,     0,
       0,     0,    96,     0,     0,    98,     0,   100,   236,   102,
       0,   103,     0,     0,     0,   237,   107,    28,     0,     0,
       0,   109,     0,     0,     0,     0,     0,     0,     0,     0,
     238,     0,     0,     0,   239,     0,     0,     0,     0,   240,
    -435,   241,   720,     0,   429,    29,   224,    21,   225,   226,
     227,    60,     0,     0,     0,     0,   228,     0,     0,     0,
       0,    61,   229,   230,    64,     0,     0,     0,     0,     0,
      67,    68,  -502,    22,    70,    71,     0,     0,    73,     0,
       0,    74,  -502,    76,     0,    78,     0,   232,    23,    24,
      25,    83,    84,    85,    86,    87,   233,    89,    26,     0,
     234,     0,     0,   235,     0,     0,     0,     0,     0,    96,
       0,     0,    98,     0,   100,   236,   102,     0,   103,     0,
       0,     0,   237,   107,    28,     0,     0,     0,   109,     0,
       0,     0,     0,     0,     0,     0,     0,   238,     0,     0,
       0,   239,     0,     0,     0,     0,   240,  -434,   241,  -434,
     223,     0,    29,     0,   224,    21,   225,   226,   227,    60,
       0,     0,     0,     0,   228,     0,     0,     0,     0,    61,
     229,   230,    64,   231,     0,     0,     0,     0,    67,    68,
    -502,    22,    70,    71,     0,     0,    73,     0,     0,    74,
    -502,    76,     0,    78,     0,   232,    23,    24,    25,    83,
      84,    85,    86,    87,   233,    89,    26,     0,   234,     0,
       0,   235,     0,     0,     0,     0,     0,    96,     0,     0,
      98,     0,   100,   236,   102,     0,   103,     0,     0,     0,
     237,   107,    28,     0,     0,     0,   109,     0,     0,     0,
       0,     0,     0,     0,     0,   238,     0,     0,     0,   239,
       0,     0,     0,     0,   240,     0,   241,   325,     0,     0,
      29,   224,    21,   225,   226,   227,    60,     0,     0,     0,
       0,   228,     0,     0,     0,     0,    61,   229,   230,    64,
       0,     0,     0,     0,     0,    67,    68,  -502,    22,    70,
      71,     0,     0,    73,     0,     0,    74,  -502,    76,     0,
      78,     0,   232,    23,    24,    25,    83,    84,    85,    86,
      87,   233,    89,    26,     0,   234,     0,     0,   235,     0,
       0,     0,     0,     0,    96,     0,     0,    98,     0,   100,
     236,   102,     0,   103,     0,     0,     0,   237,   107,    28,
       0,     0,     0,   109,     0,     0,     0,     0,     0,     0,
       0,     0,   238,     0,     0,     0,   239,     0,     0,     0,
       0,   240,     0,   241,   690,   326,     0,    29,  -239,  -239,
    -239,  -239,  -239,  -239,     0,     0,     0,     0,  -239,     0,
       0,     0,     0,  -239,  -239,  -239,  -239,   691,     0,     0,
       0,     0,  -239,  -239,  -239,  -239,  -239,  -239,     0,     0,
    -239,     0,     0,  -239,  -239,  -239,     0,  -239,     0,  -239,
    -239,  -239,  -239,  -239,  -239,  -239,  -239,  -239,  -239,  -239,
    -239,     0,  -239,     0,     0,  -239,     0,     0,     0,     0,
       0,  -239,     0,     0,  -239,     0,  -239,  -239,  -239,     0,
    -239,     0,     0,     0,  -239,  -239,  -239,     0,     0,     0,
    -239,     0,     0,     0,     0,     0,     0,     0,     0,  -239,
       0,     0,     0,  -239,     0,     0,     0,     0,  -239,     0,
    -239,   783,     0,     0,  -239,   224,    21,   225,   226,   227,
      60,     0,     0,     0,     0,   228,     0,     0,     0,     0,
      61,   229,   230,    64,   784,     0,     0,     0,     0,    67,
      68,  -502,    22,    70,    71,     0,     0,    73,     0,     0,
      74,  -502,    76,     0,    78,     0,   232,    23,    24,    25,
      83,    84,    85,    86,    87,   233,    89,    26,     0,   234,
       0,     0,   235,     0,     0,     0,     0,     0,    96,     0,
       0,    98,     0,   100,   236,   102,     0,   103,     0,     0,
       0,   237,   107,    28,     0,     0,     0,   109,     0,     0,
       0,     0,     0,     0,     0,     0,   238,     0,     0,     0,
     239,     0,     0,     0,     0,   240,     0,   241,   414,     0,
       0,    29,   224,    21,   225,   226,   227,    60,     0,     0,
       0,     0,   228,     0,     0,     0,     0,    61,   229,   230,
      64,     0,     0,     0,     0,     0,    67,    68,  -502,    22,
      70,    71,     0,     0,    73,     0,     0,    74,  -502,    76,
       0,    78,     0,   232,    23,    24,    25,    83,    84,    85,
      86,    87,   233,    89,    26,     0,   234,     0,     0,   235,
       0,     0,     0,     0,     0,    96,     0,     0,    98,     0,
     100,   236,   102,     0,   103,     0,     0,     0,   237,   107,
      28,     0,     0,     0,   109,     0,     0,     0,     0,     0,
       0,     0,     0,   238,     0,     0,     0,   239,     0,     0,
       0,     0,   240,     0,   241,   434,     0,     0,    29,   224,
      21,   225,   226,   227,    60,     0,     0,     0,     0,   228,
       0,     0,     0,     0,    61,   229,   230,    64,     0,     0,
       0,     0,     0,    67,    68,  -502,    22,    70,    71,     0,
       0,    73,     0,     0,    74,  -502,    76,     0,    78,     0,
     232,    23,    24,    25,    83,    84,    85,    86,    87,   233,
      89,    26,     0,   234,     0,     0,   235,     0,     0,     0,
       0,     0,    96,     0,     0,    98,     0,   100,   236,   102,
       0,   103,     0,     0,     0,   237,   107,    28,     0,     0,
       0,   109,     0,     0,     0,     0,     0,     0,     0,     0,
     238,     0,     0,     0,   239,     0,     0,     0,     0,   240,
       0,   241,   498,     0,     0,    29,   224,    21,   225,   226,
     227,    60,     0,     0,     0,     0,   228,     0,     0,     0,
       0,    61,   229,   230,    64,     0,     0,     0,     0,     0,
      67,    68,  -502,    22,    70,    71,     0,     0,    73,     0,
       0,    74,  -502,    76,     0,    78,     0,   232,    23,    24,
      25,    83,    84,    85,    86,    87,   233,    89,    26,     0,
     234,     0,     0,   235,     0,     0,     0,     0,     0,    96,
       0,     0,    98,     0,   100,   236,   102,     0,   103,     0,
       0,     0,   237,   107,    28,     0,     0,     0,   109,     0,
       0,     0,     0,     0,     0,     0,     0,   238,     0,     0,
       0,   239,     0,     0,     0,     0,   240,     0,   241,   509,
       0,     0,    29,   224,    21,   225,   226,   227,    60,     0,
       0,     0,     0,   228,     0,     0,     0,     0,    61,   229,
     230,    64,     0,     0,     0,     0,     0,    67,    68,  -502,
      22,    70,    71,     0,     0,    73,     0,     0,    74,  -502,
      76,     0,    78,     0,   232,    23,    24,    25,    83,    84,
      85,    86,    87,   233,    89,    26,     0,   234,     0,     0,
     235,     0,     0,     0,     0,     0,    96,     0,     0,    98,
       0,   100,   236,   102,     0,   103,     0,     0,     0,   237,
     107,    28,     0,     0,     0,   109,     0,     0,     0,     0,
       0,     0,     0,     0,   238,     0,     0,     0,   239,     0,
       0,     0,     0,   240,     0,   241,   717,     0,     0,    29,
     224,    21,   225,   226,   227,    60,     0,     0,     0,     0,
     228,     0,     0,     0,     0,    61,   229,   230,    64,     0,
       0,     0,     0,     0,    67,    68,  -502,    22,    70,    71,
       0,     0,    73,     0,     0,    74,  -502,    76,     0,    78,
       0,   232,    23,    24,    25,    83,    84,    85,    86,    87,
     233,    89,    26,     0,   234,     0,     0,   235,     0,     0,
       0,     0,     0,    96,     0,     0,    98,     0,   100,   236,
     102,     0,   103,     0,     0,     0,   237,   107,    28,     0,
       0,     4,   109,     0,  -159,     0,     0,     0,     0,     0,
       0,   238,     0,     0,     0,   239,     0,     0,     0,     0,
     240,     0,   241,     0,   292,     0,    29,  -159,  -159,     0,
       0,  -159,  -159,     0,     0,  -159,     0,     0,     0,     0,
       0,  -159,  -159,  -159,     0,  -159,     0,  -159,  -159,  -159,
       0,     6,  -159,     7,  -159,  -159,  -159,     0,  -159,     0,
    -159,     0,     0,     0,  -159,  -159,     0,     0,  -159,  -159,
       0,  -159,  -159,  -159,  -159,  -159,     0,  -159,  -159,     0,
    -159,     0,  -159,  -159,  -159,     0,     0,  -159,     0,     4,
       0,     0,  -159,     0,     0,     0,     0,     0,     0,     0,
       0,     8,     0,   293,     0,     0,     0,     0,     0,  -159,
       0,  -159,   680,     0,     0,  -159,  -159,     0,     0,  -159,
    -159,     0,     0,  -159,     0,     0,     0,     0,     0,  -159,
    -159,  -159,     0,  -159,     0,  -159,  -159,  -159,     0,     6,
    -159,     7,  -159,  -159,  -159,     0,  -159,     0,  -159,     0,
       0,     0,  -159,  -159,     0,     0,  -159,  -159,     0,  -159,
    -159,  -159,  -159,  -159,     0,  -159,  -159,     0,  -159,     0,
    -159,  -159,  -159,   460,     0,  -159,     0,   224,    21,   225,
     226,   227,     0,     0,     0,     0,     0,   228,     0,     8,
       0,   681,     0,   300,   301,     0,     0,  -159,     0,  -159,
       0,     0,   462,  -502,    22,     0,   224,    21,   225,   226,
     227,     0,     0,  -502,     0,     0,   228,     0,   302,    23,
      24,    25,   300,   301,     0,     0,     0,   303,     0,    26,
       0,   304,  -502,    22,   235,     0,   136,   137,     0,     0,
       0,     0,  -502,     0,   138,     0,    27,   302,    23,    24,
      25,     0,   139,   305,   140,    28,   303,     0,    26,     0,
     304,     0,     0,   235,     0,     0,     0,   141,   238,   142,
       0,     0,   239,   143,   144,    27,     0,   240,   145,   241,
       0,   146,   305,    29,    28,     0,     0,   147,     0,   344,
     345,   346,   347,   148,   348,   349,     0,   238,     0,     0,
       0,   239,     0,     0,     0,     0,   240,     0,   241,   464,
       0,     0,    29,   224,    21,   225,   226,   227,     0,     0,
       0,     0,     0,   228,     0,     0,     0,     0,     0,   300,
     301,     0,     0,     0,     0,     0,     0,     0,   466,  -502,
      22,     0,   224,    21,   225,   226,   227,     0,     0,  -502,
       0,     0,   228,     0,   302,    23,    24,    25,   300,   301,
       0,     0,     0,   303,     0,    26,     0,   304,  -502,    22,
     235,   356,   357,   358,   359,   360,   361,   362,  -502,     0,
       0,     0,    27,   302,    23,    24,    25,     0,     0,   305,
       0,    28,   303,     0,    26,     0,   304,     0,     0,   235,
       0,     0,     0,     0,   238,     0,     0,     0,   239,     0,
       0,    27,     0,   240,     0,   241,     0,     0,   305,    29,
      28,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   238,     0,     0,     0,   239,     0,     0,
       0,     0,   240,     0,   241,   468,     0,     0,    29,   224,
      21,   225,   226,   227,     0,     0,     0,     0,     0,   228,
       0,     0,     0,     0,     0,   300,   301,     0,     0,     0,
       0,     0,     0,     0,   470,  -502,    22,     0,   224,    21,
     225,   226,   227,     0,     0,  -502,     0,     0,   228,     0,
     302,    23,    24,    25,   300,   301,     0,     0,     0,   303,
       0,    26,     0,   304,  -502,    22,   235,     0,     0,     0,
       0,     0,     0,     0,  -502,     0,     0,     0,    27,   302,
      23,    24,    25,     0,     0,   305,     0,    28,   303,     0,
      26,     0,   304,     0,     0,   235,     0,     0,     0,     0,
     238,     0,     0,     0,   239,     0,     0,    27,     0,   240,
       0,   241,     0,     0,   305,    29,    28,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   238,
       0,     0,     0,   239,     0,     0,     0,     0,   240,     0,
     241,   472,     0,     0,    29,   224,    21,   225,   226,   227,
       0,     0,     0,     0,     0,   228,     0,     0,     0,     0,
       0,   300,   301,     0,     0,     0,     0,     0,     0,     0,
     474,  -502,    22,     0,   224,    21,   225,   226,   227,     0,
       0,  -502,     0,     0,   228,     0,   302,    23,    24,    25,
     300,   301,     0,     0,     0,   303,     0,    26,     0,   304,
    -502,    22,   235,     0,     0,     0,     0,     0,     0,     0,
    -502,     0,     0,     0,    27,   302,    23,    24,    25,     0,
       0,   305,     0,    28,   303,     0,    26,     0,   304,     0,
       0,   235,     0,     0,     0,     0,   238,     0,     0,     0,
     239,     0,     0,    27,     0,   240,     0,   241,     0,     0,
     305,    29,    28,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   238,     0,     0,     0,   239,
       0,     0,     0,     0,   240,     0,   241,   477,     0,     0,
      29,   224,    21,   225,   226,   227,     0,     0,     0,     0,
       0,   228,     0,     0,     0,     0,     0,   300,   301,     0,
       0,     0,     0,     0,     0,     0,   479,  -502,    22,     0,
     224,    21,   225,   226,   227,     0,     0,  -502,     0,     0,
     228,     0,   302,    23,    24,    25,   300,   301,     0,     0,
       0,   303,     0,    26,     0,   304,  -502,    22,   235,     0,
       0,     0,     0,     0,     0,     0,  -502,     0,     0,     0,
      27,   302,    23,    24,    25,     0,     0,   305,     0,    28,
     303,     0,    26,     0,   304,     0,     0,   235,     0,     0,
       0,     0,   238,     0,     0,     0,   239,     0,     0,    27,
       0,   240,     0,   241,     0,     0,   305,    29,    28,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   238,     0,     0,     0,   239,     0,     0,     0,     0,
     240,     0,   241,   481,     0,     0,    29,   224,    21,   225,
     226,   227,     0,     0,     0,     0,     0,   228,     0,     0,
       0,     0,     0,   300,   301,     0,     0,     0,     0,     0,
       0,     0,   483,  -502,    22,     0,   224,    21,   225,   226,
     227,     0,     0,  -502,     0,     0,   228,     0,   302,    23,
      24,    25,   300,   301,     0,     0,     0,   303,     0,    26,
       0,   304,  -502,    22,   235,     0,     0,     0,     0,     0,
       0,     0,  -502,     0,     0,     0,    27,   302,    23,    24,
      25,     0,     0,   305,     0,    28,   303,     0,    26,     0,
     304,     0,     0,   235,     0,     0,     0,     0,   238,     0,
       0,     0,   239,     0,     0,    27,     0,   240,     0,   241,
       0,     0,   305,    29,    28,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   238,     0,     0,
       0,   239,     0,     0,     0,     0,   240,     0,   241,   485,
       0,     0,    29,   224,    21,   225,   226,   227,     0,     0,
       0,     0,     0,   228,     0,     0,     0,     0,     0,   300,
     301,     0,     0,     0,     0,     0,     0,     0,   487,  -502,
      22,     0,   224,    21,   225,   226,   227,     0,     0,  -502,
       0,     0,   228,     0,   302,    23,    24,    25,   300,   301,
       0,     0,     0,   303,     0,    26,     0,   304,  -502,    22,
     235,     0,     0,     0,     0,     0,     0,     0,  -502,     0,
       0,     0,    27,   302,    23,    24,    25,     0,     0,   305,
       0,    28,   303,     0,    26,     0,   304,     0,     0,   235,
       0,     0,     0,     0,   238,     0,     0,     0,   239,     0,
       0,    27,     0,   240,     0,   241,     0,     0,   305,    29,
      28,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   238,     0,     0,     0,   239,     0,     0,
       0,     0,   240,     0,   241,   489,     0,     0,    29,   224,
      21,   225,   226,   227,     0,     0,     0,     0,     0,   228,
       0,     0,     0,     0,     0,   300,   301,     0,     0,     0,
       0,     0,     0,     0,   491,  -502,    22,     0,   224,    21,
     225,   226,   227,     0,     0,  -502,     0,     0,   228,     0,
     302,    23,    24,    25,   300,   301,     0,     0,     0,   303,
       0,    26,     0,   304,  -502,    22,   235,     0,     0,     0,
       0,     0,     0,     0,  -502,     0,     0,     0,    27,   302,
      23,    24,    25,     0,     0,   305,     0,    28,   303,     0,
      26,     0,   304,     0,     0,   235,     0,     0,     0,     0,
     238,     0,     0,     0,   239,     0,     0,    27,     0,   240,
       0,   241,     0,     0,   305,    29,    28,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   238,
       0,     0,     0,   239,     0,     0,     0,     0,   240,     0,
     241,   493,     0,     0,    29,   224,    21,   225,   226,   227,
       0,     0,     0,     0,     0,   228,     0,     0,     0,     0,
       0,   300,   301,     0,     0,     0,     0,     0,     0,     0,
     495,  -502,    22,     0,   224,    21,   225,   226,   227,     0,
       0,  -502,     0,     0,   228,     0,   302,    23,    24,    25,
     300,   301,     0,     0,     0,   303,     0,    26,     0,   304,
    -502,    22,   235,     0,     0,     0,     0,     0,     0,     0,
    -502,     0,     0,     0,    27,   302,    23,    24,    25,     0,
       0,   305,     0,    28,   303,     0,    26,     0,   304,     0,
       0,   235,     0,     0,     0,     0,   238,   281,     0,     0,
     239,     0,     0,    27,     0,   240,     0,   241,  -638,  -638,
     305,    29,    28,     0,     0,     0,     0,     0,     0,  -638,
       0,     0,     0,   136,   137,   238,     0,     0,    22,   239,
       0,   138,     0,     0,   240,     0,   241,     0,     0,   139,
      29,   140,     0,    23,    24,    25,  -638,     0,     0,  -638,
    -638,   281,     0,  -638,   141,     0,   142,     0,     0,  -638,
     143,   144,  -638,     0,     0,   145,     0,     0,   146,  -638,
      27,  -638,     0,     0,   147,     0,     0,   136,   137,    28,
     148,     0,    22,     0,     0,   138,     0,     0,     0,     0,
       0,     0,     0,   139,     0,   140,     0,    23,    24,    25,
       0,    20,   296,     0,     0,     0,    21,    29,   141,     0,
     142,     0,     0,     0,   143,   144,     0,     0,     0,   145,
       0,     0,   146,     0,    27,  -641,  -641,     0,   147,     0,
       0,     0,    22,    28,   148,     0,  -641,     0,     0,     0,
     344,   345,   346,   347,     0,   348,   349,    23,    24,    25,
       0,     0,     0,   344,   345,   346,   347,    26,   348,   349,
     350,    29,     0,  -641,     0,     0,  -641,  -641,     0,     0,
    -641,     0,     0,     0,    27,     0,  -641,     0,     0,  -641,
       0,     0,     0,    28,  -539,     0,  -641,     0,  -641,  -539,
    -539,     0,  -539,  -539,  -539,  -539,     0,  -539,  -539,  -539,
    -539,     0,     0,     0,     0,  -539,  -539,  -539,     0,     0,
       0,    29,     0,     0,     0,     0,     0,  -539,     0,  -539,
    -539,   355,   356,   357,   358,   359,   360,   361,   362,   314,
       0,     0,   353,   354,   355,   356,   357,   358,   359,   360,
     361,   362,     0,     0,  -539,     0,     0,  -539,  -539,     0,
       0,  -539,  -645,  -645,  -645,     0,     0,  -539,     0,     0,
    -539,     0,     0,     0,  -645,     0,     0,  -539,     0,  -539,
    -539,  -539,  -539,  -539,  -539,  -539,  -539,  -539,  -539,  -539,
    -539,     0,  -539,  -539,  -539,  -539,  -539,  -539,  -539,  -539,
       0,  -645,   119,     0,  -645,  -645,     0,     0,  -645,     0,
       0,     0,     0,     0,  -645,     0,     0,  -645,     0,     0,
       0,     0,     0,     0,  -645,     0,  -645,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,  -645,
       0,  -645,  -645,   315,  -645,     0,  -645,   316,   224,    21,
     225,   226,   227,    60,     0,     0,     0,     0,   228,     0,
       0,     0,     0,    61,   229,   230,    64,   503,     0,     0,
     136,   137,    67,    68,     0,    22,    70,    71,   138,     0,
      73,     0,     0,    74,     0,    76,   139,    78,   140,   232,
      23,    24,    25,    83,    84,    85,    86,    87,   233,    89,
      26,   141,   234,   142,     0,   235,     0,   143,   144,     0,
       0,    96,   145,     0,    98,   146,   100,   236,   102,     0,
     103,   147,     0,     0,   237,   107,    28,   148,     0,     0,
     109,     0,     0,     0,     0,     0,     0,   504,     0,   238,
       0,     0,     0,   239,     0,     0,     0,     0,   240,     0,
     241,  -563,     0,     0,    29,   224,    21,   225,   226,   227,
      60,     0,     0,     0,     0,   228,     0,     0,     0,     0,
      61,   229,   230,    64,     0,     0,     0,   136,   137,    67,
      68,  -502,    22,    70,    71,   138,     0,    73,     0,     0,
      74,  -502,    76,   139,    78,   140,   232,    23,    24,    25,
      83,    84,    85,    86,    87,   233,    89,    26,   141,   234,
     142,     0,   235,     0,   143,   144,     0,     0,    96,   145,
       0,    98,   146,   100,   236,   102,     0,   103,   147,     0,
       0,   237,   107,    28,   148,     0,     0,   109,     0,     0,
       0,     0,     0,     0,     0,     0,   238,     0,     0,     0,
     239,     0,     0,     0,     0,   240,     0,   241,     0,     0,
       0,    29,   224,    21,   225,   226,   227,    60,     0,     0,
       0,     0,   228,     0,     0,     0,     0,    61,   229,   230,
      64,   664,     0,     0,   136,   137,    67,    68,     0,    22,
      70,    71,   138,     0,    73,     0,     0,    74,     0,    76,
     139,    78,   140,   232,    23,    24,    25,    83,    84,    85,
      86,    87,   233,    89,    26,   141,   234,   142,     0,   235,
       0,   143,   144,     0,     0,    96,   145,     0,    98,   146,
     100,   236,   102,     0,   103,   147,     0,     0,   237,   107,
      28,   148,     0,     0,   109,     0,     0,     0,     0,     0,
       0,     0,     0,   238,   224,    21,   225,   239,     0,    60,
       0,     0,   240,     0,   241,     0,     0,     0,    29,    61,
     229,   230,    64,     0,     0,     0,   136,   137,    67,    68,
       0,    22,    70,    71,   138,     0,    73,     0,     0,    74,
       0,    76,   139,    78,   140,   232,    23,    24,    25,    83,
      84,    85,    86,    87,   233,    89,    26,   141,   234,   142,
       0,   235,     0,   143,   144,     0,     0,    96,   145,     0,
      98,   146,   100,   236,   102,     0,   103,   147,     0,     0,
     237,   107,    28,   148,     0,     0,   109,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   224,    21,   225,
     226,   227,    60,     0,   240,     0,   241,   228,     0,     0,
      29,     0,    61,   229,   230,    64,     0,     0,     0,     0,
       0,    67,    68,  -502,    22,    70,    71,     0,     0,    73,
       0,     0,    74,  -502,    76,     0,    78,     0,   232,    23,
      24,    25,    83,    84,    85,    86,    87,   233,    89,    26,
       0,   234,     0,     0,   235,     0,     0,     0,     0,     0,
      96,     0,     0,    98,     0,   100,   236,   102,     0,   103,
       0,     0,     0,   237,   107,    28,     0,     0,     0,   109,
       0,     0,     0,     0,     0,     0,     0,     0,   238,     0,
       0,     0,   239,     0,     0,     0,     0,   240,     0,   241,
       0,     0,   429,    29,   224,    21,   225,   226,   227,    60,
       0,     0,     0,     0,   228,     0,     0,     0,     0,    61,
     229,   230,    64,     0,     0,     0,     0,     0,    67,    68,
       0,    22,    70,    71,     0,     0,    73,     0,     0,    74,
       0,    76,     0,    78,     0,   232,    23,    24,    25,    83,
      84,    85,    86,    87,   233,    89,    26,     0,   234,     0,
       0,   235,     0,     0,     0,     0,     0,    96,     0,     0,
      98,     0,   100,   236,   102,     0,   103,     0,     0,     0,
     237,   107,    28,     0,     0,     0,   109,     0,     0,     0,
       0,   224,    21,   225,     0,   238,     0,     0,     0,   239,
       0,     0,     0,     0,   240,     0,   241,   300,   301,     0,
      29,   224,    21,   225,   226,   227,     0,     0,    22,     0,
       0,   228,     0,     0,     0,     0,     0,   300,   301,     0,
       0,     0,   302,    23,    24,    25,     0,     0,    22,     0,
       0,   303,     0,    26,     0,   304,     0,     0,   235,     0,
       0,     0,   302,    23,    24,    25,     0,     0,     0,     0,
      27,   303,     0,    26,     0,   304,     0,   305,   235,    28,
     365,     0,     0,     0,     0,   366,   367,     0,     0,     0,
      27,     0,     0,     0,     0,     0,     0,   305,     0,    28,
       0,   240,   368,   369,     0,     0,     0,    29,     0,     0,
       0,     0,   238,   370,     0,     0,   239,     0,     0,     0,
       0,   240,     0,   241,     0,     0,   365,    29,     0,     0,
       0,   366,   367,     0,     0,     0,     0,     0,     0,     0,
     371,     0,     0,   372,   373,     0,     0,   374,   368,   369,
       0,     0,     0,   375,     0,     0,   376,     0,     0,   370,
       0,     0,     0,   377,     0,   378,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  -596,   240,     0,   241,  -596,   371,     0,     0,   372,
     373,     0,     0,   374,     0,     0,     0,     0,     0,   375,
       0,     0,   376,     0,     0,     0,     0,     0,     0,   377,
       0,   378,   515,     0,     0,     0,     0,     0,     0,     0,
       0,    60,     0,     0,     0,     0,     0,     0,   240,     0,
     241,    61,    62,    63,    64,  -340,     0,     0,    65,    66,
      67,    68,    69,     0,    70,    71,    72,     0,    73,     0,
       0,    74,    75,    76,    77,    78,    79,    80,     0,   212,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,     0,     0,     0,    94,    95,     0,     0,    96,
      97,     0,    98,    99,   100,   101,   102,     0,   103,   104,
       0,   105,   106,   107,     0,   108,     0,     0,   109,   344,
     345,   346,   347,     0,   348,   349,   350,   351,     0,     0,
       0,     0,     0,     0,  -340,  -340,   344,   345,   346,   347,
       0,   348,   349,     0,     0,   344,   345,   346,   347,     0,
     348,   349,   461,   463,   465,   467,   469,   471,   473,   475,
       0,   478,   480,   482,   484,   486,   488,   490,   492,   494,
     496,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,     0,     0,
       0,     0,     0,     0,     0,   353,   354,   355,   356,   357,
     358,   359,   360,   361,   362,   354,   355,   356,   357,   358,
     359,   360,   361,   362,    60,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    61,    62,    63,    64,     0,     0,
       0,    65,    66,    67,    68,    69,     0,    70,    71,    72,
       0,    73,   521,   522,    74,    75,    76,    77,    78,    79,
      80,     0,     0,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,     0,     0,     0,    94,    95,
       0,     0,    96,    97,     0,    98,    99,   100,   101,   102,
       0,   103,   104,     0,   105,   106,   107,    60,   108,     0,
       0,   109,     0,     0,     0,     0,     0,    61,    62,    63,
      64,   409,     0,     0,    65,    66,    67,    68,    69,     0,
      70,    71,    72,     0,    73,     0,     0,    74,    75,    76,
      77,    78,    79,    80,     0,   212,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,     0,     0,
       0,    94,    95,     0,     0,    96,    97,     0,    98,    99,
     100,   101,   102,     0,   103,   104,     0,   105,   106,   107,
      60,   108,     0,     0,   109,     0,     0,     0,     0,     0,
      61,    62,    63,    64,   213,     0,     0,    65,    66,    67,
      68,    69,     0,    70,    71,    72,     0,    73,     0,     0,
      74,    75,    76,    77,    78,    79,    80,     0,   408,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,     0,     0,     0,    94,    95,     0,     0,    96,    97,
       0,    98,    99,   100,   101,   102,     0,   103,   104,     0,
     105,   106,   107,    60,   108,     0,     0,   109,     0,     0,
       0,     0,     0,    61,    62,    63,    64,   409,     0,     0,
      65,    66,    67,    68,    69,     0,    70,    71,    72,     0,
      73,     0,     0,    74,    75,    76,    77,    78,    79,    80,
       0,   697,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,     0,     0,     0,    94,    95,     0,
       0,    96,    97,     0,    98,    99,   100,   101,   102,     0,
     103,   104,     0,   105,   106,   107,    60,   108,     0,     0,
     109,     0,     0,     0,     0,     0,    61,    62,    63,    64,
     409,     0,     0,    65,    66,    67,    68,    69,     0,    70,
      71,    72,     0,    73,     0,     0,    74,    75,    76,    77,
      78,    79,    80,     0,   704,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,     0,     0,     0,
      94,    95,     0,     0,    96,    97,     0,    98,    99,   100,
     101,   102,     0,   103,   104,     0,   105,   106,   107,    60,
     108,     0,     0,   109,     0,     0,     0,     0,     0,    61,
      62,    63,    64,   409,     0,     0,    65,    66,    67,    68,
      69,     0,    70,    71,    72,     0,    73,     0,     0,    74,
      75,    76,    77,    78,    79,    80,     0,   847,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
       0,     0,     0,    94,    95,     0,     0,    96,    97,     0,
      98,    99,   100,   101,   102,     0,   103,   104,     0,   105,
     106,   107,   595,   108,     0,   596,   109,     0,     0,     0,
       0,   597,     0,     0,     0,     0,   409,     0,     0,     0,
       0,   598,   599,   600,   601,     0,     0,     0,   602,   603,
     604,   605,   606,     0,   607,   608,   609,     0,   610,     0,
       0,   611,   612,   613,   614,   615,   616,   617,   618,   619,
     620,   621,   622,   623,   624,   625,   626,   627,   628,   629,
     630,   631,     0,     0,     0,   632,   633,     0,     0,   634,
     635,   120,   636,   637,   638,   639,   640,     0,   641,   642,
      60,   643,   644,   645,     0,   646,   647,     0,   648,     0,
      61,    62,    63,    64,     0,     0,     0,    65,    66,    67,
      68,    69,     0,    70,    71,    72,     0,    73,     0,     0,
      74,    75,    76,    77,    78,    79,    80,     0,   121,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,     0,     0,     0,    94,    95,     0,     0,    96,    97,
     126,    98,    99,   100,   101,   102,     0,   103,   104,    60,
     105,   106,   107,     0,   108,     0,     0,   109,     0,    61,
      62,    63,    64,     0,     0,     0,    65,    66,    67,    68,
      69,     0,    70,    71,    72,     0,    73,     0,     0,    74,
      75,    76,    77,    78,    79,    80,     0,   127,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
       0,     0,     0,    94,    95,     0,     0,    96,    97,   167,
      98,    99,   100,   101,   102,     0,   103,   104,    60,   105,
     106,   107,     0,   108,     0,     0,   109,     0,    61,    62,
      63,    64,     0,     0,     0,    65,    66,    67,    68,    69,
       0,    70,    71,    72,     0,    73,     0,     0,    74,    75,
      76,    77,    78,    79,    80,     0,   168,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,     0,
       0,     0,    94,    95,     0,     0,    96,    97,   265,    98,
      99,   100,   101,   102,     0,   103,   104,    60,   105,   106,
     107,     0,   108,     0,     0,   109,     0,    61,    62,    63,
      64,     0,     0,     0,    65,    66,    67,    68,    69,     0,
      70,    71,    72,     0,    73,     0,     0,    74,    75,    76,
      77,    78,    79,    80,     0,   127,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,     0,     0,
       0,    94,    95,     0,     0,    96,    97,   273,    98,    99,
     100,   101,   102,     0,   103,   104,    60,   105,   106,   107,
       0,   108,     0,     0,   109,     0,    61,    62,    63,    64,
       0,     0,     0,    65,    66,    67,    68,    69,     0,    70,
      71,    72,     0,    73,     0,     0,    74,    75,    76,    77,
      78,    79,    80,     0,   274,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,     0,     0,     0,
      94,    95,     0,     0,    96,    97,   856,    98,    99,   100,
     101,   102,     0,   103,   104,    60,   105,   106,   107,     0,
     108,     0,     0,   109,     0,    61,    62,    63,    64,     0,
       0,     0,    65,    66,    67,    68,    69,     0,    70,    71,
      72,     0,    73,     0,     0,    74,    75,    76,    77,    78,
      79,    80,     0,   857,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,     0,     0,     0,    94,
      95,     0,     0,    96,    97,   911,    98,    99,   100,   101,
     102,     0,   103,   104,    60,   105,   106,   107,     0,   108,
       0,     0,   109,     0,    61,    62,    63,    64,     0,     0,
       0,    65,    66,    67,    68,    69,     0,    70,    71,    72,
       0,    73,     0,     0,    74,    75,    76,    77,    78,    79,
      80,     0,   857,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,     0,     0,     0,    94,    95,
       0,     0,    96,    97,     0,    98,    99,   100,   101,   102,
       0,   103,   104,     0,   105,   106,   107,     0,   108,   136,
     137,   109,     0,     0,    22,     0,     0,   138,     0,     0,
       0,     0,     0,     0,     0,   139,     0,   140,     0,    23,
      24,    25,     0,     0,     0,     0,     0,     0,     0,     0,
     141,     0,   142,     0,     0,     0,   143,   144,     0,     0,
       0,   145,     0,     0,   146,     0,    27,     0,     0,     0,
     147,     0,     0,   136,   137,    28,   148,     0,    22,     0,
       0,   138,     0,     0,     0,     0,     0,     0,     0,   139,
       0,   140,     0,    23,    24,    25,   393,     0,     0,     0,
       0,     0,     0,    29,   141,     0,   142,     0,     0,     0,
     143,   144,     0,     0,     0,   145,     0,     0,   146,     0,
      27,     0,     0,     0,   147,     0,     0,   136,   156,    28,
     148,     0,    22,     0,     0,   157,     0,     0,     0,     0,
       0,     0,     0,   139,     0,   140,     0,    23,    24,    25,
       0,     0,     0,     0,     0,     0,     0,    29,   141,     0,
     142,     0,     0,     0,   143,   144,     0,     0,     0,   145,
       0,     0,   146,     0,    27,     0,     0,     0,   147,     0,
       0,     0,    60,    28,   148,     0,     0,     0,     0,     0,
       0,     0,    61,    62,    63,    64,     0,     0,     0,    65,
      66,    67,    68,    69,     0,    70,    71,    72,     0,    73,
       0,    29,    74,    75,    76,    77,    78,    79,    80,     0,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,     0,     0,     0,    94,    95,     0,     0,
      96,    97,     0,    98,    99,   100,   101,   102,     0,   103,
     104,    60,   105,   106,   107,     0,   108,     0,     0,   109,
       0,    61,    62,    63,    64,     0,     0,     0,    65,    66,
      67,    68,    69,     0,    70,    71,    72,     0,    73,     0,
       0,    74,    75,    76,    77,    78,    79,    80,     0,   163,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,     0,     0,     0,    94,    95,     0,     0,    96,
      97,     0,    98,    99,   100,   101,   102,     0,   103,   104,
      60,   105,   106,   107,     0,   108,     0,     0,   109,     0,
      61,    62,    63,    64,     0,     0,     0,    65,    66,    67,
      68,    69,     0,    70,    71,    72,     0,    73,     0,     0,
      74,    75,    76,    77,    78,    79,    80,     0,   165,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,     0,     0,     0,    94,    95,     0,     0,    96,    97,
       0,    98,    99,   100,   101,   102,     0,   103,   104,    60,
     105,   106,   107,     0,   108,     0,     0,   109,     0,    61,
      62,    63,    64,     0,     0,     0,    65,    66,    67,    68,
      69,     0,    70,    71,    72,     0,    73,     0,     0,    74,
      75,    76,    77,    78,    79,    80,     0,   181,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
       0,     0,     0,    94,    95,     0,     0,    96,    97,     0,
      98,    99,   100,   101,   102,     0,   103,   104,    60,   105,
     106,   107,     0,   108,     0,     0,   109,     0,    61,    62,
      63,    64,     0,     0,     0,    65,    66,    67,    68,    69,
       0,    70,    71,    72,     0,    73,     0,     0,    74,    75,
      76,    77,    78,    79,    80,     0,   425,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,     0,
       0,     0,    94,    95,     0,     0,    96,    97,     0,    98,
      99,   100,   101,   102,     0,   103,   104,    60,   105,   106,
     107,     0,   108,     0,     0,   109,     0,    61,    62,    63,
      64,     0,     0,     0,    65,    66,    67,    68,    69,     0,
      70,    71,    72,     0,    73,     0,     0,    74,    75,    76,
      77,    78,    79,    80,     0,   552,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,     0,     0,
       0,    94,    95,     0,     0,    96,    97,     0,    98,    99,
     100,   101,   102,     0,   103,   104,    60,   105,   106,   107,
       0,   108,     0,     0,   109,     0,    61,    62,    63,    64,
       0,     0,     0,    65,    66,    67,    68,    69,     0,    70,
      71,    72,     0,    73,     0,     0,    74,    75,    76,    77,
      78,    79,    80,     0,   212,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,     0,     0,     0,
      94,    95,     0,     0,    96,    97,     0,    98,    99,   100,
     101,   102,     0,   103,   104,     0,   105,   106,   107,     0,
     108,     0,     0,   109
};

static const yytype_int16 yycheck[] =
{
       6,     2,   242,   243,    37,    22,   132,     6,    40,   155,
      42,    40,   513,    42,    31,   457,   263,   736,    34,    36,
     226,   227,   228,   216,   226,   227,   508,   221,   352,   552,
      24,   232,   238,   239,    40,    24,    42,   243,   244,   245,
      22,   243,   761,   242,   243,   283,   590,    32,   198,    31,
      61,   326,     1,   255,    36,   747,   174,   295,     1,   177,
       1,    88,   180,    38,    39,   772,     0,   831,   218,   309,
      48,     1,     1,   765,     1,    24,   195,   333,   105,     1,
     199,   713,   846,    24,   791,    97,   754,   327,     1,    48,
     102,   759,    32,   333,    24,   106,    85,    24,   115,   116,
     219,   302,   119,     1,   379,   307,   308,   101,    32,   311,
     309,     1,   313,   177,   131,   132,   180,   102,     1,   125,
     752,   327,    97,    32,     1,     1,    24,   333,   327,   135,
       1,   333,   149,   115,   116,   337,   135,   119,   344,   345,
     346,   347,   348,   349,   350,   351,   352,   353,   354,   355,
     356,   357,   358,   359,   360,   361,   362,   106,   396,   703,
     109,   104,   179,   104,   283,   106,   195,   149,   109,   288,
     199,    40,   189,    42,   101,   818,   105,   820,   194,   109,
     109,   704,   109,   105,     3,   441,   829,   216,   907,   195,
     219,    24,   105,   199,   312,   433,   109,   179,     1,   198,
     447,   441,   235,   321,   322,     1,   444,   189,   326,   327,
     216,   109,   459,   219,   243,   105,   217,   457,   412,   218,
       1,   104,   865,   106,   418,     7,   243,     3,   105,   105,
     731,   732,     8,     9,   105,   441,     1,   243,     1,   441,
       6,     1,   441,    32,   446,   796,   262,     4,   101,   800,
     101,   457,   743,   104,   283,   457,     1,   450,   111,   288,
     378,   379,   380,   381,   382,   383,   757,    97,     6,    24,
     594,   390,   105,   102,    31,   513,   106,   283,   277,   315,
     297,    48,   288,   289,    41,    97,   105,   406,   107,    24,
     289,    57,    95,    24,   937,    52,   108,   101,   327,    95,
     104,   104,   319,   106,   333,   948,    61,     1,   104,   315,
     327,   429,     6,    48,    96,   297,   333,   960,   800,    57,
     871,   327,   111,    80,   105,    88,   195,   333,    88,   105,
     199,   107,   365,   451,    32,   101,    24,   319,    32,    32,
     105,   102,   105,   104,   380,   105,   101,   216,   103,   900,
     219,   106,   109,    47,    48,    49,   101,   102,     1,    24,
      24,   390,   242,    57,     7,    88,   101,    97,   385,    97,
     386,    88,   103,   104,   380,   926,   106,   406,   106,   930,
      74,   932,   933,    24,   390,    38,    39,   101,   594,    83,
     872,   424,    32,   774,   775,    38,    39,   111,   949,     1,
     406,    31,   419,   385,     6,    24,    24,    47,    48,    49,
     105,    41,   441,   101,   283,   103,   534,   111,   106,   288,
     108,   450,   105,    97,   441,   543,   306,   104,   457,   106,
      32,   872,   106,   671,    74,   441,   101,   419,   103,   103,
     457,   106,   106,    83,   450,    47,    48,    49,    24,   102,
     568,   457,    97,    96,   101,    57,    24,   104,   899,   105,
     101,   106,   103,   101,   582,   106,   104,   585,   504,   716,
      88,   111,    74,     6,   592,   593,   917,     1,   228,   105,
     228,    83,   101,   101,   103,   103,    24,   106,   238,   239,
     238,   239,   242,   243,   244,   245,   244,   245,   504,    32,
     105,    25,    26,   945,   946,   255,   105,    88,   746,   111,
      90,   102,    36,   104,    47,    48,    49,    38,    39,   536,
     101,   390,   964,   104,    57,   101,   102,   103,    38,    39,
     676,   548,   111,   101,   105,   103,   730,   406,   106,    63,
       4,    74,    66,    67,    16,    17,    70,    97,    98,    99,
      83,   109,    76,   101,   536,    79,   306,   307,   308,    24,
     678,   311,    86,   101,    88,   103,   548,   686,   106,    95,
     689,    24,   773,   105,   692,    24,   101,   327,   111,    24,
     104,   450,   105,   333,   108,    32,   105,   337,    88,   707,
     875,   792,   710,    24,   344,   345,   346,   347,   348,   349,
     350,   351,   352,   353,   354,   355,   356,   357,   358,   359,
     360,   361,   362,   106,   678,    24,   106,    12,    13,   904,
     739,    16,    17,    95,    96,    97,    98,    99,   731,   732,
     831,     7,   668,   751,    90,   753,   101,   669,   103,    88,
     669,   106,    48,   707,   104,   846,   710,   108,   101,   106,
     103,   777,   101,   106,   103,    24,   101,   686,   103,   944,
     689,   105,   668,   669,    97,   950,   951,   696,   104,   106,
     101,   106,   103,   958,   106,   936,   104,   104,   939,   940,
     686,   106,   967,   689,   103,   886,   947,   972,     1,   104,
     696,   441,   101,   726,   103,   108,   446,    97,    93,    94,
      95,    96,    97,    98,    99,   945,   946,   457,    24,   970,
     739,   108,    25,    26,   743,    95,    96,    97,    98,    99,
     102,   738,   106,    36,   964,   761,    88,    24,   757,   108,
     108,   102,   101,   739,   103,   736,     7,   743,   106,   945,
     946,    38,    39,   945,   946,   774,   775,   105,   765,   104,
      63,   757,   104,    66,    67,   761,   738,    70,   964,    88,
     777,   106,   964,    76,   108,   106,    79,   109,   774,   775,
     803,   889,   789,    86,   892,    88,   106,   895,   795,    88,
      88,    88,   105,   765,   108,   101,   106,   103,   106,   106,
     796,   105,    48,   105,   800,   105,    88,    88,    88,   105,
     669,   807,   102,    88,   101,   102,   103,   789,   807,   927,
     105,   105,   105,   795,    40,   101,   105,   686,   105,   289,
     689,   886,    25,    26,   669,   889,   179,   696,   892,   412,
     763,   895,   147,    36,   867,   868,   709,   894,   142,   875,
     884,   146,   254,   876,   594,   878,   879,   801,   149,   738,
     254,   946,   964,   966,   668,   568,   585,   658,   313,    -1,
      63,    -1,    -1,    66,    67,   871,    -1,    70,   904,   875,
     739,   907,    -1,    76,   743,    -1,    79,   894,    42,    -1,
      -1,    -1,    -1,    86,    -1,    88,    -1,    -1,   757,    -1,
      54,    55,    56,    -1,   900,    -1,    -1,    -1,   904,   102,
      -1,   907,    -1,    -1,    68,   774,   775,    71,   944,    73,
      -1,    75,   894,    77,   950,   951,   945,   946,    82,    -1,
     926,    -1,   958,    87,   930,   931,   932,   933,   945,   946,
      -1,   967,    -1,    -1,    -1,   964,   972,    -1,   944,   945,
     946,    -1,    -1,   949,   950,   951,    -1,   964,    -1,     0,
       1,    -1,   958,     4,    -1,    -1,    -1,    -1,   964,    -1,
     966,   967,    -1,    -1,    -1,    -1,   972,    -1,    -1,    -1,
      -1,    -1,    -1,    24,    -1,    -1,    27,    28,    -1,    -1,
      31,    32,    -1,    -1,    35,    -1,    -1,    -1,    -1,    -1,
      41,    42,    43,    -1,    45,    -1,    47,    48,    49,    -1,
      51,    52,    53,    54,    55,    56,    -1,    58,    -1,    60,
      -1,    -1,    -1,    64,    65,    -1,    -1,    68,    69,    -1,
      71,    72,    73,    74,    75,    -1,    77,    78,    -1,    80,
      -1,    82,    83,    84,    -1,    -1,    87,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     1,    -1,    -1,
     101,     5,     6,     7,     8,     9,    10,    -1,   109,    -1,
     111,    15,    -1,    -1,    -1,    -1,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    -1,    -1,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    -1,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    -1,    81,    82,    83,
      84,    -1,    86,    87,    88,    -1,    -1,    25,    26,    -1,
      -1,    -1,    96,    -1,    -1,    -1,   100,   101,    36,    -1,
      -1,   105,    -1,   107,    -1,    -1,    -1,   111,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    63,    -1,    -1,    66,    67,
      -1,    -1,    70,    -1,    -1,    -1,    -1,    -1,    76,    -1,
      -1,    79,    -1,    -1,    -1,   945,   946,     1,    86,    -1,
      88,     5,     6,     7,     8,     9,    10,    -1,    -1,    -1,
      -1,    15,    -1,    -1,   964,    -1,    20,    21,    22,    23,
      -1,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    -1,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    -1,    81,    82,    83,
      84,    -1,    86,    87,    88,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    96,    -1,    -1,    -1,   100,    -1,    -1,    -1,
      -1,   105,    -1,   107,    -1,     1,    -1,   111,     4,     5,
       6,     7,     8,     9,    10,    -1,    -1,    -1,    -1,    15,
      -1,    -1,    -1,    -1,    20,    21,    22,    23,    24,    -1,
      -1,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      -1,    37,    -1,    -1,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    -1,    62,    -1,    64,    65,
      -1,    -1,    68,    69,    -1,    71,    72,    73,    74,    75,
      -1,    77,    78,    -1,    -1,    81,    82,    83,    84,    85,
      -1,    87,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      96,    -1,    -1,    -1,   100,   101,    -1,   103,    -1,   105,
      -1,   107,     1,   109,    -1,   111,     5,     6,     7,     8,
       9,    10,    -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,
      -1,    20,    21,    22,    23,    24,    -1,    -1,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    -1,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    -1,    62,    -1,    64,    65,    -1,    -1,    68,
      69,    -1,    71,    72,    73,    74,    75,    -1,    77,    78,
      -1,    -1,    81,    82,    83,    84,    -1,    -1,    87,    -1,
      -1,    -1,    -1,    -1,    -1,    94,    -1,    96,    97,    -1,
      -1,   100,    -1,    -1,    -1,    -1,   105,    -1,   107,    -1,
       1,    -1,   111,     4,     5,     6,     7,     8,     9,    10,
      -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,    20,
      21,    22,    23,    -1,    -1,    -1,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    -1,    37,    -1,    -1,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      -1,    62,    -1,    64,    65,    -1,    -1,    68,    69,    -1,
      71,    72,    73,    74,    75,    -1,    77,    78,    -1,    -1,
      81,    82,    83,    84,    85,    -1,    87,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,    -1,   100,
     101,    -1,    -1,    -1,   105,    -1,   107,     1,   109,    -1,
     111,     5,     6,     7,     8,     9,    10,    -1,    -1,    -1,
      -1,    15,    -1,    -1,    -1,    -1,    20,    21,    22,    23,
      24,    -1,    -1,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    -1,    37,    -1,    -1,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    -1,    62,    -1,
      64,    65,    -1,    -1,    68,    69,    -1,    71,    72,    73,
      74,    75,    -1,    77,    78,    -1,    -1,    81,    82,    83,
      84,    -1,    -1,    87,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    96,    -1,    -1,    -1,   100,   101,   102,   103,
      -1,   105,    -1,   107,     1,    -1,    -1,   111,     5,     6,
       7,     8,     9,    10,    -1,    -1,    -1,    -1,    15,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    -1,    -1,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    -1,
      37,    -1,    -1,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    -1,    62,    -1,    64,    65,    -1,
      -1,    68,    69,    -1,    71,    72,    73,    74,    75,    -1,
      77,    78,    -1,    -1,    81,    82,    83,    84,    -1,    -1,
      87,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    96,
      -1,    -1,    -1,   100,    -1,    -1,   103,    -1,   105,   106,
     107,     1,    -1,    -1,   111,     5,     6,     7,     8,     9,
      10,    -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,
      20,    21,    22,    23,    24,    -1,    -1,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    -1,    37,    -1,    -1,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    -1,    62,    -1,    64,    65,    -1,    -1,    68,    69,
      -1,    71,    72,    73,    74,    75,    -1,    77,    78,    -1,
      -1,    81,    82,    83,    84,    -1,    -1,    87,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,    -1,
     100,   101,    -1,    -1,    -1,   105,    -1,   107,     1,    -1,
      -1,   111,     5,     6,     7,     8,     9,    10,    -1,    -1,
      -1,    -1,    15,    -1,    -1,    -1,    -1,    20,    21,    22,
      23,    24,    -1,    -1,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    -1,    37,    -1,    -1,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    -1,    62,
      -1,    64,    65,    -1,    -1,    68,    69,    -1,    71,    72,
      73,    74,    75,    -1,    77,    78,    -1,    -1,    81,    82,
      83,    84,    -1,    -1,    87,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    96,    -1,    -1,    -1,   100,   101,    -1,
      -1,    -1,   105,    -1,   107,     1,    -1,    -1,   111,     5,
       6,     7,     8,     9,    10,    -1,    -1,    -1,    -1,    15,
      -1,    -1,    -1,    -1,    20,    21,    22,    23,    -1,    -1,
      -1,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      -1,    37,    -1,    -1,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    -1,    62,    -1,    64,    65,
      -1,    -1,    68,    69,    -1,    71,    72,    73,    74,    75,
      -1,    77,    78,    -1,    -1,    81,    82,    83,    84,    -1,
      -1,    87,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      96,    -1,    -1,    -1,   100,    -1,    -1,    -1,    -1,   105,
      -1,   107,     1,   109,    -1,   111,     5,     6,     7,     8,
       9,    10,    -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,
      -1,    20,    21,    22,    23,    -1,    -1,    -1,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    -1,    37,    -1,
      -1,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    -1,    62,    -1,    64,    65,    -1,    -1,    68,
      69,    -1,    71,    72,    73,    74,    75,    -1,    77,    78,
      -1,    -1,    81,    82,    83,    84,    -1,    -1,    87,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,
      -1,   100,    -1,    -1,    -1,    -1,   105,    -1,   107,     1,
      -1,    -1,   111,     5,     6,     7,     8,     9,    10,    -1,
      -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,    20,    21,
      22,    23,    -1,    -1,    -1,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    -1,    37,    -1,    -1,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    -1,
      62,    -1,    64,    65,    -1,    -1,    68,    69,    -1,    71,
      72,    73,    74,    75,    -1,    77,    78,    -1,    -1,    81,
      82,    83,    84,    -1,    -1,    87,    -1,    -1,    -1,    -1,
      -1,     1,    -1,    -1,    96,     5,     6,     7,   100,    -1,
      10,    -1,    -1,   105,    -1,   107,    -1,    -1,    -1,   111,
      20,    21,    22,    23,    24,    -1,    -1,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    -1,    37,    -1,    -1,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    -1,    62,    -1,    64,    65,    -1,    -1,    68,    69,
      -1,    71,    72,    73,    74,    75,    -1,    77,    78,    -1,
      -1,    81,    82,    83,    84,    -1,    -1,    87,    -1,    -1,
      -1,    -1,    -1,     1,    -1,    -1,    -1,     5,     6,     7,
      -1,    -1,    10,   103,    -1,   105,   106,   107,    -1,    -1,
      -1,   111,    20,    21,    22,    23,    -1,    -1,    -1,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    -1,    37,
      -1,    -1,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    -1,    62,    -1,    64,    65,    -1,    -1,
      68,    69,    -1,    71,    72,    73,    74,    75,    -1,    77,
      78,    -1,    -1,    81,    82,    83,    84,    -1,    -1,    87,
      -1,    -1,    -1,    -1,    -1,     1,    -1,    -1,    -1,     5,
       6,     7,    -1,   101,    10,    -1,    -1,   105,    -1,   107,
      -1,    -1,    -1,   111,    20,    21,    22,    23,    -1,    -1,
      -1,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      -1,    37,    -1,    -1,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    -1,    62,    -1,    64,    65,
      -1,    -1,    68,    69,    -1,    71,    72,    73,    74,    75,
      -1,    77,    78,    -1,    -1,    81,    82,    83,    84,    -1,
      -1,    87,    -1,    -1,    -1,    -1,    -1,    -1,     1,    -1,
      -1,    -1,     5,     6,     7,     8,     9,    10,    -1,   105,
      -1,   107,    15,    -1,    -1,   111,    -1,    20,    21,    22,
      23,    -1,    -1,    -1,    -1,    -1,    29,    30,    31,    32,
      33,    34,    -1,    -1,    37,    -1,    -1,    40,    41,    42,
      -1,    44,    -1,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    -1,    59,    -1,    61,    62,
      -1,    -1,    -1,    -1,    -1,    68,    -1,    -1,    71,    -1,
      73,    74,    75,    -1,    77,    -1,    -1,    -1,    81,    82,
      83,    -1,    -1,    -1,    87,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    96,    -1,    -1,    -1,   100,    -1,    -1,
      -1,    -1,   105,   106,   107,     1,    -1,   110,   111,     5,
       6,     7,     8,     9,    10,    -1,    -1,    -1,    -1,    15,
      -1,    -1,    -1,    -1,    20,    21,    22,    23,    -1,    -1,
      -1,    -1,    -1,    29,    30,    31,    32,    33,    34,    -1,
      -1,    37,    -1,    -1,    40,    41,    42,    -1,    44,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    -1,    59,    -1,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    68,    -1,    -1,    71,    -1,    73,    74,    75,
      -1,    77,    -1,    -1,    -1,    81,    82,    83,    -1,    -1,
      -1,    87,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      96,    -1,    -1,    -1,   100,    -1,    -1,    -1,    -1,   105,
     106,   107,     1,    -1,   110,   111,     5,     6,     7,     8,
       9,    10,    -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,
      -1,    20,    21,    22,    23,    -1,    -1,    -1,    -1,    -1,
      29,    30,    31,    32,    33,    34,    -1,    -1,    37,    -1,
      -1,    40,    41,    42,    -1,    44,    -1,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    -1,
      59,    -1,    -1,    62,    -1,    -1,    -1,    -1,    -1,    68,
      -1,    -1,    71,    -1,    73,    74,    75,    -1,    77,    -1,
      -1,    -1,    81,    82,    83,    -1,    -1,    -1,    87,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,
      -1,   100,    -1,    -1,    -1,    -1,   105,   106,   107,   108,
       1,    -1,   111,    -1,     5,     6,     7,     8,     9,    10,
      -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,    20,
      21,    22,    23,    24,    -1,    -1,    -1,    -1,    29,    30,
      31,    32,    33,    34,    -1,    -1,    37,    -1,    -1,    40,
      41,    42,    -1,    44,    -1,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    -1,    59,    -1,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    68,    -1,    -1,
      71,    -1,    73,    74,    75,    -1,    77,    -1,    -1,    -1,
      81,    82,    83,    -1,    -1,    -1,    87,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,    -1,   100,
      -1,    -1,    -1,    -1,   105,    -1,   107,     1,    -1,    -1,
     111,     5,     6,     7,     8,     9,    10,    -1,    -1,    -1,
      -1,    15,    -1,    -1,    -1,    -1,    20,    21,    22,    23,
      -1,    -1,    -1,    -1,    -1,    29,    30,    31,    32,    33,
      34,    -1,    -1,    37,    -1,    -1,    40,    41,    42,    -1,
      44,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    -1,    59,    -1,    -1,    62,    -1,
      -1,    -1,    -1,    -1,    68,    -1,    -1,    71,    -1,    73,
      74,    75,    -1,    77,    -1,    -1,    -1,    81,    82,    83,
      -1,    -1,    -1,    87,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    96,    -1,    -1,    -1,   100,    -1,    -1,    -1,
      -1,   105,    -1,   107,     1,   109,    -1,   111,     5,     6,
       7,     8,     9,    10,    -1,    -1,    -1,    -1,    15,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    -1,    -1,
      -1,    -1,    29,    30,    31,    32,    33,    34,    -1,    -1,
      37,    -1,    -1,    40,    41,    42,    -1,    44,    -1,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    -1,    59,    -1,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    68,    -1,    -1,    71,    -1,    73,    74,    75,    -1,
      77,    -1,    -1,    -1,    81,    82,    83,    -1,    -1,    -1,
      87,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    96,
      -1,    -1,    -1,   100,    -1,    -1,    -1,    -1,   105,    -1,
     107,     1,    -1,    -1,   111,     5,     6,     7,     8,     9,
      10,    -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,
      20,    21,    22,    23,    24,    -1,    -1,    -1,    -1,    29,
      30,    31,    32,    33,    34,    -1,    -1,    37,    -1,    -1,
      40,    41,    42,    -1,    44,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    -1,    59,
      -1,    -1,    62,    -1,    -1,    -1,    -1,    -1,    68,    -1,
      -1,    71,    -1,    73,    74,    75,    -1,    77,    -1,    -1,
      -1,    81,    82,    83,    -1,    -1,    -1,    87,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,    -1,
     100,    -1,    -1,    -1,    -1,   105,    -1,   107,     1,    -1,
      -1,   111,     5,     6,     7,     8,     9,    10,    -1,    -1,
      -1,    -1,    15,    -1,    -1,    -1,    -1,    20,    21,    22,
      23,    -1,    -1,    -1,    -1,    -1,    29,    30,    31,    32,
      33,    34,    -1,    -1,    37,    -1,    -1,    40,    41,    42,
      -1,    44,    -1,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    -1,    59,    -1,    -1,    62,
      -1,    -1,    -1,    -1,    -1,    68,    -1,    -1,    71,    -1,
      73,    74,    75,    -1,    77,    -1,    -1,    -1,    81,    82,
      83,    -1,    -1,    -1,    87,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    96,    -1,    -1,    -1,   100,    -1,    -1,
      -1,    -1,   105,    -1,   107,     1,    -1,    -1,   111,     5,
       6,     7,     8,     9,    10,    -1,    -1,    -1,    -1,    15,
      -1,    -1,    -1,    -1,    20,    21,    22,    23,    -1,    -1,
      -1,    -1,    -1,    29,    30,    31,    32,    33,    34,    -1,
      -1,    37,    -1,    -1,    40,    41,    42,    -1,    44,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    -1,    59,    -1,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    68,    -1,    -1,    71,    -1,    73,    74,    75,
      -1,    77,    -1,    -1,    -1,    81,    82,    83,    -1,    -1,
      -1,    87,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      96,    -1,    -1,    -1,   100,    -1,    -1,    -1,    -1,   105,
      -1,   107,     1,    -1,    -1,   111,     5,     6,     7,     8,
       9,    10,    -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,
      -1,    20,    21,    22,    23,    -1,    -1,    -1,    -1,    -1,
      29,    30,    31,    32,    33,    34,    -1,    -1,    37,    -1,
      -1,    40,    41,    42,    -1,    44,    -1,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    -1,
      59,    -1,    -1,    62,    -1,    -1,    -1,    -1,    -1,    68,
      -1,    -1,    71,    -1,    73,    74,    75,    -1,    77,    -1,
      -1,    -1,    81,    82,    83,    -1,    -1,    -1,    87,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,
      -1,   100,    -1,    -1,    -1,    -1,   105,    -1,   107,     1,
      -1,    -1,   111,     5,     6,     7,     8,     9,    10,    -1,
      -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,    20,    21,
      22,    23,    -1,    -1,    -1,    -1,    -1,    29,    30,    31,
      32,    33,    34,    -1,    -1,    37,    -1,    -1,    40,    41,
      42,    -1,    44,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    -1,    59,    -1,    -1,
      62,    -1,    -1,    -1,    -1,    -1,    68,    -1,    -1,    71,
      -1,    73,    74,    75,    -1,    77,    -1,    -1,    -1,    81,
      82,    83,    -1,    -1,    -1,    87,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    96,    -1,    -1,    -1,   100,    -1,
      -1,    -1,    -1,   105,    -1,   107,     1,    -1,    -1,   111,
       5,     6,     7,     8,     9,    10,    -1,    -1,    -1,    -1,
      15,    -1,    -1,    -1,    -1,    20,    21,    22,    23,    -1,
      -1,    -1,    -1,    -1,    29,    30,    31,    32,    33,    34,
      -1,    -1,    37,    -1,    -1,    40,    41,    42,    -1,    44,
      -1,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    -1,    59,    -1,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    68,    -1,    -1,    71,    -1,    73,    74,
      75,    -1,    77,    -1,    -1,    -1,    81,    82,    83,    -1,
      -1,     1,    87,    -1,     4,    -1,    -1,    -1,    -1,    -1,
      -1,    96,    -1,    -1,    -1,   100,    -1,    -1,    -1,    -1,
     105,    -1,   107,    -1,    24,    -1,   111,    27,    28,    -1,
      -1,    31,    32,    -1,    -1,    35,    -1,    -1,    -1,    -1,
      -1,    41,    42,    43,    -1,    45,    -1,    47,    48,    49,
      -1,    51,    52,    53,    54,    55,    56,    -1,    58,    -1,
      60,    -1,    -1,    -1,    64,    65,    -1,    -1,    68,    69,
      -1,    71,    72,    73,    74,    75,    -1,    77,    78,    -1,
      80,    -1,    82,    83,    84,    -1,    -1,    87,    -1,     1,
      -1,    -1,     4,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   101,    -1,   103,    -1,    -1,    -1,    -1,    -1,   109,
      -1,   111,    24,    -1,    -1,    27,    28,    -1,    -1,    31,
      32,    -1,    -1,    35,    -1,    -1,    -1,    -1,    -1,    41,
      42,    43,    -1,    45,    -1,    47,    48,    49,    -1,    51,
      52,    53,    54,    55,    56,    -1,    58,    -1,    60,    -1,
      -1,    -1,    64,    65,    -1,    -1,    68,    69,    -1,    71,
      72,    73,    74,    75,    -1,    77,    78,    -1,    80,    -1,
      82,    83,    84,     1,    -1,    87,    -1,     5,     6,     7,
       8,     9,    -1,    -1,    -1,    -1,    -1,    15,    -1,   101,
      -1,   103,    -1,    21,    22,    -1,    -1,   109,    -1,   111,
      -1,    -1,     1,    31,    32,    -1,     5,     6,     7,     8,
       9,    -1,    -1,    41,    -1,    -1,    15,    -1,    46,    47,
      48,    49,    21,    22,    -1,    -1,    -1,    55,    -1,    57,
      -1,    59,    31,    32,    62,    -1,    27,    28,    -1,    -1,
      -1,    -1,    41,    -1,    35,    -1,    74,    46,    47,    48,
      49,    -1,    43,    81,    45,    83,    55,    -1,    57,    -1,
      59,    -1,    -1,    62,    -1,    -1,    -1,    58,    96,    60,
      -1,    -1,   100,    64,    65,    74,    -1,   105,    69,   107,
      -1,    72,    81,   111,    83,    -1,    -1,    78,    -1,    11,
      12,    13,    14,    84,    16,    17,    -1,    96,    -1,    -1,
      -1,   100,    -1,    -1,    -1,    -1,   105,    -1,   107,     1,
      -1,    -1,   111,     5,     6,     7,     8,     9,    -1,    -1,
      -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,    -1,    21,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     1,    31,
      32,    -1,     5,     6,     7,     8,     9,    -1,    -1,    41,
      -1,    -1,    15,    -1,    46,    47,    48,    49,    21,    22,
      -1,    -1,    -1,    55,    -1,    57,    -1,    59,    31,    32,
      62,    93,    94,    95,    96,    97,    98,    99,    41,    -1,
      -1,    -1,    74,    46,    47,    48,    49,    -1,    -1,    81,
      -1,    83,    55,    -1,    57,    -1,    59,    -1,    -1,    62,
      -1,    -1,    -1,    -1,    96,    -1,    -1,    -1,   100,    -1,
      -1,    74,    -1,   105,    -1,   107,    -1,    -1,    81,   111,
      83,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    96,    -1,    -1,    -1,   100,    -1,    -1,
      -1,    -1,   105,    -1,   107,     1,    -1,    -1,   111,     5,
       6,     7,     8,     9,    -1,    -1,    -1,    -1,    -1,    15,
      -1,    -1,    -1,    -1,    -1,    21,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     1,    31,    32,    -1,     5,     6,
       7,     8,     9,    -1,    -1,    41,    -1,    -1,    15,    -1,
      46,    47,    48,    49,    21,    22,    -1,    -1,    -1,    55,
      -1,    57,    -1,    59,    31,    32,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    41,    -1,    -1,    -1,    74,    46,
      47,    48,    49,    -1,    -1,    81,    -1,    83,    55,    -1,
      57,    -1,    59,    -1,    -1,    62,    -1,    -1,    -1,    -1,
      96,    -1,    -1,    -1,   100,    -1,    -1,    74,    -1,   105,
      -1,   107,    -1,    -1,    81,   111,    83,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    96,
      -1,    -1,    -1,   100,    -1,    -1,    -1,    -1,   105,    -1,
     107,     1,    -1,    -1,   111,     5,     6,     7,     8,     9,
      -1,    -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,
      -1,    21,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       1,    31,    32,    -1,     5,     6,     7,     8,     9,    -1,
      -1,    41,    -1,    -1,    15,    -1,    46,    47,    48,    49,
      21,    22,    -1,    -1,    -1,    55,    -1,    57,    -1,    59,
      31,    32,    62,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      41,    -1,    -1,    -1,    74,    46,    47,    48,    49,    -1,
      -1,    81,    -1,    83,    55,    -1,    57,    -1,    59,    -1,
      -1,    62,    -1,    -1,    -1,    -1,    96,    -1,    -1,    -1,
     100,    -1,    -1,    74,    -1,   105,    -1,   107,    -1,    -1,
      81,   111,    83,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,    -1,   100,
      -1,    -1,    -1,    -1,   105,    -1,   107,     1,    -1,    -1,
     111,     5,     6,     7,     8,     9,    -1,    -1,    -1,    -1,
      -1,    15,    -1,    -1,    -1,    -1,    -1,    21,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     1,    31,    32,    -1,
       5,     6,     7,     8,     9,    -1,    -1,    41,    -1,    -1,
      15,    -1,    46,    47,    48,    49,    21,    22,    -1,    -1,
      -1,    55,    -1,    57,    -1,    59,    31,    32,    62,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    41,    -1,    -1,    -1,
      74,    46,    47,    48,    49,    -1,    -1,    81,    -1,    83,
      55,    -1,    57,    -1,    59,    -1,    -1,    62,    -1,    -1,
      -1,    -1,    96,    -1,    -1,    -1,   100,    -1,    -1,    74,
      -1,   105,    -1,   107,    -1,    -1,    81,   111,    83,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    96,    -1,    -1,    -1,   100,    -1,    -1,    -1,    -1,
     105,    -1,   107,     1,    -1,    -1,   111,     5,     6,     7,
       8,     9,    -1,    -1,    -1,    -1,    -1,    15,    -1,    -1,
      -1,    -1,    -1,    21,    22,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     1,    31,    32,    -1,     5,     6,     7,     8,
       9,    -1,    -1,    41,    -1,    -1,    15,    -1,    46,    47,
      48,    49,    21,    22,    -1,    -1,    -1,    55,    -1,    57,
      -1,    59,    31,    32,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    41,    -1,    -1,    -1,    74,    46,    47,    48,
      49,    -1,    -1,    81,    -1,    83,    55,    -1,    57,    -1,
      59,    -1,    -1,    62,    -1,    -1,    -1,    -1,    96,    -1,
      -1,    -1,   100,    -1,    -1,    74,    -1,   105,    -1,   107,
      -1,    -1,    81,   111,    83,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,
      -1,   100,    -1,    -1,    -1,    -1,   105,    -1,   107,     1,
      -1,    -1,   111,     5,     6,     7,     8,     9,    -1,    -1,
      -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,    -1,    21,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     1,    31,
      32,    -1,     5,     6,     7,     8,     9,    -1,    -1,    41,
      -1,    -1,    15,    -1,    46,    47,    48,    49,    21,    22,
      -1,    -1,    -1,    55,    -1,    57,    -1,    59,    31,    32,
      62,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    41,    -1,
      -1,    -1,    74,    46,    47,    48,    49,    -1,    -1,    81,
      -1,    83,    55,    -1,    57,    -1,    59,    -1,    -1,    62,
      -1,    -1,    -1,    -1,    96,    -1,    -1,    -1,   100,    -1,
      -1,    74,    -1,   105,    -1,   107,    -1,    -1,    81,   111,
      83,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    96,    -1,    -1,    -1,   100,    -1,    -1,
      -1,    -1,   105,    -1,   107,     1,    -1,    -1,   111,     5,
       6,     7,     8,     9,    -1,    -1,    -1,    -1,    -1,    15,
      -1,    -1,    -1,    -1,    -1,    21,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     1,    31,    32,    -1,     5,     6,
       7,     8,     9,    -1,    -1,    41,    -1,    -1,    15,    -1,
      46,    47,    48,    49,    21,    22,    -1,    -1,    -1,    55,
      -1,    57,    -1,    59,    31,    32,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    41,    -1,    -1,    -1,    74,    46,
      47,    48,    49,    -1,    -1,    81,    -1,    83,    55,    -1,
      57,    -1,    59,    -1,    -1,    62,    -1,    -1,    -1,    -1,
      96,    -1,    -1,    -1,   100,    -1,    -1,    74,    -1,   105,
      -1,   107,    -1,    -1,    81,   111,    83,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    96,
      -1,    -1,    -1,   100,    -1,    -1,    -1,    -1,   105,    -1,
     107,     1,    -1,    -1,   111,     5,     6,     7,     8,     9,
      -1,    -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,
      -1,    21,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       1,    31,    32,    -1,     5,     6,     7,     8,     9,    -1,
      -1,    41,    -1,    -1,    15,    -1,    46,    47,    48,    49,
      21,    22,    -1,    -1,    -1,    55,    -1,    57,    -1,    59,
      31,    32,    62,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      41,    -1,    -1,    -1,    74,    46,    47,    48,    49,    -1,
      -1,    81,    -1,    83,    55,    -1,    57,    -1,    59,    -1,
      -1,    62,    -1,    -1,    -1,    -1,    96,     1,    -1,    -1,
     100,    -1,    -1,    74,    -1,   105,    -1,   107,    25,    26,
      81,   111,    83,    -1,    -1,    -1,    -1,    -1,    -1,    36,
      -1,    -1,    -1,    27,    28,    96,    -1,    -1,    32,   100,
      -1,    35,    -1,    -1,   105,    -1,   107,    -1,    -1,    43,
     111,    45,    -1,    47,    48,    49,    63,    -1,    -1,    66,
      67,     1,    -1,    70,    58,    -1,    60,    -1,    -1,    76,
      64,    65,    79,    -1,    -1,    69,    -1,    -1,    72,    86,
      74,    88,    -1,    -1,    78,    -1,    -1,    27,    28,    83,
      84,    -1,    32,    -1,    -1,    35,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    43,    -1,    45,    -1,    47,    48,    49,
      -1,     1,   106,    -1,    -1,    -1,     6,   111,    58,    -1,
      60,    -1,    -1,    -1,    64,    65,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    74,    25,    26,    -1,    78,    -1,
      -1,    -1,    32,    83,    84,    -1,    36,    -1,    -1,    -1,
      11,    12,    13,    14,    -1,    16,    17,    47,    48,    49,
      -1,    -1,    -1,    11,    12,    13,    14,    57,    16,    17,
      18,   111,    -1,    63,    -1,    -1,    66,    67,    -1,    -1,
      70,    -1,    -1,    -1,    74,    -1,    76,    -1,    -1,    79,
      -1,    -1,    -1,    83,     3,    -1,    86,    -1,    88,     8,
       9,    -1,    11,    12,    13,    14,    -1,    16,    17,    18,
      19,    -1,    -1,    -1,    -1,    24,    25,    26,    -1,    -1,
      -1,   111,    -1,    -1,    -1,    -1,    -1,    36,    -1,    38,
      39,    92,    93,    94,    95,    96,    97,    98,    99,     1,
      -1,    -1,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,    -1,    -1,    63,    -1,    -1,    66,    67,    -1,
      -1,    70,    24,    25,    26,    -1,    -1,    76,    -1,    -1,
      79,    -1,    -1,    -1,    36,    -1,    -1,    86,    -1,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,    -1,   101,   102,   103,   104,   105,   106,   107,   108,
      -1,    63,   111,    -1,    66,    67,    -1,    -1,    70,    -1,
      -1,    -1,    -1,    -1,    76,    -1,    -1,    79,    -1,    -1,
      -1,    -1,    -1,    -1,    86,    -1,    88,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   101,
      -1,   103,   104,   105,   106,    -1,   108,   109,     5,     6,
       7,     8,     9,    10,    -1,    -1,    -1,    -1,    15,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    -1,    -1,
      27,    28,    29,    30,    -1,    32,    33,    34,    35,    -1,
      37,    -1,    -1,    40,    -1,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    -1,    62,    -1,    64,    65,    -1,
      -1,    68,    69,    -1,    71,    72,    73,    74,    75,    -1,
      77,    78,    -1,    -1,    81,    82,    83,    84,    -1,    -1,
      87,    -1,    -1,    -1,    -1,    -1,    -1,    94,    -1,    96,
      -1,    -1,    -1,   100,    -1,    -1,    -1,    -1,   105,    -1,
     107,   108,    -1,    -1,   111,     5,     6,     7,     8,     9,
      10,    -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,
      20,    21,    22,    23,    -1,    -1,    -1,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    -1,    37,    -1,    -1,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    -1,    62,    -1,    64,    65,    -1,    -1,    68,    69,
      -1,    71,    72,    73,    74,    75,    -1,    77,    78,    -1,
      -1,    81,    82,    83,    84,    -1,    -1,    87,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,    -1,
     100,    -1,    -1,    -1,    -1,   105,    -1,   107,    -1,    -1,
      -1,   111,     5,     6,     7,     8,     9,    10,    -1,    -1,
      -1,    -1,    15,    -1,    -1,    -1,    -1,    20,    21,    22,
      23,    24,    -1,    -1,    27,    28,    29,    30,    -1,    32,
      33,    34,    35,    -1,    37,    -1,    -1,    40,    -1,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    -1,    62,
      -1,    64,    65,    -1,    -1,    68,    69,    -1,    71,    72,
      73,    74,    75,    -1,    77,    78,    -1,    -1,    81,    82,
      83,    84,    -1,    -1,    87,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    96,     5,     6,     7,   100,    -1,    10,
      -1,    -1,   105,    -1,   107,    -1,    -1,    -1,   111,    20,
      21,    22,    23,    -1,    -1,    -1,    27,    28,    29,    30,
      -1,    32,    33,    34,    35,    -1,    37,    -1,    -1,    40,
      -1,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      -1,    62,    -1,    64,    65,    -1,    -1,    68,    69,    -1,
      71,    72,    73,    74,    75,    -1,    77,    78,    -1,    -1,
      81,    82,    83,    84,    -1,    -1,    87,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     5,     6,     7,
       8,     9,    10,    -1,   105,    -1,   107,    15,    -1,    -1,
     111,    -1,    20,    21,    22,    23,    -1,    -1,    -1,    -1,
      -1,    29,    30,    31,    32,    33,    34,    -1,    -1,    37,
      -1,    -1,    40,    41,    42,    -1,    44,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      -1,    59,    -1,    -1,    62,    -1,    -1,    -1,    -1,    -1,
      68,    -1,    -1,    71,    -1,    73,    74,    75,    -1,    77,
      -1,    -1,    -1,    81,    82,    83,    -1,    -1,    -1,    87,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    96,    -1,
      -1,    -1,   100,    -1,    -1,    -1,    -1,   105,    -1,   107,
      -1,    -1,   110,   111,     5,     6,     7,     8,     9,    10,
      -1,    -1,    -1,    -1,    15,    -1,    -1,    -1,    -1,    20,
      21,    22,    23,    -1,    -1,    -1,    -1,    -1,    29,    30,
      -1,    32,    33,    34,    -1,    -1,    37,    -1,    -1,    40,
      -1,    42,    -1,    44,    -1,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    -1,    59,    -1,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    68,    -1,    -1,
      71,    -1,    73,    74,    75,    -1,    77,    -1,    -1,    -1,
      81,    82,    83,    -1,    -1,    -1,    87,    -1,    -1,    -1,
      -1,     5,     6,     7,    -1,    96,    -1,    -1,    -1,   100,
      -1,    -1,    -1,    -1,   105,    -1,   107,    21,    22,    -1,
     111,     5,     6,     7,     8,     9,    -1,    -1,    32,    -1,
      -1,    15,    -1,    -1,    -1,    -1,    -1,    21,    22,    -1,
      -1,    -1,    46,    47,    48,    49,    -1,    -1,    32,    -1,
      -1,    55,    -1,    57,    -1,    59,    -1,    -1,    62,    -1,
      -1,    -1,    46,    47,    48,    49,    -1,    -1,    -1,    -1,
      74,    55,    -1,    57,    -1,    59,    -1,    81,    62,    83,
       3,    -1,    -1,    -1,    -1,     8,     9,    -1,    -1,    -1,
      74,    -1,    -1,    -1,    -1,    -1,    -1,    81,    -1,    83,
      -1,   105,    25,    26,    -1,    -1,    -1,   111,    -1,    -1,
      -1,    -1,    96,    36,    -1,    -1,   100,    -1,    -1,    -1,
      -1,   105,    -1,   107,    -1,    -1,     3,   111,    -1,    -1,
      -1,     8,     9,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      63,    -1,    -1,    66,    67,    -1,    -1,    70,    25,    26,
      -1,    -1,    -1,    76,    -1,    -1,    79,    -1,    -1,    36,
      -1,    -1,    -1,    86,    -1,    88,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   104,   105,    -1,   107,   108,    63,    -1,    -1,    66,
      67,    -1,    -1,    70,    -1,    -1,    -1,    -1,    -1,    76,
      -1,    -1,    79,    -1,    -1,    -1,    -1,    -1,    -1,    86,
      -1,    88,     1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    10,    -1,    -1,    -1,    -1,    -1,    -1,   105,    -1,
     107,    20,    21,    22,    23,    24,    -1,    -1,    27,    28,
      29,    30,    31,    -1,    33,    34,    35,    -1,    37,    -1,
      -1,    40,    41,    42,    43,    44,    45,    46,    -1,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    -1,    -1,    -1,    64,    65,    -1,    -1,    68,
      69,    -1,    71,    72,    73,    74,    75,    -1,    77,    78,
      -1,    80,    81,    82,    -1,    84,    -1,    -1,    87,    11,
      12,    13,    14,    -1,    16,    17,    18,    19,    -1,    -1,
      -1,    -1,    -1,    -1,   103,   104,    11,    12,    13,    14,
      -1,    16,    17,    -1,    -1,    11,    12,    13,    14,    -1,
      16,    17,   344,   345,   346,   347,   348,   349,   350,   351,
      -1,   353,   354,   355,   356,   357,   358,   359,   360,   361,
     362,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,    91,    92,    93,    94,    95,
      96,    97,    98,    99,    10,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    20,    21,    22,    23,    -1,    -1,
      -1,    27,    28,    29,    30,    31,    -1,    33,    34,    35,
      -1,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    -1,    -1,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    -1,    -1,    -1,    64,    65,
      -1,    -1,    68,    69,    -1,    71,    72,    73,    74,    75,
      -1,    77,    78,    -1,    80,    81,    82,    10,    84,    -1,
      -1,    87,    -1,    -1,    -1,    -1,    -1,    20,    21,    22,
      23,    97,    -1,    -1,    27,    28,    29,    30,    31,    -1,
      33,    34,    35,    -1,    37,    -1,    -1,    40,    41,    42,
      43,    44,    45,    46,    -1,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    -1,    -1,
      -1,    64,    65,    -1,    -1,    68,    69,    -1,    71,    72,
      73,    74,    75,    -1,    77,    78,    -1,    80,    81,    82,
      10,    84,    -1,    -1,    87,    -1,    -1,    -1,    -1,    -1,
      20,    21,    22,    23,    97,    -1,    -1,    27,    28,    29,
      30,    31,    -1,    33,    34,    35,    -1,    37,    -1,    -1,
      40,    41,    42,    43,    44,    45,    46,    -1,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    -1,    -1,    -1,    64,    65,    -1,    -1,    68,    69,
      -1,    71,    72,    73,    74,    75,    -1,    77,    78,    -1,
      80,    81,    82,    10,    84,    -1,    -1,    87,    -1,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    97,    -1,    -1,
      27,    28,    29,    30,    31,    -1,    33,    34,    35,    -1,
      37,    -1,    -1,    40,    41,    42,    43,    44,    45,    46,
      -1,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    -1,    -1,    -1,    64,    65,    -1,
      -1,    68,    69,    -1,    71,    72,    73,    74,    75,    -1,
      77,    78,    -1,    80,    81,    82,    10,    84,    -1,    -1,
      87,    -1,    -1,    -1,    -1,    -1,    20,    21,    22,    23,
      97,    -1,    -1,    27,    28,    29,    30,    31,    -1,    33,
      34,    35,    -1,    37,    -1,    -1,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    -1,    -1,    -1,
      64,    65,    -1,    -1,    68,    69,    -1,    71,    72,    73,
      74,    75,    -1,    77,    78,    -1,    80,    81,    82,    10,
      84,    -1,    -1,    87,    -1,    -1,    -1,    -1,    -1,    20,
      21,    22,    23,    97,    -1,    -1,    27,    28,    29,    30,
      31,    -1,    33,    34,    35,    -1,    37,    -1,    -1,    40,
      41,    42,    43,    44,    45,    46,    -1,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      -1,    -1,    -1,    64,    65,    -1,    -1,    68,    69,    -1,
      71,    72,    73,    74,    75,    -1,    77,    78,    -1,    80,
      81,    82,     1,    84,    -1,     4,    87,    -1,    -1,    -1,
      -1,    10,    -1,    -1,    -1,    -1,    97,    -1,    -1,    -1,
      -1,    20,    21,    22,    23,    -1,    -1,    -1,    27,    28,
      29,    30,    31,    -1,    33,    34,    35,    -1,    37,    -1,
      -1,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    -1,    -1,    -1,    64,    65,    -1,    -1,    68,
      69,     1,    71,    72,    73,    74,    75,    -1,    77,    78,
      10,    80,    81,    82,    -1,    84,    85,    -1,    87,    -1,
      20,    21,    22,    23,    -1,    -1,    -1,    27,    28,    29,
      30,    31,    -1,    33,    34,    35,    -1,    37,    -1,    -1,
      40,    41,    42,    43,    44,    45,    46,    -1,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    -1,    -1,    -1,    64,    65,    -1,    -1,    68,    69,
       1,    71,    72,    73,    74,    75,    -1,    77,    78,    10,
      80,    81,    82,    -1,    84,    -1,    -1,    87,    -1,    20,
      21,    22,    23,    -1,    -1,    -1,    27,    28,    29,    30,
      31,    -1,    33,    34,    35,    -1,    37,    -1,    -1,    40,
      41,    42,    43,    44,    45,    46,    -1,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      -1,    -1,    -1,    64,    65,    -1,    -1,    68,    69,     1,
      71,    72,    73,    74,    75,    -1,    77,    78,    10,    80,
      81,    82,    -1,    84,    -1,    -1,    87,    -1,    20,    21,
      22,    23,    -1,    -1,    -1,    27,    28,    29,    30,    31,
      -1,    33,    34,    35,    -1,    37,    -1,    -1,    40,    41,
      42,    43,    44,    45,    46,    -1,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    -1,
      -1,    -1,    64,    65,    -1,    -1,    68,    69,     1,    71,
      72,    73,    74,    75,    -1,    77,    78,    10,    80,    81,
      82,    -1,    84,    -1,    -1,    87,    -1,    20,    21,    22,
      23,    -1,    -1,    -1,    27,    28,    29,    30,    31,    -1,
      33,    34,    35,    -1,    37,    -1,    -1,    40,    41,    42,
      43,    44,    45,    46,    -1,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    -1,    -1,
      -1,    64,    65,    -1,    -1,    68,    69,     1,    71,    72,
      73,    74,    75,    -1,    77,    78,    10,    80,    81,    82,
      -1,    84,    -1,    -1,    87,    -1,    20,    21,    22,    23,
      -1,    -1,    -1,    27,    28,    29,    30,    31,    -1,    33,
      34,    35,    -1,    37,    -1,    -1,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    -1,    -1,    -1,
      64,    65,    -1,    -1,    68,    69,     1,    71,    72,    73,
      74,    75,    -1,    77,    78,    10,    80,    81,    82,    -1,
      84,    -1,    -1,    87,    -1,    20,    21,    22,    23,    -1,
      -1,    -1,    27,    28,    29,    30,    31,    -1,    33,    34,
      35,    -1,    37,    -1,    -1,    40,    41,    42,    43,    44,
      45,    46,    -1,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    -1,    -1,    -1,    64,
      65,    -1,    -1,    68,    69,     1,    71,    72,    73,    74,
      75,    -1,    77,    78,    10,    80,    81,    82,    -1,    84,
      -1,    -1,    87,    -1,    20,    21,    22,    23,    -1,    -1,
      -1,    27,    28,    29,    30,    31,    -1,    33,    34,    35,
      -1,    37,    -1,    -1,    40,    41,    42,    43,    44,    45,
      46,    -1,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    -1,    -1,    -1,    64,    65,
      -1,    -1,    68,    69,    -1,    71,    72,    73,    74,    75,
      -1,    77,    78,    -1,    80,    81,    82,    -1,    84,    27,
      28,    87,    -1,    -1,    32,    -1,    -1,    35,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    43,    -1,    45,    -1,    47,
      48,    49,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      58,    -1,    60,    -1,    -1,    -1,    64,    65,    -1,    -1,
      -1,    69,    -1,    -1,    72,    -1,    74,    -1,    -1,    -1,
      78,    -1,    -1,    27,    28,    83,    84,    -1,    32,    -1,
      -1,    35,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    43,
      -1,    45,    -1,    47,    48,    49,   104,    -1,    -1,    -1,
      -1,    -1,    -1,   111,    58,    -1,    60,    -1,    -1,    -1,
      64,    65,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,
      74,    -1,    -1,    -1,    78,    -1,    -1,    27,    28,    83,
      84,    -1,    32,    -1,    -1,    35,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    43,    -1,    45,    -1,    47,    48,    49,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   111,    58,    -1,
      60,    -1,    -1,    -1,    64,    65,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    74,    -1,    -1,    -1,    78,    -1,
      -1,    -1,    10,    83,    84,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    20,    21,    22,    23,    -1,    -1,    -1,    27,
      28,    29,    30,    31,    -1,    33,    34,    35,    -1,    37,
      -1,   111,    40,    41,    42,    43,    44,    45,    46,    -1,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    -1,    -1,    -1,    64,    65,    -1,    -1,
      68,    69,    -1,    71,    72,    73,    74,    75,    -1,    77,
      78,    10,    80,    81,    82,    -1,    84,    -1,    -1,    87,
      -1,    20,    21,    22,    23,    -1,    -1,    -1,    27,    28,
      29,    30,    31,    -1,    33,    34,    35,    -1,    37,    -1,
      -1,    40,    41,    42,    43,    44,    45,    46,    -1,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    -1,    -1,    -1,    64,    65,    -1,    -1,    68,
      69,    -1,    71,    72,    73,    74,    75,    -1,    77,    78,
      10,    80,    81,    82,    -1,    84,    -1,    -1,    87,    -1,
      20,    21,    22,    23,    -1,    -1,    -1,    27,    28,    29,
      30,    31,    -1,    33,    34,    35,    -1,    37,    -1,    -1,
      40,    41,    42,    43,    44,    45,    46,    -1,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    -1,    -1,    -1,    64,    65,    -1,    -1,    68,    69,
      -1,    71,    72,    73,    74,    75,    -1,    77,    78,    10,
      80,    81,    82,    -1,    84,    -1,    -1,    87,    -1,    20,
      21,    22,    23,    -1,    -1,    -1,    27,    28,    29,    30,
      31,    -1,    33,    34,    35,    -1,    37,    -1,    -1,    40,
      41,    42,    43,    44,    45,    46,    -1,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      -1,    -1,    -1,    64,    65,    -1,    -1,    68,    69,    -1,
      71,    72,    73,    74,    75,    -1,    77,    78,    10,    80,
      81,    82,    -1,    84,    -1,    -1,    87,    -1,    20,    21,
      22,    23,    -1,    -1,    -1,    27,    28,    29,    30,    31,
      -1,    33,    34,    35,    -1,    37,    -1,    -1,    40,    41,
      42,    43,    44,    45,    46,    -1,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    -1,
      -1,    -1,    64,    65,    -1,    -1,    68,    69,    -1,    71,
      72,    73,    74,    75,    -1,    77,    78,    10,    80,    81,
      82,    -1,    84,    -1,    -1,    87,    -1,    20,    21,    22,
      23,    -1,    -1,    -1,    27,    28,    29,    30,    31,    -1,
      33,    34,    35,    -1,    37,    -1,    -1,    40,    41,    42,
      43,    44,    45,    46,    -1,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    -1,    -1,
      -1,    64,    65,    -1,    -1,    68,    69,    -1,    71,    72,
      73,    74,    75,    -1,    77,    78,    10,    80,    81,    82,
      -1,    84,    -1,    -1,    87,    -1,    20,    21,    22,    23,
      -1,    -1,    -1,    27,    28,    29,    30,    31,    -1,    33,
      34,    35,    -1,    37,    -1,    -1,    40,    41,    42,    43,
      44,    45,    46,    -1,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    -1,    -1,    -1,
      64,    65,    -1,    -1,    68,    69,    -1,    71,    72,    73,
      74,    75,    -1,    77,    78,    -1,    80,    81,    82,    -1,
      84,    -1,    -1,    87
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,   113,   114,     0,     1,    24,    51,    53,   101,   119,
     122,   123,   126,   137,   154,   155,   243,    24,   101,   103,
       1,     6,    32,    47,    48,    49,    57,    74,    83,   111,
     296,   297,   298,   309,   310,    48,     4,    31,    41,    52,
      80,   109,   157,   232,   240,    42,    54,    55,    56,    68,
      71,    73,    75,    77,    82,    87,   148,    24,   101,   103,
      10,    20,    21,    22,    23,    27,    28,    29,    30,    31,
      33,    34,    35,    37,    40,    41,    42,    43,    44,    45,
      46,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    64,    65,    68,    69,    71,    72,
      73,    74,    75,    77,    78,    80,    81,    82,    84,    87,
     311,   312,    32,   111,    32,    32,    32,    48,   101,   111,
       1,    48,   311,   101,   309,   102,     1,    48,   124,   125,
     311,   196,   241,     1,   120,   121,    27,    28,    35,    43,
      45,    58,    60,    64,    65,    69,    72,    78,    84,   161,
     168,   170,   171,   172,   296,   141,    28,    35,   128,   156,
     165,   168,    48,    48,   311,    48,   311,     1,    48,   311,
      32,    32,   296,    24,    88,   101,   103,    88,   101,   104,
      88,    48,   143,   311,   143,    24,   101,   103,     1,   102,
     116,   115,   117,   296,   310,   105,   186,     1,   105,   105,
     105,   180,   105,   176,   186,   105,   187,   178,   179,   178,
     176,   177,    48,    97,   235,   311,    90,   114,   105,   105,
       4,   136,   101,     1,     5,     7,     8,     9,    15,    21,
      22,    24,    46,    55,    59,    62,    74,    81,    96,   100,
     105,   107,   129,   133,   159,   160,   220,   250,   278,   279,
     287,   288,   289,   292,   293,   294,   295,   296,   300,   301,
     303,   305,   310,   312,   250,     1,   124,   250,   233,   109,
      24,   101,   103,     1,    48,   311,   101,    95,   164,   168,
     115,     1,   164,   181,     1,     7,    96,   174,   188,   105,
     101,   170,    24,   103,   132,   115,   106,   158,   191,   192,
      21,    22,    46,    55,    59,    81,   129,   294,   294,   133,
     288,   294,   105,   304,     1,   105,   109,   197,   302,    32,
     196,   196,   105,   288,   288,     1,   109,   133,   163,   169,
     171,   276,   278,   133,   162,   163,   172,   294,   296,   306,
     307,   312,   288,   288,    11,    12,    13,    14,    16,    17,
      18,    19,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   232,   240,     3,     8,     9,    25,    26,
      36,    63,    66,    67,    70,    76,    79,    86,    88,   129,
     133,   280,    88,   280,   234,   242,   310,    97,   106,     1,
     104,   106,   106,   104,   164,   183,   184,   281,   106,     7,
      38,    39,   175,   164,   117,   118,   104,   281,    48,    97,
     311,   101,   104,   163,     1,   278,   302,     1,   166,   167,
     169,   172,   275,   276,   198,    48,   311,   219,     1,   110,
     277,   278,   282,   283,     1,   278,    24,   101,   103,   106,
     282,   133,   278,   284,   285,   286,   294,   312,    97,   106,
      90,   104,   106,   163,   307,    48,   108,   104,   308,   108,
       1,   287,     1,   287,     1,   287,     1,   287,     1,   287,
       1,   287,     1,   287,     1,   287,   279,     1,   287,     1,
     287,     1,   287,     1,   287,     1,   287,     1,   287,     1,
     287,     1,   287,     1,   287,     1,   287,   196,     1,   278,
       1,   282,     1,    24,    94,    97,   275,   278,   299,     1,
     278,   278,   278,   105,   230,     1,   235,   237,   239,   106,
     164,    38,    39,   142,   104,   281,     7,    96,   173,   102,
     106,   106,    88,   135,    88,   140,   158,   192,    24,   101,
     103,   106,     1,   104,    24,   101,   103,   106,   158,   194,
     203,   225,    48,   195,   204,   223,   311,   106,   196,   218,
      24,    61,   101,   103,   106,   278,    61,   106,   104,   281,
      24,   101,   103,   106,   106,   103,   131,   307,    24,   101,
     102,   103,   269,   108,   134,   104,   281,   170,   278,   108,
     306,     1,    88,   280,   102,     1,     4,    10,    20,    21,
      22,    23,    27,    28,    29,    30,    31,    33,    34,    35,
      37,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    64,    65,    68,    69,    71,    72,    73,    74,
      75,    77,    78,    80,    81,    82,    84,    85,    87,   149,
     150,   151,   152,   153,    24,   101,   103,   106,   106,    24,
     101,   103,   106,   108,    24,   275,   108,   108,   175,   154,
     227,   228,   229,   281,     1,    24,   109,   231,    88,   236,
      24,   103,   104,   200,   106,   102,   185,     7,   106,   189,
       1,    24,   193,     1,   105,   278,   105,    48,   311,    24,
     101,   103,   106,   278,    48,   311,   104,    88,   218,   104,
      88,   199,     1,   221,   277,   106,   108,     1,   278,   106,
       1,   286,   308,   278,   278,   279,   109,   290,   290,   299,
     128,   102,   104,   281,   106,   130,   114,   250,   238,   182,
     164,   164,   278,   138,   144,   146,   147,   164,   281,     1,
     308,    88,   218,    88,   202,   250,     1,   105,   145,   202,
     250,   208,   145,   196,   108,   158,   227,   227,   200,   237,
     164,   190,   146,   106,   102,   104,   281,   142,    24,   101,
     103,   106,   106,     1,    24,   278,     1,   145,   278,   158,
     194,   146,   224,   194,     1,     4,    10,    20,    23,    29,
      30,    33,    34,    37,    44,    48,    50,    51,    85,   101,
     123,   197,   200,   207,   209,   210,   211,   212,   215,   216,
     217,   244,   253,   256,   258,   261,   265,   268,   270,   272,
     275,   222,   135,   142,   311,   106,   106,   130,     1,    24,
     101,   127,   197,   144,   144,   143,   226,    48,   130,     1,
      24,   197,   201,    24,   101,   103,     1,    48,   205,   206,
     311,     1,    24,   101,   257,   274,   275,   266,   254,    48,
     214,   175,   274,   214,   102,   196,   259,   213,   245,   262,
     257,   257,   257,   201,   291,    48,   139,   201,    24,    88,
     101,   103,    88,   101,   104,    88,   257,   196,   196,   274,
     175,   269,   211,   196,   102,   196,   196,   208,   127,   250,
     250,     1,   205,   250,   267,   255,   269,   273,   274,    24,
      85,   260,   211,   246,   263,   200,   105,   105,   269,    24,
     105,   105,   105,   105,   274,   278,   274,   271,   274,   274,
     274,    24,   103,   106,   247,   101,   104,   252,   247,   257,
     247,   247,   211,     1,   249,   251,   306,   249,   247,   257,
     264,   274,   211,   211,   101,   211,   257,    40,   248,   251,
     271,   211,   247,   211
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (&yylval, YYLEX_PARAM)
#else
# define YYLEX yylex (&yylval)
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */






/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  /* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;

  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 380 "language.yacc"
    { YYACCEPT; }
    break;

  case 3:
#line 381 "language.yacc"
    { YYACCEPT; }
    break;

  case 8:
#line 392 "language.yacc"
    {
    struct pike_string *a,*b;
    copy_shared_string(a,(yyvsp[(1) - (3)].n)->u.sval.u.string);
    copy_shared_string(b,(yyvsp[(3) - (3)].n)->u.sval.u.string);
    free_node((yyvsp[(1) - (3)].n));
    free_node((yyvsp[(3) - (3)].n));
    a=add_and_free_shared_strings(a,b);
    (yyval.n)=mkstrnode(a);
    free_string(a);
  }
    break;

  case 9:
#line 404 "language.yacc"
    { (yyval.n)=(yyvsp[(2) - (2)].n); }
    break;

  case 10:
#line 405 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 11:
#line 406 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 12:
#line 407 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 13:
#line 414 "language.yacc"
    {
    STACK_LEVEL_START(0);

    ref_push_string((yyvsp[(1) - (1)].n)->u.sval.u.string);
    if (call_handle_inherit((yyvsp[(1) - (1)].n)->u.sval.u.string)) {
      STACK_LEVEL_CHECK(2);
      (yyval.n)=mksvaluenode(Pike_sp-1);
      pop_stack();
    }
    else
      (yyval.n)=mknewintnode(0);
    STACK_LEVEL_CHECK(1);
    if((yyval.n)->name) free_string((yyval.n)->name);
#ifdef PIKE_DEBUG
    if (Pike_sp[-1].type != T_STRING) {
      Pike_fatal("Compiler lost track of program name.\n");
    }
#endif /* PIKE_DEBUG */
    /* FIXME: Why not use $1->u.sval.u.string here? */
    add_ref( (yyval.n)->name=Pike_sp[-1].u.string );
    free_node((yyvsp[(1) - (1)].n));

    STACK_LEVEL_DONE(1);
  }
    break;

  case 14:
#line 439 "language.yacc"
    {
    STACK_LEVEL_START(0);

    if(Pike_compiler->last_identifier)
    {
      ref_push_string(Pike_compiler->last_identifier);
    }else{
      push_empty_string();
    }
    (yyval.n)=(yyvsp[(1) - (1)].n);

    STACK_LEVEL_DONE(1);
  }
    break;

  case 15:
#line 456 "language.yacc"
    {
    STACK_LEVEL_START(0);

    resolv_program((yyvsp[(1) - (1)].n));
    free_node((yyvsp[(1) - (1)].n));

    STACK_LEVEL_DONE(1);
  }
    break;

  case 16:
#line 467 "language.yacc"
    {
#ifdef WITH_FACETS
    struct object *o;
    if (Pike_compiler->compiler_pass == 1) {
      if (Pike_compiler->new_program->flags & PROGRAM_IS_FACET) {
	yyerror("A class can only belong to one facet.");
      }
      else {
	resolv_constant((yyvsp[(4) - (5)].n));
	if (Pike_sp[-1].type == T_OBJECT) {
	  /* FIXME: Object subtypes! */
	  o = Pike_sp[-1].u.object;
	  ref_push_string((yyvsp[(2) - (5)].n)->u.sval.u.string);
	  push_int(Pike_compiler->new_program->id);
	  push_int(!!(Pike_compiler->new_program->flags & PROGRAM_IS_PRODUCT));
	  safe_apply(o, "add_facet_class", 3);
	  if (Pike_sp[-1].type == T_INT &&
	      Pike_sp[-1].u.integer >= 0) {
	    Pike_compiler->new_program->flags &= ~PROGRAM_IS_PRODUCT;
	    Pike_compiler->new_program->flags |= PROGRAM_IS_FACET;
	    Pike_compiler->new_program->facet_index = Pike_sp[-1].u.integer;
	    add_ref(Pike_compiler->new_program->facet_group = o);
	  }
	  else
	    yyerror("Could not add facet class to system.");
	  pop_stack();
	}
	else
	  yyerror("Invalid facet group specifier.");
	pop_stack();
      }
    }
    free_node((yyvsp[(2) - (5)].n));
    free_node((yyvsp[(4) - (5)].n));
#else
    yyerror("No support for facets.");
#endif
  }
    break;

  case 17:
#line 508 "language.yacc"
    {
    SET_FORCE_RESOLVE((yyval.number));
  }
    break;

  case 18:
#line 512 "language.yacc"
    {
    UNSET_FORCE_RESOLVE((yyvsp[(1) - (2)].number));
    (yyval.n) = (yyvsp[(2) - (2)].n);
  }
    break;

  case 19:
#line 519 "language.yacc"
    {
    if (((yyvsp[(1) - (5)].number) & ID_EXTERN) && (Pike_compiler->compiler_pass == 1)) {
      yywarning("Extern declared inherit.");
    }
    if((yyvsp[(3) - (5)].n))
    {
      struct pike_string *s=Pike_sp[-1].u.string;
      if((yyvsp[(4) - (5)].n)) s=(yyvsp[(4) - (5)].n)->u.sval.u.string;
      compiler_do_inherit((yyvsp[(3) - (5)].n),(yyvsp[(1) - (5)].number),s);
    }

#ifdef WITH_FACETS
    /* If this is a product class, check that all product classes in its
     * facet-group inherit from all facets */
    if((yyvsp[(3) - (5)].n) && Pike_compiler->compiler_pass == 2) {
      if (Pike_compiler->new_program->flags & PROGRAM_IS_PRODUCT) {
	if (!Pike_compiler->new_program->facet_group)
	  yyerror("Product class without facet group.");
	else {
	  safe_apply(Pike_compiler->new_program->facet_group,
		     "product_classes_checked", 0);
	  if (Pike_sp[-1].type == T_INT &&
	      Pike_sp[-1].u.integer == 0) {
	    pop_stack();
	    safe_apply(Pike_compiler->new_program->facet_group,
		       "check_product_classes", 0);
	  }
	  pop_stack();
	}
      }
    }
#endif
    if((yyvsp[(4) - (5)].n)) free_node((yyvsp[(4) - (5)].n));
    pop_stack();
    if ((yyvsp[(3) - (5)].n)) free_node((yyvsp[(3) - (5)].n));
  }
    break;

  case 20:
#line 556 "language.yacc"
    {
    if ((yyvsp[(3) - (5)].n)) free_node((yyvsp[(3) - (5)].n));
    pop_stack();
    yyerrok;
  }
    break;

  case 21:
#line 562 "language.yacc"
    {
    if ((yyvsp[(3) - (5)].n)) free_node((yyvsp[(3) - (5)].n));
    pop_stack();
    yyerror("Missing ';'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 22:
#line 569 "language.yacc"
    {
    if ((yyvsp[(3) - (5)].n)) free_node((yyvsp[(3) - (5)].n));
    pop_stack();
    yyerror("Missing ';'.");
  }
    break;

  case 23:
#line 574 "language.yacc"
    { yyerrok; }
    break;

  case 24:
#line 576 "language.yacc"
    {
    yyerror("Missing ';'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 25:
#line 580 "language.yacc"
    { yyerror("Missing ';'."); }
    break;

  case 26:
#line 584 "language.yacc"
    {
    resolv_constant((yyvsp[(2) - (3)].n));
    free_node((yyvsp[(2) - (3)].n));
    use_module(Pike_sp-1);
    pop_stack();
  }
    break;

  case 27:
#line 591 "language.yacc"
    {
    if (call_handle_import((yyvsp[(2) - (3)].n)->u.sval.u.string)) {
      use_module(Pike_sp-1);
      pop_stack();
    }
    free_node((yyvsp[(2) - (3)].n));
  }
    break;

  case 28:
#line 598 "language.yacc"
    { yyerrok; }
    break;

  case 29:
#line 600 "language.yacc"
    {
    yyerror("Missing ';'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 30:
#line 604 "language.yacc"
    { yyerror("Missing ';'."); }
    break;

  case 31:
#line 608 "language.yacc"
    {
    /* This can be made more lenient in the future */

    /* Ugly hack to make sure that $3 is optimized */
    {
      int tmp=Pike_compiler->compiler_pass;
      (yyvsp[(3) - (3)].n)=mknode(F_COMMA_EXPR,(yyvsp[(3) - (3)].n),0);
      Pike_compiler->compiler_pass=tmp;
    }

    if (!TEST_COMPAT(7, 6) && (Pike_compiler->current_modifiers & ID_EXTERN)) {
      int depth = 0;
      struct program_state *state = Pike_compiler;
      node *n = (yyvsp[(3) - (3)].n);
      while (((n->token == F_COMMA_EXPR) || (n->token == F_ARG_LIST)) &&
	     ((!CAR(n)) ^ (!CDR(n)))) {
	if (CAR(n)) n = CAR(n);
	else n = CDR(n);
      }
      if (n->token == F_EXTERNAL) {
	while (state && (state->new_program->id != n->u.integer.a)) {
	  depth++;
	  state = state->previous;
	}
      }
      if (depth && state) {
	/* Alias for a symbol in a surrounding scope. */
	int id = really_low_reference_inherited_identifier(state, 0,
							   n->u.integer.b);
	define_alias((yyvsp[(1) - (3)].n)->u.sval.u.string, n->type,
		     Pike_compiler->current_modifiers & ~ID_EXTERN,
		     depth, id);
      } else if (Pike_compiler->compiler_pass == 1) {
	yyerror("Invalid extern declared constant.");
	add_constant((yyvsp[(1) - (3)].n)->u.sval.u.string, &svalue_undefined,
		     Pike_compiler->current_modifiers & ~ID_EXTERN);
      }
    } else {
      if (TEST_COMPAT(7, 6) &&
	  (Pike_compiler->current_modifiers & ID_EXTERN) &&
	  (Pike_compiler->compiler_pass == 1)) {
	yywarning("Extern declared constant.");
      }
      if(!is_const((yyvsp[(3) - (3)].n))) {
	if (Pike_compiler->compiler_pass == 2) {
	  yyerror("Constant definition is not constant.");
	}
	add_constant((yyvsp[(1) - (3)].n)->u.sval.u.string, 0,
		     Pike_compiler->current_modifiers & ~ID_EXTERN);
      } else {
	if(!Pike_compiler->num_parse_error)
	{
	  ptrdiff_t tmp=eval_low((yyvsp[(3) - (3)].n),1);
	  if(tmp < 1)
	  {
	    yyerror("Error in constant definition.");
	    push_undefined();
	  }else{
	    pop_n_elems(DO_NOT_WARN((INT32)(tmp - 1)));
	  }
	} else {
	  push_undefined();
	}
	add_constant((yyvsp[(1) - (3)].n)->u.sval.u.string, Pike_sp-1,
		     Pike_compiler->current_modifiers & ~ID_EXTERN);
	pop_stack();
      }
    }
  const_def_ok:
    if((yyvsp[(3) - (3)].n)) free_node((yyvsp[(3) - (3)].n));
    free_node((yyvsp[(1) - (3)].n));
  }
    break;

  case 32:
#line 680 "language.yacc"
    { if ((yyvsp[(3) - (3)].n)) free_node((yyvsp[(3) - (3)].n)); }
    break;

  case 33:
#line 681 "language.yacc"
    { if ((yyvsp[(3) - (3)].n)) free_node((yyvsp[(3) - (3)].n)); }
    break;

  case 36:
#line 688 "language.yacc"
    {}
    break;

  case 37:
#line 689 "language.yacc"
    { yyerrok; }
    break;

  case 38:
#line 691 "language.yacc"
    {
    yyerror("Missing ';'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 39:
#line 695 "language.yacc"
    { yyerror("Missing ';'."); }
    break;

  case 40:
#line 699 "language.yacc"
    {
    (yyval.n) = mknode(F_COMMA_EXPR,(yyvsp[(1) - (1)].n),mknode(F_RETURN,mkintnode(0),0));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(1) - (1)].n));
  }
    break;

  case 41:
#line 703 "language.yacc"
    { (yyval.n) = NULL; }
    break;

  case 42:
#line 704 "language.yacc"
    { yyerror("Expected ';'."); (yyval.n) = NULL; }
    break;

  case 43:
#line 705 "language.yacc"
    { (yyval.n) = NULL; }
    break;

  case 44:
#line 710 "language.yacc"
    {
#ifdef PIKE_DEBUG
    check_type_string((yyvsp[(1) - (1)].n)->u.sval.u.type);
#endif /* PIKE_DEBUG */
    if(Pike_compiler->compiler_frame->current_type)
      free_type(Pike_compiler->compiler_frame->current_type); 
    copy_pike_type(Pike_compiler->compiler_frame->current_type,
		   (yyvsp[(1) - (1)].n)->u.sval.u.type);
    free_node((yyvsp[(1) - (1)].n));
  }
    break;

  case 45:
#line 723 "language.yacc"
    {
    /* Used to hold line-number info */
    (yyval.n) = mkintnode(0);
  }
    break;

  case 46:
#line 730 "language.yacc"
    {
    /* Used to hold line-number info */
    (yyval.n) = mkintnode(0);
  }
    break;

  case 47:
#line 735 "language.yacc"
    {
    yyerror("Missing ')'.");
    /* Used to hold line-number info */
    (yyval.n) = mkintnode(0);
  }
    break;

  case 49:
#line 744 "language.yacc"
    {
    yyerror("Missing '}'.");
  }
    break;

  case 51:
#line 751 "language.yacc"
    {
    yyerror("Missing '}'.");
  }
    break;

  case 52:
#line 757 "language.yacc"
    {
    /* Used to hold line-number info */
    (yyval.n) = mkintnode(0);
  }
    break;

  case 54:
#line 765 "language.yacc"
    {
    yyerror("Missing ']'.");
  }
    break;

  case 55:
#line 771 "language.yacc"
    {
    push_compiler_frame(SCOPE_LOCAL);

    if(!Pike_compiler->compiler_frame->previous ||
       !Pike_compiler->compiler_frame->previous->current_type)
    {
      yyerror("Internal compiler error (push_compiler_frame0).");
      copy_pike_type(Pike_compiler->compiler_frame->current_type,
		mixed_type_string);
    }else{
      copy_pike_type(Pike_compiler->compiler_frame->current_type,
		Pike_compiler->compiler_frame->previous->current_type);
    }

    (yyval.ptr) = Pike_compiler->compiler_frame;
  }
    break;

  case 56:
#line 790 "language.yacc"
    {
    (yyval.number) = OPT_EXTERNAL_DEPEND|OPT_SIDE_EFFECT;
  }
    break;

  case 57:
#line 794 "language.yacc"
    {
    (yyval.number) = 0;
  }
    break;

  case 58:
#line 802 "language.yacc"
    {
    (yyval.number) = 0;
    /* Check for the (very special) case of create and create_args. */
    if (Pike_compiler->num_create_args) {
      struct pike_string *create_string = NULL;
      int e;
      MAKE_CONST_STRING(create_string, "create");
      if ((yyvsp[(6) - (8)].n)->u.sval.u.string == create_string) {
	if (TEST_COMPAT(7, 6)) {
	  yywarning("Having both an implicit and an explicit create() "
		    "was not supported in Pike 7.6 and before.");
	}
	/* Prepend the create arguments. */
	if (Pike_compiler->num_create_args < 0) {
	  Pike_compiler->varargs = 1;
	  for (e = 0; e < -Pike_compiler->num_create_args; e++) {
	    struct identifier *id =
	      Pike_compiler->new_program->identifiers + e;
	    add_ref(id->type);
	    add_local_name(empty_pike_string, id->type, 0);
	    /* Note: add_local_name() above will return e. */
	    Pike_compiler->compiler_frame->variable[e].flags |=
	      LOCAL_VAR_IS_USED;
	  }
	} else {
	  for (e = 0; e < Pike_compiler->num_create_args; e++) {
	    struct identifier *id =
	      Pike_compiler->new_program->identifiers + e;
	    add_ref(id->type);
	    add_local_name(empty_pike_string, id->type, 0);
	    /* Note: add_local_name() above will return e. */
	    Pike_compiler->compiler_frame->variable[e].flags |=
	      LOCAL_VAR_IS_USED;
	  }
	}
	(yyval.number) = e;
      }
    }
  }
    break;

  case 59:
#line 842 "language.yacc"
    {
    int e;

    /* Adjust opt_flags in case we've got an optional_constant. */
    Pike_compiler->compiler_frame->opt_flags = (yyvsp[(4) - (11)].number);

    /* construct the function type */
    push_finished_type(Pike_compiler->compiler_frame->current_type);
    if ((yyvsp[(5) - (11)].number) && (Pike_compiler->compiler_pass == 2) && !TEST_COMPAT (0, 6)) {
      yywarning("The *-syntax in types is obsolete. Use array instead.");
    }
    while(--(yyvsp[(5) - (11)].number)>=0) push_type(T_ARRAY);
    
    if(Pike_compiler->compiler_frame->current_return_type)
      free_type(Pike_compiler->compiler_frame->current_return_type);
    Pike_compiler->compiler_frame->current_return_type = compiler_pop_type();
    
    push_finished_type(Pike_compiler->compiler_frame->current_return_type);
    
    e = (yyvsp[(9) - (11)].number) + (yyvsp[(10) - (11)].number) - 1;
    if(Pike_compiler->varargs &&
       (!(yyvsp[(9) - (11)].number) || (Pike_compiler->num_create_args >= 0)))
    {
      push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
      e--;
      pop_type_stack(T_ARRAY);
    }else{
      push_type(T_VOID);
    }
    push_type(T_MANY);
    for(; e>=0; e--)
    {
      push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
      push_type(T_FUNCTION);
    }

    if ((yyvsp[(2) - (11)].n)) {
      node *n = (yyvsp[(2) - (11)].n);
      while (n) {
	push_type_attribute(CDR(n)->u.sval.u.string);
	n = CAR(n);
      }
    }

    {
      struct pike_type *s=compiler_pop_type();
      int i = isidentifier((yyvsp[(6) - (11)].n)->u.sval.u.string);

      if (Pike_compiler->compiler_pass == 1) {
	if ((yyvsp[(1) - (11)].number) & ID_VARIANT) {
	  /* FIXME: Lookup the type of any existing variant */
	  /* Or the types. */
	  fprintf(stderr, "Pass %d: Identifier %s:\n",
		  Pike_compiler->compiler_pass, (yyvsp[(6) - (11)].n)->u.sval.u.string->str);

	  if (i >= 0) {
	    struct identifier *id = ID_FROM_INT(Pike_compiler->new_program, i);
	    if (id) {
	      struct pike_type *new_type;
	      fprintf(stderr, "Defined, type:\n");
#ifdef PIKE_DEBUG
	      simple_describe_type(id->type);
#endif

	      new_type = or_pike_types(s, id->type, 1);
	      free_type(s);
	      s = new_type;

	      fprintf(stderr, "Resulting type:\n");
#ifdef PIKE_DEBUG
	      simple_describe_type(s);
#endif
	    } else {
	      my_yyerror("Lost identifier %S (%d).",
			 (yyvsp[(6) - (11)].n)->u.sval.u.string, i);
	    }
	  } else {
	    fprintf(stderr, "Not defined.\n");
	  }
	  fprintf(stderr, "New type:\n");
#ifdef PIKE_DEBUG
	  simple_describe_type(s);
#endif
	}
      } else {
	/* FIXME: Second pass reuses the type from the end of
	 * the first pass if this is a variant function.
	 */
	if (i >= 0) {
	  if (Pike_compiler->new_program->identifier_references[i].id_flags &
	      ID_VARIANT) {
	    struct identifier *id = ID_FROM_INT(Pike_compiler->new_program, i);
	    fprintf(stderr, "Pass %d: Identifier %s:\n",
		    Pike_compiler->compiler_pass, (yyvsp[(6) - (11)].n)->u.sval.u.string->str);

	    free_type(s);
	    copy_pike_type(s, id->type);

	    fprintf(stderr, "Resulting type:\n");
#ifdef PIKE_DEBUG
	    simple_describe_type(s);
#endif
	  }
	} else {
	  my_yyerror("Identifier %S lost after first pass.",
		     (yyvsp[(6) - (11)].n)->u.sval.u.string);
	}
      }

      (yyval.n) = mktypenode(s);
      free_type(s);
    }


/*    if(Pike_compiler->compiler_pass==1) */
    {
      /* FIXME:
       * set current_function_number for local functions as well
       */
      Pike_compiler->compiler_frame->current_function_number=
	define_function((yyvsp[(6) - (11)].n)->u.sval.u.string,
			(yyval.n)->u.sval.u.type,
			(yyvsp[(1) - (11)].number) & (~ID_EXTERN),
			IDENTIFIER_PIKE_FUNCTION |
			(Pike_compiler->varargs?IDENTIFIER_VARARGS:0),
			0,
			(yyvsp[(4) - (11)].number));

      Pike_compiler->varargs=0;

      if ((yyvsp[(1) - (11)].number) & ID_VARIANT) {
	fprintf(stderr, "Function number: %d\n",
		Pike_compiler->compiler_frame->current_function_number);
      }
    }
  }
    break;

  case 60:
#line 979 "language.yacc"
    {
    int e;
    if((yyvsp[(13) - (13)].n))
    {
      int f;
      node *check_args = NULL;
      struct compilation *c = THIS_COMPILATION;
      struct pike_string *save_file = c->lex.current_file;
      int save_line  = c->lex.current_line;
      int num_required_args = 0;
      struct identifier *i;
      c->lex.current_file = (yyvsp[(6) - (13)].n)->current_file;
      c->lex.current_line = (yyvsp[(6) - (13)].n)->line_number;

      if (((yyvsp[(1) - (13)].number) & ID_EXTERN) && (Pike_compiler->compiler_pass == 1)) {
	yywarning("Extern declared function definition.");
      }

      for(e=0; e<(yyvsp[(9) - (13)].number)+(yyvsp[(10) - (13)].number); e++)
      {
	if((e >= (yyvsp[(9) - (13)].number)) &&
	   (!Pike_compiler->compiler_frame->variable[e].name ||
	    !Pike_compiler->compiler_frame->variable[e].name->len))
	{
	  my_yyerror("Missing name for argument %d.", e - (yyvsp[(9) - (13)].number));
	} else {
	  if (Pike_compiler->compiler_pass == 2) {
	    if ((yyvsp[(1) - (13)].number) & ID_VARIANT) {
	      struct pike_type *arg_type =
		Pike_compiler->compiler_frame->variable[e].type;

	      /* FIXME: Generate code that checks the arguments. */
	      /* If there is a bad argument, call the fallback, and return. */
	      if (! pike_types_le(void_type_string, arg_type)) {
		/* Argument my not be void.
		 * ie it's required.
		 */
		num_required_args++;
	      }
	    } else {
	      /* FIXME: Should probably use some other flag. */
	      if ((runtime_options & RUNTIME_CHECK_TYPES) &&
		  (Pike_compiler->compiler_frame->variable[e].type !=
		   mixed_type_string)) {
		node *local_node;

		/* fprintf(stderr, "Creating soft cast node for local #%d\n", e);*/

		local_node = mklocalnode(e, 0);

		/* The following is needed to go around the optimization in
		 * mksoftcastnode().
		 */
		free_type(local_node->type);
		copy_pike_type(local_node->type, mixed_type_string);

		check_args =
		  mknode(F_COMMA_EXPR, check_args,
			 mksoftcastnode(Pike_compiler->compiler_frame->variable[e].type,
					local_node));
	      }
	    }
	  }
	}
      }

      if ((yyvsp[(1) - (13)].number) & ID_VARIANT) {
	struct pike_string *bad_arg_str;
	MAKE_CONST_STRING(bad_arg_str,
			  "Bad number of arguments!\n");

	fprintf(stderr, "Required args: %d\n", num_required_args);

	check_args =
	  mknode('?',
		 mkopernode("`<",
			    mkefuncallnode("query_num_arg", NULL),
			    mkintnode(num_required_args)),
		 mknode(':',
			mkefuncallnode("throw",
				       mkefuncallnode("aggregate",
						      mkstrnode(bad_arg_str))),
			NULL));
      }

      if ((yyvsp[(9) - (13)].number)) {
	/* Hook in the initializers for the create arguments. */
	for (e = (yyvsp[(9) - (13)].number); e--;) {
	  (yyvsp[(13) - (13)].n) = mknode(F_COMMA_EXPR,
		       mknode(F_POP_VALUE,
			      mknode(F_ASSIGN, mklocalnode(e, 0),
				     mkidentifiernode(e)), NULL),
		       (yyvsp[(13) - (13)].n));
	}
      }

      {
	int l = (yyvsp[(13) - (13)].n)->line_number;
	struct pike_string *f = (yyvsp[(13) - (13)].n)->current_file;
	if (check_args) {
	  /* Prepend the arg checking code. */
	  (yyvsp[(13) - (13)].n) = mknode(F_COMMA_EXPR, mknode(F_POP_VALUE, check_args, NULL), (yyvsp[(13) - (13)].n));
	}
	c->lex.current_line = l;
	c->lex.current_file = f;
      }

      f=dooptcode((yyvsp[(6) - (13)].n)->u.sval.u.string, (yyvsp[(13) - (13)].n), (yyvsp[(12) - (13)].n)->u.sval.u.type, (yyvsp[(1) - (13)].number));

      i = ID_FROM_INT(Pike_compiler->new_program, f);
      i->opt_flags = Pike_compiler->compiler_frame->opt_flags;

      if ((yyvsp[(1) - (13)].number) & ID_VARIANT) {
	fprintf(stderr, "Function number: %d\n", f);
      }

#ifdef PIKE_DEBUG
      if(Pike_interpreter.recoveries &&
	 ((Pike_sp - Pike_interpreter.evaluator_stack) <
	  Pike_interpreter.recoveries->stack_pointer))
	Pike_fatal("Stack error (underflow)\n");

      if((Pike_compiler->compiler_pass == 1) &&
	 (f != Pike_compiler->compiler_frame->current_function_number)) {
	fprintf(stderr, "define_function()/do_opt_code() failed for symbol %s\n",
		(yyvsp[(6) - (13)].n)->u.sval.u.string->str);
	dump_program_desc(Pike_compiler->new_program);
	Pike_fatal("define_function screwed up! %d != %d\n",
	      f, Pike_compiler->compiler_frame->current_function_number);
      }
#endif

      c->lex.current_line = save_line;
      c->lex.current_file = save_file;
    } else {
      /* Prototype; don't warn about unused arguments. */
      for (e = Pike_compiler->compiler_frame->current_number_of_locals; e--;) {
	Pike_compiler->compiler_frame->variable[e].flags |= LOCAL_VAR_IS_USED;
      }
    }
#ifdef PIKE_DEBUG
    if (Pike_compiler->compiler_frame != (yyvsp[(7) - (13)].ptr)) {
      Pike_fatal("Lost track of compiler_frame!\n"
		 "  Got: %p (Expected: %p) Previous: %p\n",
		 Pike_compiler->compiler_frame, (yyvsp[(7) - (13)].ptr),
		 Pike_compiler->compiler_frame->previous);
    }
#endif
    pop_compiler_frame();
    free_node((yyvsp[(6) - (13)].n));
    free_node((yyvsp[(11) - (13)].n));
    free_node((yyvsp[(12) - (13)].n));
    if ((yyvsp[(2) - (13)].n)) free_node((yyvsp[(2) - (13)].n));
  }
    break;

  case 61:
#line 1135 "language.yacc"
    {
#ifdef PIKE_DEBUG
    if (Pike_compiler->compiler_frame != (yyvsp[(7) - (8)].ptr)) {
      Pike_fatal("Lost track of compiler_frame!\n"
		 "  Got: %p (Expected: %p) Previous: %p\n",
		 Pike_compiler->compiler_frame, (yyvsp[(7) - (8)].ptr),
		 Pike_compiler->compiler_frame->previous);
    }
#endif
    pop_compiler_frame();
    free_node((yyvsp[(6) - (8)].n));
    if ((yyvsp[(2) - (8)].n))
      free_node((yyvsp[(2) - (8)].n));
  }
    break;

  case 62:
#line 1150 "language.yacc"
    {
    if ((yyvsp[(2) - (6)].n))
      free_node((yyvsp[(2) - (6)].n));
    compiler_discard_type();
  }
    break;

  case 63:
#line 1156 "language.yacc"
    {
    if ((yyvsp[(11) - (11)].n)) free_node((yyvsp[(11) - (11)].n));
  }
    break;

  case 64:
#line 1160 "language.yacc"
    {
    if ((yyvsp[(2) - (6)].n)) {
      yyerror("Invalid use of attributes in variable declaration.\n");
      free_node((yyvsp[(2) - (6)].n));
    }
  }
    break;

  case 65:
#line 1166 "language.yacc"
    {}
    break;

  case 66:
#line 1167 "language.yacc"
    {}
    break;

  case 67:
#line 1168 "language.yacc"
    {}
    break;

  case 68:
#line 1169 "language.yacc"
    {}
    break;

  case 69:
#line 1170 "language.yacc"
    { free_node((yyvsp[(2) - (2)].n)); }
    break;

  case 70:
#line 1171 "language.yacc"
    { free_node((yyvsp[(2) - (2)].n)); }
    break;

  case 71:
#line 1172 "language.yacc"
    {}
    break;

  case 72:
#line 1174 "language.yacc"
    {
    reset_type_stack();
    yyerror("Missing ';'.");
    yyerror("Unexpected end of file");
  }
    break;

  case 73:
#line 1180 "language.yacc"
    {
    reset_type_stack();
    yyerrok;
/*     if(Pike_compiler->num_parse_error>5) YYACCEPT; */
  }
    break;

  case 74:
#line 1186 "language.yacc"
    {
    reset_type_stack();
    yyerror("Missing ';'.");
   /* yychar = '}';	*/ /* Put the '}' back on the input stream */
  }
    break;

  case 75:
#line 1193 "language.yacc"
    {
      (yyval.number)=THIS_COMPILATION->lex.pragmas;
      THIS_COMPILATION->lex.pragmas|=(yyvsp[(1) - (2)].number);
    }
    break;

  case 76:
#line 1199 "language.yacc"
    {
      THIS_COMPILATION->lex.pragmas=(yyvsp[(3) - (5)].number);
    }
    break;

  case 77:
#line 1204 "language.yacc"
    { (yyval.number)=1; }
    break;

  case 78:
#line 1206 "language.yacc"
    {
    yyerror("Range indicator ('..') where elipsis ('...') expected.");
    (yyval.number)=1;
  }
    break;

  case 79:
#line 1210 "language.yacc"
    { (yyval.number)=0; }
    break;

  case 81:
#line 1214 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 82:
#line 1215 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 83:
#line 1219 "language.yacc"
    {
    int i;
    if(Pike_compiler->varargs) yyerror("Can't define more arguments after ...");

    if((yyvsp[(2) - (3)].number))
    {
      push_type(T_ARRAY);
      Pike_compiler->varargs=1;
    }

    if(!(yyvsp[(3) - (3)].n))
    {
      (yyvsp[(3) - (3)].n)=mkstrnode(empty_pike_string);
    }

    if((yyvsp[(3) - (3)].n)->u.sval.u.string->len &&
       islocal((yyvsp[(3) - (3)].n)->u.sval.u.string) >= 0)
      my_yyerror("Variable %S appears twice in argument list.",
		 (yyvsp[(3) - (3)].n)->u.sval.u.string);
    
    i = add_local_name((yyvsp[(3) - (3)].n)->u.sval.u.string, compiler_pop_type(),0);
    if (i >= 0) {
      /* Don't warn about unused arguments. */
      Pike_compiler->compiler_frame->variable[i].flags |= LOCAL_VAR_IS_USED;
    }
    free_node((yyvsp[(3) - (3)].n));
  }
    break;

  case 84:
#line 1249 "language.yacc"
    {
    free_node((yyvsp[(3) - (3)].n));
    (yyval.number)=(yyvsp[(2) - (3)].number);
  }
    break;

  case 85:
#line 1255 "language.yacc"
    { (yyval.number)=0; }
    break;

  case 87:
#line 1259 "language.yacc"
    { (yyval.number) = 1; }
    break;

  case 88:
#line 1260 "language.yacc"
    { (yyval.number) = (yyvsp[(1) - (3)].number) + 1; }
    break;

  case 89:
#line 1262 "language.yacc"
    {
    yyerror("Unexpected ':' in argument list.");
    (yyval.number) = (yyvsp[(1) - (3)].number) + 1;
  }
    break;

  case 90:
#line 1270 "language.yacc"
    {
      (yyval.number) = ID_FINAL | ID_INLINE;
      if( !(THIS_COMPILATION->lex.pragmas & ID_NO_DEPRECATION_WARNINGS) &&
          !TEST_COMPAT(7, 6) && Pike_compiler->compiler_pass==1 )
        yywarning("Keyword nomask is deprecated in favor for 'final'.");

    }
    break;

  case 91:
#line 1277 "language.yacc"
    { (yyval.number) = ID_FINAL | ID_INLINE; }
    break;

  case 92:
#line 1278 "language.yacc"
    { (yyval.number) = ID_PROTECTED; }
    break;

  case 93:
#line 1279 "language.yacc"
    { (yyval.number) = ID_EXTERN; }
    break;

  case 94:
#line 1280 "language.yacc"
    { (yyval.number) = ID_OPTIONAL; }
    break;

  case 95:
#line 1281 "language.yacc"
    { (yyval.number) = ID_PRIVATE | ID_PROTECTED; }
    break;

  case 96:
#line 1282 "language.yacc"
    { (yyval.number) = ID_INLINE; }
    break;

  case 97:
#line 1283 "language.yacc"
    { (yyval.number) = ID_PUBLIC; }
    break;

  case 98:
#line 1284 "language.yacc"
    { (yyval.number) = ID_PROTECTED; }
    break;

  case 99:
#line 1285 "language.yacc"
    { (yyval.number) = ID_INLINE; }
    break;

  case 100:
#line 1286 "language.yacc"
    { (yyval.number) = ID_VARIANT; }
    break;

  case 101:
#line 1290 "language.yacc"
    { (yyval.str) = "nomask"; }
    break;

  case 102:
#line 1291 "language.yacc"
    { (yyval.str) = "final"; }
    break;

  case 103:
#line 1292 "language.yacc"
    { (yyval.str) = "static"; }
    break;

  case 104:
#line 1293 "language.yacc"
    { (yyval.str) = "extern"; }
    break;

  case 105:
#line 1294 "language.yacc"
    { (yyval.str) = "private"; }
    break;

  case 106:
#line 1295 "language.yacc"
    { (yyval.str) = "local"; }
    break;

  case 107:
#line 1296 "language.yacc"
    { (yyval.str) = "public"; }
    break;

  case 108:
#line 1297 "language.yacc"
    { (yyval.str) = "protected"; }
    break;

  case 109:
#line 1298 "language.yacc"
    { (yyval.str) = "inline"; }
    break;

  case 110:
#line 1299 "language.yacc"
    { (yyval.str) = "optional"; }
    break;

  case 111:
#line 1300 "language.yacc"
    { (yyval.str) = "variant"; }
    break;

  case 112:
#line 1304 "language.yacc"
    { (yyval.str) = "void"; }
    break;

  case 113:
#line 1305 "language.yacc"
    { (yyval.str) = "mixed"; }
    break;

  case 114:
#line 1306 "language.yacc"
    { (yyval.str) = "array"; }
    break;

  case 115:
#line 1307 "language.yacc"
    { (yyval.str) = "__attribute__"; }
    break;

  case 116:
#line 1308 "language.yacc"
    { (yyval.str) = "__deprecated__"; }
    break;

  case 117:
#line 1309 "language.yacc"
    { (yyval.str) = "mapping"; }
    break;

  case 118:
#line 1310 "language.yacc"
    { (yyval.str) = "multiset"; }
    break;

  case 119:
#line 1311 "language.yacc"
    { (yyval.str) = "object"; }
    break;

  case 120:
#line 1312 "language.yacc"
    { (yyval.str) = "function"; }
    break;

  case 121:
#line 1313 "language.yacc"
    { (yyval.str) = "__func__"; }
    break;

  case 122:
#line 1314 "language.yacc"
    { (yyval.str) = "program"; }
    break;

  case 123:
#line 1315 "language.yacc"
    { (yyval.str) = "string"; }
    break;

  case 124:
#line 1316 "language.yacc"
    { (yyval.str) = "float"; }
    break;

  case 125:
#line 1317 "language.yacc"
    { (yyval.str) = "int"; }
    break;

  case 126:
#line 1318 "language.yacc"
    { (yyval.str) = "enum"; }
    break;

  case 127:
#line 1319 "language.yacc"
    { (yyval.str) = "typedef"; }
    break;

  case 128:
#line 1323 "language.yacc"
    { (yyval.str) = "if"; }
    break;

  case 129:
#line 1324 "language.yacc"
    { (yyval.str) = "do"; }
    break;

  case 130:
#line 1325 "language.yacc"
    { (yyval.str) = "for"; }
    break;

  case 131:
#line 1326 "language.yacc"
    { (yyval.str) = "while"; }
    break;

  case 132:
#line 1327 "language.yacc"
    { (yyval.str) = "else"; }
    break;

  case 133:
#line 1328 "language.yacc"
    { (yyval.str) = "foreach"; }
    break;

  case 134:
#line 1329 "language.yacc"
    { (yyval.str) = "catch"; }
    break;

  case 135:
#line 1330 "language.yacc"
    { (yyval.str) = "gauge"; }
    break;

  case 136:
#line 1331 "language.yacc"
    { (yyval.str) = "class"; }
    break;

  case 137:
#line 1332 "language.yacc"
    { (yyval.str) = "break"; }
    break;

  case 138:
#line 1333 "language.yacc"
    { (yyval.str) = "case"; }
    break;

  case 139:
#line 1334 "language.yacc"
    { (yyval.str) = "constant"; }
    break;

  case 140:
#line 1335 "language.yacc"
    { (yyval.str) = "continue"; }
    break;

  case 141:
#line 1336 "language.yacc"
    { (yyval.str) = "default"; }
    break;

  case 142:
#line 1337 "language.yacc"
    { (yyval.str) = "facet"; }
    break;

  case 143:
#line 1338 "language.yacc"
    { (yyval.str) = "import"; }
    break;

  case 144:
#line 1339 "language.yacc"
    { (yyval.str) = "inherit"; }
    break;

  case 145:
#line 1340 "language.yacc"
    { (yyval.str) = "lambda"; }
    break;

  case 146:
#line 1341 "language.yacc"
    { (yyval.str) = "predef"; }
    break;

  case 147:
#line 1342 "language.yacc"
    { (yyval.str) = "return"; }
    break;

  case 148:
#line 1343 "language.yacc"
    { (yyval.str) = "sscanf"; }
    break;

  case 149:
#line 1344 "language.yacc"
    { (yyval.str) = "switch"; }
    break;

  case 150:
#line 1345 "language.yacc"
    { (yyval.str) = "typeof"; }
    break;

  case 151:
#line 1346 "language.yacc"
    { (yyval.str) = "global"; }
    break;

  case 157:
#line 1353 "language.yacc"
    {
    struct pike_string *tmp=make_shared_string((yyvsp[(1) - (1)].str));
    (yyval.n)=mkstrnode(tmp);
    free_string(tmp);
  }
    break;

  case 158:
#line 1361 "language.yacc"
    {
   (yyval.number)=Pike_compiler->current_modifiers=(yyvsp[(1) - (1)].number) |
     (THIS_COMPILATION->lex.pragmas & ID_MODIFIER_MASK);
 }
    break;

  case 159:
#line 1367 "language.yacc"
    { (yyval.number) = 0; }
    break;

  case 160:
#line 1368 "language.yacc"
    { (yyval.number) = (yyvsp[(1) - (2)].number) | (yyvsp[(2) - (2)].number); }
    break;

  case 161:
#line 1372 "language.yacc"
    {
    (yyval.n) = (yyvsp[(3) - (5)].n);
  }
    break;

  case 162:
#line 1376 "language.yacc"
    {
    struct pike_string *deprecated_string;
    MAKE_CONST_STRING(deprecated_string, "deprecated");
    (yyval.n) = mkstrnode(deprecated_string);
  }
    break;

  case 163:
#line 1382 "language.yacc"
    {
    struct pike_string *deprecated_string;
    MAKE_CONST_STRING(deprecated_string, "deprecated");
    (yyval.n) = mkstrnode(deprecated_string);
  }
    break;

  case 164:
#line 1389 "language.yacc"
    { (yyval.n) = 0; }
    break;

  case 165:
#line 1390 "language.yacc"
    { (yyval.n) = mknode(F_ARG_LIST, (yyvsp[(1) - (2)].n), (yyvsp[(2) - (2)].n)); }
    break;

  case 166:
#line 1393 "language.yacc"
    { (yyval.number)=(yyvsp[(1) - (2)].number) + 1; }
    break;

  case 167:
#line 1394 "language.yacc"
    { (yyval.number)=0; }
    break;

  case 168:
#line 1398 "language.yacc"
    {
      struct pike_type *s = compiler_pop_type();
      (yyval.n) = mktypenode(s);
      free_type(s);
      COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(1) - (3)].n));
      free_node ((yyvsp[(1) - (3)].n));
    }
    break;

  case 169:
#line 1408 "language.yacc"
    {
      struct pike_type *s = compiler_pop_type();
      (yyval.n) = mktypenode(s);
      free_type(s);
      COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(1) - (3)].n));
      free_node ((yyvsp[(1) - (3)].n));
    }
    break;

  case 171:
#line 1419 "language.yacc"
    {
    if (Pike_compiler->compiler_pass == 2 && !TEST_COMPAT (0, 6)) {
       yywarning("The *-syntax in types is obsolete. Use array instead.");
    }
    push_type(T_ARRAY);
  }
    break;

  case 174:
#line 1430 "language.yacc"
    {
    if (Pike_compiler->compiler_pass == 2 && !TEST_COMPAT (0, 6)) {
       yywarning("The *-syntax in types is obsolete. Use array instead.");
    }
    push_type(T_ARRAY);
  }
    break;

  case 176:
#line 1440 "language.yacc"
    {
    if (Pike_compiler->compiler_pass == 2 && !TEST_COMPAT (0, 6)) {
      yywarning("The *-syntax in types is obsolete. Use array instead.");
    }
    push_type(T_ARRAY);
  }
    break;

  case 178:
#line 1450 "language.yacc"
    {
    struct pike_type *s = compiler_pop_type();
    (yyval.n) = mktypenode(s);
#ifdef PIKE_DEBUG
    if ((yyval.n)->u.sval.u.type != s) {
      Pike_fatal("mktypenode(%p) created node with %p\n", s, (yyval.n)->u.sval.u.type);
    }
#endif /* PIKE_DEBUG */
    free_type(s);
  }
    break;

  case 179:
#line 1463 "language.yacc"
    {
    struct pike_type *s = compiler_pop_type();
    (yyval.n) = mktypenode(s);
#ifdef PIKE_DEBUG
    if ((yyval.n)->u.sval.u.type != s) {
      Pike_fatal("mktypenode(%p) created node with %p\n", s, (yyval.n)->u.sval.u.type);
    }
#endif /* PIKE_DEBUG */
    free_type(s);
  }
    break;

  case 180:
#line 1476 "language.yacc"
    {
    struct pike_type *s = compiler_pop_type();
    (yyval.n) = mktypenode(s);
#ifdef PIKE_DEBUG
    if ((yyval.n)->u.sval.u.type != s) {
      Pike_fatal("mktypenode(%p) created node with %p\n", s, (yyval.n)->u.sval.u.type);
    }
#endif /* PIKE_DEBUG */
    free_type(s);
  }
    break;

  case 181:
#line 1488 "language.yacc"
    { push_type(T_OR); }
    break;

  case 183:
#line 1492 "language.yacc"
    { push_type(T_OR); }
    break;

  case 187:
#line 1499 "language.yacc"
    { push_type(T_FLOAT); }
    break;

  case 188:
#line 1500 "language.yacc"
    { push_type(T_VOID); }
    break;

  case 189:
#line 1501 "language.yacc"
    { push_type(T_MIXED); }
    break;

  case 190:
#line 1502 "language.yacc"
    {}
    break;

  case 191:
#line 1503 "language.yacc"
    {}
    break;

  case 192:
#line 1504 "language.yacc"
    {}
    break;

  case 193:
#line 1505 "language.yacc"
    {}
    break;

  case 194:
#line 1506 "language.yacc"
    {}
    break;

  case 195:
#line 1507 "language.yacc"
    { push_type(T_PROGRAM); }
    break;

  case 196:
#line 1508 "language.yacc"
    { push_type(T_ARRAY); }
    break;

  case 197:
#line 1509 "language.yacc"
    { push_type(T_MULTISET); }
    break;

  case 198:
#line 1511 "language.yacc"
    {
    push_type_attribute((yyvsp[(3) - (6)].n)->u.sval.u.string);
    free_node((yyvsp[(3) - (6)].n));
  }
    break;

  case 199:
#line 1516 "language.yacc"
    {
    push_type(T_MIXED);
    push_type_attribute((yyvsp[(3) - (5)].n)->u.sval.u.string);
    free_node((yyvsp[(3) - (5)].n));
  }
    break;

  case 200:
#line 1522 "language.yacc"
    {
    push_type(T_MIXED);
  }
    break;

  case 201:
#line 1526 "language.yacc"
    {
    struct pike_string *deprecated_string;
    MAKE_CONST_STRING(deprecated_string, "deprecated");
    push_type_attribute(deprecated_string);
  }
    break;

  case 202:
#line 1532 "language.yacc"
    {
    struct pike_string *deprecated_string;
    MAKE_CONST_STRING(deprecated_string, "deprecated");
    push_type(T_MIXED);
    push_type_attribute(deprecated_string);
  }
    break;

  case 203:
#line 1541 "language.yacc"
    { 
    resolv_constant((yyvsp[(1) - (1)].n));

    if (Pike_sp[-1].type == T_TYPE) {
      /* "typedef" */
      push_finished_type(Pike_sp[-1].u.type);
    } else {
      /* object type */
      struct program *p = NULL;

      if (Pike_sp[-1].type == T_OBJECT) {
	if(!(p = Pike_sp[-1].u.object->prog))
	{
	  pop_stack();
	  push_int(0);
	  yyerror("Destructed object used as program identifier.");
	}else{
	  int f = FIND_LFUN(p->inherits[Pike_sp[-1].subtype].prog, LFUN_CALL);
	  if(f!=-1)
	  {
	    Pike_sp[-1].subtype =
	      f + p->inherits[Pike_sp[-1].subtype].identifier_level;
	    Pike_sp[-1].type=T_FUNCTION;
	  }else{
	    extern void f_object_program(INT32);
	    if (Pike_compiler->compiler_pass == 2 && !TEST_COMPAT (7, 4)) {
	      yywarning("Using object as program identifier.");
	    }
	    f_object_program(1);
	  }
	}
      }

      switch(Pike_sp[-1].type) {
	case T_FUNCTION:
	if((p = program_from_function(Pike_sp-1))) {
	  push_object_type(0, p?(p->id):0);
	  break;
	} else {
	  /* Attempt to get the return type for the function. */
	  struct pike_type *a, *b;
	  a = get_type_of_svalue(Pike_sp-1);
	  /* Note: check_splice_call() below eats a reference from a.
	   * Note: CALL_INHIBIT_WARNINGS is needed since we don't
	   *       provide a function name (and we don't want
	   *       warnings here anyway).
	   */
	  a = check_splice_call(NULL, a, 0, mixed_type_string, NULL,
				CALL_INHIBIT_WARNINGS);
	  if (a) {
	    b = new_get_return_type(a, 0);
	    free_type(a);
	    if (b) {
	      push_finished_type(b);
	      free_type(b);
	      break;
	    }
	  }
	}
	/* FALL_THROUGH */
      
      default:
	if (Pike_compiler->compiler_pass!=1)
	  my_yyerror("Illegal program identifier: %O.", Pike_sp-1);
	pop_stack();
	push_int(0);
	push_object_type(0, 0);
	break;
	
      case T_PROGRAM:
	p = Pike_sp[-1].u.program;
	push_object_type(0, p?(p->id):0);
	break;
      }
    }
    /* Attempt to name the type. */
    if (Pike_compiler->last_identifier) {
      push_type_name(Pike_compiler->last_identifier);
    }
    pop_stack();
    free_node((yyvsp[(1) - (1)].n));
  }
    break;

  case 204:
#line 1626 "language.yacc"
    {
    (yyval.n) = mkintnode(MAX_INT_TYPE);
  }
    break;

  case 206:
#line 1631 "language.yacc"
    {
#ifdef PIKE_DEBUG
    if (((yyvsp[(2) - (2)].n)->token != F_CONSTANT) || ((yyvsp[(2) - (2)].n)->u.sval.type != T_INT)) {
      Pike_fatal("Unexpected number in negative int-range.\n");
    }
#endif /* PIKE_DEBUG */
    (yyval.n) = mkintnode(-((yyvsp[(2) - (2)].n)->u.sval.u.integer));
    free_node((yyvsp[(2) - (2)].n));
  }
    break;

  case 207:
#line 1643 "language.yacc"
    {
    (yyval.n) = mkintnode(MIN_INT_TYPE);
  }
    break;

  case 209:
#line 1648 "language.yacc"
    {
#ifdef PIKE_DEBUG
    if (((yyvsp[(2) - (2)].n)->token != F_CONSTANT) || ((yyvsp[(2) - (2)].n)->u.sval.type != T_INT)) {
      Pike_fatal("Unexpected number in negative int-range.\n");
    }
#endif /* PIKE_DEBUG */
    (yyval.n) = mkintnode(-((yyvsp[(2) - (2)].n)->u.sval.u.integer));
    free_node((yyvsp[(2) - (2)].n));
  }
    break;

  case 211:
#line 1661 "language.yacc"
    {
    yyerror("Elipsis ('...') where range indicator ('..') expected.");
  }
    break;

  case 212:
#line 1667 "language.yacc"
    {
    push_int_type(MIN_INT_TYPE, MAX_INT_TYPE);
  }
    break;

  case 213:
#line 1671 "language.yacc"
    {
    INT_TYPE min = MIN_INT_TYPE;
    INT_TYPE max = MAX_INT_TYPE;

    /* FIXME: Check that $4 is >= $2. */
    if((yyvsp[(4) - (5)].n)->token == F_CONSTANT) {
      if ((yyvsp[(4) - (5)].n)->u.sval.type == T_INT) {
	max = (yyvsp[(4) - (5)].n)->u.sval.u.integer;
#ifdef AUTO_BIGNUM
      } else if (is_bignum_object_in_svalue(&(yyvsp[(4) - (5)].n)->u.sval)) {
	push_int(0);
	if (is_lt(&(yyvsp[(4) - (5)].n)->u.sval, Pike_sp-1)) {
	  max = MIN_INT_TYPE;
	}
	pop_stack();
#endif /* AUTO_BIGNUM */
      }
    }

    if((yyvsp[(2) - (5)].n)->token == F_CONSTANT) {
      if ((yyvsp[(2) - (5)].n)->u.sval.type == T_INT) {
	min = (yyvsp[(2) - (5)].n)->u.sval.u.integer;
#ifdef AUTO_BIGNUM
      } else if (is_bignum_object_in_svalue(&(yyvsp[(2) - (5)].n)->u.sval)) {
	push_int(0);
	if (is_lt(Pike_sp-1, &(yyvsp[(2) - (5)].n)->u.sval)) {
	  min = MAX_INT_TYPE;
	}
	pop_stack();
#endif /* AUTO_BIGNUM */
      }
    }

    push_int_type(min, max);

    free_node((yyvsp[(2) - (5)].n));
    free_node((yyvsp[(4) - (5)].n));
  }
    break;

  case 214:
#line 1710 "language.yacc"
    {
    push_int_type(MIN_INT32, MAX_INT32);
    yyerror("Expected integer range.");
  }
    break;

  case 215:
#line 1717 "language.yacc"
    {
    push_type(T_STRING);
  }
    break;

  case 216:
#line 1722 "language.yacc"
    { push_object_type(0, 0); }
    break;

  case 217:
#line 1723 "language.yacc"
    {
#ifdef PIKE_DEBUG
      (yyval.ptr) = Pike_sp;
#endif /* PIKE_DEBUG */
    }
    break;

  case 218:
#line 1729 "language.yacc"
    {
    /* NOTE: On entry, there are two items on the stack:
     *   Pike_sp-2:	Name of the program reference (string).
     *   Pike_sp-1:	The resolved program (program|function|zero).
     */
    struct program *p=program_from_svalue(Pike_sp-1);

#ifdef PIKE_DEBUG
    if ((yyvsp[(1) - (4)].ptr) != (Pike_sp - 2)) {
      Pike_fatal("Unexpected stack depth: %p != %p\n",
		 (yyvsp[(1) - (4)].n), Pike_sp-2);
    }
#endif /* PIKE_DEBUG */

    if(!p) {
      if (Pike_compiler->compiler_pass!=1) {
	my_yyerror("Not a valid program specifier: %S", Pike_sp[-2].u.string);
      }
    }
    push_object_type(0, p?(p->id):0);
    /* Attempt to name the type. */
    if (Pike_sp[-2].type == T_STRING) {
      push_type_name(Pike_sp[-2].u.string);
    }
    pop_n_elems(2);
  }
    break;

  case 219:
#line 1758 "language.yacc"
    {
    type_stack_mark();
  }
    break;

  case 220:
#line 1762 "language.yacc"
    {
    /* Add the many type if there is none. */
    if ((yyvsp[(4) - (5)].number))
    {
      if (!(yyvsp[(3) - (5)].number)) {
	/* function_type_list ends with a comma, or is empty.
	 * FIXME: Should this be a syntax error or not?
	 */
	if (Pike_compiler->compiler_pass == 1) {
	  yyerror("Missing type before ... .");
	}
	push_type(T_MIXED);
      }
    }else{
      push_type(T_VOID);
    }
  }
    break;

  case 221:
#line 1780 "language.yacc"
    {
    push_reverse_type(T_MANY);
    Pike_compiler->pike_type_mark_stackp--;
    while (*Pike_compiler->pike_type_mark_stackp+1 <
	   Pike_compiler->type_stackp) {
      push_reverse_type(T_FUNCTION);
    }
  }
    break;

  case 222:
#line 1789 "language.yacc"
    {
   push_type(T_MIXED);
   push_type(T_VOID);
   push_type(T_OR);

   push_type(T_ZERO);
   push_type(T_VOID);
   push_type(T_OR);

   push_type(T_MANY);
  }
    break;

  case 223:
#line 1802 "language.yacc"
    { (yyval.number)=0; }
    break;

  case 224:
#line 1803 "language.yacc"
    { (yyval.number)=!(yyvsp[(2) - (2)].number); }
    break;

  case 225:
#line 1806 "language.yacc"
    { (yyval.number)=1; }
    break;

  case 226:
#line 1808 "language.yacc"
    {
  }
    break;

  case 229:
#line 1814 "language.yacc"
    { push_type(T_MIXED); }
    break;

  case 230:
#line 1818 "language.yacc"
    { 
  }
    break;

  case 231:
#line 1821 "language.yacc"
    { 
  }
    break;

  case 232:
#line 1824 "language.yacc"
    { 
    push_reverse_type(T_MAPPING);
  }
    break;

  case 234:
#line 1829 "language.yacc"
    {
    push_type(T_MIXED);
    push_type(T_MIXED);
    push_type(T_MAPPING);
  }
    break;

  case 237:
#line 1842 "language.yacc"
    {
    struct pike_type *type;
    push_finished_type(Pike_compiler->compiler_frame->current_type);
    if ((yyvsp[(1) - (2)].number) && (Pike_compiler->compiler_pass == 2) && !TEST_COMPAT (0, 6)) {
      yywarning("The *-syntax in types is obsolete. Use array instead.");
    }
    while((yyvsp[(1) - (2)].number)--) push_type(T_ARRAY);
    type=compiler_pop_type();
    define_variable((yyvsp[(2) - (2)].n)->u.sval.u.string, type,
		    Pike_compiler->current_modifiers);
    free_type(type);
    free_node((yyvsp[(2) - (2)].n));
  }
    break;

  case 238:
#line 1855 "language.yacc"
    {}
    break;

  case 239:
#line 1857 "language.yacc"
    {
    struct pike_type *type;
    push_finished_type(Pike_compiler->compiler_frame->current_type);
    if ((yyvsp[(1) - (3)].number) && (Pike_compiler->compiler_pass == 2) && !TEST_COMPAT (0, 6)) {
      yywarning("The *-syntax in types is obsolete. Use array instead.");
    }
    while((yyvsp[(1) - (3)].number)--) push_type(T_ARRAY);
    type=compiler_pop_type();
    if ((Pike_compiler->current_modifiers & ID_EXTERN) &&
	(Pike_compiler->compiler_pass == 1)) {
      yywarning("Extern declared variable has initializer.");
    }
    (yyval.number)=define_variable((yyvsp[(2) - (3)].n)->u.sval.u.string, type,
			       Pike_compiler->current_modifiers & (~ID_EXTERN));
    free_type(type);
  }
    break;

  case 240:
#line 1874 "language.yacc"
    {
    Pike_compiler->init_node=mknode(F_COMMA_EXPR,Pike_compiler->init_node,
		     mkcastnode(void_type_string,
				mknode(F_ASSIGN,(yyvsp[(5) - (5)].n),
				       mkidentifiernode((yyvsp[(4) - (5)].number)))));
    free_node((yyvsp[(2) - (5)].n));
  }
    break;

  case 241:
#line 1882 "language.yacc"
    {
    free_node((yyvsp[(2) - (4)].n));
  }
    break;

  case 242:
#line 1886 "language.yacc"
    {
    yyerror("Unexpected end of file in variable definition.");
    free_node((yyvsp[(2) - (4)].n));
  }
    break;

  case 243:
#line 1891 "language.yacc"
    {
    free_node((yyvsp[(4) - (4)].n));
  }
    break;

  case 244:
#line 1898 "language.yacc"
    {
    int id;
    push_finished_type((yyvsp[(0) - (2)].n)->u.sval.u.type);
    if ((yyvsp[(1) - (2)].number) && (Pike_compiler->compiler_pass == 2) && !TEST_COMPAT (0, 6)) {
      yywarning("The *-syntax in types is obsolete. Use array instead.");
    }
    while((yyvsp[(1) - (2)].number)--) push_type(T_ARRAY);
    id = add_local_name((yyvsp[(2) - (2)].n)->u.sval.u.string, compiler_pop_type(),0);
    if (id >= 0) {
      /* FIXME: Consider using mklocalnode(id, -1). */
      (yyval.n)=mknode(F_ASSIGN,mkintnode(0),mklocalnode(id,0));
    } else
      (yyval.n) = 0;
    free_node((yyvsp[(2) - (2)].n));
  }
    break;

  case 245:
#line 1913 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 246:
#line 1915 "language.yacc"
    {
    int id;
    push_finished_type((yyvsp[(0) - (4)].n)->u.sval.u.type);
    if ((yyvsp[(1) - (4)].number) && (Pike_compiler->compiler_pass == 2) && !TEST_COMPAT (0, 6)) {
      yywarning("The *-syntax in types is obsolete. Use array instead.");
    }
    while((yyvsp[(1) - (4)].number)--) push_type(T_ARRAY);
    id = add_local_name((yyvsp[(2) - (4)].n)->u.sval.u.string, compiler_pop_type(),0);
    if (id >= 0) {
      if (!(THIS_COMPILATION->lex.pragmas & ID_STRICT_TYPES)) {
	/* Only warn about unused initialized variables in strict types mode. */
	Pike_compiler->compiler_frame->variable[id].flags |= LOCAL_VAR_IS_USED;
      }
      (yyval.n)=mknode(F_ASSIGN,(yyvsp[(4) - (4)].n),mklocalnode(id,0));
    } else
      (yyval.n) = 0;
    free_node((yyvsp[(2) - (4)].n));
  }
    break;

  case 247:
#line 1934 "language.yacc"
    {
    free_node((yyvsp[(4) - (4)].n));
    (yyval.n)=0;
  }
    break;

  case 248:
#line 1939 "language.yacc"
    {
    free_node((yyvsp[(2) - (4)].n));
    /* No yyerok here since we aren't done yet. */
    (yyval.n)=0;
  }
    break;

  case 249:
#line 1945 "language.yacc"
    {
    yyerror("Unexpected end of file in local variable definition.");
    free_node((yyvsp[(2) - (4)].n));
    /* No yyerok here since we aren't done yet. */
    (yyval.n)=0;
  }
    break;

  case 250:
#line 1954 "language.yacc"
    {
    int id;
    add_ref((yyvsp[(0) - (1)].n)->u.sval.u.type);
    id = add_local_name((yyvsp[(1) - (1)].n)->u.sval.u.string, (yyvsp[(0) - (1)].n)->u.sval.u.type, 0);
    if (id >= 0) {
      /* FIXME: Consider using mklocalnode(id, -1). */
      (yyval.n)=mknode(F_ASSIGN,mkintnode(0),mklocalnode(id,0));
    } else
      (yyval.n) = 0;
    free_node((yyvsp[(1) - (1)].n));
  }
    break;

  case 251:
#line 1965 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 252:
#line 1967 "language.yacc"
    {
    int id;
    add_ref((yyvsp[(0) - (3)].n)->u.sval.u.type);
    id = add_local_name((yyvsp[(1) - (3)].n)->u.sval.u.string, (yyvsp[(0) - (3)].n)->u.sval.u.type, 0);
    if (id >= 0) {
      if (!(THIS_COMPILATION->lex.pragmas & ID_STRICT_TYPES)) {
	/* Only warn about unused initialized variables in strict types mode. */
	Pike_compiler->compiler_frame->variable[id].flags |= LOCAL_VAR_IS_USED;
      }
      (yyval.n)=mknode(F_ASSIGN,(yyvsp[(3) - (3)].n), mklocalnode(id,0));
    } else
      (yyval.n) = 0;
    free_node((yyvsp[(1) - (3)].n));
  }
    break;

  case 253:
#line 1981 "language.yacc"
    { (yyval.n)=(yyvsp[(3) - (3)].n); }
    break;

  case 254:
#line 1985 "language.yacc"
    {
    /* Used to hold line-number info */
    (yyval.n) = mkintnode(0);
  }
    break;

  case 255:
#line 1992 "language.yacc"
    {
    (yyvsp[(1) - (1)].number)=Pike_compiler->num_used_modules;
    (yyval.number)=Pike_compiler->compiler_frame->current_number_of_locals;
  }
    break;

  case 256:
#line 1997 "language.yacc"
    {
    /* Trick to store more than one number on compiler stack - Hubbe */
    (yyval.number)=Pike_compiler->compiler_frame->last_block_level;

    if((yyval.number) == -1) /* if 'first block' */
      Pike_compiler->compiler_frame->last_block_level=0; /* all variables */
    else
      Pike_compiler->compiler_frame->last_block_level=(yyvsp[(2) - (3)].number);
  }
    break;

  case 257:
#line 2007 "language.yacc"
    {
    unuse_modules(Pike_compiler->num_used_modules - (yyvsp[(1) - (6)].number));
    pop_local_variables((yyvsp[(2) - (6)].number));
    Pike_compiler->compiler_frame->last_block_level=(yyvsp[(4) - (6)].number);
    if ((yyvsp[(5) - (6)].n)) COPY_LINE_NUMBER_INFO((yyvsp[(5) - (6)].n), (yyvsp[(3) - (6)].n));
    free_node ((yyvsp[(3) - (6)].n));
    (yyval.n)=(yyvsp[(5) - (6)].n);
  }
    break;

  case 259:
#line 2019 "language.yacc"
    {
    yyerror("Missing '}'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 261:
#line 2026 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 262:
#line 2027 "language.yacc"
    { yyerror("Unexpected end of file."); (yyval.n)=0; }
    break;

  case 263:
#line 2032 "language.yacc"
    {
    (yyval.n) = (yyvsp[(-2) - (0)].n);
  }
    break;

  case 265:
#line 2040 "language.yacc"
    { (yyval.n) = mknode(F_COMMA_EXPR, mkcastnode(void_type_string, (yyvsp[(1) - (4)].n)), (yyvsp[(4) - (4)].n)); }
    break;

  case 267:
#line 2046 "language.yacc"
    { (yyval.n) = mknode(F_COMMA_EXPR, mkcastnode(void_type_string, (yyvsp[(1) - (4)].n)), (yyvsp[(4) - (4)].n)); }
    break;

  case 268:
#line 2051 "language.yacc"
    {
    struct pike_type *type;

    /* Ugly hack to make sure that $3 is optimized */
    {
      int tmp=Pike_compiler->compiler_pass;
      (yyvsp[(3) - (3)].n)=mknode(F_COMMA_EXPR,(yyvsp[(3) - (3)].n),0);
      optimize_node((yyvsp[(3) - (3)].n));
      Pike_compiler->compiler_pass=tmp;
      type=(yyvsp[(3) - (3)].n)->u.node.a->type;
    }

    if(!is_const((yyvsp[(3) - (3)].n)))
    {
      if(Pike_compiler->compiler_pass==2)
	yyerror("Constant definition is not constant.");
    }else{
      ptrdiff_t tmp=eval_low((yyvsp[(3) - (3)].n),1);
      if(tmp < 1)
      {
	yyerror("Error in constant definition.");
      }else{
	pop_n_elems(DO_NOT_WARN((INT32)(tmp - 1)));
	if((yyvsp[(3) - (3)].n)) free_node((yyvsp[(3) - (3)].n));
	(yyvsp[(3) - (3)].n)=mksvaluenode(Pike_sp-1);
	type=(yyvsp[(3) - (3)].n)->type;
	pop_stack();
      }
    }
    if(!type) type = mixed_type_string;
    add_ref(type);
    low_add_local_name(Pike_compiler->compiler_frame, /*->previous,*/
		       (yyvsp[(1) - (3)].n)->u.sval.u.string,
		       type, (yyvsp[(3) - (3)].n));
    /* Note: Intentionally not marked as used. */
    free_node((yyvsp[(1) - (3)].n));
  }
    break;

  case 269:
#line 2088 "language.yacc"
    { if ((yyvsp[(3) - (3)].n)) free_node((yyvsp[(3) - (3)].n)); }
    break;

  case 270:
#line 2089 "language.yacc"
    { if ((yyvsp[(3) - (3)].n)) free_node((yyvsp[(3) - (3)].n)); }
    break;

  case 274:
#line 2097 "language.yacc"
    { yyerrok; }
    break;

  case 275:
#line 2099 "language.yacc"
    {
    yyerror("Missing ';'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 276:
#line 2103 "language.yacc"
    { yyerror("Missing ';'."); }
    break;

  case 277:
#line 2107 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 278:
#line 2109 "language.yacc"
    {
    (yyval.n) = mknode(F_COMMA_EXPR, (yyvsp[(1) - (2)].n), mkcastnode(void_type_string, (yyvsp[(2) - (2)].n)));
  }
    break;

  case 281:
#line 2117 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 284:
#line 2120 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 288:
#line 2124 "language.yacc"
    { reset_type_stack(); (yyval.n)=0; yyerrok; }
    break;

  case 289:
#line 2126 "language.yacc"
    {
    reset_type_stack();
    yyerror("Missing ';'.");
    yyerror("Unexpected end of file.");
    (yyval.n)=0;
  }
    break;

  case 290:
#line 2133 "language.yacc"
    {
    reset_type_stack();
    yyerror("Missing ';'.");
/*    yychar = '}'; */	/* Put the '}' back on the input stream. */
    (yyval.n)=0;
  }
    break;

  case 291:
#line 2139 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 292:
#line 2143 "language.yacc"
    {
    Pike_compiler->compiler_frame->opt_flags &= ~OPT_CUSTOM_LABELS;
  }
    break;

  case 301:
#line 2157 "language.yacc"
    {
    Pike_compiler->compiler_frame->opt_flags &= ~OPT_CUSTOM_LABELS;
  }
    break;

  case 302:
#line 2161 "language.yacc"
    {
    (yyval.n) = mknode(Pike_compiler->compiler_frame->opt_flags & OPT_CUSTOM_LABELS ?
		F_CUSTOM_STMT_LABEL : F_NORMAL_STMT_LABEL,
		(yyvsp[(1) - (4)].n), (yyvsp[(4) - (4)].n));

    /* FIXME: This won't be correct if the node happens to be shared.
     * That's an issue to be solved with shared nodes in general,
     * though. */
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(1) - (4)].n));
  }
    break;

  case 304:
#line 2174 "language.yacc"
    {(yyval.n) = 0;}
    break;

  case 305:
#line 2177 "language.yacc"
    { (yyval.n)=mknode(F_BREAK,(yyvsp[(2) - (2)].n),0); }
    break;

  case 306:
#line 2178 "language.yacc"
    { (yyval.n)=mknode(F_DEFAULT,0,0); }
    break;

  case 307:
#line 2180 "language.yacc"
    {
    (yyval.n)=mknode(F_DEFAULT,0,0); yyerror("Expected ':' after default.");
  }
    break;

  case 308:
#line 2185 "language.yacc"
    { (yyval.n)=mknode(F_CONTINUE,(yyvsp[(2) - (2)].n),0); }
    break;

  case 309:
#line 2188 "language.yacc"
    {
    push_compiler_frame(SCOPE_LOCAL);

    (yyval.ptr) = Pike_compiler->compiler_frame;
  }
    break;

  case 310:
#line 2196 "language.yacc"
    {
    struct pike_string *name;
    (yyval.n)=mkstrnode(name = get_new_name(NULL));
    free_string(name);
  }
    break;

  case 311:
#line 2204 "language.yacc"
    {
    debug_malloc_touch(Pike_compiler->compiler_frame->current_return_type);
    if(Pike_compiler->compiler_frame->current_return_type)
      free_type(Pike_compiler->compiler_frame->current_return_type);
    copy_pike_type(Pike_compiler->compiler_frame->current_return_type,
		   any_type_string);
  }
    break;

  case 312:
#line 2212 "language.yacc"
    {
    struct pike_string *name = (yyvsp[(3) - (6)].n)->u.sval.u.string;
    struct pike_type *type;
    int e;

    (yyval.number) = Pike_compiler->varargs;
    Pike_compiler->varargs = 0;

    if (Pike_compiler->compiler_pass == 1) {
      /* Define a tentative prototype for the lambda. */
      push_finished_type(mixed_type_string);
      e=(yyvsp[(6) - (6)].number)-1;
      if((yyval.number))
      {
	push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
	e--;
	pop_type_stack(T_ARRAY);
      }else{
	push_type(T_VOID);
      }
      Pike_compiler->varargs=0;
      push_type(T_MANY);
      for(; e>=0; e--) {
	push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
	push_type(T_FUNCTION);
      }    
      type=compiler_pop_type();
      Pike_compiler->compiler_frame->current_function_number =
	define_function(name, type,
			ID_PROTECTED | ID_PRIVATE | ID_INLINE | ID_USED,
			IDENTIFIER_PIKE_FUNCTION, NULL,
			(unsigned INT16)
			(Pike_compiler->compiler_frame->opt_flags));
      free_type(type);
    } else {
      /* In pass 2 we just reuse the type from pass 1. */
      Pike_compiler->compiler_frame->current_function_number =
	isidentifier(name);
    }
  }
    break;

  case 313:
#line 2253 "language.yacc"
    {
    struct pike_type *type;
    int f,e;
    struct pike_string *name;
    struct compilation *c = THIS_COMPILATION;
    struct pike_string *save_file = c->lex.current_file;
    int save_line = c->lex.current_line;
    c->lex.current_file = (yyvsp[(2) - (8)].n)->current_file;
    c->lex.current_line = (yyvsp[(2) - (8)].n)->line_number;

    debug_malloc_touch((yyvsp[(8) - (8)].n));
    (yyvsp[(8) - (8)].n)=mknode(F_COMMA_EXPR,(yyvsp[(8) - (8)].n),mknode(F_RETURN,mkintnode(0),0));
    if (Pike_compiler->compiler_pass == 2) {
      /* Doing this in pass 1 might induce too strict checks on types
       * in cases where we got placeholders. */
      type=find_return_type((yyvsp[(8) - (8)].n));
      if (type) {
	push_finished_type(type);
	free_type(type);
      } else {
	yywarning("Failed to determine return type for lambda.");
	push_type(T_ZERO);
      }
    } else {
      /* Tentative return type. */
      push_type(T_MIXED);
    }

    e=(yyvsp[(6) - (8)].number)-1;
    if((yyvsp[(7) - (8)].number))
    {
      push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
      e--;
      pop_type_stack(T_ARRAY);
    }else{
      push_type(T_VOID);
    }
    Pike_compiler->varargs=0;
    push_type(T_MANY);
    for(; e>=0; e--) {
      push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
      push_type(T_FUNCTION);
    }
    
    type=compiler_pop_type();

    name = (yyvsp[(3) - (8)].n)->u.sval.u.string;

#ifdef LAMBDA_DEBUG
    fprintf(stderr, "%d: LAMBDA: %s 0x%08lx 0x%08lx\n%d:   type: ",
	    Pike_compiler->compiler_pass, name->str,
	    (long)Pike_compiler->new_program->id,
	    Pike_compiler->local_class_counter-1,
	    Pike_compiler->compiler_pass);
    simple_describe_type(type);
    fprintf(stderr, "\n");
#endif /* LAMBDA_DEBUG */

    f=dooptcode(name,
		(yyvsp[(8) - (8)].n),
		type,
		ID_PROTECTED | ID_PRIVATE | ID_INLINE | ID_USED);

#ifdef PIKE_DEBUG
    if (f != Pike_compiler->compiler_frame->current_function_number) {
      Pike_fatal("Lost track of lambda %s.\n", name->str);
    }
#endif /* PIKE_DEBUG */

#ifdef LAMBDA_DEBUG
    fprintf(stderr, "%d:   lexical_scope: 0x%08x\n",
	    Pike_compiler->compiler_pass,
	    Pike_compiler->compiler_frame->lexical_scope);
#endif /* LAMBDA_DEBUG */

    if(Pike_compiler->compiler_frame->lexical_scope & SCOPE_SCOPED) {
      (yyval.n) = mktrampolinenode(f, Pike_compiler->compiler_frame->previous);
    } else {
      (yyval.n) = mkidentifiernode(f);
    }
    free_type(type);
    c->lex.current_line = save_line;
    c->lex.current_file = save_file;
    free_node((yyvsp[(3) - (8)].n));
    free_node ((yyvsp[(2) - (8)].n));
#ifdef PIKE_DEBUG
    if (Pike_compiler->compiler_frame != (yyvsp[(4) - (8)].ptr)) {
      Pike_fatal("Lost track of compiler_frame!\n"
		 "  Got: %p (Expected: %p) Previous: %p\n",
		 Pike_compiler->compiler_frame, (yyvsp[(4) - (8)].ptr),
		 Pike_compiler->compiler_frame->previous);
    }
#endif
    pop_compiler_frame();
  }
    break;

  case 314:
#line 2349 "language.yacc"
    {
#ifdef PIKE_DEBUG
    if (Pike_compiler->compiler_frame != (yyvsp[(4) - (5)].ptr)) {
      Pike_fatal("Lost track of compiler_frame!\n"
		 "  Got: %p (Expected: %p) Previous: %p\n",
		 Pike_compiler->compiler_frame, (yyvsp[(4) - (5)].ptr),
		 Pike_compiler->compiler_frame->previous);
    }
#endif
    pop_compiler_frame();
    (yyval.n) = mkintnode(0);
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(2) - (5)].n));
    free_node((yyvsp[(3) - (5)].n));
    free_node((yyvsp[(2) - (5)].n));
  }
    break;

  case 315:
#line 2367 "language.yacc"
    {
    struct pike_string *name;
    struct pike_type *type;
    int id,e;
    node *n;
    struct identifier *i=0;

    debug_malloc_touch(Pike_compiler->compiler_frame->current_return_type);
    if(Pike_compiler->compiler_frame->current_return_type)
      free_type(Pike_compiler->compiler_frame->current_return_type);
    copy_pike_type(Pike_compiler->compiler_frame->current_return_type,
		   (yyvsp[(0) - (3)].n)->u.sval.u.type);


    /***/
    push_finished_type(Pike_compiler->compiler_frame->current_return_type);
    
    e=(yyvsp[(3) - (3)].number)-1;
    if(Pike_compiler->varargs)
    {
      push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
      e--;
      pop_type_stack(T_ARRAY);
    }else{
      push_type(T_VOID);
    }
    push_type(T_MANY);
    for(; e>=0; e--) {
      push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
    push_type(T_FUNCTION);
    }
    
    type=compiler_pop_type();
    /***/

    name = get_new_name((yyvsp[(1) - (3)].n)->u.sval.u.string);

#ifdef LAMBDA_DEBUG
    fprintf(stderr, "%d: LAMBDA: %s 0x%08lx 0x%08lx\n",
	    Pike_compiler->compiler_pass, name->str,
	    (long)Pike_compiler->new_program->id,
	    Pike_compiler->local_class_counter-1);
#endif /* LAMBDA_DEBUG */

    if(Pike_compiler->compiler_pass > 1)
    {
      id=isidentifier(name);
    }else{
      id=define_function(name,
			 type,
			 ID_PROTECTED | ID_PRIVATE | ID_INLINE | ID_USED,
			 IDENTIFIER_PIKE_FUNCTION |
			 (Pike_compiler->varargs?IDENTIFIER_VARARGS:0),
			 0,
			 OPT_SIDE_EFFECT|OPT_EXTERNAL_DEPEND);
    }
    Pike_compiler->varargs=0;
    Pike_compiler->compiler_frame->current_function_number=id;

    n=0;
    if(Pike_compiler->compiler_pass > 1 &&
       (i=ID_FROM_INT(Pike_compiler->new_program, id)))
    {
      if(i->identifier_flags & IDENTIFIER_SCOPED)
	n = mktrampolinenode(id, Pike_compiler->compiler_frame->previous);
      else
	n = mkidentifiernode(id);
    }

    low_add_local_name(Pike_compiler->compiler_frame->previous,
		       (yyvsp[(1) - (3)].n)->u.sval.u.string, type, n);

    (yyval.number)=id;
    free_string(name);
  }
    break;

  case 316:
#line 2443 "language.yacc"
    {
    int localid;
    struct identifier *i=ID_FROM_INT(Pike_compiler->new_program, (yyvsp[(4) - (5)].number));
    struct compilation *c = THIS_COMPILATION;
    struct pike_string *save_file = c->lex.current_file;
    int save_line = c->lex.current_line;
    c->lex.current_file = (yyvsp[(1) - (5)].n)->current_file;
    c->lex.current_line = (yyvsp[(1) - (5)].n)->line_number;

    (yyvsp[(5) - (5)].n)=mknode(F_COMMA_EXPR,(yyvsp[(5) - (5)].n),mknode(F_RETURN,mkintnode(0),0));

    debug_malloc_touch((yyvsp[(5) - (5)].n));
    dooptcode(i->name,
	      (yyvsp[(5) - (5)].n),
	      i->type,
	      ID_PROTECTED | ID_PRIVATE | ID_INLINE);

    i->opt_flags = Pike_compiler->compiler_frame->opt_flags;

    c->lex.current_line = save_line;
    c->lex.current_file = save_file;
#ifdef PIKE_DEBUG
    if (Pike_compiler->compiler_frame != (yyvsp[(2) - (5)].ptr)) {
      Pike_fatal("Lost track of compiler_frame!\n"
		 "  Got: %p (Expected: %p) Previous: %p\n",
		 Pike_compiler->compiler_frame, (yyvsp[(2) - (5)].ptr),
		 Pike_compiler->compiler_frame->previous);
    }
#endif
    pop_compiler_frame();
    free_node((yyvsp[(1) - (5)].n));

    /* WARNING: If the local function adds more variables we are screwed */
    /* WARNING2: if add_local_name stops adding local variables at the end,
     *           this has to be fixed.
     */

    localid=Pike_compiler->compiler_frame->current_number_of_locals-1;
    if(Pike_compiler->compiler_frame->variable[localid].def)
    {
      (yyval.n)=copy_node(Pike_compiler->compiler_frame->variable[localid].def);
    }else{
      if(Pike_compiler->compiler_frame->lexical_scope & 
	 (SCOPE_SCOPE_USED | SCOPE_SCOPED))
      {
	(yyval.n) = mktrampolinenode((yyvsp[(4) - (5)].number),Pike_compiler->compiler_frame);
      }else{
	(yyval.n) = mkidentifiernode((yyvsp[(4) - (5)].number));
      }
    }
  }
    break;

  case 317:
#line 2495 "language.yacc"
    {
#ifdef PIKE_DEBUG
    if (Pike_compiler->compiler_frame != (yyvsp[(2) - (3)].ptr)) {
      Pike_fatal("Lost track of compiler_frame!\n"
		 "  Got: %p (Expected: %p) Previous: %p\n",
		 Pike_compiler->compiler_frame, (yyvsp[(2) - (3)].ptr),
		 Pike_compiler->compiler_frame->previous);
    }
#endif
    pop_compiler_frame();
    (yyval.n)=mkintnode(0);
  }
    break;

  case 318:
#line 2510 "language.yacc"
    {
    struct pike_string *name;
    struct pike_type *type;
    int id,e;
    node *n;
    struct identifier *i=0;

    /***/
    debug_malloc_touch(Pike_compiler->compiler_frame->current_return_type);
    
    push_finished_type((yyvsp[(0) - (4)].n)->u.sval.u.type);
    if ((yyvsp[(1) - (4)].number) && (Pike_compiler->compiler_pass == 2) && !TEST_COMPAT (0, 6)) {
      yywarning("The *-syntax in types is obsolete. Use array instead.");
    }
    while((yyvsp[(1) - (4)].number)--) push_type(T_ARRAY);

    if(Pike_compiler->compiler_frame->current_return_type)
      free_type(Pike_compiler->compiler_frame->current_return_type);
    Pike_compiler->compiler_frame->current_return_type=compiler_pop_type();

    /***/
    push_finished_type(Pike_compiler->compiler_frame->current_return_type);
    
    e=(yyvsp[(4) - (4)].number)-1;
    if(Pike_compiler->varargs)
    {
      push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
      e--;
      pop_type_stack(T_ARRAY);
    }else{
      push_type(T_VOID);
    }
    push_type(T_MANY);
    for(; e>=0; e--) {
      push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
      push_type(T_FUNCTION);
    }
    
    type=compiler_pop_type();
    /***/

    name = get_new_name((yyvsp[(2) - (4)].n)->u.sval.u.string);

#ifdef LAMBDA_DEBUG
    fprintf(stderr, "%d: LAMBDA: %s 0x%08lx 0x%08lx\n",
	    Pike_compiler->compiler_pass, name->str,
	    (long)Pike_compiler->new_program->id,
	    Pike_compiler->local_class_counter-1);
#endif /* LAMBDA_DEBUG */

    if(Pike_compiler->compiler_pass > 1)
    {
      id=isidentifier(name);
    }else{
      id=define_function(name,
			 type,
			 ID_PROTECTED | ID_PRIVATE | ID_INLINE | ID_USED,
			 IDENTIFIER_PIKE_FUNCTION|
			 (Pike_compiler->varargs?IDENTIFIER_VARARGS:0),
			 0,
			 OPT_SIDE_EFFECT|OPT_EXTERNAL_DEPEND);
    }
    Pike_compiler->varargs=0;
    Pike_compiler->compiler_frame->current_function_number=id;

    n=0;
    if(Pike_compiler->compiler_pass > 1 &&
       (i=ID_FROM_INT(Pike_compiler->new_program, id)))
    {
      if(i->identifier_flags & IDENTIFIER_SCOPED)
	n = mktrampolinenode(id, Pike_compiler->compiler_frame->previous);
      else
	n = mkidentifiernode(id);
    }

    low_add_local_name(Pike_compiler->compiler_frame->previous,
		       (yyvsp[(2) - (4)].n)->u.sval.u.string, type, n);
    (yyval.number)=id;
    free_string(name);
  }
    break;

  case 319:
#line 2591 "language.yacc"
    {
    int localid;
    struct identifier *i=ID_FROM_INT(Pike_compiler->new_program, (yyvsp[(5) - (6)].number));
    struct compilation *c = THIS_COMPILATION;
    struct pike_string *save_file = c->lex.current_file;
    int save_line = c->lex.current_line;
    c->lex.current_file = (yyvsp[(2) - (6)].n)->current_file;
    c->lex.current_line = (yyvsp[(2) - (6)].n)->line_number;

    debug_malloc_touch((yyvsp[(6) - (6)].n));
    (yyvsp[(6) - (6)].n)=mknode(F_COMMA_EXPR,(yyvsp[(6) - (6)].n),mknode(F_RETURN,mkintnode(0),0));


    debug_malloc_touch((yyvsp[(6) - (6)].n));
    dooptcode(i->name,
	      (yyvsp[(6) - (6)].n),
	      i->type,
	      ID_PROTECTED | ID_PRIVATE | ID_INLINE);

    i->opt_flags = Pike_compiler->compiler_frame->opt_flags;

    c->lex.current_line = save_line;
    c->lex.current_file = save_file;
#ifdef PIKE_DEBUG
    if (Pike_compiler->compiler_frame != (yyvsp[(3) - (6)].ptr)) {
      Pike_fatal("Lost track of compiler_frame!\n"
		 "  Got: %p (Expected: %p) Previous: %p\n",
		 Pike_compiler->compiler_frame, (yyvsp[(3) - (6)].ptr),
		 Pike_compiler->compiler_frame->previous);
    }
#endif
    pop_compiler_frame();
    free_node((yyvsp[(2) - (6)].n));

    /* WARNING: If the local function adds more variables we are screwed */
    /* WARNING2: if add_local_name stops adding local variables at the end,
     *           this has to be fixed.
     */

    localid=Pike_compiler->compiler_frame->current_number_of_locals-1;
    if(Pike_compiler->compiler_frame->variable[localid].def)
    {
      (yyval.n)=copy_node(Pike_compiler->compiler_frame->variable[localid].def);
    }else{
      if(Pike_compiler->compiler_frame->lexical_scope & 
	 (SCOPE_SCOPE_USED | SCOPE_SCOPED))
      {
        (yyval.n) = mktrampolinenode((yyvsp[(5) - (6)].number),Pike_compiler->compiler_frame);
      }else{
        (yyval.n) = mkidentifiernode((yyvsp[(5) - (6)].number));
      }
    }
  }
    break;

  case 320:
#line 2645 "language.yacc"
    {
#ifdef PIKE_DEBUG
    if (Pike_compiler->compiler_frame != (yyvsp[(3) - (4)].ptr)) {
      Pike_fatal("Lost track of compiler_frame!\n"
		 "  Got: %p (Expected: %p) Previous: %p\n",
		 Pike_compiler->compiler_frame, (yyvsp[(3) - (4)].ptr),
		 Pike_compiler->compiler_frame->previous);
    }
#endif
    pop_compiler_frame();
    free_node((yyvsp[(2) - (4)].n));
    (yyval.n)=mkintnode(0);
  }
    break;

  case 321:
#line 2661 "language.yacc"
    {
    struct pike_type *type;
    int ref_no;

    if (Pike_compiler->num_create_args < 0) {
      yyerror("Can't define more variables after ...");
    }

    push_finished_type(Pike_compiler->compiler_frame->current_type);
    if ((yyvsp[(3) - (5)].number) && (Pike_compiler->compiler_pass == 2) && !TEST_COMPAT (0, 6)) {
      yywarning("The *-syntax in types is obsolete. Use array instead.");
    }
    while((yyvsp[(3) - (5)].number)--) push_type(T_ARRAY);
    if ((yyvsp[(4) - (5)].number)) {
      push_type(T_ARRAY);
    }
    type=compiler_pop_type();

    /* Add the identifier globally.
     * Note: Since these are the first identifiers (and references)
     *       to be added to the program, they will be numbered in
     *       sequence starting at 0 (zero). This means that the
     *       counter num_create_args is sufficient extra information
     *       to be able to keep track of them.
     */
    ref_no = define_variable((yyvsp[(5) - (5)].n)->u.sval.u.string, type,
			     Pike_compiler->current_modifiers);
    free_type(type);

    if (Pike_compiler->num_create_args != ref_no) {
      my_yyerror("Multiple definitions of create variable %S (%d != %d).",
		 (yyvsp[(5) - (5)].n)->u.sval.u.string,
		 Pike_compiler->num_create_args, ref_no);
    }
    if ((yyvsp[(4) - (5)].number)) {
      /* Encode varargs marker as negative number of args. */
      Pike_compiler->num_create_args = -(ref_no + 1);
    } else {
      Pike_compiler->num_create_args = ref_no + 1;
    }

    /* free_type(type); */
    free_node((yyvsp[(5) - (5)].n));
    (yyval.number)=0;
  }
    break;

  case 322:
#line 2706 "language.yacc"
    { (yyval.number)=0; }
    break;

  case 323:
#line 2709 "language.yacc"
    { (yyval.number) = 1; }
    break;

  case 324:
#line 2710 "language.yacc"
    { (yyval.number) = (yyvsp[(1) - (3)].number) + 1; }
    break;

  case 325:
#line 2712 "language.yacc"
    {
    yyerror("Unexpected ':' in create argument list.");
    (yyval.number) = (yyvsp[(1) - (3)].number) + 1;
  }
    break;

  case 326:
#line 2718 "language.yacc"
    { (yyval.number)=0; }
    break;

  case 328:
#line 2722 "language.yacc"
    { (yyval.number) = 0; }
    break;

  case 329:
#line 2724 "language.yacc"
    {
    /* NOTE: One more than the number of arguments, so that we
<     *       can detect the case of no parenthesis below. */
    (yyval.number) = (yyvsp[(2) - (3)].number) + 1;
    free_node((yyvsp[(3) - (3)].n));
  }
    break;

  case 331:
#line 2733 "language.yacc"
    { yyerrok; }
    break;

  case 332:
#line 2735 "language.yacc"
    {
		  yyerror("End of file where program definition expected.");
		}
    break;

  case 333:
#line 2742 "language.yacc"
    {
    if(!(yyvsp[(3) - (3)].n))
    {
      struct pike_string *s;
      char buffer[42];
      sprintf(buffer,"__class_%ld_%ld_line_%d",
	      (long)Pike_compiler->new_program->id,
	      (long)Pike_compiler->local_class_counter++,
	      (int) (yyvsp[(2) - (3)].n)->line_number);
      s=make_shared_string(buffer);
      (yyvsp[(3) - (3)].n)=mkstrnode(s);
      free_string(s);
      (yyvsp[(0) - (3)].number)|=ID_PROTECTED | ID_PRIVATE | ID_INLINE;
    }
    /* fprintf(stderr, "LANGUAGE.YACC: CLASS start\n"); */
    if(Pike_compiler->compiler_pass==1)
    {
      if ((yyvsp[(0) - (3)].number) & ID_EXTERN) {
	yywarning("Extern declared class definition.");
      }
      low_start_new_program(0, 1, (yyvsp[(3) - (3)].n)->u.sval.u.string,
			    (yyvsp[(0) - (3)].number),
			    &(yyval.number));

      /* fprintf(stderr, "Pass 1: Program %s has id %d\n",
	 $4->u.sval.u.string->str, Pike_compiler->new_program->id); */

      store_linenumber((yyvsp[(2) - (3)].n)->line_number, (yyvsp[(2) - (3)].n)->current_file);
      debug_malloc_name(Pike_compiler->new_program,
			(yyvsp[(2) - (3)].n)->current_file->str,
			(yyvsp[(2) - (3)].n)->line_number);
    }else{
      int i;
      struct identifier *id;
      int tmp=Pike_compiler->compiler_pass;
      i=isidentifier((yyvsp[(3) - (3)].n)->u.sval.u.string);
      if(i<0)
      {
	/* Seriously broken... */
	yyerror("Pass 2: program not defined!");
	low_start_new_program(0, 2, 0,
			      (yyvsp[(0) - (3)].number),
			      &(yyval.number));
      }else{
	id=ID_FROM_INT(Pike_compiler->new_program, i);
	if(IDENTIFIER_IS_CONSTANT(id->identifier_flags))
	{
	  struct svalue *s;
	  if ((id->func.offset >= 0) &&
	      ((s = &PROG_FROM_INT(Pike_compiler->new_program,i)->
		constants[id->func.offset].sval)->type == T_PROGRAM))
	  {
	    low_start_new_program(s->u.program, 2,
				  (yyvsp[(3) - (3)].n)->u.sval.u.string,
				  (yyvsp[(0) - (3)].number),
				  &(yyval.number));

	    /* fprintf(stderr, "Pass 2: Program %s has id %d\n",
	       $4->u.sval.u.string->str, Pike_compiler->new_program->id); */

	  }else{
	    yyerror("Pass 2: constant redefined!");
	    low_start_new_program(0, 2, 0,
				  (yyvsp[(0) - (3)].number),
				  &(yyval.number));
	  }
	}else{
	  yyerror("Pass 2: class constant no longer constant!");
	  low_start_new_program(0, 2, 0,
				(yyvsp[(0) - (3)].number),
				&(yyval.number));
	}
      }
      Pike_compiler->compiler_pass=tmp;
    }
  }
    break;

  case 334:
#line 2818 "language.yacc"
    {
    /* Clear scoped modifiers. */
    (yyval.number) = THIS_COMPILATION->lex.pragmas;
    THIS_COMPILATION->lex.pragmas &= ~ID_MODIFIER_MASK;
  }
    break;

  case 335:
#line 2824 "language.yacc"
    {
    struct program *p;

    /* Check if we have create arguments but no locally defined create(). */
    if ((yyvsp[(6) - (7)].number)) {
      struct pike_string *create_string = NULL;
      struct reference *ref = NULL;
      struct identifier *id = NULL;
      int ref_id;
      MAKE_CONST_STRING(create_string, "create");
      if (((ref_id = isidentifier(create_string)) < 0) ||
	  (ref = PTR_FROM_INT(Pike_compiler->new_program, ref_id))->inherit_offset ||
	  ((id = ID_FROM_PTR(Pike_compiler->new_program, ref))->func.offset == -1)) {
	int e;
	struct pike_type *type = NULL;
	int nargs = Pike_compiler->num_create_args;

	push_compiler_frame(SCOPE_LOCAL);
	
	/* Init: Prepend the create arguments. */
	if (Pike_compiler->num_create_args < 0) {
	  for (e = 0; e < -Pike_compiler->num_create_args; e++) {
	    id = Pike_compiler->new_program->identifiers + e;
	    add_ref(id->type);
	    add_local_name(id->name, id->type, 0);
	    /* Note: add_local_name() above will return e. */
	    Pike_compiler->compiler_frame->variable[e].flags |=
	      LOCAL_VAR_IS_USED;
	  }
	} else {
	  for (e = 0; e < Pike_compiler->num_create_args; e++) {
	    id = Pike_compiler->new_program->identifiers + e;
	    add_ref(id->type);
	    add_local_name(id->name, id->type, 0);
	    /* Note: add_local_name() above will return e. */
	    Pike_compiler->compiler_frame->variable[e].flags |=
	      LOCAL_VAR_IS_USED;
	  }
	}

	/* First: Deduce the type for the create() function. */
	push_type(T_VOID); /* Return type. */

	if ((e = nargs) < 0) {
	  /* Varargs */
	  e = nargs = -nargs;
	  push_finished_type(Pike_compiler->compiler_frame->variable[--e].type);
	  pop_type_stack(T_ARRAY); /* Pop one level of array. */
	} else {
	  /* Not varargs. */
	  push_type(T_VOID);
	}
	push_type(T_MANY);
	while(e--) {
	  push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
	  push_type(T_FUNCTION);
	}

	type = compiler_pop_type();

	/* Second: Declare the function. */

	Pike_compiler->compiler_frame->current_function_number=
	  define_function(create_string, type,
			  ID_INLINE | ID_PROTECTED,
			  IDENTIFIER_PIKE_FUNCTION |
			  (Pike_compiler->num_create_args < 0?IDENTIFIER_VARARGS:0),
			  0,
			  OPT_SIDE_EFFECT);
	
	if (Pike_compiler->compiler_pass == 2) {
	  node *create_code = NULL;
	  int f;

	  /* Third: Generate the initialization code.
	   *
	   * global_arg = [type]local_arg;
	   * [,..]
	   */

	  for(e=0; e<nargs; e++)
	  {
	    if(!Pike_compiler->compiler_frame->variable[e].name ||
	       !Pike_compiler->compiler_frame->variable[e].name->len)
	    {
	      my_yyerror("Missing name for argument %d.",e);
	    } else {
	      node *local_node = mklocalnode(e, 0);

	      /* FIXME: Should probably use some other flag. */
	      if ((runtime_options & RUNTIME_CHECK_TYPES) &&
		  (Pike_compiler->compiler_pass == 2) &&
		  (Pike_compiler->compiler_frame->variable[e].type !=
		   mixed_type_string)) {
		/* fprintf(stderr, "Creating soft cast node for local #%d\n", e);*/

		/* The following is needed to go around the optimization in
		 * mksoftcastnode().
		 */
		free_type(local_node->type);
		copy_pike_type(local_node->type, mixed_type_string);
	  
		local_node = mksoftcastnode(Pike_compiler->compiler_frame->
					    variable[e].type, local_node);
	      }
	      create_code =
		mknode(F_COMMA_EXPR, create_code,
		       mknode(F_ASSIGN, local_node,
			      mkidentifiernode(e)));
	    }
	  }

	  /* Fourth: Add a return 0; at the end. */

	  create_code = mknode(F_COMMA_EXPR,
			       mknode(F_POP_VALUE, create_code, NULL),
			       mknode(F_RETURN, mkintnode(0), NULL));

	  /* Fifth: Define the function. */

	  f=dooptcode(create_string, create_code, type, ID_PROTECTED);

#ifdef PIKE_DEBUG
	  if(Pike_interpreter.recoveries &&
	     Pike_sp-Pike_interpreter.evaluator_stack < Pike_interpreter.recoveries->stack_pointer)
	    Pike_fatal("Stack error (underflow)\n");

	  if(Pike_compiler->compiler_pass == 1 &&
	     f!=Pike_compiler->compiler_frame->current_function_number)
	    Pike_fatal("define_function screwed up! %d != %d\n",
		       f, Pike_compiler->compiler_frame->current_function_number);
#endif
	}

	/* Done. */

	free_type(type);
	pop_compiler_frame();
      }
    }

    if(Pike_compiler->compiler_pass == 1)
      p=end_first_pass(0);
    else
      p=end_first_pass(1);

    /* fprintf(stderr, "LANGUAGE.YACC: CLASS end\n"); */

    if(p) {
      /* Update the type for the program constant,
       * since we might have a lfun::create(). */
      struct identifier *i;
      struct svalue sv;
      sv.type = T_PROGRAM;
      sv.subtype = 0;
      sv.u.program = p;
      i = ID_FROM_INT(Pike_compiler->new_program, (yyvsp[(4) - (7)].number));
      free_type(i->type);
      i->type = get_type_of_svalue(&sv);
      free_program(p);
    } else if (!Pike_compiler->num_parse_error) {
      /* Make sure code in this class is aware that something went wrong. */
      Pike_compiler->num_parse_error = 1;
    }

    (yyval.n)=mkidentifiernode((yyvsp[(4) - (7)].number));

    free_node((yyvsp[(2) - (7)].n));
    free_node((yyvsp[(3) - (7)].n));
    check_tree((yyval.n),0);
    THIS_COMPILATION->lex.pragmas = (yyvsp[(5) - (7)].number);
  }
    break;

  case 337:
#line 2999 "language.yacc"
    { (yyval.n) = 0; }
    break;

  case 338:
#line 3002 "language.yacc"
    { (yyval.n) = 0; }
    break;

  case 339:
#line 3003 "language.yacc"
    { (yyval.n) = (yyvsp[(2) - (2)].n); }
    break;

  case 341:
#line 3009 "language.yacc"
    {
    if ((yyvsp[(1) - (2)].n)) {
      if ((yyvsp[(2) - (2)].n)) {
	/* Explicit enum value. */

	/* This can be made more lenient in the future */

	/* Ugly hack to make sure that $2 is optimized */
	{
	  int tmp=Pike_compiler->compiler_pass;
	  (yyvsp[(2) - (2)].n)=mknode(F_COMMA_EXPR,(yyvsp[(2) - (2)].n),0);
	  Pike_compiler->compiler_pass=tmp;
	}

	if(!is_const((yyvsp[(2) - (2)].n)))
	{
	  if(Pike_compiler->compiler_pass==2)
	    yyerror("Enum definition is not constant.");
	  push_int(0);
	} else {
	  if(!Pike_compiler->num_parse_error)
	  {
	    ptrdiff_t tmp=eval_low((yyvsp[(2) - (2)].n),1);
	    if(tmp < 1)
	    {
	      yyerror("Error in enum definition.");
	      push_int(0);
	    }else{
	      pop_n_elems(DO_NOT_WARN((INT32)(tmp - 1)));
	    }
	  } else {
	    push_int(0);
	  }
	}
	free_node((yyvsp[(2) - (2)].n));
	free_node((yyvsp[(0) - (2)].n));
	(yyvsp[(0) - (2)].n) = mkconstantsvaluenode(Pike_sp-1);
      } else {
	/* Implicit enum value. */
	(yyvsp[(0) - (2)].n) = safe_inc_enum((yyvsp[(0) - (2)].n));
	push_svalue(&(yyvsp[(0) - (2)].n)->u.sval);
      }
      add_constant((yyvsp[(1) - (2)].n)->u.sval.u.string, Pike_sp-1,
		   (Pike_compiler->current_modifiers & ~ID_EXTERN) | ID_INLINE);
      /* Update the type. */
      {
	struct pike_type *current = pop_unfinished_type();
	struct pike_type *new = get_type_of_svalue(Pike_sp-1);
	struct pike_type *res = or_pike_types(new, current, 1);
	free_type(current);
	free_type(new);
	type_stack_mark();
	push_finished_type(res);
	free_type(res);
      }
      pop_stack();
      free_node((yyvsp[(1) - (2)].n));
    } else if ((yyvsp[(2) - (2)].n)) {
      free_node((yyvsp[(2) - (2)].n));
    }
  }
    break;

  case 342:
#line 3074 "language.yacc"
    {
    (yyval.n) = (yyvsp[(-2) - (0)].n);
  }
    break;

  case 344:
#line 3081 "language.yacc"
    { (yyvsp[(0) - (4)].n) = (yyvsp[(3) - (4)].n); }
    break;

  case 346:
#line 3087 "language.yacc"
    {
    if ((Pike_compiler->current_modifiers & ID_EXTERN) &&
	(Pike_compiler->compiler_pass == 1)) {
      yywarning("Extern declared enum.");
    }

    type_stack_mark();
    push_type(T_ZERO);	/* Joined type so far. */
  }
    break;

  case 347:
#line 3097 "language.yacc"
    {
    push_int(-1);	/* Previous value. */
    (yyval.n) = mkconstantsvaluenode(Pike_sp-1);
    pop_stack();
  }
    break;

  case 348:
#line 3103 "language.yacc"
    {
    struct pike_type *t = pop_unfinished_type();
    free_node((yyvsp[(5) - (7)].n));
    if ((yyvsp[(3) - (7)].n)) {
      ref_push_type_value(t);
      add_constant((yyvsp[(3) - (7)].n)->u.sval.u.string, Pike_sp-1,
		   (Pike_compiler->current_modifiers & ~ID_EXTERN) | ID_INLINE);
      pop_stack();
      free_node((yyvsp[(3) - (7)].n));
    }
    (yyval.n) = mktypenode(t);
    free_type(t);
  }
    break;

  case 349:
#line 3119 "language.yacc"
    {
    struct pike_type *t = compiler_pop_type();

    if ((Pike_compiler->current_modifiers & ID_EXTERN) &&
	(Pike_compiler->compiler_pass == 1)) {
      yywarning("Extern declared typedef.");
    }

    if ((yyvsp[(4) - (5)].n)) {
      ref_push_type_value(t);
      add_constant((yyvsp[(4) - (5)].n)->u.sval.u.string, Pike_sp-1,
		   (Pike_compiler->current_modifiers & ~ID_EXTERN) | ID_INLINE);
      pop_stack();
      free_node((yyvsp[(4) - (5)].n));
    }
    free_type(t);
  }
    break;

  case 350:
#line 3139 "language.yacc"
    {
    (yyval.number)=Pike_compiler->compiler_frame->current_number_of_locals;
  }
    break;

  case 351:
#line 3143 "language.yacc"
    {
    /* Trick to store more than one number on compiler stack - Hubbe */
    (yyval.number)=Pike_compiler->compiler_frame->last_block_level;
    Pike_compiler->compiler_frame->last_block_level=(yyvsp[(2) - (3)].number);
  }
    break;

  case 352:
#line 3149 "language.yacc"
    {
    (yyval.n) = mknode('?', (yyvsp[(6) - (9)].n),
		mknode(':',
		       mkcastnode(void_type_string, (yyvsp[(8) - (9)].n)),
		       mkcastnode(void_type_string, (yyvsp[(9) - (9)].n))));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(3) - (9)].n));
    (yyval.n) = mkcastnode(void_type_string, (yyval.n));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(3) - (9)].n));
    free_node ((yyvsp[(3) - (9)].n));
    pop_local_variables((yyvsp[(2) - (9)].number));
    Pike_compiler->compiler_frame->last_block_level=(yyvsp[(4) - (9)].number);
  }
    break;

  case 354:
#line 3164 "language.yacc"
    { yyerror("Missing ')'."); }
    break;

  case 355:
#line 3166 "language.yacc"
    {
    yyerror("Missing ')'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 356:
#line 3172 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 357:
#line 3173 "language.yacc"
    { (yyval.n)=(yyvsp[(2) - (2)].n); }
    break;

  case 358:
#line 3177 "language.yacc"
    {
    if (!(THIS_COMPILATION->lex.pragmas & ID_STRICT_TYPES) && (yyvsp[(1) - (1)].n)) {
      if ((yyvsp[(1) - (1)].n)->token == F_ARRAY_LVALUE) {
	mark_lvalues_as_used(CAR((yyvsp[(1) - (1)].n)));
      } else if (((yyvsp[(1) - (1)].n)->token == F_LOCAL) && !((yyvsp[(1) - (1)].n)->u.integer.b)) {
	Pike_compiler->compiler_frame->variable[(yyvsp[(1) - (1)].n)->u.integer.a].flags |=
	  LOCAL_VAR_IS_USED;
      }
    }
  }
    break;

  case 359:
#line 3187 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 361:
#line 3191 "language.yacc"
    { yyerror("Unexpected end of file."); (yyval.n)=0; }
    break;

  case 362:
#line 3192 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 363:
#line 3196 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 365:
#line 3200 "language.yacc"
    { (yyval.n)=(yyvsp[(2) - (2)].n); }
    break;

  case 366:
#line 3202 "language.yacc"
    { (yyval.n)=mknode(':',(yyvsp[(2) - (4)].n),(yyvsp[(4) - (4)].n)); }
    break;

  case 367:
#line 3206 "language.yacc"
    {
    (yyval.number)=Pike_compiler->compiler_frame->current_number_of_locals;
  }
    break;

  case 368:
#line 3210 "language.yacc"
    {
    /* Trick to store more than one number on compiler stack - Hubbe */
    (yyval.number)=Pike_compiler->compiler_frame->last_block_level;
    Pike_compiler->compiler_frame->last_block_level=(yyvsp[(2) - (3)].number);
  }
    break;

  case 369:
#line 3216 "language.yacc"
    {
    if ((yyvsp[(7) - (9)].n)) {
      (yyval.n)=mknode(F_FOREACH,
		mknode(F_VAL_LVAL,(yyvsp[(6) - (9)].n),(yyvsp[(7) - (9)].n)),
		(yyvsp[(9) - (9)].n));
    } else {
      /* Error in lvalue */
      (yyval.n)=mknode(F_COMMA_EXPR, mkcastnode(void_type_string, (yyvsp[(6) - (9)].n)), (yyvsp[(9) - (9)].n));
    }
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(3) - (9)].n));
    free_node ((yyvsp[(3) - (9)].n));
    pop_local_variables((yyvsp[(2) - (9)].number));
    Pike_compiler->compiler_frame->last_block_level=(yyvsp[(4) - (9)].number);
    Pike_compiler->compiler_frame->opt_flags |= OPT_CUSTOM_LABELS;
  }
    break;

  case 370:
#line 3235 "language.yacc"
    {
    (yyval.n)=mknode(F_DO,(yyvsp[(3) - (8)].n),(yyvsp[(6) - (8)].n));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(2) - (8)].n));
    free_node ((yyvsp[(2) - (8)].n));
    Pike_compiler->compiler_frame->opt_flags |= OPT_CUSTOM_LABELS;
  }
    break;

  case 371:
#line 3242 "language.yacc"
    {
    free_node ((yyvsp[(2) - (5)].n));
    (yyval.n)=0;
    yyerror("Missing '(' in do-while loop.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 372:
#line 3249 "language.yacc"
    {
    free_node ((yyvsp[(2) - (4)].n));
    (yyval.n)=0;
    yyerror("Missing 'while' in do-while loop.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 374:
#line 3259 "language.yacc"
    {
    yyerror("Missing ';'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 375:
#line 3266 "language.yacc"
    {
    (yyval.number)=Pike_compiler->compiler_frame->current_number_of_locals;
  }
    break;

  case 376:
#line 3270 "language.yacc"
    {
    /* Trick to store more than one number on compiler stack - Hubbe */
    (yyval.number)=Pike_compiler->compiler_frame->last_block_level;
    Pike_compiler->compiler_frame->last_block_level=(yyvsp[(2) - (3)].number);
  }
    break;

  case 377:
#line 3277 "language.yacc"
    {
    (yyval.n)=mknode(F_COMMA_EXPR, mkcastnode(void_type_string, (yyvsp[(6) - (12)].n)),
	      mknode(F_FOR,(yyvsp[(8) - (12)].n),mknode(':',(yyvsp[(12) - (12)].n),(yyvsp[(10) - (12)].n))));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(3) - (12)].n));
    free_node ((yyvsp[(3) - (12)].n));
    pop_local_variables((yyvsp[(2) - (12)].number));
    Pike_compiler->compiler_frame->last_block_level=(yyvsp[(4) - (12)].number);
    Pike_compiler->compiler_frame->opt_flags |= OPT_CUSTOM_LABELS;
  }
    break;

  case 378:
#line 3290 "language.yacc"
    {
    (yyval.number)=Pike_compiler->compiler_frame->current_number_of_locals;
  }
    break;

  case 379:
#line 3294 "language.yacc"
    {
    /* Trick to store more than one number on compiler stack - Hubbe */
    (yyval.number)=Pike_compiler->compiler_frame->last_block_level;
    Pike_compiler->compiler_frame->last_block_level=(yyvsp[(2) - (3)].number);
  }
    break;

  case 380:
#line 3300 "language.yacc"
    {
    (yyval.n)=mknode(F_FOR,(yyvsp[(6) - (8)].n),mknode(':',(yyvsp[(8) - (8)].n),NULL));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(3) - (8)].n));
    free_node ((yyvsp[(3) - (8)].n));
    pop_local_variables((yyvsp[(2) - (8)].number));
    Pike_compiler->compiler_frame->last_block_level=(yyvsp[(4) - (8)].number);
    Pike_compiler->compiler_frame->opt_flags |= OPT_CUSTOM_LABELS;
  }
    break;

  case 381:
#line 3310 "language.yacc"
    { (yyval.n)=mkintnode(1); }
    break;

  case 383:
#line 3315 "language.yacc"
    {
    (yyval.number)=Pike_compiler->compiler_frame->current_number_of_locals;
  }
    break;

  case 384:
#line 3319 "language.yacc"
    {
    /* Trick to store more than one number on compiler stack - Hubbe */
    (yyval.number)=Pike_compiler->compiler_frame->last_block_level;
    Pike_compiler->compiler_frame->last_block_level=(yyvsp[(2) - (3)].number);
  }
    break;

  case 385:
#line 3325 "language.yacc"
    {
    (yyval.n)=mknode(F_SWITCH,(yyvsp[(6) - (8)].n),(yyvsp[(8) - (8)].n));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(3) - (8)].n));
    free_node ((yyvsp[(3) - (8)].n));
    pop_local_variables((yyvsp[(2) - (8)].number));
    Pike_compiler->compiler_frame->last_block_level=(yyvsp[(4) - (8)].number);
  }
    break;

  case 386:
#line 3335 "language.yacc"
    {
    (yyval.n)=mknode(F_CASE,(yyvsp[(2) - (3)].n),0);
  }
    break;

  case 387:
#line 3339 "language.yacc"
    {
     (yyval.n)=mknode(F_CASE_RANGE,(yyvsp[(2) - (5)].n),(yyvsp[(4) - (5)].n));
  }
    break;

  case 388:
#line 3343 "language.yacc"
    {
     (yyval.n)=mknode(F_CASE_RANGE,0,(yyvsp[(3) - (4)].n));
  }
    break;

  case 390:
#line 3350 "language.yacc"
    {
    yyerror("Missing ':'.");
  }
    break;

  case 391:
#line 3354 "language.yacc"
    {
    yyerror("Missing ':'.");
  }
    break;

  case 392:
#line 3358 "language.yacc"
    {
    yyerror("Missing ':'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 393:
#line 3365 "language.yacc"
    {
    if(!TEST_COMPAT(0,6) &&
       !match_types(Pike_compiler->compiler_frame->current_return_type,
		    void_type_string))
    {
      yytype_error("Must return a value for a non-void function.",
		   Pike_compiler->compiler_frame->current_return_type,
		   void_type_string, 0);
    }
    (yyval.n)=mknode(F_RETURN,mkintnode(0),0);
  }
    break;

  case 394:
#line 3377 "language.yacc"
    {
    (yyval.n)=mknode(F_RETURN,(yyvsp[(2) - (3)].n),0);
  }
    break;

  case 395:
#line 3382 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 396:
#line 3383 "language.yacc"
    { (yyval.n)=mkcastnode(void_type_string, (yyvsp[(1) - (1)].n));  }
    break;

  case 397:
#line 3386 "language.yacc"
    { (yyval.n)=mkcastnode(void_type_string, (yyvsp[(1) - (1)].n));  }
    break;

  case 398:
#line 3388 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 401:
#line 3393 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 403:
#line 3397 "language.yacc"
    { (yyval.n)=(yyvsp[(2) - (2)].n); free_node((yyvsp[(1) - (2)].n)); }
    break;

  case 404:
#line 3398 "language.yacc"
    { (yyval.n)=(yyvsp[(2) - (2)].n); free_node((yyvsp[(1) - (2)].n)); }
    break;

  case 405:
#line 3399 "language.yacc"
    { (yyval.n)=(yyvsp[(2) - (2)].n); free_node((yyvsp[(1) - (2)].n)); }
    break;

  case 406:
#line 3400 "language.yacc"
    { (yyval.n)=(yyvsp[(2) - (2)].n); free_node((yyvsp[(1) - (2)].n)); }
    break;

  case 408:
#line 3406 "language.yacc"
    {
    (yyval.n) = mknode(F_COMMA_EXPR, mkcastnode(void_type_string, (yyvsp[(1) - (3)].n)), (yyvsp[(3) - (3)].n)); 
  }
    break;

  case 410:
#line 3412 "language.yacc"
    { (yyval.n)=mknode(F_PUSH_ARRAY,(yyvsp[(2) - (2)].n),0); }
    break;

  case 412:
#line 3415 "language.yacc"
    { (yyval.n)=mknode(F_ASSIGN,(yyvsp[(3) - (3)].n),(yyvsp[(1) - (3)].n)); }
    break;

  case 413:
#line 3416 "language.yacc"
    { (yyval.n)=(yyvsp[(1) - (3)].n); reset_type_stack(); yyerrok; }
    break;

  case 414:
#line 3417 "language.yacc"
    { (yyval.n)=(yyvsp[(3) - (3)].n); }
    break;

  case 415:
#line 3419 "language.yacc"
    {
    if (!(THIS_COMPILATION->lex.pragmas & ID_STRICT_TYPES)) {
      mark_lvalues_as_used((yyvsp[(2) - (5)].n));
    }
    (yyval.n)=mknode(F_ASSIGN,(yyvsp[(5) - (5)].n),mknode(F_ARRAY_LVALUE,(yyvsp[(2) - (5)].n),0));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(1) - (5)].n));
    free_node ((yyvsp[(1) - (5)].n));
  }
    break;

  case 416:
#line 3427 "language.yacc"
    { (yyval.n)=mknode((yyvsp[(2) - (3)].number),(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 417:
#line 3428 "language.yacc"
    { (yyval.n)=(yyvsp[(1) - (3)].n); reset_type_stack(); yyerrok; }
    break;

  case 418:
#line 3429 "language.yacc"
    { (yyval.n)=(yyvsp[(3) - (3)].n); }
    break;

  case 419:
#line 3431 "language.yacc"
    {
    (yyval.n)=mknode((yyvsp[(4) - (5)].number),mknode(F_ARRAY_LVALUE,(yyvsp[(2) - (5)].n),0),(yyvsp[(5) - (5)].n));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(1) - (5)].n));
    free_node ((yyvsp[(1) - (5)].n));
  }
    break;

  case 420:
#line 3437 "language.yacc"
    {
      (yyval.n)=(yyvsp[(2) - (4)].n); free_node ((yyvsp[(1) - (4)].n)); reset_type_stack(); yyerrok;
    }
    break;

  case 422:
#line 3444 "language.yacc"
    { (yyval.n)=mknode('?',(yyvsp[(1) - (5)].n),mknode(':',(yyvsp[(3) - (5)].n),(yyvsp[(5) - (5)].n))); }
    break;

  case 423:
#line 3447 "language.yacc"
    { (yyval.number)=F_AND_EQ; }
    break;

  case 424:
#line 3448 "language.yacc"
    { (yyval.number)=F_OR_EQ; }
    break;

  case 425:
#line 3449 "language.yacc"
    { (yyval.number)=F_XOR_EQ; }
    break;

  case 426:
#line 3450 "language.yacc"
    { (yyval.number)=F_LSH_EQ; }
    break;

  case 427:
#line 3451 "language.yacc"
    { (yyval.number)=F_RSH_EQ; }
    break;

  case 428:
#line 3452 "language.yacc"
    { (yyval.number)=F_ADD_EQ; }
    break;

  case 429:
#line 3453 "language.yacc"
    { (yyval.number)=F_SUB_EQ; }
    break;

  case 430:
#line 3454 "language.yacc"
    { (yyval.number)=F_MULT_EQ; }
    break;

  case 431:
#line 3455 "language.yacc"
    { (yyval.number)=F_MOD_EQ; }
    break;

  case 432:
#line 3456 "language.yacc"
    { (yyval.number)=F_DIV_EQ; }
    break;

  case 433:
#line 3459 "language.yacc"
    { (yyval.number)=0; }
    break;

  case 434:
#line 3459 "language.yacc"
    { (yyval.number)=1; }
    break;

  case 435:
#line 3461 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 438:
#line 3467 "language.yacc"
    { (yyval.n)=mknode(F_ARG_LIST,(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 439:
#line 3470 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 442:
#line 3476 "language.yacc"
    {
    if ((yyvsp[(3) - (3)].n)) {
      (yyval.n)=mknode(F_ARG_LIST,(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n));
    } else {
      /* Error in assoc_pair */
      (yyval.n)=(yyvsp[(1) - (3)].n);
    }
  }
    break;

  case 444:
#line 3488 "language.yacc"
    {
    (yyval.n)=mknode(F_ARG_LIST,(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n));
  }
    break;

  case 445:
#line 3491 "language.yacc"
    { free_node((yyvsp[(1) - (3)].n)); (yyval.n)=0; }
    break;

  case 447:
#line 3495 "language.yacc"
    { (yyval.n)=mknode(F_LOR,(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 448:
#line 3496 "language.yacc"
    { (yyval.n)=mknode(F_LAND,(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 449:
#line 3497 "language.yacc"
    { (yyval.n)=mkopernode("`|",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 450:
#line 3498 "language.yacc"
    { (yyval.n)=mkopernode("`^",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 451:
#line 3499 "language.yacc"
    { (yyval.n)=mkopernode("`&",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 452:
#line 3500 "language.yacc"
    { (yyval.n)=mkopernode("`==",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 453:
#line 3501 "language.yacc"
    { (yyval.n)=mkopernode("`!=",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 454:
#line 3502 "language.yacc"
    { (yyval.n)=mkopernode("`>",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 455:
#line 3503 "language.yacc"
    { (yyval.n)=mkopernode("`>=",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 456:
#line 3504 "language.yacc"
    { (yyval.n)=mkopernode("`<",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 457:
#line 3505 "language.yacc"
    { (yyval.n)=mkopernode("`<=",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 458:
#line 3506 "language.yacc"
    { (yyval.n)=mkopernode("`<<",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 459:
#line 3507 "language.yacc"
    { (yyval.n)=mkopernode("`>>",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 460:
#line 3508 "language.yacc"
    { (yyval.n)=mkopernode("`+",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 461:
#line 3509 "language.yacc"
    { (yyval.n)=mkopernode("`-",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 462:
#line 3510 "language.yacc"
    { (yyval.n)=mkopernode("`*",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 463:
#line 3511 "language.yacc"
    { (yyval.n)=mkopernode("`%",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 464:
#line 3512 "language.yacc"
    { (yyval.n)=mkopernode("`/",(yyvsp[(1) - (3)].n),(yyvsp[(3) - (3)].n)); }
    break;

  case 484:
#line 3535 "language.yacc"
    {
    (yyval.n) = mkcastnode((yyvsp[(1) - (2)].n)->u.sval.u.type, (yyvsp[(2) - (2)].n));
    free_node((yyvsp[(1) - (2)].n));
  }
    break;

  case 485:
#line 3540 "language.yacc"
    {
    (yyval.n) = mksoftcastnode((yyvsp[(1) - (2)].n)->u.sval.u.type, (yyvsp[(2) - (2)].n));
    free_node((yyvsp[(1) - (2)].n));
  }
    break;

  case 486:
#line 3544 "language.yacc"
    { (yyval.n)=mknode(F_INC,(yyvsp[(2) - (2)].n),0); }
    break;

  case 487:
#line 3545 "language.yacc"
    { (yyval.n)=mknode(F_DEC,(yyvsp[(2) - (2)].n),0); }
    break;

  case 488:
#line 3546 "language.yacc"
    { (yyval.n)=mkopernode("`!",(yyvsp[(2) - (2)].n),0); }
    break;

  case 489:
#line 3547 "language.yacc"
    { (yyval.n)=mkopernode("`~",(yyvsp[(2) - (2)].n),0); }
    break;

  case 490:
#line 3548 "language.yacc"
    { (yyval.n)=mkopernode("`-",(yyvsp[(2) - (2)].n),0); }
    break;

  case 492:
#line 3552 "language.yacc"
    { (yyval.n)=mknode(F_POST_INC,(yyvsp[(1) - (2)].n),0); }
    break;

  case 493:
#line 3553 "language.yacc"
    { (yyval.n)=mknode(F_POST_DEC,(yyvsp[(1) - (2)].n),0); }
    break;

  case 494:
#line 3575 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 495:
#line 3579 "language.yacc"
    {
    debug_malloc_touch(Pike_compiler->compiler_frame->current_return_type);
    if(Pike_compiler->compiler_frame->current_return_type)
      free_type(Pike_compiler->compiler_frame->current_return_type);
    copy_pike_type(Pike_compiler->compiler_frame->current_return_type,
		   any_type_string);

    /* block code */
    (yyvsp[(1) - (3)].number)=Pike_compiler->num_used_modules;
    (yyval.number)=Pike_compiler->compiler_frame->current_number_of_locals;
  }
    break;

  case 496:
#line 3591 "language.yacc"
    {
    struct pike_type *type;
    int f/*, e */;
    struct pike_string *name;
    struct compilation *c = THIS_COMPILATION;
    struct pike_string *save_file = c->lex.current_file;
    int save_line = c->lex.current_line;
    c->lex.current_file = (yyvsp[(2) - (6)].n)->current_file;
    c->lex.current_line = (yyvsp[(2) - (6)].n)->line_number;

    /* block code */
    unuse_modules(Pike_compiler->num_used_modules - (yyvsp[(1) - (6)].number));
    pop_local_variables((yyvsp[(4) - (6)].number));

    debug_malloc_touch((yyvsp[(5) - (6)].n));
    (yyvsp[(5) - (6)].n)=mknode(F_COMMA_EXPR,(yyvsp[(5) - (6)].n),mknode(F_RETURN,mkintnode(0),0));
    if (Pike_compiler->compiler_pass == 2) {
      /* Doing this in pass 1 might induce too strict checks on types
       * in cases where we got placeholders. */
      type=find_return_type((yyvsp[(5) - (6)].n));
      if (type) {
	push_finished_type(type);
	free_type(type);
      } else {
	yywarning("Failed to determine return type for implicit lambda.");
	push_type(T_ZERO);
      }
    } else {
      /* Tentative return type. */
      push_type(T_MIXED);
    }
    
    push_type(T_VOID);
    push_type(T_MANY);
/*
    e=$5-1;
    for(; e>=0; e--)
      push_finished_type(Pike_compiler->compiler_frame->variable[e].type);
*/
    
    type=compiler_pop_type();

    name = get_new_name(NULL);

#ifdef LAMBDA_DEBUG
    fprintf(stderr, "%d: IMPLICIT LAMBDA: %s 0x%08lx 0x%08lx\n",
	    Pike_compiler->compiler_pass, name->str,
	    (long)Pike_compiler->new_program->id,
	    Pike_compiler->local_class_counter-1);
#endif /* LAMBDA_DEBUG */
    
    f=dooptcode(name,
		(yyvsp[(5) - (6)].n),
		type,
		ID_PROTECTED | ID_PRIVATE | ID_INLINE | ID_USED);

    if(Pike_compiler->compiler_frame->lexical_scope & SCOPE_SCOPED) {
      (yyval.n) = mktrampolinenode(f,Pike_compiler->compiler_frame->previous);
    } else {
      (yyval.n) = mkidentifiernode(f);
    }

    c->lex.current_line = save_line;
    c->lex.current_file = save_file;
    free_node ((yyvsp[(2) - (6)].n));
    free_string(name);
    free_type(type);
#ifdef PIKE_DEBUG
    if (Pike_compiler->compiler_frame != (yyvsp[(3) - (6)].ptr)) {
      Pike_fatal("Lost track of compiler_frame!\n"
		 "  Got: %p (Expected: %p) Previous: %p\n",
		 Pike_compiler->compiler_frame, (yyvsp[(3) - (6)].ptr),
		 Pike_compiler->compiler_frame->previous);
    }
#endif
    pop_compiler_frame();
  }
    break;

  case 497:
#line 3672 "language.yacc"
    {
    (yyval.n) = mkapplynode((yyvsp[(1) - (5)].n), mknode(F_ARG_LIST, (yyvsp[(3) - (5)].n), (yyvsp[(5) - (5)].n)));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(2) - (5)].n));
    free_node ((yyvsp[(2) - (5)].n));
  }
    break;

  case 498:
#line 3678 "language.yacc"
    {
    (yyval.n)=mkapplynode((yyvsp[(1) - (5)].n), (yyvsp[(5) - (5)].n));
    free_node ((yyvsp[(2) - (5)].n));
    yyerrok;
  }
    break;

  case 499:
#line 3684 "language.yacc"
    {
    yyerror("Missing ')'.");
    yyerror("Unexpected end of file.");
    (yyval.n)=mkapplynode((yyvsp[(1) - (4)].n), NULL);
    free_node ((yyvsp[(2) - (4)].n));
  }
    break;

  case 500:
#line 3691 "language.yacc"
    {
    yyerror("Missing ')'.");
    (yyval.n)=mkapplynode((yyvsp[(1) - (4)].n), NULL);
    free_node ((yyvsp[(2) - (4)].n));
  }
    break;

  case 501:
#line 3697 "language.yacc"
    {
    yyerror("Missing ')'.");
    (yyval.n)=mkapplynode((yyvsp[(1) - (4)].n), NULL);
    free_node ((yyvsp[(2) - (4)].n));
  }
    break;

  case 502:
#line 3705 "language.yacc"
    {
    if (TEST_COMPAT(7, 6)) {
      (yyval.number) = Pike_compiler->current_modifiers =
	(THIS_COMPILATION->lex.pragmas & ID_MODIFIER_MASK);
    } else {
      (yyval.number) = Pike_compiler->current_modifiers = ID_PROTECTED|ID_INLINE|ID_PRIVATE |
	(THIS_COMPILATION->lex.pragmas & ID_MODIFIER_MASK);
    }
  }
    break;

  case 505:
#line 3718 "language.yacc"
    { (yyval.n)=mkfloatnode((FLOAT_TYPE)(yyvsp[(1) - (1)].fnum)); }
    break;

  case 511:
#line 3724 "language.yacc"
    { (yyval.n) = (yyvsp[(2) - (2)].n); }
    break;

  case 512:
#line 3725 "language.yacc"
    { (yyval.n) = (yyvsp[(2) - (2)].n); }
    break;

  case 515:
#line 3729 "language.yacc"
    {
    (yyval.n)=mknode(F_AUTO_MAP_MARKER, (yyvsp[(1) - (4)].n), 0);
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(2) - (4)].n));
    free_node ((yyvsp[(2) - (4)].n));
  }
    break;

  case 516:
#line 3735 "language.yacc"
    {
    (yyval.n)=mknode(F_INDEX,(yyvsp[(1) - (4)].n),(yyvsp[(3) - (4)].n));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(2) - (4)].n));
    free_node ((yyvsp[(2) - (4)].n));
  }
    break;

  case 517:
#line 3742 "language.yacc"
    {
    (yyval.n)=mknode(F_RANGE,(yyvsp[(1) - (6)].n),mknode(':',(yyvsp[(3) - (6)].n),(yyvsp[(5) - (6)].n)));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(2) - (6)].n));
    free_node ((yyvsp[(2) - (6)].n));
  }
    break;

  case 518:
#line 3748 "language.yacc"
    {
    (yyval.n)=(yyvsp[(1) - (4)].n);
    free_node ((yyvsp[(2) - (4)].n));
    yyerrok;
  }
    break;

  case 519:
#line 3754 "language.yacc"
    {
    (yyval.n)=(yyvsp[(1) - (4)].n); yyerror("Missing ']'.");
    yyerror("Unexpected end of file.");
    free_node ((yyvsp[(2) - (4)].n));
  }
    break;

  case 520:
#line 3760 "language.yacc"
    {(yyval.n)=(yyvsp[(1) - (4)].n); yyerror("Missing ']'."); free_node ((yyvsp[(2) - (4)].n));}
    break;

  case 521:
#line 3762 "language.yacc"
    {(yyval.n)=(yyvsp[(1) - (4)].n); yyerror("Missing ']'."); free_node ((yyvsp[(2) - (4)].n));}
    break;

  case 522:
#line 3764 "language.yacc"
    {(yyval.n)=(yyvsp[(1) - (4)].n); yyerror("Missing ']'."); free_node ((yyvsp[(2) - (4)].n));}
    break;

  case 523:
#line 3766 "language.yacc"
    {
      (yyval.n)=(yyvsp[(2) - (3)].n);
      if ((yyval.n)) {
	COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(1) - (3)].n));
      }
      free_node ((yyvsp[(1) - (3)].n));
    }
    break;

  case 524:
#line 3774 "language.yacc"
    {
      /* FIXME: May eat lots of stack; cf Standards.FIPS10_4.divisions */
      (yyval.n)=mkefuncallnode("aggregate",(yyvsp[(3) - (5)].n));
      COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(1) - (5)].n));
      free_node ((yyvsp[(1) - (5)].n));
    }
    break;

  case 525:
#line 3783 "language.yacc"
    {
      /* FIXME: May eat lots of stack; cf Standards.FIPS10_4.divisions */
      (yyval.n)=mkefuncallnode("aggregate_mapping",(yyvsp[(3) - (5)].n));
      COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(1) - (5)].n));
      free_node ((yyvsp[(1) - (5)].n));
      free_node ((yyvsp[(2) - (5)].n));
    }
    break;

  case 526:
#line 3791 "language.yacc"
    {
      /* FIXME: May eat lots of stack; cf Standards.FIPS10_4.divisions */
      (yyval.n)=mkefuncallnode("aggregate_multiset",(yyvsp[(3) - (4)].n));
      COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(2) - (4)].n));
      free_node ((yyvsp[(2) - (4)].n));
    }
    break;

  case 527:
#line 3798 "language.yacc"
    {
      yyerror("Missing '>'.");
      (yyval.n)=mkefuncallnode("aggregate_multiset",(yyvsp[(3) - (4)].n));
      COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(2) - (4)].n));
      free_node ((yyvsp[(2) - (4)].n));
    }
    break;

  case 528:
#line 3804 "language.yacc"
    { (yyval.n)=(yyvsp[(1) - (3)].n); yyerrok; }
    break;

  case 529:
#line 3806 "language.yacc"
    {
    (yyval.n)=(yyvsp[(1) - (3)].n); yyerror("Missing ')'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 530:
#line 3810 "language.yacc"
    { (yyval.n)=(yyvsp[(1) - (3)].n); yyerror("Missing ')'."); }
    break;

  case 531:
#line 3811 "language.yacc"
    { (yyval.n)=(yyvsp[(1) - (3)].n); yyerror("Missing ')'."); }
    break;

  case 532:
#line 3812 "language.yacc"
    { (yyval.n)=(yyvsp[(2) - (4)].n); yyerrok; }
    break;

  case 533:
#line 3813 "language.yacc"
    {
    yyerror("Missing '>'.");
    (yyval.n)=(yyvsp[(2) - (4)].n); yyerrok;
  }
    break;

  case 534:
#line 3818 "language.yacc"
    {
    (yyval.n)=(yyvsp[(2) - (4)].n); yyerror("Missing '>)'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 535:
#line 3822 "language.yacc"
    { (yyval.n)=(yyvsp[(2) - (4)].n); yyerror("Missing '>)'."); }
    break;

  case 536:
#line 3823 "language.yacc"
    { (yyval.n)=(yyvsp[(2) - (4)].n); yyerror("Missing '>)'."); }
    break;

  case 537:
#line 3825 "language.yacc"
    {
    (yyval.n)=mknode(F_ARROW,(yyvsp[(1) - (4)].n),(yyvsp[(4) - (4)].n));
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(3) - (4)].n));
    free_node ((yyvsp[(3) - (4)].n));
  }
    break;

  case 538:
#line 3830 "language.yacc"
    {(yyval.n)=(yyvsp[(1) - (4)].n); free_node ((yyvsp[(3) - (4)].n));}
    break;

  case 540:
#line 3835 "language.yacc"
    {
    int i;

    if(Pike_compiler->last_identifier) free_string(Pike_compiler->last_identifier);
    copy_shared_string(Pike_compiler->last_identifier, (yyvsp[(3) - (3)].n)->u.sval.u.string);

    if (((i = find_shared_string_identifier(Pike_compiler->last_identifier,
					    Pike_compiler->new_program)) >= 0) ||
	((i = really_low_find_shared_string_identifier(Pike_compiler->last_identifier,
						       Pike_compiler->new_program,
						       SEE_PROTECTED|
						       SEE_PRIVATE)) >= 0)) {
      struct reference *ref = Pike_compiler->new_program->identifier_references + i;
      if (!TEST_COMPAT (7, 2) &&
	  IDENTIFIER_IS_VARIABLE (
	    ID_FROM_PTR (Pike_compiler->new_program, ref)->identifier_flags)) {
	/* Allowing local:: on variables would lead to pathological
	 * behavior: If a non-local variable in a class is referenced
	 * both with and without local::, both references would
	 * address the same variable in all cases except where an
	 * inheriting program overrides it (c.f. [bug 1252]).
	 *
	 * Furthermore, that's not how it works currently; if this
	 * error is removed then local:: will do nothing on variables
	 * except forcing a lookup in the closest surrounding class
	 * scope. */
	yyerror ("Cannot make local references to variables.");
	(yyval.n) = 0;
      }
      else {
	if (!(ref->id_flags & ID_HIDDEN)) {
	  /* We need to generate a new reference. */
	  int d;
	  struct reference funp = *ref;
	  funp.id_flags = (funp.id_flags & ~ID_INHERITED) | ID_INLINE|ID_HIDDEN;
	  i = -1;
	  for(d = 0; d < (int)Pike_compiler->new_program->num_identifier_references; d++) {
	    struct reference *refp;
	    refp = Pike_compiler->new_program->identifier_references + d;

	    if((refp->inherit_offset == funp.inherit_offset) &&
	       (refp->identifier_offset == funp.identifier_offset) &&
	       ((refp->id_flags | ID_USED) == (funp.id_flags | ID_USED))) {
	      i = d;
	      break;
	    }
	  }
	  if (i < 0) {
	    add_to_identifier_references(funp);
	    i = Pike_compiler->new_program->num_identifier_references - 1;
	  }
	}
	(yyval.n) = mkidentifiernode(i);
      }
    } else {
      if (Pike_compiler->compiler_pass == 2) {
	my_yyerror("%S not defined in local scope.",
		   Pike_compiler->last_identifier);
	(yyval.n) = 0;
      } else {
	(yyval.n) = mknode(F_UNDEFINED, 0, 0);
      }
    }

    free_node((yyvsp[(3) - (3)].n));
  }
    break;

  case 541:
#line 3902 "language.yacc"
    {
    (yyval.n)=0;
  }
    break;

  case 543:
#line 3909 "language.yacc"
    {
    (yyval.n)=index_node((yyvsp[(1) - (3)].n), Pike_compiler->last_identifier?Pike_compiler->last_identifier->str:NULL,
		  (yyvsp[(3) - (3)].n)->u.sval.u.string);
    free_node((yyvsp[(1) - (3)].n));
    if(Pike_compiler->last_identifier) free_string(Pike_compiler->last_identifier);
    copy_shared_string(Pike_compiler->last_identifier, (yyvsp[(3) - (3)].n)->u.sval.u.string);
    free_node((yyvsp[(3) - (3)].n));
  }
    break;

  case 544:
#line 3918 "language.yacc"
    {
    struct pike_string *dot;
    MAKE_CONST_STRING(dot, ".");
    if (call_handle_import(dot)) {
      node *tmp=mkconstantsvaluenode(Pike_sp-1);
      pop_stack();
      (yyval.n)=index_node(tmp, ".", (yyvsp[(2) - (2)].n)->u.sval.u.string);
      free_node(tmp);
    }
    else
      (yyval.n)=mknewintnode(0);
    if(Pike_compiler->last_identifier) free_string(Pike_compiler->last_identifier);
    copy_shared_string(Pike_compiler->last_identifier, (yyvsp[(2) - (2)].n)->u.sval.u.string);
    free_node((yyvsp[(2) - (2)].n));
  }
    break;

  case 545:
#line 3934 "language.yacc"
    {
    (yyval.n) = resolve_identifier ((yyvsp[(3) - (3)].n)->u.sval.u.string);
    if(Pike_compiler->last_identifier) free_string(Pike_compiler->last_identifier);
    copy_shared_string(Pike_compiler->last_identifier, (yyvsp[(3) - (3)].n)->u.sval.u.string);
    free_node ((yyvsp[(3) - (3)].n));
  }
    break;

  case 546:
#line 3940 "language.yacc"
    {}
    break;

  case 547:
#line 3941 "language.yacc"
    {}
    break;

  case 548:
#line 3945 "language.yacc"
    {
    struct compilation *c = THIS_COMPILATION;
    int e = -1;

    inherit_state = Pike_compiler;

    for (inherit_depth = 0;; inherit_depth++, inherit_state = inherit_state->previous) {
      int inh = find_inherit(inherit_state->new_program, (yyvsp[(1) - (2)].n)->u.sval.u.string);
      if (inh) {
	e = inh;
	break;
      }
      if (inherit_depth == c->compilation_depth) break;
      if (!TEST_COMPAT (7, 2) &&
	  ID_FROM_INT (inherit_state->previous->new_program,
		       inherit_state->parent_identifier)->name ==
	  (yyvsp[(1) - (2)].n)->u.sval.u.string) {
	e = 0;
	break;
      }
    }
    if (e == -1) {
      if (TEST_COMPAT (7, 2))
	my_yyerror("No such inherit %S.", (yyvsp[(1) - (2)].n)->u.sval.u.string);
      else {
	if ((yyvsp[(1) - (2)].n)->u.sval.u.string == this_program_string) {
	  inherit_state = Pike_compiler;
	  inherit_depth = 0;
	  e = 0;
	}
	else
	  my_yyerror("No inherit or surrounding class %S.",
		     (yyvsp[(1) - (2)].n)->u.sval.u.string);
      }
    }
    free_node((yyvsp[(1) - (2)].n));
    (yyval.number) = e;
  }
    break;

  case 549:
#line 3984 "language.yacc"
    {
    struct compilation *c = THIS_COMPILATION;
    inherit_state = Pike_compiler;
    for (inherit_depth = 0; inherit_depth < c->compilation_depth;
	 inherit_depth++, inherit_state = inherit_state->previous) {}
    (yyval.number) = -1;
  }
    break;

  case 550:
#line 3992 "language.yacc"
    {
    int e = 0;
    if ((yyvsp[(1) - (3)].number) < 0) {
      (yyvsp[(1) - (3)].number) = 0;
    }
#if 0
    /* FIXME: The inherit modifiers aren't kept. */
    if (!(inherit_state->new_program->inherits[(yyvsp[(1) - (3)].number)].flags & ID_PRIVATE)) {
#endif /* 0 */
      e = find_inherit(inherit_state->new_program->inherits[(yyvsp[(1) - (3)].number)].prog,
		       (yyvsp[(2) - (3)].n)->u.sval.u.string);
#if 0
    }
#endif /* 0 */
    if (!e) {
      if (inherit_state->new_program->inherits[(yyvsp[(1) - (3)].number)].name) {
	my_yyerror("No such inherit %S::%S.",
		   inherit_state->new_program->inherits[(yyvsp[(1) - (3)].number)].name,
		   (yyvsp[(2) - (3)].n)->u.sval.u.string);
      } else {
	my_yyerror("No such inherit %S.", (yyvsp[(2) - (3)].n)->u.sval.u.string);
      }
      (yyval.number) = -1;
    } else {
      /* We know stuff about the inherit structure... */
      (yyval.number) = e + (yyvsp[(1) - (3)].number);
    }
    free_node((yyvsp[(2) - (3)].n));
  }
    break;

  case 551:
#line 4021 "language.yacc"
    { (yyval.number) = -1; }
    break;

  case 552:
#line 4025 "language.yacc"
    {
    if(Pike_compiler->last_identifier) free_string(Pike_compiler->last_identifier);
    copy_shared_string(Pike_compiler->last_identifier, (yyvsp[(1) - (1)].n)->u.sval.u.string);

    if(((yyval.n)=lexical_islocal(Pike_compiler->last_identifier)))
    {
      /* done, nothing to do here */
    }else if(!((yyval.n)=find_module_identifier(Pike_compiler->last_identifier,1)) &&
	     !((yyval.n) = program_magic_identifier (Pike_compiler, 0, -1,
					      Pike_compiler->last_identifier, 0))) {
      if((Pike_compiler->flags & COMPILATION_FORCE_RESOLVE) ||
	 (Pike_compiler->compiler_pass==2)) {
	my_yyerror("Undefined identifier %S.",
		   Pike_compiler->last_identifier);
	/* FIXME: Add this identifier as a constant in the current program to
	 *        avoid multiple reporting of the same identifier.
	 * NOTE: This should then only be done in the second pass.
	 */	
	(yyval.n)=0;
      }else{
	(yyval.n)=mknode(F_UNDEFINED,0,0);
      }
    }
    free_node((yyvsp[(1) - (1)].n));
  }
    break;

  case 553:
#line 4051 "language.yacc"
    {
    ref_push_string((yyvsp[(1) - (1)].n)->u.sval.u.string);
    low_yyreport(REPORT_ERROR, NULL, 0, parser_system_string,
		 1, "Unknown reserved symbol %s.");
    free_node((yyvsp[(1) - (1)].n));
    (yyval.n) = 0;
  }
    break;

  case 554:
#line 4059 "language.yacc"
    {
    struct compilation *c = THIS_COMPILATION;
    node *tmp2;

    if(Pike_compiler->last_identifier)
      free_string(Pike_compiler->last_identifier);
    copy_shared_string(Pike_compiler->last_identifier, (yyvsp[(3) - (3)].n)->u.sval.u.string);

    tmp2 = mkconstantsvaluenode(&c->default_module);
    (yyval.n) = index_node(tmp2, "predef", (yyvsp[(3) - (3)].n)->u.sval.u.string);
    if(!(yyval.n)->name)
      add_ref( (yyval.n)->name=(yyvsp[(3) - (3)].n)->u.sval.u.string );
    free_node(tmp2);
    free_node((yyvsp[(3) - (3)].n));
  }
    break;

  case 555:
#line 4075 "language.yacc"
    {
    (yyval.n)=0;
  }
    break;

  case 556:
#line 4079 "language.yacc"
    {
    struct compilation *c = THIS_COMPILATION;
    int old_major = Pike_compiler->compat_major;
    int old_minor = Pike_compiler->compat_minor;
    struct svalue *efun = NULL;

    change_compiler_compatibility((yyvsp[(1) - (3)].n)->u.integer.a, (yyvsp[(1) - (3)].n)->u.integer.b);

    if(Pike_compiler->last_identifier)
      free_string(Pike_compiler->last_identifier);
    copy_shared_string(Pike_compiler->last_identifier, (yyvsp[(3) - (3)].n)->u.sval.u.string);

    /* Check predef:: first, and then the modules. */

    (yyval.n) = 0;

    if (c->default_module.type == T_MAPPING) {
      if ((efun = low_mapping_lookup(c->default_module.u.mapping,
				     &((yyvsp[(3) - (3)].n)->u.sval))))
	(yyval.n) = mkconstantsvaluenode(efun);
    }

    else if (c->default_module.type != T_INT) {
      JMP_BUF tmp;
      if (SETJMP (tmp))
	handle_compile_exception ("Couldn't index %d.%d "
				  "default module with %O.",
				  (yyvsp[(1) - (3)].n)->u.integer.a, (yyvsp[(1) - (3)].n)->u.integer.b,
				  &(yyvsp[(3) - (3)].n)->u.sval);
      else {
	push_svalue (&c->default_module);
	push_svalue (&(yyvsp[(3) - (3)].n)->u.sval);
	f_index (2);
	if (!IS_UNDEFINED (Pike_sp - 1))
	  (yyval.n) = mkconstantsvaluenode (Pike_sp - 1);
	pop_stack();
      }
      UNSETJMP(tmp);
    }

    if (!(yyval.n) && !((yyval.n) = resolve_identifier(Pike_compiler->last_identifier))) {
      if((Pike_compiler->flags & COMPILATION_FORCE_RESOLVE) ||
	 (Pike_compiler->compiler_pass==2)) {
	my_yyerror("Undefined identifier %d.%d::%S.",
		   (yyvsp[(1) - (3)].n)->u.integer.a, (yyvsp[(1) - (3)].n)->u.integer.b,
		   Pike_compiler->last_identifier);
	(yyval.n)=0;
      }else{
	(yyval.n)=mknode(F_UNDEFINED,0,0);
      }
    }

    change_compiler_compatibility(old_major, old_minor);
    free_node((yyvsp[(1) - (3)].n));
    free_node((yyvsp[(3) - (3)].n));
  }
    break;

  case 557:
#line 4136 "language.yacc"
    {
    free_node((yyvsp[(1) - (3)].n));
    (yyval.n)=0;
  }
    break;

  case 558:
#line 4141 "language.yacc"
    {
    int id;

    if(Pike_compiler->last_identifier) free_string(Pike_compiler->last_identifier);
    copy_shared_string(Pike_compiler->last_identifier, (yyvsp[(2) - (2)].n)->u.sval.u.string);

    if ((yyvsp[(1) - (2)].number) > 0)
      id = low_reference_inherited_identifier(inherit_state,
					      (yyvsp[(1) - (2)].number),
					      Pike_compiler->last_identifier,
					      SEE_PROTECTED);
    else
      id = really_low_find_shared_string_identifier(Pike_compiler->last_identifier,
						    inherit_state->new_program,
						    SEE_PROTECTED|SEE_PRIVATE);

    if (id != -1) {
      if (inherit_depth > 0) {
	(yyval.n) = mkexternalnode(inherit_state->new_program, id);
      } else {
	(yyval.n) = mkidentifiernode(id);
      }
    } else if (((yyval.n) = program_magic_identifier (inherit_state, inherit_depth, (yyvsp[(1) - (2)].number),
					       Pike_compiler->last_identifier, 1))) {
      /* All done. */
    }
    else {
      if ((Pike_compiler->flags & COMPILATION_FORCE_RESOLVE) ||
	  (Pike_compiler->compiler_pass == 2)) {
	if (((yyvsp[(1) - (2)].number) >= 0) && inherit_state->new_program->inherits[(yyvsp[(1) - (2)].number)].name) {
	  my_yyerror("Undefined identifier %S::%S.",
		     inherit_state->new_program->inherits[(yyvsp[(1) - (2)].number)].name,
		     Pike_compiler->last_identifier);
	} else {
	  my_yyerror("Undefined identifier %S.",
		     Pike_compiler->last_identifier);
	}
	(yyval.n)=0;
      }
      else
	(yyval.n)=mknode(F_UNDEFINED,0,0);
    }

    free_node((yyvsp[(2) - (2)].n));
  }
    break;

  case 559:
#line 4186 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 560:
#line 4187 "language.yacc"
    { (yyval.n)=0; }
    break;

  case 561:
#line 4189 "language.yacc"
    {
    int e,i;

    if(Pike_compiler->last_identifier) free_string(Pike_compiler->last_identifier);
    copy_shared_string(Pike_compiler->last_identifier, (yyvsp[(2) - (2)].n)->u.sval.u.string);

    (yyval.n)=0;
    for(e=1;e<(int)Pike_compiler->new_program->num_inherits;e++)
    {
      if(Pike_compiler->new_program->inherits[e].inherit_level!=1) continue;
      i=low_reference_inherited_identifier(0,e,(yyvsp[(2) - (2)].n)->u.sval.u.string,SEE_PROTECTED);
      if(i==-1) continue;
      if((yyval.n))
      {
	(yyval.n)=mknode(F_ARG_LIST,(yyval.n),mkidentifiernode(i));
      }else{
	(yyval.n)=mkidentifiernode(i);
      }
    }
    if(!(yyval.n))
    {
      if (!((yyval.n) = program_magic_identifier (Pike_compiler, 0, -1,
					   (yyvsp[(2) - (2)].n)->u.sval.u.string, 1)))
      {
	if (Pike_compiler->compiler_pass == 2) {
	  if (TEST_COMPAT(7,2)) {
	    yywarning("Undefined identifier ::%S.", (yyvsp[(2) - (2)].n)->u.sval.u.string);
	  } else {
	    my_yyerror("Undefined identifier ::%S.", (yyvsp[(2) - (2)].n)->u.sval.u.string);
	  }
	}
	(yyval.n)=mkintnode(0);
      }
    }else{
      if((yyval.n)->token==F_ARG_LIST) (yyval.n)=mkefuncallnode("aggregate",(yyval.n));
    }
    free_node((yyvsp[(2) - (2)].n));
  }
    break;

  case 562:
#line 4228 "language.yacc"
    {
    (yyval.n)=0;
  }
    break;

  case 563:
#line 4235 "language.yacc"
    {(yyval.n) = mknode (F_RANGE_OPEN, NULL, NULL);}
    break;

  case 564:
#line 4237 "language.yacc"
    {(yyval.n) = mknode (F_RANGE_FROM_BEG, (yyvsp[(1) - (1)].n), NULL);}
    break;

  case 565:
#line 4239 "language.yacc"
    {(yyval.n) = mknode (F_RANGE_FROM_END, (yyvsp[(2) - (2)].n), NULL);}
    break;

  case 566:
#line 4241 "language.yacc"
    {
    yyerror("Unexpected end of file.");
    (yyval.n) = mknode (F_RANGE_OPEN, NULL, NULL);
  }
    break;

  case 567:
#line 4246 "language.yacc"
    {
    yyerror("Unexpected end of file.");
    (yyval.n) = mknode (F_RANGE_OPEN, NULL, NULL);
  }
    break;

  case 568:
#line 4253 "language.yacc"
    {
    (yyval.n)=mkopernode("`/",
		  mkopernode("`-",
			     mkopernode("`-",
					mkefuncallnode("gethrvtime",
						       mkintnode(1)),
					mknode(F_COMMA_EXPR,
					       mknode(F_POP_VALUE, (yyvsp[(2) - (2)].n), NULL),
					       mkefuncallnode("gethrvtime",
							      mkintnode(1)))),
			     NULL),
		  mkfloatnode((FLOAT_TYPE)1e9));
  }
    break;

  case 569:
#line 4268 "language.yacc"
    {
    struct pike_type *t;
    node *tmp;

    /* FIXME: Why build the node at all? */
    /* Because the optimizer cannot optimize the root node of the
     * tree properly -Hubbe
     */
    tmp=mknode(F_COMMA_EXPR, (yyvsp[(3) - (4)].n), 0);
    optimize_node(tmp);

    t=(tmp && CAR(tmp) && CAR(tmp)->type ? CAR(tmp)->type : mixed_type_string);
    if(TEST_COMPAT(7,0))
    {
      struct pike_string *s=describe_type(t);
      (yyval.n) = mkstrnode(s);
      free_string(s);
    }else{
      (yyval.n) = mktypenode(t);
    }
    free_node(tmp);
  }
    break;

  case 570:
#line 4290 "language.yacc"
    { (yyval.n)=0; yyerrok; }
    break;

  case 571:
#line 4291 "language.yacc"
    { (yyval.n)=0; yyerror("Missing ')'."); }
    break;

  case 572:
#line 4293 "language.yacc"
    {
    (yyval.n)=0; yyerror("Missing ')'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 573:
#line 4297 "language.yacc"
    { (yyval.n)=0; yyerror("Missing ')'."); }
    break;

  case 574:
#line 4300 "language.yacc"
    { (yyval.n)=(yyvsp[(2) - (3)].n); }
    break;

  case 575:
#line 4301 "language.yacc"
    { (yyval.n)=0; yyerrok; }
    break;

  case 576:
#line 4303 "language.yacc"
    {
    (yyval.n)=0; yyerror("Missing ')'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 577:
#line 4307 "language.yacc"
    { (yyval.n)=0; yyerror("Missing ')'."); }
    break;

  case 578:
#line 4308 "language.yacc"
    { (yyval.n)=0; yyerror("Missing ')'."); }
    break;

  case 580:
#line 4310 "language.yacc"
    { (yyval.n)=0; yyerror("Bad expression for catch."); }
    break;

  case 581:
#line 4314 "language.yacc"
    {
       Pike_compiler->catch_level++;
     }
    break;

  case 582:
#line 4318 "language.yacc"
    {
       (yyval.n)=mknode(F_CATCH,(yyvsp[(3) - (3)].n),NULL);
       Pike_compiler->catch_level--;
     }
    break;

  case 583:
#line 4325 "language.yacc"
    {
    if ((yyvsp[(6) - (7)].n) && !(THIS_COMPILATION->lex.pragmas & ID_STRICT_TYPES)) {
      mark_lvalues_as_used((yyvsp[(6) - (7)].n));
    }
    (yyval.n)=mknode(F_SSCANF,mknode(':', mkintnode(TEST_COMPAT(7, 6)? SSCANF_FLAG_76_COMPAT : 0), mknode(F_ARG_LIST,(yyvsp[(3) - (7)].n),(yyvsp[(5) - (7)].n))),(yyvsp[(6) - (7)].n));
  }
    break;

  case 584:
#line 4332 "language.yacc"
    {
    (yyval.n)=0;
    free_node((yyvsp[(3) - (7)].n));
    free_node((yyvsp[(5) - (7)].n));
    yyerrok;
  }
    break;

  case 585:
#line 4339 "language.yacc"
    {
    (yyval.n)=0;
    free_node((yyvsp[(3) - (7)].n));
    free_node((yyvsp[(5) - (7)].n));
    yyerror("Missing ')'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 586:
#line 4347 "language.yacc"
    {
    (yyval.n)=0;
    free_node((yyvsp[(3) - (7)].n));
    free_node((yyvsp[(5) - (7)].n));
    yyerror("Missing ')'.");
  }
    break;

  case 587:
#line 4354 "language.yacc"
    {
    (yyval.n)=0;
    free_node((yyvsp[(3) - (7)].n));
    free_node((yyvsp[(5) - (7)].n));
    yyerror("Missing ')'.");
  }
    break;

  case 588:
#line 4361 "language.yacc"
    {
    (yyval.n)=0;
    free_node((yyvsp[(3) - (5)].n));
    yyerrok;
  }
    break;

  case 589:
#line 4367 "language.yacc"
    {
    (yyval.n)=0;
    free_node((yyvsp[(3) - (5)].n));
    yyerror("Missing ')'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 590:
#line 4374 "language.yacc"
    {
    (yyval.n)=0;
    free_node((yyvsp[(3) - (5)].n));
    yyerror("Missing ')'.");
  }
    break;

  case 591:
#line 4380 "language.yacc"
    {
    (yyval.n)=0;
    free_node((yyvsp[(3) - (5)].n));
    yyerror("Missing ')'.");
  }
    break;

  case 592:
#line 4385 "language.yacc"
    { (yyval.n)=0; yyerrok; }
    break;

  case 593:
#line 4387 "language.yacc"
    {
    (yyval.n)=0; yyerror("Missing ')'.");
    yyerror("Unexpected end of file.");
  }
    break;

  case 594:
#line 4391 "language.yacc"
    { (yyval.n)=0; yyerror("Missing ')'."); }
    break;

  case 595:
#line 4392 "language.yacc"
    { (yyval.n)=0; yyerror("Missing ')'."); }
    break;

  case 597:
#line 4397 "language.yacc"
    {
    (yyval.n)=mknode(F_ARRAY_LVALUE, (yyvsp[(2) - (3)].n),0);
    COPY_LINE_NUMBER_INFO((yyval.n), (yyvsp[(1) - (3)].n));
    free_node((yyvsp[(1) - (3)].n));
  }
    break;

  case 598:
#line 4403 "language.yacc"
    {
    int id = add_local_name((yyvsp[(2) - (2)].n)->u.sval.u.string,compiler_pop_type(),0);
    /* Note: Variable intentionally not marked as used. */
    if (id >= 0)
      (yyval.n)=mklocalnode(id,-1);
    else
      (yyval.n) = 0;
    free_node((yyvsp[(2) - (2)].n));
  }
    break;

  case 599:
#line 4414 "language.yacc"
    { (yyval.n)=mknewintnode(0); }
    break;

  case 600:
#line 4418 "language.yacc"
    {
    (yyval.n)=mknode(F_LVALUE_LIST,(yyvsp[(1) - (2)].n),(yyvsp[(2) - (2)].n));
  }
    break;

  case 601:
#line 4423 "language.yacc"
    { (yyval.n) = 0; }
    break;

  case 602:
#line 4425 "language.yacc"
    {
    (yyval.n) = mknode(F_LVALUE_LIST,(yyvsp[(2) - (3)].n),(yyvsp[(3) - (3)].n));
  }
    break;

  case 604:
#line 4432 "language.yacc"
    {
    struct compiler_frame *f = Pike_compiler->compiler_frame;
    if (!f || (f->current_function_number < 0)) {
      (yyval.n) = mkstrnode(lfun_strings[LFUN___INIT]);
    } else {
      struct identifier *id =
	ID_FROM_INT(Pike_compiler->new_program, f->current_function_number);
      if (!id->name->size_shift) {
	int len;
	if ((len = strlen(id->name->str)) == id->name->len) {
	  /* Most common case. */
	  (yyval.n) = mkstrnode(id->name);
	} else {
	  struct pike_string *str =
	    make_shared_binary_string(id->name->str, len);
	  (yyval.n) = mkstrnode(str);
	  free_string(str);
	}
      } else {
	struct pike_string *str;
	struct array *split;
	MAKE_CONST_STRING(str, "\0");
	split = explode(id->name, str);
	(yyval.n) = mkstrnode(split->item->u.string);
	free_array(split);
      }
    }
  }
    break;

  case 606:
#line 4464 "language.yacc"
    {
    struct pike_string *a,*b;
    copy_shared_string(a,(yyvsp[(1) - (2)].n)->u.sval.u.string);
    copy_shared_string(b,(yyvsp[(2) - (2)].n)->u.sval.u.string);
    free_node((yyvsp[(1) - (2)].n));
    free_node((yyvsp[(2) - (2)].n));
    a=add_and_free_shared_strings(a,b);
    (yyval.n)=mkstrnode(a);
    free_string(a);
  }
    break;

  case 608:
#line 4483 "language.yacc"
    { yyerror_reserved("array"); }
    break;

  case 609:
#line 4485 "language.yacc"
    { yyerror_reserved("__attribute__"); }
    break;

  case 610:
#line 4487 "language.yacc"
    { yyerror_reserved("class"); }
    break;

  case 611:
#line 4489 "language.yacc"
    { yyerror_reserved("__deprecated__"); }
    break;

  case 612:
#line 4491 "language.yacc"
    { yyerror_reserved("enum"); }
    break;

  case 613:
#line 4493 "language.yacc"
    { yyerror_reserved("float");}
    break;

  case 614:
#line 4495 "language.yacc"
    { yyerror_reserved("function");}
    break;

  case 615:
#line 4497 "language.yacc"
    { yyerror_reserved("__FUNCTION__");}
    break;

  case 616:
#line 4499 "language.yacc"
    { yyerror_reserved("int"); }
    break;

  case 617:
#line 4501 "language.yacc"
    { yyerror_reserved("mapping"); }
    break;

  case 618:
#line 4503 "language.yacc"
    { yyerror_reserved("mixed"); }
    break;

  case 619:
#line 4505 "language.yacc"
    { yyerror_reserved("multiset"); }
    break;

  case 620:
#line 4507 "language.yacc"
    { yyerror_reserved("object"); }
    break;

  case 621:
#line 4509 "language.yacc"
    { yyerror_reserved("program"); }
    break;

  case 622:
#line 4511 "language.yacc"
    { yyerror_reserved("string"); }
    break;

  case 623:
#line 4513 "language.yacc"
    { yyerror_reserved("typedef"); }
    break;

  case 624:
#line 4515 "language.yacc"
    { yyerror_reserved("void"); }
    break;

  case 625:
#line 4517 "language.yacc"
    {
    ref_push_string((yyvsp[(1) - (1)].n)->u.sval.u.string);
    low_yyreport(REPORT_ERROR, NULL, 0, parser_system_string,
		 1, "Unknown reserved symbol %s.");
    free_node((yyvsp[(1) - (1)].n));
  }
    break;

  case 626:
#line 4527 "language.yacc"
    { yyerror_reserved("inline"); }
    break;

  case 627:
#line 4529 "language.yacc"
    { yyerror_reserved("local"); }
    break;

  case 628:
#line 4531 "language.yacc"
    { yyerror_reserved("nomask"); }
    break;

  case 629:
#line 4533 "language.yacc"
    { yyerror_reserved("predef"); }
    break;

  case 630:
#line 4535 "language.yacc"
    { yyerror_reserved("private"); }
    break;

  case 631:
#line 4537 "language.yacc"
    { yyerror_reserved("protected"); }
    break;

  case 632:
#line 4539 "language.yacc"
    { yyerror_reserved("public"); }
    break;

  case 633:
#line 4541 "language.yacc"
    { yyerror_reserved("optional"); }
    break;

  case 634:
#line 4543 "language.yacc"
    { yyerror_reserved("variant"); }
    break;

  case 635:
#line 4545 "language.yacc"
    { yyerror_reserved("static"); }
    break;

  case 636:
#line 4547 "language.yacc"
    { yyerror_reserved("extern"); }
    break;

  case 637:
#line 4549 "language.yacc"
    { yyerror_reserved("final");}
    break;

  case 638:
#line 4551 "language.yacc"
    { yyerror_reserved("do"); }
    break;

  case 639:
#line 4553 "language.yacc"
    { yyerror("else without if."); }
    break;

  case 640:
#line 4555 "language.yacc"
    { yyerror_reserved("return"); }
    break;

  case 641:
#line 4557 "language.yacc"
    { yyerror_reserved("import"); }
    break;

  case 642:
#line 4559 "language.yacc"
    { yyerror_reserved("facet"); }
    break;

  case 643:
#line 4561 "language.yacc"
    { yyerror_reserved("inherit"); }
    break;

  case 644:
#line 4563 "language.yacc"
    { yyerror_reserved("catch"); }
    break;

  case 645:
#line 4565 "language.yacc"
    { yyerror_reserved("gauge"); }
    break;

  case 646:
#line 4567 "language.yacc"
    { yyerror_reserved("lambda"); }
    break;

  case 647:
#line 4569 "language.yacc"
    { yyerror_reserved("sscanf"); }
    break;

  case 648:
#line 4571 "language.yacc"
    { yyerror_reserved("switch"); }
    break;

  case 649:
#line 4573 "language.yacc"
    { yyerror_reserved("typeof"); }
    break;

  case 650:
#line 4575 "language.yacc"
    { yyerror_reserved("break"); }
    break;

  case 651:
#line 4577 "language.yacc"
    { yyerror_reserved("case"); }
    break;

  case 652:
#line 4579 "language.yacc"
    { yyerror_reserved("continue"); }
    break;

  case 653:
#line 4581 "language.yacc"
    { yyerror_reserved("default"); }
    break;

  case 654:
#line 4583 "language.yacc"
    { yyerror_reserved("for"); }
    break;

  case 655:
#line 4585 "language.yacc"
    { yyerror_reserved("foreach"); }
    break;

  case 656:
#line 4587 "language.yacc"
    { yyerror_reserved("if"); }
    break;


/* Line 1267 of yacc.c.  */
#line 9650 "y.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 4600 "language.yacc"


static void yyerror_reserved(const char *keyword)
{
  my_yyerror("%s is a reserved word.", keyword);
}

static struct pike_string *get_new_name(struct pike_string *prefix)
{
  char buf[40];
  /* Generate a name for a global symbol... */
  sprintf(buf,"__lambda_%ld_%ld_line_%d",
	  (long)Pike_compiler->new_program->id,
	  (long)(Pike_compiler->local_class_counter++ & 0xffffffff), /* OSF/1 cc bug. */
	  (int) THIS_COMPILATION->lex.current_line);
  if (prefix) {
    struct string_builder sb;
    init_string_builder_alloc(&sb, prefix->len + strlen(buf) + 1,
			      prefix->size_shift);
    string_builder_shared_strcat(&sb, prefix);
    string_builder_putchar(&sb, 0);
    string_builder_strcat(&sb, buf);
    return finish_string_builder(&sb);
  }
  return make_shared_string(buf);
}


static int low_islocal(struct compiler_frame *f,
		       struct pike_string *str)
{
  int e;
  for(e=f->current_number_of_locals-1;e>=0;e--)
    if(f->variable[e].name==str) {
      f->variable[e].flags |= LOCAL_VAR_IS_USED;
      return e;
    }
  return -1;
}


/* Add a local variable to the current function in frame.
 * NOTE: Steals the references to type and def, but not to str.
 */
int low_add_local_name(struct compiler_frame *frame,
		       struct pike_string *str,
		       struct pike_type *type,
		       node *def)
{

  if (str->len && !TEST_COMPAT(7,0)) {
    int tmp=low_islocal(frame,str);
    if(tmp>=0 && tmp >= frame->last_block_level)
    {
      my_yyerror("Duplicate local variable %S, "
		 "previous declaration on line %d\n",
		 str, frame->variable[tmp].line);
    }

    if(type == void_type_string)
    {
      my_yyerror("Local variable %S is void.\n", str);
    }
  }

  debug_malloc_touch(def);
  debug_malloc_touch(type);
  debug_malloc_touch(str);
  /* NOTE: The number of locals can be 0..255 (not 256), due to
   *       the use of READ_INCR_BYTE() in apply_low.h.
   */
  if (frame->current_number_of_locals == MAX_LOCAL-1)
  {
    my_yyerror("Too many local variables: no space for local variable %S.",
	       str);
    free_type(type);
    if (def) free_node(def);
    return -1;
  } else {
    int var = frame->current_number_of_locals;

#ifdef PIKE_DEBUG
    check_type_string(type);
#endif /* PIKE_DEBUG */
    if (pike_types_le(type, void_type_string)) {
      if (Pike_compiler->compiler_pass != 1) {
	yywarning("Declaring local variable %S with type void "
		  "(converted to type zero).", str);
      }
      free_type(type);
      copy_pike_type(type, zero_type_string);
    }
    frame->variable[var].type = type;
    frame->variable[var].name = str;
    reference_shared_string(str);
    frame->variable[var].def = def;

    frame->variable[var].line = THIS_COMPILATION->lex.current_line;
    copy_shared_string(frame->variable[var].file,
		       THIS_COMPILATION->lex.current_file);

    if (pike_types_le(void_type_string, type)) {
      /* Don't warn about unused voidable variables. */
      frame->variable[var].flags = LOCAL_VAR_IS_USED;
    } else {
      frame->variable[var].flags = 0;
    }

    frame->current_number_of_locals++;
    if(frame->current_number_of_locals > frame->max_number_of_locals)
    {
      frame->max_number_of_locals = frame->current_number_of_locals;
    }

    return var;
  }
}


/* argument must be a shared string */
/* Note that this function eats a reference to 'type' */
/* If def is nonzero, it also eats a ref to def */
int add_local_name(struct pike_string *str,
		   struct pike_type *type,
		   node *def)
{
  return low_add_local_name(Pike_compiler->compiler_frame,
			    str,
			    type,
			    def);
}

/* Mark local variables declared in a multi-assign or sscanf expression
 * as used. */
static void mark_lvalues_as_used(node *n)
{
  while (n && n->token == F_LVALUE_LIST) {
    if (!CAR(n)) {
      /* Can happen if a variable hasn't been declared. */
    } else if (CAR(n)->token == F_ARRAY_LVALUE) {
      mark_lvalues_as_used(CAAR(n));
    } else if ((CAR(n)->token == F_LOCAL) && !(CAR(n)->u.integer.b)) {
      Pike_compiler->compiler_frame->variable[CAR(n)->u.integer.a].flags |=
	LOCAL_VAR_IS_USED;
    }
    n = CDR(n);
  }
}

#if 0
/* Note that this function eats a reference to each of
 * 'type' and 'initializer', but not to 'name'.
 * Note also that 'initializer' may be NULL.
 */
static node *add_protected_variable(struct pike_string *name,
				    struct pike_type *type,
				    int depth,
				    node *initializer)
{
  struct compiler_frame *f = Pike_compiler->compiler_frame;
  int i;
  int id;
  node *n = NULL;

  if (initializer) {
    /* FIXME: We need to pop levels off local and external variables here. */
  }

  for(i = depth; f && i; i--) {
    f->lexical_scope |= SCOPE_SCOPED;
    f = f->previous;
  }
  if (!f) {
    int parent_depth = i;
    struct program_state *p = Pike_compiler;
    struct pike_string *tmp_name;
    while (i--) {
      if (!p->previous) {
	my_yyerror("Too many levels of protected (%d, max:%d).",
		   depth, depth - (i+1));
	parent_depth -= i+1;
	break;
      }
      p->new_program->flags |= PROGRAM_USES_PARENT;
      p = p->previous;
    }
    
    tmp_name = get_new_name(name);
    id = define_parent_variable(p, tmp_name, type,
				ID_PROTECTED|ID_PRIVATE|ID_INLINE);
    free_string(tmp_name);
    if (id >= 0) {
      if (def) {
	p->init_node =
	  mknode(F_COMMA_EXPR, Pike_compiler->init_node,
		 mkcastnode(void_type_string,
			    mknode(F_ASSIGN, initializer,
				   mkidentifiernode(id))));
	initializer = NULL;
      }
      n = mkexternalnode(id, parent_depth);
    }
  } else if (depth) {
    f->lexical_scope|=SCOPE_SCOPE_USED;
    tmp_name = get_new_name(name);
    id = low_add_local_name(f, tmp_name, type, NULL);
    free_string(tmp_name);
    if(f->min_number_of_locals < id+1)
      f->min_number_of_locals = id+1;
    if (initializer) {
      /* FIXME! */
      yyerror("Initializers not yet supported for protected variables with function scope.");
    }
    n = mklocalnode(id, depth);
  }
  id = add_local_name(name, type, n);
  if (id >= 0) {
    if (initializer) {
      return mknode(F_ASSIGN, initializer, mklocalnode(id,0));
    }
    return mklocalnode(id, 0);
  }
  if (initializer) {
    free_node(initializer);
  }
  return NULL;
}
#endif /* 0 */

int islocal(struct pike_string *str)
{
  return low_islocal(Pike_compiler->compiler_frame, str);
}

/* argument must be a shared string */
static node *lexical_islocal(struct pike_string *str)
{
  int e,depth=0;
  struct compiler_frame *f=Pike_compiler->compiler_frame;
  
  while(1)
  {
    for(e=f->current_number_of_locals-1;e>=0;e--)
    {
      if(f->variable[e].name==str)
      {
	struct compiler_frame *q=Pike_compiler->compiler_frame;

	f->variable[e].flags |= LOCAL_VAR_IS_USED;

	while(q!=f) 
	{
	  q->lexical_scope|=SCOPE_SCOPED;
	  q=q->previous;
	}

	if(depth) {
	  q->lexical_scope|=SCOPE_SCOPE_USED;

	  if(q->min_number_of_locals < e+1)
	    q->min_number_of_locals = e+1;
	}

	if(f->variable[e].def) {
	  /*fprintf(stderr, "Found prior definition of \"%s\"\n", str->str); */
	  return copy_node(f->variable[e].def);
	}

	return mklocalnode(e,depth);
      }
    }
    if(!(f->lexical_scope & SCOPE_LOCAL)) return 0;
    depth++;
    f=f->previous;
  }
}

static node *safe_inc_enum(node *n)
{
  JMP_BUF recovery;
  STACK_LEVEL_START(0);

  if (SETJMP(recovery)) {
    handle_compile_exception ("Bad implicit enum value (failed to add 1).");
    push_int(0);
  } else {
    if (n->token != F_CONSTANT) Pike_fatal("Bad node to safe_inc_enum().\n");
    push_svalue(&n->u.sval);
    push_int(1);
    f_add(2);
  }
  UNSETJMP(recovery);
  STACK_LEVEL_DONE(1);
  free_node(n);
  n = mkconstantsvaluenode(Pike_sp-1);
  pop_stack();
  return n;
}


static int call_handle_import(struct pike_string *s)
{
  struct compilation *c = THIS_COMPILATION;
  int args;

  ref_push_string(s);
  ref_push_string(c->lex.current_file);
  if (c->handler && c->handler->prog) {
    ref_push_object(c->handler);
    args = 3;
  }
  else args = 2;

  if (safe_apply_handler("handle_import", c->handler, c->compat_handler,
			 args, BIT_MAPPING|BIT_OBJECT|BIT_PROGRAM|BIT_ZERO))
    if (Pike_sp[-1].type != T_INT)
      return 1;
    else {
      pop_stack();
      my_yyerror("Couldn't find module to import: %S", s);
    }
  else
    handle_compile_exception ("Error finding module to import");

  return 0;
}

void cleanup_compiler(void)
{
}

