/* $Id: addrows.c,v 1.4 2004/12/04 14:54:19 dannybackx Exp $ */

#include <stdlib.h>
#include <stdio.h>

#include <X11/Intrinsic.h>
#include <Xm/Xm.h>

#include <Xbae/Matrix.h> /* for version info */
#include <Xm/Form.h>
#include <Xm/PushB.h>

#define	NUM_ARGS_PER_ROW	5

Widget form, mat;

void doit()
{
	int	num_cmds = 5;
	int	x, y, z;
	int	num_args_in_cmd = 5;
	char buff[30];
      
      char *cmd_labels[] = { "", "TIME",
                           "ARG1",  "ARG2",  "ARG3",  "ARG4",  "ARG5",
                           "ARG6",  "ARG7",  "ARG8",  "ARG9",  "ARG10",
                           "ARG11", "ARG12", "ARG13", "ARG14", "ARG15",
                           "ARG16", "ARG17", "ARG18", "ARG19", "ARG20",
                           "ARG21", "ARG22", "ARG23", "ARG24", "ARG25",
                           "ARG26", "ARG27", "ARG28", "ARG29", "ARG30", "ARG31"
                          };

      short cmd_widths[] = { 12, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
                                  7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
                                  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7 };
#if 1
      mat = XtVaCreateWidget ("test_xbae",
                             xbaeMatrixWidgetClass,
                             form,
                             XmNtopAttachment,    XmATTACH_FORM,
                             XmNleftAttachment,   XmATTACH_FORM,
                             XmNbottomAttachment, XmATTACH_FORM,
                             XmNtopOffset, 10,
                             XmNbottomOffset, 100,
                             XmNleftOffset, 10,
                             XmNrightOffset, 10,
                             XmNrightAttachment,  XmATTACH_FORM,
                             XmNcolumns, XtNumber(cmd_labels),
                             XmNcolumnLabels, cmd_labels,
                             XmNcolumnWidths, cmd_widths,
                              XmNrows, 0,
                              NULL);
       XbaeMatrixAddRows(mat, 0, NULL, NULL, NULL, num_cmds); 
#else
      mat = XtVaCreateWidget ("test_xbae",
                             xbaeMatrixWidgetClass,
                             form,
                             XmNtopAttachment,    XmATTACH_FORM,
                             XmNleftAttachment,   XmATTACH_FORM,
                             XmNbottomAttachment, XmATTACH_FORM,
                             XmNtopOffset, 10,
                             XmNbottomOffset, 10,
                             XmNleftOffset, 10,
                             XmNrightOffset, 10,
                             XmNrightAttachment,  XmATTACH_FORM,
                             XmNcolumns, XtNumber(cmd_labels),
                             XmNcolumnLabels, cmd_labels,
                             XmNcolumnWidths, cmd_widths,
                              XmNrows, num_cmds,
                              NULL);
#endif
       for(x = 0; x < num_cmds; x++ )
       {
         printf ("Handling Command #%d\n", x);
     
          sprintf(buff, "Test %d", x);
         XbaeMatrixSetCell(mat, x, 0, buff); 
         sprintf(buff, "Delay %d", x);
         XbaeMatrixSetCell(mat, x, 1, buff); 
         for( y = 0; y < num_args_in_cmd; y++ )
         {
           XbaeMatrixSetCell(mat, x, y+2 , "XX"); 
         }

         for (z = y; z < NUM_ARGS_PER_ROW; z++)
         {
            XbaeMatrixSetCell(mat, x, z+2 , "---"); 
          }

       }
       XtManageChild(mat);
}

void push(Widget w, XtPointer client, XtPointer call)
{
	int	i, ncols;
	char	**str;

	ncols = XbaeMatrixNumColumns(mat);
	str = (char **)XtCalloc(ncols, sizeof(String));
	for (i=0; i<ncols; i++)
		str[i] = "xxx";
	for (i=0; i<100; i++) {
		XbaeMatrixAddRows(mat, XbaeMatrixNumRows(mat), str, NULL, NULL, 1);
		XbaeMatrixDeleteRows(mat, 0, 1);
	}
	XtFree((void *)str);
}

int
main (int argc, char **argv)
{
  Widget toplevel, pb;

  /* Print run-time version of libXbae to stdout */
  printf("Using Xbae %s\n", XbaeGetVersionTxt());
  printf("This example was built with %s\n", XbaeVersionTxt);

  /* Print compile-time version of libXm to stdout */
  printf("Xbae was built with %s\n", XbaeGetXmVersionTxt());
  printf("and is running with %s\n", XmVERSION_STRING);

  toplevel = XtInitialize (argv[0], "Test", NULL, 0, &argc, argv);
  form = XtVaCreateManagedWidget("form", xmFormWidgetClass, toplevel,
	XmNwidth, 	500,
	XmNheight,	400,
	NULL);

  doit();
  
  pb = XtVaCreateManagedWidget("add/delete rows", xmPushButtonWidgetClass, form,
	XmNtopAttachment,	XmATTACH_WIDGET,
	XmNtopWidget,		mat,
	XmNtopOffset,		10,
	XmNleftAttachment,	XmATTACH_FORM,
	XmNleftOffset,		10,
  	NULL);
  XtAddCallback(pb, XmNactivateCallback, push, NULL);
  
  XtRealizeWidget (toplevel);
  XtMainLoop ();

  return (EXIT_SUCCESS);
}
