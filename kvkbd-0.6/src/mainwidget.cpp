/*
 * This file is part of the Kvkbd project.
 * Copyright (C) 2007-2008 Todor Gyumyushev <yodor@developer.bg>
 * Copyright (C) 2008 Guillaume Martres <smarter@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "mainwidget.h"
#include "kbddock.h"
#include "vbutton.h"
#include "numpadvbutton.h"
#include "keysym2ucs.h"

#include <math.h>

#include <X11/extensions/XTest.h>
#include <X11/keysym.h>
#include <X11/keysymdef.h>
#include <fixx11h.h>

#include <QX11Info>
#include <QDesktopWidget>
#include <QDBusConnection>
#include <QHash>
#include <QTimer>
#include <QPainter>
#include <QResizeEvent>
#include <QApplication>
#include <KMenu>
#include "kvkbdadaptor.h"

#include <KAboutData>
#include <KAction>
#include <KToggleAction>
#include <KFontDialog>
#include <KHelpMenu>
#include <KConfigGroup>
#include <KDebug>

#define ROWLENGTH1 13
#define ROWLENGTH2 10
#define ROWLENGTH3 9
#define ROWLENGTH4 7

VButton *globalShift;

MainWidget::MainWidget(const KAboutData *about, bool loginhelper, QWidget *parent, Qt::WindowFlags f) : ResizableDragWidget(parent, f)
{
    setAttribute(Qt::WA_AlwaysShowToolTips);

    alone = loginhelper;

    new KvkbdAdaptor(this);
    QDBusConnection dbus = QDBusConnection::sessionBus();
    dbus.registerObject("/Kvkbd", this);

    tray = 0;
    dock = 0;
    resizable = true;
    aboutData = about;

    display = QX11Info::display();

    setupKeyboard();
    restoreSettings();
    calculateSize();

    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(close()));

    if (!isAlone()) {
        KConfigGroup cfg = KGlobal::config()->group("");

        bool visible = cfg.readEntry("visible", true);
        bool showDock = cfg.readEntry("showdock", false);

        dock = new KbdDock(this);

        tray = new KbdTray(this);
        tray->show();

        buttonMenu = new VButton(this);
        buttonMenu->resize(22, 30);
        buttonMenu->move(523, 15 + 35);
        buttonMenu->res();
        otherKeys.append(buttonMenu);

        buttonMenu->setMenu(tray->contextMenu());
        buttonMenu->setIcon(SmallIcon("configure"));
        buttonMenu->setToolTip(i18nc("@info:tooltip Displayed on configure button", "Configure"));

        dock->setVisible(showDock);
        setVisible(visible);
    } else {
        show();
    }
}

void MainWidget::restoreSettings()
{
    KConfigGroup cfg = KGlobal::config()->group("");

    bool locked = cfg.readEntry("locked", false);
    bool autoResize = cfg.readEntry("autoresfont", true);
    QFont font = cfg.readEntry("KvkbdFont", QFont());
    bool extentVisible = cfg.readEntry("extentVisible", false);

    setLocked(locked);
    setAutoResized(autoResize);
    updateFont(font);

    if (extentVisible) {
        isExtentVisible = false;
        toggleNumericPad();
    }

    restorePosition();
}

bool MainWidget::isAlone() const
{
    return alone;
}

bool MainWidget::autoResized() const
{
    return autoResize;
}

void MainWidget::setAutoResized(bool mode)
{
    autoResize = mode;
    emit(autoResizeToggled(mode));
}

void MainWidget::restorePosition()
{
    QDesktopWidget *desktop = QApplication::desktop();
    QRect screenGeom = desktop->screenGeometry();
    int dWidth = 0;
    if (isExtentVisible) {
        dWidth = 700;
    } else {
        dWidth = 550;
    }
    int dHeight = 235;
    QRect dfltGeom(screenGeom.width() - dWidth, screenGeom.height() - dHeight, dWidth, dHeight);

    KConfigGroup cfg = KGlobal::config()->group("");

    if (cfg.isValid()) {
        QRect geom  = cfg.readEntry("geometry", QRect());
        if (!geom.isNull() && geom.isValid()) {
            dfltGeom = geom;
        }
    }
    setGeometry(dfltGeom);
}

void MainWidget::saveState()
{
    KConfigGroup cfg = KGlobal::config()->group("");
    if (cfg.isValid()) {
        cfg.writeEntry("visible", isVisible());
        cfg.writeEntry("geometry", geometry());
        cfg.sync();
    }
}

void MainWidget::calculateSize()
{
    if (isExtentVisible) {
        sdxb = width();
        sdxs = width() - (width() * (150.0 / 700.0));
        VButton::pw = 700.0;
        VButton::ph = 235.0;
        setMinimumSize(700 / 3, 235 / 3);
    } else {
        sdxs = width();
        sdxb = width() + (width() * (150.0 / 550.0));
        VButton::pw = 550.0;
        VButton::ph = 235.0;
        setMinimumSize(550 / 3, 235 / 3);
    }
}

void MainWidget::closeEvent(QCloseEvent *e)
{
    saveState();
}

void MainWidget::resizeEvent(QResizeEvent * e)
{
    if (!resizable)
        return;

    const QSize sz = e->size();

    calculateSize();

    for (int a = 0; a < buttonsList.size(); a++) {
        VButton *v = buttonsList.at(a);
        v->reposition(sz.width(), sz.height());
    }
    for (int a = 0; a < modKeys.size(); a++) {
        VButton *v = modKeys.at(a);
        v->reposition(sz.width(), sz.height());
    }
    for (int a = 0; a < otherKeys.size(); a++) {
        VButton *v = otherKeys.at(a);
        v->reposition(sz.width(), sz.height());
    }
    for (int a = 0; a < numLockKeys.size(); a++) {
        VButton *v = numLockKeys.at(a);
        v->reposition(sz.width(), sz.height());
    }

    if (autoResized()) {
        double rp = (8.0 / 600.0) * sdxs;
        QFont fnt = this->font();
        fnt.setPointSizeF(rp);
        setFont(fnt);
    }

}

void MainWidget::updateFont(QFont &font)
{
//  fnt.setWeight(QFont::Bold);
    if (autoResized()) {
        double rp = (8.0 / 600.0) * width();
        font.setPointSizeF(rp);
    }
    setFont(font);
    KConfigGroup cfg = KGlobal::config()->group("");
    cfg.writeEntry("KvkbdFont", font);
    cfg.sync();
}
void MainWidget::toggleNumericPad()
{
    resizable = false;
    KConfigGroup cfg = KGlobal::config()->group("");
    cfg.writeEntry("extentVisible", !isExtentVisible);
    cfg.sync();

    if (isExtentVisible) {
        isExtentVisible = false;
        QWidget::resize((int)sdxs, height());
        //extent->setDown(false);
        extent->setIcon(KIcon("arrow-right-double"));
        extent->setToolTip(i18nc("@info:tooltip Displayed on button for toggling numpad", "Show Numeric Keypad"));
    } else {
        isExtentVisible = true;
        //extent->setDown(true);
        extent->setIcon(KIcon("arrow-left-double"));
        resize((int)sdxb, height());

        // Make sure the numpad doesn't go beyond the screen edge
        QDesktopWidget *desktop = QApplication::desktop();
        QRect screenGeometry = desktop->screenGeometry();
        int screenRight = screenGeometry.x() + screenGeometry.width();
        int widgetRight = x() + width();
        int overlap = widgetRight - screenRight;
        if (overlap > 0) {
            move(screenRight - width(), y());
        }

        extent->setToolTip(i18nc("@info:tooltip Displayed on button for toggling numpad", "Hide Numeric Keypad"));
    }
    resizable = true;
}

void MainWidget::chooseFont()
{
    // Hide the widget before showing the dialog to avoid overlapping it
    bool toShow = false;
    if (isVisible()) {
        hide();
        toShow = true;
    }

    QFont font = this->font();
    KFontDialog *fontDialog = new KFontDialog(this);
    if (fontDialog->getFont(font) == KFontDialog::Accepted) {
        updateFont(font);
    }

    if (toShow) {
        show();
    }
}

void MainWidget::lockWidget(bool lock)
{
    setLocked(lock);
    KConfigGroup cfg = KGlobal::config()->group("");
    cfg.writeEntry("locked", lock);
    cfg.sync();
}

void MainWidget::showDock(bool showit)
{
    dock->setVisible(showit);
    KConfigGroup cfg = KGlobal::config()->group("");
    cfg.writeEntry("showdock", showit);
    cfg.sync();
}

void MainWidget::autoResizeFont(bool autores)
{
    setAutoResized(autores);
    KConfigGroup cfg = KGlobal::config()->group("");
    cfg.writeEntry("autoresfont", autores);
    cfg.sync();
}

void MainWidget::toggleNumlock()
{
    bool p = numLock->isChecked();
    for (int a = 0; a < numLockKeys.size(); a++) {
        NumpadVButton *v = numLockKeys[a];
        v->numLockPressed(p);
    }
}

void MainWidget::toggleCaps()
{
    modTimer->stop();
    sendKey(caps->getKeyCode());
    modTimer->start();

    resetKeysCap();
}

void MainWidget::resetKeysCap()
{

    for (int a = 0; a < buttonsList.size(); a++) {
        VButton *v = buttonsList[a];
        v->setKeyCap(leftShift->isChecked() || rightShift->isChecked(), caps->isChecked());
    }

}

void MainWidget::keyPress(unsigned int a)
{
    // Prevent modKeys state from changing state three times in a row
    // because the modTimer timed out when a modKey was clicked
    modTimer->stop();
    sendKey(a);
    modTimer->start();

    for (int a = 0; a < modKeys.size(); a++) {
        VButton *mod = (VButton*) modKeys[a];
        mod->setChecked(false);
    }

    resetKeysCap();
}

void MainWidget::sendKey(unsigned int keycode)
{
    Window currentFocus;
    int revertTo;

    XGetInputFocus(display, &currentFocus, &revertTo);

    for (int a = 0; a < modKeys.size(); a++) {
        VButton *mod = (VButton*) modKeys[a];
        if (mod->isChecked()) {
            XTestFakeKeyEvent(display, mod->getKeyCode(), true, 0);
        }
    }

    XTestFakeKeyEvent(display, keycode, true, 1);
    XTestFakeKeyEvent(display, keycode, false, 2);
    for (int a = 0; a < modKeys.size(); a++) {
        VButton *mod = (VButton*) modKeys[a];
        if (mod->isChecked()) {
            XTestFakeKeyEvent(display, mod->getKeyCode(), false, 2);
        }
    }
    XFlush(display);

}

bool MainWidget::keyState(int iKey)
{
    int          iKeyMask = 0;
    Window       wDummy1, wDummy2;
    int          iDummy3, iDummy4, iDummy5, iDummy6;
    unsigned int iMask;
    XModifierKeymap* map = XGetModifierMapping(display);
    KeyCode keyCode = XKeysymToKeycode(display, iKey);
    if (keyCode == NoSymbol) return false;
    for (int i = 0; i < 8; ++i) {
        if (map->modifiermap[map->max_keypermod * i] == keyCode) {
            iKeyMask = 1 << i;
        }
    }
    XQueryPointer(display, DefaultRootWindow(display), &wDummy1, &wDummy2, &iDummy3, &iDummy4, &iDummy5, &iDummy6, &iMask);
    XFreeModifiermap(map);
    return (iMask & iKeyMask) != 0;
}

void MainWidget::queryModState()
{
    bool capsState = keyState(XK_Caps_Lock);
    bool numLockState = keyState(XK_Num_Lock);

    if (capsState != caps->isChecked()) {
        caps->setChecked(capsState);
        resetKeysCap();
    }
    if (numLockState != numLock->isChecked()) {
        numLock->setChecked(numLockState);
        toggleNumlock();
    }
}

void MainWidget::setupText(VButton& v)
{

    KeyCode keycode = v.getKeyCode();
    KeySym keysym = XKeycodeToKeysym(display, keycode, 0);
    KeySym keysymShift = XKeycodeToKeysym(display, keycode, 1);

    //HACK: XKeycodeToKeysym report weird values for "^" and "¨"
    //We should consider using another way to get the events if it happens for other keys
    if (keysym == 0xfe52) {
        keysym = 0x005e;
    } else if (keysym == 0xfe57) {
        keysym = 0x00a8;
    }

    if (keysymShift == 0xfe52) {
        keysymShift = 0x005e;
    } else if (keysymShift == 0xfe57) {
        keysymShift = 0x00a8;
    }

    long ret = KeySym2Ucs::keysym2ucs(keysym);

    QString buttonText(QChar((uint)ret));

    // To display an ampersand, the text has to be set to "&&" because
    // "&" is used to create shortcut keys
    if (buttonText == "&") {
        v.setText("&&");
    } else {
        v.setText(buttonText);
    }

    long shiftRet = KeySym2Ucs::keysym2ucs(keysymShift);
    QChar c((uint)shiftRet);
    if (c == '&') {
        v.setShiftText("&&");
    } else {
        v.setShiftText(c);
    }
}

void MainWidget::mappingNotify(XMappingEvent *)
{
    for (int a = 0; a < buttonsList.size(); a++) {
        VButton *v = (VButton *)buttonsList[a];
        setupText(*v);
    }
}

void MainWidget::setupKeyboard()
{
    //QString k1= "`1234567890-=";
    //QString k1s = "~!@#$%^&*()_+";
    unsigned int kc1[ROWLENGTH1] = {49, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21};

    //QString k2= "qwertyuiop";
    //QString k2s = "QWERTYUIOP";
    unsigned int kc2[ROWLENGTH2] = {24, 25, 26, 27, 28, 29, 30, 31, 32, 33};

    //QString k3= "asdfghjkl"; //;'";
    //QString k3s="ASDFGHJKL";
    unsigned int kc3[ROWLENGTH3] = {38, 39, 40, 41, 42, 43, 44, 45, 46}; //,{47,48};

    //QString k4="zxcvbnm"; //,./";
    //QString k4s="ZXCVBNM";
    unsigned int kc4[ROWLENGTH4] = {52, 53, 54, 55, 56, 57, 58};//59,60,61};

    int stx = 15;
    int sty = 15;
    isExtentVisible = false;

    VButton *esc = new VButton(this);
    esc->setKeyCode(9);
    esc->move(stx, sty);
    esc->resize(40, 30);
    esc->setText(i18nc("@action:button", "Esc"));
    esc->res();
    otherKeys.append(esc);
    connect(esc, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));


    for (int a = 0; a < 4; a++) {
        VButton *f = new VButton(this);
        f->setKeyCode(67 + a);
        f->setText('F' + QString("%1").arg(a + 1));
        f->move(stx + esc->width() + (35*a) + 10, sty);
        f->res();
        otherKeys.append(f);
        connect(f, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    }
    for (int a = 0; a < 4; a++) {
        VButton *f = new VButton(this);
        f->setKeyCode(71 + a);
        f->setText('F' + QString("%1").arg(a + 5));
        f->move(stx + esc->width() + (35*a) + 30 + (4*35), sty);
        f->res();
        otherKeys.append(f);
        connect(f, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    }
    for (int a = 0; a < 4; a++) {
        VButton *f = new VButton(this);
        f->setKeyCode(75 + a);
        if (a > 1) f->setKeyCode(93 + a);
        f->setText('F' + QString("%1").arg(a + 9));
        f->move(stx + esc->width() + (35*a) + 35 + (8*35) + 10, sty);
        f->res();
        otherKeys.append(f);
        connect(f, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    }

    //ROW 1
    for (int a = 0; a < ROWLENGTH1; a++) {
        VButton *v = new VButton(this);
        v->setKeyCode(kc1[a]);
        v->move(stx + (35*a), sty + 35);
        connect(v, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
        buttonsList.append(v);
        v->res();
        //caps_buttonsList.append ( v );
    }

    VButton *bksp = new VButton(this);
    bksp->setKeyCode(22);
    bksp->move(stx + (ROWLENGTH1 *35), sty + 35);
    bksp->resize(46, 30);
    bksp->setText(i18nc("@action:button Backspace key", "   &lt;--  ")); // displays "<--"
    bksp->res();
    otherKeys.append(bksp);
    connect(bksp, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));

    //ROW 2
    VButton *tab = new VButton(this);
    tab->setKeyCode(23);
    tab->move(stx, sty + 35 + 35);
    tab->resize(47, 30);
    tab->setText(i18nc("@action:button Tabulation key", "Tab  "));
    tab->res();
    otherKeys.append(tab);
    connect(tab, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));

    for (int a = 0; a < ROWLENGTH2; a++) {
        VButton *v = new VButton(this);
        v->setKeyCode(kc2[a]);
        //v->setText ( k2.mid ( a,1 ) );
        //v->setShiftText ( k2s.mid ( a,1 ) );
        v->move(stx + tab->width() + 5 + (35*a), sty + 35 + 35);
        v->res();
        connect(v, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
        buttonsList.append(v);
    }

    VButton *lbr = new VButton(this);
    lbr->setKeyCode(34);
    lbr->move(stx + tab->width() + 5 + (ROWLENGTH2 *35), sty + (2*35));
    //lbr->setText ( "[" );
    //lbr->setShiftText ( "{" );
    lbr->res();
    connect(lbr, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    buttonsList.append(lbr);

    VButton *rbr = new VButton(this);
    rbr->setKeyCode(35);
    rbr->move(stx + tab->width() + 5 + ((ROWLENGTH2 + 1) *35), sty + (2*35));
    //rbr->setText ( "]" );
    //rbr->setShiftText ( "}" );
    rbr->res();
    connect(rbr, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    buttonsList.append(rbr);

    VButton *bksl = new VButton(this);
    bksl->setKeyCode(51);
    bksl->move(stx + tab->width() + 5 + ((ROWLENGTH2 + 2) *35), sty + 35 + 35);
    bksl->resize(30, 30);
    //bksl->setText ( "\\" );
    //bksl->setShiftText ( "|" );
    bksl->res();
    connect(bksl, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    buttonsList.append(bksl);

    //ROW 3
    caps = new VButton(this);
    caps->setKeyCode(66);
    caps->move(stx, sty + (3*35));
    caps->resize(63, 30);
    caps->setText(i18nc("@action:button", "Caps Lock"));
    caps->setCheckable(true);
    caps->res();
    otherKeys.append(caps);
    connect(caps, SIGNAL(clicked()), this, SLOT(toggleCaps()));

    for (int a = 0; a < ROWLENGTH3; a++) {
        VButton *v = new VButton(this);
        v->setKeyCode(kc3[a]);
        //v->setText ( k3.mid ( a,1 ) );
        //v->setShiftText ( k3s.mid ( a,1 ) );
        v->move(stx + caps->width() + 5 + (35*a), sty + (3*35));
        buttonsList.append(v);
        v->res();
        connect(v, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    }
    VButton *smcl = new VButton(this);
    smcl->setKeyCode(47);
    smcl->move(stx + (ROWLENGTH3 *35) + caps->width() + 5, sty + (3*35));
    //smcl->setText ( ";" );
    //smcl->setShiftText ( ":" );
    connect(smcl, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    buttonsList.append(smcl);
    smcl->res();

    VButton *sngq = new VButton(this);
    sngq->setKeyCode(48);
    sngq->move(stx + ((ROWLENGTH3 + 1) *35) + caps->width() + 5, sty + (3*35));
    //sngq->setText ( "'" );
    //sngq->setShiftText ( "\"" );
    connect(sngq, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    buttonsList.append(sngq);
    sngq->res();

    VButton *enter = new VButton(this);
    enter->setKeyCode(36);
    enter->move(stx + ((ROWLENGTH3 + 2) *35) + caps->width() + 5, sty + (3*35));
    enter->resize(50, 30);
    enter->setText(i18nc("@action:button", "Enter"));
    connect(enter, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    otherKeys.append(enter);
    enter->res();

    //ROW 4
    leftShift = new VButton(this);
    leftShift->setKeyCode(50);
    leftShift->move(stx, sty + (4*35));
    leftShift->resize(80, 30);
    leftShift->setText(i18nc("@action:button", "Shift"));
    leftShift->setCheckable(true);
    connect(leftShift, SIGNAL(toggled(bool)), this, SLOT(resetKeysCap()));
    modKeys.append(leftShift);
    leftShift->res();
    globalShift = leftShift;

    for (int a = 0; a < ROWLENGTH4; a++) {
        VButton *v = new VButton(this);
        v->setKeyCode(kc4[a]);
        //v->setText ( k4.mid ( a,1 ) );
        //v->setShiftText ( k4s.mid ( a,1 ) );
        v->move(stx + 35 + 16 + 35 + (35*a), sty + (4*35));
        buttonsList.append(v);
        v->res();
        connect(v, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    }

    VButton *sm = new VButton(this);
    sm->setKeyCode(59);
    sm->move(stx + (ROWLENGTH4 *35) + leftShift->width() + 5, sty + (4*35));
    //sm->setText ( "," );
    //sm->setShiftText ( "<" );
    connect(sm, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    buttonsList.append(sm);
    sm->res();

    VButton *gr = new VButton(this);
    gr->setKeyCode(60);
    gr->move(stx + ((ROWLENGTH4 + 1) *35) + leftShift->width() + 5, sty + (4*35));
    //gr->setText ( "." );
    //gr->setShiftText ( ">" );
    connect(gr, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    buttonsList.append(gr);
    gr->res();

    VButton *sl = new VButton(this);
    sl->setKeyCode(61);
    sl->move(stx + ((ROWLENGTH4 + 2) *35) + leftShift->width() + 5, sty + (4*35));
    //sl->setText ( "/" );
    //sl->setShiftText ( "?" );
    connect(sl, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    buttonsList.append(sl);
    sl->res();

    rightShift = new VButton(this);
    rightShift->setKeyCode(62);
    rightShift->move(stx + ((ROWLENGTH4 + 3) *35) + leftShift->width() + 5, sty + (4*35));
    rightShift->resize(68, 30);
    rightShift->setText(i18nc("@action:button", "Shift"));
    rightShift->setCheckable(true);
    connect(rightShift, SIGNAL(toggled(bool)), this, SLOT(resetKeysCap()));
    //connect(leftShift, SIGNAL(toggled(bool)), rightShift, SLOT(setChecked(bool)));
    //connect(rightShift, SIGNAL(toggled(bool)), leftShift, SLOT(setChecked(bool)));
    modKeys.append(rightShift);
    rightShift->res();



    leftCtrl = new VButton(this);
    leftCtrl->resize(45, 30);
    leftCtrl->move(stx, sty + (5*35));
    leftCtrl->setText(i18nc("@action:button", "Ctrl"));
    leftCtrl->setKeyCode(37);
    leftCtrl->setCheckable(true);
    modKeys.append(leftCtrl);
    leftCtrl->res();


    winButton = new VButton(this);
    winButton->resize(45, 30);
    winButton->move(5 + leftCtrl->x() + leftCtrl->width(), sty + (5*35));
    winButton->setText(i18nc("@action:button Windows(Super) key", "Win"));
    //winButton->setKeyCode(115);
    winButton->setKeyCode(133);
    winButton->setCheckable(true);
    modKeys.append(winButton);
    winButton->res();


    leftAlt = new VButton(this);
    leftAlt->resize(45, 30);
    leftAlt->move(5 + winButton->x() + winButton->width(), sty + (5*35));
    leftAlt->setText(i18nc("@action:button", "Alt"));
    leftAlt->setKeyCode(64);
    leftAlt->setCheckable(true);
    modKeys.append(leftAlt);
    leftAlt->res();


    VButton *space = new VButton(this);
    space->setKeyCode(65);
    space->resize(5*35 + 28, 30);
    space->move(5 + leftAlt->x() + leftAlt->width(), sty + (5*35));
    connect(space, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    space->res();
    otherKeys.append(space);

    rightAlt = new VButton(this);
    rightAlt->resize(45, 30);
    rightAlt->move(5 + space->x() + space->width(), sty + (5*35));
    rightAlt->setText(i18nc("@action:button", "Alt Gr"));
    //rightAlt->setKeyCode(113);
    rightAlt->setKeyCode(108);
    rightAlt->setCheckable(true);
    modKeys.append(rightAlt);
    rightAlt->res();


    menuButton = new VButton(this);
    menuButton->resize(45, 30);
    menuButton->move(5 + rightAlt->x() + rightAlt->width(), sty + (5*35));
    menuButton->setText(i18nc("@action:button Menu(Context Menu) key", "Menu"));
    //menuButton->setKeyCode(117);
    menuButton->setKeyCode(135);
    menuButton->setCheckable(false);
    connect(menuButton, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    otherKeys.append(menuButton);
    menuButton->res();


    rightCtrl = new VButton(this);
    rightCtrl->resize(45, 30);
    rightCtrl->move(5 + menuButton->x() + menuButton->width(), sty + (5*35));
    rightCtrl->setText(i18nc("@action:button", "Ctrl"));
    rightCtrl->setKeyCode(105);
    rightCtrl->setCheckable(true);
    modKeys.append(rightCtrl);
    rightCtrl->res();

    mappingNotify(NULL);

    quit = new VButton(this);
    quit->resize(23, 30);
    quit->move(522, 15);
    quit->res();
    otherKeys.append(quit);

    quit->setIcon(KIcon("dialog-close"));
    //QPalette quitColor;
    //quitColor.setColor(quit->backgroundRole(), Qt::red);
    //quit->setPalette(quitColor);

    if (isAlone()) {
        quit->setToolTip(i18nc("@info:tooltip close the application", "Quit"));
        connect(quit, SIGNAL(clicked()), this, SLOT(close()));
    } else {
        quit->setToolTip(i18nc("@info:tooltip minimize in the system tray", "Minimize"));
        connect(quit, SIGNAL(clicked()), this, SLOT(hide()));
    }

    extent = new VButton(this);
    extent->resize(23, 65);
    extent->move(522, 85);
    extent->setIcon(KIcon("arrow-right-double"));
    QSize smallIconSize(22, 22);
    extent->setIconSize(smallIconSize);
    extent->setToolTip(i18nc("@info:tooltip Button for toggling numpad", "Show Numeric Keypad"));
    extent->res();
    otherKeys.append(extent);
    connect(extent, SIGNAL(clicked()) , this, SLOT(toggleNumericPad()));

    modTimer = new QTimer(this);
    connect(modTimer, SIGNAL(timeout()), this, SLOT(queryModState()));
    modTimer->setInterval(500);
    modTimer->start();

    //TODO: make the background color configurable
    QPalette backgroundColor;
    backgroundColor.setColor(backgroundRole(), Qt::black);
    setPalette(backgroundColor);

    setFocusPolicy(Qt::NoFocus);

    int padx = 550;
    QString txt[9] = { i18nc("@action:button Numpad key", "Ho<nl/>me"), QString::fromUtf8("▲"), i18nc("@action:button Page Up key", "Pg<nl/>Up"),
                       QString::fromUtf8("◄"), " ", QString::fromUtf8("►"), i18nc("@action:button", "End"),
                       QString::fromUtf8("▼"), i18nc("@action:button Page Down key", "Pg<nl/>Dn")
                     };
    //FIXME: should this be translatable?
    QString nump[9] = {"7", "8", "9", "4", "5", "6", "1", "2", "3"};
    int val = 0;
    int nval[9] = {79, 80, 81, 83, 84, 85, 87, 88, 89};
    for (int a = 2;a < 5; a++) {
        for (int b = 0;b < 3;b++) {
            NumpadVButton *v = new NumpadVButton(this);
            v->move(padx + (b*35), sty + (a*35));
            v->resize(32, 32);
            v->res();
            v->setKeyCode(nval[val]);
            v->setText(txt[val], nump[val]);
            numLockKeys.append(v);
            connect(v, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
            val++;
        }
    }
    ins = new NumpadVButton(this);
    ins->resize(65, 32);
    ins->move(padx, sty + (5*35));
    ins->res();
    ins->setText(i18nc("@action:button Numpad insert key", "Ins"), "0");
    ins->setKeyCode(90);
    numLockKeys.append(ins);

    connect(ins, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));

    del = new NumpadVButton(this);
    del->resize(32, 32);
    del->move(padx + 70, sty + (5*35));
    del->res();
    del->setText(i18nc("@action:button Numpad delete key", "Del"), ".");
    del->setKeyCode(91);
    numLockKeys.append(del);
    connect(del, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));


    numLock = new VButton(this);
    numLock->setKeyCode(77);
    numLock->move(padx, sty + (1*35));
    numLock->resize(32, 32);
    numLock->res();
    numLock->setText(i18nc("@action:button Numpad numeric lock key", "Num<nl/>Lock"));
    numLock->setCheckable(true);
    otherKeys.append(numLock);
    connect(numLock, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
    connect(numLock, SIGNAL(clicked()), this, SLOT(toggleNumlock()));


    div = new VButton(this);
    div->move(padx + (35), sty + (1*35));
    numLock->resize(32, 32);
    div->res();
    div->setText("/");
    div->setKeyCode(112);
    otherKeys.append(div);
    connect(div, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));

    mul = new VButton(this);
    mul->move(padx + (2*35), sty + (1*35));
    mul->resize(32, 32);
    mul->res();
    mul->setText("*");
    mul->setKeyCode(63);
    otherKeys.append(mul);
    connect(mul, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));

    ent = new VButton(this);
    ent->resize(32, 65);
    ent->move(padx + 70 + 35, sty + (4*35));
    ent->res();
    ent->setText(i18nc("@action:button Numpad enter key", "Ent"));
    ent->setKeyCode(36);
    otherKeys.append(ent);
    connect(ent, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));

    plu = new VButton(this);
    plu->resize(32, 65);
    plu->move(padx + 70 + 35, sty + (2*35));
    plu->res();
    plu->setText("+");
    plu->setKeyCode(86);
    otherKeys.append(plu);
    connect(plu, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));

    min = new VButton(this);
    min->resize(32, 32);
    min->move(padx + 70 + 35, sty + (1*35));
    min->setText("-");
    min->setKeyCode(82);
    otherKeys.append(min);
    min->res();
    connect(min, SIGNAL(keyClick(unsigned int)), this, SLOT(keyPress(unsigned int)));
}

// TODO: find a way to hide the widget before asking for quit
// FIXME: clicking on the tray icon to hide/show the widget only work for showinButtong it
KbdTray::KbdTray(MainWidget* parent) : KSystemTrayIcon(parent)
{
    setObjectName("Kvkbd Tray");
    setIcon(UserIcon("tray"));
    QAction *titleAction = contextMenuTitle();
    titleAction->setText("Kvkbd");
    setContextMenuTitle(titleAction);

    KAction *chooseFontAction = new KAction(KIcon("preferences-desktop-font"), i18nc("@action:inmenu", "Choose Font..."), this);
    KToggleAction *autoResizeAction = new KToggleAction(i18nc("@action:inmenu", "Auto Resize Font"), this);
    KToggleAction *showDockAction = new KToggleAction(i18nc("@action:inmenu", "Dock Keyboard"), this);
    KToggleAction *lockWidgetAction = new KToggleAction(i18nc("@action:inmenu", "Lock on Screen"), this);

    connect(chooseFontAction, SIGNAL(triggered(bool)), parent, SLOT(chooseFont()));

    connect(parent, SIGNAL(autoResizeToggled(bool)), autoResizeAction, SLOT(setChecked(bool)));
    connect(autoResizeAction, SIGNAL(triggered(bool)), parent, SLOT(autoResizeFont(bool)));

    connect(parent->dock, SIGNAL(dockShown(bool)), showDockAction, SLOT(setChecked(bool)));
    connect(showDockAction, SIGNAL(toggled(bool)), parent, SLOT(showDock(bool)));

    connect(parent, SIGNAL(widgetLocked(bool)), lockWidgetAction, SLOT(setChecked(bool)));
    connect(lockWidgetAction, SIGNAL(toggled(bool)), parent, SLOT(lockWidget(bool)));

    KHelpMenu *helpMenu = new KHelpMenu(parent, parent->aboutData);

    contextMenu()->addAction(chooseFontAction);
    contextMenu()->addAction(autoResizeAction);
    contextMenu()->addAction(showDockAction);
    contextMenu()->addAction(lockWidgetAction);

    contextMenu()->addMenu(helpMenu->menu());
}

KbdTray::~KbdTray()
{
}

