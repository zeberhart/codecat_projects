Index: gap-4r8p3/tst/testinstall/morpheus.tst
===================================================================
--- gap-4r8p3.orig/tst/testinstall/morpheus.tst	2016-03-19 22:17:52.000000000 +0100
+++ gap-4r8p3/tst/testinstall/morpheus.tst	2016-03-25 10:11:01.140461604 +0100
@@ -29,7 +29,7 @@
 gap> iso4:=iso3*InnerAutomorphism(g,r^-1);;
 gap> iso4=IdentityMapping(g);
 true
-gap> g:=TransitiveGroup(6,7);;
+gap> g:=Group((1,4)(2,5),(1,3,5)(2,4,6),(1,5)(2,4));;
 gap> IsSolvableGroup(g);
 true
 gap> Size(AutomorphismGroup(g));
Index: gap-4r8p3/tst/testinstall/ctblsolv.tst
===================================================================
--- gap-4r8p3.orig/tst/testinstall/ctblsolv.tst	2016-03-25 10:10:30.000000000 +0100
+++ gap-4r8p3/tst/testinstall/ctblsolv.tst	2016-03-25 11:36:48.366816801 +0100
@@ -8,15 +8,10 @@
 ##  To be listed in testinstall.g
 ##
 gap> START_TEST("ctblsolv.tst");
-gap> CharacterDegrees( SmallGroup( 256, 529 ) );
+gap> F:=FreeGroup(8);;
+gap> G:=F/[ F.1^2*F.4^-1, F.2^-1*F.1^-1*F.2*F.1*F.3^-1, F.3^-1*F.1^-1*F.3*F.1*F.5^-1, F.4^-1*F.1^-1*F.4*F.1, F.5^-1*F.1^-1*F.5*F.1*F.6^-1, F.6^-1*F.1^-1*F.6*F.1*F.7^-1, F.7^-1*F.1^-1*F.7*F.1*F.8^-1, F.8^-1*F.1^-1*F.8*F.1, F.2^2, F.3^-1*F.2^-1*F.3*F.2*F.8^-1*F.5^-1, F.4^-1*F.2^-1*F.4*F.2*F.8^-1, F.5^-1*F.2^-1*F.5*F.2*F.6^-1, F.6^-1*F.2^-1*F.6*F.2*F.7^-1, F.7^-1*F.2^-1*F.7*F.2*F.8^-1, F.8^-1*F.2^-1*F.8*F.2, F.3^2*F.8^-1*F.6^-1*F.5^-1, F.4^-1*F.3^-1*F.4*F.3, F.5^-1*F.3^-1*F.5*F.3, F.6^-1*F.3^-1*F.6*F.3, F.7^-1*F.3^-1*F.7*F.3, F.8^-1*F.3^-1*F.8*F.3, F.4^2*F.8^-1, F.5^-1*F.4^-1*F.5*F.4, F.6^-1*F.4^-1*F.6*F.4, F.7^-1*F.4^-1*F.7*F.4, F.8^-1*F.4^-1*F.8*F.4, F.5^2*F.7^-1*F.6^-1, F.6^-1*F.5^-1*F.6*F.5, F.7^-1*F.5^-1*F.7*F.5, F.8^-1*F.5^-1*F.8*F.5, F.6^2*F.8^-1*F.7^-1, F.7^-1*F.6^-1*F.7*F.6, F.8^-1*F.6^-1*F.8*F.6, F.7^2*F.8^-1, F.8^-1*F.7^-1*F.8*F.7, F.8^2 ];;
+gap> CharacterDegrees( G );
 [ [ 1, 8 ], [ 2, 30 ], [ 4, 8 ] ]
-gap> for pair in [ [ 18, 3 ], [ 27, 3 ], [ 36, 7 ], [ 50, 3 ], [ 54, 4 ] ] do
->      G:= SmallGroup( pair[1], pair[2] );
->      if CharacterDegrees( G, 0 )
->         <> Collected( List( Irr( G ), x -> x[1] ) ) then
->        Error( IdGroup( G ) );
->      fi;
->    od;
 gap> STOP_TEST( "ctblsolv.tst", 3640000);
 
 #############################################################################
Index: gap-4r8p3/tst/testinstall/grppc.tst
===================================================================
--- gap-4r8p3.orig/tst/testinstall/grppc.tst	2016-03-19 22:17:52.000000000 +0100
+++ gap-4r8p3/tst/testinstall/grppc.tst	2016-03-25 10:11:01.144461690 +0100
@@ -57,7 +57,7 @@
 [ 4, 7 ]
 gap> List(sys,i->Length(AsList(i)));
 [ 4, 7 ]
-gap> G := SmallGroup( 144, 183 );;
+gap> G := Group([ (6,7), (2,3), (2,3,4), (5,6,7), (1,2)(3,4), (1,3)(2,4) ]);;
 gap> F := FittingSubgroup( G );;
 gap> S := SylowSubgroup( F, 2 );;
 gap> Length(ComplementClassesRepresentatives( G, S ));
