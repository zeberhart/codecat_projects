# -*- coding: utf-8 -*-

import os, sys, string, subprocess, gc
from time import sleep
from PyQt4 import QtCore, QtGui
from syrthesIHMContext import syrthesIHMContext

class CpWhatsThis(object):
    def CpWhatsThis(self, parent=None):
        
        # What's this dictionnary
        Cpdic_what={
            #calculation_progress
            self.progressBar : "progressBar",#barre de progressions de Syrthes
            #self.Cb_Tep : "117",#combobox du paramètre d'évolution du temps
            self.Cb_Top : "Cb_Top",#combobox du choix du point dont lequle on veut l'évolution de la température
            self.textBrowser_3 : "textBrowser_3",#texte des 100 dernières lignes du fichier de sorties
            self.textEdit_2 : "textEdit_2",#texte éditable de tout le fichier de sortie jusqu'à sa fin à l'instant t
            self.btnResetScale : "btnResetScale",
            self.Screenshot_pb : "Screenshot_pb",
            self.Gnuplot_pb : "Gnuplot_pb",
            self.plot : "plot",
            self.Cb_Vars[0] : "Cb_Vars0",
            self.Cb_Vars[1] : "Cb_Vars1",
            self.Cb_Vars[2] : "Cb_Vars2",
            self.Cb_Vars[3] : "Cb_Vars3",
            self.Cb_Types[0] : "Cb_Types0",
            self.Cb_Types[1] : "Cb_Types1",
            self.Cb_Types[2] : "Cb_Types2",
            self.Cb_Types[3] : "Cb_Types3",
            self.Rb_ButtonGroups[0].button(1) : "yleft0",
            self.Rb_ButtonGroups[0].button(2) : "hide0",
            self.Rb_ButtonGroups[0].button(3) : "yright0",
            self.Rb_ButtonGroups[1].button(1) : "yleft1",
            self.Rb_ButtonGroups[1].button(2) : "hide1",
            self.Rb_ButtonGroups[1].button(3) : "yright1",
            self.Rb_ButtonGroups[2].button(1) : "yleft2",
            self.Rb_ButtonGroups[2].button(2) : "hide2",
            self.Rb_ButtonGroups[2].button(3) : "yright2",
            self.Rb_ButtonGroups[3].button(1) : "yleft3",
            self.Rb_ButtonGroups[3].button(2) : "hide3",
            self.Rb_ButtonGroups[3].button(3) : "yright3",
            self.cbStyle_tab1 : "cbStyle_tab1",
            self.cbStyle_tab2 : "cbStyle_tab2",
            self.cbStyle_tab3 : "cbStyle_tab3",
            self.cbStyle_tab4 : "cbStyle_tab4",
            self.Cb_Top2 : "Cb_Top2", 
            self.Cb_Top3 : "Cb_Top3",
            self.Cb_Top4 : "Cb_Top4"
        }
        
        path = syrthesIHMContext.getExeAbsDirPath()
        for wid in self.widget:
            wid.setWhatsThis(QtGui.QApplication.translate("MainWindow", self.funcReadWhat(Cpdic_what[wid]), None, QtGui.QApplication.UnicodeUTF8))
        pass
    
    def funcReadWhat(self, nomfic):
        path = syrthesIHMContext.getExeAbsDirPath()
        self.ficwhat=open(path + os.sep + "ficwhatsthis" + os.sep + nomfic + ".html", "r")
        self.linewhat=self.ficwhat.readline()
        resultat = ""
        while self.linewhat:
            resultat += self.linewhat
            self.linewhat=self.ficwhat.readline()
        
        resultat = resultat.replace("ficwhatsthis/images/", path + os.sep + "ficwhatsthis" + os.sep + "images" + os.sep)
        return resultat
