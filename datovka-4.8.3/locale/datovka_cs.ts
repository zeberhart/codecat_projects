<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="47"/>
        <source>Version:</source>
        <translation>Verze:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="20"/>
        <source>About Datovka</source>
        <translation>O Datovce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="105"/>
        <source>Copyright ©</source>
        <translation>Copyright ©</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="124"/>
        <source>Licence</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_about.ui" line="131"/>
        <source>Credits</source>
        <translation>Zásluhy</translation>
    </message>
</context>
<context>
    <name>AccountModel</name>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="428"/>
        <source>Accounts</source>
        <translation>Účty</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="242"/>
        <source>Recent Received</source>
        <translation>Nedávno přijaté</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="255"/>
        <source>Recent Sent</source>
        <translation>Nedávno odeslané</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="258"/>
        <source>All</source>
        <translation>Vše</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="261"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../src/models/accounts_model.cpp" line="264"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
</context>
<context>
    <name>AttachmentInteraction</name>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="157"/>
        <source>Error storing attachment.</source>
        <translation>Chyba ukládání přílohy.</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="158"/>
        <source>Cannot write temporary file for attachment &apos;%1&apos;.</source>
        <translation>Nelze zapsat dočasný soubor s přílohou &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="195"/>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="340"/>
        <source>Save attachment</source>
        <translation>Uložit přílohu</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="213"/>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="333"/>
        <source>Error saving attachment.</source>
        <translation>Chyba při ukládání přílohy.</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="214"/>
        <source>Cannot write file &apos;%1&apos;.</source>
        <translation>Nemohu zapsat soubor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="314"/>
        <source>Save attachments</source>
        <translation>Uložit všechny přílohy</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="334"/>
        <source>Some files already exist.</source>
        <translation>Některé soubory již existují.</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="361"/>
        <source>In total %1 attachment files could not be written.</source>
        <translation>Celkem &apos;%1&apos; souborů s přílohami nemohlo být uloženo.</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="363"/>
        <source>These are:</source>
        <translation>Jsou to:</translation>
    </message>
    <message>
        <location filename="../src/model_interaction/attachment_interaction.cpp" line="369"/>
        <source>Error saving attachments.</source>
        <translation>Chyba při ukládání příloh.</translation>
    </message>
</context>
<context>
    <name>BoxContactsModel</name>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="73"/>
        <source>yes</source>
        <translation>ano</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="73"/>
        <source>no</source>
        <translation>ne</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="124"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="125"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="127"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="129"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="131"/>
        <source>Postal Code</source>
        <translation>PSČ</translation>
    </message>
    <message>
        <location filename="../src/models/data_box_contacts_model.cpp" line="133"/>
        <source>PDZ</source>
        <translation>PDZ</translation>
    </message>
</context>
<context>
    <name>ChangeDirectory</name>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="14"/>
        <source>Change data directory for current  account</source>
        <translation>Změnit datové úložiště pro vybraný účet</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="20"/>
        <source>Data for this account is currenly stored in:</source>
        <translation>Data pro tento účet jsou aktuálně uloženy v:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="32"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="39"/>
        <source>Select a new directory where data should be stored:</source>
        <translation>Vyberte nový adresář, kde budou data uložena:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="52"/>
        <source>New data directory</source>
        <translation>Nový adresář pro data</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="75"/>
        <source>Choose</source>
        <translation>Vybrat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="103"/>
        <source>Cannot used the original directory as destination!</source>
        <translation>Nemůžete použít původní adresář jako cílový!</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="119"/>
        <source>Action</source>
        <translation>Akce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="131"/>
        <source>Move data to the new directory</source>
        <translation>Přesunout stávající data do nového adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="147"/>
        <source>Copy data to the new directory</source>
        <translation>Kopírovat stávající data do nového adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_directory.ui" line="160"/>
        <source>Start afresh in the new directory</source>
        <translation>Začít novou databázi v novém adresáři</translation>
    </message>
</context>
<context>
    <name>ChangePwd</name>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="20"/>
        <source>Change password</source>
        <translation>Změnit heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="45"/>
        <source>DataBox ID:</source>
        <translation>ID datové schránky:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="62"/>
        <source>Current password:</source>
        <translation>Staré heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="76"/>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="87"/>
        <source>The password must be at least 8 characters long and
must contain at least one digit and one capital letter.</source>
        <translation>Heslo musí mít nejméně 8 znaků a musí obsahovat
alespoň jednu číslici a jedno velké písmeno.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="80"/>
        <source>New password:</source>
        <translation>Nové heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="30"/>
        <source>This sets a new password on the ISDS server. Please enter your current and new password below:</source>
        <translation>Zde se nastavuje nové heslo na serveru ISDS. Zadejte prosím stávající a nové heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="98"/>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="110"/>
        <source>Repeat the password. The password must be
at least 8 characters long and must contain
at least one digit and one capital letter.</source>
        <translation>Zopakujte heslo. Heslo musí mít nejméně 8
znaků a musí obsahovat alespoň
jednu číslici a jedno velké písmeno.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="103"/>
        <source>Repeat new password:</source>
        <translation>Nové heslo znovu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="144"/>
        <source>Show</source>
        <translation>Ukázat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="151"/>
        <source>Generate</source>
        <translation>Generovat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="160"/>
        <source>Username:</source>
        <translation>Uživatelské jméno:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="184"/>
        <source>Note: Remember your new password.</source>
        <translation>Poznámka: Zapamatujte si Vaše nové heslo.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="200"/>
        <source>Enter SMS code:</source>
        <translation>Zadejte SMS kód:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="207"/>
        <source>Enter SMS or security code</source>
        <translation>Zadejte bezpečnostní SMS kód</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="214"/>
        <source>Send SMS security code.</source>
        <translation>Odeslat bezpečnostní SMS kód.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_change_pwd.ui" line="217"/>
        <source>Send SMS code</source>
        <translation>Odeslat SMS kód</translation>
    </message>
</context>
<context>
    <name>Contacts</name>
    <message>
        <location filename="../src/gui/ui/dlg_contacts.ui" line="14"/>
        <source>Add recipient</source>
        <translation>Přidat příjemce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_contacts.ui" line="24"/>
        <source>Select recipients from the list of current contacts (collected from existing messages):</source>
        <translation>Vyberte příjemce ze seznamu kontaktů (vytvořeno z existujících zpráv):</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_contacts.ui" line="33"/>
        <source>Filter:</source>
        <translation>Filtr:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_contacts.ui" line="47"/>
        <source>Clear</source>
        <translation>Vyčistit</translation>
    </message>
</context>
<context>
    <name>CorrespondenceOverview</name>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="14"/>
        <source>Correspondence overview</source>
        <translation>Přehled korespondence</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="28"/>
        <source>Account:</source>
        <translation>Účet:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="66"/>
        <source>Output format:</source>
        <translation>Výstupní formát:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="91"/>
        <source>Select the dates and types of messages to export.</source>
        <translation>Vyberte časové období a typy zpráv pro export.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="102"/>
        <source>From date:</source>
        <translation>Od data:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="129"/>
        <source>To date:</source>
        <translation>Do data:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="156"/>
        <source>Message type:</source>
        <translation>Typ zprávy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="164"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="198"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="233"/>
        <source>HTML overview:</source>
        <translation>HTML přehled:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="241"/>
        <source>Add tags</source>
        <translation>Přidat tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="248"/>
        <source>Colour tags</source>
        <translation>Barvit tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="260"/>
        <source>Export selected messages also as:</source>
        <translation>Exportovat vybrané zprávy také jako:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="268"/>
        <source>ZFO message files</source>
        <translation>Zprávy ve formátu ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="286"/>
        <source>ZFO acceptance info files</source>
        <translation>Doručenky ve formátu ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="275"/>
        <source>PDF envelope files</source>
        <translation>Obálku zprávy do PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_correspondence_overview.ui" line="293"/>
        <source>PDF acceptance info files</source>
        <translation>Doručenku do PDF</translation>
    </message>
</context>
<context>
    <name>CreateAccount</name>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="26"/>
        <source>Create a new account</source>
        <translation>Vytvořit nový účet</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="36"/>
        <source>Please enter credentials for your Data Box.</source>
        <translation>Prosím, zadejte přihlašovací údaje pro přístup k Vaší datové schránce.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="58"/>
        <source>Custom account name:</source>
        <translation>Vlastní název účtu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="65"/>
        <source>You can give a descriptive name to your account.</source>
        <translation>Svému účtu můžete přiřadit jméno pro snazší identifikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="79"/>
        <source>Username:</source>
        <translation>Uživatelské jméno:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="86"/>
        <source>Enter your username</source>
        <translation>Zadejte své přihlašovací jméno</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="106"/>
        <source>If the credentials you entered are for a test account,
select this option. For normal account
(created at a Czech Point) leave this unchecked.</source>
        <translation>Pokud zadáváte přihlašovací údaje k testovacímu účtu,
vyberte tuto možnost.V případě normálního účtu
(založeného na Czech Pointu) ponechte nastavení odškrtnuté.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="111"/>
        <source>This is a test account</source>
        <translation>Tento účet je testovací</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="118"/>
        <source>Login method:</source>
        <translation>Způsob přihlášení:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="125"/>
        <source>Select authorization method for login into your databox</source>
        <translation>Vyberte metodu přihlašování do datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="139"/>
        <source>Password:</source>
        <translation>Heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="146"/>
        <source>Enter your password</source>
        <translation>Zadejte vaše heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="169"/>
        <source>The password will be saved in a readable form in the 
configuration file of Datovka in your home directory.
Do not use this unless you are pretty sure about
your account security.</source>
        <translation>Heslo bude uložené v čitelné podobě v konfiguračním
souboru ve vašem domovském adresáři. Nepoužívejte
tuto funkci pokud si nejste dostatečně jistí zabezpečením
svého uživatelského účtu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="175"/>
        <source>Remember password</source>
        <translation>Pamatovat si heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="182"/>
        <source>Certificate file:</source>
        <translation>Certifikát:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="198"/>
        <source>Select a certificate</source>
        <translation>Vyberte certifikát pro přihlášení</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="201"/>
        <source>Add</source>
        <translation>Vybrat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="229"/>
        <source>Account will be included into synchronisation
process of all accounts on the background</source>
        <translation>Účet bude zahrnut do hromadné synchronizace
všech účtů na pozadí aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_create_account.ui" line="233"/>
        <source>Synchronise account when &quot;Synchronise all&quot; is activated</source>
        <translation>Synchronizovat účet při aktivaci hromadné synchronizace</translation>
    </message>
</context>
<context>
    <name>CreateAccountFromDb</name>
    <message>
        <location filename="../src/gui/ui/dlg_account_from_db.ui" line="14"/>
        <source>Create account form database file</source>
        <translation>Vytvořit účet z existující databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_account_from_db.ui" line="27"/>
        <source>What do you want to create the account(s) from?</source>
        <translation>Jakým způsobem chce účet vytvořit?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_account_from_db.ui" line="33"/>
        <source>Database file(s) from directory</source>
        <translation>Databázové soubory z adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_account_from_db.ui" line="43"/>
        <source>Selected database file(s)</source>
        <translation>Vybrat databázové soubory</translation>
    </message>
</context>
<context>
    <name>CreateAccountFromDbDialog</name>
    <message>
        <location filename="../src/gui/dlg_account_from_db.cpp" line="32"/>
        <source>A new account will be created according to the name and the content of the database file. This account will operate over the selected database. Should such an account or database file already exist in Datovka then the association will fail. During the association no database file copy is created nor is the content of the database file modified. Nevertheless, we strongly advice you to back-up all important files before associating a database file. In order for the association to succeed you will need an active connection to the ISDS server.</source>
        <translation>Z názvu a obsahu vybraného databázového souboru se v Datovce vytvoří nový účet, který bude pracovat s vybranou databází. Pokud již tento účet či databáze v Datovce existují, nebude se účet znova vytvářet. Při vytváření účtu a jeho přiřazení k databázi se nevytváří žádná kopie databázového souboru ani se nemodifikuje jeho obsah, přesto silně doporučujeme databázový soubor projistotu pře importem zálohovat. Pro úspěšné přiřazení databáze k novému účtu je také zapotřebí aktivní připojení na server Datové schránky.</translation>
    </message>
</context>
<context>
    <name>DbFlsTblModel</name>
    <message>
        <location filename="../src/models/files_model.cpp" line="180"/>
        <source>local database</source>
        <translation>místní databáze</translation>
    </message>
    <message>
        <location filename="../src/models/files_model.cpp" line="355"/>
        <source>File size</source>
        <translation>Velikost souboru</translation>
    </message>
    <message>
        <location filename="../src/models/files_model.cpp" line="357"/>
        <source>File path</source>
        <translation>Cesta k souboru</translation>
    </message>
    <message>
        <location filename="../src/models/files_model.cpp" line="481"/>
        <source>unknown</source>
        <translation>neznámý</translation>
    </message>
</context>
<context>
    <name>DbMsgsTblModel</name>
    <message>
        <location filename="../src/models/messages_model.cpp" line="394"/>
        <location filename="../src/models/messages_model.cpp" line="440"/>
        <source>Attachments downloaded</source>
        <translation>Přílohy staženy</translation>
    </message>
    <message>
        <location filename="../src/models/messages_model.cpp" line="402"/>
        <source>Processing state</source>
        <translation>Stav vyřizování</translation>
    </message>
</context>
<context>
    <name>DlgAbout</name>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="47"/>
        <source>Portable version</source>
        <translation>Přenosná verze</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="51"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="53"/>
        <source>Free client for Czech eGov data boxes.</source>
        <translation>Svobodný klient pro Datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="61"/>
        <source>Additional informations</source>
        <translation>Další informace</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="62"/>
        <source>home page</source>
        <translation>domovská stránka</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="64"/>
        <source>handbook</source>
        <translation>příručka</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="66"/>
        <source>FAQ</source>
        <translation>FAQ</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="67"/>
        <source>Support</source>
        <translation>Podpora</translation>
    </message>
</context>
<context>
    <name>DlgChangeDirectory</name>
    <message>
        <location filename="../src/gui/dlg_change_directory.cpp" line="76"/>
        <source>Open Directory</source>
        <translation>Otevřít adresář</translation>
    </message>
</context>
<context>
    <name>DlgChangePwd</name>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="78"/>
        <source>Enter security code:</source>
        <translation>Zadejte bezpečnostní kód:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="86"/>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="245"/>
        <source>Enter SMS code:</source>
        <translation>Zadejte SMS kód:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="185"/>
        <source>Hide</source>
        <translation>Skrýt</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="190"/>
        <source>Show</source>
        <translation>Ukázat</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="205"/>
        <source>SMS code for account </source>
        <translation>SMS kód pro účet </translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="207"/>
        <source>Account &quot;%1&quot; requires authentication via security code for connection to databox.</source>
        <translation>Účet &quot;%1&quot; vyžaduje pro přihlášení k datové schránce autorizaci pomocí bezpečnostního kódu OTP.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="211"/>
        <source>Security code will be sent you via Premium SMS.</source>
        <translation>Bezpečnostní kód Vám bude zaslán přes Prémiovou SMS.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="213"/>
        <source>Do you want to send Premium SMS with security code into your mobile phone?</source>
        <translation>Chcete poslat Prémiovou SMS s bezpečnostním kódem do Vašeho mobilu?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="235"/>
        <source>Enter SMS security code</source>
        <translation>Zadejte SMS bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="236"/>
        <source>SMS security code for account &quot;%1&quot;&lt;br/&gt;has been sent on your mobile phone...</source>
        <translation>SMS bezpečnostní kód pro účet &quot;%1&quot;&lt;br/&gt;byl zaslán do Vašeho mobilu...</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="240"/>
        <source>Enter SMS security code for account</source>
        <translation>Zadejte SMS bezpečnostní kód pro účet</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="248"/>
        <source>Login error</source>
        <translation>Chyba přihlášení</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="249"/>
        <source>An error occurred while preparing request for SMS with OTP security code.</source>
        <translation>Došlo k chybě v průběhu přípravy požadavku pro přihlášení pomocí SMS OTP.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="252"/>
        <source>Please try again later or you have to use the official web interface of Datové schránky for access to your data box.</source>
        <translation>Zkuste to později nebo použijte oficiální webové rozhraní pro Datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="294"/>
        <source>Password has been changed</source>
        <translation>Heslo bylo změněno</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="295"/>
        <source>Password has been changed successfully on the server ISDS.</source>
        <translation>Heslo bylo na ISDS serveru uspěšně změněno.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="298"/>
        <source>Restart the application. Also don&apos;t forget to remember the new password so you will still be able to log into your data box via the web interface.</source>
        <translation>Restartujte aplikaci. Nezapomeňte si také zapamatovat nové heslo, abyste je byli schopni použít pro přihlašování přes webové rozhraní.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="311"/>
        <source>Error: </source>
        <translation>Chyba: </translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="313"/>
        <source>ISDS returns: </source>
        <translation>ISDS vrací:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="317"/>
        <source>An error occurred while password was changed.</source>
        <translation>Nastala chyba v průběhu změny hesla.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="319"/>
        <source>You have to fix the problem and try to again.</source>
        <translation>Napravte chybu a zkuste to znovu.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_change_pwd.cpp" line="316"/>
        <source>Password error</source>
        <translation>Chybné heslo</translation>
    </message>
</context>
<context>
    <name>DlgCorrespondenceOverview</name>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="141"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="144"/>
        <source>messages: </source>
        <translation>zpráv: </translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="420"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="430"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="391"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="402"/>
        <source>Correspondence overview</source>
        <translation>Přehled korespondence</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="405"/>
        <source>From date:</source>
        <translation>Od data:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="409"/>
        <source>To date:</source>
        <translation>Do data:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="413"/>
        <source>Generated:</source>
        <translation>Vygenerováno:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="327"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="328"/>
        <source>Message type</source>
        <translation>Typ zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="329"/>
        <source>Delivery time</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="330"/>
        <source>Acceptance time</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="288"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="331"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="292"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="332"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="333"/>
        <source>Sender Address</source>
        <translation>Adresa odesílatele</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="296"/>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="334"/>
        <source>Recipient</source>
        <translation>Adresát</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="278"/>
        <source>Delivery</source>
        <translation>Dodání</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="283"/>
        <source>Acceptance</source>
        <translation>Doručení</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="335"/>
        <source>Recipient Address</source>
        <translation>Adresa příjemce</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="336"/>
        <source>Our file mark</source>
        <translation>Naše spisová značka</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="337"/>
        <source>Our reference number</source>
        <translation>Naše číslo jednací</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="338"/>
        <source>Your file mark</source>
        <translation>Vaše spisová značka</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="339"/>
        <source>Your reference number</source>
        <translation>Vaše číslo jednací</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="461"/>
        <source>Select file to save correspondence overview</source>
        <translation>Vybrat soubor pro uložení přehledu korespondence</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="477"/>
        <source>Correspondence Overview Export Error</source>
        <translation>Chyba při exportu přehledu korespondence</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="485"/>
        <source>correspondence overview file was exported to HTML.</source>
        <translation>soubor s přehledem korespondence byl exportován do HTML.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="486"/>
        <source>correspondence overview file was exported to CSV.</source>
        <translation>soubor s přehledem korespondence byl exportován do CSV.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="490"/>
        <source>correspondence overview file was exported.</source>
        <translation>soubor s přehledem korespondence byl exportován.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="610"/>
        <source>Select directory for export of ZFO/PDF file(s)</source>
        <translation>Vyberte adresář pro export ZFO/PDF souborů</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="617"/>
        <source>messages were successfully exported to ZFO/PDF.</source>
        <translation>zpráv bylo úspěšně exportováno do ZFO/PDF.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="643"/>
        <source>messages were successfully exported to ZFO.</source>
        <translation>zpráv bylo úspěšně exportováno do ZFO.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="664"/>
        <source>acceptance infos were successfully exported to ZFO.</source>
        <translation>doručenek bylo exportováno do ZFO.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="685"/>
        <source>message envelopes were successfully exported to PDF.</source>
        <translation>obálek zpráv bylo úspěšně exportováno do PDF.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="706"/>
        <source>acceptance infos were successfully exported to PDF.</source>
        <translation>doručenek bylo exportováno do PDF.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="713"/>
        <source>Export results</source>
        <translation>Výsledky exportu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="715"/>
        <source>Export of correspondence overview finished with these results:</source>
        <translation>Export přehledu korespondence skončil s tímto výsledkem:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="719"/>
        <source>Some errors occurred during export.</source>
        <translation>Během exportu se vyskytly nějaké chyby.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="721"/>
        <source>See detail for more info...</source>
        <translation>Více informací v podrobnostech...</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="451"/>
        <source>Overview</source>
        <translation>Prehled</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="462"/>
        <source>Files</source>
        <translation>Soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="478"/>
        <source>Correspondence overview file &apos;%1&apos; could not be written.</source>
        <translation>Soubor s přehledem korespondence &apos;%1&apos; nemohl být zapsán.</translation>
    </message>
</context>
<context>
    <name>DlgCreateAccount</name>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="154"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="155"/>
        <source>Certificate</source>
        <translation>Certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="156"/>
        <source>Certificate + Password</source>
        <translation>Certifikát + heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="157"/>
        <source>Password + Secure code</source>
        <translation>Heslo + Bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="158"/>
        <source>Password + Secure SMS</source>
        <translation>Heslo + SMS kód</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="188"/>
        <source>Update account %1</source>
        <translation>Aktualizovat účet %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="192"/>
        <source>Enter password for account %1</source>
        <translation>Zadejte heslo pro účet %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="200"/>
        <source>Set certificate for account %1</source>
        <translation>Nastavit certifikát pro účet %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="208"/>
        <source>Enter password/certificate for account %1</source>
        <translation>Zadejte heslo/certifikát pro účet %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="243"/>
        <source>mojeID</source>
        <translation>mojeID</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="133"/>
        <source>Open Certificate</source>
        <translation>Otevřít certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_create_account.cpp" line="134"/>
        <source>Certificate Files (*.p12 *.pem)</source>
        <translation>Soubory certifikátů (*.p12 *.pem)</translation>
    </message>
</context>
<context>
    <name>DlgDsSearch</name>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="68"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="85"/>
        <source>All</source>
        <translation>Vše</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="68"/>
        <source>All types</source>
        <translation>Všechny</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="71"/>
        <source>OVM</source>
        <translation>OVM</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="71"/>
        <source>Orgán veřejné moci</source>
        <translation>Orgán veřejné moci</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="74"/>
        <source>PO</source>
        <translation>PO</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="74"/>
        <source>Právnická osoba</source>
        <translation>Právnická osoba</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="77"/>
        <source>PFO</source>
        <translation>PFO</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="77"/>
        <source>Podnikající fyzická osoba</source>
        <translation>Podnikající fyzická osoba</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="80"/>
        <source>FO</source>
        <translation>FO</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="80"/>
        <source>Fyzická osoba</source>
        <translation>Fyzická osoba</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="85"/>
        <source>Search in all fields</source>
        <translation>Hledat ve všech položkách</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="88"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="88"/>
        <source>Search in address data</source>
        <translation>Hledat podle adresy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="91"/>
        <source>IC</source>
        <translation>IČ</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="91"/>
        <source>Identification number</source>
        <translation>Identifikační číslo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="94"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="94"/>
        <source>Box identifier</source>
        <translation>ID datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="261"/>
        <source>Full-text data box search. Enter phrase for finding and set optional restrictions:</source>
        <translation>Fulltextové vyhledávání datových schránek. Zadejte vyhledávaný výraz a zvolte případná omezení:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="277"/>
        <source>Enter the ID, IČ or at least three letters from the name of the data box you look for:</source>
        <translation>Zadejte ID, IČ nebo alespoň tři písmena z názvu hledané datové schránky:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="313"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="325"/>
        <source>Your account is of type</source>
        <translation>Váš účet je typu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="315"/>
        <source>You have also Post Data Messages activated.
This means you can only search for accounts of type OVM and accounts that have Post Data Messages delivery activated.
Because of this limitation the results of your current search might not contain all otherwise matching databoxes.</source>
        <translation>Máte také aktivovány poštovní datové zprávy.
To znamená, že můžete vyhledávat pouze schránky typu OVM a schránky, 
které mají aktivováno doručování poštovních datových zpráv.
Vzhledem k tomuto omezení nemusí výsledky tohoto hledání obsahovat 
všechny datové schránky, které by jinak vyhovovaly parametrům.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="323"/>
        <source>commercial messages are enabled</source>
        <translation>poštovní datové zprávy jsou povoleny</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="327"/>
        <source>This means you can only search for accounts of type OVM.
The current search settings will thus probably yield no result.</source>
        <translation>To znamená, že můžete vyhledávat pouze schránky typu OVM.
Vyhledávání s aktuálními parametry tak pravděpodobně nevrátí žádné výsledky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="331"/>
        <source>commercial messages are disabled</source>
        <translation>poštovní datové zprávy jsou vypnuty</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="381"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="382"/>
        <source>Enter last name of the PFO or company name.</source>
        <translation>Zadejte příjmení PFO nebo název firmy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="388"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="389"/>
        <source>Enter last name or last name at birth of the FO.</source>
        <translation>Zadejte příjmení nebo rodné příjmení FO.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="525"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="588"/>
        <source>Total found</source>
        <translation>Celkem nalezeno</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="553"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="616"/>
        <source>It was not possible find any data box because</source>
        <translation>Nebylo možné najít žádnou datovou schránku protože</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="561"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="624"/>
        <source>It was not possible find any data box because an error occurred during the search process!</source>
        <translation>Nebylo možné najít žádnou datovou schránku, protože během procesu vyhledávání došlo k chybě!</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="634"/>
        <source>Displayed</source>
        <translation>Zobrazeno</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="366"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="373"/>
        <source>Subject Name:</source>
        <translation>Název subjektu:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="367"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="368"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="374"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="375"/>
        <source>Enter name of subject</source>
        <translation>Zadejte název instituce</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="380"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="387"/>
        <source>Last Name:</source>
        <translation>Příjmení:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="444"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="547"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="552"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="610"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="615"/>
        <source>Search result</source>
        <translation>Výsledek hledání</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="445"/>
        <source>This is a special ID for system databox of Datové schránky. You can&apos;t use this ID for message delivery. Try again.</source>
        <translation>Toto je speciální ID &quot;systémové schránky&quot; systému Datových schránek. Momentálně není možné zasílat zprávy do této schránky. Hledejte dál.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_ds_search.cpp" line="560"/>
        <location filename="../src/gui/dlg_ds_search.cpp" line="623"/>
        <source>Search error</source>
        <translation>Chyba při vyhledávání</translation>
    </message>
</context>
<context>
    <name>DlgDsSearchMojeId</name>
    <message>
        <location filename="../src/gui/dlg_search_mojeid.cpp" line="120"/>
        <source>Search</source>
        <translation>Vyhledat</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_search_mojeid.cpp" line="175"/>
        <source>Search next</source>
        <translation>Vyhledat další</translation>
    </message>
</context>
<context>
    <name>DlgLoginToMojeId</name>
    <message>
        <location filename="../src/gui/dlg_login_mojeid.cpp" line="51"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_login_mojeid.cpp" line="52"/>
        <source>Certificate</source>
        <translation>Certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_login_mojeid.cpp" line="53"/>
        <source>Password + Secure code</source>
        <translation>Heslo + Bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_login_mojeid.cpp" line="74"/>
        <source>Login to account: %1</source>
        <translation>Přihlásit se do účtu: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_login_mojeid.cpp" line="90"/>
        <source>Open Certificate</source>
        <translation>Otevřít certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_login_mojeid.cpp" line="91"/>
        <source>Certificate Files (*.p12 *.pem)</source>
        <translation>Soubory certifikátů (*.p12 *.pem)</translation>
    </message>
</context>
<context>
    <name>DlgMsgSearch</name>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="65"/>
        <source>Here it is possible to search for messages according to supplied criteria. You can search for messages in selected account or in all accounts. Double clicking on a found message will change focus to the selected message in the application window. Note: You can view additional information when hovering your mouse cursor over the message ID.</source>
        <translation>Zde je možné vyhledávat zprávy podle zvolených kritérií. Je možné vyhledávat zprávy v aktivním účtu nebo ve všech účtech. Dvojklik na nalezenou zprávu změní fokus na zvolenou zprávu v hlavním okně. Poznámka: Dodatečné informace je možné zobrazit najetím kurzoru myši nad identifikátor zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="89"/>
        <source>Account</source>
        <translation>Účet</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="90"/>
        <source>Message ID</source>
        <translation>ID zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="91"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="92"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="93"/>
        <source>Recipient</source>
        <translation>Příjemce</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="94"/>
        <source>Delivery Year</source>
        <translation>Rok dodání</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_msg_search.cpp" line="95"/>
        <source>Message Type</source>
        <translation>Typ zprávy</translation>
    </message>
</context>
<context>
    <name>DlgPreferences</name>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="59"/>
        <source>Note: If you have a slow network connection or you cannot download complete messages, here you can increase the connection timeout. Default value is %1 minutes. Use 0 to disable timeout limit (not recommended).</source>
        <translation>Poznámka: Pokud máte pomalé síťové připojení, nebo se Vám nedaří stáhnout komplentí zprávu, můžete zvýšit časový limit spojení. Výchozí hodnota je %1 minut. Použijte 0 pro vypnutí limitu (není doporučeno).</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="67"/>
        <source>Note: Marked unread message will be marked as read after set interval. Default value is %1 seconds. Use -1 disable the function.</source>
        <translation>Poznámka: Nepřečtená zpráva bude označena jako lokálně přečtená za nastavený čas. Výchozí hodnota je %1 sekund. Použijte -1 pro vytnutí této funkce.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_preferences.cpp" line="220"/>
        <location filename="../src/gui/dlg_preferences.cpp" line="232"/>
        <source>Select directory</source>
        <translation>Vybrat adresář pro import</translation>
    </message>
</context>
<context>
    <name>DlgProxysets</name>
    <message>
        <location filename="../src/gui/dlg_proxysets.cpp" line="53"/>
        <location filename="../src/gui/dlg_proxysets.cpp" line="74"/>
        <source>Proxy has been detected</source>
        <translation>Proxy byla detekována</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_proxysets.cpp" line="48"/>
        <location filename="../src/gui/dlg_proxysets.cpp" line="69"/>
        <source>No proxy detected, direct connection will be used.</source>
        <translation>Nebyla detekována žádná proxy, použije se přímé spojení.</translation>
    </message>
</context>
<context>
    <name>DlgSendMessage</name>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="449"/>
        <source>sending of PDZ: enabled</source>
        <translation>posílání PDZ: povoleno</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="450"/>
        <source>remaining credit: </source>
        <translation>zbývající kredit: </translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="453"/>
        <source>sending of PDZ: disabled</source>
        <translation>posílání PDZ: zakázáno</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="284"/>
        <source>Our reference number:</source>
        <translation>Naše číslo jednací:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="291"/>
        <source>Enter reference number:</source>
        <translation>Zadejte Vaše referenční číslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1050"/>
        <source>Warning: The permitted amount (%1) of attachments has been exceeded.</source>
        <translation>Upozornění: Povolený počet (%1) příloh byl překročen.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1060"/>
        <source>Total size of attachments is ~%1 KB</source>
        <translation>Celková velikost příloh je ~%1 KB</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1066"/>
        <source>Warning: Total size of attachments is larger than %1 MB!</source>
        <translation>Upozornění: Celková velikost příloh přesahuje %1 MB!</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1072"/>
        <source>Total size of attachments is ~%1 B</source>
        <translation>Celková velikost příloh je ~%1 B</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1014"/>
        <source>Message contains non-OVM recipients.</source>
        <translation>Zpráva obsahuje více ne-OVM příjemců.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="200"/>
        <source>Wrong data box ID</source>
        <translation>Špatné ID datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="201"/>
        <source>Wrong data box ID &apos;%1&apos;!</source>
        <translation>Špatné ID datové schránky &apos;%1&apos;!</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="520"/>
        <source>Message was successfully sent to &lt;i&gt;%1 (%2)&lt;/i&gt; as PDZ with number &lt;i&gt;%3&lt;/i&gt;.</source>
        <translation>Zpráva byla úspěšně odeslána do &lt;i&gt;%1 (%2)&lt;/i&gt; jako PDZ s číslem &lt;i&gt;%3&lt;/i&gt;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="528"/>
        <source>Message was successfully sent to &lt;i&gt;%1 (%2)&lt;/i&gt; as message number &lt;i&gt;%3&lt;/i&gt;.</source>
        <translation>Zpráva byla úspěšně odeslána do &lt;i&gt;%1 (%2)&lt;/i&gt; jako zpráva s číslem &lt;i&gt;%3&lt;/i&gt;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="537"/>
        <source>Message was NOT sent to &lt;i&gt;%1 (%2)&lt;/i&gt;. Server says: %3</source>
        <translation>Zpráva nebyla odeslána do &lt;i&gt;%1 (%2)&lt;/i&gt;. Server vrací: %3</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="551"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="591"/>
        <source>Message was successfully sent to all recipients.</source>
        <translation>Zpráva byla úspěšně odeslána všem příjemcům.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="563"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="607"/>
        <source>Message was NOT sent to all recipients.</source>
        <translation>Zpráva nebyla odeslána všem příjemcům.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1015"/>
        <source>Your message contains %1 non-OVM recipients therefore this message will be sent as a commercial messages (PDZ) for these recipients.</source>
        <translation>Vaše zpráva obsahuje %1 ne-OVM příjemců, proto bude zpráva těmto příjemcům odeslaná v režimu placená poštovní datová zpráva (PDZ).</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1022"/>
        <source>Message contains non-OVM recipient.</source>
        <translation>Zpráva obsahuje ne-OVM příjemce.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1023"/>
        <source>Your message contains non-OVM recipient therefore this message will be sent as a commercial message (PDZ) for this recipient.</source>
        <translation>Vaše zpráva obsahuje jednoho ne-OVM příjemce, proto bude zpráva tomuto příjemci odeslaná v režimu placená poštovní datová zpráva (PDZ).</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1030"/>
        <source>Your remaining credit is </source>
        <translation>Váš zbývající kredit je </translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1165"/>
        <source>Wrong Recipient</source>
        <translation>Chybný příjemce</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1175"/>
        <source>Recipient Search Failed</source>
        <translation>Chyba vyhledávání příjemce</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1176"/>
        <source>Information about recipient data box could not be obtained.</source>
        <translation>Nebylo možné získat informace o datové schránce příjemce.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1178"/>
        <source>Do you still want to add the box &apos;%1&apos; into the recipient list?</source>
        <translation>Chcete stále přidat schránku &apos;%1&apos; do seznamu příjemců?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1179"/>
        <source>Enable commercial messages (PDZ).</source>
        <translation>Zapnout poštovní datové zprávy (PDZ).</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1181"/>
        <source>Obtained ISDS error</source>
        <translation>Obdržená chyba ISDS</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1449"/>
        <source>It has not been possible to send a message to the ISDS server.</source>
        <translation>Nebylo možné odeslat zprávu na server ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="549"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="589"/>
        <source>Message sent</source>
        <translation>Zpráva odeslána</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="561"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="605"/>
        <source>Message sending error</source>
        <translation>Chyba odesílání zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="565"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="609"/>
        <source>Do you want to close the Send message form?</source>
        <translation>Chcete zavřít formulář pro odesílání zpráv a zahodit vyplněné údaje?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1448"/>
        <source>Send message error</source>
        <translation>Chyba při odesílání zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="761"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="1077"/>
        <source>Total size of attachments is %1 B</source>
        <translation>Celková velikost příloh je %1 B</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1113"/>
        <location filename="../src/gui/dlg_send_message.cpp" line="1114"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1128"/>
        <source>Data box is not active</source>
        <translation>Datová schránka není aktivní</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1129"/>
        <source>Recipient with data box ID &apos;%1&apos; does not have active data box.</source>
        <translation>Příjemce s datovou schránkou &apos;%1&apos; nemá tuto schránku aktivní.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1133"/>
        <source>The message cannot be delivered.</source>
        <translation>Zprávu nelze dodat.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1148"/>
        <source>Cannot send to data box</source>
        <translation>Nelze odeslat do datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1149"/>
        <source>Cannot send message to recipient with data box ID &apos;%1&apos;.</source>
        <translation>Nelze odeslat zprávu příjemci s datovou schránkou &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1152"/>
        <source>You won&apos;t be able as user &apos;%1&apos; to send messages into data box &apos;%2&apos;.</source>
        <translation>Jako uživatel &apos;%1&apos; nebudete moct odeslat zprávu do schránky &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1166"/>
        <source>Recipient with data box ID &apos;%1&apos; does not exist.</source>
        <translation>Příjemce s datovou schránkou &apos;%1&apos; neexistuje.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1402"/>
        <source>An error occurred while loading attachments into message.</source>
        <translation>Nastala chyba v průběhu vkládání příloh do zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1406"/>
        <source>An error occurred during message envelope creation.</source>
        <translation>Nastala chyba v průběhu vytváření obálky zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1450"/>
        <source>The message will be discarded.</source>
        <translation>Zpráva bude zahozena.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="189"/>
        <source>Databox ID</source>
        <translation>ID datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="190"/>
        <source>Enter Databox ID (7 characters):</source>
        <translation>Vložte ID datové schránky (7 znaků):</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1020"/>
        <source>Do you want to send all messages?</source>
        <translation>Chcete opravdu odeslat tuto zprávu všem příjemcům?</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_send_message.cpp" line="1027"/>
        <source>Do you want to send message?</source>
        <translation>Chcete opravdu odeslat zprávu tomuto příjemci?</translation>
    </message>
</context>
<context>
    <name>DlgTag</name>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="71"/>
        <source>Choose tag colour</source>
        <translation>Vyberte barvu tagu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="89"/>
        <location filename="../src/gui/dlg_tag.cpp" line="137"/>
        <location filename="../src/gui/dlg_tag.cpp" line="147"/>
        <source>Tag error</source>
        <translation>Chyba tagu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="90"/>
        <source>Tag name is empty.</source>
        <translation>Jméno tagu je prázdné.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="91"/>
        <source>Tag wasn&apos;t created.</source>
        <translation>Tag nebyl vytvořen.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="109"/>
        <source>Tag update error</source>
        <translation>Chyba aktualizace tagu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="110"/>
        <source>Tag with name &apos;%1&apos;&apos; wasn&apos;t updated in the WebDatovka database.</source>
        <translation>Tag pojmenovaný &apos;%1&apos; nebyl aktualizován v databázi WebDatovky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="127"/>
        <source>Tag insert error</source>
        <translation>Chyba vložení tagu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="128"/>
        <source>Tag with name &apos;%1&apos;&apos; wasn&apos;t&apos; created in WebDatovka database.</source>
        <translation>Tag pojmenovaný &apos;%1&apos; nebyl vytvořen v databázi WebDatovky.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="138"/>
        <location filename="../src/gui/dlg_tag.cpp" line="148"/>
        <source>Tag with name &apos;%1&apos;&apos; already exists in database.</source>
        <translation>Tag se jménem &apos;%1&apos; již v databázi existuje.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_tag.cpp" line="141"/>
        <location filename="../src/gui/dlg_tag.cpp" line="151"/>
        <source>Tag wasn&apos;t created again.</source>
        <translation>Tag nebyl znovu vytvořen.</translation>
    </message>
</context>
<context>
    <name>DlgTimestampExpir</name>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="20"/>
        <source>Time stamp expiration checking</source>
        <translation>Kontrola expirace časového razítka</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="26"/>
        <source>What do you want to do?</source>
        <translation>Co chcete udělat?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="32"/>
        <source>Check for expiring time stamps in current account</source>
        <translation>Zkontrolovat expiraci časových razítek v aktuálním účtu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="42"/>
        <source>Check for expiring time stamps in all accounts</source>
        <translation>Zkontrolovat expiraci časových razítek u všech účtů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="54"/>
        <source>Note: Checking in all accounts can be slow. The action cannot be aborted.</source>
        <translation>Poznámka: Kontrola přes všechny účty může být pomalá. Akci nebude možné přerušit.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="71"/>
        <source>Check for expiring time stamps in ZFO files in selected directory</source>
        <translation>Zkontrolovat expiraci časových razítek u ZFO souborů ve vybraném adresáři</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="86"/>
        <source>Include subdirectories</source>
        <translation>Zahrnout podadresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_timestamp_expir.ui" line="116"/>
        <source>Note: Checking many files can be slow. The action cannot be aborted.</source>
        <translation>Poznámka: Kontrola velkého počtu ZFO souborů může být pomalá. Akci nebude možné přerušit.</translation>
    </message>
</context>
<context>
    <name>DlgViewZfo</name>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="60"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="85"/>
        <source>Error parsing content</source>
        <translation>Chyba načítání obsahu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="61"/>
        <source>Cannot parse the content of file &apos;%1&apos;.</source>
        <translation>Nelze načíst obsah souboru &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="86"/>
        <source>Cannot parse the content of message.</source>
        <translation>Nelze načíst obsah zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="114"/>
        <source>Open attachment</source>
        <translation>Otevřít přílohu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="118"/>
        <source>Save attachment</source>
        <translation>Uložit přílohu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="122"/>
        <source>Save attachments</source>
        <translation>Uložit všechny přílohy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="355"/>
        <source>Identification</source>
        <translation>Identifikace</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="357"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="358"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="359"/>
        <source>Message type</source>
        <translation>Typ zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="364"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="365"/>
        <source>Sender Databox ID</source>
        <translation>ID datové schránky odesílatele</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="366"/>
        <source>Sender Address</source>
        <translation>Adresa odesílatele</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="371"/>
        <source>Recipient</source>
        <translation>Příjemce</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="372"/>
        <source>Recipient Databox ID</source>
        <translation>ID datové schránky příjemce</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="326"/>
        <source>Events</source>
        <translation>Události</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="373"/>
        <source>Recipient Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="376"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="396"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="378"/>
        <source>Delivery time</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="383"/>
        <source>Acceptance time</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="298"/>
        <source>Attachments</source>
        <translation>Přílohy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="404"/>
        <source>Signature</source>
        <translation>Podpis</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="414"/>
        <source>Message signature</source>
        <translation>Podpis zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="426"/>
        <source>Signing certificate</source>
        <translation>Podepisující certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="438"/>
        <source>Time stamp</source>
        <translation>Časové razítko</translation>
    </message>
</context>
<context>
    <name>DsSearch</name>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="14"/>
        <source>Search recipient</source>
        <translation>Vyhledat příjemce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="26"/>
        <source>Current account:</source>
        <translation>Aktuální účet:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="33"/>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="71"/>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="215"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="64"/>
        <source>Use full-text search similar to the ISDS client portal.</source>
        <translation>Použít fulltextové vyhledávání podobné vyhledávání na portálu ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="86"/>
        <source>Your account is not of type OVM (i.e. non-OVM). Sending of post data messages
from your account is activated. This means that you can only search for accounts
of the type OVM and accounts that have the the receiving of post data messages
activated. Because of this limitation the results of your current search may not
contain all otherwise matching databoxes.</source>
        <translation>Váš účet není typu OVM (tedy ne-OVM). Zasílání poštovních datových zpráv z Vašeho
účtu je aktivní. To znamená, že můžete vyhledávat pouze datové schránky typu OVM
nebo schránky, které mají aktivován příjem poštovních datových zpráv. Díky tomuto
omezení nemusí výsledek tohoto vyhledávání obsahovat všechny jinak odpovídající
datové zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="93"/>
        <source>Note: Your search results will be limited. See tooltip for more information.</source>
        <translation>Poznámka: Výsledky vyhledávání budou omezeny. Více najdete v tooltipu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="113"/>
        <source>Databox type:</source>
        <translation>Typ datové schránky:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="123"/>
        <source>Search in fileds:</source>
        <translation>Vyhledávat v položkách:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="140"/>
        <source>Text:</source>
        <translation>Text:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="150"/>
        <source>ID:</source>
        <translation>ID schránky:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="160"/>
        <source>IČ:</source>
        <translation>IČ:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="170"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="180"/>
        <source>Postal code:</source>
        <translation>Směrovací číslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_ds_search.ui" line="195"/>
        <source>Search</source>
        <translation>Vyhledat</translation>
    </message>
</context>
<context>
    <name>DsSearchMojeId</name>
    <message>
        <location filename="../src/gui/ui/dlg_search_mojeid.ui" line="14"/>
        <source>Search recipient</source>
        <translation>Vyhledat příjemce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_search_mojeid.ui" line="26"/>
        <source>Current account:</source>
        <translation>Aktuální účet:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_search_mojeid.ui" line="33"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_search_mojeid.ui" line="67"/>
        <source>Keyword:</source>
        <translation>Klíčové slovo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_search_mojeid.ui" line="82"/>
        <source>Search</source>
        <translation>Vyhledat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_search_mojeid.ui" line="136"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_search_mojeid.ui" line="141"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_search_mojeid.ui" line="146"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
</context>
<context>
    <name>ImportZFO</name>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="20"/>
        <source>ZFO import</source>
        <translation>Import ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="59"/>
        <source>What do you want to import?</source>
        <translation>Co chcete importovat?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="65"/>
        <source>Message and acceptance info ZFO file(s)</source>
        <translation>Zprávy a doručenky současně</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="75"/>
        <source>Only message ZFO file(s)</source>
        <translation>Pouze soubory zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="85"/>
        <source>Only acceptance info ZFO file(s)</source>
        <translation>Pouze doručenky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="95"/>
        <source>How do you want to import?</source>
        <translation>Jakým způsobem chcete importovat?</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="111"/>
        <source>Import all ZFO files from selected directory</source>
        <translation>Importovat všechny ZFO ze zvoleného adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="126"/>
        <source>Include subdirectory to the import</source>
        <translation>Zahrnout podadresáře do importu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="154"/>
        <source>Import options</source>
        <translation>Možnosti importu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="162"/>
        <source>Sending imported data to ISDS server to check them
is significantly slower but safer.
By disabling this option you may introduce invalid data
into your local database.</source>
        <translation>Odesílání importovaných dat serveru ISDS za účelem
jejich ověření je značně pomalejší, ale bezpečnější.
Vypnutím této volby můžete do své lokální databáze
zanést neplatná data.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="168"/>
        <source>Check imported ZFO files on server</source>
        <translation>Ověřit importované ZFO soubory na serveru</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo.ui" line="101"/>
        <source>Import selected ZFO file(s)</source>
        <translation>Importovat vybrané ZFO soubory</translation>
    </message>
</context>
<context>
    <name>ImportZFODialog</name>
    <message>
        <location filename="../src/gui/dlg_import_zfo.cpp" line="35"/>
        <source>Here you can import whole messages and message acceptance information from ZFO files into local database. The message or acceptance information import will succeed only for those files whose validity can be approved by the Datové schránky server (working connection to server is required). Acceptance information ZFO will be inserted into local database only if a corresponding complete message already exists in the database.</source>
        <translation>Zde můžete importovat celé zprávy a doručenky ze ZFO souborů do místní databáze. Import zprávy nebo doručenky proběhne úspěšně jen pro ty soubory, jejichž správnost se podaří ověřit na serveru Datové schránky (je vyžadováno funkční spojení se serverem). ZFO doručenky jde vložit do databáze pouze tehdy, pokud již v databázi existuje odpovídající zpráva.</translation>
    </message>
</context>
<context>
    <name>ImportZFOResult</name>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="14"/>
        <source>Import ZFO result</source>
        <translation>Výsledek importu ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="22"/>
        <source>Number of ZFO files for import to database(s):</source>
        <translation>Počet ZFO souborů k importování do databází:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="53"/>
        <source>Number of new imported files:</source>
        <translation>Počet nově importovaných souborů:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="75"/>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="123"/>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="164"/>
        <source>Details:</source>
        <translation>Detaily:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="101"/>
        <source>Number of existing files:</source>
        <translation>Počet již existujícíh v databázi:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="114"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_import_zfo_result.ui" line="142"/>
        <source>Number of unsuccessfully imported files:</source>
        <translation>Počet neúspěšně importovaných souborů:</translation>
    </message>
</context>
<context>
    <name>IsdsLogin</name>
    <message>
        <location filename="../src/io/isds_login.cpp" line="105"/>
        <source>Error when connecting to ISDS server!</source>
        <translation>Chyba během připojování k serveru ISDS!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="111"/>
        <source>Error during authentication!</source>
        <translation>Chyba během autentizace!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="113"/>
        <location filename="../src/io/isds_login.cpp" line="129"/>
        <source>It was not possible to connect to your data box from account &quot;%1&quot;.</source>
        <translation>Nebylo možné se připojit k datové schránce z účtu &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="115"/>
        <source>Authentication failed!</source>
        <translation>Chyba autentizace!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="117"/>
        <location filename="../src/io/isds_login.cpp" line="133"/>
        <location filename="../src/io/isds_login.cpp" line="147"/>
        <location filename="../src/io/isds_login.cpp" line="164"/>
        <location filename="../src/io/isds_login.cpp" line="178"/>
        <location filename="../src/io/isds_login.cpp" line="192"/>
        <location filename="../src/io/isds_login.cpp" line="208"/>
        <location filename="../src/io/isds_login.cpp" line="221"/>
        <location filename="../src/io/isds_login.cpp" line="235"/>
        <location filename="../src/io/isds_login.cpp" line="251"/>
        <source>Error: </source>
        <translation>Chyba: </translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="119"/>
        <source>Please check your credentials and login method together with your password.</source>
        <translation>Prosím, zkontrolujte své přihlašovací údaje, metodu přihlášení či heslo.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="121"/>
        <location filename="../src/io/isds_login.cpp" line="137"/>
        <source>It is also possible that your password has expired - in this case, you need to use the official ISDS web interface to change it.</source>
        <translation>Také je možné, že vypršela platnost Vašeho hesla - v tomto případě se musíte přihlásit na stránky ISDS a heslo si změnit.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="127"/>
        <source>Error during OTP authentication!</source>
        <translation>Chyba během OTP autentizace!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="131"/>
        <source>OTP authentication failed!</source>
        <translation>OTP autentizace selhala!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="135"/>
        <source>Please check your credentials together with entered security/SMS code and try again.</source>
        <translation>Prosím, zkontrolujte své přihlašovací údaje, metodu přihlášení a zadaný bezpečnostní/SMS kód a zkuste to znovu.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="143"/>
        <source>It was not possible to establish a connection within a set time.</source>
        <translation>Nebylo možné vytvořit spojení ve stanoveném čase.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="145"/>
        <source>Time-out for connection to server expired!</source>
        <translation>Vypršel čas vymezený pro připojení k serveru!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="149"/>
        <source>This is either caused by an extremely slow and/or unstable connection or by an improper set-up.</source>
        <translation>Tento problém je obvykle způsoben velmi pomalým a/nebo nestabilním spojením či nesprávným nastavením sítě.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="151"/>
        <location filename="../src/io/isds_login.cpp" line="196"/>
        <source>Please check your internet connection and try again.</source>
        <translation>Prosím, zkontrolujte Vaše připojení k internetu a zkuste akci opakovat.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="153"/>
        <source>It might be necessary to use a proxy to connect to the server. It is also possible that the ISDS server is inoperative or busy. Try again later.</source>
        <translation>Je možné, že pro spojení se serverem je třeba použít proxy. Také je možné, že ISDS server je mimo provoz, nebo je zaneprázdněný. Zkuste to později.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="160"/>
        <location filename="../src/io/isds_login.cpp" line="174"/>
        <location filename="../src/io/isds_login.cpp" line="188"/>
        <location filename="../src/io/isds_login.cpp" line="204"/>
        <location filename="../src/io/isds_login.cpp" line="217"/>
        <location filename="../src/io/isds_login.cpp" line="231"/>
        <source>It was not possible to establish a connection between your computer and the ISDS server.</source>
        <translation>Nebylo možné navázat spojení mezi Vaším počítačem a serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="162"/>
        <source>HTTPS problem occurred or redirection to server failed!</source>
        <translation>Vyskytl se problém s HTTPS nebo selhalo přesměrování na server!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="166"/>
        <location filename="../src/io/isds_login.cpp" line="180"/>
        <source>This is usually caused by either lack of internet connectivity or by some problem with the ISDS server.</source>
        <translation>Je to obvykle způsobeno výpadkem internetového připojení nebo nějakým problémem na serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="168"/>
        <location filename="../src/io/isds_login.cpp" line="182"/>
        <source>It is possible that the ISDS server is inoperative or busy. Try again later.</source>
        <translation>Pravděpodobně je server ISDS mimo provoz nebo momentálně zaneprázdněný. Zkuste to později.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="176"/>
        <source>An ISDS server problem occurred or service was not found!</source>
        <translation>Vyskytl se problém serveru ISDS nebo požadována služba nebyla nalezena!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="190"/>
        <source>The connection to server failed or a problem with the network occurred!</source>
        <translation>Připojení k serveru selhalo nebo se vyskytl problém se síťovým spojením!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="194"/>
        <source>This is usually caused by either lack of internet connectivity or by a firewall on the way.</source>
        <translation>Tento problém je obvykle způsoben neexistencí aktivního internetového připojení nebo přítomností firewallu.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="198"/>
        <source>It might be necessary to use a proxy to connect to the server. If yes, please set it up in the proxy settings menu.</source>
        <translation>Je možné, že pro spojení se serverem je třeba použít proxy. Pokud ano, nastavte ji prosím v menu nastavení proxy.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="206"/>
        <source>Problem with HTTPS connection!</source>
        <translation>Problém s HTTPS spojením!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="210"/>
        <source>This may be caused by a missing certificate for the SSL communication or the application cannot open an SSL socket.</source>
        <translation>Pravděpodobně chybí potřebný certifikát pro HTTPS komunikaci se serverem nebo se nepodařilo otevřít SSL soket.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="223"/>
        <source>This may be caused by a missing SSL certificate needed for communication with the server or it was not possible to establish a secure connection with the ISDS server.</source>
        <translation>Toto může být způsobeno chybějícím SSL certifikátem, který je potřeba pro komunikaci se serverem, nebo nebylo možné ustavit bezpečný komunikační kanál se serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="237"/>
        <source>This may be caused by an error in SOAP or the XML content for this web service is invalid.</source>
        <translation>Toto může být způsobeno chybou požadavku SOAP nebo obsah XML pro tuto službu není validní.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="212"/>
        <source>It is also possible that some libraries (e.g. CURL, SSL) may be missing or may be incorrectly configured.</source>
        <translation>Je také možné, že nebyly nalezeny některé potřebné knihovny (např. CURL, SSL) anebo jsou nesprávně nakonfigurovány.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="219"/>
        <source>HTTPS problem or security problem!</source>
        <translation>Problém s HTTPS spojením či bezpečnostní problém!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="225"/>
        <source>It is also possible that the certificate has expired.</source>
        <translation>Dále je možné, že vypršela platnost certifikátu.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="233"/>
        <source>SOAP problem or XML problem!</source>
        <translation>Problém SOAP či XML!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="239"/>
        <source>It is also possible that the ISDS server is inoperative or busy. Try again later.</source>
        <translation>Je také možné, že server ISDS je momentálně mimo provoz nebo přetížen. Zkuste to později.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="245"/>
        <location filename="../src/io/isds_login.cpp" line="249"/>
        <source>Datovka internal error!</source>
        <translation>Interní chyba Datovky!</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="247"/>
        <source>It was not possible to establish a connection to the ISDS server.</source>
        <translation>Nebylo možné navázat spojení se serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/io/isds_login.cpp" line="253"/>
        <source>An unexpected error occurred. Please restart the application and try again. It this doesn&apos;t help then you should contact the support for this application.</source>
        <translation>Nastala neočekávaná chyba v aplikaci. Prosím, restartujte aplikaci a zkuste to znova. Kontaktujte technickou podporu aplikace, pokud se problém opakuje.</translation>
    </message>
</context>
<context>
    <name>JsonLayer</name>
    <message>
        <location filename="../src/web/json.cpp" line="141"/>
        <source>Login to mojeID failed. You must choose correct login method and enter correct login data. Try again.</source>
        <translation>Přihlášení do mojeID selhalo. Musíte vybrat správný způsob přihlašování a zadat správné přihlašovací údaje. Zkuste to znovu.</translation>
    </message>
    <message>
        <location filename="../src/web/json.cpp" line="177"/>
        <source>Cannot open client certificate from path &apos;%1&apos;</source>
        <translation>Nelze otevřít certifikát z cesty &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/web/json.cpp" line="183"/>
        <source>Certificate password</source>
        <translation>Heslo certifikátu</translation>
    </message>
    <message>
        <location filename="../src/web/json.cpp" line="184"/>
        <source>Enter certificate password:</source>
        <translation>Zadejte heslo k certifikátu:</translation>
    </message>
    <message>
        <location filename="../src/web/json.cpp" line="208"/>
        <source>Cannot parse client certificate from path &apos;%1&apos;</source>
        <translation>Nemohu načíst certifikát z cesty &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/web/json.cpp" line="308"/>
        <location filename="../src/web/json.cpp" line="335"/>
        <location filename="../src/web/json.cpp" line="371"/>
        <location filename="../src/web/json.cpp" line="405"/>
        <location filename="../src/web/json.cpp" line="455"/>
        <location filename="../src/web/json.cpp" line="488"/>
        <location filename="../src/web/json.cpp" line="525"/>
        <location filename="../src/web/json.cpp" line="546"/>
        <location filename="../src/web/json.cpp" line="567"/>
        <location filename="../src/web/json.cpp" line="595"/>
        <location filename="../src/web/json.cpp" line="632"/>
        <location filename="../src/web/json.cpp" line="670"/>
        <location filename="../src/web/json.cpp" line="707"/>
        <location filename="../src/web/json.cpp" line="745"/>
        <location filename="../src/web/json.cpp" line="783"/>
        <location filename="../src/web/json.cpp" line="822"/>
        <location filename="../src/web/json.cpp" line="857"/>
        <location filename="../src/web/json.cpp" line="990"/>
        <location filename="../src/web/json.cpp" line="1029"/>
        <source>User is not logged to mojeID</source>
        <translation>Uživatel není přihlášen do mojeID</translation>
    </message>
    <message>
        <location filename="../src/web/json.cpp" line="473"/>
        <location filename="../src/web/json.cpp" line="503"/>
        <location filename="../src/web/json.cpp" line="580"/>
        <location filename="../src/web/json.cpp" line="611"/>
        <location filename="../src/web/json.cpp" line="649"/>
        <location filename="../src/web/json.cpp" line="685"/>
        <location filename="../src/web/json.cpp" line="723"/>
        <location filename="../src/web/json.cpp" line="761"/>
        <location filename="../src/web/json.cpp" line="798"/>
        <location filename="../src/web/json.cpp" line="839"/>
        <location filename="../src/web/json.cpp" line="916"/>
        <location filename="../src/web/json.cpp" line="938"/>
        <location filename="../src/web/json.cpp" line="960"/>
        <location filename="../src/web/json.cpp" line="1007"/>
        <location filename="../src/web/json.cpp" line="1046"/>
        <source>Reply content missing</source>
        <translation>Chybí obsah odpovědi</translation>
    </message>
</context>
<context>
    <name>LoginToMojeId</name>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="26"/>
        <source>Add a new mojeID account(s)</source>
        <translation>Přidejte účet mojeID</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="48"/>
        <source>Please select login method and enter credentials for mojeID.</source>
        <translation>Vyberte způsob přihlašování a zadejte přihlašovací údaje do mojeID.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="85"/>
        <source>Login method:</source>
        <translation>Způsob přihlášení:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="92"/>
        <source>Select authorization method for login into your databox</source>
        <translation>Vyberte metodu přihlašování do datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="99"/>
        <source>Username:</source>
        <translation>Uživatelské jméno:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="106"/>
        <source>Enter your username</source>
        <translation>Zadejte své přihlašovací jméno</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="113"/>
        <source>Password:</source>
        <translation>Heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="120"/>
        <source>Enter your password</source>
        <translation>Zadejte své heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="137"/>
        <source>Certificate file:</source>
        <translation>Soubor certifikátu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="146"/>
        <source>Select a certificate</source>
        <translation>Vyberte certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="149"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="185"/>
        <source>Security code (OTP):</source>
        <translation>Bezpečnostní kód (OTP):</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="201"/>
        <source>Account will be included into synchronisation
process of all accounts on the background</source>
        <translation>Účet bude zahrnut do hromadné synchronizace
všech účtů na pozadí aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_login_mojeid.ui" line="205"/>
        <source>Synchronise account(s) when &quot;Synchronise all&quot; is activated</source>
        <translation>Synchronizovat účet při aktivaci hromadné synchronizace</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8345"/>
        <source>Search: </source>
        <translation>Vyhledat: </translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8361"/>
        <source>Clear search field</source>
        <translation>Vymazat pole</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8290"/>
        <source>Mode: offline</source>
        <translation>Režim: nepřipojeno (offline)</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="764"/>
        <source>All messages</source>
        <translation>Všechny zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="517"/>
        <source>Add new account</source>
        <translation>Vytvořit nový účet</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3386"/>
        <source>Test account</source>
        <translation>Testovací účet</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3388"/>
        <source>Standard account</source>
        <translation>Standardní účet</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3394"/>
        <source>Account name</source>
        <translation>Název účtu</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3418"/>
        <source>User name</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3509"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3509"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3543"/>
        <source>Password expiration date</source>
        <translation>Datum a čas expirace hesla</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3583"/>
        <location filename="../src/gui/datovka.cpp" line="3626"/>
        <source>Received messages</source>
        <translation>Přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3586"/>
        <location filename="../src/gui/datovka.cpp" line="3600"/>
        <location filename="../src/gui/datovka.cpp" line="3632"/>
        <source>none</source>
        <translation>žádné</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3597"/>
        <location filename="../src/gui/datovka.cpp" line="3628"/>
        <source>Sent messages</source>
        <translation>Odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3658"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3661"/>
        <source>Powered by</source>
        <translation>Vytvořil</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3734"/>
        <source>Datovka: Database file present</source>
        <translation>Datovka: Databázový soubor přítomen</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3735"/>
        <source>Database file for account &apos;%1&apos; already exists.</source>
        <translation>Databázový soubor pro účet &apos;%1&apos; již existuje.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3741"/>
        <source>If you want to use a new blank file then delete, rename or move the existing file so that the application can create a new empty file.</source>
        <translation>Pokud chcete použít nový prázdný soubor, pak odstraňte, přejmenujte nebo přesuňte stávající soubor, aby aplikace mohla vytvořit nový prázdný soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3765"/>
        <location filename="../src/gui/datovka.cpp" line="3802"/>
        <location filename="../src/gui/datovka.cpp" line="3832"/>
        <location filename="../src/gui/datovka.cpp" line="3874"/>
        <location filename="../src/gui/datovka.cpp" line="3910"/>
        <source>Datovka: Problem loading database</source>
        <translation>Datovka: Problém otevírání databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3766"/>
        <location filename="../src/gui/datovka.cpp" line="3803"/>
        <location filename="../src/gui/datovka.cpp" line="3833"/>
        <location filename="../src/gui/datovka.cpp" line="3875"/>
        <location filename="../src/gui/datovka.cpp" line="3911"/>
        <location filename="../src/gui/datovka.cpp" line="3951"/>
        <source>Could not load data from the database for account &apos;%1&apos;</source>
        <translation>Nemohu načíst data z databázového souboru pro účet &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3772"/>
        <source>I&apos;ll try to create an empty one.</source>
        <translation>Pokusím se vytvořit prázdný.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3654"/>
        <location filename="../src/gui/datovka.cpp" line="8267"/>
        <source>Datovka - Free client for Datové schránky</source>
        <translation>Datovka - Svobodný klient pro Datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3190"/>
        <source>No account synchronised.</source>
        <translation>Nebyl synchronizován žádný účet.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3404"/>
        <source>Account and user information could not be acquired.</source>
        <translation>Nepodařilo se získat informace o účtu a uživateli.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3410"/>
        <source>User information</source>
        <translation>Informace o uživateli</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3413"/>
        <source>Databox information</source>
        <translation>Informace o datové schránce</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3738"/>
        <source>The existing database files %1 in &apos;%2&apos; are going to be used.</source>
        <translation>Existující databázové soubory %1 v &apos;%2&apos; budou použity.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3769"/>
        <source>Database files are missing in &apos;%1&apos;.</source>
        <translation>Databázové soubory chybí v &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3806"/>
        <source>Some databases of %1 in &apos;%2&apos; are not a file.</source>
        <translation>Některé databáze %1 v &apos;%2&apos; asi nejsou soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3836"/>
        <source>Some databases of &apos;%1&apos; in &apos;%2&apos; cannot be accessed.</source>
        <translation>Některé databáze %1 v &apos;%2&apos; nelze zpřístupnit.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3839"/>
        <source>You don&apos;t have enough access rights to use the file.</source>
        <translation>Nemáte dostatečná oprávnění k použití souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3881"/>
        <source>The file either does not contain an sqlite database or the file is corrupted.</source>
        <translation>Soubor buď neobsahuje slite databázi nebo je poškozen.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3950"/>
        <source>Datovka: Database opening error</source>
        <translation>Datovka: Chyba otevírání databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5001"/>
        <source>Remove account </source>
        <translation>Odstranit účet </translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5002"/>
        <source>Do you want to remove account</source>
        <translation>Chcete opravdu odstranit účet</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5004"/>
        <source>Delete also message database from storage</source>
        <translation>Odstranit také databázi zpráv z disku</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5005"/>
        <source>Warning: If you delete the message database then all locally accessible messages that are not stored on the ISDS server will be lost.</source>
        <translation>Upozornění: Jestli-že smažete databázi zpráv potom všechny zprávy, které nejsou uloženy na serveru ISDS budou ztraceny.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5025"/>
        <source>Account &apos;%1&apos; was deleted together with message database file.</source>
        <translation>Účet &apos;%1&apos; byl smazán společně s jeho databází zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5034"/>
        <source>Account &apos;%1&apos; was deleted.</source>
        <translation>Účet &apos;%1&apos; byl smazán.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5239"/>
        <location filename="../src/gui/datovka.cpp" line="5245"/>
        <location filename="../src/gui/datovka.cpp" line="5264"/>
        <location filename="../src/gui/datovka.cpp" line="5270"/>
        <location filename="../src/gui/datovka.cpp" line="5289"/>
        <location filename="../src/gui/datovka.cpp" line="5296"/>
        <source>Change data directory for current account</source>
        <translation>Změnit adresář pro ukládání dat pro aktuální účet</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7421"/>
        <source>Connection to ISDS or user authentication failed!</source>
        <translation>Připojení k serveru datové schránky se nezdařilo nebo selhala autorizace uživatele!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7423"/>
        <source>Please check your internet connection and try again or it is possible that your password (certificate) has expired - in this case, you need to use the official web interface of Datové schránky to change it.</source>
        <translation>Zkontrolujte prosím připojení k internetu a zkuste to znovu. Také je možné, že vaše heslo (certifikát) již vypršelo - v tomto případě musíte použít oficiální webové rozhraní Datové schránky a heslo si změnit.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5937"/>
        <location filename="../src/gui/datovka.cpp" line="6134"/>
        <source>Add ZFO file</source>
        <translation>Vybrat ZFO soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5937"/>
        <location filename="../src/gui/datovka.cpp" line="6135"/>
        <location filename="../src/gui/datovka.cpp" line="6273"/>
        <source>ZFO file (*.zfo)</source>
        <translation>Soubor ZFO (*.zfo)</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5977"/>
        <source>Message is authentic</source>
        <translation>Zpráva je autentická</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5988"/>
        <source>Message is not authentic</source>
        <translation>Zpráva není autentická</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="453"/>
        <location filename="../src/gui/datovka.cpp" line="454"/>
        <source>disk</source>
        <translation>disk</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="457"/>
        <location filename="../src/gui/datovka.cpp" line="461"/>
        <source>memory</source>
        <translation>paměť</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="464"/>
        <source>Storage:</source>
        <translation>Úložiště:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="526"/>
        <source>New version of Datovka is available:</source>
        <translation>Nová verze Datovky je k dispozici:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="530"/>
        <location filename="../src/gui/datovka.cpp" line="546"/>
        <source>New version of Datovka</source>
        <translation>Nová verze Datovky</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="531"/>
        <location filename="../src/gui/datovka.cpp" line="547"/>
        <source>New version of Datovka is available.</source>
        <translation>Nová verze Datovky je k dispozici.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="532"/>
        <source>Current version is %1</source>
        <translation>Vaše současná verze je %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="535"/>
        <source>New version is %1</source>
        <translation>Nová verze je %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="537"/>
        <source>Do you want to download new version?</source>
        <translation>Chcete nyní stáhnout novou verzi?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="548"/>
        <source>Current version is &quot;%1&quot;</source>
        <translation>Vaše verze je &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="551"/>
        <source>New version is &quot;%1&quot;</source>
        <translation>Nová verze je &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="553"/>
        <source>Update your application...</source>
        <translation>Aktualizujte aplikaci...</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="715"/>
        <source>Database files for account &apos;%1&apos; cannot be accessed in location &apos;%2&apos;.</source>
        <translation>Databázové soubory pro účet &apos;%1&apos; nelze zpřístupnit ve složce &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="774"/>
        <source>All received messages</source>
        <translation>Všechny přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="790"/>
        <source>All sent messages</source>
        <translation>Všechny odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="970"/>
        <location filename="../src/gui/datovka.cpp" line="984"/>
        <location filename="../src/gui/datovka.cpp" line="998"/>
        <location filename="../src/gui/datovka.cpp" line="1322"/>
        <source>As Unsettled</source>
        <translation>Nevyřízeno</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="972"/>
        <location filename="../src/gui/datovka.cpp" line="986"/>
        <location filename="../src/gui/datovka.cpp" line="1000"/>
        <location filename="../src/gui/datovka.cpp" line="1324"/>
        <source>As in Progress</source>
        <translation>Vyřizuje se</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="974"/>
        <location filename="../src/gui/datovka.cpp" line="988"/>
        <location filename="../src/gui/datovka.cpp" line="1002"/>
        <location filename="../src/gui/datovka.cpp" line="1326"/>
        <source>As Settled</source>
        <translation>Vyřízeno</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1942"/>
        <location filename="../src/gui/datovka.cpp" line="6515"/>
        <source>Select target folder to save</source>
        <translation>Vyberte cílovou složku pro uložení</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1962"/>
        <source>Error saving of attachments</source>
        <translation>Chyba při ukládání příloh</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1963"/>
        <source>Some attachments of message &apos;%1&apos; were not saved to target folder!</source>
        <translation>Některé přílohy zprávy &apos;%1&apos; nebyly uloženy do cílové složky!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6449"/>
        <source>First you must download the complete message to continue with the action.</source>
        <translation>Před pokračováním akce musíte nejdříve stáhnout úplnou zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7008"/>
        <source>Invalid certificate data</source>
        <translation>Neplatná data certifikátu</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7009"/>
        <source>The certificate or the supplied pass-phrase are invalid.</source>
        <translation>Certifikát nebo zadané heslo jsou neplatné.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7011"/>
        <source>Please enter a path to a valid certificate and/or provide a correct key to unlock the certificate.</source>
        <translation>Prosím zadejte cestu k platnému certifikátu a/nebo zadejte správný klíč pro odemknutí certifikátu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7014"/>
        <source>Bad certificate data for account &quot;%1&quot;.</source>
        <translation>Špatná data certifikátu pro účet &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7032"/>
        <source>The log-in method used in account &quot;%1&quot; is not implemented.</source>
        <translation>Přihlašovací metoda použitá v účtu &quot;%1&quot; není implementována.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7139"/>
        <source>Account &quot;%1&quot; requires authentication via OTP&lt;br/&gt;security code for connection to data box.</source>
        <translation>Pro připojení ke schránce pro účet &quot;%1&quot; je&lt;br/&gt;vyžadována OTP autentizace bezpečnostním kódem.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7169"/>
        <source>Account &quot;%1&quot; requires authentication via security code for connection to data box.</source>
        <translation>Účet &quot;%1&quot; vyžaduje pro přihlášení k datové schránce autorizaci bezpečnostním kódem OTP.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7172"/>
        <source>Security code will be sent to you via a Premium SMS.</source>
        <translation>Bezpečnostní kód Vám bude zaslán Prémiovou SMS zprávou.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7174"/>
        <source>Do you want to send a Premium SMS with a security code into your mobile phone?</source>
        <translation>Chcete zaslat Prémiovou SMS s bezpečnostním kódem do Vašeho mobilu?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8049"/>
        <source>Select target folder for export</source>
        <translation>Vyberte cílový adresář pro export</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8204"/>
        <source>Database file error</source>
        <translation>Chyba databazového souboru</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8237"/>
        <source>Split of message database for account &apos;%1&apos; was not successfully. Please, restart the application for loading original database.</source>
        <translation>Rozdělení databáze zpráv pro účet &apos;%1&apos; nebylo úspěšné. Prosím, restartujte aplikaci pro načtení původní databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8247"/>
        <source>Database split result</source>
        <translation>Výsledek rozdělení databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8354"/>
        <source>Enter sought expression</source>
        <translation>Zadete hledaný výraz</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9143"/>
        <source>Import of messages to account %1 finished</source>
        <translation>Import zpráv na účet %1 skončil</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8310"/>
        <source>In Progress</source>
        <translation>Vyřizuje se</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="713"/>
        <source>Database access error</source>
        <translation>Chyba přístupu k databázi</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="718"/>
        <source>The file cannot be accessed or is corrupted. Please fix the access privileges or remove or rename the file so that the application can create a new empty file.</source>
        <translation>Nelze přistoupik k souboru nebo je soubor poškozen. Opravte prosím přístupová oprávnění nebo odstraňte nebo přejmenujte soubor, aby aplikace mohla vytvořit nový prázdný soubor.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="723"/>
        <source>Create a backup copy of the affected file. This will help when trying to perform data recovery.</source>
        <translation>Vytvořte si kopii postiženého souboru. Může to pomoct při případném obnovování dat.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="726"/>
        <source>In general, it is recommended to create backup copies of the database files to prevent data loss.</source>
        <translation>Všeobecně je doporučováno vyrábět záložní kopie databázových souborů aby se zamezilo ztrátě dat.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="959"/>
        <location filename="../src/gui/datovka.cpp" line="1313"/>
        <source>Mark</source>
        <translation>Označit</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="964"/>
        <location filename="../src/gui/datovka.cpp" line="978"/>
        <location filename="../src/gui/datovka.cpp" line="992"/>
        <location filename="../src/gui/datovka.cpp" line="1316"/>
        <source>As Read</source>
        <translation>Přečtené</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="966"/>
        <location filename="../src/gui/datovka.cpp" line="980"/>
        <location filename="../src/gui/datovka.cpp" line="994"/>
        <location filename="../src/gui/datovka.cpp" line="1318"/>
        <source>As Unread</source>
        <translation>Nepřečtené</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6858"/>
        <location filename="../src/gui/datovka.cpp" line="6944"/>
        <source>Cannot write file &apos;%1&apos;.</source>
        <translation>Nemohu uložit soubor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2133"/>
        <source>Download message list error</source>
        <translation>Chyba stahování seznamu zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2052"/>
        <source>ISDS: </source>
        <translation>ISDS: </translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2049"/>
        <source>It was not possible to download a complete message &quot;%1&quot; from server Datové schránky.</source>
        <translation>Nebylo možné stáhnout kompletní zprávu &quot;%1&quot; ze serveru Datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2844"/>
        <location filename="../src/gui/datovka.cpp" line="2933"/>
        <source>Delete message %1</source>
        <translation>Mazání zprávy %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2845"/>
        <location filename="../src/gui/datovka.cpp" line="2934"/>
        <source>Do you want to delete message &apos;%1&apos;?</source>
        <translation>Chcete smazat zprávu &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2847"/>
        <source>Delete this message also from server ISDS</source>
        <translation>Smazat tuto zprávu také z ISDS</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2848"/>
        <source>Warning: If you delete the message from ISDS then this message will be lost forever.</source>
        <translation>Upozornění: Jestli-že smažete zprávu ze serveru ISDS, bude tato zpráva navždy ztracena.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2851"/>
        <location filename="../src/gui/datovka.cpp" line="2939"/>
        <source>Delete messages</source>
        <translation>Mazaní zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2852"/>
        <location filename="../src/gui/datovka.cpp" line="2940"/>
        <source>Do you want to delete selected messages?</source>
        <translation>Chcete smazat vybrané zprávy?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2854"/>
        <source>Delete these messages also from server ISDS</source>
        <translation>Smazat tyto zprávy také z ISDS</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6295"/>
        <source>No ZFO files to import.</source>
        <translation>Žádné ZFO soubory k importování.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6301"/>
        <source>There is no account to import of ZFO files into.</source>
        <translation>Není účet, do kterého lze importovat ZFO soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6445"/>
        <source>Complete message &apos;%1&apos; is missing.</source>
        <translation>Chybí úplná zpráva &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6451"/>
        <source>Do you want to download the complete message now?</source>
        <translation>Chcete nyní stáhnout úplnou zprávu?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6459"/>
        <source>Complete message &apos;%1&apos; has been downloaded.</source>
        <translation>Úplná zpráva &apos;%1&apos; byla stažena.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6464"/>
        <source>Complete message &apos;%1&apos; has not been downloaded.</source>
        <translation>Úplná zpráva &apos;%1&apos; nebyla stažena.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6565"/>
        <source>Data message</source>
        <translation>Datová zpráva</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6565"/>
        <source>Data messages</source>
        <translation>Datové zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6660"/>
        <source>Attachments of message</source>
        <translation>Přílohy zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6660"/>
        <source>Attachments of messages</source>
        <translation>Přílohy zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6761"/>
        <source>Attachment of message %1</source>
        <translation>Příloha zprávy %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6761"/>
        <source>Attachments of message %1</source>
        <translation>Přílohy zprávy %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7346"/>
        <source>Datovka is currently processing some tasks.</source>
        <translation>Datovka právě zpracovává úlohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7347"/>
        <source>Do you want to abort pending actions and close Datovka?</source>
        <translation>Přejete si zrušit nedokončené akce a zavřít Datovku?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7458"/>
        <location filename="../src/gui/datovka.cpp" line="7462"/>
        <source>Adding new account failed</source>
        <translation>Přidávání nového účtu selhalo</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7459"/>
        <source>Account could not be added because an error occurred.</source>
        <translation>Účet nemohl být přidán, protože nastala chyba.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7463"/>
        <source>Account could not be added because account already exists.</source>
        <translation>Účet nemohl být přidán, protože účet již existuje.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8102"/>
        <source>Import of mesages from database</source>
        <translation>Import zpráv z databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8103"/>
        <source>This action allow to import messages from selected database files into current account. Keep in mind that this action may takes a few minutes based on number of messages in the imported database. Import progress will be displayed in the status bar.</source>
        <translation>Tato akce umožňuje importovat zprávy z vybraných databázových souborů do aktuálního účtu. Mějte na paměti, že tato akce může trvat i několik minut v závislosti na počtu zpráv v importované databázi. Průběh importu zpráv se bude zobrazovat ve stavovém panelu aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8108"/>
        <location filename="../src/gui/datovka.cpp" line="8164"/>
        <location filename="../src/gui/datovka.cpp" line="8631"/>
        <source>Do you want to continue?</source>
        <translation>Chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8117"/>
        <source>Select database file(s)</source>
        <translation>Vyberte databázové soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8118"/>
        <source>DB file (*.db)</source>
        <translation>DB soubor (*.db)</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9148"/>
        <source>Messages import result</source>
        <translation>Výsledek importu zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9149"/>
        <source>Import of messages into account &apos;%1&apos; finished with result:</source>
        <translation>Import zpráv do účtu &apos;%1&apos; skončil s tímto výsledkem:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9154"/>
        <source>Imported messages: %1</source>
        <translation>Importováno zpráv: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9156"/>
        <source>Non-imported messages: %1</source>
        <translation>Ignorováno zpráv: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8151"/>
        <source>Database split</source>
        <translation>Rozdělení databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8152"/>
        <source>This action split current account message database into several new databases which will contain messages relevant by year only. It is recommended for large database because the performance of application will be better.</source>
        <translation>Tato akce umožňuje rozdělit stávající databázi zpráv do několika menších databází podle roku dodání zpráv. Akce je doporučena spíše pro účty s velkou databází, která obsahuje mnoho zpráv. Rozdělení databáze může vést ke zrychlení aplikace i rychlejšímu přístupu k datům jednotlivých zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8156"/>
        <source>Original database file will copy to selected directory and new database files will created in the same location. If action finished with success, new databases will be used instead of original. Restart of application is required.</source>
        <translation>Aktuální databáze zpráv bude zkopírována do vybrané složky. Nové databázové soubory budou vytvořeny do stejné složky. Jestli-že bude proces rozdělení úspěšný, původní databáze bude nehrazena novými databázemi. Poté je třeba provést restart aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8162"/>
        <source>Note: Keep in mind that this action may takes a few minutes based on number of messages in the database.</source>
        <translation>Poznámka: Mějte na paměti, že tato akce může trvat i několik minut v závislosti na počtu zpráv v databázi.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8201"/>
        <source>Split of message database finished with error</source>
        <translation>Rozdělení databáze skončilo s chybou</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8587"/>
        <source>Vacuum cannot be performed on databases in memory.</source>
        <translation>Vakuum nelze aplikovat na databáze v paměti.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8591"/>
        <source>Database operation error</source>
        <translation>Chyba funkce databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8592"/>
        <source>Database clean-up cannot be performed on database in memory.</source>
        <translation>Pročištění databáze nelze provést s databází v paměti.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8593"/>
        <source>Cannot call VACUUM on database in memory.</source>
        <translation>Nelze volat VACUUM na databázi v paměti.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8626"/>
        <source>Clean message database</source>
        <translation>Pročisti databázi zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8627"/>
        <source>Performs a message database clean-up for the selected account. This action will block the entire application. The action may take several minutes to be completed. Furthermore, it requires more than %1 of free disk space to successfully proceed.</source>
        <translation>Provede pročištění databáze zpráv ve zvoleném účtu. Tato akce zablokuje celou aplikaci. Dokončení akce může trvat několik minut. Akce navíc k úspěšnému dokončení vyžaduje více jak %1 volného místa na disku.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8639"/>
        <source>Performing database clean-up.</source>
        <translation>Provádím pročišťování databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8648"/>
        <source>Database clean-up finished.</source>
        <translation>Počištění databáze dokončeno.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8653"/>
        <source>Database clean-up successful</source>
        <translation>Pročištění úspěšně dokončeno</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8654"/>
        <source>The database clean-up has finished successfully.</source>
        <translation>Pročištění databáze bylo úspěšně dokončeno.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8658"/>
        <source>Database clean-up failure</source>
        <translation>Pročištění selhalo</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8659"/>
        <source>The database clean-up failed with error message: %1</source>
        <translation>Pročišťování databáze selhalo s chybovým hlášením: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8678"/>
        <source>You have to be logged into the WebDatovka if you want to modify tags.</source>
        <translation>Musíte být přihlášeni do WebDatovky, aby bylo možné upravovat tagy.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8768"/>
        <source>Add account(s) error</source>
        <translation>Chyba přidávání účtu</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8789"/>
        <source>There aren&apos;t any Webdatovka accounts for this mojeID identity.</source>
        <translation>Pro tuto mojeID identitu nejsou žádné účty ve WebDatovce.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8796"/>
        <location filename="../src/gui/datovka.cpp" line="8805"/>
        <source>You are login into wrong mojeID identity.</source>
        <translation>Jste přihlášeni do špatné mojeID identity.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8799"/>
        <source>Please enter correct mojeID login for account &apos;%1&apos;.</source>
        <translation>Zadejte prosím správné mojeID přihlašovací jméno &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8810"/>
        <source>New mojeID identity has some account(s).</source>
        <translation>Nová modeID identita má nějaké účty.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8811"/>
        <source>Do you want to add account(s) for this mojeID identity to Datovka?</source>
        <translation>Chcete přidat účet pro tuto mojeID identitu do Datovky?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8826"/>
        <source>Some account(s) were removed from Webdatovka for this mojeID identity.</source>
        <translation>Některé účty byly odebrány z WebDatovky pro tuto mojeID identitu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8832"/>
        <source>Do you want to also remove these accounts from Datovka?</source>
        <translation>Chcete také odebrat tyto účty z Datovky?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8943"/>
        <source>This action is not supported for MojeID account &apos;%1&apos;</source>
        <translation>Tato akce není podporována pro mojeID účet &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="9028"/>
        <source>Login problem</source>
        <translation>Problém přihlášení</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8189"/>
        <source>Select directory for new databases</source>
        <translation>Vyberte složku pro nové databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8205"/>
        <source>Database file cannot split into same directory.</source>
        <translation>Databázový soubor nelze rozdělit do stejné složky.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8206"/>
        <source>Please, you must choose another directory.</source>
        <translation>Prosím, vyberte jinou složku.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8228"/>
        <source>Split of message database finished</source>
        <translation>Rozdělení databáze zpráv skončilo</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8230"/>
        <source>Congratulation: message database for account &apos;%1&apos; was split successfully. Please, restart the application for loading of new databases.</source>
        <translation>Gratulujeme: databáze zpráv pro účet &apos;%1&apos; byla úspěšně rozdělena. Prosím, restartujte aplikaci pro načtení nových databází zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8233"/>
        <source>Note: Original database file was backup to:</source>
        <translation>Původní databáze byla přesunuta do:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2045"/>
        <source>It was not possible download complete message &quot;%1&quot; from ISDS server.</source>
        <translation>Nebylo možné stáhnout celou zprávu &quot;%1&quot; ze serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2048"/>
        <location filename="../src/gui/datovka.cpp" line="2089"/>
        <source>Download message error</source>
        <translation>Chyba stahování zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3085"/>
        <source>Message &quot;%1&quot; was deleted from local database.</source>
        <translation>Zpráva &quot;%1&quot; byla smazána z místní databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3095"/>
        <source>Message &quot;%1&quot; was deleted from ISDS and local database.</source>
        <translation>Zpráva &quot;%1&quot; byla smazána z ISDS i z místní databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3068"/>
        <source>Message &quot;%1&quot; was deleted only from ISDS.</source>
        <translation>Zpráva &quot;%1&quot; byla smazána pouze z ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3078"/>
        <source>Message &quot;%1&quot; was deleted only from local database.</source>
        <translation>Zpráva &quot;%1&quot; byla smazána pouze z místní databáze.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3061"/>
        <source>Message &quot;%1&quot; was not deleted.</source>
        <translation>Zpráva &quot;%1&quot; nebyla smazána.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3150"/>
        <source>Synchronise all accounts with ISDS server.</source>
        <translation>Synchronizují se všechny účty ze serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3126"/>
        <source>Messages on the server</source>
        <translation>Zprávy na serveru</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3127"/>
        <source>received</source>
        <translation>přijato</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3128"/>
        <location filename="../src/gui/datovka.cpp" line="3130"/>
        <source>new</source>
        <translation>nových</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3129"/>
        <source>sent</source>
        <translation>odesláno</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4931"/>
        <source>Create a new account.</source>
        <translation>Vytvořit nový účet.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5773"/>
        <location filename="../src/gui/datovka.cpp" line="6230"/>
        <location filename="../src/gui/datovka.cpp" line="7801"/>
        <source>Select directory</source>
        <translation>Vybrat adresář pro import</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5788"/>
        <source>Database file(s) not found in selected directory.</source>
        <translation>Databázové soubory nenalezeny ve vybraném adresáři.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5798"/>
        <source>Select db file(s)</source>
        <translation>Vybrat databázové soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5799"/>
        <source>Database file (*.db)</source>
        <translation>Databázový soubor (*.db)</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5804"/>
        <location filename="../src/gui/datovka.cpp" line="8122"/>
        <source>Database file(s) not selected.</source>
        <translation>Databázové soubory nebyly vybrány.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5948"/>
        <source>Verifying the ZFO file &quot;%1&quot;</source>
        <translation>Ověřování ZFO souboru &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5975"/>
        <source>Server Datové schránky confirms that the message is authentic.</source>
        <translation>Server Datové schránky potvrdil, že zpráva je autentická.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5978"/>
        <location filename="../src/gui/datovka.cpp" line="6081"/>
        <source>Message was &lt;b&gt;successfully verified&lt;/b&gt; against data on the server Datové schránky.</source>
        <translation>Zpráva byla &lt;b&gt;úspěšně ověřena&lt;/b&gt; proti datům na serveru Datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5981"/>
        <location filename="../src/gui/datovka.cpp" line="6084"/>
        <source>This message has passed through the system of Datové schránky and has not been tampered with since.</source>
        <translation>Tato zpráva prošla systémem Datových schránek a nebyla od té doby upravena.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5986"/>
        <source>Server Datové schránky confirms that the message is not authentic.</source>
        <translation>Server Datové schránky potvrdil, že zpráva není autentická.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5989"/>
        <location filename="../src/gui/datovka.cpp" line="6092"/>
        <source>Message was &lt;b&gt;not&lt;/b&gt; authenticated as processed by the system Datové schránky.</source>
        <translation>Zpráva &lt;b&gt;nebyla&lt;/b&gt; ověřena jako zpracovaná systémem Datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5991"/>
        <location filename="../src/gui/datovka.cpp" line="6094"/>
        <source>It is either not a valid ZFO file or it was modified since it was downloaded from Datové schránky.</source>
        <translation>Soubor buď neobsahuje platný ZFO obsah, nebo byl od chvíle stažení ze systému Datových schránek modifikován.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5997"/>
        <location filename="../src/gui/datovka.cpp" line="6005"/>
        <location filename="../src/gui/datovka.cpp" line="6014"/>
        <source>Message authentication failed</source>
        <translation>Autentizace zprávy selhala</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5998"/>
        <location filename="../src/gui/datovka.cpp" line="6101"/>
        <source>Authentication of message has been stopped because the connection to server Datové schránky failed!
Check your internet connection.</source>
        <translation>Autentizace zprávy byla zastavena, protože se nezdařilo připojení k serveru Datové schránky!
Zkontrolujte připojení k internetu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6078"/>
        <source>Server Datové schránky confirms that the message is valid.</source>
        <translation>Server Datové schránky potvrdil, že zpráva je validní.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6089"/>
        <source>Server Datové schránky confirms that the message is not valid.</source>
        <translation>Server Datové schránky potvrdil, že zpráva není validní.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6091"/>
        <source>Message is not valid</source>
        <translation>Zpráva není validní</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6061"/>
        <location filename="../src/gui/datovka.cpp" line="6099"/>
        <location filename="../src/gui/datovka.cpp" line="6107"/>
        <location filename="../src/gui/datovka.cpp" line="6114"/>
        <location filename="../src/gui/datovka.cpp" line="6121"/>
        <source>Message verification failed.</source>
        <translation>Verifikace zprávy selhala.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6100"/>
        <source>Verification failed</source>
        <translation>Verifikace zprávy selhala</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6062"/>
        <location filename="../src/gui/datovka.cpp" line="6108"/>
        <location filename="../src/gui/datovka.cpp" line="6115"/>
        <location filename="../src/gui/datovka.cpp" line="6122"/>
        <source>Verification error</source>
        <translation>Chyba verifikace</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6109"/>
        <source>The message hash is not in local database.
Please download complete message from ISDS and try again.</source>
        <translation>Otisk zprávy nebyl nalezen v lokální databázi.
Prosím, stáhněte kompletní zprávu ze serveru Datové schránky a zkuste to znova.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6259"/>
        <location filename="../src/gui/datovka.cpp" line="6263"/>
        <location filename="../src/gui/datovka.cpp" line="7829"/>
        <location filename="../src/gui/datovka.cpp" line="7833"/>
        <source>ZFO file(s) not found in selected directory.</source>
        <translation>V adresáři nebyly nalezeny ZFO soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6262"/>
        <location filename="../src/gui/datovka.cpp" line="7832"/>
        <source>No ZFO file(s)</source>
        <translation>Žádné ZFO soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6272"/>
        <source>Select ZFO file(s)</source>
        <translation>Vyberte ZFO soubory</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6278"/>
        <source>ZFO file(s) not selected.</source>
        <translation>Nebyly vybrány ZFO soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6848"/>
        <source>Message &apos;%1&apos; stored to temporary file &apos;%2&apos;.</source>
        <translation>Zpráva &apos;%1&apos; uložena do dočasného souboru &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6854"/>
        <source>Message &apos;%1&apos; couldn&apos;t be stored to temporary file.</source>
        <translation>Zpráva &apos;%1&apos; nemohla být uložena do dočasného souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6857"/>
        <location filename="../src/gui/datovka.cpp" line="6943"/>
        <source>Error opening message &apos;%1&apos;.</source>
        <translation>Chyba při otevírání zprávy &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6934"/>
        <source>Message acceptance information &apos;%1&apos; stored to temporary file &apos;%2&apos;.</source>
        <translation>Doručenka zprávy &apos;%1&apos; uložena do dočasného souboru &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6940"/>
        <source>Message acceptance information &apos;%1&apos; couldn&apos;t be stored to temporary file.</source>
        <translation>Doručenka zprávy &apos;%1&apos; nemohla být uložena do dočasného souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7107"/>
        <source>Password required</source>
        <translation>Vyžadováno heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7108"/>
        <source>Account: %1
User name: %2
Certificate file: %3
Enter password to unlock certificate file:</source>
        <translation>Účet: %1
Uživatelské jméno: %2
Soubor certifikátu: %3
Zadejte heslo pro odemčení souboru certifikátu:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7027"/>
        <location filename="../src/gui/datovka.cpp" line="7055"/>
        <location filename="../src/gui/datovka.cpp" line="7075"/>
        <location filename="../src/gui/datovka.cpp" line="7094"/>
        <location filename="../src/gui/datovka.cpp" line="7155"/>
        <location filename="../src/gui/datovka.cpp" line="7179"/>
        <location filename="../src/gui/datovka.cpp" line="7197"/>
        <source>It was not possible to connect to your data box from account &quot;%1&quot;.</source>
        <translation>Nepodařilo se přihlásit k datové schránce z účtu &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7704"/>
        <source>Password expiration</source>
        <translation>Expirace hesla</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7707"/>
        <source>According to the last available information, your password for account &apos;%1&apos; (login &apos;%2&apos;) expired %3 days ago (%4).</source>
        <translation>Podle nejnovějších dostupných informací Vaše heslo pro účet &apos;%1&apos; (přihlašovací jméno &apos;%2&apos;) vypršelo před %3 dnem/dny (%4).</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7718"/>
        <source>According to the last available information, your password for account &apos;%1&apos; (login &apos;%2&apos;) will expire in %3 days (%4).</source>
        <translation>Podle nejnovějších dostupných informací Vaše heslo pro účet &apos;%1&apos; (přihlašovací jméno &apos;%2&apos;) vyprší za %3 dny/dnů (%4).</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7723"/>
        <source>You can change your password now, or later using the &apos;Change password&apos; command. Your new password will be valid for 90 days.

Change password now?</source>
        <translation>Heslo můžete změnit nyní nebo později pomocí příkazu &apos;Změnit heslo&apos;. Nové heslo bude platné po dobu 90 dnů.

Změnit heslo nyní?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7137"/>
        <source>Enter OTP security code</source>
        <translation>Zadejte OTP bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3554"/>
        <source>Database is stored in memory. Data will be lost on application exit.</source>
        <translation>Databáze je uložena v paměti. Data budou ztracena při vypnutí aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3557"/>
        <location filename="../src/gui/datovka.cpp" line="3561"/>
        <source>Local database file location</source>
        <translation>Umístění souboru místní databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4901"/>
        <source>Create and send a message.</source>
        <translation>Vytvoř a odešli zprávu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5029"/>
        <source>Account &apos;%1&apos; was deleted but its message database was not deleted.</source>
        <translation>Účet &apos;%1&apos; byl smazán, ale jeho databázi zpráv se smazat nepodařilo.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5901"/>
        <source>This database file has been set as actual message database for this account. Maybe you have to change account properties for correct login to the server Datové schránky.</source>
        <translation>Tento databázový soubor byl nastaven jako aktuální databáze pro ukládání zpráv pro nový účet. Možná bude zapotřebí upravit vlastnosti tohoto účtu aby bylo možné se připojit k serveru Datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5856"/>
        <location filename="../src/gui/datovka.cpp" line="5881"/>
        <location filename="../src/gui/datovka.cpp" line="5908"/>
        <source>Create account: %1</source>
        <translation>Nový účet: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="295"/>
        <source>Tags</source>
        <translation>Tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1853"/>
        <source>Saving attachment of message &apos;%1&apos; to files was not successful!</source>
        <translation>Nepodařilo se uložit přílohu zprávy &apos;%1&apos;!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1883"/>
        <source>Saving attachment of message &apos;%1&apos; to file was successful.</source>
        <translation>Nepodařilo se uložit přílohu zprávy &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1893"/>
        <source>Saving attachment of message &apos;%1&apos; to file was not successful!</source>
        <translation>Nepodařilo se uložit přílohu zprávy &apos;%1&apos;!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1978"/>
        <source>Attachment &apos;%1&apos; stored into temporary file &apos;%2&apos;.</source>
        <translation>Příloha &apos;%1&apos; uložena do dočasného souboru &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1982"/>
        <source>Attachment &apos;%1&apos; couldn&apos;t be stored into temporary file.</source>
        <translation>Příloha &apos;%1&apos; nemohla být uložena do dočasného souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2054"/>
        <location filename="../src/gui/datovka.cpp" line="2095"/>
        <source>A connection error occurred or the message has already been deleted from the server.</source>
        <translation>Nastala chyba spojení, nebo zpráva již byla smazána ze serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2064"/>
        <location filename="../src/gui/datovka.cpp" line="2105"/>
        <source>Couldn&apos;t download message &apos;%1&apos;.</source>
        <translation>Nešlo stáhnout zprávu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2086"/>
        <source>It was not possible download complete message &quot;%1&quot; from webdatovka server.</source>
        <translation>Nebylo možné stáhnout celou zprávu &apos;%1&apos; ze serveru WebDatovky.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2090"/>
        <source>It was not possible to download a complete message &quot;%1&quot; from webdatovka server.</source>
        <translation>Nebylo možné stáhnout celou zprávu &apos;%1&apos; ze serveru WebDatovky.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2093"/>
        <source>Webdatovka: </source>
        <translation>WebDatovka:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2126"/>
        <source>It was not possible download received message list from server.</source>
        <translation>Nebylo možné stáhnout seznam přijatých zpráv ze serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2128"/>
        <source>It was not possible download sent message list from server.</source>
        <translation>Nebylo možné stáhnout seznam odeslaných zpráv ze serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2136"/>
        <source>Server: </source>
        <translation>Server:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2139"/>
        <source>A connection error occurred.</source>
        <translation>Nastala chyba spojení.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2206"/>
        <source>Message from &apos;%1&apos; (%2) has been successfully sent to &apos;%3&apos; (%4).</source>
        <translation>Zpráva od &apos;%1&apos; (%2) byla úspěšně odeslána příjemci &apos;%3&apos; (%4).</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2214"/>
        <source>Error while sending message from &apos;%1&apos; (%2) to &apos;%3&apos; (%4).</source>
        <translation>Chyba během odesílání zprávy od &apos;%1&apos; (%2) příjemci &apos;%3&apos; (%4).</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2233"/>
        <source>Message &quot;%1&quot;  was downloaded from server.</source>
        <translation>Zpráva &quot;%1&quot; byla úspěšně stažena ze serveru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2855"/>
        <source>Warning: If you delete selected messages from ISDS then these messages will be lost forever.</source>
        <translation>Upozornění: Jestli-že smažete vybrané zprávy ze serveru ISDS, budou tyto zprávy navždy ztraceny.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2918"/>
        <source>You have to be logged into the Webdatovka if you want to delete message(s).</source>
        <translation>Musíte být přihlášeni do WebDatovky, aby bylo možné mazat zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2936"/>
        <source>Warning: If you delete the message from Webdatovka then this message will be lost forever.</source>
        <translation>Upozornění: Jestliže smažete vybrané zprávy z WebDatovky, budou tyto zprávy navždy ztraceny.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="2941"/>
        <source>Warning: If you delete selected messages from Webdatovka then these messages will be lost forever.</source>
        <translation>Upozornění: Jestli-že smažete vybrané zprávy z WebDatovky, budou tyto zprávy navždy ztraceny.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3322"/>
        <location filename="../src/gui/datovka.cpp" line="6390"/>
        <source>You have to be logged into the WebDatovka if you want to download complete message.</source>
        <translation>Musíte být přihlášeni do WebDatovky, aby bylo možné stahovat kompletní zprávy.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3382"/>
        <source>MojeID account</source>
        <translation>MojeID účet</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3878"/>
        <source>Some databases of %1 in &apos;%2&apos; cannot be used.</source>
        <translation>Některé databáze %1 v &apos;%2&apos; nelze použít.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3914"/>
        <source>Conflicting databases %1 in &apos;%2&apos; cannot be used.</source>
        <translation>Konfliktní databáze %1 v &apos;%2&apos; nelze použít.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3917"/>
        <source>Please remove the conflicting files.</source>
        <translation>Prosím, odstraňte konfliktní databázové soubory.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3954"/>
        <source>Database files in &apos;%1&apos; cannot be created or are corrupted.</source>
        <translation>Databázové soubory v &apos;%1&apos; nelze vytvořit nebo jsou poškozené.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="4886"/>
        <source>Full message not present!</source>
        <translation>Úplná zpráva není k dispozici!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5240"/>
        <source>Database files for &apos;%1&apos; have been successfully moved to

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; byl úspěšně přesunuty do

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5246"/>
        <source>Database files for &apos;%1&apos; could not be moved to

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; nemohly být přesunuty do

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5265"/>
        <source>Database files for &apos;%1&apos; have been successfully copied to

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; byl úspěšně překopírovány do

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5271"/>
        <source>Database files for &apos;%1&apos; could not be copied to

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; nemohli být překopírovány do

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5290"/>
        <source>New database files for &apos;%1&apos; have been successfully created in

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; byl úspěšně vytvořeny v

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5297"/>
        <source>New database files for &apos;%1&apos; could not be created in

&apos;%2&apos;.</source>
        <translation>Databázové soubory pro &apos;%1&apos; nemohli být vytvořeny v

&apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5332"/>
        <source>You have to be logged into the WebDatovka if you want to find databox.</source>
        <translation>Musíte být přihlášeni do WebDatovky, aby bylo možné vyhledávat datové schránky.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7142"/>
        <source>Enter OTP security code for account</source>
        <translation>Zadejte OTP bezpečnostní kód pro účet</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3656"/>
        <location filename="../src/gui/datovka.cpp" line="8269"/>
        <source>Portable version</source>
        <translation>Přenosná verze</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5087"/>
        <location filename="../src/gui/datovka.cpp" line="7283"/>
        <source>Change password of account &quot;%1&quot;.</source>
        <translation>Změnit heslo pro účet &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5109"/>
        <source>Change properties of account &quot;%1&quot;.</source>
        <translation>Změnit vlastnosti účtu &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5123"/>
        <source>Account &quot;%1&quot; was updated.</source>
        <translation>Účet &quot;%1&quot; byl aktualizován.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5145"/>
        <source>Account was moved up.</source>
        <translation>Účet byl posunut nahoru.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5164"/>
        <source>Account was moved down.</source>
        <translation>Účet byl posunut dolů.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5189"/>
        <source>Change data dierctory of account &quot;%1&quot;.</source>
        <translation>Změnit adresář pro data u účtu &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5351"/>
        <source>Find databoxes from account &quot;%1&quot;.</source>
        <translation>Vyhledat datovou schránku z účtu &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5996"/>
        <location filename="../src/gui/datovka.cpp" line="6004"/>
        <location filename="../src/gui/datovka.cpp" line="6013"/>
        <source>Message authentication failed.</source>
        <translation>Zpráva je validní.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6006"/>
        <source>Authentication of message has been stopped because the message file has wrong format!</source>
        <translation>Ověřování zprávy bylo zastaveno, protože zpráva má chybný formát!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6015"/>
        <location filename="../src/gui/datovka.cpp" line="6063"/>
        <location filename="../src/gui/datovka.cpp" line="6123"/>
        <source>An undefined error occurred!
Try again.</source>
        <translation>Došlo k neznámé chybě!
Zkuste to znovu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6080"/>
        <source>Message is valid</source>
        <translation>Zpráva je platná</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6116"/>
        <source>The message hash cannot be verified because an internal error occurred!
Try again.</source>
        <translation>Nebylo možné ověřit otisk zprávy, protože se v aplikaci vyskytla chyba!
Zkuste to znova.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="1384"/>
        <location filename="../src/gui/datovka.cpp" line="6600"/>
        <location filename="../src/gui/datovka.cpp" line="6696"/>
        <source>Message export error!</source>
        <translation>Chyba exportu zprávy!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7712"/>
        <source>You have to change your password from the ISDS web interface. Your new password will be valid for 90 days.</source>
        <translation>Heslo si změňte ve webovém rozhraní ISDS. Vaše nové heslo bude platné 90 dnů.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7779"/>
        <location filename="../src/gui/datovka.cpp" line="7791"/>
        <source>Checking time stamps in account &apos;%1&apos;...</source>
        <translation>Kontrola časových razítek v účtu &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7974"/>
        <source>Time stamp expiration check results</source>
        <translation>Výsledky kontroly expirace časových razítek</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7959"/>
        <source>Time stamp expiration check in account &apos;%1&apos; finished with result:</source>
        <translation>Kontrola časových razítek v účtu &apos;%1&apos; skončila s výsledkem:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7879"/>
        <source>Loading of ZFO file(s) failed!</source>
        <translation>Načítání ZFO souborů selhalo!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7918"/>
        <source>Time stamp expiration check of ZFO files finished with result:</source>
        <translation>Kontrola časových razítek ZFO souborů skončila s výsledkem:</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7921"/>
        <source>Total of ZFO files: %1</source>
        <translation>Celkem ZFO souborů: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7923"/>
        <source>ZFO files with time stamp expiring within %1 days: %2</source>
        <translation>ZFO souborů s časovým razítkem expirujícím do %1 dnů: %2</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7927"/>
        <source>Unchecked ZFO files: %1</source>
        <translation>Nezkontrolovaných ZFO souborů: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7963"/>
        <location filename="../src/gui/datovka.cpp" line="9152"/>
        <source>Total of messages in database: %1</source>
        <translation>Celkem zpráv v databázi: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7965"/>
        <source>Messages with time stamp expiring within %1 days: %2</source>
        <translation>Zpráv s časovým razítkem expirujícím do %1 dnů: %2</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7969"/>
        <source>Unchecked messages: %1</source>
        <translation>Nezkontrolovaných zpráv: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7979"/>
        <source>See details for more info...</source>
        <translation>Více informací naleznete v podrobnostech...</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7982"/>
        <source>Do you want to export the expiring messages to ZFO?</source>
        <translation>Chcete exportovat expirující zprávy do ZFO?</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7990"/>
        <location filename="../src/gui/datovka.cpp" line="8006"/>
        <source>Time stamp of message %1 expires within specified interval.</source>
        <translation>Časové razítko zprávy %1 expiruje do určeného limitu.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7998"/>
        <location filename="../src/gui/datovka.cpp" line="8014"/>
        <source>Time stamp of message %1 is not present.</source>
        <translation>Časové razítko zprávy %1 není k dispozici.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6826"/>
        <location filename="../src/gui/datovka.cpp" line="6912"/>
        <source>Datovka - Export error!</source>
        <translation>Datovka - chyba exportu!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6827"/>
        <location filename="../src/gui/datovka.cpp" line="6913"/>
        <source>Cannot export the message </source>
        <translation>Nelze exportovat zprávu </translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="6830"/>
        <location filename="../src/gui/datovka.cpp" line="6916"/>
        <source>First you must download message before its export...</source>
        <translation>Musíte nejprve stáhnout kompletní zprávu před exportem...</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7168"/>
        <source>SMS code for account </source>
        <translation>SMS kód pro účet </translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="21"/>
        <location filename="../src/gui/datovka.cpp" line="7337"/>
        <source>Datovka</source>
        <translation>Datovka</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7414"/>
        <source>New account error</source>
        <translation>Chyba nového účtu</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7417"/>
        <source>It was not possible to get user info and databox info from ISDS server for account</source>
        <translation>Nebylo možné získat informace o uživateli a jeho datové schránce ze serveru ISDS pro účet</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7429"/>
        <source>Account</source>
        <translation>Účet</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7432"/>
        <source>was not created!</source>
        <translation>nebyl vytvořen!</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8280"/>
        <source>Welcome...</source>
        <translation>Vítejte...</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8284"/>
        <source>Storage: disk | disk</source>
        <translation>Úložiště: disk | disk</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="3538"/>
        <source>unknown or without expiration</source>
        <translation>Neznámý nebo bez expirace</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5873"/>
        <source>Account with user name &apos;%1&apos; and its message database already exist. New account was not created and selected database file was not associated with this account.</source>
        <translation>Účet s uživatelským jménem &apos;%1&apos; a jeho databáze již existují. Nový účet nebyl vytvořen a vybraná databáze nebyla k účtu přiřazena.</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="5898"/>
        <source>Account with name &apos;%1&apos; has been created (user name &apos;%1&apos;).</source>
        <translation>Účet s názvem &apos;%1&apos; byl vytvořen (uživatelské jméno &apos;%1&apos;).</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="7003"/>
        <location filename="../src/gui/datovka.cpp" line="9032"/>
        <source>Mode: online</source>
        <translation>Režim: připojeno (online)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="357"/>
        <location filename="../src/gui/datovka.cpp" line="5857"/>
        <location filename="../src/gui/datovka.cpp" line="5882"/>
        <location filename="../src/gui/datovka.cpp" line="5909"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="405"/>
        <source>Message</source>
        <translation>Zpráva</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="435"/>
        <source>Tools</source>
        <translation>Nástroje</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="453"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="474"/>
        <source>toolBar</source>
        <translation>Panel nástrojů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="499"/>
        <source>Sync all accounts</source>
        <translation>Synchronizovat účty</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="835"/>
        <source>Download complete message, including attachments and verify its signature</source>
        <translation>Stáhnout kompletní zprávu, včetně příloh a ověřit její podpis</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="990"/>
        <source>Open attachment in an associated application</source>
        <translation>Otevřít přílohu v přiřazené aplikaci</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="160"/>
        <location filename="../src/gui/ui/datovka.ui" line="865"/>
        <source>Signature details</source>
        <translation>Detail podpisu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="378"/>
        <source>Data box</source>
        <translation>Schránka</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="532"/>
        <location filename="../src/gui/ui/datovka.ui" line="535"/>
        <source>Remove account</source>
        <translation>Odstranit účet</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="157"/>
        <source>Show details about electronic signature of this message</source>
        <translation>Zobrazit detailní informace o digitálním podpisu zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="186"/>
        <source>For current message you can locally set
if the message is not processed, if the 
message is in progress or if was already 
sent reply on this message</source>
        <translation>Pro aktuální zprávu můžete lokálně nastavit
v jakém stavu vyřízení se zpráva zrovna nachází
Zpráva je buď ve stavu nevyřízeno, vyřizuje se
nebo vyřízeno (bylo na zprávu odpovězeno)</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8308"/>
        <source>Unsettled</source>
        <translation>Nevyřízeno</translation>
    </message>
    <message>
        <location filename="../src/gui/datovka.cpp" line="8312"/>
        <source>Settled</source>
        <translation>Vyřízeno</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="505"/>
        <source>Shift+F5</source>
        <translation>Shift+F5</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="550"/>
        <location filename="../src/gui/ui/datovka.ui" line="553"/>
        <source>Proxy settings</source>
        <translation>Nastavení proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="565"/>
        <source>Preferences</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="568"/>
        <source>Preferences of Datovka</source>
        <translation>Nastavení Datovky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="580"/>
        <source>Quit</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="586"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="598"/>
        <location filename="../src/gui/ui/datovka.ui" line="601"/>
        <source>Create account from database</source>
        <translation>Vytvořit účet z databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="619"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="631"/>
        <source>Create message</source>
        <translation>Vytvořit zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="637"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="649"/>
        <source>Mark all as read</source>
        <translation>Označit vše jako přečtené</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="664"/>
        <source>Change password</source>
        <translation>Změnit heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="679"/>
        <source>Account properties</source>
        <translation>Vlastnosti účtu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="694"/>
        <source>Move account up</source>
        <translation>Posunout účet nahoru</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="709"/>
        <source>Move account down</source>
        <translation>Posunout účet dolů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="724"/>
        <source>Change data directory</source>
        <translation>Změnit adresář pro uložení dat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="739"/>
        <source>About Datovka</source>
        <translation>O Datovce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="742"/>
        <source>About Datovka application</source>
        <translation>O Datovce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="772"/>
        <source>Find Data Box</source>
        <translation>Vyhledat datovou schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="787"/>
        <source>Authenticate message file</source>
        <translation>Ověřit zprávu ze souboru ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="802"/>
        <source>View message from ZFO file</source>
        <translation>Zobrazit zprávu ze souboru ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="817"/>
        <source>Export correspondence overview</source>
        <translation>Exportovat přehled korespondence</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="502"/>
        <source>Synchronize all accounts at once</source>
        <translation>Synchronizovat všechny účty najednou</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="520"/>
        <source>Add new data box account</source>
        <translation>Přidat nový účet k datovým schránkám</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="583"/>
        <source>Quit the application</source>
        <translation>Ukončit aplikaci</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="613"/>
        <source>Sync account</source>
        <translation>Synchronizovat účet</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="616"/>
        <source>Synchronize selected account</source>
        <translation>Synchronizovat vybraný účet</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="652"/>
        <source>Mark all messages as read</source>
        <translation>Označit všechny zprávy jako přečtené</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="667"/>
        <source>Sets a new password to the selected account on the ISDS server</source>
        <translation>Nastaví nové heslo pro zvolený účet na serveru ISDS</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="682"/>
        <source>Manage account properties</source>
        <translation>Spravovat vlastnosti účtu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="697"/>
        <source>Move selectet account one position up</source>
        <translation>Posunout vybraný účet jednu pozici nahoru</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="712"/>
        <source>Move selectet account one position down</source>
        <translation>Posunout vybraný účet jednu pozici dolů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="727"/>
        <source>Change the directory where data are being stored for the selected account</source>
        <translation>Změnit adresář, kam se ukládají data pro vybraný účet</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="775"/>
        <source>Find data box</source>
        <translation>Najít datovou schránku</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="790"/>
        <source>Verify message authenticity</source>
        <translation>Ověřit pravost zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="805"/>
        <source>View content of a ZFO file</source>
        <translation>Zobrazit obsah ZFO souboru</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="820"/>
        <source>Create a correspondence overview</source>
        <translation>Vytvořit přehled korespondence</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="832"/>
        <source>Download signed message</source>
        <translation>Stáhnout podepsanou zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="850"/>
        <source>Reply to the selected message</source>
        <translation>Odpovědět na vybranou zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="868"/>
        <source>Display details about the signature</source>
        <translation>Zobrazit podrobnosti o podpisu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="898"/>
        <source>Deletes message from local database and/or from ISDS server</source>
        <translation>Vymazat zprávu z lokální databáze a/nebo ze serveru ISDS</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="909"/>
        <source>Export the selected message as ZFO file</source>
        <translation>Exportovat vybranou zprávu jako ZFO soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="920"/>
        <source>Pass the selected message to an external application</source>
        <translation>Předat vybranou zprávu externí aplikaci</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="931"/>
        <source>Pass the acceptance information of the selected message to an external application</source>
        <translation>Předat doručenku vybrané zprávy externí aplikaci</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="942"/>
        <source>Export the acceptance information of the selected message as ZFO file</source>
        <translation>Exportovat doručenku vybrané zprávy jako ZFO soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="953"/>
        <source>Export the acceptance information of the selected message as PDF file</source>
        <translation>Exportovat doručenku vybrané zprávy jako PDF soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="964"/>
        <source>Export the envelope of the selected message as PDF file</source>
        <translation>Exportovat obálku vybrané zprávy jako PDF soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="972"/>
        <source>Export envelope PDF with attachments</source>
        <translation>Exportovat obálku jako PDF s přílohami</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="975"/>
        <source>Export the envelope to a PDF file together with message attachments</source>
        <translation>Exportovat obálku to souboru PDF společně s přílohami zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1005"/>
        <source>Save selected attachments to files</source>
        <translation>Uložit vybrané přílohy do souborů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1020"/>
        <source>Saves all message attachments</source>
        <translation>Uloží všechny přílohy zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1032"/>
        <source>Import messages from ZFO files</source>
        <translation>Importovat zprávy ze souborů ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1035"/>
        <source>Import a message from ZFO file into the database</source>
        <translation>Importovat zprávu se souboru ZFO do databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1050"/>
        <source>Use the selected message as a template</source>
        <translation>Použít vybranou zprávu jako šablonu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1062"/>
        <source>Search message</source>
        <translation>Vyhledat zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1083"/>
        <source>Check whether the message time stamp is not expired or expiring</source>
        <translation>Oveřit, zda časové razítko zprávy neexpirovalo nebo neexpiruje</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1095"/>
        <source>Homepage</source>
        <translation>Domovská stránka</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1098"/>
        <source>Open the home page of the application</source>
        <translation>Otevřít domovskou stránku aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1110"/>
        <source>Import messages from database</source>
        <translation>Importovat zprávy z databáze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1113"/>
        <source>Import messages into database from an external database file</source>
        <translation>Importovat zprávy do databáze z externího databázového souboru</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1125"/>
        <source>Split database by years</source>
        <translation>Rozdělit databázi podle roků</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1128"/>
        <source>Messages are going to be stored into separate database files according to years</source>
        <translation>Zprávy budou ukládány do databázových souborů rozdělených podle let</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1136"/>
        <source>E-mail with ZFOs</source>
        <translation>E-mail se ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1139"/>
        <source>Creates an e-mail containing ZFOs of selected messages</source>
        <translation>Vytvoří e-mail obsahující ZFO vybraných zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1147"/>
        <source>E-mail with all attachments</source>
        <translation>E-mail se všemi přílohami</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1150"/>
        <source>Creates an e-mail containing all attachments of selected messages</source>
        <translation>Vytvoří e-mail obsahující všechny přílohy vybraných zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1158"/>
        <source>E-mail with selected attachments</source>
        <translation>E-mail s vybranými přílohami</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1161"/>
        <source>Creates an e-mail containing selected attachments</source>
        <translation>Vytvoří e-mail obsahující vybrané přílohy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1173"/>
        <location filename="../src/gui/ui/datovka.ui" line="1176"/>
        <source>Edit tags</source>
        <translation>Editovat tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1188"/>
        <source>Vacuum message database</source>
        <translation>Vakuum databáze zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1191"/>
        <source>This may reduce the database file size and optimise the access speed</source>
        <translation>To může zmenšit velikost databáze a optimalizovat přístupovou rychlost</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1203"/>
        <location filename="../src/gui/ui/datovka.ui" line="1206"/>
        <source>Forward message</source>
        <translation>Přeposlat zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1218"/>
        <source>Add mojeID account</source>
        <translation>Přidat účet mojeID</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1221"/>
        <source>Add new mojeID account</source>
        <translation>Přidat nový mojeID účet</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1047"/>
        <source>Use message as template</source>
        <translation>Použít zprávu jako šablonu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1065"/>
        <source>Advanced searching in message envelopes</source>
        <translation>Rozšířené vyhledávání v obálkách zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1080"/>
        <source>Time stamp expiration check</source>
        <translation>Zkontrolovat expirace časových razítek</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1068"/>
        <source>Ctrl+Shift+F</source>
        <translation>Ctrl+Shift+F</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="757"/>
        <location filename="../src/gui/ui/datovka.ui" line="760"/>
        <source>User manual</source>
        <translation>Uživatelská příručka</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="634"/>
        <source>Create and send a new message</source>
        <translation>Vytvořit a poslat novou zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="883"/>
        <source>Verify the selected message</source>
        <translation>Ověřit vybranou zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="847"/>
        <source>Reply to message</source>
        <translation>Odpovědět na zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="853"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="880"/>
        <source>Authenticate message</source>
        <translation>Ověřit zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="895"/>
        <source>Delete message</source>
        <translation>Vymazat zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="906"/>
        <source>Export message as ZFO</source>
        <translation>Exportovat zprávu jako soubor ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="917"/>
        <source>Open message externally</source>
        <translation>Otevřít zprávu externí aplikací</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="928"/>
        <source>Open acceptance info externally</source>
        <translation>Otevřít doručenku pomocí externí aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="939"/>
        <source>Export acceptance info as ZFO</source>
        <translation>Exportovat doručenku do ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="950"/>
        <source>Export acceptance info as PDF</source>
        <translation>Exportovat doručenku do PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="961"/>
        <source>Export message envelope as PDF</source>
        <translation>Exportovat obálku zprávy do PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="987"/>
        <source>Open attachment</source>
        <translation>Otevřít přílohu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1002"/>
        <source>Save attachment</source>
        <translation>Uložit soubor jako</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/datovka.ui" line="1017"/>
        <source>Save all attachments</source>
        <translation>Uložit vše</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="46"/>
        <source>Downloading</source>
        <translation>Stahování</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="70"/>
        <source>Messages downloading</source>
        <translation>Stahování zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="148"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="271"/>
        <source>minutes</source>
        <translation>minut</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="183"/>
        <source>Automatically download whole messages (may be slow)</source>
        <translation>Automaticky stahovat kompletní zprávy (může být pomalé)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="200"/>
        <source>Automatically synchronize all accounts on startup</source>
        <translation>Automaticky synchronizovat všechny účty po spuštění aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="424"/>
        <source>New versions</source>
        <translation>Nové verze</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="20"/>
        <source>Datovka - Preferences</source>
        <translation>Datovka - nastavení</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="88"/>
        <source>When allowed, Datovka will automatically download messages 
on the background of application without blocking of user interface.
Download period you can set bellow.</source>
        <translation>Pokud povolíte tuto možnost, Datovka bude automaticky stahovat
nové zprávy na pozadí aplikace bez blokování uživatelského rozhraní.
Periodu stahování můžete nastavit níže.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="93"/>
        <source>Automatically synchronise all in background</source>
        <translation>Automaticky provádět hromadnou synchronizaci na pozadí</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="112"/>
        <source> Check every</source>
        <translation> Kontrolovat každých</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="176"/>
        <source>When accessing your Databox, only envelopes of messages are downloaded at first. 
You can then download the whole message, including any attachments, manually. 
When this option is turned on, Datovka will perform the download of complete 
message for you. The only downside to this approach is longer waiting time on 
slower internet connections.</source>
        <translation>Při přístupu k vaší datové schránce, Datovka nejprve stahuje pouze obálky zpráv.
Poté je možné stáhnout celou zprávu včetně příloh ručně. Pokud zapnete tuto volbu,
Datovka pro vás kompletní zprávy stáhne automaticky. Jedinou nevýhodou tohoto 
přístupu je delší doba stahování při pomalejším internetovém připojení.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="196"/>
        <source>When allowed, Datovka will automatically synchronise accounts 
and also download new messages on background at startup.</source>
        <translation>Pokud povolíte tuto možnost, Datovka bude automaticky synchronizovat
účty a stahovat nové zprávy na pozadí po spuštění aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="436"/>
        <source>When this option is active, Datovka will automatically
check for new Datovka versions on startup.</source>
        <translation>Pokud je tato volba aktivní, Datovka bude automaticky
zjišťovat nové verze programu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="440"/>
        <source>Check for new Datovka versions on startup</source>
        <translation>Kontrolovat nové verze Datovky po spuštění aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="459"/>
        <source>When this option is active, Datovka will send information about your 
Datovka version and related data as part of the request for new versions. 
This data contains the version of Datovka, version of libraries, operating 
system platform and an ID randomly created for your application at first start.

No personal data of any kind is sent. 
The data is used solely for statistical purposes.</source>
        <translation>Pokud je tato volba aktivní, Datovka bude při dotazu na nové verze odesílat
informaci o verzi programu a informace s tím spojené. Tato data jsou verze 
Datovky, verze knihoven, platforma operačního systému a ID vaší aplikace 
náhodně vygenerované při prvním spuštění.

Neposílají se žádná osobní data.
Odeslaná data jsou použita pouze pro účely statistiky.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="468"/>
        <source>Send version info alongside the request</source>
        <translation>Posílat informaci o stávající verzi s dotazem</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="495"/>
        <source>Security</source>
        <translation>Bezpečnost</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="507"/>
        <source>Storage options</source>
        <translation>Volba uložení dat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="520"/>
        <source>It migth be possible for a person with access to your home directory to modify data in the Datovka database with malicious intent or to steal you login credentials for Datové Schránky. Using the checkboxes below, you can influence which data Datovka stores on the disk. Note: Password storage can be adjusted on per-account basis in the credentials dialog.</source>
        <translation>Osoby, které mají přístup do Vašeho domovského adresáře, mohou zlomyslně modifikovat data v databázi, nebo odcizit přihlašovací údaje k datovým schránkám. Pomocí následujících voleb můžete zvolit, jaká data se budou ukládat na disk. Poznámka: Ukládání hesla se dá nastavit pro jednotlivé účty v nastavení účtu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="536"/>
        <source>When allowed, messages are stored in a database in your home directory. 
Such storage might be compromised by an attacker and the content of 
messages might be modified. When turned off, messages are freshly 
downloaded each time you start Datovka. In this case messages older 
than 90 day may not be available.</source>
        <translation>Pokud povolíte tuto možnost, Datovka bude ukládat stažené zprávy do databáze 
ve vašem domovském adresáři. Takové úložiště může být napadeno útočníkem a 
obsah zpráv upraven. Pokud tuto možnost vypnete, zprávy se budou při každém 
spuštění Datovky nově stahovat. V tomto případě nebudou k dispozici zprávy 
starší než 90 dní.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="543"/>
        <source>Allow message storage</source>
        <translation>Povolit ukládání zpráv na disk</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="559"/>
        <source>Datovka stores some additional data outside the message database. 
These are data about the databox which allow for example warning 
about an expiring password. These data might be also changed by 
an attacker, but their nature makes it a relatively harmless possibility.</source>
        <translation>Datovka ukládá některá doplňková data mimo databázi zpráv. Jsou to data o 
datové schránce, která například umožňují varování před vypršením hesla. 
Tato data by mohla být rovněž změněna případným útočníkem, ale jejich 
povaha činí tuto možnost relativně neškodnou.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="565"/>
        <source>Allow storage of additional data</source>
        <translation>Povolit ukládání ostatních dat na disk</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="725"/>
        <source>days</source>
        <translation>dnů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="762"/>
        <source>Navigation</source>
        <translation>Navigace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="774"/>
        <source>When account is opened, select</source>
        <translation>Když je vybrán účet, tak označit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="786"/>
        <source>After an account is opened,
the newest message will be activated.</source>
        <translation>Po otevření účtu bude otevřena
nejnovější zpráva.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="790"/>
        <source>Newest message</source>
        <translation>Nejnovější zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="806"/>
        <source>When account is opened, the last message
displayed in this account will be selected.</source>
        <translation>Po otevření účtu bude vybrána poslední
zobrazená zpráva z tohoto účtu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="810"/>
        <source>Last displayed message</source>
        <translation>Poslední zobrazenou zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="823"/>
        <source>When account is opened, random message
displayed in this account will be selected.</source>
        <translation>Po otevření účtu bude vybrána náhodná zpráva z tohoto účtu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="827"/>
        <source>Nothing</source>
        <translation>Nic</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1228"/>
        <source>Specifies file name format for attachment files and other
exported files. Default value when saving attachment
files is %f (i.e. saved file name will match the original
file name including its suffix).</source>
        <translation>Určuje formát názvu souboru pro přílohy a jiné exportované soubory.
Výchozí nastavení pro ukládání souborů příloh je %f (tzn.
jméno ukládáného souboru bude odpovídat původnímu jmému
přílohy).</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1234"/>
        <source>File name format of saved/exported files</source>
        <translation>Formát jména ukládaných/exportovaných souborů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1251"/>
        <source>Message file name:</source>
        <translation>Jméno souboru zprávy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1291"/>
        <source>Acceptance info file name:</source>
        <translation>Jméno souboru doručenky:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1304"/>
        <source>Attachment file name:</source>
        <translation>Jméno souboru přílohy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1428"/>
        <source>acceptance year (YYYY)</source>
        <translation>rok doručení (YYYY)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1442"/>
        <source>acceptance month (MM)</source>
        <translation>měsíc doručení (MM)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1456"/>
        <source>acceptance day (DD)</source>
        <translation>den doručení (DD)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1470"/>
        <source>acceptance minute (mm)</source>
        <translation>minuta doručení (mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1484"/>
        <source>acceptance hour (hh)</source>
        <translation>hodina doručení (hh)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1783"/>
        <source>Saves acceptance info for every attachment file separately.
The acceptance info file name has to contain the original
attachment file name (parameter %f is required).</source>
        <translation>Uloží zvlášť doručenku pro každý soubor přílohy.
Název souboru doručenky musí obsahovat původní
název přílohy (je vyžadován parametr %f).</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="584"/>
        <source>Signing certificate validity</source>
        <translation>Platnost podepisujícího certifikátu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="596"/>
        <source>Validity of the signing certificate will be checked against
the current date. This gives the highest protection against
compromised certificate exploit.</source>
        <translation>Platnost podepisujícího certifikátu je kontrolována proti současnému datu.
Toto nastavení poskytuje maximální ochranu proti zneužití kompromitovaného
certifikátu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="601"/>
        <source>Check against current date (safer)</source>
        <translation>Kontrolovat proti aktuálnímu datu (bezpečnější)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="617"/>
        <source>The validity of signing certificate is checked against a
stored download date. This allows long-term storage 
of messages without false alarms about invalid signature. 
It is less safe, because the attacker could modify the 
download date in the Datovka database.</source>
        <translation>Platnost podepisujícího certifikátu je kontrolována proti uloženému času
stažení. Toto nastavení umožňuje dlouhodobé ukládání zpráv bez chybných
hlášení o neplatném podpisu. Tato možnost je méně bezpečná, protože
útočník by mohl upravit čas stažení zprávy v databázi Datovky.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="624"/>
        <source>Check against the date of download</source>
        <translation>Kontrolovat proti datu stažení zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="644"/>
        <source>Check certificate revocation list (CRL).</source>
        <translation>Kontrolovat seznam zneplatněných certifikátů.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="647"/>
        <source>Check certificate revocation list (CRL)</source>
        <translation>Kontrolovat seznam zneplatněných certifikátů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="216"/>
        <source>Connection settings</source>
        <translation>Nastavení připojení</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="236"/>
        <source>Timeout for message downloading is set on</source>
        <translation>Časový limit pro stahování zpráv je nastaven</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1123"/>
        <source>Saving</source>
        <translation>Ukládání</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="969"/>
        <source>When allowed, Datovka will use the following directories
as paths for loading and saving files in all accounts. 
Per-account path remembering will be disabled.</source>
        <translation>V případě zvolení bude Datovka používat zvolené adresáře
k ukládání a otevírání zpráv pro všechny účty.
Pamatování cest pro jednotlivé účty bude vypnuto.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1138"/>
        <source>When saving all attachments also</source>
        <translation>Při ukládání všech příloh také proveď</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="954"/>
        <source>Directories</source>
        <translation>Adresáře</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="974"/>
        <source>Use global path settings</source>
        <translation>Používat nastavené globální cesty</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="992"/>
        <source>Path for saving of attachments is currently set on:</source>
        <translation>Cesta pro ukládání příloh je aktuálně nastavena na:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1024"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1084"/>
        <source>Change</source>
        <translation>Změnit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1052"/>
        <source>Path for adding of file to attachments is currently set on:</source>
        <translation>Cesta pro přidávání příloh je aktuálně nastavena na:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1135"/>
        <source>These actions will be performed when saving all message attachments.</source>
        <translation>Tyto akce budou prováděny současně s ukládáním všech příloh.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1159"/>
        <source>Save signed message to ZFO</source>
        <translation>Ulož podepsanou zprávu jako ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1193"/>
        <source>Save signed acceptance info to ZFO</source>
        <translation>Ulož podepsanou doručenku jako ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1210"/>
        <source>Save acceptance info to PDF</source>
        <translation>Ulož doručenku jako PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1176"/>
        <source>Save message envelope to PDF</source>
        <translation>Ulož obálku zprávy jako PDF</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="108"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="125"/>
        <source>Allows to set a time interval for automatically
synchronise all accounts in the background.</source>
        <translation>Umožňuje nastavit interval pro periodickou
synchronizaci všech účtů na pozadí aplikace.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="230"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="249"/>
        <source>Allows to set a timeout interval for network connection.
If you have a slow network connection or you cannot 
download complete messages, here you can increase
connection timeout.</source>
        <translation>Umožňuje nastavit časový limit pro držení sítového připojení.
Jestli že máte pomalé připojení do internetu nebo nemůžete
stáhnou kompletní zprávy, zde můžete zvýšit časový limit pro
udržení spojení.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="319"/>
        <source>Mark message as read settings</source>
        <translation>Nastavení označování přečtených zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="336"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="353"/>
        <source>Allows to set a timeout interval for
automatically marking message as read.</source>
        <translation>Umožňuje nastavit časový interval pro
automatické označení zprávy jako přečtené.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="340"/>
        <source>Automatically mark message as read after</source>
        <translation>Automaticky označit zprávu jako lokálně přečtenou za</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="376"/>
        <source>seconds</source>
        <translation>sekund</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="666"/>
        <source>Message time stamps expiring within the selected numer
of days will be included into the expiration notification summary.</source>
        <translation>Časová razítka zpráv expirujících v průběhu zvoleného počtu
dnů budou zahrnuta do upozornění o expiraci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="670"/>
        <source>Message time stamp expiration</source>
        <translation>Expirace časových razítek zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="682"/>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="699"/>
        <source>Allows to set a interval how many days before expiring 
of timestamp will be message included in the check results.</source>
        <translation>Umožňuje nastavit časový interval s jakým předstihem před vypršením
platnosti časového razítka bude zpráva zahrnuta do výsledku kontroly.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="686"/>
        <source>Check for time stamps expiring within</source>
        <translation>Zjišťovat časová razítka expirující do</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="854"/>
        <source>Interface</source>
        <translation>Rozhraní</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="866"/>
        <source>Style of toolbar buttons</source>
        <translation>Vzhled tlačítek v panelu nástrojů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="878"/>
        <source>Only icons will be displayed in the toolbar.</source>
        <translation>Pouze ikony budou zobrazeny v nástrojovém panelu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="881"/>
        <source>Only display the icon</source>
        <translation>Zobrazovat pouze ikony</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="894"/>
        <source>Icons and text beside them will be displayed in the toolbar.</source>
        <translation>Ikony a popisky vedle nich budou zobrazeny v nástrojovém panelu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="897"/>
        <source>The text appears beside the icon</source>
        <translation>Zobrazovat text za ikonou</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="910"/>
        <source>Icons and text under them will be displayed in the toolbar.</source>
        <translation>Ikony a popisky pod nimi budou zobrazeny v nástrojovém panelu.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="913"/>
        <source>The text appears under the icon</source>
        <translation>Zobrazovat text pod ikonou</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="930"/>
        <source>Note: toolbar settings will not be applied until you restart the application.</source>
        <translation>Poznámka: nastavení panelu nástrojů nebude aktivní dokud nerestartujete aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1021"/>
        <source>Select a new path for saving of attachments.</source>
        <translation>Vyberte novou cestu pro ukládání příloh.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1081"/>
        <source>Select a new path for adding of files to attachments.</source>
        <translation>Vyberte novou cestu pro přidávání souborů do příloh.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1155"/>
        <source>When saving all attachments also save
signed message as ZFO to the same path.</source>
        <translation>Uložit také podepsanou zprávu jako ZFO
soubor do cesty, kam se ukládají přílohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1172"/>
        <source>When saving all attachments also save
message envelope as PDF to the same path.</source>
        <translation>Uložit také obálku zprávy jako PDF
soubor do cesty, kam se ukládají přílohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1189"/>
        <source>When saving all attachments also save
signed acceptance info as ZFO to the same path.</source>
        <translation>Uložit také podepsanou doručenku jako ZFO
soubor do cesty, kam se ukládají přílohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1206"/>
        <source>When saving all attachments also save
acceptance info as PDF to the same path.</source>
        <translation>Uložit také informace o doručení jako PDF
soubor do cesty, kam se ukládají přílohy.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1558"/>
        <source>message ID</source>
        <translation>ID zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1572"/>
        <source>message subject (separated by &quot;-&quot;)</source>
        <translation>předmět zprávy (oddělen pomocí &quot;-&quot;)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1586"/>
        <source>databox ID</source>
        <translation>ID schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1614"/>
        <source>attachment filename (with suffix)</source>
        <translation>jméno souboru přílohy (s příponou)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1642"/>
        <source>user ID</source>
        <translation>ID uživatele</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1656"/>
        <source>name of sender (separated by &quot;-&quot;)</source>
        <translation>jméno odesílatele (oddělené &quot;-&quot;)</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1690"/>
        <source>Example:</source>
        <translation>Příklad:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1718"/>
        <source>means</source>
        <translation>znamená</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1761"/>
        <source>Note: Illegal characters in the name of file will be replaced.</source>
        <translation>Poznámka: Nepovolené znaky v názvu souboru budou automaticky nahrazeny.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1788"/>
        <source>Save acceptance info for every attachment file. Format:</source>
        <translation>Uložit doručenku pro každý soubor přílohy. Formát:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1827"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1839"/>
        <source>Application language</source>
        <translation>Jazyk aplikace</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1852"/>
        <source>Use system language</source>
        <translation>Použít jazyk systému</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1857"/>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1862"/>
        <source>English</source>
        <translation>Angličtina</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_preferences.ui" line="1877"/>
        <source>Note: language settings will not be applied until you restart the application.</source>
        <translation>Poznámka: nastavení jazyka nebude aktivní dokud nerestartujete aplikaci.</translation>
    </message>
</context>
<context>
    <name>Proxysets</name>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="32"/>
        <source>Proxy Settings</source>
        <translation>Nastavení proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="45"/>
        <source>The HTTPS protocol is used when accessing the Databox server while HTTP is used to download the certificate revocation list and information about new Datovka versions. </source>
        <translation>Protokol HTTPS je využíván k přístupu na server Datových schránek. Protokol HTTP je použit ke stažení seznamu zneplatněných certifikátů a informací o nových verzích Datovky.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="64"/>
        <source>HTTPS proxy</source>
        <translation>HTTPS proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="76"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="79"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="290"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="293"/>
        <source>No proxy</source>
        <translation>Bez proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="92"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="95"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="306"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="309"/>
        <source>Automatic proxy detection</source>
        <translation>Automatická detekce proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="125"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="128"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="339"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="342"/>
        <source>Manual proxy settings</source>
        <translation>Ruční nastavení proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="143"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="357"/>
        <source>Proxy hostname:</source>
        <translation>Jméno počítače proxy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="156"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="370"/>
        <source>Enter your proxy hostname</source>
        <translation>Zadejte název vašeho proxy hostitele</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="169"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="383"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="188"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="414"/>
        <source>Enter port</source>
        <translation>Zadejte číslo portu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="210"/>
        <source>Show HTTPS proxy authentication</source>
        <translation>Zobrazit HTTPS proxy přihlašovací údaje</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="213"/>
        <source>Show HTTPS proxy authentication.</source>
        <translation>Zobrazit HTTPS proxy přihlašovací údaje.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="229"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="455"/>
        <source>Username:</source>
        <translation>Uživatelské jméno:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="236"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="462"/>
        <source>Enter your username</source>
        <translation>Zadejte své uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="249"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="475"/>
        <source>Password:</source>
        <translation>Heslo:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="256"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="482"/>
        <source>Enter your password</source>
        <translation>Zadejte heslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="278"/>
        <source>HTTP proxy</source>
        <translation>HTTP proxy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="436"/>
        <location filename="../src/gui/ui/dlg_proxysets.ui" line="439"/>
        <source>Show HTTP proxy authentication.</source>
        <translation>Zobrazit HTTP proxy přihlašovací údaje.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/common.cpp" line="330"/>
        <source>Postal data message</source>
        <translation>Poštovní datová zpráva</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="333"/>
        <source>Initializing postal data message</source>
        <translation>Inicializační poštovní datová zpráva</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="336"/>
        <source>Reply postal data message</source>
        <translation>Odpovědní poštovní datová zpráva</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="339"/>
        <source>Initializing postal data message - expired</source>
        <translation>Inicializační poštovní datová zpráva - propadlá</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="343"/>
        <source>Initializing postal data message - used</source>
        <translation>Inicializační poštovní datová zpráva - použitá</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="65"/>
        <source>Primary user</source>
        <translation>Oprávněná osoba</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="68"/>
        <source>Entrusted user</source>
        <translation>Pověřená osoba</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="71"/>
        <source>Administrator</source>
        <translation>Administrátor</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="74"/>
        <source>Official</source>
        <translation>Oficiální</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="77"/>
        <source>Virtual</source>
        <translation>Virtuální</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="447"/>
        <source>File &apos;%1&apos; does not contain a valid database filename.</source>
        <translation>Databázový soubor &apos;%1&apos; neobsahuje správny název.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="453"/>
        <source>File &apos;%1&apos; does not contain a valid username in the database filename.</source>
        <translation>Databáze &apos;%1&apos; neobsahuje správné uživatelské jméno v názvu souboru.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="467"/>
        <source>File &apos;%1&apos; does not contain valid year in the database filename.</source>
        <translation>Databáze &apos;%1&apos; neobsahuje správný rok v názvu souboru.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="476"/>
        <location filename="../src/common.cpp" line="482"/>
        <source>File &apos;%1&apos; does not contain valid database filename.</source>
        <translation>Databáze &apos;%1&apos; neobsahuje správný název souboru.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="492"/>
        <source>File &apos;%1&apos; does not contain a valid account type flag or filename has wrong format.</source>
        <translation>Databáze &apos;%1&apos; neobsahuje správný typ účtu nebo názvu souboru má špatný formát.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="498"/>
        <source>File &apos;%1&apos; does not contain a valid message database or filename has wrong format.</source>
        <translation>Soubor &apos;%1&apos; neobsahuje validní databázi zpráv nebo název souboru má špatný formát.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="524"/>
        <source>Message did not pass through AV check; infected paper deleted; final status before deletion.</source>
        <translation>Zpráva neprošla AV kontrolou; nakažená písemnost je smazána; konečný stav zprávy před smazáním.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="529"/>
        <source>Message handed into ISDS (delivery time recorded).</source>
        <translation>Zpráva dodána do ISDS (zapsán čas dodání).</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="538"/>
        <source>10 days have passed since the delivery of the public message which has not been accepted by logging-in (assumption of acceptance through fiction in non-OVM DS); this state cannot occur for commercial messages.</source>
        <translation>Uplynulo 10 dnů od dodání veřejné zprávy, která dosud nebyla doručena přihlášením (předpoklad doručení fikcí u ne-OVM DS); tento stav nemůže nastat v případě poštovních datových zpráv.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="553"/>
        <source>Message has been read (on the portal or by ESS action).</source>
        <translation>Zpráva byla přečtena (na portále nebo akcí ESS).</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="511"/>
        <source>Message has been submitted (has been created in ISDS)</source>
        <translation>Zpráva byla podána (vznikla v ISDS)</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="517"/>
        <source>Data message including its attachments signed with time-stamp.</source>
        <translation>Datová zpráva včetně písemností podepsána časovým razítkem.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="548"/>
        <source>A person authorised to read this message has logged in -- delivered message has been accepted.</source>
        <translation>Osoba oprávněná číst tuto zprávu se přihlásila -- dodaná zpráva byla doručena.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="561"/>
        <source>Message marked as undeliverable because the target DS has been made inaccessible.</source>
        <translation>Zpráva byla označena jako nedoručitelná, protože DS adresáta byla zpětně znepřístupněna.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="569"/>
        <source>Message content deleted, envelope including hashes has been moved into archive.</source>
        <translation>Zpráva byla označena jako nedoručitelná, protože DS adresáta byla zpětně znepřístupněna.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="574"/>
        <source>Message resides in data vault.</source>
        <translation>Zpráva je v Datovém trezoru.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="227"/>
        <source>Full control</source>
        <translation>Plný přístup</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="229"/>
        <source>Restricted control</source>
        <translation>Omezený přístup</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="235"/>
        <source>download and read incoming DM</source>
        <translation>stahovat a číst došlé DZ</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="240"/>
        <source>download and read DM sent into own hands</source>
        <translation>stahovat a číst DZ určené do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="246"/>
        <source>create and send DM, download sent DM</source>
        <translation>vytvářet a odesílat DZ, stahovat odeslané DZ</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="252"/>
        <source>retrieve DM lists, delivery and acceptance reports</source>
        <translation>načítat seznamy DZ, dodejky a doručenky</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="259"/>
        <source>search for data boxes</source>
        <translation>vyhledávat DS</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="263"/>
        <source>manage the data box</source>
        <translation>spravovat DS</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="269"/>
        <source>read message in data vault</source>
        <translation>číst zprávy v datovém trezoru</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="274"/>
        <source>erase messages from data vault</source>
        <translation>mazat zprávy v datovém trezoru</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="196"/>
        <source>System ISDS</source>
        <translation>Systém ISDS</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="198"/>
        <source>Public authority</source>
        <translation>Orgán veřejné moci (OVM)</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="200"/>
        <source>Legal person</source>
        <translation>Právnická osoba (PO)</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="202"/>
        <source>Self-employed person</source>
        <translation>Podnikající fyzická osoba (OSVČ)</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="204"/>
        <source>Natural person</source>
        <translation>Fyzická osoba (FO)</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="367"/>
        <source>The data box is accessible. It is possible to send messages into it. It can be looked up on the Portal.</source>
        <translation>Datová schránka je přístupná, lze do ní dodávat zprávy, na Portále lze vyhledat.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="372"/>
        <source>The data box is temporarily inaccessible (at own request). It may be made accessible again at some point in the future.</source>
        <translation>Datová schránka je dočasně znepřístupněna (na vlastní žádost), může být později opět zpřístupněna.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="377"/>
        <source>The data box is so far inactive. The owner of the box has to log into the web interface at first in order to activate the box.</source>
        <translation>Datová schránka je dosud neaktivní. Vlastník schránky se musí poprvé přihlásit do webového rozhraní, aby došlo k aktivaci schránky.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="382"/>
        <source>The data box is permanently inaccessible. It is waiting to be deleted (but it may be made accessible again).</source>
        <translation>Datová schránka je trvale znepřístupněna, čeká na smazání (může být opět zpřístupněna).</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="387"/>
        <source>The data box has been deleted (none the less it exists in ISDS).</source>
        <translation>Datová schránka byla smazána (přesto existuje v ISDS).</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="391"/>
        <source>An error occurred while checking the status.</source>
        <translation>Došlo k chybě při zjišťování stavu.</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="330"/>
        <source>File name</source>
        <translation>Název souboru</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="78"/>
        <source>Data box ID</source>
        <translation>ID datové schránky</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="79"/>
        <source>Data box type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="80"/>
        <location filename="../src/io/db_tables.cpp" line="159"/>
        <source>IČ</source>
        <translation>IČ</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="81"/>
        <location filename="../src/io/db_tables.cpp" line="148"/>
        <source>Given name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="82"/>
        <location filename="../src/io/db_tables.cpp" line="149"/>
        <source>Middle name</source>
        <translation>Prostření jméno</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="83"/>
        <location filename="../src/io/db_tables.cpp" line="150"/>
        <source>Surname</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="84"/>
        <location filename="../src/io/db_tables.cpp" line="151"/>
        <source>Surname at birth</source>
        <translation>Rodné příjmení</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="85"/>
        <location filename="../src/io/db_tables.cpp" line="160"/>
        <source>Company name</source>
        <translation>Název firmy</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="86"/>
        <location filename="../src/io/db_tables.cpp" line="158"/>
        <source>Date of birth</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="87"/>
        <source>City of birth</source>
        <translation>Místo narození</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="88"/>
        <source>County of birth</source>
        <translation>Okres narození</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="89"/>
        <source>State of birth</source>
        <translation>Stát narození</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="90"/>
        <location filename="../src/io/db_tables.cpp" line="162"/>
        <source>City of residence</source>
        <translation>Sídlo - město</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="91"/>
        <location filename="../src/io/db_tables.cpp" line="161"/>
        <source>Street of residence</source>
        <translation>Sídlo - ulice</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="92"/>
        <location filename="../src/io/db_tables.cpp" line="154"/>
        <source>Number in street</source>
        <translation>Číslo orientační</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="93"/>
        <location filename="../src/io/db_tables.cpp" line="155"/>
        <source>Number in municipality</source>
        <translation>Číslo popisné</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="94"/>
        <location filename="../src/io/db_tables.cpp" line="156"/>
        <location filename="../src/io/db_tables.cpp" line="163"/>
        <source>Zip code</source>
        <translation>PSČ</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="95"/>
        <location filename="../src/io/db_tables.cpp" line="164"/>
        <source>State of residence</source>
        <translation>Sídlo - stát</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="96"/>
        <source>Nationality</source>
        <translation>Státní občanství</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="99"/>
        <source>Databox state</source>
        <translation>Stav schránky</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="100"/>
        <source>Effective OVM</source>
        <translation>Efektivní OVM</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="101"/>
        <source>Open addressing</source>
        <translation>Otevřené adresování</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="146"/>
        <source>User type</source>
        <translation>Typ uživatele</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="147"/>
        <source>Permissions</source>
        <translation>Oprávnění</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="152"/>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="153"/>
        <source>Street</source>
        <translation>Ulice</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="157"/>
        <source>State</source>
        <translation>Stát</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="258"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="262"/>
        <location filename="../src/io/message_db.cpp" line="1020"/>
        <location filename="../src/io/message_db.cpp" line="1384"/>
        <location filename="../src/io/message_db.cpp" line="1617"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="263"/>
        <source>Sender address</source>
        <translation>Adresa odesílatele</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="265"/>
        <location filename="../src/io/message_db.cpp" line="1056"/>
        <location filename="../src/io/message_db.cpp" line="1396"/>
        <location filename="../src/io/message_db.cpp" line="1626"/>
        <source>Recipient</source>
        <translation>Příjemce</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="266"/>
        <source>Recipient address</source>
        <translation>Adresa příjemce</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="273"/>
        <location filename="../src/io/message_db.cpp" line="1466"/>
        <location filename="../src/io/message_db.cpp" line="1692"/>
        <source>To hands</source>
        <translation>K rukám</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="281"/>
        <source>Section</source>
        <translation>Sekce</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="285"/>
        <source>Acceptance through fiction enabled</source>
        <translation>Doručení fikcí povoleno</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="287"/>
        <source>Delivery time</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="288"/>
        <source>Acceptance time</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="275"/>
        <source>Your reference number</source>
        <translation>Vaše číslo jednací</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="276"/>
        <source>Our reference number</source>
        <translation>Naše číslo jednací</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="277"/>
        <source>Your file mark</source>
        <translation>Vaše spisová značka</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="278"/>
        <source>Our file mark</source>
        <translation>Naše spisová značka</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="279"/>
        <source>Law</source>
        <translation>Zákon</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="280"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="282"/>
        <source>Paragraph</source>
        <translation>Odstavec</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="283"/>
        <source>Letter</source>
        <translation>Písmeno</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="284"/>
        <source>Personal delivery</source>
        <translation>Doručení do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="289"/>
        <location filename="../src/io/message_db.cpp" line="1128"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="290"/>
        <source>Attachment size</source>
        <translation>Velikost příloh</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="333"/>
        <source>Mime type</source>
        <translation>Typ MIME</translation>
    </message>
    <message>
        <location filename="../src/io/db_tables.cpp" line="509"/>
        <source>Read locally</source>
        <translation>Přečteno lokálně</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="146"/>
        <source>Data box application</source>
        <translation>Aplikace pro datové schránky</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="151"/>
        <source>Use &lt;conf-subdir&gt; subdirectory for configuration.</source>
        <translation>Použít &lt;conf-subdir&gt; podadresář pro konfiguraci.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="153"/>
        <source>conf-subdir</source>
        <translation>conf-subdir</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="157"/>
        <source>On start load &lt;conf&gt; file.</source>
        <translation>Po spuštění načíst &lt;conf&gt; soubor.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="158"/>
        <location filename="../src/main.cpp" line="163"/>
        <source>conf</source>
        <translation>conf</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="162"/>
        <source>On stop save &lt;conf&gt; file.</source>
        <translation>Po ukončení uložit &lt;conf&gt; soubor.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="167"/>
        <source>Log messages to &lt;file&gt;.</source>
        <translation>Zaznamenávat výpisy do &lt;souboru&gt;.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="168"/>
        <source>file</source>
        <translation>soubor</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="172"/>
        <source>Set verbosity of logged messages to &lt;level&gt;. Default is </source>
        <translation>Nastavit podrobnost ladicích zpráv na &lt;level&gt;. Výchozí je </translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="174"/>
        <location filename="../src/main.cpp" line="188"/>
        <source>level</source>
        <translation>úroveň</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="186"/>
        <source>Set debugging verbosity to &lt;level&gt;. Default is </source>
        <translation>Nastavit podrobnost ladicích informací na &lt;level&gt;. Výchozí je </translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="196"/>
        <source>Service: connect to isds and login into databox.</source>
        <translation>Služba: připojit se k datové schránce.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="218"/>
        <source>Service: download acceptance info of message with signature and time stamp of MV.</source>
        <translation>Služba: stáhnout informace o doručení zprávy s podpisem a časovým razítkem MV.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="571"/>
        <source>Application is loading...</source>
        <translation>Aplikace se načítá...</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="197"/>
        <location filename="../src/main.cpp" line="203"/>
        <location filename="../src/main.cpp" line="208"/>
        <location filename="../src/main.cpp" line="214"/>
        <location filename="../src/main.cpp" line="220"/>
        <location filename="../src/main.cpp" line="243"/>
        <source>string-of-parameters</source>
        <translation>řetězec-parametrů</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="201"/>
        <source>Service: download list of received/sent messages from ISDS.</source>
        <translation>Služba: stáhnout seznam přijatých a odeslaných zpráv z ISDS.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="207"/>
        <source>Service: create and send a new message to ISDS.</source>
        <translation>Služba: vytvořit a odeslat novou zprávu do ISDS.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="212"/>
        <source>Service: download complete message with signature and time stamp of MV.</source>
        <translation>Služba: stáhnout kompletní zprávu s podpisem a časovým razítkem MV.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="224"/>
        <source>Service: get information about user (role, privileges, ...).</source>
        <translation>Služba: stáhnout informace o uživateli (role, oprávnění, ...).</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="230"/>
        <source>Service: get information about owner and its databox.</source>
        <translation>Služba: stáhnout informace o vlastníkovi a jeho datové schránce.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="236"/>
        <source>Service: get list of messages where attachment missing (local database only).</source>
        <translation>Služba: získat seznam zpráv bez přílohy (pouze lokální databaze).</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="242"/>
        <source>Service: find a databox via several parameters.</source>
        <translation>Služba: vyhledat datovou schránku na ISDS.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="248"/>
        <source>ZFO file to be viewed.</source>
        <translation>Zobrazovaný ZFO soubor.</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1589"/>
        <source>Advice of Acceptance</source>
        <translation>Doručenka</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1359"/>
        <location filename="../src/io/message_db.cpp" line="1591"/>
        <source>Message ID:</source>
        <translation>ID zprávy:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="521"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="559"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="632"/>
        <location filename="../src/io/message_db.cpp" line="1388"/>
        <location filename="../src/io/message_db.cpp" line="1400"/>
        <location filename="../src/io/message_db.cpp" line="1622"/>
        <location filename="../src/io/message_db.cpp" line="1631"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1410"/>
        <location filename="../src/io/message_db.cpp" line="1636"/>
        <source>General Information</source>
        <translation>Obecné informace</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="548"/>
        <location filename="../src/io/db_tables.cpp" line="274"/>
        <location filename="../src/io/message_db.cpp" line="1009"/>
        <location filename="../src/io/message_db.cpp" line="1412"/>
        <location filename="../src/io/message_db.cpp" line="1638"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1428"/>
        <location filename="../src/io/message_db.cpp" line="1654"/>
        <source>paragraph</source>
        <translation>odstavec</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1430"/>
        <location filename="../src/io/message_db.cpp" line="1656"/>
        <source>letter</source>
        <translation>písmeno</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1433"/>
        <location filename="../src/io/message_db.cpp" line="1659"/>
        <source>Delegation</source>
        <translation>Zmocnění</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1436"/>
        <location filename="../src/io/message_db.cpp" line="1443"/>
        <location filename="../src/io/message_db.cpp" line="1449"/>
        <location filename="../src/io/message_db.cpp" line="1456"/>
        <location filename="../src/io/message_db.cpp" line="1462"/>
        <location filename="../src/io/message_db.cpp" line="1662"/>
        <location filename="../src/io/message_db.cpp" line="1669"/>
        <location filename="../src/io/message_db.cpp" line="1675"/>
        <location filename="../src/io/message_db.cpp" line="1682"/>
        <location filename="../src/io/message_db.cpp" line="1688"/>
        <source>Not specified</source>
        <translation>Nebylo zadáno</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1440"/>
        <location filename="../src/io/message_db.cpp" line="1666"/>
        <source>Our ref.number</source>
        <translation>Naše číslo jednací</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1447"/>
        <location filename="../src/io/message_db.cpp" line="1673"/>
        <source>Our doc.id</source>
        <translation>Naše spisová značka</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1453"/>
        <location filename="../src/io/message_db.cpp" line="1679"/>
        <source>Your ref.number</source>
        <translation>Vaše číslo jednací</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1460"/>
        <location filename="../src/io/message_db.cpp" line="1686"/>
        <source>Your doc.id</source>
        <translation>Vaše spisová značka</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1470"/>
        <location filename="../src/io/message_db.cpp" line="1480"/>
        <location filename="../src/io/message_db.cpp" line="1695"/>
        <location filename="../src/io/message_db.cpp" line="1705"/>
        <source>yes</source>
        <translation>ano</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1472"/>
        <location filename="../src/io/message_db.cpp" line="1478"/>
        <location filename="../src/io/message_db.cpp" line="1697"/>
        <location filename="../src/io/message_db.cpp" line="1703"/>
        <source>no</source>
        <translation>ne</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1474"/>
        <location filename="../src/io/message_db.cpp" line="1699"/>
        <source>Personal Delivery</source>
        <translation>Osobní doručení</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1552"/>
        <source>List of attachments</source>
        <translation>Seznam příloh</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1714"/>
        <source>Delivery/Acceptance Information</source>
        <translation>Informace o dodání/doručení</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1403"/>
        <location filename="../src/io/message_db.cpp" line="1716"/>
        <source>Delivery</source>
        <translation>Dodání</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1030"/>
        <source>Databox type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1324"/>
        <source>Download the complete message in order to verify its time stamp.</source>
        <translation>Stáhněte kompletní zprávu pro ověření jejího časového razítka.</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1483"/>
        <location filename="../src/io/message_db.cpp" line="1708"/>
        <source>Prohibit Acceptance through Fiction</source>
        <translation>Zakázat doručení fikcí</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1719"/>
        <source>Acceptance</source>
        <translation>Doručení</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1178"/>
        <location filename="../src/io/message_db.cpp" line="1741"/>
        <source>Events</source>
        <translation>Události</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1023"/>
        <source>Sender Databox ID</source>
        <translation>ID datové schránky odesílatele</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1059"/>
        <source>Recipient Databox ID</source>
        <translation>ID datové schránky příjemce</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="614"/>
        <location filename="../src/io/message_db.cpp" line="1751"/>
        <source>Time</source>
        <translation>Čas</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1357"/>
        <source>Envelope</source>
        <translation>Obálka</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1389"/>
        <location filename="../src/io/message_db.cpp" line="1401"/>
        <source>Databox ID</source>
        <translation>ID schránky</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1391"/>
        <source>Databox Type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1209"/>
        <location filename="../src/io/message_db.cpp" line="1227"/>
        <location filename="../src/io/message_db.cpp" line="1233"/>
        <location filename="../src/io/message_db.cpp" line="1504"/>
        <source>Attachments</source>
        <translation>Přílohy</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="990"/>
        <source>Identification</source>
        <translation>Identifikace</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="992"/>
        <source>Message ID</source>
        <translation>ID zprávy</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1013"/>
        <source>Message type</source>
        <translation>Typ zprávy</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1033"/>
        <source>Sender Address</source>
        <translation>Adresa odesílatele</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1047"/>
        <source>Message author</source>
        <translation>Odesílající osoba</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1061"/>
        <source>Recipient Address</source>
        <translation>Adresa příjemce</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1211"/>
        <source>(downloaded and ready)</source>
        <translation>(staženo a připraveno)</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1228"/>
        <source>not downloaded yet, ~</source>
        <translation>ještě nestaženo, ~</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1230"/>
        <source> KB; use &apos;Download&apos; to get them.</source>
        <translation> KB; použijte &apos;Stáhnout&apos; k jejich získání.</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1234"/>
        <source>(not available)</source>
        <translation>(nestažené)</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1253"/>
        <source>Signature</source>
        <translation>Podpis</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1258"/>
        <location filename="../src/io/message_db.cpp" line="1269"/>
        <location filename="../src/io/message_db.cpp" line="1275"/>
        <source>Message signature</source>
        <translation>Podpis zprávy</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1259"/>
        <location filename="../src/io/message_db.cpp" line="1298"/>
        <source>Not present</source>
        <translation>Není k dispozici</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1265"/>
        <source>Download the complete message in order to verify its signature.</source>
        <translation>Stáhněte kompletní zprávu pro ověření jejího podpisu.</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1321"/>
        <source>Time stamp</source>
        <translation>Časové razítko</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="410"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="419"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="433"/>
        <location filename="../src/io/message_db.cpp" line="1270"/>
        <location filename="../src/io/message_db.cpp" line="1281"/>
        <location filename="../src/io/message_db.cpp" line="1310"/>
        <source>Invalid</source>
        <translation>Neplatný</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="411"/>
        <location filename="../src/io/message_db.cpp" line="1271"/>
        <source>Message signature and content do not correspond!</source>
        <translation>Podpis zprávy a její obsah si neodpovídají!</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="378"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="382"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="419"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="605"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="408"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="417"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="433"/>
        <location filename="../src/io/message_db.cpp" line="1276"/>
        <location filename="../src/io/message_db.cpp" line="1281"/>
        <location filename="../src/io/message_db.cpp" line="1309"/>
        <source>Valid</source>
        <translation>Platný</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="432"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="480"/>
        <location filename="../src/gui/dlg_view_zfo.cpp" line="423"/>
        <location filename="../src/io/message_db.cpp" line="1284"/>
        <source>Certificate revocation check is turned off!</source>
        <translation>Kontrola zneplatnění certifikátu je vypnutá!</translation>
    </message>
    <message>
        <location filename="../src/io/message_db.cpp" line="1289"/>
        <source>Signing certificate</source>
        <translation>Podepisující certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_about.cpp" line="75"/>
        <source>Depends on libraries:</source>
        <translation>Závisí na knihovnách:</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="364"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="408"/>
        <source>Message signature is not present.</source>
        <translation>Podpis zprávy není přítomný.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="345"/>
        <location filename="../src/io/message_db.cpp" line="1118"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="342"/>
        <location filename="../src/io/message_db.cpp" line="1117"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="349"/>
        <source>Information not available</source>
        <translation>Informace není dostupná</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="410"/>
        <source>Cannot check signing certificate.</source>
        <translation>Nelze zkontrolovat podpisový certifikát.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="451"/>
        <source>Trusted certificates were found</source>
        <translation>Důvěryhodné certifikáty nalezeny</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="456"/>
        <source>Signing algorithm supported</source>
        <translation>Podepisující algoritmus je podporován</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="462"/>
        <source>Trusted parent certificate found</source>
        <translation>Důvěryhodný rodičovský certifikát nalezen</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="467"/>
        <source>Certificate time validity is ok</source>
        <translation>Časová platnost certifikátu je v pořádku</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="476"/>
        <source>Certificate was not revoked</source>
        <translation>Certifikát nebyl revokován</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="486"/>
        <source>Certificate signature verified</source>
        <translation>Podpis certifikátu ověřen</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="498"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="500"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="566"/>
        <source>Serial number</source>
        <translation>Sériové číslo</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="505"/>
        <source>Signature algorithm</source>
        <translation>Algoritmus podpisu</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="508"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="621"/>
        <source>Issuer</source>
        <translation>Vydal</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="514"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="553"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="624"/>
        <source>Organisation</source>
        <translation>Organizace</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="527"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="572"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="637"/>
        <source>Country</source>
        <translation>Země</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="531"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="642"/>
        <source>Validity</source>
        <translation>Platnost</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="540"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="645"/>
        <source>Valid from</source>
        <translation>Platnost od</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="543"/>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="649"/>
        <source>Valid to</source>
        <translation>Platnost do</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="628"/>
        <source>Organisational unit</source>
        <translation>Organizační jednotka</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_signature_detail.cpp" line="596"/>
        <source>Time stamp not present.</source>
        <translation>Časové razítko není přítomno.</translation>
    </message>
    <message>
        <location filename="../src/common.cpp" line="318"/>
        <source>Created using Datovka</source>
        <translation>Vytvořeno Datovkou</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="218"/>
        <source>This file (message) has not been inserted into the database because the corresponding database file could not be accessed or created.</source>
        <translation>Tento soubor (zpráva) nebyl vložen do databáze, protože odpovídající databázový soubor nemohl být otevřen, nebo vytvořen.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="226"/>
        <source>Message &apos;%1&apos; already exists in the local database, account &apos;%2&apos;.</source>
        <translation>Zpráva &apos;%1&apos; již v lokální databázi existuje, účet &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="240"/>
        <location filename="../src/worker/task_import_zfo.cpp" line="398"/>
        <source>Couldn&apos;t read data from file for authentication on the ISDS server.</source>
        <translation>Data ze souboru nemohla být přečtena za účelem ověření serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="245"/>
        <location filename="../src/worker/task_import_zfo.cpp" line="403"/>
        <source>Error contacting ISDS server.</source>
        <translation>Chyba při navazování spojení se serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="250"/>
        <source>Message &apos;%1&apos; could not be authenticated by ISDS server.</source>
        <translation>Zpráva &apos;%1&apos; nemohla být ověřena serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="262"/>
        <location filename="../src/worker/task_import_zfo.cpp" line="418"/>
        <source>File has not been imported because an error was detected during insertion process.</source>
        <translation>Soubor nebyl importován, protože byla zjištěna chyba v průběhu ukládání.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="267"/>
        <source>Imported message &apos;%1&apos;, account &apos;%2&apos;.</source>
        <translation>Importována zpráva &apos;%1&apos;, účet &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/io/imports.cpp" line="63"/>
        <location filename="../src/worker/task_import_zfo.cpp" line="285"/>
        <location filename="../src/worker/task_import_zfo.cpp" line="442"/>
        <source>Wrong ZFO format. This file does not contain correct data for import.</source>
        <translation>Špatný formát ZFO. Soubor neobsahuje platná data pro import.</translation>
    </message>
    <message>
        <location filename="../src/io/imports.cpp" line="87"/>
        <source>The selection does not contain any valid ZFO file.</source>
        <translation>Vybraný soubor neobsahuje platný ZFO formát.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="377"/>
        <source>This file (acceptance info) has not been inserted into database because there isn&apos;t any related message with id &apos;%1&apos; in the databases.</source>
        <translation>Tento soubor (doručenka) nebyl vložen do databáze, protože v databázi nebyla nalezena žádná odpovídající zpráva s id &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="384"/>
        <source>Acceptance info for message &apos;%1&apos; already exists in the local database, account &apos;%2&apos;.</source>
        <translation>Doručenka &apos;%1&apos; již v lokální databázi existuje, účet &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="408"/>
        <source>Acceptance info for message &apos;%1&apos; could not be authenticated by ISDS server.</source>
        <translation>Doručenka zprávy &apos;%1&apos; nemohla být ověřena serverem ISDS.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_zfo.cpp" line="423"/>
        <source>Imported acceptance info for message &apos;%1&apos;, account &apos;%2&apos;.</source>
        <translation>Importována doručenka zprávy &apos;%1&apos;, účet &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_vacuum_db_set.cpp" line="154"/>
        <source>Could not determine database directory</source>
        <translation>Nelze určit adresář uložení databáze</translation>
    </message>
    <message>
        <location filename="../src/worker/task_vacuum_db_set.cpp" line="160"/>
        <source>Not enough space on device where &apos;%1&apos; resides.</source>
        <translation>Nedostatek místa na zařízení, kde se nachází &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_vacuum_db_set.cpp" line="166"/>
        <source>Calling vacuum on database set failed.</source>
        <translation>Nastala chyba při aplikaci vakua na množitě databází.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="88"/>
        <location filename="../src/io/exports.cpp" line="244"/>
        <location filename="../src/io/exports.cpp" line="333"/>
        <source>Cannot access message database for username &quot;%1&quot;.</source>
        <translation>Nelze zpřístupnit databázový soubor zpráv pro uživatele &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="96"/>
        <source>message</source>
        <translation>zpráva</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="102"/>
        <location filename="../src/io/exports.cpp" line="108"/>
        <location filename="../src/io/exports.cpp" line="114"/>
        <location filename="../src/io/exports.cpp" line="120"/>
        <source>acceptance info</source>
        <translation>doručenka</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="126"/>
        <source>message envelope</source>
        <translation>obálka zpráv</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="133"/>
        <source>Export file type of message &quot;%1&quot; was not specified!</source>
        <translation>Nebyl specifikován výstupní typ exportované zprávy &quot;%1&quot;!</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="141"/>
        <location filename="../src/io/exports.cpp" line="256"/>
        <location filename="../src/io/exports.cpp" line="342"/>
        <source>Complete message &quot;%1&quot; missing!</source>
        <translation>Chybí kompletní zpráva &quot;%1&quot;!</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="146"/>
        <source>Export of %1 &quot;%2&quot; to %3 was not successful!</source>
        <translation>Export %1 &quot;%2&quot; do %3 nebyl úspěšný!</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="172"/>
        <source>Save %1 as file (*%2)</source>
        <translation>Uložit %1 jako soubor (*%2)</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="174"/>
        <source>File (*%1)</source>
        <translation>Soubor (*%1)</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="213"/>
        <source>Export of %1 &quot;%2&quot; to %3 was successful.</source>
        <translation>Export %1 &quot;%2&quot; do %3 byl úspěšný.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="266"/>
        <location filename="../src/io/exports.cpp" line="284"/>
        <location filename="../src/io/exports.cpp" line="352"/>
        <location filename="../src/io/exports.cpp" line="371"/>
        <source>Some files of message &quot;%1&quot; were not saved to disk!</source>
        <translation>Některé přílohy zprávy &quot;%1&quot; se nepodařilo uložit na disk!</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="300"/>
        <source>Export of message envelope &quot;%1&quot; to PDF was not successful!</source>
        <translation>Export obálky zprávy &quot;%1&quot; do souboru PDF nebyl úspěšný!</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="309"/>
        <source>Export of message envelope &quot;%1&quot; to PDF and attachments were successful.</source>
        <translation>Export obálky zprávy &quot;%1&quot; do PDF a všech příloh byl úspěšný.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="313"/>
        <source>Export of message envelope &quot;%1&quot; to PDF and attachments were not successful!</source>
        <translation>Export obálky zprávy &quot;%1&quot; do PDF nebyl úspěšný!</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="417"/>
        <source>All message attachments &quot;%1&quot; were successfully saved to target folder.</source>
        <translation>Všechny přílohy zprávy &quot;%1&quot; byly úspěšně uloženy do cílové složky.</translation>
    </message>
    <message>
        <location filename="../src/io/exports.cpp" line="421"/>
        <source>Some attachments of message &quot;%1&quot; were not successfully saved!</source>
        <translation>Některé přílohy zprávy &quot;%1&quot; nebyly úspěšně uloženy do cílové složky!</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="94"/>
        <source>Failed to open database file of target account &apos;%1&apos;</source>
        <translation>Chyba při otevírání databáze zpráv cílového účtu &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="101"/>
        <source>Message &apos;%1&apos; already exists in database for this account.</source>
        <translation>Zpráva &apos;%1 již existuje v databázi pro tento účet.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="108"/>
        <source>Message &apos;%1&apos; cannot be imported into this account. Message does not contain any valid ID of databox corresponding with this account.</source>
        <translation>Zpráva &apos;%1&apos; nebyla importována do aktuálního účtu. Zpráva neobsahuje identifikátor datové schránky korespondující s tímto účtem.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="118"/>
        <source>Message &apos;%1&apos; cannot be inserted into database of this account. An error occurred during insertion procedure.</source>
        <translation>Zpráva &apos;%1&apos; nebyla vložena do databáze aktuálního účtu. Zpráva neobsahuje validní informace pro import nebo se vyskytla neočekávaná chyba během importu.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="161"/>
        <source>Database file &apos;%1&apos; cannot import into selected account because username of account and username of database file do not correspond.</source>
        <translation>Databázový soubor &apos;%1&apos; nemůže být importován do vybraného účtu protože uživatelské jméno učtu nekoresponduje s názvem souboru.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_import_message.cpp" line="173"/>
        <source>Failed to open import database file %1&apos;.</source>
        <translation>Nepodařilo se otevřít importovaný databázový souboru &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="104"/>
        <source>Action was canceled and original database file was returned back.</source>
        <translation>Akce byla zrušena a původní databáze byla navrácena zpět.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="119"/>
        <source>Database file cannot split by years because this account already use database files split according to years.</source>
        <translation>Databázový soubor nelze rozdělit podle roků. Tento účet již má databázi rozdělenou přes jednotlivé roky.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="127"/>
        <source>Copying origin database file to selected location</source>
        <translation>Kopírování původní databáze do vybrané složky</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="131"/>
        <source>Cannot copy database file for account &apos;%1&apos; to &apos;%2&apos;. Probably not enough disk space.</source>
        <translation>Nepodařilo se zkopirovat databázový soubor účtu &apos;%1&apos; do &apos;%2&apos;. Pravděpodobně není dostatek místa na disku.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="139"/>
        <source>Database file for account &apos;%1&apos; does not exist.</source>
        <translation>Databázový soubor pro účet &apos;%1&apos; neexistuje.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="159"/>
        <source>Set of new database files for account &apos;%1&apos; could not be created.</source>
        <translation>Nové databázové soubory pro účet &apos;%1&apos; nebudou vytvořeny.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="178"/>
        <source>Creating a new database file for year %1</source>
        <translation>Vytvářím novou databázi pro rok %1</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="197"/>
        <source>Existing file &apos;%1&apos; could not be deleted.</source>
        <translation>Existující soubor &apos;%1&apos; nemohl být smazán.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="206"/>
        <source>New database file for account &apos;%1&apos; corresponds with year &apos;%2&apos; could not be created. Messages were not copied.</source>
        <translation>Nový databázový soubor účtu &apos;%1&apos; pro rok &apos;%2&apos; nemohl být vytvořen. Zprávy nebyly zkopírovány.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="216"/>
        <source>Messages correspond with year &apos;%1&apos; for account &apos;%2&apos; were not copied.</source>
        <translation>Zprávy korespondující s rokem &apos;%1&apos; pro účet &apos;%2&apos; nebyly zkopírovány.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="228"/>
        <source>Error to set and open original database for account &apos;%1&apos;.</source>
        <translation>Chyba při otevírání původní databáze u účtu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="230"/>
        <source>Action was canceled and the origin database is now used from location:
&apos;%1&apos;</source>
        <translation>Akce byla zrušena a původní databáze byla nově načtena z:
&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="236"/>
        <source>Replacing of new database files to origin database location</source>
        <translation>Přemisťování nových databázových souborů do původní složky</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="241"/>
        <source>Error when move new databases for account &apos;%1&apos;</source>
        <translation>Chyba při přesouvání nových databázových souborů pro účet &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="243"/>
        <source>Action was canceled because new databases cannot move from
&apos;%1&apos;
to origin path
&apos;%2&apos;</source>
        <translation>Akce byla zrušena protože se nepodařilo přesunout nové databáze z
&apos;%1&apos;
do původního umístění
&apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="246"/>
        <source>Probably not enough disk space. The origin database is still used.</source>
        <translation>Pravděpodobně není dostatek místa na disku. Bude použita původní databáze.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="251"/>
        <source>Deleting of old database from origin location</source>
        <translation>Mazání staré databáze z původní lokace</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="255"/>
        <source>Error when removed origin database for account &apos;%1&apos;</source>
        <translation>Nepodařilo se odstranit původní databázi pro účet &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="257"/>
        <source>Action was canceled.</source>
        <translation>Akce byla zrušena.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="259"/>
        <source>Please, remove the origin database file manually from origin location:
&apos;%1&apos;</source>
        <translation>Prosím, odstraňte původní databázový soubor ručně z adresáře:
&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="264"/>
        <source>Opening of new database files</source>
        <translation>Otevírání nových databázových souborů</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="270"/>
        <source>A problem when opening new databases for account &apos;%1&apos;</source>
        <translation>Objevil se problém s otevřením nových databázových souborů pro účet &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="272"/>
        <source>Action was done but it cannot open new database files.</source>
        <translation>Akce sice byla provedena, nelze ale otevřít nové databázové soubory.</translation>
    </message>
    <message>
        <location filename="../src/worker/task_split_db.cpp" line="274"/>
        <source>Please, restart the application.</source>
        <translation>Prosím, restartujte aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="245"/>
        <source>Tags</source>
        <translation>Tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="513"/>
        <source>Message &apos;%1&apos; does not contain data necessary for ZFO export.</source>
        <translation>Zpráva &apos;%1&apos; neobsahuje potřebná data pro export do ZFO souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="520"/>
        <source>Message &apos;%1&apos; does not contain acceptance info data necessary for ZFO export.</source>
        <translation>Zpráva &apos;%1&apos; neobsahuje data potřebná pro export doručenky do ZFO souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="527"/>
        <source>Message &apos;%1&apos; does not contain message envelope data necessary for PDF export.</source>
        <translation>Zpráva &apos;%1&apos; neobsahuje data potřebná pro export obálky do PDF souboru.</translation>
    </message>
    <message>
        <location filename="../src/gui/dlg_correspondence_overview.cpp" line="534"/>
        <source>Message &apos;%1&apos; does not contain acceptance info data necessary for PDF export.</source>
        <translation>Ke zprávě &apos;%1&apos; chybí data doručenky, která jsou potřeba pro export do PDF.</translation>
    </message>
</context>
<context>
    <name>SendMessage</name>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="23"/>
        <source>Create and send message</source>
        <translation>Vytvořit a poslat zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="42"/>
        <source>Please fill in at least the &lt;b&gt;subject&lt;/b&gt;, &lt;b&gt;one recipient&lt;/b&gt; and &lt;b&gt;one attachment&lt;/b&gt;:</source>
        <translation>Prosím, vyplňte ve formuláři &lt;b&gt;předmět&lt;/b&gt;, &lt;b&gt;alespoň jednoho příjemce&lt;/b&gt; a &lt;b&gt;alespoň jednu přílohu&lt;/b&gt;:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="116"/>
        <source>Subject:</source>
        <translation>Předmět:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="132"/>
        <source>Enter subject of the message</source>
        <translation>Zadejte smysluplný předmět Vaší zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="150"/>
        <source>This is a PDZ prepaid reply. It means that PDZ is paid by recipient.</source>
        <translation>Toto je předplacená odpověď na PDZ. To znamená, že odpověď hradí příjemce.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="167"/>
        <source>Show optional fields</source>
        <translation>Zobrazit nepovinné položky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="201"/>
        <source>Mandate:</source>
        <translation>Pověření:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="213"/>
        <source>Number of law</source>
        <translation>Číslo zákona</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="220"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="227"/>
        <source>Year of law</source>
        <translation>Rok vydání zákona</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="234"/>
        <source>§</source>
        <translation>§</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="241"/>
        <source>Section of law</source>
        <translation>Paragraf zákona</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="248"/>
        <source>paragraph</source>
        <translation>odstavec</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="255"/>
        <source>Paragraph of the corresponding section of the law</source>
        <translation>Odstavec odpovídajícího paragrafu zákona</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="262"/>
        <source>letter</source>
        <translation>písmeno</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="269"/>
        <source>Letter of the paragraph</source>
        <translation>Písmeno odstavce</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="278"/>
        <source>Our reference number:</source>
        <translation>Naše číslo jednací:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="292"/>
        <source>Our file mark:</source>
        <translation>Naše spisová značka:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="306"/>
        <source>Your reference number:</source>
        <translation>Vaše číslo jednací:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="320"/>
        <source>Your file mark:</source>
        <translation>Vaše spisová značka:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="350"/>
        <source>Personal delivery:</source>
        <translation>Do vlastních rukou:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="357"/>
        <source>Enable personal delivery</source>
        <translation>Povolit doručení do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="334"/>
        <source>To hands:</source>
        <translation>K rukám:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="343"/>
        <source>Enter name of person</source>
        <translation>Zadejte jméno osoby</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="299"/>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="327"/>
        <source>Enter our file mark</source>
        <translation>Zadejte spisovou značku</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="285"/>
        <source>Enter our reference number</source>
        <translation>Zadejte jednací číslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="313"/>
        <source>Enter your reference number</source>
        <translation>Zadejte jednací číslo</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="387"/>
        <source>Recipients:</source>
        <translation>Příjemci:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="516"/>
        <source>Even if the recipient did not read this message,
the message is considered to be accepted after
(currently) 10 days. This is acceptance through fiction.</source>
        <translation>I když si příjemce nepřečetl zprávu,
bude zpráva považována za doručenou 
(aktuálně) po 10 dnech. Toto je doručení fikcí.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="521"/>
        <source>Allow acceptance through fiction</source>
        <translation>Povolit doručení fikcí</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="733"/>
        <source>Attachment size is larger than 20 MB. Message cannot be sent!</source>
        <translation>Velikost příloh je větší než 20 MB. Zprávu nebude možné odeslat!</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="403"/>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="624"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="423"/>
        <source>Remove selected recipient from the list</source>
        <translation>Odstranit příjemce ze seznamu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="426"/>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="647"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="400"/>
        <source>Add recipient from contacts selected from existing messages</source>
        <translation>Přidat příjemce z kontaktů existujících zpráv</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="67"/>
        <source>Account:</source>
        <translation>Účet:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="93"/>
        <source>Sender: </source>
        <translation>Odesílatel:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="447"/>
        <source>Find and add a recipient from Datové schránky server</source>
        <translation>Najít a přidat příjemce ze serveru Datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="450"/>
        <source>Find</source>
        <translation>Najít</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="468"/>
        <source>Allows to enter address of Databox manually</source>
        <translation>Umožňuje ruční vložení adresy datové schránky</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="471"/>
        <source>Enter DB ID</source>
        <translation>Vložit ID DS</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="559"/>
        <source>You will pay the transfer charges of the reply message.
This is useful when the recipient does not have sending
of postal data messages active.</source>
        <translation>Zaplatíte poštovní poplatky za odpovědní zprávu.
Tato možnost je užitečná, pokud adresátova schránka
neumožňuje zasílání poštovních datových zpráv.</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="621"/>
        <source>Add a new file to the attachments</source>
        <translation>Přidat nový soubor do přílohy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="644"/>
        <source>Remove the selected file from attachments</source>
        <translation>Odstranit vybraný soubor z přílohy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="674"/>
        <source>Open selected file in associated application</source>
        <translation>Otevřít vybraný soubor v externí aplikaci</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="537"/>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="540"/>
        <source>Include sender identification</source>
        <translation>Přidat identifikaci odesílatele</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="564"/>
        <source>Pay transfer charges for reply</source>
        <translation>Zaplatit poštovní poplatky za odpověď</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="580"/>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="583"/>
        <source>Use offered payment of transfer charges by recipient</source>
        <translation>Využít nabízenou platbu za poštovní poplatky příjemcem</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="608"/>
        <source>Attachments:</source>
        <translation>Přílohy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="677"/>
        <source>Open</source>
        <translation>Otevřít</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="765"/>
        <source>Send message</source>
        <translation>Odeslat zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="768"/>
        <source>Send</source>
        <translation>Odeslat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_send_message.ui" line="781"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>SignatureDetail</name>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="14"/>
        <source>Signature detail for current message</source>
        <translation>Detaily o podpisu zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="26"/>
        <source>Message signature</source>
        <translation>Podpis zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="142"/>
        <source>Show verification detail</source>
        <translation>Zobrazit detail ověření</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="210"/>
        <source>Show certificate detail</source>
        <translation>Zobrazit detail certifikátu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="40"/>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="99"/>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="179"/>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="247"/>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="299"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="85"/>
        <source>Signing certificate</source>
        <translation>Podepisující certifikát</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_signature_detail.ui" line="285"/>
        <source>Timestamp</source>
        <translation>Časové razítko</translation>
    </message>
</context>
<context>
    <name>TagDialog</name>
    <message>
        <location filename="../src/gui/ui/dlg_tag.ui" line="14"/>
        <source>Tag properties</source>
        <translation>Vlastnosti tagu</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag.ui" line="22"/>
        <source>Tag name:</source>
        <translation>Jméno tagu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag.ui" line="32"/>
        <source>Tag color:</source>
        <translation>Barva tagu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tag.ui" line="54"/>
        <source>Change</source>
        <translation>Změnit</translation>
    </message>
</context>
<context>
    <name>TagsDialog</name>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="14"/>
        <source>Tag manager</source>
        <translation>Správce tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="34"/>
        <source>Tag operations</source>
        <translation>Operace s tagy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="43"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="50"/>
        <source>Update</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="57"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="67"/>
        <source>Tag assignment</source>
        <translation>Přiřazení tagů</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="76"/>
        <source>Assign</source>
        <translation>Přiřadit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="83"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_tags.ui" line="90"/>
        <source>Remove all</source>
        <translation>Odstranit vše</translation>
    </message>
</context>
<context>
    <name>TagsModel</name>
    <message>
        <location filename="../src/models/tags_model.cpp" line="64"/>
        <source>Tags</source>
        <translation>Tagy</translation>
    </message>
</context>
<context>
    <name>ViewZfo</name>
    <message>
        <location filename="../src/gui/ui/dlg_view_zfo.ui" line="14"/>
        <source>View message from ZFO</source>
        <translation>Zobrazit zprávu ze souboru ZFO</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_view_zfo.ui" line="56"/>
        <source>Verify Signature</source>
        <translation>Ověřit podpis</translation>
    </message>
</context>
<context>
    <name>msgSearchDialog</name>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="20"/>
        <source>Advanced message search</source>
        <translation>Rozšířené vyhledávání ve zprávách</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="48"/>
        <source>Current account:</source>
        <translation>Aktuální účet:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="90"/>
        <source>Search also in other accounts (can be slow).</source>
        <translation>Vyhledávat také v ostatních účtech (může být pomalé).</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="97"/>
        <source>Message type</source>
        <translation>Typ zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="103"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="113"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="126"/>
        <source>Fill in data according to which you want to search:</source>
        <translation>Vyplňte údaje podle kterých chcete vyhledávat:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="136"/>
        <source>Message ID:</source>
        <translation>ID zprávy:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="150"/>
        <source>Subject:</source>
        <translation>Předmět:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="174"/>
        <source>Sender Databox ID:</source>
        <translation>ID schránky odesílatete:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="191"/>
        <source>Sender name:</source>
        <translation>Jméno odesílatele:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="201"/>
        <source>Our reference number:</source>
        <translation>Naše číslo jednací:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="211"/>
        <source>Our file mark:</source>
        <translation>Naše spisová značka:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="231"/>
        <source>Recipient name:</source>
        <translation>Jméno příjemce:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="245"/>
        <source>Recipient Databox ID:</source>
        <translation>ID schránky příjemce:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="255"/>
        <source>Your reference number:</source>
        <translation>Vaše číslo jednací:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="265"/>
        <source>Your file mark:</source>
        <translation>Vaše spisová značka:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="281"/>
        <source>Address:</source>
        <translation>Adresa:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="291"/>
        <source>To hands:</source>
        <translation>K rukám:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="312"/>
        <source>Tag text:</source>
        <translation>Text tagu:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="327"/>
        <source>Too much parameters for search!</source>
        <translation>Příliš mnoho parametrů vyhledávání!</translation>
    </message>
    <message>
        <location filename="../src/gui/ui/dlg_msg_search.ui" line="346"/>
        <source>Search</source>
        <translation>Vyhledat</translation>
    </message>
</context>
</TS>
