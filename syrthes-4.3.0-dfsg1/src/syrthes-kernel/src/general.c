/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <string.h>

# include "syr_usertype.h"
# include "syr_bd.h"
# include "syr_parall.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_const.h"
# include "syr_proto.h"

#ifdef _SYRTHES_MPI_
# include <mpi.h>
MPI_Status status;
#endif

extern char nommnx[CHLONG];
extern struct Performances perfo;
extern struct Affichages affich;


static FILE *fmnx;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |  Allocations generales                                               |
  |======================================================================| */
void alloue_nbvar(int champmax_actif,struct Humid humid,
		  struct Variable *variable,struct Cvol **fluxvol)
{
  int n;

  variable->nbvar=1;
  if (humid.model==2) variable->nbvar=2;
  else if (humid.model==3)variable->nbvar=3;


  /* flux volumiques */
  /* --------------- */
  *fluxvol=(struct Cvol*)malloc(variable->nbvar*sizeof(struct Cvol));
  

  

  /* en conduction on a T et Tn-1 */
  /* ---------------------------- */
  if (!humid.actif)
    {
      variable->nbadr=2;
      variable->adr_t=0;
      variable->adr_t_m1=1;

      if (champmax_actif) 
	{
	  variable->nbadr=4;
	  variable->adr_tmin=2;
	  variable->adr_tmax=3;
	}

      variable->nomvar=(char**)malloc(sizeof(char*));
      variable->nomvar[0]=(char*)malloc(100*sizeof(char));
      strcpy(variable->nomvar[0],"Temperature\0");
    }

  /* transferts couples a 2 equations */
  /* -------------------------------- */
  /* si hmt on T,Tn-1,Tn-2  et PV,PVn-1,PVn-2  */
  else if (humid.model==2) 
    {
      variable->nbadr=6;

      variable->adr_t =0;
      variable->adr_pv=1;

      variable->adr_t_m1 =2;
      variable->adr_pv_m1=3;

      variable->adr_t_m2 =4;
      variable->adr_pv_m2=5;

      variable->adr_pt=-1;
      variable->adr_pt_m1=-1;
      variable->adr_pt_m2=-1;

      if (champmax_actif) 
	{
	  variable->nbadr=10;
	  variable->adr_tmin=6;
	  variable->adr_tmax=7;
	  variable->adr_pvmin=8;
	  variable->adr_pvmax=9;

	  variable->adr_ptmin=-1;
	  variable->adr_ptmax=-1;
	}

      variable->nomvar=(char**)malloc(variable->nbvar*sizeof(char*));
      for (n=0;n<variable->nbvar;n++) variable->nomvar[n]=(char*)malloc(100*sizeof(char));
      strcpy(variable->nomvar[0],"Temperature\0");
      strcpy(variable->nomvar[1],"PV\0");
      
    }
  /* transferts couples a 3 equations */
  /* -------------------------------- */
  /* si hmt on T,Tn-1,Tn-2  et PV,PVn-1,PVn-2 et  PT, PTn-1,PTn-2 */
  else if (humid.model==3) 
    {
      variable->nbadr=9;

      variable->adr_t =0;
      variable->adr_pv=1;
      variable->adr_pt=2;

      variable->adr_t_m1 =3;
      variable->adr_pv_m1=4;
      variable->adr_pt_m1=5;

      variable->adr_t_m2 =6;
      variable->adr_pv_m2=7;
      variable->adr_pt_m2=8;

      if (champmax_actif) 
	{
	  variable->nbadr=15;;
	  variable->adr_tmin=9;
	  variable->adr_tmax=10;
	  variable->adr_pvmin=11;
	  variable->adr_pvmax=12;
	  variable->adr_ptmin=13;
	  variable->adr_ptmax=14;
	}

      variable->nomvar=(char**)malloc(variable->nbvar*sizeof(char*));
      for (n=0;n<variable->nbvar;n++) variable->nomvar[n]=(char*)malloc(100*sizeof(char));
      strcpy(variable->nomvar[0],"Temperature\0");
      strcpy(variable->nomvar[1],"PV\0");
      strcpy(variable->nomvar[2],"PT\0");
      
    }
  
  /* allocation du tableau des variables */
  variable->var =(double**)malloc(variable->nbadr*sizeof(double*));

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |  Allocations generales                                               |
  |======================================================================| */
void alloue_tvar(int npoin,struct Variable *variable)
{
  int n;

  for (n=0;n<variable->nbadr;n++)
    {
      variable->var[n]=(double*)malloc(npoin*sizeof(double)); 
      verif_alloue_double1d("alloue_tvar",variable->var[n]);
    }

  perfo.mem_cond+=npoin * variable->nbadr * sizeof(double);

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |  Allocations generales                                               |
  |======================================================================| */
void alloue_resol(int lray,int nelray_orig,int nbvar,
		  struct Maillage maillnodes,struct Maillage maillnodray,
		  struct MaillageCL maillnodeus,  
		  struct Matrice *matr,struct MatriceRay *matrray,
		  struct Prophy physol,
		  struct Travail *trav,struct Travail *trave)
{   
  int n,i;

  /* tableaux de travail */
  trav->nbtab=6;
  trav->nbitab=0;

  /* initialisation des longueurs des tableaux de travail */
  trav->nlong=(int*)malloc(trav->nbtab*sizeof(int));
  trav->nlong[0]=max(maillnodes.npoin,maillnodeus.nelem*maillnodeus.ndmat);
  if (lray) trav->nlong[0]=max(trav->nlong[0],nelray_orig);
  
  trav->nlong[1]=maillnodes.npoin;
  trav->nlong[2]=maillnodes.npoin;
  trav->nlong[3]=maillnodes.npoin;
  trav->nlong[4]=maillnodes.npoin;
  trav->nlong[5]=maillnodes.npoin;
  
  trav->tab=(double**)malloc(trav->nbtab*sizeof(double*));
  for  (i=0;i<trav->nbtab;i++)
    {
      trav->tab[i]=(double*)malloc(trav->nlong[i]*sizeof(double)); 
      verif_alloue_double1d("syrthes",trav->tab[i]);
      perfo.mem_cond+=trav->nlong[i]*sizeof(double);
    }
  
  if (trav->nbitab>0)
    {
      trav->itab=(int**)malloc(trav->nbitab*sizeof(int*));
      for  (i=0;i<trav->nbitab;i++)
	{
	  trav->itab[i]=(int*)malloc(trav->ilong[i]*sizeof(int)); 
	  verif_alloue_int1d("syrthes",trav->itab[i]);
	  /*	  perfo.mem_f1d+=trav->ilong[i]*sizeof(int); */
	}
    }
        

  /* tableaux de travail a la taille du nombre d'elements */
  trave->nbtab=1;
  
  trave->nlong=(int*)malloc(trave->nbtab*sizeof(int));
  trave->nlong[0]=maillnodes.nelem;
  
  trave->tab=(double**)malloc(trave->nbtab*sizeof(double*));
  for  (i=0;i<trave->nbtab;i++)
    {
      trave->tab[i]=(double*)malloc(trave->nlong[i]*sizeof(double)); 
      verif_alloue_double1d("syrthes",trave->tab[i]);
      perfo.mem_cond+=trave->nlong[i]*sizeof(double);
    }
  
  
  alloue_matrice(maillnodes,physol,matr);
  
  
  
  if (lray)
    {
      matrray->x=(double*)malloc(maillnodray.nelem*sizeof(double));       verif_alloue_double1d("syrthes",matrray->x);
      matrray->z=(double*)malloc(maillnodray.nelem*sizeof(double));       verif_alloue_double1d("syrthes",matrray->z);
      matrray->b=(double*)malloc(maillnodray.nelem*sizeof(double));       verif_alloue_double1d("syrthes",matrray->b);
      matrray->xm1=(double*)malloc(maillnodray.nelem*sizeof(double));     verif_alloue_double1d("syrthes",matrray->xm1);
      matrray->gd=(double*)malloc(maillnodray.nelem*sizeof(double));      verif_alloue_double1d("syrthes",matrray->gd);
      matrray->res=(double*)malloc(maillnodray.nelem*sizeof(double));     verif_alloue_double1d("syrthes",matrray->res);
      matrray->resm1=(double*)malloc(maillnodray.nelem*sizeof(double));   verif_alloue_double1d("syrthes",matrray->resm1);
      matrray->di=(double*)malloc(maillnodray.nelem*sizeof(double));      verif_alloue_double1d("syrthes",matrray->di);
    }
  
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Initialisation des variables etapes n-1 et event n-2                 |
  |======================================================================| */
void recopie(int npoin,struct Variable variable)
{
  int i;
  
  /* initialisation de l'etape n-1 voire n-2 */
  for (i=0;i<npoin;i++) variable.var[variable.adr_t_m1][i] = variable.var[variable.adr_t][i];
  
  if (variable.nbvar>1)
    for (i=0;i<npoin;i++)
      {
	variable.var[variable.adr_t_m2][i]  = variable.var[variable.adr_t][i];
	variable.var[variable.adr_pv_m1][i] = variable.var[variable.adr_pv_m2][i] = variable.var[variable.adr_pv][i];
      }
  if (variable.nbvar>2)
    for (i=0;i<npoin;i++)
      variable.var[variable.adr_pt_m1][i] = variable.var[variable.adr_pt_m2][i] = variable.var[variable.adr_pt][i];

}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |  Calcul des extremaux                                                |
  |======================================================================| */
void calcul_extreme(double tempss,
		    struct Maillage maillnodes,struct Prophy physol,
		    struct Variable variable,
		    struct Humid humid,struct SDparall sdparall)
{
  int i,j,ii,nbval=15;
  /* pour chaque variable (15) on stocke (min,x,y,z, max,x,y,z) */
  double **vmnx;

  static int prem=1;

  if (prem && (sdparall.nparts==1 || sdparall.rang==0))
    {
      prem=0;

      if ((fmnx=fopen(nommnx,"w")) == NULL)
	{
	  if (SYRTHES_LANG == FR)
	    printf("Impossible d'ouvrir le fichier %s\n",nommnx);
	  else if (SYRTHES_LANG == EN)
	    printf("Impossible to open the file %s\n",nommnx);
	  syrthes_exit(1);
	}

      fprintf(fmnx,"# 1=temps  2=T_min    3=x   4=y   5=z   6=T_max     7=x   8=y   9=z  \n");
      ii=10;

      if (variable.nbvar>1){
	fprintf(fmnx,"#       %2d=PVmin %2d=x  %2d=y  %2d=z  %2d=PVmax  %2d=x  %2d=y  %2d=z  \n",
		ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	ii+=8;}

      if (variable.nbvar>2){
	fprintf(fmnx,"#       %2d=PTmin %2d=x  %2d=y  %2d=z  %2d=PTmax  %2d=x  %2d=y  %2d=z  \n",
		ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	ii+=8;}
      
      if (!humid.actif)
	{
	fprintf(fmnx,"#         %2d=rho_min %2d=x  %2d=y  %2d=z  %2d=rho_max  %2d=x  %2d=y  %2d=z  \n",
		ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	ii+=8;
	fprintf(fmnx,"#         %2d=cp_min  %2d=x  %2d=y  %2d=z  %2d=cp_max   %2d=x  %2d=y  %2d=z  \n",
		ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	ii+=8;

	if (physol.isotro){
	fprintf(fmnx,"#         %2d=k_min   %2d=x  %2d=y  %2d=z  %2d=k_max    %2d=x  %2d=y  %2d=z  \n",
		ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	ii+=8;}

	if (physol.orthotro){
	  fprintf(fmnx,"#         %2d=k11_min %2d=x  %2d=y  %2d=z  %2d=k11_max  %2d=x  %2d=y  %2d=z  \n",
		  ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	  ii+=8;
	  fprintf(fmnx,"#         %2d=k22_min %2d=x  %2d=y  %2d=z  %2d=k22_max  %2d=x  %2d=y  %2d=z  \n",
		  ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	  ii+=8;
	  if(maillnodes.ndim==3){
	    fprintf(fmnx,"#         %2d=k33_min %2d=x  %2d=y  %2d=z  %2d=k33_max  %2d=x  %2d=y  %2d=z  \n",
		    ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	    ii+=8;
	  }
	}

	if (physol.anisotro){
	  fprintf(fmnx,"#         %2d=k11_min %2d=x  %2d=y  %2d=z  %2d=k11_max  %2d=x  %2d=y  %2d=z  \n",
		  ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	  ii+=8;
	  fprintf(fmnx,"#         %2d=k22_min %2d=x  %2d=y  %2d=z  %2d=k22_max  %2d=x  %2d=y  %2d=z  \n",
		  ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	  ii+=8;
	  if(maillnodes.ndim==3){
	    fprintf(fmnx,"#         %2d=k33_min %2d=x  %2d=y  %2d=z  %2d=k33_max  %2d=x  %2d=y  %2d=z  \n",
		    ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	    ii+=8;
	  }
	  fprintf(fmnx,"#         %2d=k11_min %2d=x  %2d=y  %2d=z  %2d=k12_max  %2d=x  %2d=y  %2d=z  \n",
		  ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	  ii+=8;
	  if(maillnodes.ndim==3){
	    fprintf(fmnx,"#         %2d=k13_min %2d=x  %2d=y  %2d=z  %2d=k13_max  %2d=x  %2d=y  %2d=z  \n",
		    ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	    ii+=8;
	    fprintf(fmnx,"#         %2d=k23_min %2d=x  %2d=y  %2d=z  %2d=k23_max  %2d=x  %2d=y  %2d=z  \n",
		    ii,ii+1,ii+2,ii+3,ii+4,ii+5,ii+6,ii+7);
	    ii+=8;
	  }

	}

	}/* fin de !humid */
    }/* fin de prem */


  vmnx=(double**)malloc(nbval*sizeof(double*));
  for (i=0;i<nbval;i++) vmnx[i]=(double*)malloc(8*sizeof(double));

  if (sdparall.nparts==1 || sdparall.rang==0) fprintf(fmnx,"%16.9e ",tempss);

  /* T */
  extreme(0,maillnodes.npoin,variable.var[variable.adr_t],maillnodes,vmnx[0],sdparall);
  if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[0][j]);

  /* PV */
  if (variable.nbvar>1) {
    extreme(0,maillnodes.npoin,variable.var[variable.adr_pv],maillnodes,vmnx[1],sdparall);
    if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[1][j]);}

  /* PT */
  if (variable.nbvar>2){
    extreme(0,maillnodes.npoin,variable.var[variable.adr_pt],maillnodes,vmnx[2],sdparall);
    if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[2][j]);}
  
  if (!humid.actif)
    {
      /* rho */
      extreme(1,maillnodes.nelem,physol.rho,maillnodes,vmnx[3],sdparall);
      if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[3][j]);
	
      /* cp */
      extreme(1,maillnodes.nelem,physol.cp,maillnodes,vmnx[4],sdparall);
      if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[4][j]);
  
      if (physol.isotro){
	/* k */
	extreme_partial(1,physol.kiso.nelem,physol.kiso.k,physol.kiso.ele,maillnodes,vmnx[5],sdparall);
	if (sdparall.nparts==1 || sdparall.rang==0 ) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[5][j]);}

      if (physol.orthotro){
	/* k11 */
	extreme_partial(1,physol.kortho.nelem,physol.kortho.k11,physol.kortho.ele,maillnodes,vmnx[6],sdparall);
	if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[6][j]);

	/* k22 */
	extreme_partial(1,physol.kortho.nelem,physol.kortho.k22,physol.kortho.ele,maillnodes,vmnx[7],sdparall);
	if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[7][j]);

	if (maillnodes.ndim==3) {
	/*k33  */
	  extreme_partial(1,physol.kortho.nelem,physol.kortho.k33,physol.kortho.ele,maillnodes,vmnx[8],sdparall);
	  if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[8][j]);}
      }


      if (physol.anisotro){
	/* k11 */
	extreme_partial(1,physol.kaniso.nelem,physol.kaniso.k11,physol.kaniso.ele,maillnodes,vmnx[9],sdparall);
	if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[9][j]);

	/* k22 */
	extreme_partial(1,physol.kaniso.nelem,physol.kaniso.k22,physol.kaniso.ele,maillnodes,vmnx[10],sdparall);
	if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[10][j]);

	if (maillnodes.ndim==3){
	/* k33 */
	  extreme_partial(1,physol.kaniso.nelem,physol.kaniso.k22,physol.kaniso.ele,maillnodes,vmnx[11],sdparall);
	  if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[11][j]);}
	
	/* k12 */
	extreme_partial(1,physol.kaniso.nelem,physol.kaniso.k12,physol.kaniso.ele,maillnodes,vmnx[12],sdparall);
	if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[12][j]);

	if (maillnodes.ndim==3)
	  {
	    /* k13 */
	    extreme_partial(1,physol.kaniso.nelem,physol.kaniso.k13,physol.kaniso.ele,maillnodes,vmnx[13],sdparall);
	    if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[13][j]);
	    
	    extreme_partial(1,physol.kaniso.nelem,physol.kaniso.k23,physol.kaniso.ele,maillnodes,vmnx[14],sdparall);
	    /* k23 */
	    if (sdparall.nparts==1 || sdparall.rang==0) for (j=0;j<8;j++) fprintf(fmnx,"%16.9e ",vmnx[14][j]);
	  }
      }



    }

  if (sdparall.nparts==1 || sdparall.rang==0) 
    {
      fprintf(fmnx,"\n");
      printf("\n -->  Tmin=%15.5f      Tmax=%15.5f\n",vmnx[0][0],vmnx[0][4]);
      if (variable.nbvar>1) printf(" --> PVmin=%15.5f     PVmax=%15.5f\n",vmnx[1][0],vmnx[1][4]);
      if (variable.nbvar>2) printf(" --> PTmin=%15.5f     PTmax=%15.5f\n",vmnx[2][0],vmnx[2][4]);
    }

  for (i=0;i<nbval;i++) free(vmnx[i]);
  free(vmnx);

  fflush(fmnx);

}


  

