/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: 579487e67ce8d02d7a465cb8ae75a94c229a8460 $
*/

/*
 * This file is provided for CVS's sake, and to define a simple string to be
 * used as the module's release version
 */

#define PGSQL_VERSION "Postgres/1.0.2"
