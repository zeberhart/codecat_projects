This directory contains both the sources and executable versions
of the SYRTHES GUI.
 
Sources:
=======
Located in directory:
/Sources_syrthes-GUI_1.0.0

To compile/use these sources on Linux, the pre-requisit are
Python2.6.x
sip 4.10 or above
Qt 4.6.2 or above
PyQt 4.7 or above
PyQwt 5.2.0
cx_freeze 4.1.2

(packages on Windows are slightly different - and numpy 1.1.0 is necessary) 

Install:
=======
cd /.../syrthes4/syrthes4/src/syrthes-gui/Sources_syrthes-GUI_xxx
./Install.sh
cd install
python setup_cxfl.py build

cd build  
mv exe.linux-x86_64_xxx   /.../syrthes4/syrthes4/src/syrthes-gui/exe.linux-x86_64


To test the syrthes-GUI:
=======================
Located in directory 
/exe.linux-x86_64

For most users, having compatible systems,
a more practicable and ready to use executable 
(compiled on Linux_x86_64-2.6   - calibre7 at EDF)
of the Syrthes-GUI (in which case there is no need to install all the packages)
is available.

To test/launch the syrthes GUI, enter the directory : exe.linux-x86_64
and 

> ./SyrthesMain


------------------------------------------
When used interactively with the SYRTHES software (normal usage), 
the activation of 
the SYRTHES GUI (providing the syrthes.profile has been 
sourced) is done through the command :

> syrthes.gui

------------------------------------------

Enjoy using SYRTHES


Contact : syrthes-support@edf.fr


 


