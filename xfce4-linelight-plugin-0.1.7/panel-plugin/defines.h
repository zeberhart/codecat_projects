/*
 *  Copyright 2008 Michael Pfeuti <m_pfeuti@students.unibe.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#ifndef DEFINES_H_INCLUDED
#define DEFINES_H_INCLUDED

#define MAX_PATH_LENGTH 1024
#define VERSION "0.1.6"
#define ICON_SIZE 16

typedef enum _corner
{
    TOP_RIGHT_CORNER,
    TOP_LEFT_CORNER,
    BOTTOM_RIGHT_CORNER,
    BOTTOM_LEFT_CORNER
} GrowingCorner;

typedef enum _section
{
    MUSIC_SECTION,
    VIDEO_SECTION,
    PICTURES_SECTION,
    //WEB_SECTION,
    BIN_SECTION,
    FILES_SECTION,
    FOLDER_SECTION,
    SECTION_COUNT
} Section;

enum
{
    ICON_COL,
    TEXT_COL,
    NUM_COLUMS
};

static char *sectionNames[SECTION_COUNT] = {"MUSIC", "VIDEOS", "PICTURES", /*"WEB",*/ "APPLICATIONS", "FILES","FOLDERS"};

static const char *categories[] = {"Name", "Exec", "Icon"};
#endif // DEFINES_H_INCLUDED
