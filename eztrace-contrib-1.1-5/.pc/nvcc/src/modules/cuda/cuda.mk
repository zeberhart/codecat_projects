.cu.o:
	$(NVCC) -o $@ -c $< $(AM_CPPFLAGS) $(AM_CFLAGS) $(TLCFLAGS) $(CUPTI_CFLAGS)

.cu.lo:
	python $(top_srcdir)/src/modules/cuda/cudalt.py $@ $(NVCC) --compiler-options=\" $(CFLAGS) $(DEFAULT_INCLUDES) $(INCLUDES) $(AM_CPPFLAGS) $(AM_CFLAGS) $(CPPFLAGS) \" $(TLCFLAGS) -c $< $(CUPTI_CFLAGS)

