/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: 7a85aa33d86ddca44ce889c4f07ce609155f4a49 $
*/

#ifndef PROTOCOLS_DNS_SD_H
#define PROTOCOLS_DNS_SD_H

@TOP@
@BOTTOM@

#undef HAVE_DNS_SD
#undef HAVE_HOWL

#endif
