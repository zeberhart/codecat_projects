/*
 * This file is part of the Kvkbd project.
 * Copyright (C) 2007-2008 Todor Gyumyushev <yodor@developer.bg>
 * Copyright (C) 2008 Guillaume Martres <smarter@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "vbutton.h"


#include <QMouseEvent>
#include <QTimer>

#include <KDebug>
double VButton::pw = 550.0;
double VButton::ph = 235.0;

extern VButton *globalShift;

VButton::VButton(QWidget *parent): QPushButton(parent)
{
    setFocusPolicy(Qt::NoFocus);
    resize(30, 30);
    rightClicked = false;
    keyTimer = new QTimer(this);

    connect(keyTimer, SIGNAL(timeout()), this, SLOT(repeatKey()));
}
VButton::~VButton()
{
}

void VButton::setKeyCap(bool shiftPressed, bool capsPressed)
{
    //TODO: find a better way to detect if the key is an alphabetic character
    bool capsKey = (text != text.toUpper());

    if (capsPressed && capsKey) {
        QPushButton::setText(shiftPressed ? shiftText
                             : text.toUpper());
    } else {
        QPushButton::setText(shiftPressed ? shiftText
                             : text);
    }
}

void VButton::setText(const QString& input, const QString& shiftInput)
{
    text = input;
    QPushButton::setText(text);
    if (!shiftInput.isNull()) {
        shiftText = shiftInput;
    } else if (shiftText.isNull()) {
        shiftText = text.toUpper();
    }
}
void VButton::setShiftText(const QString& input)
{
    shiftText = input;
}

void VButton::setKeyCode(unsigned int keycode)
{
    this->keycode = keycode;

}
unsigned int VButton::getKeyCode()
{
    return this->keycode;
}
void VButton::sendKey()
{
    emit keyClick(keycode);
}

void VButton::reposition(int width, int height)
{
    double dx = pw / originalSize.x();
    double dy = ph / originalSize.y();
    double sdx = pw / originalSize.width();
    double sdy = ph / originalSize.height();
    move((int)(width / dx), (int)(height / dy));
    resize((int)(width / sdx), (int)(height / sdy));

}
void VButton::res()
{
    originalSize = geometry();
}

//When a button is pressed, a timer is set which regularly check if
//the button is still pressed and repeat the key if it's the case
void VButton::mousePressEvent(QMouseEvent *e)
{
    rightClicked = false;
    if (e->button() == Qt::RightButton) {
        if (globalShift) { //FIXME: is this check really necessary?
            rightClicked = true;
            globalShift->setChecked(true);
        }
    }
    QPushButton::mousePressEvent(e);
    if (!keyTimer->isActive()) {
        //200 ms is a bit more that the time needed for a single click
        keyTimer->start(200);
        sendKey();
    }
}

void VButton::mouseReleaseEvent(QMouseEvent *e)
{
    keyTimer->stop();
    QPushButton::mouseReleaseEvent(e);
}

void VButton::repeatKey()
{
    //if the user is still pressing the button after 200 ms, we assume
    //he wants the key to be quickly repeated and we decrease the interval
    if (keyTimer->interval() == 200) {
        //TODO: make this configurable?
        keyTimer->setInterval(40);
    }
    if (rightClicked) {
        globalShift->setChecked(true);
    }
    sendKey();
}
