<SECTION>
<FILE>SwamiControlMidi</FILE>
<TITLE>SwamiControlMidi</TITLE>
SwamiControlMidi
swami_control_midi_new
swami_control_midi_set_callback
swami_control_midi_send
swami_control_midi_transmit
<SUBSECTION Standard>
SWAMI_CONTROL_MIDI
SWAMI_IS_CONTROL_MIDI
SWAMI_TYPE_CONTROL_MIDI
swami_control_midi_get_type
SWAMI_CONTROL_MIDI_CLASS
SWAMI_IS_CONTROL_MIDI_CLASS
</SECTION>

<SECTION>
<FILE>SwamiPlugin</FILE>
SwamiPluginInfo
SwamiPluginInitFunc
SwamiPluginExitFunc
SwamiPluginSaveXmlFunc
SwamiPluginLoadXmlFunc
<TITLE>SwamiPlugin</TITLE>
SwamiPlugin
SWAMI_PLUGIN_MAGIC
SWAMI_PLUGIN_INFO
swami_plugin_add_path
swami_plugin_load_all
swami_plugin_load
swami_plugin_load_absolute
swami_plugin_load_plugin
swami_plugin_is_loaded
swami_plugin_find
swami_plugin_get_list
swami_plugin_save_xml
swami_plugin_load_xml
<SUBSECTION Standard>
SWAMI_PLUGIN
SWAMI_IS_PLUGIN
SWAMI_TYPE_PLUGIN
swami_plugin_get_type
SWAMI_PLUGIN_CLASS
SWAMI_IS_PLUGIN_CLASS
</SECTION>

<SECTION>
<FILE>SwamiControlQueue</FILE>
SwamiControlQueueTestFunc
<TITLE>SwamiControlQueue</TITLE>
SwamiControlQueue
swami_control_queue_new
swami_control_queue_add_event
swami_control_queue_run
swami_control_queue_set_test_func
<SUBSECTION Standard>
SWAMI_CONTROL_QUEUE
SWAMI_IS_CONTROL_QUEUE
SWAMI_TYPE_CONTROL_QUEUE
swami_control_queue_get_type
SWAMI_CONTROL_QUEUE_CLASS
SWAMI_IS_CONTROL_QUEUE_CLASS
</SECTION>

<SECTION>
<FILE>SwamiControlFunc</FILE>
SwamiControlFuncDestroy
<TITLE>SwamiControlFunc</TITLE>
SwamiControlFunc
SWAMI_CONTROL_FUNC_DATA
swami_control_func_new
swami_control_func_assign_funcs
<SUBSECTION Standard>
SWAMI_CONTROL_FUNC
SWAMI_IS_CONTROL_FUNC
SWAMI_TYPE_CONTROL_FUNC
swami_control_func_get_type
SWAMI_CONTROL_FUNC_CLASS
SWAMI_IS_CONTROL_FUNC_CLASS
</SECTION>

<SECTION>
<FILE>SwamiMidiDevice</FILE>
<TITLE>SwamiMidiDevice</TITLE>
SwamiMidiDevice
swami_midi_device_open
swami_midi_device_close
swami_midi_device_get_control
<SUBSECTION Standard>
SWAMI_MIDI_DEVICE
SWAMI_IS_MIDI_DEVICE
SWAMI_TYPE_MIDI_DEVICE
swami_midi_device_get_type
SWAMI_MIDI_DEVICE_CLASS
SWAMI_IS_MIDI_DEVICE_CLASS
</SECTION>

<SECTION>
<FILE>SwamiControl</FILE>
SwamiControlGetSpecFunc
SwamiControlSetSpecFunc
SwamiControlGetValueFunc
SwamiControlSetValueFunc
<TITLE>SwamiControl</TITLE>
SwamiControl
SwamiControlFlags
SWAMI_CONTROL_FLAGS_USER_MASK
SWAMI_CONTROL_SENDRECV
SWAMI_CONTROL_UNUSED_FLAG_SHIFT
SwamiControlConnPriority
SWAMI_CONTROL_CONN_PRIORITY_MASK
SWAMI_CONTROL_CONN_DEFAULT_PRIORITY_VALUE
SwamiControlConnFlags
SWAMI_CONTROL_CONN_BIDIR_INIT
SWAMI_CONTROL_CONN_BIDIR_SPEC_INIT
swami_control_new
swami_control_connect
swami_control_connect_transform
swami_control_connect_item_prop
swami_control_disconnect
swami_control_disconnect_all
swami_control_disconnect_unref
swami_control_get_connections
swami_control_set_transform
swami_control_get_transform
swami_control_set_flags
swami_control_get_flags
swami_control_set_queue
swami_control_get_queue
swami_control_get_spec
swami_control_set_spec
swami_control_sync_spec
swami_control_transform_spec
swami_control_set_value_type
swami_control_get_value
swami_control_get_value_native
swami_control_set_value
swami_control_set_value_no_queue
swami_control_set_event
swami_control_set_event_no_queue
swami_control_set_event_no_queue_loop
swami_control_transmit_value
swami_control_transmit_event
swami_control_transmit_event_loop
swami_control_do_event_expiration
swami_control_new_event
<SUBSECTION Standard>
SWAMI_CONTROL
SWAMI_IS_CONTROL
SWAMI_TYPE_CONTROL
swami_control_get_type
SWAMI_CONTROL_CLASS
SWAMI_IS_CONTROL_CLASS
SWAMI_CONTROL_GET_CLASS
</SECTION>

<SECTION>
<FILE>SwamiControlHub</FILE>
<TITLE>SwamiControlHub</TITLE>
SwamiControlHub
swami_control_hub_new
<SUBSECTION Standard>
SWAMI_CONTROL_HUB
SWAMI_IS_CONTROL_HUB
SWAMI_TYPE_CONTROL_HUB
swami_control_hub_get_type
SWAMI_CONTROL_HUB_CLASS
SWAMI_IS_CONTROL_HUB_CLASS
</SECTION>

<SECTION>
<FILE>SwamiLock</FILE>
<TITLE>SwamiLock</TITLE>
SwamiLock
SWAMI_LOCK_WRITE
SWAMI_UNLOCK_WRITE
SWAMI_LOCK_READ
SWAMI_UNLOCK_READ
swami_lock_set_atomic
swami_lock_get_atomic
<SUBSECTION Standard>
SWAMI_LOCK
SWAMI_IS_LOCK
SWAMI_TYPE_LOCK
swami_lock_get_type
SWAMI_LOCK_CLASS
SWAMI_IS_LOCK_CLASS
</SECTION>

<SECTION>
<FILE>SwamiRoot</FILE>
<TITLE>SwamiRoot</TITLE>
SwamiRoot
swami_root_new
swami_root_get_patch_items
swami_get_root
swami_root_get_objects
swami_root_add_object
swami_root_new_object
swami_root_prepend_object
swami_root_append_object
swami_root_insert_object_before
swami_root_patch_load
swami_root_patch_save
<SUBSECTION Standard>
SWAMI_ROOT
SWAMI_IS_ROOT
SWAMI_TYPE_ROOT
swami_root_get_type
SWAMI_ROOT_CLASS
SWAMI_IS_ROOT_CLASS
</SECTION>

<SECTION>
<FILE>SwamiWavetbl</FILE>
<TITLE>SwamiWavetbl</TITLE>
SwamiWavetbl
swami_wavetbl_get_virtual_bank
swami_wavetbl_set_active_item_locale
swami_wavetbl_get_active_item_locale
swami_wavetbl_open
swami_wavetbl_close
swami_wavetbl_get_control
swami_wavetbl_load_patch
swami_wavetbl_load_active_item
swami_wavetbl_check_update_item
swami_wavetbl_update_item
<SUBSECTION Standard>
SWAMI_WAVETBL
SWAMI_IS_WAVETBL
SWAMI_TYPE_WAVETBL
swami_wavetbl_get_type
SWAMI_WAVETBL_CLASS
SWAMI_IS_WAVETBL_CLASS
SWAMI_WAVETBL_GET_CLASS
</SECTION>

<SECTION>
<FILE>SwamiContainer</FILE>
<TITLE>SwamiContainer</TITLE>
SwamiContainer
swami_container_new
<SUBSECTION Standard>
SWAMI_CONTAINER
SWAMI_IS_CONTAINER
SWAMI_TYPE_CONTAINER
swami_container_get_type
SWAMI_CONTAINER_CLASS
SWAMI_IS_CONTAINER_CLASS
</SECTION>

<SECTION>
<FILE>SwamiPropTree</FILE>
SwamiPropTreeNode
SwamiPropTreeValue
<TITLE>SwamiPropTree</TITLE>
SwamiPropTree
swami_prop_tree_new
swami_prop_tree_set_root
swami_prop_tree_prepend
swami_prop_tree_append
swami_prop_tree_insert_before
swami_prop_tree_remove
swami_prop_tree_remove_recursive
swami_prop_tree_replace
swami_prop_tree_get_children
swami_prop_tree_object_get_node
swami_prop_tree_add_value
swami_prop_tree_remove_value
<SUBSECTION Standard>
SWAMI_PROP_TREE
SWAMI_IS_PROP_TREE
SWAMI_TYPE_PROP_TREE
swami_prop_tree_get_type
SWAMI_PROP_TREE_CLASS
SWAMI_IS_PROP_TREE_CLASS
</SECTION>

<SECTION>
<FILE>SwamiControlValue</FILE>
<TITLE>SwamiControlValue</TITLE>
SwamiControlValue
swami_control_value_new
swami_control_value_assign_value
swami_control_value_alloc_value
<SUBSECTION Standard>
SWAMI_CONTROL_VALUE
SWAMI_IS_CONTROL_VALUE
SWAMI_TYPE_CONTROL_VALUE
swami_control_value_get_type
SWAMI_CONTROL_VALUE_CLASS
SWAMI_IS_CONTROL_VALUE_CLASS
</SECTION>

<SECTION>
<FILE>SwamiLoopResults</FILE>
<TITLE>SwamiLoopResults</TITLE>
SwamiLoopResults
swami_loop_results_new
swami_loop_results_get_values
<SUBSECTION Standard>
SWAMI_LOOP_RESULTS
SWAMI_IS_LOOP_RESULTS
SWAMI_TYPE_LOOP_RESULTS
swami_loop_results_get_type
</SECTION>

<SECTION>
<FILE>SwamiControlProp</FILE>
<TITLE>SwamiControlProp</TITLE>
SwamiControlProp
swami_get_control_prop
swami_get_control_prop_by_name
swami_control_prop_connect_objects
swami_control_prop_connect_to_control
swami_control_prop_connect_from_control
swami_control_prop_new
swami_control_prop_assign
swami_control_prop_assign_by_name
<SUBSECTION Standard>
SWAMI_CONTROL_PROP
SWAMI_IS_CONTROL_PROP
SWAMI_TYPE_CONTROL_PROP
swami_control_prop_get_type
SWAMI_CONTROL_PROP_CLASS
SWAMI_IS_CONTROL_PROP_CLASS
</SECTION>

<SECTION>
<FILE>SwamiLoopFinder</FILE>
<TITLE>SwamiLoopFinder</TITLE>
SwamiLoopFinder
swami_loop_finder_new
swami_loop_finder_full_search
swami_loop_finder_verify_params
swami_loop_finder_find
swami_loop_finder_get_results
<SUBSECTION Standard>
SWAMI_LOOP_FINDER
SWAMI_IS_LOOP_FINDER
SWAMI_TYPE_LOOP_FINDER
swami_loop_finder_get_type
</SECTION>

<SECTION>
<FILE>SwamiEvent_ipatch</FILE>
SWAMI_TYPE_EVENT_ITEM_ADD
SWAMI_VALUE_HOLDS_EVENT_ITEM_ADD
SWAMI_TYPE_EVENT_ITEM_REMOVE
SWAMI_VALUE_HOLDS_EVENT_ITEM_REMOVE
SWAMI_TYPE_EVENT_PROP_CHANGE
SWAMI_VALUE_HOLDS_EVENT_PROP_CHANGE
SwamiEventItemAdd
swami_event_item_add_get_type
swami_event_item_remove_get_type
swami_event_prop_change_get_type
swami_event_item_add_copy
swami_event_item_add_free
swami_event_item_remove_new
swami_event_item_remove_copy
swami_event_item_remove_free
swami_event_prop_change_new
swami_event_prop_change_copy
swami_event_prop_change_free
</SECTION>

<SECTION>
<FILE>libswami</FILE>
swami_init
swami_patch_prop_title_control
swami_patch_add_control
swami_patch_remove_control
</SECTION>

<SECTION>
<FILE>version</FILE>
SWAMI_VERSION
SWAMI_VERSION_MAJOR
SWAMI_VERSION_MINOR
SWAMI_VERSION_MICRO
swami_version
</SECTION>

<SECTION>
<FILE>util</FILE>
swami_util_get_child_types
swami_util_new_value
swami_util_free_value
swami_util_midi_note_to_str
swami_util_midi_str_to_note
</SECTION>

<SECTION>
<FILE>SwamiLog</FILE>
SWAMI_ERROR
SwamiError
swami_error_quark
swami_log_if_fail
SWAMI_DEBUG
SWAMI_INFO
SWAMI_PARAM_ERROR
SWAMI_CRITICAL
</SECTION>

<SECTION>
<FILE>SwamiMidiEvent</FILE>
SwamiMidiEvent
SwamiMidiEventNote
SwamiMidiEventControl
SWAMI_TYPE_MIDI_EVENT
SwamiMidiEventType
SWAMI_MIDI_CC_BANK_MSB
SWAMI_MIDI_CC_MODULATION
SWAMI_MIDI_CC_VOLUME
SWAMI_MIDI_CC_PAN
SWAMI_MIDI_CC_EXPRESSION
SWAMI_MIDI_CC_BANK_LSB
SWAMI_MIDI_CC_SUSTAIN
SWAMI_MIDI_CC_REVERB
SWAMI_MIDI_CC_CHORUS
SWAMI_MIDI_RPN_BEND_RANGE
SWAMI_MIDI_RPN_MASTER_TUNE
swami_midi_event_get_type
swami_midi_event_new
swami_midi_event_free
swami_midi_event_copy
swami_midi_event_set
swami_midi_event_note_on
swami_midi_event_note_off
swami_midi_event_bank_select
swami_midi_event_program_change
swami_midi_event_bend_range
swami_midi_event_pitch_bend
swami_midi_event_control
swami_midi_event_control14
swami_midi_event_rpn
swami_midi_event_nrpn
</SECTION>

<SECTION>
<FILE>SwamiObject</FILE>
SwamiObjectPropBag
SwamiRank
SwamiObjectFlags
swami_object_propbag_quark
swami_type_set_rank
swami_type_get_rank
swami_type_get_children
swami_type_get_default
swami_object_set_default
swami_object_get_by_name
swami_object_find_by_type
swami_object_get_by_type
swami_object_get_valist
swami_object_get_property
swami_object_get
swami_object_set_valist
swami_object_set_property
swami_object_set
swami_find_object_property
swami_list_object_properties
swami_object_get_flags
swami_object_set_flags
swami_object_clear_flags
swami_object_set_origin
swami_object_get_origin
</SECTION>

<SECTION>
<FILE>SwamiControlEvent</FILE>
SwamiControlEvent
SWAMI_TYPE_CONTROL_EVENT
SWAMI_CONTROL_EVENT_VALUE
swami_control_event_get_type
swami_control_event_new
swami_control_event_free
swami_control_event_duplicate
swami_control_event_transform
swami_control_event_stamp
swami_control_event_set_origin
swami_control_event_ref
swami_control_event_unref
swami_control_event_active_ref
swami_control_event_active_unref
</SECTION>

<SECTION>
<FILE>SwamiParam</FILE>
SwamiValueTransform
swami_param_get_limits
swami_param_set_limits
swami_param_type_has_limits
swami_param_convert
swami_param_convert_new
swami_param_type_transformable
swami_param_type_transformable_value
swami_param_transform
swami_param_transform_new
swami_param_type_from_value_type
</SECTION>

