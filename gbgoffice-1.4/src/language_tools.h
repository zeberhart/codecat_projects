/*************************************************************************
*
* Gbgoffice
* Copyright (C) 2004-2005 Miroslav Yordanov <mironcho@linux-bg.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*************************************************************************/

#ifndef GBGOFFICE_LANGUAGE_TOOLS_H
#define GBGOFFICE_LANGUAGE_TOOLS_H


// simple way for i18n without using gettext
#define LT_(x)	true == lang ? Glib::convert(x[1], "UTF-8", "CP1251"): x[0]


static const char *HELP_MESSAGE[] = {
	"Type a word (bulgarian or english) in entry box above",
	"�������� ���� (�� ��������� ��� ���������) � ������ ��-����"
};


static const char *WELLCOME_MESSAGE[] = {
	"Wellcome to GTK BG Office!",
	"����� ����� � GTK �� ����. ������� ����������!"
};


static const char *ABOUT_MESSAGE[] = {
	"GTK BG Office assistant - version 1.4 \n"
	"Official webpage - http://gbgoffice.info\n\n"
	"(C) 2004-2006 Miroslav Yordanov <mironcho@linux-bg.org>\n"
	"Gbgoffice is part from BG Office project  - http://bgoffice.sf.net",

	"GTK �� ���� �������� - ������ 1.4 \n"
	"��������� �������� - http://gbgoffice.info\n\n"
	"(C) 2004-2006 �������� �������� <mironcho@linux-bg.org>\n"
	"Gbgoffice � ���� �� ������� �� ���� - http://bgoffice.sf.net"
};

	
static const char *CONFIG_ERROR[] = {
	"The configuration could not be initialized\n"
	"This is fatal error and gbgoffice now will exit!",
	
	"�������������� �� ���� �� ���� ��������������\n"
	"���� ������ � �������!"
};


static const char *ERROR_INIT_TRAYICON[] = {
	"Error initializing trayicon module.\n"
	"This is fatal error and gbgoffice now will exit!",
	
	"������ ��� ���������������� �� trayicon ������.\n"
	"���� ������ � �������!"
};


static const char *DATA_MISSING[] = {
	"Dicionary files are missing.\n"
	"Please check that you have installed them\n"
	"and if they are missing, visit\n"
	"http://bgoffice.sourceforge.net\n"
	"for information how to get and install them\n\n"
	"This error is fatal and gbgoffice now will exit!",
	
	"��������� �������.\n"
	"���� ��������� ���� ��� �� �����������\n"
	"� ��� �� ���, �������� ����� http://bgoffice.sf.net\n"
	"�� ������ ���������� �� ���� �� �� ��������� � ��� �� �� �����������\n\n"
	"���� ������ � �������!"
};


static const char *DATA_MISSING_FEDORA[] = {
	"Dicionary files are missing.\n"
	"Please check that you have installed them\n"
	"and if they are missing, please use the supplied \n"
	"bgoffice-dicts.spec file to download and build a rpm file.\n"
	"For information how to get and install them please follow \n"
	"the instructions in the bgoffice-dicts.spec file.\n\n"
	"This error is fatal and gbgoffice now will exit!",
	
	"��������� �������.\n"
	"���� ��������� ���� ��� �� �����������\n"
	"� ��� �� ���, ���� ����������� bgoffice-dicts.spec �����\n"
	"�� �� ��������� � ��������� rpm �����. �� �������� ��������,\n"
	"��������� ������������ � bgoffice-dicts.spec �����.\n\n"
	"���� ������ � �������!"
};



static const char *GUI_CURRENT_DICT[] = {
	"Current dictionary - ",
	"����� ������ - "
};


static const char *GUI_NEXT_WORDS[] = {
	"next words",
	"�������� ����"
};


static const char *GUI_MENU_FILE[] = {
	"_File",
	"_����"
};


static const char *GUI_MENU_EDIT[] = {
	"_Edit",
	"�_����������"
};


static const char *GUI_MENU_DICTS[] = {
	"_Dictionaries",
	"_�������"
};


static const char *GUI_MENU_SETTINGS[] = {
	"_Settings",
	"_���������"
};


static const char *GUI_MENU_HELP[] = {
	"_Help",
	"_�����"
};


static const char *GUI_VIEW_HISTORY[] = {
	"View history",
	"������� ���������"
};


static const char *GUI_PREFS_NUM_WORDS[] = {
	" Number of words in list",
	" ���� ���� � �������"
};


static const char *GUI_PREFS_USE_CLIPBOARD[] = {
	" Watch clipboard for new words",
	" ���������� ��������� �� ���� ����"
};


static const char *GUI_PREFS_TAB_GENERAL[] = {
	"General",
	"�������"
};


static const char *GUI_PREFS_TAB_TRAY[] = {
	"Trayicon",
	"Trayicon"
};


static const char *GUI_PREFS_TAB_TRAY_HELP[] = {
	"<b>You must restart gbgoffice \nbefore these settings take effect</b>",
	"<b>������ �� ������������ gbgoffice \n�� �� ������ � ���� ���� ���������</b>"
};


static const char *GUI_PREFS_USE_TRAYICON[] = {
	" Use trayicon",
	" �������� trayicon"
};


static const char *GUI_PREFS_USE_TRAYICON_CLOSE[] = {
	" Closing main window,\n quits application",
	" ����������� �� �������� ��������,\n ����� ����������"
};

static const char *GUI_PREFS_TRAYICON_HIDE_ON_START[] = {
	" Hide main window on startup",
	" ������ �������� �������� ��� ����������"
};


static const char *GUI_PREFS_USE_WH[] = {
	" Use helper",
	" �������� ���������"
};


static const char *GUI_PREFS_WH_SECONDS[] = {
	" time for showing helper\n (in seconds)",
	" ����� �� ��������� �� ���������\n (� �������)"
};


static const char *GUI_EXAM_MENU[] = {
	"Make a test",
	"�������� �� ��������"
};


static const char *GUI_EXAM_CORRECT[] = {
	"correct",
	"��������"
};

static const char *GUI_EXAM_INCORRECT[] = {
	"incorrect",
	"������"
};

static const char *GUI_EXAM_NEWTEST[] = {
	"Press button \"New\" for new test.",
	"��������� ������ \"���\" �� ��� ����."
};

static const char *GUI_EXAM_NEW_LEVEL1[] = {
	"Novice",
	"�������"
};

static const char *GUI_EXAM_NEW_LEVEL2[] = {
	"Beginner",
	"�����"
};

static const char *GUI_EXAM_NEW_LEVEL3[] = {
	"Intermediate",
	"������"
};

static const char *GUI_EXAM_NEW_LEVEL4[] = {
	"Specialist",
	"������"
};

static const char *GUI_EXAM_NEW_LEVEL5[] = {
	"Expert",
	"���������"
};

static const char *GUI_EXAM_TRANSLATION[] = {
	"Translation: ",
	"������: "
};

static const char *GUI_EXAM_DIFFICULTY[] = {
	"Difficulty: ",
	"����: "
};

static const char *GUI_EXAM_NUMTEST[] = {
	"Test (0 = random): ",
	"���� (0 = ����������): "
};

static const char *GUI_EXAM_NUMQUEST[] = {
	"Num of questions: ",
	"���� �������: "
};

static const char *GUI_EXAM_ENDOFTEST[] = {
	"End of test.",
	"���� �� �����."
};

static const char *GUI_EXAM_TESTNOTSTARTED[] = {
	"Test not started.",
	"�� � �������� ����."
};

static const char *GUI_EXAM_CORRECT_ANSWERS[] = {
	"correct",
	"��������"
};


#endif	// GBGOFFICE_LANGUAGE_TOOLS_H

