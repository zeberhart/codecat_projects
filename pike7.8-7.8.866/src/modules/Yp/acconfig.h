/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: 365899bc1dbf3245d93d8c90525fcabf5de9ad74 $
*/

#ifndef YP_MACHINE_H
#define YP_MACHINE_H

@TOP@
@BOTTOM@

/* The last argument to yp_order() is a YP_ORDER_TYPE * */
#define YP_ORDER_TYPE	unsigned

/* Define if the prototype for yperr_string() is missing. */
#undef YPERR_STRING_PROTOTYPE_MISSING

#endif
