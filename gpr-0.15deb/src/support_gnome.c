
/* This is an internally used function to create pixmaps. */
static GtkWidget *create_dummy_pixmap(GtkWidget * widget, gboolean gnome_pixmap)
{
	GdkColormap *colormap;
	GdkPixmap *gdkpixmap;
	GdkBitmap *mask;
	GtkWidget *pixmap;

	if (gnome_pixmap) {
		return gnome_pixmap_new_from_xpm_d(dummy_pixmap_xpm);
	}

	colormap = gtk_widget_get_colormap(widget);
	gdkpixmap =
	    gdk_pixmap_colormap_create_from_xpm_d(NULL, colormap, &mask, NULL,
						  dummy_pixmap_xpm);
	if (gdkpixmap == NULL)
		g_error(_("Couldn't create replacement pixmap."));
	pixmap = gtk_pixmap_new(gdkpixmap, mask);
	gdk_pixmap_unref(gdkpixmap);
	gdk_bitmap_unref(mask);
	return pixmap;
}

/*-------------------------------------------------------------------
  This is an internally used function to create pixmaps from 
  an internal pixmap structure.
-------------------------------------------------------------------*/
GtkWidget *create_pixmap_from_data
(
 GtkWidget * widget, 
 gchar ** xpm_data,
 gboolean gnome_pixmap
) {

	GdkColormap *colormap;
	GdkPixmap *gdkpixmap;
	GdkBitmap *mask;
	GtkWidget *pixmap;

	if (gnome_pixmap) {
		return gnome_pixmap_new_from_xpm_d(xpm_data);
	}

	colormap = gtk_widget_get_colormap(widget);
	gdkpixmap =
	    gdk_pixmap_colormap_create_from_xpm_d(NULL, colormap, &mask, NULL,
						  xpm_data);
	if (gdkpixmap == NULL)
		g_error(_("Couldn't create pixmap from data."));
	pixmap = gtk_pixmap_new(gdkpixmap, mask);
	gdk_pixmap_unref(gdkpixmap);
	gdk_bitmap_unref(mask);
	return pixmap;
}

/* This is an internally used function to create pixmaps. */
GtkWidget *create_pixmap(GtkWidget * widget, const gchar * filename,
			 gboolean gnome_pixmap)
{
	GtkWidget *pixmap;
	GdkColormap *colormap;
	GdkPixmap *gdkpixmap;
	GdkBitmap *mask;
	gchar *pathname;

	pathname = gnome_pixmap_file(filename);
	if (!pathname) {
		g_warning(_("Couldn't find pixmap file: %s"), filename);
		return create_dummy_pixmap(widget, gnome_pixmap);
	}

	if (gnome_pixmap) {
		pixmap = gnome_pixmap_new_from_file(pathname);
		g_free(pathname);
		return pixmap;
	}

	colormap = gtk_widget_get_colormap(widget);
	gdkpixmap =
	    gdk_pixmap_colormap_create_from_xpm(NULL, colormap, &mask, NULL,
						pathname);
	if (gdkpixmap == NULL) {
		g_warning(_("Couldn't create pixmap from file: %s"), pathname);
		g_free(pathname);
		return create_dummy_pixmap(widget, gnome_pixmap);
	}
	g_free(pathname);

	pixmap = gtk_pixmap_new(gdkpixmap, mask);
	gdk_pixmap_unref(gdkpixmap);
	gdk_bitmap_unref(mask);
	return pixmap;
}

/* This is an internally used function to create imlib images. */
/* GdkImlibImage *create_image(const gchar * filename) */
/* { */
/* 	GdkImlibImage *image; */
/* 	gchar *pathname; */

/* 	pathname = gnome_pixmap_file(filename); */
/* 	if (!pathname) { */
/* 		g_warning(_("Couldn't find pixmap file: %s"), filename); */
/* 		return NULL; */
/* 	} */

/* 	image = gdk_imlib_load_image(pathname); */
/* 	g_free(pathname); */
/* 	return image; */
/* } */
