/*
     This file is part of GNUnet.
     (C) 2010 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/setup/gnunet-setup-datacache-config.c
 * @brief test datacache configuration
 * @author Christian Grothoff
 */
#include "gnunet-setup.h"
#include <gnunet/gnunet_datacache_plugin.h>

/**
 * Stub implementation of the 'DeleteNotifyCallback' callback.
 */
static void
dnc_dummy (void *cls, const struct GNUNET_HashCode * key, size_t size)
{
  /* do nothing */
}


/**
 * Test if the configuration works for the given plugin.
 *
 * @param name name of the plugin to check
 * @return GNUNET_OK on success, GNUNET_SYSERR if the config is wrong
 */
static int
test_config (const char *name)
{
  struct GNUNET_DATACACHE_PluginEnvironment env = {
    cfg,
    "dhtcache",
    NULL, &dnc_dummy,
    1024LL
  };
  void *ret;

  ret = GNUNET_PLUGIN_load (name, &env);
  if (NULL == ret)
    return GNUNET_SYSERR;
  GNUNET_PLUGIN_unload (name, ret);
  return GNUNET_OK;
}


static void
show (const char *name)
{
  gtk_widget_show (GTK_WIDGET (GNUNET_SETUP_get_object (name)));
}


static void
hide (const char *name)
{
  gtk_widget_hide (GTK_WIDGET (GNUNET_SETUP_get_object (name)));
}


void
GNUNET_setup_datacache_postgres_invalidate_cb ()
{
  hide ("GNUNET_setup_datacache_postgres_tab_ok_image");
  hide ("GNUNET_setup_datacache_postgres_tab_error_image");
}


void
GNUNET_setup_datacache_postgres_tab_test_button_clicked_cb (GtkWidget * widget,
                                                            gpointer user_data)
{
  if (GNUNET_OK == test_config ("libgnunet_plugin_datacache_postgres"))
  {
    show ("GNUNET_setup_datacache_postgres_tab_ok_image");
    hide ("GNUNET_setup_datacache_postgres_tab_error_image");
  }
  else
  {
    hide ("GNUNET_setup_datacache_postgres_tab_ok_image");
    show ("GNUNET_setup_datacache_postgres_tab_error_image");
  }
}



/* end of gnunet-setup-datacache-config.c */
