/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "pp_usertype.h"
# include "pp_abs.h"
# include "pp_tree.h"
# include "pp_proto.h"

/*|======================================================================|
  | SYRTHES 2.1                JANV 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | build_quadtree                                                       |
  |         Construction du quadtree                                     |
  |======================================================================| */
void pp_build_quadtree_2d (struct node *arbre,rp_int npoin,rp_int nelem,
			   rp_int **node,double **coord, double *size_min,
			   double dim_boite[],rp_int nbeltmax_tree)

{
  struct child *p1;
  struct element *f1,*f2;
    
  rp_int i,nbelt;
  double dx,dy,dz,dd,ddp;
  double xmin,xmax,ymin,ymax,zmin,zmax;
    

  /* calcul du rectangle englobant */
  xmin =  1.E10; ymin=  1.E6 ;
  xmax = -1.E10; ymax= -1.E6 ;
  
  for (i=0;i<npoin;i++)
    {
      xmin=min(coord[0][i],xmin);
      ymin=min(coord[1][i],ymin);
      xmax=max(coord[0][i],xmax);
      ymax=max(coord[1][i],ymax);
    }


  /* agrandir legerement le carre englobant */
  dx = xmax-xmin; dy=ymax-ymin;
  xmin -= (dx*0.01); ymin -= (dy*0.01); 
  xmax += (dx*0.01); ymax += (dy*0.01); 
  
  
  /* stockage des min et max du carre */
  dim_boite[0]=xmin; dim_boite[1]=xmax; 
  dim_boite[2]=ymin; dim_boite[3]=ymax; 
  
  arbre->xc = (xmin+xmax)*0.5;
  arbre->yc = (ymin+ymax)*0.5;
  arbre->sizx = dx*0.5;    
  arbre->sizy = dy*0.5;    
  arbre->lelement = NULL;
  arbre->lfils = NULL;
  *size_min = min(dx,dy);
  
  /* mise en place de la liste des elements dans le champ 'element' */
  f1 = (struct element *)malloc(sizeof(struct element));
  f1->num = 0;
  f1->suivant=NULL;
  arbre->lelement=f1;
  
  for (i=1;i<nelem;i++)
    {
      f2 = (struct element *)malloc(sizeof(struct element));
      f2->num = i;
      f2->suivant=NULL;
      f1->suivant = f2;
      f1 = f2;
    }
  nbelt = nelem;
  
  pp_decoupe2d(arbre,node,coord,nelem,npoin,nbelt,size_min,nbeltmax_tree);


  elague_tree(arbre); 

    /* printf("\n\n Arbre apres elaguage\n");
    affiche_tree(arbre,4);   */
      
}

/*|======================================================================|
  | SYRTHES 2.1                JANV 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decoupe2d                                                            |
  |         Construction de l'octree                                     |
  |======================================================================| */
void pp_decoupe2d(struct node *noeud,rp_int **node,double **coord,
		  rp_int nelem,rp_int npoin,rp_int nbelt,double *size_min,
		  rp_int nbeltmax_tree)
{
  double xmin[4],xmax[4],ymin[4],ymax[4];
  double x,y,dx,dy ;
  rp_int i,nbfac;
  struct node *n1,*n2,*noeudi;
  struct child *f1,*f2;
  struct element *elt1;
  
  if (nbelt>nbeltmax_tree)
    {
      /* calcul des xmin, xmax,... de chaque sous carre */
      x = noeud->xc; y = noeud->yc;
      dx = noeud->sizx; dy = noeud->sizy;
      
      xmax[0]=xmax[3]= x;
      xmin[1]=xmin[2]= x;
      xmin[0]=xmin[3]= x - dx;
      xmax[1]=xmax[2]= x + dx;

      ymax[2]=ymax[3]= y;
      ymin[0]=ymin[1]= y;
      ymin[2]=ymin[3]= y - dy;
      ymax[0]=ymax[1]= y + dy;


      /* generation des fils */
      f1= (struct child *)malloc(sizeof(struct child));
      n1= (struct node *) malloc(sizeof(struct node ));

      noeud->lfils = f1;
      f1->fils = n1;
      f1->suivant = NULL;

      for (i=1;i<4;i++)
	  {
	      f2= (struct child *)malloc(sizeof(struct child));
	      n2= (struct node *) malloc(sizeof(struct node ));
	      f1->suivant = f2;
	      f2->fils = n2;
	      f2->suivant = NULL;
	      f1 = f2;
	  }

      /* remplissage des noeuds lies aux fils crees */

      f1 = noeud->lfils;
      
      for (i=0;i<4;i++)
	  {
	      noeudi = f1->fils;
/*	      noeudi->name =  (noeud->name)*10 + i+1; */
	      noeudi->xc = (xmin[i]+xmax[i])*0.5;
	      noeudi->yc = (ymin[i]+ymax[i])*0.5;
	      noeudi->sizx = (xmax[i]-xmin[i])*0.5;
	      noeudi->sizy = (ymax[i]-ymin[i])*0.5;
	      *size_min = min(*size_min,noeudi->sizx);
	      *size_min = min(*size_min,noeudi->sizy);
	      noeudi->lfils = NULL;
	      elt1= (struct element *)malloc(sizeof(struct element));
	      noeudi->lelement = elt1;
	      
	      pp_tritria(noeud->lelement,noeudi->lelement,
			 &nbfac,nelem,npoin,node,coord,
			 noeudi->xc,noeudi->yc,noeudi->sizx,noeudi->sizy);

              if (nbfac != 0)
		pp_decoupe2d(noeudi,node,coord,nelem,npoin,nbfac,size_min,nbeltmax_tree);
	      else
		{
		  noeudi->lelement = NULL;
		  free(elt1);
		}
	      
	      f1 = f1->suivant;
	  }
      
    }

}

/*|======================================================================|
  | SYRTHES 2.1                JANV 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | tritria                                                              |
  |         Tri des triangles pour les placer dans le quadtree           |
  |======================================================================| */
void pp_tritria( struct element *face_pere, struct element *face_fils, 
		 rp_int *nbfac,rp_int nelem,rp_int npoin,rp_int **node,double **coord,
		 double xcc,double ycc,double dx,double dy)
{
  rp_int i,n,prem ;
  double xg,yg,epsi=1.E-5;
  struct element *fp1,*ff1,*ff2;
  
  prem = 1;
  fp1 = face_pere;
  ff1 = face_fils;
  *nbfac = 0;
  
  /* pour chaque element de la liste en cours */
  do
    {
      /* numero des noeuds de la element et coordonnees */
      n=node[0][fp1->num];  xg =coord[0][n];  yg =coord[1][n];
      n=node[1][fp1->num];  xg+=coord[0][n];  yg+=coord[1][n];
      n=node[2][fp1->num];  xg+=coord[0][n];  yg+=coord[1][n];
      
      xg/=3.;  yg/=3.;
      
      /* si le triangle appartient au carre
	 on le rajoute a la table des triangles du fils */
      if (xg>xcc-dx-epsi && xg<xcc+dx+epsi
	  && yg>ycc-dy-epsi && yg<ycc+dy+epsi)
	{
	  if (prem)
	    {
	      prem = 0;
	      ff1->num = fp1->num;
	      ff1->suivant = NULL;
	    }
	  else
	    {
	      ff2= (struct element *)malloc(sizeof(struct element));
	      ff2->num = fp1->num;
	      ff2->suivant = NULL;
	      ff1->suivant = ff2;
	      ff1 = ff2;
	    }
	  *nbfac += 1;
	  
	}
      
      fp1 = fp1->suivant;
      
    }while (fp1 != NULL);
  
}

