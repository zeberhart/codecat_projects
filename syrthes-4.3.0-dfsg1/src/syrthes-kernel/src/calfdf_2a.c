/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_option.h"
# include "syr_bd.h"
# include "syr_parall.h"
# include "syr_const.h"
# include "syr_proto.h"

/* # include "mpi.h" */


#define  nquartier 72
int tabint[nquartier];

extern struct Performances perfo;
extern struct Affichages affich;

int ss_sega[2][2]={ {0,2},
	             {2,1} };
int nsp,ordrei,ordrej;
double wi[24],xli[24],wj[24],xlj[24];


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | facforme_2a_fc                                                       |
  |           calcul des facteurs de forme en axisymetrique              |
  |           faces cachees                                              |
  |======================================================================| */
void facforme_2a(int nelem0,int npoin,int nelem,int iaxisy,int **nod,
		 double **coord,double **xnf,
		 struct FacForme ***listefdf,double *diagfdf,int *grcon,
		 struct Mask mask,
		 struct SDparall_ray sdparall_ray)
{
  int i,j,k,l,ik,il,id,ideb,n,icode,nelemloc,np,idecal;
  int noeud[6],prem,ndecoup,idec,jdec;
  int nbfcoplanaire;
  double xi[4],yi[4],fforme,xp[4],yp[4],xq[4],yq[4],xt[2],yt[2];
  double xn1,yn1,zn1,x,y,z,size_min,dim_boite[4],dsign[4];
  double l1,l2,d,x1m,y1m,x2m,y2m;
  struct node *arbre;
  double titi, titi1, titi2;
  int ncomplique,pasok;
  double total_fac,pourcent,pourcent_ecrit,valmask=1.;
  double xi_ini[4],yi_ini[4];
  double dist12,dist03;
  int ndecmax;
  double *lignefdf;
  int *existefdf;
  int nbffcree=0;
  struct FacForme *pff,*qff,*ppff;
  int iaxir ;
  int ligne_deb,ligne_fin;


  nbfcoplanaire=0 ;
    
  ndecmax=(int)(pow(2,ndecoup_max) +0.1);

  if (sdparall_ray.npartsray>1)    {
    ligne_deb=sdparall_ray.ieledeb[sdparall_ray.rang];
    ligne_fin=sdparall_ray.ieledeb[sdparall_ray.rang+1];
    nelemloc=sdparall_ray.nelrayloc[sdparall_ray.rang];
  }
  else {
    ligne_deb=0;
    ligne_fin=nelem0;
    nelemloc=nelem0;
  }

  lignefdf=(double*)malloc(nelem0*sizeof(double));
  existefdf=(int*)malloc(nelem0*sizeof(int));

	    
  titi1 =0;
  titi2 =0;
  pourcent_ecrit=0.1;

  for (i=ligne_deb;i<ligne_fin;i++) 
  /*-----------------*/
    {
      if(affich.ray_fdf) 
	if (SYRTHES_LANG == FR)
	  printf(" *** facforme_2a :  facette i=%d \n",i+1);
	else if (SYRTHES_LANG == EN)
	  printf(" *** facforme_2a :  face i=%d \n",i+1);

      pourcent=i/nelem;
      if (pourcent>pourcent_ecrit)
	{
	  if (SYRTHES_LANG == FR)
	    printf("              %5.2f %% du calcul effectue\n",pourcent_ecrit*100);
	  else if (SYRTHES_LANG == EN)
	    printf("              %5.2f %% of calculation done\n",pourcent_ecrit*100);
	  pourcent_ecrit += 0.1;
	}
      xn1=xnf[0][i];	  yn1=xnf[1][i];
      noeud[0]=nod[0][i]; noeud[1]=nod[1][i];
      for (k=0;k<nelem0;k++) {lignefdf[k]=0; existefdf[k]=0;}  /* on met les fdf de la ligne a 0 */ 
	  
      for  (j=0 ; j<nelem ; j++ ) 
      /*------------------------*/  
	{
	  if (grcon[i]!=grcon[j]) {continue;}

	  fforme=coplanaire_2a(iaxisy,xn1,yn1,xnf[0][j],xnf[1][j]);
	  if (fforme>-0.1) {nbfcoplanaire += 1; pasok=1;}
	  if (fforme<-1.)  
	    {
	      noeud[2]=nod[0][j]; noeud[3]=nod[1][j];
	      for (k=0;k<4;k++)
		{ 
		  xi_ini[k]=coord[0][noeud[k]];
		  yi_ini[k]=coord[1][noeud[k]];
		} 
	      
	      decouphor_2a(iaxisy,xi_ini,yi_ini,xn1,yn1,xnf[0][j],xnf[1][j]);
	      fforme=0.; pasok=0;
	      for (idec=0;idec<ndecmax;idec++)
		{
		  for(jdec=0;jdec<ndecmax;jdec++)
		    {
		      xi[0]=xi_ini[0]+(xi_ini[1]-xi_ini[0])/ndecmax*idec;
		      xi[1]=xi_ini[0]+(xi_ini[1]-xi_ini[0])/ndecmax*(idec+1);
		      yi[0]=yi_ini[0]+(yi_ini[1]-yi_ini[0])/ndecmax*idec;
		      yi[1]=yi_ini[0]+(yi_ini[1]-yi_ini[0])/ndecmax*(idec+1);
		      
		      xi[2]=xi_ini[2]+(xi_ini[3]-xi_ini[2])/ndecmax*jdec;
		      xi[3]=xi_ini[2]+(xi_ini[3]-xi_ini[2])/ndecmax*(jdec+1);
		      yi[2]=yi_ini[2]+(yi_ini[3]-yi_ini[2])/ndecmax*jdec;
		      yi[3]=yi_ini[2]+(yi_ini[3]-yi_ini[2])/ndecmax*(jdec+1);
		      
		      
		      for (n=0;n<nquartier;n++) tabint[n]=1;
		      
		      x1m=0.5*(xi[0]+xi[1]);y1m=0.5*(yi[0]+yi[1]);
		      x2m=0.5*(xi[2]+xi[3]);y2m=0.5*(yi[2]+yi[3]);
		      
		      iaxir=2 ;
		      derriere_2a(iaxir,i,j,xn1,yn1,xnf[0][j],xnf[1][j],x1m,y1m,x2m,y2m,&icode); 
/* 		      if (SYRTHES_LANG == FR) */
/* 			printf("  calfdf_2a : derriere_2a cas icode=%d\n",icode); */
/* 		      else if (SYRTHES_LANG == EN) */
/* 			printf("  calfdf_2a : derriere_2a case icode=%d\n",icode); */

		      if (icode==0)
			pasok=1;
		      else
			{
			  if (i==j)
			    {
			      l1=sqrt((xi[0]-xi[1])*(xi[0]-xi[1])+(yi[0]-yi[1])*(yi[0]-yi[1]));
			      if (iaxisy==1)
				d=0.5*(yi[0]+yi[1]);
			      else
				d=0.5*(xi[0]+xi[1]);
			      if (l1/d<0.3) 
				{ordrei=3; ordrej=4;}
			      else
				{ordrei=10;ordrej=12;}
			    }
			  else
			    {
			      d=sqrt((x2m-x1m)*(x2m-x1m)+(y2m-y1m)*(y2m-y1m));
			      l1=sqrt((xi[0]-xi[1])*(xi[0]-xi[1])+(yi[0]-yi[1])*(yi[0]-yi[1]));
			      l2=sqrt((xi[2]-xi[3])*(xi[2]-xi[3])+(yi[2]-yi[3])*(yi[2]-yi[3]));
			      if (l1/d<0.3) ordrei=3; else ordrei=10;
			      if (l2/d<0.3) ordrej=3; else ordrej=10;
			      
			      dist12=(xi[2]-xi[1])*(xi[2]-xi[1])+(yi[2]-yi[1])*(yi[2]-yi[1]) ;
			      dist03=(xi[3]-xi[0])*(xi[3]-xi[0])+(yi[3]-yi[0])*(yi[3]-yi[0]) ;
			      if (dist12 < 1e-10 || dist03 < 1e-10 )  { ordrei=24 ; ordrej=24;} 
			    }
			  
			  gauss();
			      
			  fforme=segfdfaxi(iaxisy,i,j,nelem,xi,yi,nod,coord);

			  if (affich.ray_fdf)
			    if (SYRTHES_LANG == FR)
			      printf(" Facteur de forme : i %d j %d  fdf= %f\n",i+1,j+1,1e6*fforme);
			    else if (SYRTHES_LANG == EN)
			      printf(" View factor : i %d j %d  fdf= %f\n",i+1,j+1,1e6*fforme);


			  if(fforme < 0.) 
			    {
			      if (affich.ray_fdf)
				if (SYRTHES_LANG == FR)
				  printf(" Facteur de forme negatif : i %d j %d  fdf= %f\n",i+1,j+1,1e6*fforme);
				else if (SYRTHES_LANG == EN)
				  printf(" Negative view factor :  i %d j %d  fdf= %f\n",i+1,j+1,1e6*fforme);

			    }
			  else if (pasok==0)
			    {lignefdf[j%nelem0] += fforme*valmask;  existefdf[j%nelem0]=1;}
			}
		    }
		}
	      
	    }
	}  /* for j*/


      /* reste a compacter la ligne calculee dans la listefdf et creer la diagonale */
      for (j=0;j<nelem0;j++)
	{
	  if (existefdf[j])
	    {
	      if (j==i)  /* on est sur la diagonale */
		{
/* 		  diagfdf[j]=lignefdf[j]; */
		  diagfdf[j-ligne_deb]=lignefdf[j];
		}
	      else
		{
		  /* sur quel proc est j ? */
		  idecal=np=0;
		  if (sdparall_ray.npartsray>1)
		    {
		      while (j>=sdparall_ray.ieledeb[np+1]) np++;
		      idecal=sdparall_ray.ieledeb[np];
		    }
		  qff=(struct FacForme*)malloc(sizeof(struct FacForme));
		  qff->numenface=j-idecal; 
		  qff->suivant=NULL;
		  qff->fdf=lignefdf[j];
		  nbffcree++;

		  if (!listefdf[np][i-ligne_deb])
		    listefdf[np][i-ligne_deb]=qff;
		  else
		    {
		      pff=listefdf[np][i-ligne_deb];
		      while(pff) {ppff=pff; pff=pff->suivant;}
		      ppff->suivant=qff;
		    }

		}
	    }
	}

      if (SYRTHES_LANG == FR)
	printf("facforme_2a : ligne %d : on a cree %d fdf\n",i,nbffcree);
      else if (SYRTHES_LANG == EN)
	printf("facforme_2a : line %d : one has stored %d fdf\n",i,nbffcree);
      
    } /*  for i */

  free(lignefdf); free(existefdf);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | segfdfaxi                                                            |
  |        organisation du calcul du facteur de forme entre 2 segments   |
  |        (axisymetrique)                                               |
  |======================================================================| */
double segfdfaxi (int iaxi,int ii,int jj,int nel2,double x[],double y[],
		  int **nod, double **coord)
 
{
  double xi,yi,xj,yj,xc,yc,xd,yd,xk,yk,xx,c0,cpi,s1,s2,s3,s4;
  double xmin,ymin,ymax,xn0,yn0,xnpi,ynpi;
  int i,iq1,iq2,n,ns,n1,n2;
  double xmax,xk_min,yk_min,xk_max,yk_max;
  double ytop_ab,ybot_ab,ytop_cd,ybot_cd,fforme;


  yi=0.5*(y[0]+y[1]); yj=0.5*(y[2]+y[3]); 
  xi=0.5*(x[0]+x[1]); xj=0.5*(x[2]+x[3]); 


/*   ytop_ab=max(y[0],y[1]); ybot_ab=min(y[0],y[1]); */
/*   ytop_cd=max(y[2],y[3]); ybot_cd=min(y[2],y[3]); */
/*   ymin=min(ybot_ab,ybot_cd)+1e-5; ymax=max(ytop_ab,ytop_cd)-1e-5; */
/*   optimisation CPU*/
  ymin=min(yi,yj)+1e-5; ymax=max(yi,yj)-1e-5; 

  xmin=min(xi,xj); xmax=max(xi,xj);

  xn0=-fabs(yj-yi); xnpi= fabs(yj-yi); 
  if (yi<yj) {yn0=xj-xi;} else {yn0=xi-xj;}
  if (yn0<0.) {ynpi=-xi-xj;} else {ynpi=xi+xj;}

  /* peut etre a revoir CP !!*/
  if (fabs(yj-yi)<1.e-5) 
    {xn0=0;yn0=1.;xnpi=0;ynpi=1; }
 
  xx=sqrt(xn0*xn0+yn0*yn0); xn0 /=xx; yn0 /=xx;
  xx=sqrt(xnpi*xnpi+ynpi*ynpi); xnpi /=xx; ynpi /=xx;
  c0=-xn0*xi-yn0*yi;   cpi=-xnpi*xi-ynpi*yi;
  
  for (i=0;i<nel2;i++)
    {

      /* printf(" test pour facette oculte  i= %d \n",i); */
      if (i==ii || i==jj) continue;

      n1=nod[0][i]; n2=nod[1][i];
      xc=coord[0][n1]; yc=coord[1][n1];
      xd=coord[0][n2]; yd=coord[1][n2];
      xk=0.5*(xc+xd); yk=0.5*(yc+yd); 

      xk_min=min(xc,xd); xk_max=max(xc,xd);

      yk_min=min(yc,yd); yk_max=max(yc,yd);
      
/*       if (yk_min > ymax || yk_max < ymin || xk_min >= xmax  || xk_max<=xmin) */
      if (yk_min > ymax || yk_max < ymin || xk_min >= xmax)
	{
	  /* printf(" on sort sur ce test yk=%f ymax=%f ymin=%f xkmin=%f  xmax=%f\n",yk_min,yk_max,ymin,xk_min,xmax); */
	   continue;
	 }

      s1=xn0*xc+yn0*yc+c0; s2=-xnpi*xc+ynpi*yc+cpi; 
      s3=xn0*xd+yn0*yd+c0; s4=-xnpi*xd+ynpi*yd+cpi;
      if (s1*s3>0 && s2*s4>0 && s1*s2>0 && fabs(yj-yi) < 1.e-5)
	{
	  /* printf(" on sort sur ce test s1=%f s2=%f s3=%f s4=%f \n",s1,s2,s3,s4); */
	  continue;
	}

      if (fabs(yi-yj) >  1e-5)
	quel_angle(ii,jj,xi,yi,xj,yj,xc,yc,xd-xc,yd-yc,&iq1,&iq2);
      else
	quel_angle_hide(ii,jj,xi,yi,xj,yj,xc,yc,xd-xc,yd-yc);
	
    }


  ns=0; 
  for (n=0;n<nquartier;n++) ns += tabint[n];


  if (ns==0) 
    fforme=0;
  else
    fforme=axi_integ_fc(x,y);

  return fforme;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | coplanaire_2a                                                        |
  |           retourne 0 si les faces sont coplanaires, -1000 sinon      |
  |           (pour de l'axisymetrique)                                  |
  |======================================================================| */
double coplanaire_2a (int iaxi,double xn1,double yn1,double xn2,double yn2)
{
  double dnx,dny,dnz,xn,epscop,eps0;
  
  epscop=1.E-3;
  eps0=1.E-8;
  dnx=xn1 - xn2;
  dny=yn1 - yn2;
  
  if (iaxi==2 && fabs(xn1)<eps0 && fabs(xn2)<eps0 && fabs(dny)<epscop)
    return(0);
  else if (iaxi==1 && fabs(yn1)<eps0 && fabs(yn2)<eps0 && fabs(dnx)<epscop)
    return(0);
  else
    return(-1000);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | axi_integ_vis                                                        |
  |           calcul du facteur de forme en axi                          |
  |           (pas de face cachee algo CP-IR)                            |
  |======================================================================| */
double axi_integ_vis (double x[4],double y[4])
{
  double c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15;
  double h,dzi,dzj,dxi,dxj,xi2,xj2,h2,F,t1,t2,li,lj,xi,xj;
  int i,j;

  t1=0; t2=Pi;


  dzi=y[1]-y[0];  dxi=x[1]-x[0];  
  dzj=y[3]-y[2];  dxj=x[3]-x[2];


  F=0;
  for (i=0;i<ordrei;i++)
    for (j=0;j<ordrej;j++)
      {
	li=0.5*xli[i]+0.5; lj=0.5*xlj[j]+0.5;
	xi=x[0]+li*dxi;      xj=x[2]+lj*dxj;
	h=y[2]-y[0]+lj*dzj-li*dzi;
	xi2=xi*xi; xj2=xj*xj; h2=h*h;
	
	c1=xi2*xj2*dzi*dzj;
	c2=-xi*xj*((xi2+xj2)*dzi*dzj-h*(xj*dxj*dzi-xi*dxi*dzj));
	c3=xi*xj*(xi*dzi+h*dxi)*(xj*dzj-h*dxj);
	c4=xi2+xj2+h2;
	c5=-2*xi*xj;
	c6=c4/c5;
	c7=c2-2*c1*c6;
	c8=c3-c1*c6*c6;
	c9=c7*c6-c8;
	c10=2*(c7-c6*c8);
	c11=sqrt(c6*c6-1);
	c12=(c6-1)/c11;
	
	c13=c1/(c5*c5);
	c14=1./(c5*c5*c11*c11);
	c15=c10/c11;
	


	
	if (c12>0)
	  F += wi[i]*wj[j] *(c13*(t2-t1)-c14*c15*Pi*0.5);
	else
	  F += wi[i]*wj[j] *(c13*(t2-t1)+c14*c15*Pi*0.5);
      }
  
  return F;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | axi_integ_vis                                                        |
  |           calcul du facteur de forme en axi                          |
  |           (faces cachees algo CP-IR)                                 |
  |======================================================================| */
double axi_integ_fc (double x[4],double y[4])
{
  double c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15;
  double h,dzi,dzj,dxi,dxj,xi2,xj2,h2,F,t1,t2,li,lj,xi,xj,pisnq;
  int i,j,ic,nc,couple[nquartier][2],n;


  dzi=y[1]-y[0];  dxi=x[1]-x[0];  
  dzj=y[3]-y[2];  dxj=x[3]-x[2];

  pisnq=Pi/(nquartier-1);
  ic=tabint[0];
  couple[0][0]=0;
  if (ic==1) nc=0; else nc=-1;

  for (i=1;i<nquartier;i++)
    if (tabint[i] != ic)
      { 
	if (ic==1) 
	  {couple[nc][1]=i-1; ic=0;} 
	else 
	  {nc +=1; couple[nc][0]=i; ic=1;} 
      }

  if (tabint[nquartier-1]==1) couple[nc][1]=nquartier-1;

  F=0;
  for (n=0;n<=nc;n++)
    {
      t1=pisnq*couple[n][0];
      t2=pisnq*couple[n][1];

      for (i=0;i<ordrei;i++)
	for (j=0;j<ordrej;j++)
	  {
	    li=0.5*xli[i]+0.5; lj=0.5*xlj[j]+0.5;
	    xi=x[0]+li*dxi;      xj=x[2]+lj*dxj;
	    h=y[2]-y[0]+lj*dzj-li*dzi;
	    xi2=xi*xi; xj2=xj*xj; h2=h*h;
	    
	    c1=xi2*xj2*dzi*dzj;
	    c2=-xi*xj*((xi2+xj2)*dzi*dzj-h*(xj*dxj*dzi-xi*dxi*dzj));
	    c3=xi*xj*(xi*dzi+h*dxi)*(xj*dzj-h*dxj);
	    c4=xi2+xj2+h2;
	    c5=-2*xi*xj;
	    c6=c4/c5;
	    c7=c2-2*c1*c6;
	    c8=c3-c1*c6*c6;
	    c9=c7*c6-c8;
	    c10=2*(c7-c6*c8);
	    c11=sqrt(c6*c6-1);
	    c12=(c6-1)/c11;
	    
	    c13=c1/(c5*c5);
	    c14=1./(c5*c5*c11*c11);
	    c15=c10/c11;
	

	    F += wi[i]*wj[j] *(
			       c13*(t2-t1)+
			       c14*(c9*( sin(t2)/(c6+cos(t2))-sin(t1)/(c6+cos(t1)) )
				    -c15*(atan(c12*tan(0.5*t2))-atan(c12*tan(0.5*t1)) )
				    )
			       );

	  }
    }
  
  return F;
}
  
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | gauss                                                                |
  |           calcul du facteur de forme en axi                          |
  |           (pas de face cachee)                                       |
  |======================================================================| */

void gauss()
{

  if (ordrei==3) 
    {
      xli[0]= 0.;
      xli[1]= 0.774596669241483;
      xli[2]=-0.774596669241483;
       wi[0]= 0.888888888888889;
       wi[1]=wi[2]= 0.555555555555556; 
    }
  else if (ordrei==4)
    {
      xli[0]=0.339981043584856;
      xli[1] =-0.339981043584856;
      xli[2]=0.861136311594053;
      xli[3] =-0.861136311594053;
       wi[0]=wi[1]=0.652145154862546;
       wi[2]=wi[3]=0.347854845137454;
    }
  else if (ordrei==5)
    {
      xli[0]=0.;
      xli[1]=0.538469310105683;
      xli[2] =-0.538469310105683;
      xli[3]=0.906179845938664;
      xli[4] =-0.906179845938664;
       wi[0]=0.568888888888889;
       wi[1]=wi[2]=0.478628670499366;
       wi[3]=wi[4]=0.236926885056189; 
    }
  else if (ordrei==10)
    {
      xli[0]=0.148874338981631;
      xli[1] =-0.148874338981631;
      xli[2]=0.433395394129247;
      xli[3] =-0.433395394129247;
      xli[4]=0.679409568299024;
      xli[5] =-0.679409568299024;
      xli[6]=0.865063366688985;
      xli[7] =-0.865063366688985;
      xli[8]=0.973906528517172;
      xli[9] =-0.973906528517172;
       wi[0]=wi[1]=0.295524224714753;
       wi[2]=wi[3]=0.269266719309996;
       wi[4]=wi[5]=0.219086362515982;
       wi[6]=wi[7]=0.149451349150581;
       wi[8]=wi[9]=0.066671344308688;
    }
  else if (ordrei==12)
    {
      xli[0]=0.1252334085;
      xli[1] =-0.1252334085;
      xli[2]=0.3678314989;
      xli[3] =-0.3678314989;
      xli[4]=0.5873179542;
      xli[5] =-0.5873179542;
      xli[6]=0.7699026741;
      xli[7] =-0.7699026741;
      xli[8]=0.9041172563;
      xli[9] =-0.9041172563;
     xli[10]=0.9815606342;
     xli[11] =-0.9815606342;
       wi[0]=wi[1]=0.2491470458;
       wi[2]=wi[3]=0.2334925365;
       wi[4]=wi[5]=0.2031674267;
       wi[6]=wi[7]=0.1600783285;
       wi[8]=wi[9]=0.1069393259;
      wi[10] =wi[11]=0.0471753363;
    }
  else if (ordrei==24)
    {
      xli[0]=0.0640568928;
      xli[1] =-0.0640568928;
      xli[2]=0.1911188674;
      xli[3] =-0.1911188674;
      xli[4]=0.3150426796;
      xli[5] =-0.3150426796;
      xli[6]=0.4337935076;
      xli[7] =-0.4337935076;
      xli[8]=0.5454214713;
      xli[9] =-0.5454214713;
     xli[10]=0.6480936519;
     xli[11] =-0.6480936519;
     xli[12]=0.7401241915;
     xli[13] =-0.7401241915;
     xli[14]=0.8200019859;
     xli[15] =-0.8200019859;
     xli[16]=0.8864155270;
     xli[17] =-0.8864155270;
     xli[18]=0.9382745520;
     xli[19] =-0.9382745520;
     xli[20]=0.9747285559;
     xli[21] =-0.9747285559;
     xli[22]=0.9951872199;
     xli[23] =-0.9951872199;
       wi[0]=wi[1] =0.1279381953;
       wi[2]=wi[3]=0.1258374563;
       wi[4]=wi[5]=0.1216704729;
       wi[6]=wi[7]=0.1155056680;
       wi[8]=wi[9]=0.1074442701;
      wi[10]=wi[11]=0.0976186521;
      wi[12]=wi[13]=0.0861901615;
      wi[14]=wi[15]=0.0733464814;
      wi[16]=wi[17]=0.0592985849;
      wi[18]=wi[19]=0.0442774388;
      wi[20]=wi[21]=0.0285313886;
      wi[22]=wi[23]=0.0123412297;
    }


  if (ordrej==3) 
    {
      xlj[0]=0.;
      xlj[1]=0.774596669241483;
      xlj[2] =-0.774596669241483;
       wj[0]=0.888888888888889;
       wj[1]=wj[2]=0.555555555555556; 
    }
  else if (ordrej==4)
    {
      xlj[0]=0.339981043584856;
      xlj[1] =-0.339981043584856;
      xlj[2]=0.861136311594053;
      xlj[3] =-0.861136311594053;
       wj[0]=wj[1]=0.652145154862546;
       wj[2]=wj[3]=0.347854845137454;
    }
  else if (ordrej==5)
    {
      xlj[0]=0.;
      xlj[1]=0.538469310105683;
      xlj[2] =-0.538469310105683;
      xlj[3]=0.906179845938664;
      xlj[4] =-0.906179845938664;
       wj[0]=0.568888888888889;
       wj[1]=wj[2]=0.478628670499366;
       wj[3]=wj[4]=0.236926885056189; 
    }
  else if (ordrej==10)
    {
      xlj[0]=0.148874338981631;
      xlj[1] =-0.148874338981631;
      xlj[2]=0.433395394129247;
      xlj[3] =-0.433395394129247;
      xlj[4]=0.679409568299024;
      xlj[5] =-0.679409568299024;
      xlj[6]=0.865063366688985;
      xlj[7] =-0.865063366688985;
      xlj[8]=0.973906528517172;
      xlj[9] =-0.973906528517172;
       wj[0]=wj[1]=0.295524224714753;
       wj[2]=wj[3]=0.269266719309996;
       wj[4]=wj[5]=0.219086362515982;
       wj[6]=wj[7]=0.149451349150581;
       wj[8]=wj[9]=0.066671344308688;
    }
  else if (ordrej==12)
    {
      xlj[0]=0.1252334085;
      xlj[1] =-0.1252334085;
      xlj[2]=0.3678314989;
      xlj[3] =-0.3678314989;
      xlj[4]=0.5873179542;
      xlj[5] =-0.5873179542;
      xlj[6]=0.7699026741;
      xlj[7] =-0.7699026741;
      xlj[8]=0.9041172563;
      xlj[9] =-0.9041172563;
     xlj[10]=0.9815606342;
     xlj[11] =-0.9815606342;
       wj[0]=wj[1]=0.2491470458;
       wj[2]=wj[3]=0.2334925365;
       wj[4]=wj[5]=0.2031674267;
       wj[6]=wj[7]=0.1600783285;
       wj[8]=wj[9]=0.1069393259;
      wj[10] =wj[11]=0.0471753363;
    }
  else if (ordrej==24)
    {
      xlj[0]=0.0640568928;
      xlj[1] =-0.0640568928;
      xlj[2]=0.1911188674;
      xlj[3] =-0.1911188674;
      xlj[4]=0.3150426796;
      xlj[5] =-0.3150426796;
      xlj[6]=0.4337935076;
      xlj[7] =-0.4337935076;
      xlj[8]=0.5454214713;
      xlj[9] =-0.5454214713;
     xlj[10]=0.6480936519;
     xlj[11] =-0.6480936519;
     xlj[12]=0.7401241915;
     xlj[13] =-0.7401241915;
     xlj[14]=0.8200019859;
     xlj[15] =-0.8200019859;
     xlj[16]=0.8864155270;
     xlj[17] =-0.8864155270;
     xlj[18]=0.9382745520;
     xlj[19] =-0.9382745520;
     xlj[20]=0.9747285559;
     xlj[21] =-0.9747285559;
     xlj[22]=0.9951872199;
     xlj[23] =-0.9951872199;
       wj[0]=wj[1]=0.1279381953;
       wj[2]=wj[3]=0.1258374563;
       wj[4]=wj[5]=0.1216704729;
       wj[6]=wj[7]=0.1155056680;
       wj[8]=wj[9]=0.1074442701;
      wj[10]=wj[11]=0.0976186521;
      wj[12]=wj[13]=0.0861901615;
      wj[14]=wj[15]=0.0733464814;
      wj[16]=wj[17]=0.0592985849;
      wj[18]=wj[19]=0.0442774388;
      wj[20]=wj[21]=0.0285313886;
      wj[22]=wj[23]=0.0123412297;
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | surface_seg                                                          |
  |           calcul de la longueur des segments                         |
  |======================================================================| */
void surface_anneau (struct Maillage maill)
{
  int i,*n0,*n1;
  double xa,ya,xb,yb;
  double xab,yab;
    
  if (maill.iaxisy==1)
    for (i=0,n0=maill.node[0],n1=maill.node[1];i<maill.nelem;i++,n0++,n1++)   
      {
	xa=maill.coord[0][*n0]; xb=maill.coord[0][*n1];        
	ya=maill.coord[1][*n0]; yb=maill.coord[1][*n1];
	xab=xb-xa;
	yab=yb-ya;
	maill.volume[i]=Pi*fabs(yb+ya)*sqrt(xab*xab + yab*yab); 
      }
  else
    for (i=0,n0=maill.node[0],n1=maill.node[1];i<maill.nelem;i++,n0++,n1++)   
      {
	xa=maill.coord[0][*n0]; xb=maill.coord[0][*n1];        
	ya=maill.coord[1][*n0]; yb=maill.coord[1][*n1];
	xab=xb-xa;
	yab=yb-ya;
	maill.volume[i]=Pi*fabs(xb+xa)*sqrt(xab*xab + yab*yab); 
      }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | quel_angle                                                           |
  |     detremination de l'angle d'integration                           |
  |======================================================================| */
void quel_angle(int ii,int jj,double xi, double  zi, double xj, double zj,
		double u0, double v0, double u1, double v1,
		int *iq1,int *iq2)
{

  double a,b,c,d,teta;
  double b1,b2,c1,c2,d1,d2,r1,r2,alpha,beta,gama,deltap;
  double rac1,rac2,delta,dteta,ct;
  double teta1,teta2,aa,bb,sqd;
  int    n,i,voir0,voirPi,ideb;
  double x3d_inter,z3d_inter,rac;
  double ratioz;


  a=-(zj-zi)*(zj-zi);


  b1=xi*xi+xj*xj; b2=-2*xi*xj;
  c1=-2*(xi*xi*zj+xj*xj*zi); c2=2*xi*xj*(zi+zj);
  d1=xi*xi*zj*zj+zi*zi*xj*xj; d2=-2*xi*xj*zi*zj;


  r1=(u1*v0-v1*u0)*(u1*v0-v1*u0);
  r2=u1*v1*u0-u1*u1*v0;
  
  alpha=v1*v1*(c2*c2-4*b2*d2);
  beta=4*a*(-b2*r1+c2*r2)+v1*v1*(2*c1*c2-4*(b1*d2+b2*d1))-4*u1*u1*a*d2;
  gama=4*a*(-b1*r1+c1*r2)+v1*v1*(c1*c1-4*b1*d1)-4*(u1*u1*a*d1);

  deltap=beta*beta-4*alpha*gama;
  teta1=0;
  if (deltap<0 && alpha<0)
      {/*printf("Aucune racine : pas d'intersection\n");*/ teta1=-1;}
  
    
  if (teta1>-0.1)
    {
      /* en 0 : ct=1 */
      if (ii==jj)
	{
	  voir0=1;
	}
      else if (fabs(-v1*(xj-xi)+u1*(zj-zi))<1.e-5)
	{
	  voir0=1;
	}
      else
	{
	  ct=1;
	  b=b1+b2;
	  c=c1+c2;
	  d=d1+d2;
	  delta=alpha + beta + gama;
	  voir0=1;
	  if (delta>=0)
	    {
	      aa=a*u1*u1+b*v1*v1;
	      bb=2*a*u0*u1+2*b*v1*v0+c*v1;
	      if (delta==0)
		rac1=rac2=-0.5*bb/aa;
	      else
		{
		  sqd=sqrt(delta);
		  rac1=0.5*(-bb-sqd)/aa;
		  rac2=0.5*(-bb+sqd)/aa;
		}

	      rac= -1e6;
	      if (rac1>=0 && rac1<=1) rac=rac1 ;
	      else if ( rac2>=0 && rac2<=1) rac=rac2 ;

	      if ( rac>=0 && rac<=1)
		{
		  ratioz=(fabs(v0+rac*v1)-zi)/(zj-zi);
		  if (0 <= ratioz && ratioz <= 1.) {voir0=0;tabint[0]=0 ;}
		}
	    }
	}

      /* en Pi : ct=-1 */
      ct=-1;
      b=b1-b2;
      c=c1-c2;
      d=d1-d2;
      delta=alpha - beta + gama;
      voirPi=1;
      if (delta>=0)
	{
	  aa=a*u1*u1+b*v1*v1;
	  bb=2*a*u0*u1+2*b*v1*v0+c*v1;
	  if (delta==0)
	    rac1=rac2=-0.5*bb/aa;
	  else
	    {
	      sqd=sqrt(delta);
	      rac1=0.5*(-bb-sqd)/aa;
	      rac2=0.5*(-bb+sqd)/aa;
	    }

	  rac=-1e6;
	  if (rac1>=0 && rac1<=1) rac=rac1 ;
	  else if ( rac2>=0 && rac2<=1) rac=rac2 ;

	  if ( rac>=0 && rac<=1)
	    {
	      ratioz=(fabs(v0+rac*v1)-zi)/(zj-zi);
	      if (0 < ratioz && ratioz < 1.) {voirPi=0;tabint[nquartier-1]=0;}
	    }
	}

      /* printf("voir0=%d voirPi=%d\n",voir0,voirPi);  */

      if (voir0==0 && voirPi==0) 
	{
	  /* printf("les facettes ne se voient jamais\n");*/
	  for(n=0;n<nquartier;n++)tabint[n]=0;
	}
      else
	{
	  dteta=Pi/(nquartier-1);
	  if (ii==jj || fabs(-v1*(xj-xi)+u1*(zj-zi))<1.e-5) {ideb=1;teta=dteta;}
	  else {ideb=0;teta=0;}

	  for(i=ideb;i<nquartier;i++)
	    {
	      if (tabint[i]==1)
		{
		  ct=cos(teta);
		  b=b1+b2*ct;
		  c=c1+c2*ct;
		  d=d1+d2*ct;
		  
		  delta=alpha*ct*ct + beta*ct + gama;
		  
		  if (delta>=0)
		    {
		      aa=a*u1*u1+b*v1*v1;
		      bb=2*a*u0*u1+2*b*v1*v0+c*v1;
		      if (delta==0)
			rac1=rac2=-0.5*bb/aa;
		      else
			{
			  sqd=sqrt(delta);
			  rac1=0.5*(-bb-sqd)/aa;
			  rac2=0.5*(-bb+sqd)/aa;
			}
		      
		      rac=-1e6;
		      if (rac1>=0 && rac1<=1) rac=rac1 ;
		      else if ( rac2>=0 && rac2<=1) rac=rac2 ;

		      /*   printf(" i=%d   rac1=%f rac2=%f \n",i,rac1,rac2); */

		      if ( rac>=0 && rac<=1)
			{
			  ratioz=(fabs(v0+rac*v1)-zi)/(zj-zi);
			  if (0 < ratioz && ratioz < 1.) tabint[i]=0 ;

			  if (voir0==1)
			    {
/* 			      if (voirPi==0) {for(n=i;n<nquartier;n++)tabint[n]=0;break;} */
			    }
			}
		    }
		}
	      teta+=dteta;
	    }
	}
      
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | tab_integ                                                            |
  |      mise a jour des indicateurs d'integration                       |
  |======================================================================| */
void tab_integ(int iq1,int iq2)
{
  int i;
  
  for(i=iq1;i<=iq2;i++) tabint[i]=0;
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | derriere                                                             |
  |         Detection des segments en arriere et retour d'un code de     |
  |         Classement   (axisymetrique)                                 |
  |======================================================================| */

void derriere_2a (int iaxi,int i,int j,double xni0,double yni0,double xnj0,double ynj0,
		  double xi,double yi,double xj,double yj,
		  int *icode)
{
  int n;
  double xnipi,ynipi,xnjpi,ynjpi,xij0,yij0,xijpi,yijpi;
  double psi0,psj0,psipi,psjpi,xij,yij,zij,xnj,ynj,znj,psi,psj;
  double pisnq,t,epsi;


  epsi=1.e-6;
  if (i==j)
    {
      if (iaxi==2 && xni0>0) {*icode=0;   /*printf("    --> face concave\n");*/ }
      else if (iaxi==1 && yni0>0) {*icode=0;   /*printf("    --> face concave\n");*/ }
      else *icode=1;
    }
  else
    {
      pisnq=Pi/(nquartier-1);

      xnipi=-xni0; ynipi=yni0; xnjpi=-xnj0; ynjpi=ynj0; 
      xij0=xj-xi; yij0=yj-yi;  xijpi=-xj-xi; yijpi=yj-yi;
      
       psi0=xij0*xni0+yij0*yni0;        psj0=-(xij0*xnj0+yij0*ynj0);  
       psipi=xijpi*xni0+yijpi*yni0;   psjpi=-(xijpi*xnjpi+yijpi*ynjpi);
      
      if ((psi0<-epsi || psj0<-epsi) && (psipi<-epsi || psjpi<-epsi)) 
	{*icode=0; /*printf("    --> ne se voient jamais\n");*/}
      else if (psi0>0 && psipi>0 && psj0>0 && psjpi>0) 
	{*icode=1;/*printf("    --> se voient potentiellement toujours\n");*/}
      else
	{
	  *icode=2;
	  for (n=0;n<nquartier;n++) 
	    {
	      t=pisnq*n;
	      xij=xj*cos(t)-xi; yij=xj*sin(t);zij=yj-yi;
	      xnj=xnj0*cos(t); ynj=xnj0*sin(t);  znj=ynj0;
	      psi= xij*xni0+zij*yni0;  psj=-(xij*xnj+yij*ynj+zij*znj);
	      if (psi<0 || psj<0) tabint[n]=0;
	    }
	}
    }
} 

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | quel_angle_ide                                                       |
  |     detremination de l'angle d'integration (hauteur identique)       |
  |======================================================================| */
void quel_angle_hide (int ii,int jj,double xi, double  zi, double xj, double zj,
		      double u0, double v0, double u1, double v1)

{

  double teta;
  double alpha,beta,gama,u;
  double rac1,rac2,delta,dteta,ct;
  double teta1,teta2,aa,bb,sqd;
  int    n,i,voir0,voirPi,ind,tet[2],ideb;
  int    test;

  test=0;
  if ( v1 > 0 ) {if (zi < v0 ||  v0+v1 < zi)    return ; else  test=1;}
  else { if (zi < v0+v1 ||  v0 < zi)    return ; test=1; }

  if (test == 1)
    {
      u=u0 + (zi-v0)/v1*u1;
      alpha=xi*xi+xj*xj;
      beta=-2*xi*xj*u*u;
      gama=(xi*xi+xj*xj)*u*u-alpha;

      /* en 0 : ct=1 */
      if (ii==jj)
	{
	  voir0=1;
	}
      else if (fabs(-v1*(xj-xi)+u1*(zj-zi))<1.e-5)
	{
	  voir0=1;
	}
      else
	{
	  delta=alpha + beta + gama;
	  voir0=1;
	  if (delta>=0)
	    {
	      aa=alpha-2*xi*xj;
	      bb=xi*(xj-xi) ;
	      if (delta==0)
		rac1=rac2=-bb/aa;
	      else
		{
		  sqd=sqrt(delta);
		  rac1=(-bb-sqd)/aa;
		  rac2=(-bb+sqd)/aa;
		}
	      if (rac1>=0 && rac1<=1  || rac2>=0 && rac2<=1) 
		{voir0=0; tabint[0]=0;}
	    }
	}

      /* en Pi : ct=-1 */
      delta=alpha - beta + gama;
      voirPi=1;
      if (delta>=0)
	{
	  aa=alpha+2*xi*xj;
	  bb=xi*(-xj-xi);
	  if (delta==0)
	    rac1=rac2=-bb/aa;
	  else
	    {
	      sqd=sqrt(delta);
	      rac1=(-bb-sqd)/aa;
	      rac2=(-bb+sqd)/aa;

	    }
	  if (rac1>=0 && rac1<=1  || rac2>=0 && rac2<=1) 
	    {voirPi=0; tabint[nquartier-1]=0;}
	}

      /* printf("voir0=%d voirPi=%d\n",voir0,voirPi);  */

      if (voir0==0 && voirPi==0) 
	{
	  /* printf("les facettes ne se voient jamais\n");*/
	  tet[0]=tet[1]=0;  for(n=0;n<nquartier;n++)tabint[n]=0;
	}
      else
	{
	  tet[0]=0; tet[1]=nquartier-1; ind=0;
	  dteta=Pi/(nquartier-1);
	  if (ii==jj || fabs(-v1*(xj-xi)+u1*(zj-zi))<1.e-5) {ideb=1;teta=dteta;}
	  else {ideb=0;teta=0;}

	  for(i=ideb;i<nquartier;i++)
	    {
	      if (tabint[i]==1)
		{
		  ct=cos(teta);		  
		  delta=alpha*ct*ct + beta*ct + gama;
		  
		  if (delta>=0)
		    {
		      aa=alpha-2*xi*xj*ct;
		      bb=xi*(xj*ct-xi);
		      if (delta==0)
			rac1=rac2=-bb/aa;
		      else
			{
			  sqd=sqrt(delta);
			  rac1=(-bb-sqd)/aa;
			  rac2=(-bb+sqd)/aa;
			}
		      if (rac1>=0 && rac1<=1  || rac2>=0 && rac2<=1)
			{ 
                          tabint[i]=0; 
			  if (voir0==1)
			    {
			      if (voirPi==0) {tet[1]=i;for(n=i;n<nquartier;n++)tabint[n]=0;break;}
			      else if (voirPi==1 && ind==0) {tet[0]=i;ind=1;}
			    }
			}
		      else
			{
			  if (voir0==0) {tet[0]=i;break;}
			  else if (voir0==1 && voirPi==1 && ind==1) {tet[1]=i;break;}
			}
		    }
		  /* if (delta<0) printf("teta=%12.4e , delta < 0 \n",teta); 
		     else
		     printf("teta=%12.4e delta=%12.4e rac1=%12.4e rac2=%12.4e \n",teta,delta,rac1,rac2); 
		  */
		}
	      teta+=dteta;
	    }
	}
    }

}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decouphor_2a                                                         |
  |         Decoupage de segments  optimisation qualite                  |
  |                                                                      |
  |======================================================================| */
void decouphor_2a(int iaxi,double *xi_ini,double *yi_ini,double xn_i,
		  double yn_i,double xn_j,double yn_j)

{
  
  double epsih=1.e-5,epsd=1e-4,eps=1e-6;
  double denom,numer,alfa,s0,s1,s2,s3;
  double pland_i,pland_j ;
  double iaxir ;


  pland_i=-xi_ini[0]*xn_i-yi_ini[0]*yn_i;
  pland_j=-xi_ini[2]*xn_j-yi_ini[2]*yn_j;

  s0=xi_ini[0]*xn_j+yi_ini[0]*yn_j+pland_j ;
  s1=xi_ini[1]*xn_j+yi_ini[1]*yn_j+pland_j ;
  s2=xi_ini[2]*xn_i+yi_ini[2]*yn_i+pland_i ;
  s3=xi_ini[3]*xn_i+yi_ini[3]*yn_i+pland_i ;

  iaxir =2;
  
  /* detection facettes (wilkinson) */
  if (iaxir==1) 
    {
      printf(" sans objet - decision cp \n");
    }
  else
    {
      if (fabs(xn_i) < epsih && s2*s3 < 0) 
	{
	  if ( s2 > 0 ) 
	    {
	      denom=xn_i*(xi_ini[3]-xi_ini[2]) + yn_i *(yi_ini[3]-yi_ini[2]);
	      
	      if ( fabs(denom) > eps )
		{
		  numer= xn_i*xi_ini[2]+ yn_i*yi_ini[2]+ pland_i ;
		  alfa =- numer/denom - epsd ;
		      
		  xi_ini[3]= xi_ini[2]+ alfa*(xi_ini[3]-xi_ini[2]);
		  yi_ini[3]= yi_ini[2]+ alfa*(yi_ini[3]-yi_ini[2]);
		}
	    }
	  else
	    {
	      denom=xn_i*(xi_ini[3]-xi_ini[2]) + yn_i *(yi_ini[3]-yi_ini[2]);
	      
	      if ( fabs(denom) > eps )
		{
		      numer= xn_i*xi_ini[2]+ yn_i*yi_ini[2]+ pland_i ;
		      alfa =- numer/denom + epsd ;
		      
		      xi_ini[2]= xi_ini[2]+ alfa*(xi_ini[3]-xi_ini[2]);
		      yi_ini[2]= yi_ini[2]+ alfa*(yi_ini[3]-yi_ini[2]);
		    }
	    }
	    }
      
      else if  (fabs(xn_j) < epsih && s0*s1 < 0) 
	{
	  if ( s0 > 0 ) 
	    {
	      /* deuxieme segment (wilkinson) */
	      denom=xn_j*(xi_ini[1]-xi_ini[0]) + yn_j *(yi_ini[1]-yi_ini[0]);
	      
	      if ( fabs(denom) > eps )
		{
		  numer= xn_j*xi_ini[0]+ yn_j*yi_ini[0]+ pland_j ;
		  alfa =- numer/denom - epsd ;
		  
		  xi_ini[1]= xi_ini[0]+ alfa*(xi_ini[1]-xi_ini[0]);
		  yi_ini[1]= yi_ini[0]+ alfa*(yi_ini[1]-yi_ini[0]);
		}
	    }
	  else
		{
		  denom=xn_j*(xi_ini[1]-xi_ini[0]) + yn_j *(yi_ini[1]-yi_ini[0]);
		  
		  if ( fabs(denom) > eps )
		    {
		      numer= xn_j*xi_ini[0]+ yn_j*yi_ini[0]+ pland_j ;
		      alfa =- numer/denom + epsd ;
		      
		      xi_ini[0]= xi_ini[0]+ alfa*(xi_ini[1]-xi_ini[0]);
		      yi_ini[0]= yi_ini[0]+ alfa*(yi_ini[1]-yi_ini[0]);
		    }
		}
	  
	}
      
      else
	{
	  if (s2*s3 < 0 && xn_i >=0 )
	    { 
	      /* i (transformation wilkinson) j */
	      if ( s2 > 0 ) 
		{
		  denom=xn_i*(xi_ini[3]-xi_ini[2]) + yn_i *(yi_ini[3]-yi_ini[2]);
		  
		  if ( fabs(denom) > eps )
		    {
		      numer= xn_i*xi_ini[2]+ yn_i*yi_ini[2]+ pland_i ;
		      alfa =- numer/denom - epsd ;
		      
		      xi_ini[3]= xi_ini[2]+ alfa*(xi_ini[3]-xi_ini[2]);
		      yi_ini[3]= yi_ini[2]+ alfa*(yi_ini[3]-yi_ini[2]);
		    }
		}
	      else
		{
		  denom=xn_i*(xi_ini[3]-xi_ini[2]) + yn_i *(yi_ini[3]-yi_ini[2]);
		  
		  if ( fabs(denom) > eps )
		    {
		      numer= xn_i*xi_ini[2]+ yn_i*yi_ini[2]+ pland_i ;
		      alfa =- numer/denom + epsd ;
		      
		      xi_ini[2]= xi_ini[2]+ alfa*(xi_ini[3]-xi_ini[2]);
		      yi_ini[2]= yi_ini[2]+ alfa*(yi_ini[3]-yi_ini[2]);
		    }
		}
	    }
	  
	  else if (s0*s1 < 0 && xn_j >=0 )
	    {
	      if ( s0 > 0 ) 
		{
		  denom=xn_j*(xi_ini[1]-xi_ini[0]) + yn_j *(yi_ini[1]-yi_ini[0]);

		  if ( fabs(denom) > eps )
		    {
		      numer= xn_j*xi_ini[0]+ yn_j*yi_ini[0]+ pland_j ;
		      alfa =- numer/denom - epsd ;
		      
		      xi_ini[1]= xi_ini[0]+ alfa*(xi_ini[1]-xi_ini[0]);
		      yi_ini[1]= yi_ini[0]+ alfa*(yi_ini[1]-yi_ini[0]);
		    }
		}
	      else
		{
		  denom=xn_j*(xi_ini[1]-xi_ini[0]) + yn_j *(yi_ini[1]-yi_ini[0]);
		  
		  if ( fabs(denom) > eps )
		    {
		      numer= xn_j*xi_ini[0]+ yn_j*yi_ini[0]+ pland_j ;
		      alfa = - numer/denom + epsd ;
		      
		      xi_ini[0]= xi_ini[0]+ alfa*(xi_ini[1]-xi_ini[0]);
		      yi_ini[0]= yi_ini[0]+ alfa*(yi_ini[1]-yi_ini[0]);
		    }
		}

	    }
	}
    }
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | alter_axi1                                                           |
  |         alteration des coordonnees                                   |
  |                                                                      |
  |======================================================================| */
void alter_axi1(int change,struct Maillage maill,struct SymPer defsymper)
{
  int i;
  double aa;

  if (change==1) /* inversion */
    {
      for (i=0;i<maill.npoin;i++) 
	{
	  aa=maill.coord[0][i];
	  maill.coord[0][i]=maill.coord[1][i];
	  maill.coord[1][i]=-aa;
	}
      if (defsymper.nbsym==1) /* il y a au max 1 sym */
	{
	  aa=defsymper.sym[0][0];
	  defsymper.sym[0][0]=-defsymper.sym[0][1];
	  defsymper.sym[0][1]=aa;
	}
    }
  else  /* retour a la normale */
    {
      for (i=0;i<maill.npoin;i++) 
	{
	  aa=maill.coord[0][i];
	  maill.coord[0][i]=-maill.coord[1][i];
	  maill.coord[1][i]=aa;
	}
      if (defsymper.nbsym==1) /* il y a au max 1 sym */
	{
	  aa=defsymper.sym[0][0];
	  defsymper.sym[0][0]=defsymper.sym[0][1];
	  defsymper.sym[0][1]=-aa;
	}
    }
}






