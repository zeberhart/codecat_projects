��    @        Y         �  %   �  '   �  &   �     �  (         :     [     _     g     �     �     �  
   �     �     �     �     �     �  /        @     E     L     R  )   X  %   �     �  
   �  	   �     �     �     �  	   �     �     �     �     �            5     9   S     �  :   �  $   �     	     
	     	     	     	  	   #	  #   -	  ?   Q	  @   �	     �	     �	     �	     �	     �	     �	     �	     
     
     

     
  �  
  +   �  '     .   9     h  -   �  !   �  	   �     �  (   �       (   2     [     b     x     �  
   �     �     �  %   �     �     �     �     �  5   �  1   /     a     h     t     �     �     �  	   �     �     �     �     �     �     �  5   �  >   #     b  6   �  '   �     �     �     �     �     �     �  2      <   3  8   p     �  
   �     �     �     �     �     �     �     �     �     �        6                      *      ?       /      !       9   :         #   +                              8                     -       "   2       	       
   &             <   3          ;       4                  ,       '         @      (   $   .          )   =   0           7   1          %                 >       5    %s can not be converted to a currency %s could not be converted to an integer '%s' can not be converted to a boolean '%s' is not a valid object '%s' is not a valid value for this field A file named "%s" already exists Any Boolean Could not load image: %s Could not open file "%s" Could not select folder "%s" Currency Custom day Custom interval Date Date and Time Decimal Do you want to remove %s ? Do you wish to replace it with the current one? Enum Finish Float From: Inproperly placed thousand separators: %r Inproperly placed thousands separator Integer Last month Last week Long Object Open Password: Replace Save Search: Select folder Show more _details String The file "%s" could not be opened. Permission denied. The folder "%s" could not be selected. Permission denied. This field is mandatory This field requires a date of the format "%s" and not "%s" This field requires a number, not %r Time To: Today Total: Unicode Yesterday You cannot enter a year before 1900 You have a thousand separator to the right of the decimal point You have more than one decimal point ("%s")  in your number "%s" _Cancel _Select _Today dd hh hh:mm:ss hh:mm:ss LL mm ss yy yyyy Project-Id-Version: kiwi 1.9.15
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-05-28 17:38-0300
PO-Revision-Date: 2007-11-11 11:11+0100
Last-Translator: Sandro Bonazzola <sandrobonazzola@users.sourceforge.net>
Language-Team: Sandro Bonazzola <sandrobonazzola@users.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Language: Italian
X-Poedit-Country: ITALY
 %s non può essere convertito in una valuta %s non può essere convertito in intero '%s' non può essere convertito in un booleano '%s' non è un oggetto valido '%s' non è un valore valido per questo campo Un file chiamato "%s" esiste già Qualsiasi Booleano Non è possibile caricare l'immagine: %s Impossibile aprire il file "%s" Impossibile selezionare la cartella "%s" Valuta Giorno personalizzato Intervallo personalizzato Data Data e ora Decimale Vuoi cancellare %s? Vuoi sostituirlo con quello corrente? Enum Fine Float Da: Separatori di migliaia posizionati impropriamente: %r Separatore di migliaia posizionato impropriamente Intero Mese scorso Settimana scorsa Lungo Oggetto Apri Password: Sostituisci Salva Cerca: Seleziona cartella Mostra altri _dettagli Stringa Il file "%s" non può essere aperto. Permesso negato. La cartella "%s" non può essere selezionata. Permesso negato. Questo campo è obbligatorio. Il campo richiede una data nel formato "%s" e non "%s" Questo campo richiede un numero, non %r Ora A: Oggi Totale: Unicode Ieri Non è possibile inserire un anno anteriore a 1900 Hai un separatore di migliaia alla destra del punto decimale Hai più di un punto decimale ("%s") nel tuo numero "%s" _Annulla _Seleziona _Oggi gg hh hh:mm:ss hh:mm:ss LL mm ss aa aaaa 