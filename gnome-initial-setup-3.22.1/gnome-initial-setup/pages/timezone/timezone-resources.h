#ifndef __RESOURCE_timezone_H__
#define __RESOURCE_timezone_H__

#include <gio/gio.h>

extern GResource *timezone_get_resource (void);
#endif
