/*
     This file is part of GNUnet
     (C) 2005, 2006, 2010, 2012 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/fs/gnunet-fs-gtk_open-uri.h
 * @author Christian Grothoff
 * @brief code for the 'Open URI' dialog.
 */
#ifndef GNUNET_FS_GTK_OPEN_URI_H
#define GNUNET_FS_GTK_OPEN_URI_H

/**
 * Handle a URI string by running the appropriate action.
 *
 * @param uris string we got
 * @param anonymity_level anonymity level to use
 * @return #GNUNET_OK on success,
 *         #GNUNET_NO if the URI type is not supported,
 *         #GNUNET_SYSERR if we failed to parse the URI
 */
int
GNUNET_FS_GTK_handle_uri_string (const char *uris,
				 guint anonymity_level);

#endif
