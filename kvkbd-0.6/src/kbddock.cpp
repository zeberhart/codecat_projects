/*
 * This file is part of the Kvkbd project.
 * Copyright (C) 2007-2008 Todor Gyumyushev <yodor@developer.bg>
 * Copyright (C) 2008 Guillaume Martres <smarter@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "kbddock.h"

#include <QDBusConnection>
#include <QPainter>
#include <QWidget>
#include <QMouseEvent>

#include <KIconLoader>
#include <KLocale>

#include "dockadaptor.h"

KbdDock::KbdDock(QWidget *parent)
        : DragWidget(0, Qt::X11BypassWindowManagerHint)
{
    new DockAdaptor(this);
    QDBusConnection dbus = QDBusConnection::sessionBus();
    dbus.registerObject("/Dock", this);

    window = parent;

    setFocusPolicy(Qt::NoFocus);
    icn = UserIcon("dock");
    resize(96, 47);
    setAttribute(Qt::WA_AlwaysShowToolTips);
    setToolTip(i18nc("@info:tooltip Displayed on the movable dock", "Toggle keyboard visibility"));

    setVisible(false);
    moved = false;
}

QWidget* KbdDock::parentWidget() const
{
    return window;
}

void KbdDock::setVisible(bool visible)
{
    if (visible != isVisible()) {
        QWidget::setVisible(visible);
        emit(dockShown(visible));
    }
}

void KbdDock::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.drawPixmap(0, 0, icn);
}

KbdDock::~KbdDock()
{
}

void KbdDock::mouseMoveEvent(QMouseEvent *e)
{
    if (!moved && isDragged()) {
        moved = true;
    }
    DragWidget::mouseMoveEvent(e);
}
void KbdDock::mouseReleaseEvent(QMouseEvent *e)
{
    QWidget *pw = window;

    if (moved) {
        DragWidget::mouseReleaseEvent(e);
        moved = false;
        return;
    }

    if (pw->isVisible()) {
        pw->hide();
    } else {
        pw->show();
    }
    DragWidget::mouseReleaseEvent(e);
}
