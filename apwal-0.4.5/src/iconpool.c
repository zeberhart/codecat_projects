/* iconpool.c
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* NOT USED AT THE MOMENT */
/* NOT USED AT THE MOMENT */

#include "common.h"
#include <unistd.h>
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#define ICONPOOL_MAXTHREAD 3
#define ICONPOOL_TASK_START 0x1000
#define ICONPOOL_ADD_FOLDER (ICONPOOL_TASK_START+0)
#define ICONPOOL_DEL_FOLDER (ICONPOOL_TASK_START+1)
#define ICONPOOL_ADD_ICON   (ICONPOOL_TASK_START+2)
#define ICONPOOL_DEL_ICON   (ICONPOOL_TASK_START+3) // ???
#define ICONPOOL_TASK_END   (ICONPOOL_TASK_START+3)


typedef struct iconpool_t
{
  GThreadPool *threadpool;
  GError      *gerror;
} iconpool_t;
typedef struct iconpool_task_t
{
  gint type;
  union
  {  char c;
     int i;
  } data;
} iconpool_task_t;

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
void iconpool_do(gpointer data, gpointer userdata)
{
  TRACE("hello %d, %p", GPOINTER_TO_INT(data), g_thread_self());
  sleep(1);
}
// ----------------------------------------------------------------------------
iconpool_t* iconpool_new()
{
  iconpool_t *iconpl;
  iconpl = (iconpool_t*) malloc(sizeof(iconpool_t));
  g_assert(iconpl != NULL);
  iconpl->gerror = NULL;
  iconpl->threadpool = g_thread_pool_new(iconpool_do, GINT_TO_POINTER(123456),
                                         ICONPOOL_MAXTHREAD/*maxthread*/,
                                         TRUE/*excusive*/, &iconpl->gerror);
  if (iconpl->gerror != NULL)
    ERR("%s", "g_thread_pool_new failed");
  return iconpl;
}
// ----------------------------------------------------------------------------
void iconpool_free(iconpool_t *iconpl)
{
  g_thread_pool_free(iconpl->threadpool, FALSE/*imediate*/, TRUE/*wait*/);
  free(iconpl);
}
// ----------------------------------------------------------------------------
void iconpool_push(iconpool_t *iconpl, iconpool_task_t *task)
{
  g_assert(!task);
  g_assert(task->type>=ICONPOOL_TASK_START && task->type<=ICONPOOL_TASK_END);
  
}


int main(int argc, char **argv)
{
  int i;
  iconpool_t *iconpl;
  GError *e = NULL;
  //gtk_init(&argc, &argv);
  g_thread_init(NULL);
  iconpl = iconpool_new();
  for(i=123456; i<123466; i++)
  {
    g_thread_pool_push(iconpl->threadpool, GINT_TO_POINTER(i), &e);
    TRACE("%p", e);
  }
  
  iconpool_free(iconpl);
  return 0;
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
