/*
     This file is part of GNUnet.
     (C) 2010, 2011 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/setup/gnunet-setup-transport-test.c
 * @brief support for testing transport configurations
 * @author Christian Grothoff
 */
#include "gnunet-setup.h"
#include <gnunet/gnunet_resolver_service.h>
#include <gnunet/gnunet_nat_lib.h>

/**
 * How long do we wait for the NAT test to report success?
 */
#define TIMEOUT GNUNET_TIME_relative_multiply (GNUNET_TIME_UNIT_SECONDS, 15)


struct TestContext
{

  /**
   * Handle to the active NAT test.
   */
  struct GNUNET_NAT_Test *tst;

  /**
   * Task identifier for the timeout.
   */
  GNUNET_SCHEDULER_TaskIdentifier tsk;

  /**
   * Name of widget to show on success.
   */
  const char *success_image;

  /**
   * Name of widget to show on failure.
   */
  const char *failure_image;

};


/**
 * Display the result of the test.
 *
 * @param tc test context
 * @param result GNUNET_YES on success
 */
static void
display_test_result (struct TestContext *tc, int result)
{
  GObject *w;

  if (GNUNET_YES != result)
  {
    w = GNUNET_SETUP_get_object (tc->failure_image);
    if (NULL != w)
      gtk_widget_show (GTK_WIDGET (w));
    w = GNUNET_SETUP_get_object (tc->success_image);
    if (NULL != w)
      gtk_widget_hide (GTK_WIDGET (w));
  }
  else
  {
    w = GNUNET_SETUP_get_object (tc->failure_image);
    if (NULL != w)
      gtk_widget_hide (GTK_WIDGET (w));
    w = GNUNET_SETUP_get_object (tc->success_image);
    if (NULL != w)
      gtk_widget_show (GTK_WIDGET (w));
  }
  if (GNUNET_SCHEDULER_NO_TASK != tc->tsk)
  {
    GNUNET_SCHEDULER_cancel (tc->tsk);
    tc->tsk = GNUNET_SCHEDULER_NO_TASK;
  }
  if (NULL != tc->tst)
  {
    GNUNET_NAT_test_stop (tc->tst);
    tc->tst = NULL;
  }
  GNUNET_free (tc);
}


/**
 * Function called by NAT on success.
 * Clean up and update GUI (with success).
 *
 * @param cls test context
 * @param success currently always #GNUNET_OK
 * @param emsg error message, NULL on success
 */
static void
result_callback (void *cls, int success,
                 const char *emsg)
{
  struct TestContext *tc = cls;

  display_test_result (tc, success);
}


/**
 * Function called if NAT failed to confirm success.
 * Clean up and update GUI (with failure).
 *
 * @param cls test context
 * @param tc scheduler callback
 */
static void
fail_timeout (void *cls, const struct GNUNET_SCHEDULER_TaskContext *tc)
{
  struct TestContext *tstc = cls;

  tstc->tsk = GNUNET_SCHEDULER_NO_TASK;
  display_test_result (tstc, GNUNET_NO);
}


/**
 * Function called whenever the user wants to test a
 * transport configuration.
 *
 * @param section_name section with the port numbers
 * @param is_tcp #GNUNET_YES for TCP, #GNUNET_NO for UDP
 * @param success_image image to show on success
 * @param failure_image image to show on failure
 */
void
GNUNET_setup_transport_test (const char *section_name, int is_tcp,
                             const char *success_image,
                             const char *failure_image)
{
  struct TestContext *tc;
  unsigned long long bnd_port;
  unsigned long long adv_port;
  GtkWidget *w;

  if (GNUNET_OK !=
      GNUNET_CONFIGURATION_get_value_number (cfg, section_name, "PORT",
                                             &bnd_port))
  {
    GNUNET_break (0);
    return;
  }
  if (GNUNET_OK !=
      GNUNET_CONFIGURATION_get_value_number (cfg, section_name,
                                             "ADVERTISED_PORT", &adv_port))
    adv_port = bnd_port;
  tc = GNUNET_malloc (sizeof (struct TestContext));
  tc->success_image = success_image;
  tc->failure_image = failure_image;
  w = GTK_WIDGET (GNUNET_SETUP_get_object (success_image));
  gtk_widget_hide (w);
  GNUNET_assert (NULL != cfg);
  GNUNET_RESOLVER_connect (cfg);
  tc->tst =
      GNUNET_NAT_test_start (cfg, is_tcp, (uint16_t) bnd_port,
                             (uint16_t) adv_port, &result_callback, tc);
  if (NULL == tc->tst)
  {
    display_test_result (tc, GNUNET_SYSERR);
    return;
  }
  tc->tsk = GNUNET_SCHEDULER_add_delayed (TIMEOUT, &fail_timeout, tc);
}

/* end of gnunet-setup-transport-test.c */
