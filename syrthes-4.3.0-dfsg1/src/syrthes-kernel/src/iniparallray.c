/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_option.h"
# include "syr_bd.h"
# include "syr_proto.h"
# include "syr_parall.h"


/* # include "mpi.h" */


extern struct Performances perfo;
extern struct Affichages affich;
                    

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C.PENIGUEL                                       |
  |======================================================================|
  | iniparallray                                                         |
  |   Initialisation du parallelisme pour le rayonnement                 |
  |======================================================================| */
void iniparallray(struct Maillage maillnodray,
		  struct SDparall_ray *sdparall_ray)
{
  int i,nb,reste;

  sdparall_ray->nelrayloc=(int*)malloc(sdparall_ray->npartsray*sizeof(int));
  sdparall_ray->ieledeb=(int*)malloc((sdparall_ray->nparts+1)*sizeof(int));

  /* nombre de facettes par proc */
  nb=maillnodray.nelem/sdparall_ray->npartsray;
  for (i=0;i<sdparall_ray->npartsray;i++)  sdparall_ray->nelrayloc[i]=nb;

  for (i=0;i<sdparall_ray->npartsray+1;i++)  sdparall_ray->ieledeb[i]=i*nb;


  /* rajout du reste des facettes sur le dernier proc */ 
  reste=(maillnodray.nelem-sdparall_ray->npartsray*nb);
  sdparall_ray->nelrayloc[sdparall_ray->npartsray-1]+=reste;
  sdparall_ray->ieledeb[sdparall_ray->npartsray]+=reste;

  sdparall_ray->nelrayloc_max=max(sdparall_ray->nelrayloc[0],
				  sdparall_ray->nelrayloc[sdparall_ray->npartsray-1]);


  /* initialisation de ieledeb pour les proc au-dela de npartsray */ 
  /* on met un nombre > neleray pour etre sur les corresp ne puissent pas tombe dans cette plage */
  for (i=sdparall_ray->npartsray+1;i<sdparall_ray->nparts+1;i++)  sdparall_ray->ieledeb[i]=maillnodray.nelem+1;
  

  /* en parallele, il faut fournir a TOUS les proc nelrayloc_max car cela est utile */
  /* lors des transferts cond/ray meme si le proc ne fait pas de rayt               */
#ifdef _SYRTHES_MPI_
  MPI_Bcast(&(sdparall_ray->nelrayloc_max),1,MPI_INT,0,sdparall_ray->syrthes_comm_world);
#endif 
    
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C.PENIGUEL                                       |
  |======================================================================|
  | raccourcir_maillage                                                  |
  |   Pour la resolution parallele : reduire le maillage a une partition |
  |======================================================================| */
void reduire_maillage(struct Maillage *maillnodray,struct Couple scoupr,
		      struct SDparall_ray *sdparall_ray)
{
  int i,j,n,ideb,ifin,nb;
  int *itrav;
  double **cc;
  
  ideb=sdparall_ray->ieledeb[sdparall_ray->rang];
  ifin=sdparall_ray->ieledeb[sdparall_ray->rang+1];
  nb=ifin-ideb;

  sdparall_ray->frglob=(int*)malloc(nb*sizeof(int));

  /* suppression des elements */
  /* ------------------------ */
  for (n=0,i=ideb;i<ifin;i++,n++)
    {
      maillnodray->volume[n]=maillnodray->volume[i];
      sdparall_ray->frglob[n]=i;  
      for (j=0;j<maillnodray->ndmat;j++)
	maillnodray->node[j][n]=maillnodray->node[j][i];
    }
  
  for (j=0;j<maillnodray->ndmat;j++) 
    maillnodray->node[j]=(int*)realloc(maillnodray->node[j],nb*sizeof(int));
  maillnodray->volume=(double*)realloc(maillnodray->volume,nb*sizeof(double));
  
  maillnodray->nelem=nb;


  /* suppression des references et des types des elements */
  /* ---------------------------------------------------- */
  for (n=0,i=ideb;i<ifin;i++,n++)
    {
      maillnodray->nrefe[n]=maillnodray->nrefe[i];
      maillnodray->type[n]=maillnodray->type[i];
    }  
  maillnodray->nrefe=(int*)realloc(maillnodray->nrefe,nb*sizeof(int));
  maillnodray->type=(int*)realloc(maillnodray->type,nb*sizeof(int));
  

  /* determination des noeuds utiles */
  /* ------------------------------- */
  itrav=(int*)malloc(maillnodray->npoin*sizeof(int));
  for (i=0;i<maillnodray->npoin;i++) itrav[i]=-2;

  for (i=0;i<maillnodray->nelem;i++)
    for (j=0;j<maillnodray->ndmat;j++)
      itrav[maillnodray->node[j][i]]=-1;
  
  for (nb=0,i=0;i<maillnodray->npoin;i++)
    if (itrav[i]==-1) {itrav[i]=nb; nb++;}

  /* creation nouvelle table de coord */
  /* -------------------------------- */

  cc=(double**)malloc(maillnodray->ndim*sizeof(double*));
  for (i=0;i<maillnodray->ndim;i++) cc[i]=(double*)malloc(nb*sizeof(double));

  for (nb=0,i=0;i<maillnodray->npoin;i++)
    if (itrav[i]>-1)
      {
	for (j=0;j<maillnodray->ndim;j++) 
	  cc[j][nb]=maillnodray->coord[j][i];
	nb++;
      }

  maillnodray->npoin=nb;

  for (i=0;i<maillnodray->ndim;i++)
    {
      free(maillnodray->coord[i]);
      maillnodray->coord[i]=cc[i];
    }

  /* decalage des numeros de noeuds des elts */
  /* --------------------------------------- */
  for (i=0;i<maillnodray->nelem;i++)
    for (j=0;j<maillnodray->ndmat;j++)
      maillnodray->node[j][i]=itrav[maillnodray->node[j][i]];


  free(itrav);


  /* decalage des numeros des elts rayt dans scoupr */
  /* ---------------------------------------------- */
/*   for (i=0;i<scoupr.nelem;i++) */
/*     scoupr.numcor[i]-=ideb; */


}
