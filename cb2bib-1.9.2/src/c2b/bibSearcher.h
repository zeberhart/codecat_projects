/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#ifndef BIBSEARCHER_H
#define BIBSEARCHER_H

#include "documentCache.h"
#include "searchPattern.h"

#include <QMap>
#include <QObject>
#include <QStringList>


class bibParser;
class bibReference;


class bibSearcher : public QObject
{

    Q_OBJECT

public:
    bibSearcher(bibParser* bp, QObject* parento = 0);
    bibSearcher(bibParser* bp, const QString& bib_dir, QObject* parento = 0);
    inline ~bibSearcher() {}

    static QString searchDocumentKeyword(const QString& bibtexfn, const QString& documentfn, const QString& keyword);

    const QString highlight(const QString& abstract) const;
    void addPattern(bool Not, bool caseSensitive, const QString& patternType, const QString& scope,
                    const QChar& yearScope, const QString& pattern);
    void clear();
    void exec();

    inline void setBoolean(bool AND)
    {
        _boolean_and = AND;
    }

    inline void setSearchScope(const QString& file, const QString& dir, bool all, bool documents)
    {
        _bibtex_file = file;
        _bibtex_dir = dir;
        _all_bibtex_files = all;
        _include_documents = documents;
    }

    inline void setSimplifySource(bool simplify)
    {
        _simplify_source = simplify;
    }

    inline int errorsCount() const
    {
        return _error_counter;
    }

    inline int hitsCount() const
    {
        return _hits_map.count();
    }

    inline QString hitsString() const
    {
        return _hits_string;
    }

    inline QString hitValue(const QString& key) const
    {
        return _hits_map.value(key);
    }

    inline int patternsCount() const
    {
        return _patterns.count();
    }

    inline int referencesCount() const
    {
        return _reference_counter;
    }

    inline QString logString() const
    {
        return _log_string;
    }


public slots:
    void abort();


private:
    bibSearcher();

    QList<searchPattern> _patterns;
    QMap<QString, QString> _hits_map;
    QString _bibtex_dir;
    QString _bibtex_file;
    QString _do_search_similar_citeid;
    QString _hits_string;
    QString _log_string;
    QStringList _scopes;
    bibParser* _bpP;
    bool _aborted;
    bool _all_bibtex_files;
    bool _boolean_and;
    bool _include_documents;
    bool _simplify_source;
    const QString excerpts(const QString& contents) const;
    const QString location(const QString& fn, const bibReference& ref) const;
    const bool _do_search_similar;
    documentCache _documents;
    int _bibtex_counter;
    int _document_counter;
    int _error_counter;
    int _reference_counter;
    void search(const QString& bib_file);
    void searchReference(const QString& bib_file, const bibReference& ref);
    void searchSimilarReferences(const QString& bib_file, const bibReference& ref);

};

#endif
