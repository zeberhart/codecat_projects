/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "syr_usertype.h"
#include "syr_abs.h"
#include "syr_bd.h"
#include "syr_tree.h"
#include "syr_option.h"
#include "syr_const.h"
#include "syr_proto.h"
/* #include "mpi.h" */

extern struct Performances perfo;
extern struct Affichages affich;
extern int somfac[4][6];
double invPi;
double *ecl;

extern char nomdata[CHLONG];
extern FILE *fdata;
static char ch[CHLONG],motcle[CHLONG],repc[CHLONG];
static double dlist[100];
static int ilist[100];


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decryptage des flux volumiques                                       |
  |======================================================================| */
void decode_mst(struct Maillage maillnodes,struct Mst *mst,
		struct SDparall sdparall)
{
  int n,i,j,nr,nb,netot;
  int i1,i2,id,ii;
  double val;
  int *ref;  /* [max_ref] */


  /* Allocations */
  /* ----------- */
  
  for (i=0;i<MAX_REF;i++) mst->listref[i]=0;


  /* 1ere passe : decodage des references pour les cond vol */
  /* ====================================================== */
  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  id=i2+1;
	  if (!strcmp(motcle,"MST=")) 
	    {
	      rep_listint(ilist,&nb,ch+id+ii);
	      verif_liste_ref_0N("MST=",nb,ilist);
	      for (n=0;n<nb;n++) mst->listref[ilist[n]]=1;
	    }

	}
    }


  /* compte du nombre d'elt mst */
  /* -------------------------- */
  for (mst->nelem=i=0;i<maillnodes.nelem;i++)
    {
      nr=maillnodes.nrefe[i];
      if (mst->listref[nr]) (mst->nelem)++;
    }

  mst->nume=(int*)malloc(mst->nelem*sizeof(int)); verif_alloue_int1d("decode_mst",mst->nume);
  perfo.mem_cond+=mst->nelem*sizeof(int);

  for (i=nb=0;i<maillnodes.nelem;i++)
    {
      nr=maillnodes.nrefe[i];
      if (mst->listref[nr]) {mst->nume[nb]=i;nb++;}
    }

  netot=somme_int_parall(mst->nelem);


      if (mst->nelem &&(sdparall.nparts==1 || sdparall.rang==0))
	if (SYRTHES_LANG == FR)
	  printf("\n *** Milieu semi-transparent : nombre d'elements concernes : %d\n",netot);
	else if (SYRTHES_LANG == EN)
	  printf("\n *** Semi-transparent domain : number of elements : %d\n",netot);
    

  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */

void nummst(struct Mst *mst,struct Maillage maillnodes)
{
  int i,j,k,num;
  int nbs,nbr,nf,ne,nvois;
  int *dejafait,*it;

  nbs=nbr=0;
  for (i=0;i<mst->nelem;i++)
    for (j=0;j<maillnodes.nbface;j++)
      {
	ne=mst->nume[i]; nvois=maillnodes.nvoisin[j][ne]; 
	printf("ne %d j %d nfabor %d\n",ne,j,maillnodes.nvoisin[j][ne]);
	if (nvois<0)
	  nbr++;
	else if (nvois>=0 && mst->listref[maillnodes.nrefe[nvois]]) 
	  nbs++;
      }
  
  mst->nbfbr=nbr;
  mst->nbfbs=nbs;
  mst->nbfbord=nbs+nbr;

  mst->nodeb=(int**)malloc(maillnodes.ndim*sizeof(int*));
  for (i=0;i<maillnodes.ndim;i++) mst->nodeb[i]=(int*)malloc((nbs+nbr)*sizeof(int));
  verif_alloue_int2d(maillnodes.ndim,"nummst",mst->nodeb);

  mst->face=(int**)malloc((maillnodes.ndim+1)*sizeof(int*));
  for (i=0;i<maillnodes.ndim+1;i++) mst->face[i]=(int*)malloc(mst->nelem*sizeof(int));
  verif_alloue_int2d(maillnodes.ndim+1,"nummst",mst->face);

  mst->refbord=(int*)malloc((nbs+nbr)*sizeof(int));
  verif_alloue_int1d("nummst",mst->refbord);

  /* creation du nfabor local au mst */
  mst->nfabor=(int**)malloc(maillnodes.nbface*sizeof(int*));
  for (i=0; i<maillnodes.nbface; i++) mst->nfabor[i]=(int*)malloc(mst->nelem*sizeof(int));
  verif_alloue_int2d(maillnodes.nbface,"nummst",mst->nfabor);
  for (i=0;i<mst->nelem;i++) for (j=0;j<maillnodes.nbface;j++) mst->nfabor[j][i]=-1;

  it=(int*)malloc(maillnodes.nelem*sizeof(int)); verif_alloue_int1d("nummst",it);
  for (i=0;i<mst->nelem;i++) it[mst->nume[i]]=i;

  
  nf=0;
  if (mst->nbfbr)
    for (i=0;i<mst->nelem;i++)
      for (j=0;j<maillnodes.nbface;j++)
	{
	  ne=mst->nume[i]; nvois=maillnodes.nvoisin[j][ne]; 
	  if (nvois<0)
	    {
	      mst->nodeb[0][nf]=maillnodes.node[somfac[j][0]][ne];
	      mst->nodeb[1][nf]=maillnodes.node[somfac[j][1]][ne];
	      if (maillnodes.ndim==3) mst->nodeb[2][nf]=maillnodes.node[somfac[j][2]][ne];
	      /*mst->refbord[nf]=maillnodes.nrefac[j][i];*/
	      mst->face[j][i]=nf;
	      nf++;
	    }	      
	}

  if (mst->nbfbs)
    for (i=0;i<mst->nelem;i++)
      for (j=0;j<maillnodes.nbface;j++)
	{
	  ne=mst->nume[i]; nvois=maillnodes.nvoisin[j][ne]; 
	  if (nvois>=0 && mst->listref[maillnodes.nrefe[nvois]]) 
	    {
	      mst->nodeb[0][nf]=maillnodes.node[somfac[j][0]][ne];
	      mst->nodeb[1][nf]=maillnodes.node[somfac[j][1]][ne];
	      if (maillnodes.ndim==3) mst->nodeb[2][nf]=maillnodes.node[somfac[j][2]][ne];
	      /*mst->refbord[nf]=maillnodes.nrefac[j][i];*/
	      mst->face[j][i]=nf;
	      nf++;
	    }	      
	}


  dejafait=(int*)malloc(maillnodes.nelem*sizeof(int)); verif_alloue_int1d("nummst",dejafait);
  for (j=0;j<maillnodes.nelem;j++) dejafait[j]=-1;

  for (i=0;i<mst->nelem;i++)
    for (j=0;j<maillnodes.nbface;j++)
      {
	ne=mst->nume[i]; nvois=maillnodes.nvoisin[j][ne]; 
	dejafait[ne]=i;
	if (nvois>=0 && mst->listref[maillnodes.nrefe[nvois]]==0) mst->nfabor[j][i]=-2;
	else if (nvois>=0 && mst->listref[maillnodes.nrefe[nvois]]) 
	  {
	    mst->nfabor[j][i]=it[maillnodes.nvoisin[j][ne]];
	    if (dejafait[nvois]>-1)
	      {
		for (k=0;k<maillnodes.nbface;k++) if (maillnodes.nvoisin[k][nvois]==ne) num=k;
		mst->face[j][i]=mst->face[num][dejafait[nvois]];
	      }	    
	    else
	      {
		mst->face[j][i]=nf;
		nf++;
	      }	    
	  }
      }
  
  mst->nface=nf;

  free (dejafait); free(it);
    
  if (SYRTHES_LANG == FR)
    {
      printf("\n *** NUMMST : numerotation des faces des semi-transparents\n");
      printf("                  - %d faces en contat avec un materiau opaque\n",mst->nbfbs);
      printf("                  - %d faces couplees au rayonnement transparent\n",mst->nbfbr);
    }
  else if (SYRTHES_LANG == EN)
    {
      printf("\n *** NUMMST : number of semi-transparent faces\n");
      printf("                  - %d faces in contact with an opaque material\n",mst->nbfbs);
      printf("                  - %d faces in contact with a semi-transparent medium\n",mst->nbfbr);
    }

  if (affich.mst)
    {
      if (SYRTHES_LANG == FR)
	printf("\n >>> nummst : numerotation des faces des elements mst\n");
      else if (SYRTHES_LANG == EN)
	printf("\n >>> nummst : coding of semi-transparent element faces\n");

      for (i=0;i<mst->nelem;i++)
	{ printf("\n elt %d  faces ",i);
	  for (j=0;j<maillnodes.nbface;j++) printf(" %d ",mst->face[j][i]); }
      if (SYRTHES_LANG == FR)
	printf("\n numerotation locale de nfabor\n");
      else if (SYRTHES_LANG == EN)
	printf("\n local number of nfabor\n");
      for (i=0;i<mst->nelem;i++) for (j=0;j<maillnodes.nbface;j++) 
	printf(" ij %d %d nfabor %d\n",i,j,mst->nfabor[j][i]);

    }
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |     Directions                                                       |
  |======================================================================| */
void calcosdir(int nbray,double *mu, double *ksi, double *eta,double *wm)
{
  int i,j;
  double x;


  mu[0]=ccd(Pi/4.,Pi/4.);        ksi[0]=eta[0]=mu[0];

  if (nbray>31)
    {
      mu[1]=ccd(Pi/4.,2.*Pi/5.);     ksi[1]=ccd(2.*Pi/5.,Pi/10.);     eta[1]=ccd(Pi/10.,Pi/4.);
      mu[2]=eta[1];                  ksi[2]=mu[1];                    eta[2]=ksi[1];
      mu[3]=ksi[1];                  ksi[3]=eta[1];                   eta[3]=mu[1];
    }
  if (nbray>127)
    {
      mu[4]=ccd(Pi/4.,5.*Pi/11.);    ksi[4]=ccd(5.*Pi/11.,Pi/22.);    eta[4]=ccd(Pi/22.,Pi/4.);  
      mu[5]=ccd(2.*Pi/5.,7.*Pi/22.); ksi[5]=ccd(7.*Pi/22,Pi/16.);     eta[5]=ccd(Pi/16.,2.*Pi/5.);
      mu[6]=ccd(Pi/10.,7.*Pi/16.);   ksi[6]=ccd(7.*Pi/16.,2.*Pi/11.); eta[6]=ccd(2.*Pi/11.,Pi/10.); 
      mu[7]=ccd(Pi/7.,5.*Pi/14.);    ksi[7]=ccd(5.*Pi/14.,Pi/4.);     eta[7]=ccd(Pi/4.,Pi/7.);
      mu[8]=ksi[7];                  ksi[8]=eta[7];                   eta[8]=mu[7];
      mu[9]=eta[7];                  ksi[9]=mu[7];                    eta[9]=ksi[7];
      mu[10]=eta[4];                 ksi[10]=mu[4];                   eta[10]=ksi[4];
      mu[11]=eta[5];                 ksi[11]=mu[5];                   eta[11]=ksi[5];
      mu[12]=eta[6];                 ksi[12]=mu[6];                   eta[12]=ksi[6];
      mu[13]=ksi[4];                 ksi[13]=eta[4];                  eta[13]=mu[4];
      mu[14]=ksi[5];                 ksi[14]=eta[5];                  eta[14]=mu[5];
      mu[15]=ksi[6];                 ksi[15]=eta[6];                  eta[15]=mu[6];
    }
  if (nbray>511)
    {
      mu[16]=ccd(Pi/7.,17.*Pi/38.);     ksi[16]=ccd(17.*Pi/38.,5.*Pi/44.); eta[16]=ccd(5.*Pi/44.,Pi/7.);  
      mu[17]=ccd(5.*Pi/14.,17.*Pi/44.); ksi[17]=ccd(17.*Pi/44.,Pi/19.);    eta[17]=ccd(Pi/19.,5.*Pi/14.);      
      mu[18]=ccd(Pi/4.,7.*Pi/19.);      ksi[18]=ccd(7.*Pi/19.,5.*Pi/38.);  eta[18]=ccd(5.*Pi/38.,Pi/4.); 
      mu[19]=ccd(Pi/4.,11.*Pi/23.);     ksi[19]=ccd(11.*Pi/23.,Pi/46.);    eta[19]=ccd(Pi/46.,Pi/4.);  
      mu[20]=ccd(Pi/5.,19.*Pi/46.);     ksi[20]=ccd(19*Pi/46.,Pi/40.);     eta[20]=ccd(Pi/40.,Pi/5.);  
      mu[21]=ccd(Pi/10.,19*Pi/40.);     ksi[21]=ccd(19*Pi/40.,2.*Pi/23.);  eta[21]=ccd(2.*Pi/23.,Pi/10.);  
      mu[22]=ccd(7.*Pi/16.,8.*Pi/23.);  ksi[22]=ccd(8.*Pi/23.,Pi/34.);     eta[22]=ccd(Pi/34.,7.*Pi/16.);  
      mu[23]=ccd(5.*Pi/11.,13.*Pi/46.); ksi[23]=ccd(13.*Pi/46.,Pi/28.);    eta[23]=ccd(Pi/28.,5.*Pi/11.);  
      mu[24]=ccd(7.*Pi/22.,13.*Pi/40.); ksi[24]=ccd(13.*Pi/40.,2.*Pi/17.); eta[24]=ccd(2.*Pi/17.,7.*Pi/22.);  
      mu[25]=ccd(Pi/16.,8.*Pi/17.);     ksi[25]=ccd(8.*Pi/17.,7.*Pi/46.);  eta[25]=ccd(7.*Pi/46.,Pi/16.);  
      mu[26]=ccd(2.*Pi/11.,13.*Pi/34.); ksi[26]=ccd(13.*Pi/34.,7.*Pi/40.); eta[26]=ccd(7.*Pi/40.,2.*Pi/11.);  
      mu[27]=ccd(Pi/22.,13.*Pi/28.);    ksi[27]=ccd(13.*Pi/28.,5.*Pi/23.); eta[27]=ccd(5.*Pi/23.,Pi/22.);  
      mu[28]=ccd(Pi/13.,11.*Pi/26.);    ksi[28]=ccd(11.*Pi/26.,Pi/4.);     eta[28]=ccd(Pi/4.,Pi/13.);  
      mu[29]=ccd(5.*Pi/26.,11.*Pi/32.); ksi[29]=ccd(11.*Pi/32.,4.*Pi/19.); eta[29]=ccd(4.*Pi/19.,11.*Pi/32.);  
      mu[30]=ccd(5.*Pi/32.,4.*Pi/13.);  ksi[30]=ccd(4.*Pi/13.,11.*Pi/38.); eta[30]=ccd(11.*Pi/38.,5.*Pi/32.);  
      mu[31]=ccd(Pi/4.,5.*Pi/17.);      ksi[31]=ccd(5.*Pi/17.,7.*Pi/34.);  eta[31]=ccd(7.*Pi/34.,Pi/4.);
      for (i=0;i<16;i++)
	{
	  mu[32+2*i]=ksi[16+i];  ksi[32+2*i]=eta[16+i];  eta[32+2*i]=mu[16+i];
	  mu[33+2*i]=eta[16+i];  ksi[33+2*i]=mu[16+i];   eta[33+2*i]=ksi[16+i];
	}
    }

  /* extension a l'integralite de la sphere */
  for (i=0;i<nbray/8;i++)
    {
      for (j=1;j<4;j++)
	{
	  if (j<3) mu[j*nbray/8+i]=-mu[i];   else mu[j*nbray/8+i]=mu[i];
	  if (j==1) ksi[j*nbray/8+i]=ksi[i]; else ksi[j*nbray/8+i]=-ksi[i];
	  eta[j*nbray/8+i]=eta[i];
	}
      for (j=4;j<8;j++)
	{
	  mu[j*nbray/8+i]=mu[(j-4)*nbray/8+i];
	  ksi[j*nbray/8+i]=ksi[(j-4)*nbray/8+i];
	  eta[j*nbray/8+i]=-eta[(j-4)*nbray/8+i];
	}
    }


  x=4*Pi/nbray;
  for (i=0;i<nbray;i++) wm[i]=x;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |     Poids directionels                                               |
  |======================================================================| */
void calcosdirS(int nbray,double *mu, double *ksi, double *eta,double *wm)
{
  int i,j;
  double v1,v2,v3,v4,p1,p2,p3,x;

  if (nbray==24)
    {
      /* LSN */
      v1=0.2959;  v2=0.9082;
      p1=Pi/6;

      mu[0]=v2;        ksi[0]=v1;     eta[0]=v1;
      mu[1]=v1;        ksi[1]=v2;     eta[1]=v1;
      mu[2]=v1;        ksi[2]=v1;     eta[2]=v2;
      
      if (SYRTHES_LANG == FR)
	{
	  printf("==> calcosdir2 : somme des carres des cos dir\n");
	  for (i=0;i<3;i++) printf("i=%d som des carres=%f\n",i,
				   mu[i]*mu[i]+ksi[i]*ksi[i]+eta[i]*eta[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("==> calcosdir2 : Sum of square of cos dir\n");
	  for (i=0;i<3;i++) printf("i=%d sum of square=%f\n",i,
				   mu[i]*mu[i]+ksi[i]*ksi[i]+eta[i]*eta[i]);
	}

      wm[0]=wm[1]=wm[2]=p1;

      for (i=0,x=0;i<3;i++) x+=wm[i];
      if (SYRTHES_LANG == FR)
	{
	  printf("    som des poids du 8e de sphere=%f\n",x);
	  printf("    som des poids de la sphere=%f\n",x*8);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("    sum of weight of 1/8th of the sphere=%f\n",x);
	  printf("    sum of weight of the sphere=%f\n",x*8);
	}
    }
  else if (nbray==80)
    {
      /* LSN */
      v1=0.1776218;  v2=0.5773503;  v3=0.7969424; v4=0.9679364;
      p1=0.1296002;  p2=0.1528596;  p3=0.1637695;
      
      /* LSH */
      /*   v1=0.1584891;  v2=0.5773503;  v3=0.8009668; v4=0.9745575;
	   p1=0.2744222;  p2=0.1513901;  p3=0.1403673;
      */
      
      mu[0]=v4;        ksi[0]=v1;     eta[0]=v1;
      mu[1]=v3;        ksi[1]=v2;     eta[1]=v1;
      mu[2]=v2;        ksi[2]=v3;     eta[2]=v1;
      mu[3]=v1;        ksi[3]=v4;     eta[3]=v1;
      mu[4]=v3;        ksi[4]=v1;     eta[4]=v2;
      mu[5]=v2;        ksi[5]=v2;     eta[5]=v2;
      mu[6]=v1;        ksi[6]=v3;     eta[6]=v2;
      mu[7]=v2;        ksi[7]=v1;     eta[7]=v3;
      mu[8]=v1;        ksi[8]=v2;     eta[8]=v3;
      mu[9]=v1;        ksi[9]=v1;     eta[9]=v4;
      
      if (SYRTHES_LANG == FR)
	{
	  printf("==> calcosdir2 : somme des carres des cos dir\n");
	  for (i=0;i<10;i++) printf("i=%d som des carres=%f\n",i,
				    mu[i]*mu[i]+ksi[i]*ksi[i]+eta[i]*eta[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("==> calcosdir2 : sum of square of cos dir\n");
	  for (i=0;i<10;i++) printf("i=%d sum of squares=%f\n",i,
				    mu[i]*mu[i]+ksi[i]*ksi[i]+eta[i]*eta[i]);
	}
      
      
      wm[0]=wm[3]=wm[9]=p2;
      wm[5]=p1;
      wm[1]=wm[2]=wm[4]=wm[6]=wm[7]=wm[8]=p3;
      
      for (i=0,x=0;i<10;i++) x+=wm[i];
      if (SYRTHES_LANG == FR)
	{
	  printf("    som des poids du 8e de sphere=%f\n",x);
	  printf("    som des poids de la sphere=%f\n",x*8);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("    sum of weight of 1/8th of sphere=%f\n",x);
	  printf("    sum of weight of the sphere=%f\n",x*8);
	}
    }

  /* extension a l'integralite de la sphere */
  for (i=0;i<nbray/8;i++)
    {
      for (j=1;j<4;j++)
	{
	  if (j<3) mu[j*nbray/8+i]=-mu[i];   else mu[j*nbray/8+i]=mu[i];
	  if (j==1) ksi[j*nbray/8+i]=ksi[i]; else ksi[j*nbray/8+i]=-ksi[i];
	  eta[j*nbray/8+i]=eta[i];
	  wm[j*nbray/8+i]=wm[i];
	}
      for (j=4;j<8;j++)
	{
	  mu[j*nbray/8+i]=mu[(j-4)*nbray/8+i];
	  ksi[j*nbray/8+i]=ksi[(j-4)*nbray/8+i];
	  eta[j*nbray/8+i]=-eta[(j-4)*nbray/8+i];
	  wm[j*nbray/8+i]=wm[(j-4)*nbray/8+i];
	}
    }

  for (i=0,x=0;i<nbray;i++) x+=wm[i];
  if (SYRTHES_LANG == FR)
    printf("    som des poids=%f\n",x);
  else if (SYRTHES_LANG == EN)
    printf("    sum of weight=%f\n",x);
  for (i=0,x=0;i<nbray;i++) {printf("mu[%d]=%f\n",i,mu[i]);x+=wm[i]*mu[i];}
  if (SYRTHES_LANG == FR)
    printf("    som des p.mu=%f\n",x);
  else if (SYRTHES_LANG == EN)
    printf("    sum of p.mu=%f\n",x);
  for (i=0,x=0;i<nbray;i++) {printf("ksi[%d]=%f\n",i,mu[i]);x+=wm[i]*ksi[i];}
  if (SYRTHES_LANG == FR)
    printf("    som des p.ksi=%f\n",x);
  else if (SYRTHES_LANG == EN)
    printf("    sum of p.ksi=%f\n",x);
  for (i=0,x=0;i<nbray;i++) {printf("eta[%d]=%f\n",i,mu[i]);x+=wm[i]*eta[i];}
  if (SYRTHES_LANG == FR)
    printf("    som des p.eta=%f\n",x);
  else if (SYRTHES_LANG == EN)
    printf("    sum of p.eta=%f\n",x);

   
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */
double ccd(double a1,double a2)
{
  return (sin(a1)*sin(a2)/sqrt(1.-cos(a1)*sin(a2)*cos(a1)*sin(a2)));
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */
void normarete2d(struct Mst *mst,int **nodes,double **coords)
{
  int n,n1,n2,n3,ne;
  double ***p;
  double xn,yn,an;

  for (n=0,p=mst->xnora;n<mst->nelem;n++)
    {
      ne=mst->nume[n]; n1=nodes[0][ne]; n2=nodes[1][ne]; n3=nodes[2][ne];

      xn=-coords[1][n2] + coords[1][n1]; yn= coords[0][n2] - coords[0][n1];
      an=sqrt(xn*xn+yn*yn);p[0][0][n]=xn/an; p[1][0][n]=yn/an;

      xn=-coords[1][n3] + coords[1][n2]; yn= coords[0][n3] - coords[0][n2];
      an=sqrt(xn*xn+yn*yn);p[0][1][n]=xn/an; p[1][1][n]=yn/an;

      xn=-coords[1][n1] + coords[1][n3]; yn= coords[0][n1] - coords[0][n3];
      an=sqrt(xn*xn+yn*yn);p[0][2][n]=xn/an; p[1][2][n]=yn/an;

    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */
void surfbmst(struct Mst *mst,double **coords)
{
  int n,n1,n2;
  double *p;
  double xab,yab;

  for (n=0,p=mst->surfb;n<mst->nbfbord;n++,p++)
    {
      n1=mst->nodeb[0][n]; n2=mst->nodeb[1][n];
      xab=coords[0][n2]-coords[0][n1]; yab=coords[1][n2]-coords[1][n1];
      *p=sqrt(xab*xab+yab*yab);
    }

} 
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */
void trimst(struct Mst *mst,struct Maillage maillnodes,
	    double vx,double vy)
{
  int i,j;
  int **abesoin,**estdemande,*dejapris;

  abesoin=(int **)malloc(maillnodes.nbface*sizeof(int*));
  for (i=0;i<maillnodes.nbface;i++) abesoin[i]=(int*)malloc(mst->nelem*sizeof(int));
  verif_alloue_int2d(maillnodes.nbface,"trimst",abesoin);

  estdemande=(int **)malloc((maillnodes.nbface+1)*sizeof(int*));
  for (i=0;i<maillnodes.nbface+1;i++) estdemande[i]=(int*)malloc(mst->nelem*sizeof(int));
  verif_alloue_int2d(maillnodes.nbface+1,"trimst",abesoin);

  dejapris=(int*)malloc(mst->nelem*sizeof(int));
  verif_alloue_int1d("trimst",dejapris);
  
  for (i=0;i<mst->nelem;i++) {dejapris[i]=mst->elttri[i]=-1;}

  if (maillnodes.ndim==2)
    besoin2d(abesoin,estdemande,dejapris,mst->nelem,
	     maillnodes.node,mst->xnora,mst->nfabor,vx,vy);
  /*  else
      besoin3d(abesoin,estdemande,dejapris,maillnodes.nelem,nodes,xnora,nfabor,vdir[0],vdir[1],vdir[2]);
    */
  ordonne(mst->nelem,maillnodes.nbface,abesoin,estdemande,mst->elttri,dejapris);

  free(abesoin); free(estdemande); free(dejapris);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void ordonne(int nelem,int nbface,
	     int **abesoin,int **estdemande,int *liste,int *dejapris)
{
  int il=0,i,j,ok,nb,np;

  np=0;
  do
    {
      nb=0; np++;
      for (i=0;i<nelem;i++) 
	if (dejapris[i]<0)
	  {
	    nb++;
	    if (abesoin[0][i]<0) ajoute_liste(i,nbface,&il,abesoin,estdemande,liste,dejapris);
	    else
	      {
		ok=1;
		for (j=0;j<nbface;j++) if (abesoin[j][i]>-1 && dejapris[abesoin[j][i]]<0) ok=0; 
		if (ok) ajoute_liste(i,nbface,&il,abesoin,estdemande,liste,dejapris);
	      }
	  }

      if (SYRTHES_LANG == FR)
	printf(" ordonne ==> passage %d , nombre d'elements encore traites %d\n",np-1,nb);
      else if (SYRTHES_LANG == EN)
	printf(" ordonne ==> run %d , number of elements treated %d\n",np-1,nb);

    }
  while(nb!=0);

  /*  printf(" *** TRI : liste triee des elements\n");
      for (i=0;i<nelem;i++) printf("     elt %d = %d \n",i,liste[i]); */
      

}
	
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void besoin2d(int **abesoin,int **estdemande,int *dejapris,
	      int nelem,int **nodes,double ***xnora,int **nfabor,
	      double xdir,double ydir)
{
  int i,j,nb;
  double s;

  for (i=0;i<nelem;i++)
    for (j=0;j<3;j++) abesoin[j][i]=-1;

  for (i=0;i<nelem;i++)
    {
      nb=0;
      for (j=0;j<3;j++)
	{
	  if (nfabor[j][i]>-1)
	    {
	      s=xdir*xnora[0][j][i]+ydir*xnora[1][j][i];
	      if (s>0) 
		{abesoin[nb][i]=nfabor[j][i];nb++;}
	    }
	}
    }


  for (i=0;i<nelem;i++)
    {
      estdemande[0][i]=0;
      for (j=1;j<4;j++) estdemande[j][i]=-1;
    }  

  for (i=0;i<nelem;i++)
    {
      for (j=0;j<3;j++)
	if (abesoin[j][i]>-1) 
	  {
	    estdemande[0][abesoin[j][i]]++;
	    estdemande[estdemande[0][abesoin[j][i]]][abesoin[j][i]]=i;
	  }
    }
  
  if (affich.mst)
    {
      printf("\n *** BESOIN2D \n");
      if (SYRTHES_LANG == FR)
	printf("          a besoin de... \n");
      else if (SYRTHES_LANG == EN)
	printf("          needs... \n");
      for (i=0;i<nelem;i++)
	printf("              abesoin[%d] = %d %d %d\n",i,abesoin[0][i],abesoin[1][i],abesoin[2][i]);
      if (SYRTHES_LANG == FR)
	printf("          est demande par... \n");
      else if (SYRTHES_LANG == EN)
	printf("          is asked by... \n");
      if (SYRTHES_LANG == FR)
	for (i=0;i<nelem;i++)
	  printf("              elt %d nb demande=%d :  %d %d %d\n",
		 i,estdemande[0][i],estdemande[1][i],estdemande[2][i],estdemande[3][i]);
      else if (SYRTHES_LANG == EN)
	for (i=0;i<nelem;i++)
	  printf("              elt %d nb asked=%d :  %d %d %d\n",
		 i,estdemande[0][i],estdemande[1][i],estdemande[2][i],estdemande[3][i]);
    }
}

	
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void ajoute_liste(int n,int nbface,int *il,int **abesoin,int **estdemande,
		  int *liste,int *dejapris)
{
  int j,k,m,ok;


  dejapris[n]=1;
  liste[*il]=n; (*il)++;

  for (j=0;j<estdemande[0][n];j++)
    {
      m=estdemande[j+1][n]; ok=1;
      for (k=0;k<nbface;k++)
	if (abesoin[k][m]!=-1 && dejapris[abesoin[k][m]]<0) ok=0;
      if (ok) ajoute_liste(m,nbface,il,abesoin,estdemande,liste,dejapris);
    }
}
  
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */

void inimst(struct Mst *mst,struct Maillage maillnodes)
{
  int i,j;
  double x;

  invPi=1./Pi;
  /* ??????????????????????????? */
  /*  mst->omega=0.5; mst->beta=4;*/
  mst->omega=0.5; mst->beta=4;
  /* ??????????????????????????? */

  mst->mu=(double*)malloc(mst->nbray*sizeof(double));   verif_alloue_double1d("inimst",mst->mu);
  mst->ksi=(double*)malloc(mst->nbray*sizeof(double));  verif_alloue_double1d("inimst",mst->ksi);
  mst->eta=(double*)malloc(mst->nbray*sizeof(double));  verif_alloue_double1d("inimst",mst->eta);

  mst->xnora=(double***)malloc(maillnodes.ndim*sizeof(double**));
  for (i=0;i<maillnodes.ndim;i++) 
    {
      mst->xnora[i]=(double**)malloc(maillnodes.nbface*sizeof(double*));
      for (j=0;j<maillnodes.nbface;j++) 
	mst->xnora[i][j]=(double*)malloc(mst->nelem*sizeof(double));
      verif_alloue_double2d(maillnodes.nbface,"inimst",mst->xnora[i]);
    }

  mst->wm=(double*)malloc(mst->nbray*sizeof(double));       verif_alloue_double1d("inimst",mst->wm);

  mst->lbnm1=(double*)malloc(mst->nbfbord*sizeof(double));  verif_alloue_double1d("inimst",mst->lbnm1);
  mst->lbn=(double*)malloc(mst->nbfbord*sizeof(double));    verif_alloue_double1d("inimst",mst->lbn);
  mst->lf=(double*)malloc(mst->nface*sizeof(double));       verif_alloue_double1d("inimst",mst->lf);

  mst->tc=(double*)malloc(mst->nelem*sizeof(double));       verif_alloue_double1d("inimst",mst->tc);
  mst->tb=(double*)malloc(mst->nbfbord*sizeof(double));     verif_alloue_double1d("inimst",mst->tb);

  mst->surfb=(double*)malloc(mst->nbfbord*sizeof(double));  verif_alloue_double1d("inimst",mst->surfb);
  mst->Jnm1=(double*)malloc(mst->nelem*sizeof(double));     verif_alloue_double1d("inimst",mst->Jnm1);
  mst->Jn=(double*)malloc(mst->nelem*sizeof(double));       verif_alloue_double1d("inimst",mst->Jn);
  mst->divflux=(double*)malloc(mst->nelem*sizeof(double));  verif_alloue_double1d("inimst",mst->divflux);

  mst->elttri=(int*)malloc(mst->nelem*sizeof(int));         verif_alloue_int1d("inimst",mst->elttri);


  if (mst->nbray==24 || mst->nbray==80)
    calcosdirS(mst->nbray,mst->mu,mst->ksi,mst->eta,mst->wm);
  else
    calcosdir(mst->nbray,mst->mu,mst->ksi,mst->eta,mst->wm);

  normarete2d(mst,maillnodes.node,maillnodes.coord);
  
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */
void resoumst(struct Mst *mst,struct Maillage maillnodes,
	      double *tmps)
{
  int i,j,numray,k,ii=0;
  double residu,*p,*q,t4;
  double tm,**LC,*XJMI;
  double x=sigma*invPi,x1,x2,xx,xxx;
  double som;


  LC=(double**)malloc(mst->nbray*sizeof(double*));  
  for (numray=0;numray<mst->nbray;numray++) LC[numray]=(double*)malloc(mst->nelem*sizeof(double));
  verif_alloue_double2d(mst->nbray,"resoumst",LC);
  XJMI=(double*)malloc(mst->nelem*sizeof(double));  verif_alloue_double1d("resoumst",XJMI);
  ecl=(double*)malloc(mst->nbfbord*sizeof(double)); verif_alloue_double1d("resoumst",ecl);
  for (i=0;i<mst->nbfbord;i++) ecl[i]=0.;


  /*   for (i=0;i<maillnodes.nelem;i++) printf("%d %d %d\n",nodes[0][i]+1,nodes[1][i]+1,nodes[2][i]+1);
  for (i=0;i<maillnodes.npoin;i++) printf("%f %f\n",coords[0][i],coords[1][i]);*/

  surfbmst(mst,maillnodes.coord);
  caltempmst(maillnodes.ndim,mst->nelem,mst->nbfbord,mst->nume,mst->tc,mst->tb,
	     maillnodes.node,mst->nodeb,tmps);

  /* initialisation des luminances de bord */
  for (i=0,p=mst->lbn;i<mst->nbfbord;i++,p++) *p=x*pow(mst->tb[i],4);

  /* init de J */
  x1=mst->omega*0.25*invPi;
  x2=(1-mst->omega)*sigma*invPi;
  for (i=0,p=mst->Jn,q=mst->Jnm1;i<mst->nelem;i++,p++,q++)
    {
      t4=pow(mst->tc[i],4);
      xxx=sigma*t4*invPi;
      for (j=0,xx=0.;j<mst->nbray;j++) xx+=mst->wm[j]*xxx;
      *q=xx*x1+t4*x2;
      *p=0.;
    }


  for (i=0;i<mst->nelem;i++) 
    for (j=0;j<mst->nbray;j++) LC[j][i]=sigma*pow(mst->tc[i],4)*invPi;


  for (i=mst->nbfbord;i<mst->nface;i++) mst->lf[i]=0.;
      
  do
    {
      ii++; printf(" *** MST : iteration %d\n",ii);
      for (i=0;i<mst->nbfbord;i++) ecl[i]=0.; 

      for (i=0,p=mst->lbn,q=mst->lbnm1;i<mst->nbfbord;i++,p++,q++) *q=*p;
      for (i=0,p=mst->Jn;i<mst->nelem;i++,p++) *p=0.;
      /* calcul des luminances de bord */
      for (i=0,p=mst->lbn;i<mst->nbfbord;i++,p++) *p=mst->eps[i]*x*pow(mst->tb[i],4);

      for (numray=0;numray<mst->nbray;numray++)
	{
	  printf("\n ======>>>> DIRECTION %d : %f %f\n",numray,mst->mu[numray],mst->ksi[numray]);
	  for (i=0;i<mst->nbfbord;i++) mst->lf[i]=mst->lbnm1[i];
	  trimst(mst,maillnodes,mst->mu[numray],mst->ksi[numray]);

	  /*	  printf(">>>> avant lumcent2d\n");
	  for (i=0;i<mst->nface;i++) printf("i=%2d lf=%8.3f\n",i,mst->lf[i]);*/

	  for (i=0;i<mst->nelem;i++) 
	    {
	      XJMI[i]=0;
	      for (j=0;j<mst->nbray;j++) XJMI[i]+=mst->wm[j]*LC[j][i];
	      XJMI[i]*=mst->omega/(4*Pi);
	      XJMI[i]+=(1-mst->omega)*sigma*pow(mst->tc[i],4)/Pi;
	    }

	  lumcent2d(mst->nelem,mst->elttri,mst->nume,mst->face,
		    mst->xnora,mst->lf,mst->Jn,mst->Jnm1,
		    mst->ksi[numray],mst->mu[numray],mst->beta,mst->omega,mst->wm[numray],
		    maillnodes.node,maillnodes.coord,XJMI,LC[numray]);
	  /*	  printf(">>>> avant lumbor2d\n");
	  for (i=0;i<mst->nface;i++) printf("i=%2d lf=%8.3f\n",i,mst->lf[i]);*/

	  lumbord2d(mst->nbfbord,mst->nodeb,maillnodes.coord,mst->lf,mst->lbn,mst->eps,
		    mst->ksi[numray],mst->mu[numray],mst->wm[numray]);

	  printf(">>>> apres lumbor2d\n");
	  for (i=0;i<mst->nbfbord;i++) printf("i=%2d lbn=%8.3f\n",i,mst->lbn[i]);
	}
      
      if (SYRTHES_LANG == FR)
	printf(">>>> FLUX SUR LES FACETTES\n");
      else if (SYRTHES_LANG == EN)
	printf(">>>> FLUX ON FACES\n");
	  for (i=0;i<mst->nbfbord;i++) printf("i=%2d ecl=%8.3f emi=%8.3f flux_perdu=%8.3f S=%8.3f flux*S=%8.3f\n",
	  i,ecl[i],mst->eps[i]*sigma*pow(mst->tb[i],4),
          mst->eps[i]*ecl[i]-mst->eps[i]*sigma*pow(mst->tb[i],4),mst->surfb[i],
          (mst->eps[i]*ecl[i]-mst->eps[i]*sigma*pow(mst->tb[i],4))*mst->surfb[i]);
	  for (i=0,som=0;i<mst->nbfbord;i++) 
	    som+=(mst->eps[i]*ecl[i]-mst->eps[i]*sigma*pow(mst->tb[i],4))*mst->surfb[i];
	  if (SYRTHES_LANG == FR)
	    printf("Somme des flux = %f\n",som);
	  else if (SYRTHES_LANG == EN)
	    printf("Sum of flux = %f\n",som);
      /* completion de J */
      for (i=0,p=mst->Jn,q=mst->Jnm1;i<mst->nelem;i++,p++,q++) *q=*p*x1+pow(mst->tc[i],4)*x2;
      
      for (i=0,residu=0,p=mst->lbn,q=mst->lbnm1;i<mst->nbfbord;i++,p++,q++) 
	residu+=2*fabs((*p-*q)/(*p+*q));
      residu/=mst->nbfbord;
    }
  while (residu>1.e-10);


  divmst(mst->nelem,mst->divflux,mst->Jn,mst->beta,mst->omega,mst->tc);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */
void caltempmst(int ndim,int nelem,int nbor,int *nume,double *tc,double *tb,
		int **node,int **nodb,double *tmps)
{
  int i,n1,n2,n3,n4,ne;
  double t,x,tiers=1./3.;
  double *p;

  x=sigma*invPi;

  if (ndim==2)
    {
      for (i=0,p=tc;i<nelem;i++,p++)
	{
	  ne=nume[i];
	  n1=node[0][ne];n2=node[1][ne]; n3=node[2][ne]; 
	  *p=tiers*(tmps[n1]+tmps[n2]+tmps[n3])+tkel;
	}
      for (i=0,p=tb;i<nbor;i++,p++)
	{
	  n1=nodb[0][i];n2=nodb[1][i]; 
	  *p=0.5*(tmps[n1]+tmps[n2])+tkel;
	}
    }
  else
    {
      for (i=0,p=tc;i<nelem;i++,p++)
	{
	  ne=nume[i];
	  n1=node[0][ne];n2=node[1][ne];n3=node[2][ne];n4=node[3][ne];
	  *p=0.25*(tmps[n1]+tmps[n2]+tmps[n3]+tmps[n4])+tkel;
	}
      for (i=0,p=tb;i<nbor;i++,p++)
	{
	  n1=nodb[0][i];n2=nodb[1][i];n3=nodb[2][i];
	  *p=tiers*(tmps[n1]+tmps[n2]+tmps[n3])+tkel;
	}
    }
  /* ??????????????????????????? */
  /*    for (i=0;i<nelem;i++) tc[i]=273.15;
    for (i=0;i<nbor;i++) tb[i]=273.15;*/
      /* tb[11]=tb[12]=tb[13]=tb[14]=1273.15;*/  /* maillage 5 non sym */
  /*     tb[1]=tb[2]=tb[5]=tb[6]=1273.15;   */       /* maillage 5 sym */
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */
void lumcent2d(int nelem,int *elttri,int *nume,int **face,
	       double ***xnora,double *lf,
	       double *jn, double *jnm1,double ksi,double mu,double beta,
	       double omega,double wm,
	       int **nodes,double **coords,
	       double *XJMI,double *LC)
{
  int i,nf1,nf2,nf3,nn1,nn2,nn3,ne,ng,typ;
  double s0,s1,s2,lc;

  for (i=0;i<nelem;i++)
    {
      ne=elttri[i];   ng=nume[ne];
      s0=mu*xnora[0][0][ne]+ksi*xnora[1][0][ne];
      s1=mu*xnora[0][1][ne]+ksi*xnora[1][1][ne];
      s2=mu*xnora[0][2][ne]+ksi*xnora[1][2][ne];
      if (s0<0 && s1>0 && s2>0)      {typ=2; nf1=0; nf2=2; nf3=1; nn1=1; nn2=0; nn3=2;}
      else if (s1<0 && s0>0 && s2>0) {typ=2; nf1=1; nf2=0; nf3=2; nn1=2; nn2=1; nn3=0;}
      else if (s2<0 && s0>0 && s1>0) {typ=2; nf1=2; nf2=1; nf3=0; nn1=0; nn2=2; nn3=1;}
      else if (s0>0) {typ=1; nf1=1; nf2=0; nf3=2; nn1=2; nn2=1; nn3=0;}
      else if (s1>0) {typ=1; nf1=2; nf2=1; nf3=0; nn1=0; nn2=2; nn3=1;}
      else if (s2>0) {typ=1; nf1=0; nf2=2; nf3=1; nn1=1; nn2=0; nn3=2;} 
      if (typ==1)
	{
		lumfac31(ne,ng,nf1,nf2,nf3,nn1,nn2,nn3,nodes,face,xnora,
			 ksi,mu,jnm1[ne],beta,coords,lf,&lc);
      	lumfac31(ne,ng,nf1,nf2,nf3,nn1,nn2,nn3,nodes,face,xnora,
		 ksi,mu,XJMI[ne],beta,coords,lf,&lc);
	}
      else
	{
		lumfac32(ne,ng,nf1,nf2,nf3,nn1,nn2,nn3,nodes,face,xnora,
			 ksi,mu,jnm1[ne],beta,coords,lf,&lc);
      	lumfac32(ne,ng,nf1,nf2,nf3,nn1,nn2,nn3,nodes,face,xnora,
		 ksi,mu,XJMI[ne],beta,coords,lf,&lc); 
	}
      /*            printf(" --> LUMCENT2D elt %2d typ=%d xji=%8.3f lc=%8.3f xji-lc=%8.3f\n",ne,typ,jnm1[ne],lc,jnm1[ne]-lc);
      printf(" --> LUMCENT2D elt %2d typ=%d xji=%8.3f lc=%8.3f xji-lc=%8.3f\n",ne,typ,XJMI[ne],lc,XJMI[ne]-lc);*/
      jn[ne]+=wm*(jnm1[ne]-lc);
      LC[ne]=XJMI[ne]-lc;
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */
void lumbord2d(int nbord,int **nodeb,double **coords,
	       double *lf,double *lbn,double *eps,
	       double ksi,double mu,double wm)
{
  int i,n1,n2,n3;
  double xn,yn,an,norm;

  for (i=0;i<nbord;i++)
    {
      n1=nodeb[0][i]; n2=nodeb[1][i]; 
      xn=coords[1][n2]-coords[1][n1]; yn=-coords[0][n2]+coords[0][n1];
      an=sqrt(xn*xn+yn*yn);xn/=an; yn/=an;
      an=xn*mu+yn*ksi;
      if (an>0.) lbn[i]+=wm*an*lf[i]*(1.-eps[i])*invPi; 
      if (an>0.) ecl[i]+=wm*an*lf[i];

    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */
void lumfac31(int i,int ne,int nf1,int nf2,int nf3,int nn1,int nn2,int nn3,
	      int **nodes,int **face,double ***xnora,
	      double ksi,double mu,double xji,double beta,
	      double **coords,double *lf,double *lc)
{
  double x1,y1,x2,y2,x3,y3;
  double xx,l1,l2,l3,singam2,sinalp2,surf,phi1,phi2,phi3;
  int n1,n2,n3,nfg1,nfg2,nfg3;

  double eps=1.e-8;

  n1=nodes[nn1][ne]; n2=nodes[nn2][ne]; n3=nodes[nn3][ne]; 
  nfg1=face[nf1][i]; nfg2=face[nf2][i]; nfg3=face[nf3][i]; 

  x1=coords[0][n1];  y1=coords[1][n1];
  x2=coords[0][n2];  y2=coords[1][n2];
  x3=coords[0][n3];  y3=coords[1][n3];

  l1=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
  l2=sqrt((x2-x3)*(x2-x3)+(y2-y3)*(y2-y3));
  l3=sqrt((x1-x3)*(x1-x3)+(y1-y3)*(y1-y3));

  xx=ksi*(x2-x3)-mu*(y2-y3);             singam2=xx/l2;
  xx=(y1-y2)*(x3-x2)-(x1-x2)*(y3-y2);    sinalp2=-xx/(l1*l2);

  xx=beta/sqrt(mu*mu+ksi*ksi)*sinalp2/singam2*l1;
  if (xx>eps) 
    {xx=(1-exp(-xx))/xx; lf[nfg1]=lf[nfg2]*xx+(1-xx)*xji;}
  else        
    {lf[nfg1]=lf[nfg2]+0.5*xx*xji;}
  lf[nfg3]=lf[nfg1];

  surf=(0.5*l1*l2*sinalp2);
  phi1=-xnora[0][nf1][i]*mu-xnora[1][nf1][i]*ksi;
  phi2=-xnora[0][nf2][i]*mu-xnora[1][nf2][i]*ksi;
  phi3=-xnora[0][nf3][i]*mu-xnora[1][nf3][i]*ksi;
  
  if (beta>1.e-10) xx=beta;
  else xx=1.e-10;

  /*  printf("lumfac31 : on a f2=%f  on trouve f1=f3=%f     ",lf[nfg2],lf[nfg1]); */
  *lc=(phi1*l1*lf[nfg1]+phi2*l2*lf[nfg2]+phi3*l3*lf[nfg3])/(xx*surf);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */
void lumfac32(int i,int ne,int nf1,int nf2,int nf3,int nn1,int nn2,int nn3,
	      int **nodes,int **face,double ***xnora,
	      double ksi,double mu,double xji,double beta,
	      double **coords,double *lf,double *lc)
{
  double x1,y1,x2,y2,x3,y3;
  double xx,l1,l2,l3,surf,phi1,phi2,phi3;
  int n1,n2,n3,nfg1,nfg2,nfg3;
  double sinalp1,sinalp2,sinalp3,singam1,singam2,singam3;

  n1=nodes[nn1][ne]; n2=nodes[nn2][ne]; n3=nodes[nn3][ne]; 
  nfg1=face[nf1][i]; nfg2=face[nf2][i]; nfg3=face[nf3][i]; 

  x1=coords[0][n1];  y1=coords[1][n1];
  x2=coords[0][n2];  y2=coords[1][n2];
  x3=coords[0][n3];  y3=coords[1][n3];

  l1=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
  l2=sqrt((x2-x3)*(x2-x3)+(y2-y3)*(y2-y3));
  l3=sqrt((x1-x3)*(x1-x3)+(y1-y3)*(y1-y3));


  sinalp1=-((y3-y1)*(x2-x1)-(x3-x1)*(y2-y1))/(l1*l3);
  sinalp2=((y3-y2)*(x1-x2)-(x3-x2)*(y1-y2))/(l1*l2);
  sinalp3=((y1-y3)*(x2-x3)-(x1-x3)*(y2-y3))/(l2*l3);
  
  xx=sqrt(mu*mu+ksi*ksi);
  singam1=-(ksi*(x1-x2)-mu*(y1-y2))/(xx*l1);
  singam2=(ksi*(x2-x3)-mu*(y2-y3))/(xx*l2);
  singam3=(mu*(y1-y3)-ksi*(x1-x3))/(xx*l3);
  
  xx=sinalp1*sinalp2*beta*l1/(xx*sinalp3*singam1);
  if (xx>1.e-8)
    {
      xx=(1-exp(-xx))/xx;
      lf[nfg1]= (sinalp2/sinalp3*singam3/singam1*lf[nfg3] +
	         sinalp1/sinalp3*singam2/singam1*lf[nfg2])*xx 
	        + (1-xx)*xji;
    }
  else
    {
      lf[nfg1]=sinalp2/sinalp3*singam3/singam1*lf[nfg3] +
               sinalp1/sinalp3*singam2/singam1*lf[nfg2] +
	       0.5*xx*xji;
    }

  surf=(0.5*l1*l2*sinalp2);
  phi1=-xnora[0][nf1][i]*mu-xnora[1][nf1][i]*ksi;
  phi2=-xnora[0][nf2][i]*mu-xnora[1][nf2][i]*ksi;
  phi3=-xnora[0][nf3][i]*mu-xnora[1][nf3][i]*ksi;
  
  if (beta>1.e-10) xx=beta;
  else xx=1.e-10;

  /*  printf("lumfac32 : on a f2=%f f3=%f  on trouve f1=%f   ",lf[nfg2],lf[nfg3],lf[nfg1]); */
  *lc=(phi1*l1*lf[nfg1]+phi2*l2*lf[nfg2]+phi3*l3*lf[nfg3])/(xx*surf);
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |======================================================================| */
void divmst(int nelem,double *divflux,
	    double *jn, double beta,double omega,double *tc)
{
  int i;
  double som,xx;

  xx=4*sigma; /* *Pi/Pi  */
  som=0;

  for (i=0;i<nelem;i++)
    {
      divflux[i]=-beta*(1-omega)*(xx*pow(tc[i],4)-jn[i]);
      som+=divflux[i];
    }

  if (affich.mst)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf(" *** DIVMST : valeur du flux volumique equivalent\n");
	  for (i=0;i<nelem;i++) 
	    printf("         elt %d  fluxvol=%f\n",i,divflux[i]);
	  printf(" ---> somme des flux =%f\n",som);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf(" *** DIVMST : value of the equivalent volumic flux\n");
	  for (i=0;i<nelem;i++) 
	    printf("         elt %d  fluxvol=%f\n",i,divflux[i]);
	  printf(" ---> sum of the flux =%f\n",som);
	}
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_limite_mst(struct Mst *mst)
{
  int i,j,k;
  char ch[120],*motcle;
  int i1,i2,i3,i4,id,ii,nb,nr,n;
  int *ilist,nval;
  double val,*dlist;


/*   if ((fdata=fopen(nomdata,"r")) == NULL) */
/*     { */
/*       printf("Impossible d'ouvrir le fichier de donnees : %s\n",nomdata);	 */
/*       syrthes_exit(1) ; */
/*     } */

  motcle=(char*)malloc(120*sizeof(char));   verif_alloue_char("lire_limite_mst",motcle);
  ilist=(int*)malloc(100*sizeof(int));      verif_alloue_int1d("lire_limite_mst",ilist);
  dlist=(double*)malloc(10*sizeof(double)); verif_alloue_double1d("lire_limite_mst",dlist);

  mst->eps=(double*)malloc(mst->nbfbord*sizeof(double)); verif_alloue_double1d("lire_limite_mst",mst->eps);
  for (i=0;i<mst->nbfbord;i++) mst->eps[i]=1.;

  mst->nbray=8;

  while (fgets(ch,120,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"CMST")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"EPSILON")) 
		{
		  rep_ndbl(1,&val,&ii,ch+id);
		  rep_listint(ilist,&nb,ch+id+ii);
		  for (n=0;n<nb;n++)
		    {
		      nr=ilist[n];
		      if (nr==-1)
			for (i=0;i<mst->nbfbord;i++) mst->eps[i]=val;
		      else
			for (i=0;i<mst->nbfbord;i++) 
			  if (mst->refbord[i]==nr) mst->eps[i]=val;
		    }
		}

	      else if (!strcmp(motcle,"NB DIRECTIONS (8,32,128)")) 
		{
		  rep_ndbl(1,&val,&ii,ch+id); nval=(int)val;
		  /*		  if (nval!=8 && nval!=32 && nval!=128 && nval!=512)
		    {
		    printf(" ERREUR : pour le traitement des materiaux semi-transparents\n");
		    printf("          le nombre de directions est 8,32 ou 128\n");
		    syrthes_exit(1);
		    }
		    */
		  mst->nbray=nval;
		}
			           

	    }
	}
    }

  free(motcle); free(ilist); free(dlist);
}
