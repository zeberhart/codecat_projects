# Spanish translation of the thunar-media-tags-plugin.
# Copyright (C) 2006-2007 Jannis Pohlmann <jannis@xfce.org>
# This file is distributed under the same license as the thunar-media-tags-plugin package.
# Abel Martín <abel.martin.ruiz@gmail.com>, 2008.
# 
msgid ""
msgstr ""
"Project-Id-Version: thunar-media-tags-plugin\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-01-24 16:36+0000\n"
"PO-Revision-Date: 2008-10-20 20:27+0100\n"
"Last-Translator: Abel Martín <abel.martin.ruiz@gmail.com>\n"
"Language-Team: Spanish <xfce-i18n@xfce.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"

#: ../thunar-plugin/tag-renamer.c:59 ../thunar-plugin/tag-renamer.c:657
msgid "Title"
msgstr "Título"

#: ../thunar-plugin/tag-renamer.c:60
msgid "Artist - Title"
msgstr "Artista - Título"

#: ../thunar-plugin/tag-renamer.c:61
msgid "Track - Title"
msgstr "Pista - Título"

#: ../thunar-plugin/tag-renamer.c:62
msgid "Track - Artist - Title"
msgstr "Pista - Artista - Título"

#: ../thunar-plugin/tag-renamer.c:63
msgid "Track. Title"
msgstr "Pista. Título"

#: ../thunar-plugin/tag-renamer.c:64
msgid "Track. Artist - Title"
msgstr "Pista. Artista - Título"

#: ../thunar-plugin/tag-renamer.c:65
msgid "Artist - Track - Title"
msgstr "Artista - Pista - Título"

#: ../thunar-plugin/tag-renamer.c:67
msgid "Custom"
msgstr "Personalizado"

#. Custom format
#: ../thunar-plugin/tag-renamer.c:243
msgid "Cust_om format:"
msgstr "Formato per_sonalizado:"

#. Format label
#: ../thunar-plugin/tag-renamer.c:271
msgid "_Format:"
msgstr "_Formato:"

#: ../thunar-plugin/tag-renamer.c:312
msgid "_Underscores"
msgstr "_Subrayados"

#: ../thunar-plugin/tag-renamer.c:314
msgid ""
"Activating this option will replace all spaces in the target filename with "
"underscores."
msgstr ""
"Activar esta opción sustituirá todos los espacios en el nombre del archivo "
"objetivo con subrayados."

#: ../thunar-plugin/tag-renamer.c:319
msgid "_Lowercase"
msgstr "_Minúsculas"

#: ../thunar-plugin/tag-renamer.c:321
msgid ""
"If you activate this, the resulting filename will only contain lowercase "
"letters."
msgstr ""
"Si activa esta opción, el nombre de archivo resultante sólo contendrá letras "
"minúsculas."

#: ../thunar-plugin/tag-renamer.c:464 ../thunar-plugin/audio-tags-page.c:182
msgid "Unknown Artist"
msgstr "Artista desconocido"

#: ../thunar-plugin/tag-renamer.c:472 ../thunar-plugin/audio-tags-page.c:208
msgid "Unknown Title"
msgstr "Título desconocido"

#. Edit tags action
#: ../thunar-plugin/tag-renamer.c:627
msgid "Edit _Tags"
msgstr "Editar _etiquetas"

#: ../thunar-plugin/tag-renamer.c:627
msgid "Edit ID3/OGG tags of this file."
msgstr "Editar etiquetas ID3/OGG de este archivo."

#: ../thunar-plugin/tag-renamer.c:646
msgid "Tag Help"
msgstr "Etiqueta"

#: ../thunar-plugin/tag-renamer.c:657
msgid "Artist"
msgstr "Artista"

#: ../thunar-plugin/tag-renamer.c:658
msgid "Album"
msgstr "Álbum"

#: ../thunar-plugin/tag-renamer.c:658
msgid "Genre"
msgstr "Genero"

#: ../thunar-plugin/tag-renamer.c:659
msgid "Track number"
msgstr "Número de pista"

#: ../thunar-plugin/tag-renamer.c:659
msgid "Year"
msgstr "Año"

#: ../thunar-plugin/tag-renamer.c:660
msgid "Comment"
msgstr "Comentario"

#: ../thunar-plugin/tag-renamer.c:717
msgid "Audio Tags"
msgstr "Etiquetas de audio"

#: ../thunar-plugin/audio-tags-page.c:195
msgid "Unknown Album"
msgstr "Álbum desconocido"

#: ../thunar-plugin/audio-tags-page.c:328
msgid "<b>Track:</b>"
msgstr "<b>Pista:</b>"

#: ../thunar-plugin/audio-tags-page.c:343
msgid "Enter the track number here."
msgstr "Introduzca aquí el número de pista."

#: ../thunar-plugin/audio-tags-page.c:351
msgid "<b>Year:</b>"
msgstr "<b>Año:</b>"

#: ../thunar-plugin/audio-tags-page.c:366
msgid "Enter the release year here."
msgstr "Introduzca aquí el año de publicación."

#: ../thunar-plugin/audio-tags-page.c:374
msgid "<b>Artist:</b>"
msgstr "<b>Artista:</b>"

#: ../thunar-plugin/audio-tags-page.c:381
msgid "Enter the name of the artist or author of this file here."
msgstr "Introduzca aquí el nombre del artista o autor de este archivo."

#: ../thunar-plugin/audio-tags-page.c:389
msgid "<b>Title:</b>"
msgstr "<b>Título:</b>"

#: ../thunar-plugin/audio-tags-page.c:396
msgid "Enter the song title here."
msgstr "Introduzca aquí el título de la canción."

#: ../thunar-plugin/audio-tags-page.c:403
msgid "<b>Album:</b>"
msgstr "<b>Álbum:</b>"

#: ../thunar-plugin/audio-tags-page.c:410
msgid "Enter the album/record title here."
msgstr "Introduzca aquí el título del álbum/disco."

#: ../thunar-plugin/audio-tags-page.c:417
msgid "<b>Comment:</b>"
msgstr "<b>Comentario:</b>"

#: ../thunar-plugin/audio-tags-page.c:424
msgid "Enter your comments here."
msgstr "Introduzca aquí sus comentarios."

#: ../thunar-plugin/audio-tags-page.c:431
msgid "<b>Genre:</b>"
msgstr "<b>Género:</b>"

#: ../thunar-plugin/audio-tags-page.c:438
msgid "Select or enter the genre of this song here."
msgstr "Selecciones el género de esta canción aquí."

#. Create and add the save action
#: ../thunar-plugin/audio-tags-page.c:449
msgid "_Save"
msgstr "_Guardar"

#: ../thunar-plugin/audio-tags-page.c:449
msgid "Save audio tags."
msgstr "Guardar etiquetas de audio."

#. Create and add the info action
#: ../thunar-plugin/audio-tags-page.c:456
msgid "_Information"
msgstr "_Información"

#: ../thunar-plugin/audio-tags-page.c:456
msgid "Display more detailed information about this audio file."
msgstr "Mostrar información más detallada sobre este archivo de audio."

#: ../thunar-plugin/audio-tags-page.c:524
msgid "Audio"
msgstr "Audio"

#. Set up the dialog
#: ../thunar-plugin/audio-tags-page.c:560
msgid "Edit Tags"
msgstr "Editar etiquetas"

#. Create dialog
#: ../thunar-plugin/audio-tags-page.c:982
msgid "Audio Information"
msgstr "Información de audio"

#: ../thunar-plugin/audio-tags-page.c:993
#, c-format
msgid "%d:%d Minutes"
msgstr "%d:%d minutos"

#: ../thunar-plugin/audio-tags-page.c:994
#, c-format
msgid "%d KBit/s"
msgstr "%d KBit/s"

#: ../thunar-plugin/audio-tags-page.c:995
#, c-format
msgid "%d Hz"
msgstr "%d Hz"

#: ../thunar-plugin/audio-tags-page.c:1019
msgid "<b>Filename:</b>"
msgstr "<b>Nombre de archivo:</b>"

#: ../thunar-plugin/audio-tags-page.c:1032
msgid "<b>Filesize:</b>"
msgstr "<b>Tamaño de archivo:</b>"

#: ../thunar-plugin/audio-tags-page.c:1045
msgid "<b>MIME Type:</b>"
msgstr "<b>Tipo MIME:</b>"

#: ../thunar-plugin/audio-tags-page.c:1058
msgid "<b>Bitrate:</b>"
msgstr "<b>Tasa de bits:</b>"

#: ../thunar-plugin/audio-tags-page.c:1071
msgid "<b>Samplerate:</b>"
msgstr "<b>Tasa de muestreo:</b>"

#: ../thunar-plugin/audio-tags-page.c:1084
msgid "<b>Channels:</b>"
msgstr "<b>Canales:</b>"

#: ../thunar-plugin/audio-tags-page.c:1097
msgid "<b>Length:</b>"
msgstr "<b>Duración:</b>"
