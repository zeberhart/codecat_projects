/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include <string.h>

# include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_bd.h"
# include "syr_proto.h"


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | build_octree_3d                                                      |
  |         Construction de l'octree volumique                           |
  |======================================================================| */
void build_octree_3d (struct node *arbre,int npoinl,int nellag,
		      int **nodlag,double **coolag, double *size_min,double dim_boite[],
		      int nbeltmax_tree3d)

{
  struct child *p1;
  struct element *f1,*f2;
  
  
  int i,nbele;
  double dx,dy,dz,dd,ddp;
  double xmin,xmax,ymin,ymax,zmin,zmax;
    

  /* calcul du cube englobant */
  xmin =  1.E10; ymin=  1.E6 ; zmin=  1.E6;
  xmax = -1.E10; ymax= -1.E6 ; zmax= -1.E6;
    
  for (i=0;i<npoinl;i++)
    {
      xmin = min(coolag[0][i],xmin);  xmax = max(coolag[0][i],xmax);
      ymin = min(coolag[1][i],ymin);  ymax = max(coolag[1][i],ymax);
      zmin = min(coolag[2][i],zmin);  zmax = max(coolag[2][i],zmax);
    }

  /* agrandir legerement la boite englobante */
  dx = xmax-xmin; dy=ymax-ymin; dz=zmax-zmin;
  xmin -= (dx*0.01); ymin -= (dy*0.01); zmin -= (dz*0.01);
  xmax += (dx*0.01); ymax += (dy*0.01); zmax += (dz*0.01); 
     

  /* stockage des min et max de la boite */
  dim_boite[0]=xmin; dim_boite[1]=xmax; 
  dim_boite[2]=ymin; dim_boite[3]=ymax; 
  dim_boite[4]=zmin; dim_boite[5]=zmax;     
  /* initialisation de l'octree */
  /*    arbre= (struct node *)malloc(sizeof(struct node)); */
  /*    arbre->name = 1; */
  arbre->xc = (xmin+xmax)*0.5;
  arbre->yc = (ymin+ymax)*0.5;
  arbre->zc = (zmin+zmax)*0.5;
  arbre->sizx = dx*0.5;    
  arbre->sizy = dy*0.5;    
  arbre->sizz = dz*0.5;    
  arbre->lelement = NULL;
  arbre->lfils = NULL;
  *size_min = min(dx,dy); *size_min = min(*size_min,dz);

  /* mise en place de la liste des elements dans le champ 'element' */
  f1 = (struct element *)malloc(sizeof(struct element));
  f1->num = 0;
  f1->suivant=NULL;
  arbre->lelement=f1;
    
  for (i=1;i<nellag;i++)
    {
      f2 = (struct element *)malloc(sizeof(struct element));
      f2->num = i;
      f2->suivant=NULL;
      f1->suivant = f2;
      f1 = f2;
    }
  nbele = nellag;

  decoupe3d(arbre,nodlag,coolag,nbele,size_min,nbeltmax_tree3d);

  /* chris : test d'optimisation */
  /*    elague_tree(arbre);   */
  
  /*    printf("\n\n Arbre apres elaguage\n");
	affiche_tree(arbre,8); */
  
  /* printf("\n Arbre termine\n"); */
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | decoupe3d                                                            |
  |   repartition des elts dans l'octree                                 |
  |======================================================================| */
void decoupe3d(struct node *noeud,int **nodlag,double **coolag,
	       int nbele,double *size_min,
	       int nbeltmax_tree3d)
{
  double xmin[8],xmax[8],ymin[8],ymax[8],zmin[8],zmax[8];
  double x,y,z,dx,dy,dz ;
  int i,nbfac;
  struct node *n1,*n2,*noeudi;
  struct child *f1,*f2;
  struct element *element1;
  
  /*    printf(">>> decoupe3d noeud_name :%d nbele %d \n",noeud->name,nbele); */
  if (nbele>nbeltmax_tree3d)
    {
      /* calcul des xmin, xmax,... de chaque sous cube */
      x = noeud->xc; y = noeud->yc; z = noeud->zc; 
      dx = noeud->sizx; dy = noeud->sizy; dz = noeud->sizz;
      
      xmax[0]=xmax[3]=xmax[4]=xmax[7]= x;
      xmin[1]=xmin[2]=xmin[5]=xmin[6]= x;
      xmin[0]=xmin[3]=xmin[4]=xmin[7]= x - dx;
      xmax[1]=xmax[2]=xmax[5]=xmax[6]= x + dx;
      
      ymax[0]=ymax[1]=ymax[2]=ymax[3]= y;
      ymin[4]=ymin[5]=ymin[6]=ymin[7]= y;
      ymin[0]=ymin[1]=ymin[2]=ymin[3]= y - dy;
      ymax[4]=ymax[5]=ymax[6]=ymax[7]= y + dy;
      
      zmax[2]=zmax[3]=zmax[6]=zmax[7]= z;
      zmin[0]=zmin[1]=zmin[4]=zmin[5]= z;
      zmin[2]=zmin[3]=zmin[6]=zmin[7]= z - dz;
      zmax[0]=zmax[1]=zmax[4]=zmax[5]= z + dz;


      /* generation des fils */
      f1= (struct child *)malloc(sizeof(struct child));
      n1= (struct node *) malloc(sizeof(struct node ));

      noeud->lfils = f1;
       /*    f1->name = (noeud->name)*10 + 1; */
      f1->fils = n1;
      f1->suivant = NULL;

      for (i=1;i<8;i++)
	  {
	      f2= (struct child *)malloc(sizeof(struct child));
	      n2= (struct node *) malloc(sizeof(struct node ));
	      f1->suivant = f2;
	      /*     f2->name = (noeud->name)*10 + i+1; */
	      f2->fils = n2;
	      f2->suivant = NULL;
	      f1 = f2;
	  }

      /* remplissage des noeuds lies aux fils crees */

      f1 = noeud->lfils;

      for (i=0;i<8;i++)
	{
	  noeudi = f1->fils;
	  /*  noeudi->name =  (noeud->name)*10 + i+1; */
	  noeudi->xc = (xmin[i]+xmax[i])*0.5;
	  noeudi->yc = (ymin[i]+ymax[i])*0.5;
	  noeudi->zc = (zmin[i]+zmax[i])*0.5;
	  noeudi->sizx = (xmax[i]-xmin[i])*0.5;
	  noeudi->sizy = (ymax[i]-ymin[i])*0.5;
	  noeudi->sizz = (zmax[i]-zmin[i])*0.5;
	  *size_min = min(*size_min,noeudi->sizx);
	  *size_min = min(*size_min,noeudi->sizy);
	  *size_min = min(*size_min,noeudi->sizz);
	  noeudi->lfils = NULL;
	  element1= (struct element *)malloc(sizeof(struct element));
	  noeudi->lelement = element1;
	  
	  tritetra(noeud->lelement,noeudi->lelement,
		   &nbfac,nodlag,coolag,
		   noeudi->xc,noeudi->yc,noeudi->zc,
		   noeudi->sizx,noeudi->sizy,noeudi->sizz);
	  
	  if (nbfac != 0)
	    decoupe3d(noeudi,nodlag,coolag,nbfac,size_min,nbeltmax_tree3d);
	  else
	    {
	      noeudi->lelement = NULL;
	      free(element1);
	    }
	  
	  f1 = f1->suivant;
	  
	}
      
    }
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | tritetra                                                             |
  |         Tri des tetraedres pour les placer dans l'octree             |
  |======================================================================| */
void tritetra( struct element *elt_pere, struct element *elt_fils, 
              int *nbfac,int **nodlag,double **coolag,
              double xcc,double ycc,double zcc,double dx,double dy,double dz)
{
  int i,n,prem ;
  double xa,ya,za,xb,yb,zb,xc,yc,zc,xd,yd,zd;
  struct element *fp1,*ff1,*ff2;
  
  prem = 1;
  fp1 = elt_pere;
  ff1 = elt_fils;
  *nbfac = 0;
  
  /* pour chaque tetraedre de la liste en cours */
  do
    {
      /* numero des noeuds de l'element  et coordonnees */
      n=nodlag[0][fp1->num];  xa=coolag[0][n];  ya=coolag[1][n]; za=coolag[2][n];
      n=nodlag[1][fp1->num];  xb=coolag[0][n];  yb=coolag[1][n]; zb=coolag[2][n];
      n=nodlag[2][fp1->num];  xc=coolag[0][n];  yc=coolag[1][n]; zc=coolag[2][n];
      n=nodlag[3][fp1->num];  xd=coolag[0][n];  yd=coolag[1][n]; zd=coolag[2][n];

      /* si le tetraedre appartient au cube
	 on la rajoute a la table des tetraedres du fils */
      if (tetra_in_cube(xa,ya,za,xb,yb,zb,xc,yc,zc,xd,yd,zd,
			xcc,ycc,zcc,dx,dy,dz))
	{
	  if (prem)
	    {
	      prem = 0;
	      ff1->num = fp1->num;
	      ff1->suivant = NULL;
	    }
	  else
	    {
	      ff2= (struct element *)malloc(sizeof(struct element));
	      ff2->num = fp1->num;
	      ff2->suivant = NULL;
	      ff1->suivant = ff2;
	      ff1 = ff2;
	    }
	  *nbfac += 1;
	  
	}
      
      fp1 = fp1->suivant;
      
    }while (fp1 != NULL);
  
   
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | tetra_in_cube                                                        |
  |         Tri des tetraedres pour les placer dans l'octree             |
  |======================================================================| */
int tetra_in_cube(double xa,double ya,double za,double xb,double yb,double zb,
		 double xc,double yc,double zc,double xd,double yd,double zd,
		 double xcc,double ycc,double zcc,double dx,double dy,double dz)
{
  double xmin,xmax,ymin,ymax,zmin,zmax;
  double ta,tb,tc,td,xab,yab,zab,xac,yac,zac,xad,yad,zad;
  double xbd,ybd,zbd,xcd,ycd,zcd,xbc,ybc,zbc;
  double xp,yp,zp;
  double epsi,dxy,dxz,dyz,d3,t;
  
  t=0.;
  epsi=1.E-5;
  dxy= dx+dy; dxz=dx+dz; dyz=dy+dz;
  d3=dxy+dz;
  
  xmin = xcc-1.05*dx; xmax = xcc+1.05*dx;
  ymin = ycc-1.05*dy; ymax = ycc+1.05*dy;
  zmin = zcc-1.05*dz; zmax = zcc+1.05*dz;
  
  /* il existe  1 point dans le cube => intersection*/
  if (in_boite (xa,ya,za,xmin,xmax,ymin,ymax,zmin,zmax))
    return(1);
  else if (in_boite (xb,yb,zb,xmin,xmax,ymin,ymax,zmin,zmax))
    return(1);
  else if (in_boite (xc,yc,zc,xmin,xmax,ymin,ymax,zmin,zmax))
    return(1);
  else if (in_boite (xd,yd,zd,xmin,xmax,ymin,ymax,zmin,zmax))
    return(1);
  
  
  /* tous les points sont du meme cote d'un plan ==> pas intersection */
  else if (xa>xmax && xb>xmax && xc>xmax && xd>xmax)
    return(0);
  else if (xa<xmin && xb<xmin && xc<xmin && xd<xmin)
    return(0);
  else if (ya>ymax && yb>ymax && yc>ymax && yd>ymax)
    return(0);
  else if (ya<ymin && yb<ymin && yc<ymin && yd<ymin)
    return(0);
  else if (za>zmax && zb>zmax && zc>zmax && zd>zmax)
    return(0);
  else if (za<zmin && zb<zmin && zc<zmin && zd<zmin)
    return(0);
  
  else
    {
      /* translation a l'origine */
      xa -= xcc; ya -= ycc; za -= zcc;
      xb -= xcc; yb -= ycc; zb -= zcc;
      xc -= xcc; yc -= ycc; zc -= zcc;
      xd -= xcc; yd -= ycc; zd -= zcc;
      
      /* exterieur des plans qui passent par les aretes ==> pas intersection */
      if (xa-za<-dxz && xb-zb<-dxz && xc-zc<-dxz && xd-zd<-dxz)
	return(0);
      else if (xa-za>dxz  && xb-zb>dxz  && xc-zc>dxz  && xd-zd>dxz)
	return(0);
      else if (xa+za<-dxz && xb+zb<-dxz && xc+zc<-dxz && xd+zd<-dxz)
	return(0);
      else if (xa+za>dxz  && xb+zb>dxz  && xc+zc>dxz  && xd+zd>dxz)
	return(0);
      else if (ya-za<-dyz && yb-zb<-dyz && yc-zc<-dyz && yd-zd<-dyz)
	return(0);
      else if (ya-za>dyz  && yb-zb>dyz  && yc-zc>dyz  && yd-zd>dyz)
	return(0);
      else if (ya+za<-dyz && yb+zb<-dyz && yc+zc<-dyz && yd+zd<-dyz)
	return(0);
      else if (ya+za>dyz  && yb+zb>dyz  && yc+zc>dyz  && yd+zd>dyz)
	return(0);
      else if (xa-ya<-dxy && xb-yb<-dxy && xc-yc<-dxy && xd-yd<-dxy)
	return(0);
      else if (xa-ya>dxy  && xb-yb>dxy  && xc-yc>dxy  && xd-yd>dxy)
	return(0);
      else if (xa+ya<-dxy && xb+yb<-dxy && xc+yc<-dxy && xd+yd<-dxy)
	return(0);
      else if (xa+ya>dxy  && xb+yb>dxy  && xc+yc>dxy  && xd+yd>dxy)
	return(0);
      
      /* exterieur des plans qui passent par les coins ==> pas intersection */
      else if ( xa+ya-za>d3 &&  xb+yb-zb>d3 &&  xc+yc-zc>d3 && xd+yd-zd>d3)
	return(0);
      else if (-xa+ya-za>d3 && -xb+yb-zb>d3 && -xc+yc-zc>d3 && -xd+yd-zd>d3)
	return(0);
      else if (-xa+ya+za>d3 && -xb+yb+zb>d3 && -xc+yc+zc>d3 && -xd+yd+zd>d3)
	return(0);
      else if ( xa+ya+za>d3 &&  xb+yb+zb>d3 &&  xc+yc+zc>d3 && xd+yd+zd>d3)
	return(0);
      else if ( xa-ya-za>d3 &&  xb-yb-zb>d3 &&  xc-yc-zc>d3 && xd-yd-zd>d3)
	return(0);
      else if (-xa-ya-za>d3 && -xb-yb-zb>d3 && -xc-yc-zc>d3 && -xd-yd-zd>d3)
	return(0);
      else if (-xa-ya+za>d3 && -xb-yb+zb>d3 && -xc-yc+zc>d3 && -xd-yd+zd>d3)
	return(0);
      else if ( xa-ya+za>d3 &&  xb-yb+zb>d3 &&  xc-yc+zc>d3 && xd-yd+zd>d3)
	return(0);
      
      /* intersections segments cube */
      else if (seg_cubex( dx,dy,dz,xa,xb,ya,yb,za,zb))
	return(1);
      else if (seg_cubex(-dx,dy,dz,xa,xb,ya,yb,za,zb))
	return(1);
      else if (seg_cubey(dx, dy,dz,xa,xb,ya,yb,za,zb))
	return(1);
      else if (seg_cubey(dx,-dy,dz,xa,xb,ya,yb,za,zb))
	return(1);
      else if (seg_cubez(dx,dy, dz,xa,xb,ya,yb,za,zb))
	return(1);
      else if (seg_cubez(dx,dy,-dz,xa,xb,ya,yb,za,zb))
	return(1);
      
      else if (seg_cubex( dx,dy,dz,xb,xc,yb,yc,zb,zc))
	return(1);
      else if (seg_cubex(-dx,dy,dz,xb,xc,yb,yc,zb,zc))
	return(1);
      else if (seg_cubey(dx, dy,dz,xb,xc,yb,yc,zb,zc))
	return(1);
      else if (seg_cubey(dx,-dy,dz,xb,xc,yb,yc,zb,zc))
	return(1);
      else if (seg_cubez(dx,dy, dz,xb,xc,yb,yc,zb,zc))
	return(1);
      else if (seg_cubez(dx,dy,-dz,xb,xc,yb,yc,zb,zc))
	return(1);
      
      else if (seg_cubex( dx,dy,dz,xa,xc,ya,yc,za,zc))
	return(1);
      else if (seg_cubex(-dx,dy,dz,xa,xc,ya,yc,za,zc))
	return(1);
      else if (seg_cubey(dx, dy,dz,xa,xc,ya,yc,za,zc))
	return(1);
      else if (seg_cubey(dx,-dy,dz,xa,xc,ya,yc,za,zc))
	return(1);
      else if (seg_cubez(dx,dy, dz,xa,xc,ya,yc,za,zc))
	return(1);
      else if (seg_cubez(dx,dy,-dz,xa,xc,ya,yc,za,zc))
	return(1);
      
      else if (seg_cubex( dx,dy,dz,xa,xd,ya,yd,za,zd))
	return(1);
      else if (seg_cubex(-dx,dy,dz,xa,xd,ya,yd,za,zd))
	return(1);
      else if (seg_cubey(dx, dy,dz,xa,xd,ya,yd,za,zd))
	return(1);
      else if (seg_cubey(dx,-dy,dz,xa,xd,ya,yd,za,zd))
	return(1);
      else if (seg_cubez(dx,dy, dz,xa,xd,ya,yd,za,zd))
	return(1);
      else if (seg_cubez(dx,dy,-dz,xa,xd,ya,yd,za,zd))
	return(1);
      
      else if (seg_cubex( dx,dy,dz,xb,xd,yb,yd,zb,zd))
	return(1);
      else if (seg_cubex(-dx,dy,dz,xb,xd,yb,yd,zb,zd))
	return(1);
      else if (seg_cubey(dx, dy,dz,xb,xd,yb,yd,zb,zd))
	return(1);
      else if (seg_cubey(dx,-dy,dz,xb,xd,yb,yd,zb,zd))
	return(1);
      else if (seg_cubez(dx,dy, dz,xb,xd,yb,yd,zb,zd))
	return(1);
      else if (seg_cubez(dx,dy,-dz,xb,xd,yb,yd,zb,zd))
	return(1);
      
      else if (seg_cubex( dx,dy,dz,xc,xd,yc,yd,zc,zd))
	return(1);
      else if (seg_cubex(-dx,dy,dz,xc,xd,yc,yd,zc,zd))
	return(1);
      else if (seg_cubey(dx, dy,dz,xc,xd,yc,yd,zc,zd))
	return(1);
      else if (seg_cubey(dx,-dy,dz,xc,xd,yc,yd,zc,zd))
	return(1);
      else if (seg_cubez(dx,dy, dz,xc,xd,yc,yd,zc,zd))
	return(1);
      else if (seg_cubez(dx,dy,-dz,xc,xd,yc,yd,zc,zd))
	return(1);

	
      /* triangle enfonce dans un coin */
      else
	{
	  /* equation du plan du triangle (a,b,c)*/
	  xab=xb-xa; yab=yb-ya; zab=zb-za;
	  xac=xc-xa; yac=yc-ya; zac=zc-za;
	  ta = yab*zac-zab*yac;
	  tb = zab*xac-xab*zac;
	  tc = xab*yac-yab*xac;
	  td = -(ta*xa+tb*ya+tc*za);
	  
	  if (diag_tria(ta,tb,tc,td,dx,dy,dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xac,yac,zac,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,dx,dy,-dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xac,yac,zac,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,-dx,dy,dz,&t))
	    {
	      xp=-t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xac,yac,zac,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,-dx,dy,-dz,&t))
	    {
	      xp=-t*dx; yp=t*dy; zp=-t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xac,yac,zac,xp,yp,zp))
		return(1);
	    }
	  
	  /* equation du plan du triangle (a,b,d)*/
	  xad=xd-xa; yad=yd-ya; zad=zd-za;
	  ta = yab*zad-zab*yad;
	  tb = zab*xad-xab*zad;
	  tc = xab*yad-yab*xad;
	  td = -(ta*xa+tb*ya+tc*za);
	  
	  if (diag_tria(ta,tb,tc,td,dx,dy,dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xad,yad,zad,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,dx,dy,-dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xad,yad,zad,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,-dx,dy,dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xad,yad,zad,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,-dx,dy,-dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xad,yad,zad,xp,yp,zp))
		return(1);
	    }

	  /* equation du plan du triangle (a,c,d)*/
	  ta = yac*zad-zac*yad;
	  tb = zac*xad-xac*zad;
	  tc = xac*yad-yac*xad;
	  td = -(ta*xa+tb*ya+tc*za);
	  
	  if (diag_tria(ta,tb,tc,td,dx,dy,dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xac,yac,zac,xad,yad,zad,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,dx,dy,-dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xac,yac,zac,xad,yad,zad,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,-dx,dy,dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xac,yac,zac,xad,yad,zad,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,-dx,dy,-dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xac,yac,zac,xad,yad,zad,xp,yp,zp))
		return(1);
	    }
	  
	  
	  
	  /* equation du plan du triangle (b,c,d)*/
	  xbc=xc-xb; ybc=yc-yb; zbc=zd-zb;
	  xbd=xd-xb; ybd=yd-yb; zbd=zd-zb;
	  ta = ybc*zbd-zbc*ybd;
	  tb = zbc*xbd-xbc*zbd;
	  tc = xbc*ybd-ybc*xbd;
	  td = -(ta*xa+tb*ya+tc*za);
	  
	  if (diag_tria(ta,tb,tc,td,dx,dy,dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xb,yb,zb,xbc,ybc,zbc,xbd,ybd,zbd,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,dx,dy,-dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xb,yb,zb,xbc,ybc,zbc,xbd,ybd,zbd,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,-dx,dy,dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xb,yb,zb,xbc,ybc,zbc,xbd,ybd,zbd,xp,yp,zp))
		return(1);
	    }
	  if (diag_tria(ta,tb,tc,td,-dx,dy,-dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xb,yb,zb,xbc,ybc,zbc,xbd,ybd,zbd,xp,yp,zp))
		return(1);
	    }
	  
	  }
    }
  return(2);
}



