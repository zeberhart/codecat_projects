/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#ifndef _CONST_H_
#define _CONST_H_

/* longueur des chaines lues dans le fichier syrthes.data */
#define CHLONG 1000

/* adresses des variables pour les cond limites */
#define ADR_T 0
#define ADR_PV 1
#define ADR_PT 2

/* codes des conditions aux limites */

#define POSTYPFLUX 0

#define POSFLUX       1
#define POSECH        2
#define POSRESCON     3
#define POSRAYINF     4
#define POSCOUPF      5
#define POSCOUPR      6
#define POSHMTHHH     7


#define POSDIRIC 20
#define POSPERIO 21

#define TYPRCOUPS 30
#define TYPRTI 31
#define TYPRFI 32
#define TYPRVITRE 33

#define MAXPOS 35

/* nombre max de references */
#define MAX_REF 1000
#define MAX_BIL 1000


/* constantes */
#define sigma 5.6696e-8
#define tkel 273.15
#define Pi 3.141592653589793

#endif


