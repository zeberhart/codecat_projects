/*
     This file is part of GNUnet.
     (C) 2007, 2011 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/peerinfo/gnunet-peerinfo-gtk-flags.c
 * @brief flag lookup
 * @author Christian Grothoff
 */
#include "gnunet_gtk.h"
#include "gnunet-peerinfo-gtk-flags.h"

/**
 * Entry we keep for each flag.
 */
struct Flag
{
  /**
   * This is a linked list.
   */
  struct Flag *next;

  /**
   * This is a linked list.
   */
  struct Flag *prev;

  /**
   * Associated country code.
   */
  char *cc;

  /**
   * Flag image.
   */
  GdkPixbuf *flag;
};


/**
 * Head of the DLL of loaded flags.
 */
static struct Flag *flags_head;

/**
 * Tail of the DLL of loaded flags.
 */
static struct Flag *flags_tail;


/**
 * Lookup the flag image for the given country code
 *
 * @return NULL on error
 */
GdkPixbuf *
GNUNET_PEERINFO_GTK_get_flag (const char *cc)
{
  GdkPixbuf *flagBuf;
  char *mcc;
  char *dir;
  char *fn;
  size_t i;
  struct Flag *pos;

  if (NULL == cc)
    return NULL;
  if ((0 == strcasecmp (cc, "edu")) || (0 == strcasecmp (cc, "com")) ||
      (0 == strcasecmp (cc, "net")) || (0 == strcasecmp (cc, "org")) ||
      (0 == strcasecmp (cc, "gov")) || (0 == strcasecmp (cc, "mil")))
    cc = "us";
  if (0 == strcasecmp (cc, "uk"))
    cc = "gb";
  if (strlen (cc) > 2)
    return NULL;
  pos = flags_head;
  while (pos != NULL)
  {
    if (0 == strcmp (pos->cc, cc))
      return pos->flag;
    pos = pos->next;
  }
  mcc = GNUNET_strdup (cc);
  for (i = 0; i < strlen (mcc); i++)
    mcc[i] = tolower (mcc[i]);
  dir = GNUNET_GTK_installation_get_path (GNUNET_OS_IPK_DATADIR);
  GNUNET_asprintf (&fn, "%sflags%s%s.png", dir,
                   DIR_SEPARATOR_STR, mcc);
  GNUNET_free (dir);
  flagBuf = gdk_pixbuf_new_from_file (fn, NULL);
  pos = GNUNET_malloc (sizeof (struct Flag));
  pos->cc = mcc;
  pos->flag = flagBuf;
  GNUNET_CONTAINER_DLL_insert (flags_head, flags_tail, pos);
  return flagBuf;
}


/**
 * Deallocate all cached flags.
 */
void
GNUNET_PEERINFO_GTK_flags_shutdown ()
{
  struct Flag *flag;

  while (NULL != (flag = flags_head))
  {
    GNUNET_CONTAINER_DLL_remove (flags_head, flags_tail, flag);
    if (flag->flag != NULL)
      g_object_unref (flag->flag);
    GNUNET_free (flag->cc);
    GNUNET_free (flag);
  }
}


/* end of gnunet-peerinfo-gtk-flags.c */
