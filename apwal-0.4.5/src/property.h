/* property.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#ifndef PROPERTY__H
#define PROPERTY__H

#include "common.h"

#define PROPERTY_OK      1
#define PROPERTY_CANCEL  2
#define PROPERTY_DELETE  3
#define PROPERTY_REMOVE  4

typedef struct property_t
{
  struct apwalapp_t *apwal;
  GtkWidget   *entry_cmdline;
  GtkWidget   *entry_path;
  GtkWidget   *entry_icon;
  GtkWidget   *img_icon;
  GtkWidget   *btn_remove;
  
} property_t;

// ----------------------------------------------------------------------------
property_t * property_new(struct apwalapp_t *apwal);
void         property_refresh(property_t *prop);


#endif /* PROPERTY__H*/
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
