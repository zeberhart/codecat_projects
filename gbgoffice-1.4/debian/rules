#!/usr/bin/make -f

%:
	dh $@

TMP=$(CURDIR)/debian/gbgoffice

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

CPPFLAGS := $(shell dpkg-buildflags --get CPPFLAGS)
CXXFLAGS := -Wall $(shell dpkg-buildflags --get CXXFLAGS)
LDFLAGS := -Wl,--as-needed -Wl,-z,defs $(shell dpkg-buildflags --get LDFLAGS)

ifeq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
  confflags += --build=$(DEB_HOST_GNU_TYPE)
else
  confflags += --build=$(DEB_BUILD_GNU_TYPE) --host=$(DEB_HOST_GNU_TYPE)
endif

override_dh_auto_configure:
	CXXFLAGS="$(CXXFLAGS)" LDFLAGS="$(LDFLAGS)" CPPFLAGS="$(CPPFLAGS)" \
		./autogen.sh \
	       $(confflags) --prefix=/usr \
	       --mandir=\$${prefix}/share/man --infodir=\$${prefix}/share/info \
	       --enable-dictionary-dirs=/usr/local/share/bgoffice:/usr/share/bgoffice
	desktop-file-validate debian/gbgoffice.desktop

override_dh_auto_clean:
	# avoid re-generation of Makefile, pretend it is just created
	if [ -e Makefile ]; then \
		touch configure.in && \
		touch aclocal.m4 && \
		touch Makefile.in pixmaps/Makefile.in src/Makefile.in && \
		touch configure && \
		touch config.status && \
		touch Makefile pixmaps/Makefile src/Makefile && \
		$(MAKE) distclean ; \
	fi

override_dh_auto_install:
	dh_auto_install
	
	convert -resize 32x32 \
	    $(TMP)/usr/share/bgoffice/gbgoffice-icon.png \
	    $(TMP)/usr/share/bgoffice/gbgoffice-icon.xpm

	dh_link /usr/share/bgoffice/gbgoffice-icon.png \
		/usr/share/pixmaps/gbgoffice-icon.png
	install -D -m 0644 debian/gbgoffice.desktop \
			   $(TMP)/usr/share/applications/gbgoffice.desktop

override_dh_installchangelogs:
	iconv -f CP1251 -t UTF-8 ChangeLog > ChangeLog.UTF-8
	dh_installchangelogs ChangeLog.UTF-8

override_dh_installman:
	dh_installman gbgoffice.1
