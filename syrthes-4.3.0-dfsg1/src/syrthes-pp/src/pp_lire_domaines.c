/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pp_usertype.h"
#include "pp_bd.h"
#include "pp_proto.h"


/*|======================================================================|
  | SYRTHES 4.3                                        COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture de syrthes.env                                        |
  |======================================================================| */
void pp_lire_domaines(char *nomdom,rp_int nbliredom,rp_int nelem,
		     rp_int *numdome,rp_int *nbdomtot)
{
  rp_int i,j,ne,nb,nbdom,nbd;
  char nomfich[300],chnum[6];
  FILE *fd;
  rp_int form;

  
  if (SYRTHES_LANG == FR) printf("\n *** LIRE_DOMAINE : lecture du pre-partitionnement\n");
  else if (SYRTHES_LANG == EN) printf("\n *** LIRE_DOMAINE : read of the existing partitioning\n");

  form=1;
  if (!strncmp(nomdom+strlen(nomdom)-5, ".domb",5)) form=0;

  for (nbd=ne=i=0;i<nbliredom;i++)
    {
      strcpy(nomfich,nomdom); 
      strncat(nomfich,".",1);
      sprintf(chnum,"%05d",i);
      strncat(nomfich,chnum,5);

      if (form==1)
	{
	  if ( (fd=fopen(nomfich,"r")) == NULL )
	    {
	      if (SYRTHES_LANG == FR) printf("Impossible d'ouvrir le fichier %s\n",nomfich);	
	      else if (SYRTHES_LANG == EN) printf("Unable to open file : %s\n",nomfich);	
	      exit(1) ;
	    }

	  if (PP_TYPEWIDTH ==32)
	    {
	      fscanf(fd,"%d",&nb);
	      fscanf(fd,"%d",&nbdom);
	      if (SYRTHES_LANG == FR) printf("         - maillage %d nelem=%d nbdom=%d\n",i,nb,nbdom);
	      else if (SYRTHES_LANG == EN) printf("         - meshe %d nelem=%d nbdom=%d\n",i,nb,nbdom);
	      for (j=0;j<nb;j++) fscanf(fd,"%d",&(numdome[ne+j]));
	    }
	  else
	    {
	      fscanf(fd,"%ld",&nb);
	      fscanf(fd,"%ld",&nbdom);
	      if (SYRTHES_LANG == FR) printf("         - maillage %d nelem=%d nbdom=%d\n",i,nb,nbdom);
	      else if (SYRTHES_LANG == EN) printf("         - meshe %d nelem=%d nbdom=%d\n",i,nb,nbdom);
	      for (j=0;j<nb;j++) fscanf(fd,"%ld",&(numdome[ne+j]));
	    }
	}
      else
	{
	  if ( (fd=fopen(nomfich,"rb")) == NULL )
	    {
	      if (SYRTHES_LANG == FR) printf("Impossible d'ouvrir le fichier %s\n",nomfich);	
	      else if (SYRTHES_LANG == EN) printf("Unable to open file : %s\n",nomfich);	
	      exit(1) ;
	    }

	  fseek(fd,0,SEEK_SET);
	  fread(&nb,sizeof(rp_int),1,fd);
	  fread(&nbdom,sizeof(rp_int),1,fd);
	  printf("         - maillage %d nelem=%d nbdom=%d\n",i,nb,nbdom);
	  fread(numdome+ne,sizeof(rp_int),nb,fd);
	}

      for (j=0;j<nb;j++) numdome[ne+j]+=nbd;

      ne+=nb;
      nbd+=nbdom;

      fclose(fd);
    }

  *nbdomtot=nbd;

  if (ne!=nelem)
    {
      if (SYRTHES_LANG == FR){
	printf(" ERREUR LIRE_DOMAINE : on n'a pas lu le bon nombre de numeros de domaines\n");
	printf("                       on a lu %d numeros pour %d elements\n",ne,nelem);
      }
      else if (SYRTHES_LANG == EN) {
	printf(" ERREUR LIRE_DOMAINE : the number of sub-domains read is wrong\n");
	printf("                       one has read %d numbers for %d elements\n",ne,nelem);
      }
    }

  
  /* impressions de controle */
  if (SYRTHES_LANG == FR)
    printf("         --> le maillage complet (%d elements) est partitionne en %d domaines\n",nelem,*nbdomtot);
  else if (SYRTHES_LANG == EN) 
    printf("         --> the whole mesh (%d elements) is partitioned into %d domains\n",nelem,*nbdomtot);

}


