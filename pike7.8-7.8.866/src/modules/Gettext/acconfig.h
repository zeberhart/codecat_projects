/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: ec342cd6fb191f5d34b6d347f0bfbad96d2f0dbf $
*/

#ifndef GETTEXT_CONFIG_H
#define GETTEXT_CONFIG_H

@TOP@

/* define if you have gettext */
#undef HAVE_GETTEXT

/* Defined if bindtextdomain(3) copes with NULL as dirname */
#undef BINDTEXTDOMAIN_HANDLES_NULL

@BOTTOM@

#endif
