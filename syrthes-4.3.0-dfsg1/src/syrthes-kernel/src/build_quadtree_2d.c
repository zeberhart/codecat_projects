/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include <string.h>

# include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_bd.h"
# include "syr_proto.h"


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | build_octree_3d                                                      |
  |         Construction du quadtree (volumique) en 2D                   |
  |======================================================================| */
void build_quadtree_2d (struct node *arbre, int npoinl,int nellag,
			int **nodlag,double **coolag, double *size_min,
			double dim_boite[],int nbeltmax_tree3d)
{
  struct child *p1;
  struct element *f1,*f2;
    
  
  int i,nbelt;
  double dx,dy,dz,dd,ddp;
  double xmin,xmax,ymin,ymax,zmin,zmax;
  
  
  /* calcul du rectangle englobant */
  xmin =  1.E10; ymin=  1.E6 ;
  xmax = -1.E10; ymax= -1.E6 ;
    
  for (i=0;i<npoinl;i++)
    {
      xmin = min(coolag[0][i],xmin);  xmax = max(coolag[0][i],xmax);
      ymin = min(coolag[1][i],ymin);  ymax = max(coolag[1][i],ymax);
    }


  /* agrandir legerement le carre englobant */
  dx = xmax-xmin; dy=ymax-ymin;
  xmin -= (dx*0.01); ymin -= (dy*0.01); 
  xmax += (dx*0.01); ymax += (dy*0.01); 
  
  
  /* stockage des min et max du carre */
  dim_boite[0]=xmin; dim_boite[1]=xmax; 
  dim_boite[2]=ymin; dim_boite[3]=ymax; 
  
  /* initialisation du quadtree */
/*    arbre= (struct node *)malloc(sizeof(struct node)); */
/*    arbre->name = 1; */
  arbre->xc = (xmin+xmax)*0.5;
  arbre->yc = (ymin+ymax)*0.5;
  arbre->sizx = dx*0.5;    
  arbre->sizy = dy*0.5;    
  arbre->lelement = NULL;
  arbre->lfils = NULL;
  *size_min = min(dx,dy);
  
    /* mise en place de la liste des elements dans le champ 'element' */
  f1 = (struct element *)malloc(sizeof(struct element));
  f1->num = 0;
  f1->suivant=NULL;
  arbre->lelement=f1;
  
  for (i=1;i<nellag;i++)
    {
      f2 = (struct element *)malloc(sizeof(struct element));
      f2->num = i;
      f2->suivant=NULL;
      f1->suivant = f2;
      f1 = f2;
    }
  nbelt = nellag;
  
  decoupe2d(arbre,nodlag,coolag,nbelt,size_min,nbeltmax_tree3d);
  
  
  /*    elague_tree(arbre); */
  
  /* printf("\n\n Arbre apres elaguage\n");
     affiche_tree(arbre,4);   */
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | decoupe2d                                                            |
  |  repartition des elts dans le quadtree                               |
  |======================================================================| */
void decoupe2d(struct node *noeud,int **nodlag,double **coolag,
	       int nbelt,double *size_min,
	       int nbeltmax_tree3d)
{
  double xmin[4],xmax[4],ymin[4],ymax[4];
  double x,y,dx,dy ;
  int i,nbfac;
  struct node *n1,*n2,*noeudi;
  struct child *f1,*f2;
  struct element *elt1;
  
  if (nbelt>nbeltmax_tree3d)
    {
      
      /* calcul des xmin, xmax,... de chaque sous carre */
      x = noeud->xc; y = noeud->yc;
      dx = noeud->sizx; dy = noeud->sizy;

      xmax[0]=xmax[3]= x;
      xmin[1]=xmin[2]= x;
      xmin[0]=xmin[3]= x - dx;
      xmax[1]=xmax[2]= x + dx;

      ymax[2]=ymax[3]= y;
      ymin[0]=ymin[1]= y;
      ymin[2]=ymin[3]= y - dy;
      ymax[0]=ymax[1]= y + dy;


      /* generation des fils */
      f1= (struct child *)malloc(sizeof(struct child));
      n1= (struct node *) malloc(sizeof(struct node ));

      noeud->lfils = f1;
/*      f1->name = (noeud->name)*10 + 1; */
      f1->fils = n1;
      f1->suivant = NULL;

      for (i=1;i<4;i++)
	  {
	      f2= (struct child *)malloc(sizeof(struct child));
	      n2= (struct node *) malloc(sizeof(struct node ));
	      f1->suivant = f2;
/*	      f2->name = (noeud->name)*10 + i+1; */
	      f2->fils = n2;
	      f2->suivant = NULL;
	      f1 = f2;
	  }

      /* remplissage des noeuds lies aux fils crees */

      f1 = noeud->lfils;

      for (i=0;i<4;i++)
	{
	  noeudi = f1->fils;
	  noeudi->xc = (xmin[i]+xmax[i])*0.5;
	  noeudi->yc = (ymin[i]+ymax[i])*0.5;
	  noeudi->sizx = (xmax[i]-xmin[i])*0.5;
	  noeudi->sizy = (ymax[i]-ymin[i])*0.5;
	  *size_min = min(*size_min,noeudi->sizx);
	  *size_min = min(*size_min,noeudi->sizy);
	  noeudi->lfils = NULL;
	  elt1= (struct element *)malloc(sizeof(struct element));
	  noeudi->lelement = elt1;
	  
	  tritria(noeud->lelement,noeudi->lelement,
		  &nbfac,nodlag,coolag,
		  noeudi->xc,noeudi->yc,noeudi->sizx,noeudi->sizy);
	  
	  if (nbfac != 0)
	    decoupe2d(noeudi,nodlag,coolag,nbfac,size_min,
		      nbeltmax_tree3d);
	  else
	    {
	      noeudi->lelement = NULL;
	      free(elt1);
	    }
	  
	  f1 = f1->suivant;
	}
      
    }
  
}

/*|======================================================================|
  | SYRTHES 2.1                JANV 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | tritria                                                              |
  |         Tri des triangles pour les placer dans le quadtree           |
  |======================================================================| */
void tritria( struct element *face_pere, struct element *face_fils, 
              int *nbfac,int **nodlag,double **coolag,
              double xcc,double ycc,double dx,double dy)
{
  int i,n,prem ;
  double xa,ya,xb,yb,xc,yc;
  struct element *fp1,*ff1,*ff2;
  
  prem = 1;
  fp1 = face_pere;
  ff1 = face_fils;
  *nbfac = 0;
  
  /* pour chaque element de la liste en cours */
  do
    {
      /* numero des noeuds de la element et coordonnees */
      n=nodlag[0][fp1->num];  xa=coolag[0][n];  ya=coolag[1][n];
      n=nodlag[1][fp1->num];  xb=coolag[0][n];  yb=coolag[1][n];
      n=nodlag[2][fp1->num];  xc=coolag[0][n];  yc=coolag[1][n];

      /* si le triangle appartient au carre
	 on le rajoute a la table des triangles du fils */
      if (tria_in_rectan(xa,ya,xb,yb,xc,yc,xcc,ycc,dx,dy))
	{
	  if (prem)
	    {
	      prem = 0;
	      ff1->num = fp1->num;
	      ff1->suivant = NULL;
	    }
	  else
	    {
	      ff2= (struct element *)malloc(sizeof(struct element));
	      ff2->num = fp1->num;
	      ff2->suivant = NULL;
	      ff1->suivant = ff2;
	      ff1 = ff2;
	    }
	  *nbfac += 1;
	  
	}
      
      fp1 = fp1->suivant;
      
    }while (fp1 != NULL);

   
}

/*|======================================================================|
  | SYRTHES 2.1                JANV 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | tria_in_rectan                                                       |
  |         Tri des triangles pour les placer dans le quadtree           |
  |======================================================================| */
int tria_in_rectan(double xa,double ya,double xb,double yb,
		   double xc,double yc,
		   double xcc,double ycc,double dx,double dy)
{
  double xmin,xmax,ymin,ymax;
  double d2,xab,yab,xac,yac,xbc,ybc,xp,yp,t;
  double epsi,ta,tb,tc;
  
  t=0.;
  epsi=1.E-5;
  d2=dx+dy; 

  xmin = xcc-dx-epsi; xmax = xcc+dx+epsi;
  ymin = ycc-dy-epsi; ymax = ycc+dy+epsi;
  
  /* il existe  1 point dans le rectangle => intersection*/
  if (in_rectan (xa,ya,xmin,xmax,ymin,ymax))
    return(1);
  else if (in_rectan(xb,yb,xmin,xmax,ymin,ymax))
    return(1);
  else if (in_rectan(xc,yc,xmin,xmax,ymin,ymax))
    return(1);
  
  
  /* tous les points sont du meme cote d'une droite ==> pas intersection */
  else if (xa>xmax && xb>xmax && xc>xmax)
    return(0);
  else if (xa<xmin && xb<xmin && xc<xmin)
    return(0);
  else if (ya>ymax && yb>ymax && yc>ymax)
    return(0);
  else if (ya<ymin && yb<ymin && yc<ymin)
    return(0);
  
  else
    {
      /* translation a l'origine */
      xa -= xcc; ya -= ycc;
      xb -= xcc; yb -= ycc;
      xc -= xcc; yc -= ycc;
      
      /* exterieur des droites qui passent par les coins ==> pas intersection */
      if (xa+ya<-d2 && xb+yb<-d2 && xc+yc<-d2)
	return(0);
      else if (xa+ya>d2  && xb+yb>d2  && xc+yc>d2)
	return(0);
      else if (xa-ya>d2  && xb-yb>d2  && xc-yc>d2)
	return(0);
      else if (ya-ya<-d2 && yb-yb<-d2 && yc-yc<-d2)
	return(0);
      
      
      /* intersections segments rectangle */
      else if (seg_rectanx( dx, dy,xa,xb,ya,yb))
	return(1);
      else if (seg_rectanx(-dx, dy,xa,xb,ya,yb))
	return(1);
      else if (seg_rectany( dx, dy,xa,xb,ya,yb))
	return(1);
      else if (seg_rectany( dx,-dy,xa,xb,ya,yb))
	return(1);
      
      else if (seg_rectanx( dx, dy,xa,xc,ya,yc))
	return(1);
      else if (seg_rectanx(-dx, dy,xa,xc,ya,yc))
	return(1);
      else if (seg_rectany( dx, dy,xa,xc,ya,yc))
	return(1);
      else if (seg_rectany( dx,-dy,xa,xc,ya,yc))
	return(1);
      
      else if (seg_rectanx( dx, dy,xb,xc,yb,yc))
	return(1);
      else if (seg_rectanx(-dx, dy,xb,xc,yb,yc))
	return(1);
      else if (seg_rectany( dx, dy,xb,xc,yb,yc))
	return(1);
      else if (seg_rectany( dx,-dy,xb,xc,yb,yc))
	return(1);
      
      /* segment enfonce dans un coin */
      else
	{
/* 	  printf(" Error in tria_in_rectan\n"); */
	  return(1);
/* 	  exit(1); */
	}
    }
    
}

    

