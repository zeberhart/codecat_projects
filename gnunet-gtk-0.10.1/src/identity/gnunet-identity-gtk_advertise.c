/*
     This file is part of GNUnet
     (C) 2005-2013 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/identity/gnunet-identity-gtk_advertise.c
 * @author LRN
 * @author Christian Grothoff
 */
#include "gnunet-identity-gtk_advertise.h"
#include "gnunet-identity-gtk.h"
#include <gnunet/gnunet_fs_service.h>


#define NEW_KEYWORD_TEXT "<add>"


/**
 * Types of metadata we offer for namespaces.
 */
static gint types[] =
{
  EXTRACTOR_METATYPE_TITLE,
  EXTRACTOR_METATYPE_KEYWORDS,
  EXTRACTOR_METATYPE_SUBJECT,
  EXTRACTOR_METATYPE_DESCRIPTION,
  EXTRACTOR_METATYPE_COMMENT,
  EXTRACTOR_METATYPE_COPYRIGHT,
  EXTRACTOR_METATYPE_URI,
  EXTRACTOR_METATYPE_CREATOR,
  EXTRACTOR_METATYPE_CREATION_DATE,
  EXTRACTOR_METATYPE_RESERVED
};


/**
 * Columns in the metadata list store.
 */
enum METADATA_ModelColumns
{

  /**
   * A guint
   */
  METADATA_MC_TYPE_AS_ENUM = 0,

  /**
   * A gchararray
   */
  METADATA_MC_TYPE_AS_STRING = 1,

  /**
   * A gchararray
   */
  METADATA_MC_VALUE = 2

};


/**
 * Columns in the keyword list tree store.
 */
enum KEYWORDS_ModelColumns
{

  /**
   * A gchararray
   */
  KEYWORDS_MC_KEYWORD = 0

};


/**
 * Columns in the meta types list tree store.
 */
enum METATYPES_ModelColumns
{

  /**
   * A gchararray
   */
  METATYPES_MC_TYPE_AS_STRING = 0,

  /**
   * A guint
   */
  METATYPES_MC_TYPE_AS_ENUM = 1

};


/**
 * Context for advertisement operations.
 */
struct AdvertiseContext
{
  /**
   * Builder for accessing objects in the dialog.
   */
  GtkBuilder *builder;

  /**
   * Private key of the namespace we will be advertising.
   */
  struct GNUNET_CRYPTO_EcdsaPrivateKey priv;

  /**
   * Main dialog object.
   */
  GtkWidget *dialog;

  /**
   * List of keywords to advertise under.
   */
  GtkListStore *keywords;

  /**
   * Metadata to include in the advertisement.
   */
  GtkListStore *metadata;

  /**
   * Model with the meta types.
   */
  GtkListStore *meta_types;

  /**
   * Number of keywords in the view.
   */
  unsigned int num_keywords;

};


/**
 * Context for the publishing operation.
 */
struct PublishContext
{

  /**
   * Kept in a DLL.
   */
  struct PublishContext *next;

  /**
   * Kept in a DLL.
   */
  struct PublishContext *prev;

  /**
   * Handle to FS subsystem.
   */
  struct GNUNET_FS_Handle *fs;

  /**
   * Handle to the publish operation.
   */
  struct GNUNET_FS_PublishKskContext *pub;

};


/**
 * Kept in a DLL.
 */
static struct PublishContext *pc_head;

/**
 * Kept in a DLL.
 */
static struct PublishContext *pc_tail;


/**
 * Shutdown advertisement subsystem, this process is terminating.
 */
void
GIG_advertise_shutdown_ ()
{
  struct PublishContext *pc;

  while (NULL != (pc = pc_head))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
		_("Aborting advertising operation due to shutdown.\n"));
    GNUNET_CONTAINER_DLL_remove (pc_head,
				 pc_tail,
				 pc);
    GNUNET_FS_publish_ksk_cancel (pc->pub);
    GNUNET_FS_stop (pc->fs);
    GNUNET_free (pc);
  }
}


/**
 * Function called once we published the advertisement.
 *
 * @param cls closure with the `struct PublishContext`
 * @param uri URI under which the block is now available, NULL on error
 * @param emsg error message, NULL on success
 */
static void
publish_continuation (void *cls,
		      const struct GNUNET_FS_Uri *uri,
		      const char *emsg)
{
  struct PublishContext *pc = cls;

  pc->pub = NULL;
  if (NULL == uri)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
		_("Failed to advertise ego: %s\n"),
		emsg);
    /* FIXME: might want to output to GUI... */
  }
  GNUNET_FS_stop (pc->fs);
  GNUNET_CONTAINER_DLL_remove (pc_head,
			       pc_tail,
			       pc);
  GNUNET_free (pc);
}


/**
 * Notification of FS to a client about the progress of an
 * operation.  Callbacks of this type will be used for uploads,
 * downloads and searches.  Some of the arguments depend a bit
 * in their meaning on the context in which the callback is used.
 *
 * @param cls closure
 * @param info details about the event, specifying the event type
 *        and various bits about the event
 * @return client-context (for the next progress call
 *         for this operation; should be set to NULL for
 *         SUSPEND and STOPPED events).  The value returned
 *         will be passed to future callbacks in the respective
 *         field in the `struct GNUNET_FS_ProgressInfo`.
 */
static void *
progress_cb (void *cls,
	     const struct GNUNET_FS_ProgressInfo *info)
{
  return NULL;
}


/**
 * The user terminated the dialog.  Perform the appropriate action.
 *
 * @param dialog the advertisement dialog
 * @param response_id action selected by the user
 * @param user_data our 'struct AdvertiseContext' (to be cleaned up)
 */
void
GNUNET_GTK_identity_advertise_dialog_response_cb (GtkDialog *dialog,
						  gint response_id,
						  gpointer user_data)
{
  struct AdvertiseContext *ac = user_data;
  struct GNUNET_FS_Uri *ksk_uri;
  struct GNUNET_FS_Uri *uri;
  gchar *keyword;
  struct GNUNET_CONTAINER_MetaData *meta;
  guint ntype;
  gchar *value;
  GtkTreeIter iter;
  struct GNUNET_FS_BlockOptions bo;
  struct GNUNET_CRYPTO_EcdsaPublicKey pk;
  struct PublishContext *pc;
  guint anonymity;
  const char *id;

  if (GTK_RESPONSE_OK != response_id)
    goto cleanup;
  ksk_uri = NULL;
  if (! gtk_tree_model_get_iter_first (GTK_TREE_MODEL (ac->keywords), &iter))
  {
    GNUNET_break (0);
    goto cleanup;
  }
  do
  {
    gtk_tree_model_get (GTK_TREE_MODEL (ac->keywords), &iter,
			KEYWORDS_MC_KEYWORD, &keyword,
			-1);
    if (NULL == ksk_uri)
      ksk_uri = GNUNET_FS_uri_ksk_create_from_args (1, (const char **) &keyword);
    else
      GNUNET_FS_uri_ksk_add_keyword (ksk_uri, keyword, GNUNET_NO);
    g_free (keyword);
  }
  while (gtk_tree_model_iter_next (GTK_TREE_MODEL (ac->keywords), &iter));

  meta = GNUNET_CONTAINER_meta_data_create ();
  if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (ac->metadata),
				     &iter))
  {
    do
    {
      gtk_tree_model_get (GTK_TREE_MODEL (ac->metadata), &iter,
                          METADATA_MC_TYPE_AS_ENUM, &ntype,
                          METADATA_MC_VALUE, &value,
                          -1);
      if (ntype > 0)
      {
        GNUNET_CONTAINER_meta_data_insert (meta, "<user>",
					   ntype,
					   EXTRACTOR_METAFORMAT_UTF8,
                                           "text/plain", value,
                                           strlen (value) + 1);
      }
      g_free (value);
    }
    while (gtk_tree_model_iter_next (GTK_TREE_MODEL (ac->metadata), &iter));
  }

  bo.expiration_time = GNUNET_GTK_get_expiration_time
    (GTK_SPIN_BUTTON
     (gtk_builder_get_object
      (ac->builder,
       "GNUNET_GTK_identity_advertise_expiration_year_spin_button")));
  anonymity = 1;
  GNUNET_break (GNUNET_GTK_get_selected_anonymity_level (ac->builder,
							 "GNUNET_GTK_identity_advertise_anonymity_combobox",
							 &anonymity));
  bo.anonymity_level = anonymity;
  bo.content_priority
    = (unsigned int) gtk_spin_button_get_value
    (GTK_SPIN_BUTTON
     (gtk_builder_get_object
      (ac->builder,
       "GNUNET_GTK_identity_advertise_priority_spin_button")));
  bo.replication_level
    = (unsigned int) gtk_spin_button_get_value
    (GTK_SPIN_BUTTON
     (gtk_builder_get_object
      (ac->builder,
       "GNUNET_GTK_identity_advertise_replication_spin_button")));
  GNUNET_CRYPTO_ecdsa_key_get_public (&ac->priv,
				    &pk);
  id = gtk_entry_get_text
    (GTK_ENTRY
     (gtk_builder_get_object
      (ac->builder,
       "GNUNET_GTK_identity_advertise_root_entry")));
  uri = GNUNET_FS_uri_sks_create (&pk, id);

  pc = GNUNET_new (struct PublishContext);
  pc->fs = GNUNET_FS_start (GIG_get_configuration (),
			    "gnunet-identity-gtk",
			    &progress_cb, pc,
			    GNUNET_FS_FLAGS_NONE,
			    GNUNET_FS_OPTIONS_END);
  pc->pub = GNUNET_FS_publish_ksk (pc->fs,
				   ksk_uri,
				   meta,
				   uri,
				   &bo,
				   GNUNET_FS_PUBLISH_OPTION_NONE,
				   &publish_continuation, pc);
  GNUNET_CONTAINER_DLL_insert (pc_head,
			       pc_tail,
			       pc);
  GNUNET_FS_uri_destroy (uri);
  GNUNET_CONTAINER_meta_data_destroy (meta);
 cleanup:
  gtk_widget_destroy (ac->dialog);
  g_object_unref (G_OBJECT (ac->builder));
  GNUNET_free (ac);
}


/**
 * User edited keywords in the keyword tree view.  Update
 * the model accordingly.
 *
 * @param renderer the object that created the signal
 * @param path the path identifying the edited cell
 * @param new_text the new text
 * @param user_data the 'struct AdvertiseContext'
 */
void
GNUNET_GTK_identity_advertise_keywords_text_edited_cb (GtkCellRendererText *renderer,
						       gchar               *path,
						       gchar               *new_text,
						       gpointer             user_data)
{
  struct AdvertiseContext *ac = user_data;
  GtkTreePath *tree_path;
  GtkTreeIter iter;
  char *old_text;

  tree_path = gtk_tree_path_new_from_string (path);
  if (NULL == tree_path)
  {
    GNUNET_break (0);
    return;
  }
  if (! gtk_tree_model_get_iter (GTK_TREE_MODEL (ac->keywords),
				 &iter, tree_path))
  {
    GNUNET_break (0);
    gtk_tree_path_free (tree_path);
    return;
  }
  gtk_tree_path_free (tree_path);
  gtk_tree_model_get (GTK_TREE_MODEL (ac->keywords), &iter,
		      KEYWORDS_MC_KEYWORD, &old_text,
		      -1);
  if (0 == strcmp (NEW_KEYWORD_TEXT, old_text))
  {
    if ( (NULL != new_text) &&
	 (0 != strlen (new_text)) )
    {
      gtk_list_store_insert_with_values (ac->keywords, &iter, -1,
					 KEYWORDS_MC_KEYWORD, new_text,
					 -1);
      ac->num_keywords++;
    }
  }
  else
  {
    if ( (NULL != new_text) &&
	 (0 != strlen (new_text)) )
    {
      gtk_list_store_set (ac->keywords, &iter,
			  KEYWORDS_MC_KEYWORD, new_text,
			  -1);
    }
    else
    {
      gtk_list_store_remove (ac->keywords, &iter);
      ac->num_keywords--;
    }
  }
  g_free (old_text);
  gtk_widget_set_sensitive (GTK_WIDGET (gtk_builder_get_object
					(ac->builder,
					 "GNUNET_GTK_identity_advertise_ok_button")),
			    0 != ac->num_keywords);
}


/**
 * User pushed a key in the metadata tree view.  Check if it was
 * "DEL" and if so, remove the selected values.
 *
 * @param widget widget creating the signal
 * @param event the event to process
 * @param user_data the 'struct AdvertiseContext'
 * @return TRUE if we handled the event, FALSE if not
 */
gboolean
GNUNET_GTK_identity_advertise_metadata_treeview_key_press_event_cb (GtkWidget *widget,
								    GdkEventKey *event,
								    gpointer user_data)
{
  struct AdvertiseContext *ac = user_data;
  GtkTreeSelection *sel;
  GtkTreeModel *model;
  GtkTreeIter iter;

  if (event->keyval != GDK_KEY_Delete)
    return FALSE;
  sel = gtk_tree_view_get_selection (GTK_TREE_VIEW
				     (gtk_builder_get_object
				      (ac->builder,
				       "GNUNET_GTK_identity_advertise_metadata_treeview")));
  if (! gtk_tree_selection_get_selected (sel,
					 &model,
					 &iter))
    {
      gdk_beep ();
      return TRUE;
    }
  gtk_list_store_remove (ac->metadata,
			 &iter);
  return TRUE;
}


/**
 * User edited metadata value in the tree view. Update the model.
 *
 * @param renderer the object that created the signal
 * @param path the path identifying the edited cell
 * @param new_text the new text, if empty, remove the line
 * @param user_data the 'struct AdvertiseContext'
 */
void
GNUNET_GTK_identity_advertise_metadata_value_text_edited_cb (GtkCellRendererText *renderer,
							     gchar *path,
							     gchar *new_text,
							     gpointer user_data)
{
  struct AdvertiseContext *ac = user_data;
  GtkTreePath *tree_path;
  GtkTreeIter iter;

  tree_path = gtk_tree_path_new_from_string (path);
  if (NULL == tree_path)
  {
    GNUNET_break (0);
    return;
  }
  if (! gtk_tree_model_get_iter (GTK_TREE_MODEL (ac->metadata),
				 &iter, tree_path))
  {
    GNUNET_break (0);
    gtk_tree_path_free (tree_path);
    return;
  }
  gtk_tree_path_free (tree_path);
  if ( (NULL != new_text) &&
       (0 != strlen (new_text)) )
    gtk_list_store_set (ac->metadata, &iter,
			METADATA_MC_VALUE, new_text,
			-1);
  else
    gtk_list_store_remove (ac->metadata, &iter);
}


/**
 * User edited the 'value' field for inserting meta data.   Update
 * sensitivity of the 'add' button accordingly.
 *
 * @param editable the widget that was edited
 * @param user_data the 'struct AdvertiseContext'
 */
void
GNUNET_GTK_identity_advertise_metadata_value_entry_changed_cb (GtkEditable *editable,
							       gpointer user_data)
{
  struct AdvertiseContext *ac = user_data;
  const char *value;
  GtkEntry *value_entry;
  GtkWidget * add_button;

  value_entry = GTK_ENTRY (gtk_builder_get_object (ac->builder,
						   "GNUNET_GTK_identity_advertise_metadata_value_entry"));
  value = gtk_entry_get_text (value_entry);
  add_button = GTK_WIDGET (gtk_builder_get_object (ac->builder,
						   "GNUNET_GTK_identity_advertise_metadata_add_button"));
  if ( (NULL == value) ||
       (0 == strlen (value)) )
  {
    gtk_widget_set_sensitive (add_button,
			      FALSE);
    return;
  }
  gtk_widget_set_sensitive (add_button,
			    TRUE);
}


/**
 * User clicked the 'add' button, get the type and value and update
 * the metadata model.
 *
 * @param button the 'add' button
 * @param user_data the 'struct AdvertiseContext'
 */
void
GNUNET_GTK_identity_advertise_metadata_add_button_clicked_cb (GtkButton * button,
							      gpointer user_data)
{
  struct AdvertiseContext *ac = user_data;
  GtkTreeIter iter;
  guint type;
  const char *type_as_string;
  const char *value;
  GtkEntry *value_entry;
  GtkComboBox *type_box;

  type_box = GTK_COMBO_BOX (gtk_builder_get_object (ac->builder,
						    "GNUNET_GTK_identity_advertise_metadata_type_combobox"));
  if (! gtk_combo_box_get_active_iter (type_box,
				       &iter))
  {
    GNUNET_break (0);
    return;
  }
  gtk_tree_model_get (GTK_TREE_MODEL (ac->meta_types),
		      &iter,
		      METATYPES_MC_TYPE_AS_ENUM, &type,
		      -1);
  type_as_string = EXTRACTOR_metatype_to_string (type);
  value_entry = GTK_ENTRY (gtk_builder_get_object (ac->builder,
						   "GNUNET_GTK_identity_advertise_metadata_value_entry"));
  value = gtk_entry_get_text (value_entry);
  if ( (NULL == value) ||
       (0 == strlen (value)) )
  {
    GNUNET_break (0);
    return;
  }
  gtk_list_store_insert_with_values (ac->metadata,
                                     &iter, 0,
                                     METADATA_MC_TYPE_AS_ENUM, type,
                                     METADATA_MC_TYPE_AS_STRING, type_as_string,
                                     METADATA_MC_VALUE, value,
                                     -1);
  gtk_entry_set_text (value_entry, "");
}


/**
 * Run the dialog for advertising a namespace.
 *
 * @param priv private key of the namespace to advertise
 */
void
GIG_advertise_dialog_start_ (const struct GNUNET_CRYPTO_EcdsaPrivateKey *priv)
{
  struct AdvertiseContext *ac;
  gint i;
  GtkTreeIter iter;

  ac = GNUNET_new (struct AdvertiseContext);
  ac->priv = *priv;
  ac->builder =
    GNUNET_GTK_get_new_builder ("gnunet_identity_gtk_advertise_dialog.glade",
				ac);
  if (NULL == ac->builder)
  {
    GNUNET_break (0);
    GNUNET_free (ac);
    return;
  }
  GNUNET_GTK_setup_expiration_year_adjustment (ac->builder);
  ac->dialog = GTK_WIDGET (gtk_builder_get_object
			   (ac->builder, "GNUNET_GTK_identity_advertise_dialog"));
  ac->keywords = GTK_LIST_STORE (gtk_builder_get_object
				(ac->builder, "GNUNET_GTK_identity_advertise_keywords_liststore"));
  ac->metadata = GTK_LIST_STORE (gtk_builder_get_object
			     (ac->builder, "GNUNET_GTK_identity_advertise_metadata_liststore"));
  ac->meta_types = GTK_LIST_STORE (gtk_builder_get_object
				   (ac->builder, "GNUNET_GTK_identity_metadata_types_liststore"));
  for (i = 0; EXTRACTOR_METATYPE_RESERVED != types[i]; i++)
  {
    gtk_list_store_insert_with_values (ac->meta_types,
				       &iter, G_MAXINT,
				       METATYPES_MC_TYPE_AS_STRING,
				       EXTRACTOR_metatype_to_string (types[i]),
				       METATYPES_MC_TYPE_AS_ENUM,
				       types[i],
				       -1);
    if (0 == i)
      gtk_combo_box_set_active_iter (GTK_COMBO_BOX
				     (gtk_builder_get_object
				      (ac->builder,
				       "GNUNET_GTK_identity_advertise_metadata_type_combobox")),
				     &iter);
  }
  gtk_list_store_insert_with_values (ac->keywords,
				     &iter, G_MAXINT,
				     KEYWORDS_MC_KEYWORD, NEW_KEYWORD_TEXT,
				     -1);
  gtk_window_present (GTK_WINDOW (ac->dialog));
}


/* end of gnunet-identity-gtk_advertise.c */
