fuse-emulator-utils (1.3.1-1) unstable; urgency=medium

  * New upstream release.

 -- Alberto Garcia <berto@igalia.com>  Tue, 08 Nov 2016 14:10:44 +0200

fuse-emulator-utils (1.3.0-1) unstable; urgency=medium

  * New upstream release.

 -- Alberto Garcia <berto@igalia.com>  Mon, 03 Oct 2016 10:00:10 +0300

fuse-emulator-utils (1.2.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Replace dependency on libgcrypt11-dev with libgcrypt20-dev.

 -- Alberto Garcia <berto@igalia.com>  Tue, 23 Aug 2016 09:19:25 -0400

fuse-emulator-utils (1.2.0-1) unstable; urgency=medium

  * New upstream release.
    - Drop all patches.
  * debian/control:
    - Build depend on libspectrum 1.2.0.
    - Replace build dependencies on libavformat-dev, libavresample-dev and
      libswscale-dev with zlib1g-dev, libjpeg-dev and libpng-dev.
    - Update Standards-Version to 3.9.8 (no changes).
    - Update the package description with the new tape2pulses tool.
  * debian/copyright:
    - Update copyright years.
  * Verify GPG signature of the upstream tarball:
    - debian/upstream/signing-key.asc: Add key file.
    - debian/watch: Add pgpsigurlmangle option.

 -- Alberto Garcia <berto@igalia.com>  Tue, 07 Jun 2016 16:56:39 +0300

fuse-emulator-utils (1.1.1-5) unstable; urgency=medium

  [ Andreas Cadhalpun ]
  * ffmpeg_2.9.patch:
    - Add support for FFmpeg 2.9 (Closes: #803815).

 -- Alberto Garcia <berto@igalia.com>  Wed, 13 Jan 2016 15:17:47 +0200

fuse-emulator-utils (1.1.1-4) unstable; urgency=medium

  * debian/control: update Standards-Version to 3.9.6 (no changes).
  * fix-progname.patch:
    - Fix FTBFS with GCC 5 (Closes: #799125).

 -- Alberto Garcia <berto@igalia.com>  Wed, 16 Sep 2015 10:34:17 +0300

fuse-emulator-utils (1.1.1-3) unstable; urgency=medium

  * Release to unstable (Closes: #739243):
  * fix-wav-sample-rate.patch: fix WAV sample rate (Closes: #744397).
  * debian/rules: enable parallel builds.
  * debian/copyright: update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Mon, 12 May 2014 10:54:51 +0300

fuse-emulator-utils (1.1.1-2) experimental; urgency=medium

  * debian/control: update Standards-Version to 3.9.5 (no changes).
  * Build with libav10 (Closes: #739243):
    - libav10.patch: Fix FTBFS with libav10.
    - debian/control: add build dependency on libavresample-dev and set
      minimum version to (>= 6:10~).

 -- Alberto Garcia <berto@igalia.com>  Sat, 15 Mar 2014 13:58:29 +0200

fuse-emulator-utils (1.1.1-1) unstable; urgency=low

  * New upstream release.
  * Drop all patches, they are already included in this release.
  * Update my e-mail address in debian/control and debian/copyright.
  * Build with hardening options.
    - debian/compat: bump debhelper compatibility level to 9.
    - debian/control: update build dependency on debhelper as well.
  * debian/control:
    - Add build dependencies on libswscale-dev and libavformat-dev.
    - Update build dependency on libspectrum to >= 1.1.0.
    - Remove obsolete DM-Upload-Allowed flag.
    - Update Standards-Version to 3.9.4 (no changes).
    - Mention the new fmfconv tool in the package description.
  * debian/copyright: update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Mon, 03 Jun 2013 01:42:44 +0200

fuse-emulator-utils (1.0.0-4) unstable; urgency=low

  * Add tags to all patches and remove their numbering.
  * Add support for libaudiofile 0.3:
    - audiofile-0.3.patch: detect libaudiofile using pkg-config, recent
      versions no longer ship audiofile-config.
    - debian/control: add build dependency on dh-autoreconf.
    - debian/rules: run dh --with autoreconf.
  * link-rzxdump-with-gcrypt.patch: update to work with dh-autoreconf.
  * debian/control: update Standards-Version to 3.9.3.
  * debian/copyright: update debian copyright format URL.

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 20 Mar 2012 12:51:39 +0200

fuse-emulator-utils (1.0.0-3) unstable; urgency=low

  * debian/control: update Standards-Version to 3.9.2.
  * debian/copyright: rewrite using the machine-readable format.
  * debian/fuse-emulator-utils.lintian-overrides: the package description
    is really meant to start with 'The'.

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 27 Dec 2011 19:03:37 +0100

fuse-emulator-utils (1.0.0-2) unstable; urgency=low

  * debian/control: add DM-Upload-Allowed flag.
  * 02_link-rzxdump-with-gcrypt.patch: Fix FTBFS (Closes: #614231).

 -- Alberto Garcia <agarcia@igalia.com>  Sun, 20 Feb 2011 18:48:04 +0200

fuse-emulator-utils (1.0.0-1) unstable; urgency=low

  * Initial release (Closes: #608202).

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 28 Dec 2010 18:09:27 +0100
