#-------------------------------------------------------------------------------
#  cb2bib cmake build macros
#-------------------------------------------------------------------------------

macro(PATH_OPTION VAR DESCRIPTION DEFAULT)
  if(DEFINED ${VAR})
    set(${VAR} ${${VAR}} CACHE PATH "${DESCRIPTION}" FORCE)
  else()
    set(${VAR} ${DEFAULT} CACHE PATH "${DESCRIPTION}" FORCE)
  endif()
endmacro(PATH_OPTION)

macro(LIST_SOURCE_FILES_AT CPP H MOCH RPATH)
  file(GLOB ${CPP} RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "${RPATH}*.cpp")
  file(GLOB ${H} RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "${RPATH}*.h")
  set(${MOCH})
  foreach(_file ${${H}})
    file(READ ${_file} _contents)
    string(REGEX MATCH "[\n\r] *Q_OBJECT" _match "${_contents}")
    if(_match)
      list(APPEND ${MOCH} ${_file})
    endif(_match)
  endforeach(_file ${${H}})
endmacro(LIST_SOURCE_FILES_AT)

macro(LIST_SOURCE_FILES CPP H MOCH)
  list_source_files_at(${CPP} ${H} ${MOCH} "")
endmacro(LIST_SOURCE_FILES)

#-------------------------------------------------------------------------------