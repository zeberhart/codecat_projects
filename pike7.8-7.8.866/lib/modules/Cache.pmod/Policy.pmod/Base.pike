/*
 * Sample class for the Cache.Storage stuff.
 * by Francesco Chemolli <kinkie@roxen.com>
 *
 * All Storage-related class must MUST implement this method.
 *
 * $Id: b3e842ce7d6f2787bc589a403a74c811b4ac74c4 $
 */

#pike __REAL_VERSION__

//! Base class for cache expiration policies.

//! Expire callback.
//!
//!   This function is called to expire parts
//!   of @[storage].
//!
//! @note
//!   All Storage.Policy classes must MUST implement this method.
void expire(Cache.Storage.Base storage) {
  throw("Override this!");
}
