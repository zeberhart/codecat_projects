/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 1988-2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/
/* Conversions de maillages en format Syrthes */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "convert2syrthes.h"


/* extern char *basename (__const char *__filename) __THROW __nonnull ((1)); */
extern char *basename (__const char *__filename);




/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion des formats de maillages vers le format SYRTHES 4        |
  |======================================================================| */
int main(int iargc, char **iargv) {

  struct typ_maillage maillage;     /* structure de maillage */
  struct Maillage maillnodes;
  struct MaillageBord maillnodebord;

  int extension; /* extension : 1 neu, 2 msh, 3 med */
  int m; /* position de l'option -m */
  int v; /* position de l'option -v */
  int o; /* position de l'option -o */
  int dim; /* position de l'option -dim dimension si GMSH */
  int mode; /* position de l'option -b */
  int longueur; /* longueur de chaine de caracteres */
  char  nom_syr[400]; /* base du nom de fichier d'entree */
  char  buffer[400];

  printf("\n***************************************************************\n");
  printf("  convert2syrthes : convert mesh file to SYRTHES format     \n");
  printf("**************************************************************\n\n");

  if (iargc < 3 || iargc > 9) {

    printf("  convert2syrthes [-dim dimension]  -m mesh1.ext [-o mesh2.syr]  \n");
    printf("                  with  .ext = \n");
    printf("                               .neu (GAMBIT format)\n");
    printf("                               .msh (GMSH format)\n");
    printf("                               .med (MED format)\n");
    printf("                               .des (SIMAIL format)\n");
    printf("                               .unv (IDEAS universal file format)\n");
    printf("     --> changes initial mesh file format to SYRTHES 4 mesh format\n");
    printf("\n  Additional options :\n");
    printf("     -o mesh2.syr : to change the name of the file\n");
    printf("     -dim 2 or -dim 3 : needed for GMSH file format, ignored in the other cases\n");
    printf("\n  Note :\n");
    printf("     For med format, convert2syrthes creates an additionnal ASCII file (..._desc)\n");
    printf("     where you will find the correspondances between group names (used in Salome)\n");
    printf("     and familly (or references) numbers used in SYRTHES\n");
    printf("\n  Examples :\n");
    printf("     convert2syrthes -m square.med                         --> square.syr + square.syr_desc\n");
    printf("     convert2syrthes -m square.neu                         --> square.syr\n");
    printf("     convert2syrthes -m square.des                         --> square.syr\n");
    printf("     convert2syrthes -m square.unv                         --> square.syr\n");
    printf("     convert2syrthes -m square.msh -dim 2                  --> square.syr\n");
    printf("     convert2syrthes -m square.med -o square2.syr          --> square2.syr + square2.syr_desc\n");
    
    return 1;

  }
 
  /* initialisations des entiers */
  extension = 0;
  maillage.dimension = 0; /* dimension de l'espace  2 ou 3 */
  maillage.dim_elem = 0; /* dimension des elements 1, 2 ou 3 */
  maillage.nbnoeuds = 0; /* nombre de noeuds */
  maillage.nbelem = 0; /* nombre d'elements */
  maillage.nbnoeuds_par_elem = 0; /* nombre de noeuds par element */
  maillage.nbelem_de_bord = 0; /* nombre d'elements de bord pour version Syrthes 4.0 */
  maillage.numligne = 0; /* numero de ligne lue dans le maillage de lecture */
  maillage.nbzones = 0; /* nombre de zones */
  maillage.nbcl = 0; /* nombre de conditions limites */
  maillage.fichier_ext = NULL;
  maillage.fichier_syr = NULL;
  maillage.fichier_desc = NULL;
  maillage.mode_ecriture = 0; /*texte par defaut*/

  /* gestion de l'extension */
  m=1;
  while (strcmp(iargv[m],"-m")!=0 && m<iargc-1) m++;
  if (m==iargc-1) {
    printf("ERROR : wrong list of parameters\n");
    printf("  convert2syrthes  -m mesh1.ext [-o mesh2.syr] [-dim dimension] \n\n");
    return 1;
  }
  extension = 0;
  longueur = strlen(iargv[m+1]);
  if (longueur > 3) {
    if ( !strncmp(iargv[m+1]+longueur-4,".neu",4))     extension = 1; /* Gambit neutral */
    else if ( !strncmp(iargv[m+1]+longueur-4,".msh",4)) extension = 2; /* GMSH */
    else if ( !strncmp(iargv[m+1]+longueur-4,".med",4)) extension = 3; /* MED */
    else if ( !strncmp(iargv[m+1]+longueur-4,".des",4)) extension = 4; /* SIMAIL */
    else if ( !strncmp(iargv[m+1]+longueur-4,".unv",4)) extension = 5; /* IDEAS */
  }

#ifndef MED
  if (extension==3){
    printf("ERROR : type of input file = MED but convert2syrthes installed without MED\n");
    printf("--> re-run your installation !\n");
    exit(1);
  }
#endif

  /* version Syrthes a ecrire */
  maillage.version_syr = 4; /* 4.0 par defaut */

 
  /* si GMSH ou IDEAS lecture de la dimension "-dim 2 ou -dim 3" */
  if (extension == 2 || extension == 5) {
    dim=1;
    while (strcmp(iargv[dim],"-dim")!=0 && dim<iargc-1) dim++;
    if (dim<iargc-1) {
      if (iargv[dim+1][0] == '3') maillage.dimension = 3;
      else if (iargv[dim+1][0] == '2') maillage.dimension = 2;
      else {
        printf("ERROR : wrong value for the space dimension : %c\n\n",iargv[dim+1][0]);
        return 1;
      }	
    }
    else {
      printf("ERROR : to convert GMSH or IDEAS file format, you have to give the space dimension in arguments (-dim 2 ou -dim 3) \n");
      printf("  convert2syrthes -dim dimension  -m mesh1.ext [-o mesh2.syr]\n\n");
    return 1;
    }
  }

  /* gestion de l'option -b */
  mode=1;
  while (mode < iargc && strcmp(iargv[mode],"-b") != 0) {
    mode++;
  }

  if (mode < iargc) {
    printf("Binary writing mode enabled.\n");
    maillage.mode_ecriture = 1; /* ecriture binaire */
  }

  if (extension !=0) {
    /* gestion de l'option -o */
    o=1;
    while (strcmp(iargv[o],"-o")!=0 && o<iargc-1) o++;
    if (o<iargc-1) {
      longueur = strlen(iargv[o+1]);
      strncpy(nom_syr,iargv[o+1],longueur); nom_syr[longueur]='\0';
    }
    else {
      /* nom de fichier de sortie base sur le nom du fichier d'entree */
      longueur = strlen(iargv[m+1]);
      strncpy(nom_syr,iargv[m+1],longueur-4); nom_syr[longueur-4]='\0';
      strcat(nom_syr,".syr");
    }
    /* ouverture du fichier Syrthes en ecriture */
    if (ouvrir_syr(&maillage,nom_syr) != 0) return 1;
  }


  /* conversion selon extension */
  switch (extension) {
  case 1 : if (conversion_neu(&maillage,iargv[m+1]) != 0) return 1; break;
  case 2 : if (conversion_msh(&maillage,iargv[m+1]) != 0) return 1; break;
#ifdef MED
  case 3 : if (conversion_med(&maillage,iargv[m+1], nom_syr) != 0) return 1; break;
#endif
  case 4 : if (conversion_sim(&maillage,&maillnodes,&maillnodebord,iargv[m+1], nom_syr) != 0) return 1; break;
  case 5 : if (conversion_ide(&maillage,iargv[m+1], nom_syr) != 0) return 1; break;
  default : printf("ERROR : wrong type of mesh format\n");
    printf("        check the file name extension :\n");
    printf("             .neu : GAMBIT \n");
    printf("             .msh : GMSH\n");
    printf("             .med : Salome\n") ; 
    printf("             .des : SIMAIL\n") ; 
    printf("             .unv : IDEAS\n\n") ; 
	    return 1;

  }
  
  /* fermeture du fichier Syrthes */
  if (fermer_syr(&maillage) != 0) return 1;

  /* reouverture du fichier Syrthes pour au moins l'entete*/
  if (reouvrir_syr(&maillage, nom_syr) != 0) return 1;
  
  /* reecriture de l'entete avec les bonnes informations cette fois-ci */
  printf("Modification of the head file...\n");
  if (ecrire_entete_syr(&maillage) != 0) return 1;
   
  /* reecriture des noeuds avec la couleur */
  if (extension==1) {
    printf("Modification of the SYRTHES for the nodes references...\n");
    if (ouvrir_neu(&maillage,iargv[m+1]) != 0) return 1;
    if (modif_noeuds_neusyr(&maillage) != 0) return 1;
    if (fermer_neu(&maillage) != 0) return 1;
  }
  else if (extension==2) {
    printf("Modification of the SYRTHES for the nodes references...\n");
    if (ouvrir_msh(&maillage,iargv[m+1]) != 0) return 1;
    if (modif_noeuds_mshsyr(&maillage) != 0) return 1;
    if (fermer_msh(&maillage) != 0) return 1;
  }
  
  /* fermeture du fichier Syrthes */
  if (fermer_syr(&maillage) != 0) return 1;
  
  printf("\nSYRTHES file conversion OK.\n\n");
  return 0;
  /* sortie normale */

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format SIMAIL                                         |
  |======================================================================| */
int conversion_sim(struct typ_maillage *maillage, 
		   struct Maillage *maillnodes,struct MaillageBord *maillnodebord,
		   char* nomfich) 
{
  if (ouvrir_sim(maillage, nomfich) != 0) return 1;
  if (lire_sim(maillage,maillnodes,maillnodebord) != 0) return 1;
  if (ecrire_entete_syr(maillage) != 0) return 1;
  if (ecrire_coord_syr(maillage) != 0) return 1;
  if (ecrire_elem_syr(maillage) != 0) return 1;
  if ( ((*maillage).dimension == 3 && (*maillage).dim_elem == 3)
       || ((*maillage).dimension == 2 && (*maillage).dim_elem == 2) ) {
    if(ecrire_elem_de_bord_syr(maillage) != 0) return 1;
  }
  if (fermer_sim(maillage) != 0) return 1;
  return 0;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format IDEAS                                          |
  |======================================================================| */
int conversion_ide(struct typ_maillage *maillage, char* nomfich) 
{
  if (ouvrir_ide(maillage, nomfich) != 0) return 1;
  if (lire_ideas(maillage) != 0) return 1;
  if (ecrire_entete_syr(maillage) != 0) return 1;
  if (ecrire_coord_syr(maillage) != 0) return 1;
  if (ecrire_elem_syr(maillage) != 0) return 1;
  if ( ((*maillage).dimension == 3 && (*maillage).dim_elem == 3)
       || ((*maillage).dimension == 2 && (*maillage).dim_elem == 2) ) {
    if(ecrire_elem_de_bord_syr(maillage) != 0) return 1;
  }
  if (fermer_ide(maillage) != 0) return 1;
  return 0;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format GAMBIT                                         |
  |======================================================================| */
int conversion_neu(struct typ_maillage *maillage, char* nomfich) {
/* lecture format Gambit Neutral, conversion au format Syrthes, ecriture au format Syrthes */
   if (ouvrir_neu(maillage, nomfich) != 0) return 1;
   if (lire_entete_neu(maillage) != 0) return 1;
   if (ecrire_entete_syr(maillage) != 0) return 1;
   if (lire_coord_neu(maillage) != 0) return 1;
   if (lire_noeuds_colores_neu(maillage) != 0) return 1;
   if (ecrire_coord_syr(maillage) != 0) return 1;
   if (lire_elem_neu(maillage) != 0) return 1;
   if (lire_zones_neu(maillage) != 0) return 1;
   if (ecrire_elem_syr(maillage) != 0) return 1;
   if (lire_cl_neu(maillage) != 0) return 1;
   if ((*maillage).version_syr == 4) {  /* version Syrthes 4.x */
     if ( ((*maillage).dimension == 3 && (*maillage).dim_elem == 3)
       || ((*maillage).dimension == 2 && (*maillage).dim_elem == 2) ) {
	if(ecrire_elem_de_bord_syr(maillage) != 0) return 1;
     }
   }
   if (fermer_neu(maillage) != 0) return 1;
   return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format GMSH                                           |
  |======================================================================| */
int conversion_msh(struct typ_maillage *maillage, char* nomfich) {
/* lecture format GMSH, conversion au format Syrthes, ecriture au format Syrthes */
   if (ouvrir_msh(maillage, nomfich) != 0) return 1;
   if (lire_entete_msh(maillage) != 0) return 1;
   if (ecrire_entete_syr(maillage) != 0) return 1;
   if (lire_coord_msh(maillage) != 0) return 1;
   if (lire_noeuds_colores_msh(maillage) != 0) return 1;
   if (ecrire_coord_syr(maillage) != 0) return 1;
   if (lire_elem_msh(maillage) != 0) return 1;
   if (lire_zones_msh(maillage) != 0) return 1;
   if (ecrire_elem_syr(maillage) != 0) return 1;
   if (lire_cl_msh(maillage) != 0) return 1;
   if ( ((*maillage).dimension == 3 && (*maillage).dim_elem == 3)
	|| ((*maillage).dimension == 2 && (*maillage).dim_elem == 2) ) {
     if(ecrire_elem_de_bord_syr(maillage) != 0) return 1;
   }
   if (fermer_msh(maillage) != 0) return 1;
   return 0;
}

#ifdef MED
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format MED                                            |
  |======================================================================| */
int conversion_med(struct typ_maillage *maillage, char* nomfich, char* nomsyr) {
/* lecture format MED, conversion au format Syrthes, ecriture au format Syrthes */
  if (ouvrir_med(maillage, nomfich, nomsyr) != 0) return 1;
  if (lire_entete_med(maillage) != 0) return 1;
  if (ecrire_entete_syr(maillage) != 0) return 1;
  if (lire_coord_med(maillage) != 0) return 1;
  if (lire_noeuds_colores_med(maillage) != 0) return 1;
  if (ecrire_coord_syr(maillage) != 0) return 1;
  if (lire_elem_med(maillage) != 0) return 1;
  if (lire_zones_med(maillage) != 0) return 1;
  if (ecrire_elem_syr(maillage) != 0) return 1;
  if (lire_cl_med(maillage) != 0) return 1;
  if (ecrire_correspondance_syr_med(maillage) !=0 ) return 1;
  if ( ((*maillage).dimension == 3 && (*maillage).dim_elem == 3)
       || ((*maillage).dimension == 2 && (*maillage).dim_elem == 2) ) {
    if(ecrire_elem_de_bord_syr(maillage) != 0) return 1;
  }
  if (fermer_med(maillage) != 0) return 1;
  return 0;
}
#endif
