/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
#include "ppfunc_proto.h"


extern FILE *ff;


/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Interpretation des conditions aux limites                            |
  |======================================================================| */
void ecrire_vide_clim()
{
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_limfso_fct(struct Maillage maillnodes,struct MaillageBord maillnodebord,\n");
  fprintf(ff,"                     struct MaillageCL maillnodeus,\n");
  fprintf(ff,"                     double *var,struct Clim diric,struct Clim flux,\n");
  fprintf(ff,"                     struct Clim echang,struct Clim rayinf,double tempss)\n");


  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}

/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Interpretation des conditions aux limites                            |
  |======================================================================| */
void ecrire_vide_chmt()
{
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_hmt_limfso_fct(struct Maillage maillnodes,struct MaillageCL maillnodeus,\n");
  fprintf(ff,"	                       double *t,double *pv,double *pt,\n");
  fprintf(ff,"	                       struct HmtClimhhh hmtclimhhh,struct Clim rayinf,double tempss)\n");


  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}

/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Interpretation des proprietes physiques                              |
  |======================================================================| */
void ecrire_vide_prophy()
{
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_cphyso_fct(struct Maillage maillnodes,\n");
  fprintf(ff,"                     double *tmps,struct Prophy physol,double tempss)\n");
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}
/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Interpretation des conditions de flux volumiques                     |
  |======================================================================| */
void ecrire_vide_cini()
{
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_cini_fct(struct Maillage maillnodes,double *t)\n");
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}
/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Interpretation des conditions de flux volumiques                     |
  |======================================================================| */
void ecrire_vide_condvol()
{
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_cfluvs_fct(struct Maillage maillnodes,\n");
  fprintf(ff,"                     double *tmps,struct Cvol fluxvol,double tempss)\n");
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}
/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void ecrire_vide_transfo_perio()
{
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_transfo_perio_fct(int ndim,\n");
  fprintf(ff,"                            int nb1,int *iref,\n");
  fprintf(ff,"                            double x,double y,double z,\n");
  fprintf(ff,"                            double *xt, double *yt, double *zt)\n");
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}
/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void ecrire_vide_ray()
{
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_ray_ftc(struct Maillage maillnodray,\n");
  fprintf(ff,"                  double *tmpray,struct ProphyRay phyray,\n");
  fprintf(ff,"                  struct PropInfini *propinf,struct Clim fimpray,struct Vitre vitre,\n");
  fprintf(ff,"                  double tempss)\n");
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}
/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void ecrire_vide_solaire()
{
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_solaire_fct(double *fdirect,double *fdiffus, int nbande,double tempss)\n");
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}
/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void ecrire_vide_propincidence()
{
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_propincidence_fct(int n,int nbande,double teta,\n");
  fprintf(ff,"                            double **e,double **r,double **a,double **t,\n");
  fprintf(ff,"                            double **emissi,double **reflec,\n");
  fprintf(ff,"                            double **absorb,double **transm)\n");
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}



/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void ecrire_vide_rescon()
{
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_rescon_fct(struct Maillage maillnodes,struct MaillageCL maillnodeus,\n");
  fprintf(ff,"	  	           double *t,double *tcor,struct Contact rescon,double tempss,\n");
  fprintf(ff,"		           struct SDparall sdparall)\n");
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}

/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void ecrire_vide_rescon_hmt()
{
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_hmt_rescon_fct(struct Maillage maillnodes,struct MaillageCL maillnodeus,\n");
  fprintf(ff,"		               double *t,double *pv,double *pt,\n");
  fprintf(ff,"    	               double *tcor,double *pvcor,double *ptcor,\n");
  fprintf(ff,"		               struct Contact rescon,double tempss,\n");
  fprintf(ff,"		               struct SDparall sdparall)\n");
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}
/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void ecrire_vide_condvol_hmt()
{
  fprintf(ff,"/************************************************************************/ \n");
  fprintf(ff,"void user_hmt_cfluvs_fct(struct Maillage maillnodes,\n");
  fprintf(ff,"			 double *t, struct Cvol fluxvol_t ,\n");
  fprintf(ff,"			 double *pv,struct Cvol fluxvol_pv,\n");
  fprintf(ff,"			 double *pt,struct Cvol fluxvol_pt,double tempss)\n");
  fprintf(ff,"/************************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}
/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2010                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void ecrire_vide_cini_hmt()
{
  fprintf(ff,"/************************************************************************/ \n");
  fprintf(ff,"void user_hmt_cini_fct(struct Maillage maillnodes,\n");
  fprintf(ff,"		             double *t,double *pv,double *pt)\n");
  fprintf(ff,"/************************************************************************/ \n");
  fprintf(ff,"{\n}\n");
}
