#!/bin/sh -e

# To be used with the same args as if it were called by uscan:
# debian/orig-tar.sh --upstream-version <version> <file>
SOURCE_NAME=syrthes
VERSION=$2
DEBIAN_VERSION=$VERSION-dfsg1
UPSTREAM_SOURCE_DIR=${SOURCE_NAME}$VERSION
DEBIAN_SOURCE_DIR=${SOURCE_NAME}-$DEBIAN_VERSION
TAR=../${SOURCE_NAME}_$DEBIAN_VERSION.orig.tar.gz

# extract the upstream archive
tar xf $3
# rename upstream source dir
mv ${UPSTREAM_SOURCE_DIR} ${DEBIAN_SOURCE_DIR}
# repack into orig.tar.gz without unwanted files
tar -c -z -X debian/orig-tar.exclude -f $TAR ${DEBIAN_SOURCE_DIR}/
rm -rf ${DEBIAN_SOURCE_DIR}
echo "syrthes: Applied DFSG removals and renamed archive to `basename ${TAR}`"
