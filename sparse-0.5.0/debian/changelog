sparse (0.5.0-1) unstable; urgency=medium

  * new upstream release (Closes: #743923)
    - upstream relicensed under MIT (Closes: #524319)
  * provide PREFIX already at build time (Closes: #660274)
  * Standards-Version: 3.9.6 (no changes needed) 
  * Merge 0.4.5~rc1-2, thanks to Andreas Beckmann 
  * Take over maintenance, drop Loïc from Uploaders (Closes: #794643)
  * cherry-pick patches from upstream to make sparse multi-arch aware
    (Closes: #755979)

 -- Uwe Kleine-König <ukleinek@debian.org>  Tue, 03 Nov 2015 00:43:13 +0100

sparse (0.4.5~rc1-2~deb8u1) jessie; urgency=medium

  * QA upload.
  * Rebuild for jessie.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 09 Sep 2015 22:28:16 +0200

sparse (0.4.5~rc1-2) unstable; urgency=medium

  [ Andreas Beckmann ]
  * QA upload.
  * Set maintainer to Debian QA Group.  (See #794643)
  * Fix Homepage and Vcs-Browser URLs.
  * Refresh patch to apply without fuzz.

  [ Uwe Kleine-König ]
  * Cherry-pick commit from upstream to fix build failure with llvm-3.5.
  * Temporarily build-depend on libedit-dev because llvm-config claims to need
    that.  (Closes: #793197)

 -- Andreas Beckmann <anbe@debian.org>  Sat, 08 Aug 2015 13:17:27 +0200

sparse (0.4.5~rc1-1) unstable; urgency=low

  [ Uwe Kleine-König ]
  * new upstream release candidate
  * switch to debhelper 9 to get hardend binaries
  * Standards-Version: 3.9.4 (no changes needed)
  * Add patch to add --as-needed to LDFLAGS (except for LLVM).
  * Add missing ${perl:Depends}.

  [ Loïc Minier ]
  * Use my Debian address in control.
  * Update Vcs fields to use anonscm URLs.
  * Don't repeat Section and Priority in binary package stanza.
  * Fix typo in copyright file.
  * Add watch file.
  * Build sparse-llvm by adding llvm-dev (>= 3.0~) build-dep.
  * Add patch to pass CFLAGS and CPPFLAGS from rules to Makefile, notably
    hardening flags.

 -- Loïc Minier <lool@debian.org>  Sat, 15 Jun 2013 01:06:33 +0200

sparse (0.4.3+20110419-1) unstable; urgency=low

  * Merge upstream up to 87f4a7fda3d17:
    + Fixes build with gcc-4.6 (Closes: 625962).

 -- Pierre Habouzit <madcoder@debian.org>  Sat, 07 May 2011 16:26:29 +0200

sparse (0.4.3-1) unstable; urgency=low

  * New upstream release (Closes: #587005):
    + inline forward declarations are now allowed (Closes: #607432).
  * Update Homepage (Closes: #566605).
  * Bump standards versions to 3.9.1.
  * Switch from cdbs to debhelper.
  * Add libxml2-dev and libgtk2.0-dev to build-depends to build new tools
    (Closes: #608592).

 -- Pierre Habouzit <madcoder@debian.org>  Sat, 19 Mar 2011 20:26:16 +0100

sparse (0.4.1-1) unstable; urgency=low

  * New upstreal release.
  * (debian/control):
      + rename XS-VCS-* headers into VCS-*.
      + have a real Homepage header.

 -- Pierre Habouzit <madcoder@debian.org>  Sun, 18 Nov 2007 09:33:43 +0100

sparse (0.4-2) unstable; urgency=low

  * Upload to unstable.

 -- Pierre Habouzit <madcoder@debian.org>  Fri, 12 Oct 2007 10:11:58 +0200

sparse (0.4-1) experimental; urgency=low

  * New upstream release: closes: #426143, #444879:
      + (debian/copyright): update copyrights.
  * Take over maintainance with permission from Loïc.
  * (debian/control):
      + add XS-Vcs-* headers.
      + add XS-autobuild: Yes.
  * (debian/sparse.manpages): install cgcc.1, sparse.1.

 -- Pierre Habouzit <madcoder@debian.org>  Wed, 10 Oct 2007 00:55:55 +0200

sparse (0.3-1) experimental; urgency=low

  * New upstream release.
    - Fixes usage of prefix versus DESTDIR in sparse.pc; closes: #404399.
    - Pass PREFIX to all make invocations.
    - Update path to pkg-config file.
  * Set CFLAGS manually in debian/rules for the upstream build until it
    permits passing additional CFLAGS; thanks Pierre Habouzit;
    closes: #422896.
  * Update homepage; add Homepage field to Description.
  * Workaround bogus upstream version by passing the upstream version computed
    by CDBS to the upstream build.

 -- Loic Minier <lool@dooz.org>  Fri, 25 May 2007 18:05:17 +0200

sparse (0.2-2) experimental; urgency=low

  * Ship FAQ.

 -- Loic Minier <lool@dooz.org>  Thu,  7 Dec 2006 21:59:18 +0100

sparse (0.2-1) experimental; urgency=low

  * New upstream release.
    - Update vars for new upstream Makefile (which supports DESTDIR).
    - Drop manual bindir creation in build/sparse which isn't needed anymore.
  * Ship pkg-config file.
  * Ship headers and static library.
  * Build and ship shared library.

 -- Loic Minier <lool@dooz.org>  Thu,  7 Dec 2006 17:27:13 +0100

sparse (0.1-1) experimental; urgency=low

  * Initial release; closes: #397780.

 -- Loic Minier <lool@dooz.org>  Thu,  9 Nov 2006 13:09:59 +0100
