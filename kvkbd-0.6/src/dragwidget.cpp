/*
 * This file is part of the Kvkbd project.
 * Copyright (C) 2007-2008 Todor Gyumyushev <yodor@developer.bg>
 * Copyright (C) 2008 Guillaume Martres <smarter@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "dragwidget.h"

#include <QMouseEvent>

#include <KDebug>

DragWidget::DragWidget(QWidget *parent, Qt::WindowFlags f) : QWidget(parent, f)
{
    dragPoint = QPoint(0, 0);
    dragged = false;
    setLocked(false);
}

DragWidget::~DragWidget()
{

}

bool DragWidget::isLocked() const
{
    return locked;
}

void DragWidget::setLocked(bool mode)
{
    if (mode != isLocked()) {
        locked = mode;
        emit(widgetLocked(mode));
    }
}

bool DragWidget::isDragged() const
{
    return dragged;
}

void DragWidget::mousePressEvent(QMouseEvent *e)
{
    if (isLocked()) {
        return;
    }
    gpress = e->globalPos();
    dragged = true;
    dragPoint = e->pos();
    setCursor(Qt::SizeAllCursor);

}

void DragWidget::mouseReleaseEvent(QMouseEvent *)
{
    dragged = false;
    unsetCursor();
}

void DragWidget::mouseMoveEvent(QMouseEvent *e)
{
    if (!isDragged()) {
        return;
    }
    QPoint curr(e->globalPos().x() - dragPoint.x(), e->globalPos().y() - dragPoint.y());
    QWidget::move(curr);
}
