/*
 * shm_ringbuf.c
 *
 * Copyright (C) 2004-2011  Simon Wunderlich <sw@simonwunderlich.de>
 *
 * This file is part of s3d, a 3d network display server.
 * See http://s3d.berlios.de/ for more updates.
 *
 * s3d is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * s3d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with s3d; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "global.h"
#include <stdio.h>		/* printf(), getchar() */
#include <stdint.h>		/* uint32_t */
#include <string.h>		/* memcpy() */

int shm_write(struct buf_t *rb, char *buf, int n)
{
	int wrap = 0;
	int rs;
	int32_t e, s, size;
	char *data;

	e = rb->end;
	s = rb->start;
	size = rb->bufsize;
	data = ((char *)rb) + sizeof(struct buf_t);
	if (e < s) {
		wrap = 1;
	}
	if ((((s + size * (1 - wrap)) - e) < (n + 1))) {	/* checking free space */
		printf("buffer reached maxsize, no resizing possible");
	}
	if ((e + n) > size) {
		rs = size - e;
		memcpy(data + e, buf, rs);	/* copy the first part ... */
		memcpy(data, buf + rs, n - rs);	/* .. end the rest */
	} else {
		memcpy(data + e, buf, n);	/* plain copy */
	}
	rb->end = e + n;	/* update end of the buffer */
	if (rb->end >= rb->bufsize)
		rb->end -= rb->bufsize;
	return 0;
}

int shm_read(struct buf_t *rb, char *buf, int n)
{
	int wrap = 0;
	int mn;
	int rs;
	int32_t e, s, size;
	char *data;

	e = rb->end;
	s = rb->start;
	size = rb->bufsize;
	data = ((char *)rb) + sizeof(struct buf_t);
	if (e < s)
		wrap = 1;
	rs = (e + wrap * size - s);
	mn = (n > rs) ? rs : n;
	if ((wrap) && (mn > (size - s))) {
		rs = size - s;	/* size of the first part */
		memcpy(buf, data + s, rs);
		memcpy(buf + rs, data, mn - rs);
	} else {		/* no wrap (needed) */
		memcpy(buf, data + s, mn);
	}
	rb->start = s + mn;
	if (rb->start >= rb->bufsize)
		rb->start -= rb->bufsize;
	return mn;
}

void ringbuf_init(char *data, uint32_t init_size)
{
	struct buf_t *ringbuf = (struct buf_t *)data;
	ringbuf->start = 0;
	ringbuf->end = 0;
	ringbuf->bufsize = init_size - RB_OVERHEAD;
}
