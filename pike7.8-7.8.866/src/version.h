/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: dcde942d38aa67f551c42d5c983324bccf503c2e $
*/

#define PIKE_MAJOR_VERSION 7
#define PIKE_MINOR_VERSION 8
#define PIKE_BUILD_VERSION 866

/* Prototypes begin here */
void f_version(INT32 args);
void push_compact_version();
/* Prototypes end here */
