/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#include "c2bColors.h"
#include "c2bSettings.h"


c2bColors::c2bColors()
{
    colorList.insert("BibTeX delimiters", &bib_delimiter_color);
    colorList.insert("BibTeX reference field", &bib_reference_field_color);
    colorList.insert("BibTeX reference type", &bib_reference_type_color);
    colorList.insert("Comments and line numbers", &comment_color);
    colorList.insert("NetQInf command contents", &netqinf_command_content_color);
    colorList.insert("NetQInf command keys", &netqinf_command_key_color);
    colorList.insert("RegExp and NetQInf cb2Bib tags", &cb2bib_tag_color);
    colorList.insert("cb2Bib digits", &cb2bib_digit_color);
    colorList.insert("cb2Bib highly relevant text", &cb2bib_highly_relevant_color);
    colorList.insert("cb2Bib metadata", &cb2bib_metadata_color);
    colorList.insert("cb2Bib relevant text", &cb2bib_relevant_color);
    colorList.insert("cb2Bib unrelevant text", &cb2bib_unrelevant_color);
}

c2bColors::~c2bColors()
{}


void c2bColors::updateColor(const QColor& color, const QString& colorName)
{
    *colorList[colorName] = color;
}

void c2bColors::saveColors(c2bSettings* settings)
{
    settings->setValue("c2bColors/bib_delimiter_color", bib_delimiter_color);
    settings->setValue("c2bColors/bib_reference_field_color", bib_reference_field_color);
    settings->setValue("c2bColors/bib_reference_type_color", bib_reference_type_color);
    settings->setValue("c2bColors/cb2bib_digit_color", cb2bib_digit_color);
    settings->setValue("c2bColors/cb2bib_highly_relevant_color", cb2bib_highly_relevant_color);
    settings->setValue("c2bColors/cb2bib_metadata_color", cb2bib_metadata_color);
    settings->setValue("c2bColors/cb2bib_relevant_color", cb2bib_relevant_color);
    settings->setValue("c2bColors/cb2bib_tag_color", cb2bib_tag_color);
    settings->setValue("c2bColors/cb2bib_unrelevant_color", cb2bib_unrelevant_color);
    settings->setValue("c2bColors/comment_color", comment_color);
    settings->setValue("c2bColors/netqinf_command_content_color", netqinf_command_content_color);
    settings->setValue("c2bColors/netqinf_command_key_color", netqinf_command_key_color);
}

void c2bColors::loadColors(c2bSettings* settings)
{
    bib_delimiter_color = settings->value("c2bColors/bib_delimiter_color", "#A9A9A9").value<QColor>();
    bib_reference_field_color = settings->value("c2bColors/bib_reference_field_color", "#FFE66A").value<QColor>();
    bib_reference_type_color = settings->value("c2bColors/bib_reference_type_color", "#5EB7F7").value<QColor>();
    cb2bib_digit_color = settings->value("c2bColors/cb2bib_digit_color", "#39C9FD").value<QColor>();
    cb2bib_highly_relevant_color = settings->value("c2bColors/cb2bib_highly_relevant_color", "#00FD00").value<QColor>();
    cb2bib_metadata_color = settings->value("c2bColors/cb2bib_metadata_color", "#FFE66A").value<QColor>();
    cb2bib_relevant_color = settings->value("c2bColors/cb2bib_relevant_color", "#008B8B").value<QColor>();
    cb2bib_tag_color = settings->value("c2bColors/cb2bib_tag_color", "#5AF7F4").value<QColor>();
    cb2bib_unrelevant_color = settings->value("c2bColors/cb2bib_unrelevant_color", "#868374").value<QColor>();
    comment_color = settings->value("c2bColors/comment_color", "#A5A5A5").value<QColor>();
    netqinf_command_content_color = settings->value("c2bColors/netqinf_command_content_color", "#FFFFFF").value<QColor>();
    netqinf_command_key_color = settings->value("c2bColors/netqinf_command_key_color", "#D5A805").value<QColor>();
}
