# -*- coding: utf-8 -*-

from  ui_Output_Times_form import Ui_Output_Times_form
import sys
import os
from PyQt4 import QtCore, QtGui

class Output_Times_FormImpl(QtGui.QMainWindow,Ui_Output_Times_form):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        Ui_Output_Times_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)
    pass

class Output_Times_FormHandler(Output_Times_FormImpl):
    def __init__(self, parent=None,casePath=None,parentLineEdit=None):
        Output_Times_FormImpl.__init__(self,parent)
        self.deleteButton.setEnabled(False)
        self.listWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.listWidget.setSortingEnabled(True)
        self._connectSlots()
        self._setValidator()
        self.casePath=casePath
        self.parentLineEdit=parentLineEdit
        self._addLineEdit()
        pass

    def convertOSsep(self, strIn):    
        # special treatment for possible anormal inversion of os separator (/, \\) in Windows
        sIn = str(strIn)
        if sIn.find(os.sep) == -1 :
            if os.sep == "\\" :
                sIn = sIn.replace("/", "\\")
            else :
                sIn = sIn.replace("\\", "/")
            pass
        return os.path.abspath(sIn)

    def _addLineEdit(self):
        line = str(self.parentLineEdit.text())
        if len(line) == 0:
            return
        for v in line.split(" "):
            try:
                float(v)
            except ValueError:
                continue
            lvi = QtGui.QListWidgetItem(self.listWidget)            
            lvi.setData(QtCore.Qt.DisplayRole,float(v))
            self.deleteButton.setEnabled(True)
        pass

    def save(self):
        if (self.listWidget.count() > 0):
            buf=""
        else:
            return
        for i in range(self.listWidget.count()):
            t = self.listWidget.item(i).text()
            buf = buf + " " + t
            pass
        self.parentLineEdit.setText(buf)
        pass

    def load(self, dtfile):
        while(self.listWidget.count() > 0):
            self.listWidget.takeItem(0)
        
        f = open(dtfile)
        for line in f:
            if len(line):
                try: 
                    float(line)
                except ValueError:
                    continue
                lvi = QtGui.QListWidgetItem(self.listWidget)            
                lvi.setData(QtCore.Qt.DisplayRole,float(line))
                self.deleteButton.setEnabled(True)
                pass
            pass
        self._sortItems()
        pass
    
    def export(self, dtfile):
        f = open(dtfile,"w")
        for i in range(self.listWidget.count()):
            f.write(self.listWidget.item(i).text())
            f.write("\n")
            pass
        f.close()
        self.save()
        pass

    def _sortItems(self):
        self.listWidget.sortItems(QtCore.Qt.AscendingOrder)
        pass
    
    def _setValidator(self):
        self.lineEdit.setValidator(QtGui.QDoubleValidator(self))
        pass
    
    def _connectSlots(self):
        # Connect our two methods to SIGNALS the GUI emits.
        self.connect(self.addButton,QtCore.SIGNAL("clicked()"),self._slotAddClicked)
        self.connect(self.deleteButton,QtCore.SIGNAL("clicked()"),self._slotDeleteClicked)
        self.connect(self.exportButton,QtCore.SIGNAL("clicked()"),self._slotSaveClicked)
        self.connect(self.importButton,QtCore.SIGNAL("clicked()"),self._slotImportClicked)
        self.lineEdit.returnPressed.connect(self.addButton.click)

    def _slotAddClicked(self):
        # Read the text from the lineEdit,
        text = self.lineEdit.text()
        # if the lineEdit is not empty,
        if len(text):
            try: 
                float(text)
            except ValueError:
                self.lineEdit.clear()
                return
            lvi = QtGui.QListWidgetItem(self.listWidget)            
            lvi.setData(QtCore.Qt.DisplayRole,float(text))
            self.lineEdit.clear()
            self.deleteButton.setEnabled(True)
            pass
        self._sortItems()
        self.save()
        pass
    
    def _slotDeleteClicked(self):
        for item in self.listWidget.selectedItems():
            self.listWidget.takeItem(self.listWidget.row(item))
            pass

        if self.listWidget.count() == 0:
            self.deleteButton.setEnabled(False)
            pass
        self.save()
        pass

    def _slotSaveClicked(self):
        savePathSuggestion = ""
        if self.casePath == "" :
            savePathSuggestion = QtCore.QDir.currentPath() + self.tr(os.sep+"instants.dt") 
        else :
            savePathSuggestion = self.casePath + self.tr(os.sep+"instants.dt")
            
        dataFileselection = QtGui.QFileDialog.getSaveFileName(self, self.tr("Select File"), savePathSuggestion, self.tr(" *.dt ;; All Files (*)"))
        if dataFileselection == "" :
            return        
        dataFileselection = str(self.convertOSsep(dataFileselection))

        self.export(dataFileselection)
        pass

    def _slotImportClicked(self):
        formatlist=self.tr(' *.dt ;; All Files (*)')
        dtFile = QtGui.QFileDialog.getOpenFileName(self, self.tr("Select File"), self.casePath+self.tr(os.sep),formatlist)
        if str(dtFile) == "" : # user clicks on "Cancel"
            return
        dtFile = str(self.convertOSsep(dtFile))

        self.lineEdit_2.setText(dtFile)

        self.load(dtFile)
        self.save()
        pass

    pass




if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = Output_Times_FormHandler(casePath="/export/data/kanemich/syrthes4.2/src-IHM_20141118/")
    myapp.show()
    sys.exit(app.exec_())

