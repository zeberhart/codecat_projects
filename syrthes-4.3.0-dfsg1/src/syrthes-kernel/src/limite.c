/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "syr_usertype.h"
#include "syr_tree.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_parall.h"
#include "syr_proto.h"
#include "syr_hmt_proto.h"
#include "syr_const.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
#endif

extern struct Performances perfo;
extern struct Affichages affich;
extern char nomdata[CHLONG];
extern FILE *fdata;

extern int nbVar;

static char ch[CHLONG],motcle[CHLONG],motcle2[CHLONG],motcle3[CHLONG],repc[CHLONG],formule[CHLONG];
static double dlist[100];
static int ilist[100];


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Allocation du tableau de type des cond lim                           |
  |======================================================================| */
void decode_clim_alloue(struct MaillageBord *maillnodebord)
{
  int i,j;

  maillnodebord->type=(int**)malloc(maillnodebord->nelem*sizeof(int*));
  for (i=0;i<maillnodebord->nelem;i++) maillnodebord->type[i]=(int*)malloc(MAXPOS*sizeof(int));
  verif_alloue_int2d(maillnodebord->nelem,"decode_clim_alloue",maillnodebord->type);

    for (i=0;i<maillnodebord->nelem;i++) 
      for (j=0;j<MAXPOS;j++)
	maillnodebord->type[i][j]=0;

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Calcul en conduction/rayt - une seule variable temperature           |
  | Typage des faces de bord                                             |
  | Premiere lecture des CL pour identification des types de facettes    |
  |======================================================================| */
void decode_clim_cond(struct MaillageBord *maillnodebord)
{
  int n,i,j,nr,ityp;
  int n0,n1,n2,n3;
  int n0tot,n1tot,n2tot,n3tot;

  int i1,i2,i3,i4,ii,ii2,id,nb,ok;
  double val;

  int nbc=4;     /* 0=flux 1=dir 2=ech 3=ray_inf */
  int **ref;   


  /* Allocations */
  /* ----------- */

  /* Rq : ici nbVar est toujours = a 1 */

  ref=(int**)malloc(nbc*sizeof(int*));
  for (i=0;i<nbc;i++)                    
    {
      ref[i]=(int*)malloc(MAX_REF*sizeof(int));
      for (j=0;j<MAX_REF;j++) ref[i][j]=0;
    }


  /* 1ere passe : decodage des references pour les CL */
  /* ================================================ */
  
  fseek(fdata,0,SEEK_SET);
  
  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  
	  ityp=-1;
	  if      (!strcmp(motcle,"CLIM_T")) ityp=0;
	  else if (!strcmp(motcle,"CLIM_T_FCT"))  ityp=1;
	  else if (!strcmp(motcle,"CLIM_T_PROG")) ityp=2;


	  /* conditions commencant par "CLIM_T" ou "CLIM_T_FCT" ou "CLIM_T_PROG" */
	  if (ityp>=0)
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
	      if (!strcmp(motcle,"FLUX"))
		{           
		  switch(ityp){
		  case 0: rep_ndbl(1,&val,&ii,ch+id); id+=ii; break;
		  case 1: ii=rep_ch(formule,ch+id);id+=ii; break;
		  }
		  rep_listint(ilist,&nb,ch+id);
		  verif_liste_ref_0N("CLIM_   FLUX",nb,ilist);
		  for (n=0;n<nb;n++) ref[0][ilist[n]]=1;
		}
	      else if (!strcmp(motcle,"DIRICHLET"))
		{            
		  switch(ityp){
		  case 0: rep_ndbl(1,&val,&ii,ch+id);id+=ii; break;
		  case 1: ii=rep_ch(formule,ch+id); id+=ii; break;
		  }
		  rep_listint(ilist,&nb,ch+id);
		  verif_liste_ref_0N("CLIM_   DIRICHLET",nb,ilist);
		  for (n=0;n<nb;n++) ref[1][ilist[n]]=1;
		}
	      else if (!strcmp(motcle,"COEF_ECH"))
		{            
		  switch(ityp){
		  case 0: rep_ndbl(2,dlist,&ii,ch+id); id+=ii; break;
		  case 1: ii=rep_ch(formule,ch+id); id+=ii;
		          ii=rep_ch(formule,ch+id); id+=ii;
			  break;
		  }
		  rep_listint(ilist,&nb,ch+id);
		  verif_liste_ref_0N("CLIM_   COEF_ECH",nb,ilist);
		  for (n=0;n<nb;n++) ref[2][ilist[n]]=1;
		}
	      else if (!strcmp(motcle,"RAY_INFINI"))
		{            
		  switch(ityp){
		  case 0: rep_ndbl(2,dlist,&ii,ch+id); id+=ii; break;
		  case 1: ii=rep_ch(formule,ch+id); id+=ii;
		          ii=rep_ch(formule,ch+id); id+=ii;
			  break;
		  }
		  rep_listint(ilist,&nb,ch+id);
		  verif_liste_ref_0N("CLIM_   RAY_INFINI",nb,ilist);
		  for (n=0;n<nb;n++) ref[3][ilist[n]]=1;
		}

	    }

	}
    } /* fin du while */


  /* ref : 0=flux 1=dir 2=ech 3=ray_inf */

  /* compte des facettes de chaque type de CL */
  /* ---------------------------------------- */
  /* on repere aussi les elts de type flux : type[POSTYPFLUX]>0 si l'elt est de type flux */

  n0=n1=n2=n3=0;
  for (i=0;i<maillnodebord->nelem;i++)
    {
      nr=maillnodebord->nrefe[i];
      
      if (ref[0][nr]) {maillnodebord->type[i][POSFLUX]=maillnodebord->type[i][POSTYPFLUX]=1;    n0++;}
      if (ref[1][nr]) {
	for (j=0;j<MAXPOS;j++) maillnodebord->type[i][j]=0;
	maillnodebord->type[i][POSDIRIC]=1;   n1++;
      }
      if (ref[2][nr]) {maillnodebord->type[i][POSECH]=maillnodebord->type[i][POSTYPFLUX]=1;     n2++;}
      if (ref[3][nr]) {maillnodebord->type[i][POSRAYINF]=maillnodebord->type[i][POSTYPFLUX]=1;  n3++;}
    }
  
  
  n0tot=n0;      n1tot=n1;      n2tot=n2;      n3tot=n3; 
  
  if (syrglob_nparts>1)
    {
      n0tot=somme_int_parall(n0);
      n1tot=somme_int_parall(n1);
      n2tot=somme_int_parall(n2);
      n3tot=somme_int_parall(n3);
    }
  
  if (syrglob_nparts==1 || syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR){
	printf("\n *** decode_clim_cond : conditions aux limites\n");
	printf("           -----------------------------------------------\n");
	printf("           |  Conditions aux limites    |  Nbre de faces |\n");
	printf("           -----------------------------|-----------------\n");
	printf("           |          Flux              | %10d     |\n",n0tot);
	printf("           |        Dirichlet           | %10d     |\n",n1tot);
	printf("           |         Echange            | %10d     |\n",n2tot);
	printf("           |     Rayonnement infini     | %10d     |\n",n3tot);
	printf("           -----------------------------------------------\n");
      }
      else if (SYRTHES_LANG == EN){
	printf("\n *** decode_clim : boundary conditions\n");
	printf("           -----------------------------------------------\n");
	printf("           |    Boundary conditions     |  Nbre of faces |\n");
	printf("           -----------------------------|-----------------\n");
	printf("           |          Flux              | %10d     |\n",n0tot);
	printf("           |        Dirichlet           | %10d     |\n",n1tot);
	printf("           |         Exchange           | %10d     |\n",n2tot);
	printf("           |    Infinite radiation      | %10d     |\n",n3tot);
	printf("           -----------------------------------------------\n");
      }
      
    } /* fin si nparts=1 */


  /* liberation memoire */
  for (i=0;i<nbc;i++) free(ref[i]);
  free (ref);
  
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Calcul cond ou hmt :                                                 |
  | Typage des faces de bord pour la temperature :                       |
  |   RC,ray inf,couplees fluide, couplees ray, perio                    |
  |======================================================================| */
void decode_clim_all(struct MaillageBord *maillnodebord)
{
  int n,i,j,nr,ityp;
  int n0,n1,n2,n3,n4;
  int n0tot,n1tot,n2tot,n3tot,n4tot,n5tot,n6tot,n7tot;

  int i1,i2,i3,i4,ii,ii2,id,nb,ok;
  double val;

  int nbc=5;     /* 0=resc 1=--- 2=couple_fluide  3=couple_ray 4=perio  */
  int **ref;   


  /* Allocations */
  /* ----------- */

  ref=(int**)malloc(nbc*sizeof(int*));
  for (i=0;i<nbc;i++)                    
    {
      ref[i]=(int*)malloc(MAX_REF*sizeof(int));
      for (j=0;j<MAX_REF;j++) ref[i][j]=0;
    }


  /* 1ere passe : decodage des references pour les CL */
  /* ================================================ */
  
  fseek(fdata,0,SEEK_SET);
  
  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  
	  if (!strncmp(motcle,"CLIM",4) ){
	    extr_motcle(motcle2,ch+i2+1,&i3,&i4);
	    id=i2+1+i4+1;
	  }
	  else
	    continue;


	  if (!strcmp(motcle2,"RES_CONTACT"))
	    {            
	      if (!strstr(motcle,"PROG")) /* s'il n'y a pas PROG, il y a des reels a lire */
		if (strstr(motcle,"FCT")) /* si c'est une fonction, on lit la formule */
		  for (i=0;i<nbVar;i++) {ii=rep_ch(formule,ch+id); id+=ii;}
		else
		  {rep_ndbl(nbVar,dlist,&ii,ch+id); id+=ii;}
	      rep_listint(ilist,&nb,ch+id);
	      /* verification que la liste contient un -1 */
	      for (ok=0,i=0;i<MAX_REF;i++) if (ilist[i]==-1) ok=1;
	      if (!ok)
		{
		  if (SYRTHES_LANG == FR)
		    {
		      printf("\n\n %%%% ERREUR decode_clim : la liste des references ");
		      printf("des faces avec resistance de contact est incorrecte\n");
		      printf("                         elle doit avoir la forme suivante :\n");
		      printf("                         liste_ref_1  -1  liste_ref_2\n");
		      if (nbVar==1) printf("                         exemple : CLIM  RES_CONTACT 23.   2 34 12 -1 5 32\n");
		      else          printf("                         exemple : CLIM  RES_CONTACT  g_t g_pv (g_pt)  2 34 12 -1 5 32\n");
		      printf("                         --> verifier le fichier fichier de donnees\n");
		    }
		  else if (SYRTHES_LANG == EN)
		    {
		      printf("\n\n %%%% ERROR decode_clim : The list of references ");
		      printf("of patches with contact resistance is wrong\n");
		      printf("                         it must have the fllowing form :\n");
		      printf("                         ref_list_1  -1  ref_list_2\n");
		       if (nbVar==1) printf("                         example : CLIM  RES_CONTACT 23.  2 34 12 -1 5 32\n");
		      else           printf("                         example : CLIM  RES_CONTACT  g_t g_pv (g_pt)  2 34 12 -1 5 32\n");

		      printf("                         --> Please, check the data file\n");
		    }
		  syrthes_exit(1);
		}
	      for (n=0;n<nb;n++) if (ilist[n]>0) ref[0][ilist[n]]=1;
	    }
	  
	  else if (!strncmp(motcle2,"COUPLAGE_SURF_FLUIDE",20))
	    {            
	      rep_listint(ilist,&nb,ch+id);
	      verif_liste_ref_0N("CLIM_   COUPLAGE_SURF_FLUIDE",nb,ilist);
	      for (n=0;n<nb;n++) ref[2][ilist[n]]=1;
	    }
	  
	  else if (!strcmp(motcle2,"COUPLAGE_RAYONNEMENT"))
	    {            
	      rep_listint(ilist,&nb,ch+id);
	      verif_liste_ref_0N("CLIM_   COUPLAGE_RAYONNEMENT",nb,ilist);
	      for (n=0;n<nb;n++) ref[3][ilist[n]]=1;
	    }
	  
	  else if (!strcmp(motcle2,"PERIODICITE_2D") || !strcmp(motcle,"PERIODICITE_3D"))
	    {   
	      extr_motcle(motcle3,ch+id,&i3,&i4);
	      id+=i4+1;
	      
	      if (!strcmp(motcle2,"PERIODICITE_2D") && !strcmp(motcle3,"T")) 
		rep_ndbl(2,dlist,&ii,ch+id);
	      else if (!strcmp(motcle2,"PERIODICITE_3D") && !strcmp(motcle3,"T")) 
		rep_ndbl(3,dlist,&ii,ch+id);
	      else if (!strcmp(motcle2,"PERIODICITE_2D") && !strcmp(motcle3,"R")) 
		rep_ndbl(3,dlist,&ii,ch+id);
	      else if (!strcmp(motcle2,"PERIODICITE_3D") && !strcmp(motcle3,"R")) 
		rep_ndbl(7,dlist,&ii,ch+id);
	      
	      rep_listint(ilist,&nb,ch+id+ii);
	      
	      /* verification que la liste contient un -1 */
	      for (ok=0,i=0;i<MAX_REF;i++) if (ilist[i]==-1) ok=1;
	      if (!ok)
		{
		  if (SYRTHES_LANG == FR)
		    {
		      printf("\n\n %%%% ERREUR decode_clim : la liste des references ");
		      printf("des faces avec periodicite est incorrecte\n");
		      printf("                         elle doit avoir la forme suivante :\n");
		      printf("                         liste_ref_1  -1  liste_ref_2\n");
		      printf("                         exemple : CLIM  PERIODICITE_2D  T   1. 0.     5 -1 2 3\n");
		      printf("                         --> verifier le fichier fichier de donnees\n");
		    }
		  else if (SYRTHES_LANG == EN)
		    {
		      printf("\n\n %%%% ERROR decode_clim : The list of references ");
		      printf("of patches with periodicity is wrong\n");
		      printf("                         it must have the fllowing form :\n");
		      printf("                         ref_list_1  -1  ref_list_2\n");
		      printf("                         example : CLIM  PERIODICITE_2D  T   1. 0.     5 -1 2 3\n");
		      printf("                         --> Please, check the data file\n");
		    }
		  syrthes_exit(1);
		}
	      for (n=0;n<nb;n++) if (ilist[n]>0) ref[4][ilist[n]]=1;
	    }
	}
      
    } /* fin du while */


  /* 0=resc 1=---   2=couple_fluide  3=couple_ray 4=perio  */

  /* compte des facettes de chaque type de CL */
  /* ---------------------------------------- */
  /* on repere aussi les elts de type flux : type[POSTYPFLUX]>0 si l'elt est de type flux */

  n0=n1=n2=n3=n4=0;
  for (i=0;i<maillnodebord->nelem;i++)
    {
      nr=maillnodebord->nrefe[i];
      
      if (ref[0][nr]) {maillnodebord->type[i][POSRESCON]=maillnodebord->type[i][POSTYPFLUX]=1;  n0++;}
      
      if (ref[2][nr]) {maillnodebord->type[i][POSCOUPF]=maillnodebord->type[i][POSTYPFLUX]=1;   n2++;}
      if (ref[3][nr]) {maillnodebord->type[i][POSCOUPR]=maillnodebord->type[i][POSTYPFLUX]=1;   n3++;}
      
      if (ref[4][nr]) {n4++;}
    }
  
  
  n0tot=n0;          n2tot=n2;      n3tot=n3;     n4tot=n4;     
  
  if (syrglob_nparts>1)
    {
      n0tot=somme_int_parall(n0);
      n2tot=somme_int_parall(n2);
      n3tot=somme_int_parall(n3);
      n4tot=somme_int_parall(n4);
    }
  
  if (syrglob_nparts==1 || syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR){
	printf("\n *** decode_clim_all : Conditions aux limites (suite)\n");
	printf("           -----------------------------------------------\n");
	printf("           |  Conditions aux limites    |  Nbre de faces |\n");
	printf("           -----------------------------|-----------------\n");
	printf("           |  Resistance de contact     | %10d     |\n",n0tot);
	printf("           | Couplage avec le fluide    | %10d     |\n",n2tot);
	printf("           |Couplage avec le rayonnement| %10d     |\n",n3tot);
	printf("           |       Periodicite          | %10d     |\n",n4tot);
	printf("           -----------------------------------------------\n");
      }
      else if (SYRTHES_LANG == EN){
	printf("\n *** decode_clim_all : boundary conditions\n");
	printf("           -----------------------------------------------\n");
	printf("           |    Boundary conditions     |  Nbre of faces |\n");
	printf("           -----------------------------|-----------------\n");
	printf("           |    Contact resistance      | %10d     |\n",n0tot);
	printf("           |    Coupling with fluid     | %10d     |\n",n2tot);
	printf("           |   Coupling with radiation  | %10d     |\n",n3tot);
	printf("           |       Periodicity          | %10d     |\n",n4tot);
	printf("           -----------------------------------------------\n");
      }
      
    } /* fin si nparts=1 */


  /* liberation memoire */
  for (i=0;i<nbc;i++) free(ref[i]);
  free (ref);
  
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_limite_cond(struct Clim *flux,struct Clim *diric,struct Clim *echang,
		      struct Maillage maillnodes,struct MaillageBord maillnodebord,
		      struct MaillageCL maillnodeus)
{
  int i,j,k,nn,indicT=0;
  int i1,i2,i3,i4,id,ok=1,ii,nb,nr,n;
  int *tab;
  double val;

  fseek(fdata,0,SEEK_SET);


  if (syrglob_nparts==1 ||syrglob_rang==0){
    printf("\n");
    if (SYRTHES_LANG == FR)  
      printf("\n *** LECTURE DES CONDITIONS AUX LIMITES DANS LE FICHIER DE DONNEES\n");
    else if (SYRTHES_LANG == EN)
      printf("\n *** READING OF THE BOUNDARY CONDITIONS IN THE DATA FILE\n");
  }

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);

	  if (!strncmp(motcle,"CLIM_T",6) && 
	      !strstr(motcle ,"FCT")  && !strstr(motcle,"PROG")) /* valeurs a lire que si ce n'est */ 
	    {
	      indicT=1;
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"FLUX"))            
		lire_flux(flux,maillnodeus,ch,id," T");
	      else if (!strcmp(motcle,"DIRICHLET")) 
		lire_dirichlet(diric,maillnodes,maillnodebord,ch,id," T");
	      else if (!strcmp(motcle,"COEF_ECH")) 
		lire_echange(echang,maillnodeus,ch,id," T");
	    }

	}
    }
  
  
  if (!indicT && syrglob_rang==0) 
    {
      if (SYRTHES_LANG == FR)
	printf("        --> Aucune condition limite exterieure sur T n'est presente dans le fichier de donnees\n");
      else if (SYRTHES_LANG == EN)
	printf("        --> No exterior boundary condition for T is present in the data file\n");
    }

  /* impression des conditions aux limites lues */
  if (affich.cond_clim)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n>>>lire_limite :Conditions aux limites sur la temperature\n");
	  printf("---------------------------------------------------------\n");
	  printf("\n>>>lire_limite : Impression des Dirichlet\n");
	  imprime_Climp(*diric);
	  printf(">>>lire_limite : Impression des Flux\n");
	  imprime_Clim(*flux);
	  printf(">>>lire_limite : Impression des Coefficients d'echange\n");
	  imprime_Clim(*echang);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n>>>lire_limite :Boundary condition for the temperature variable\n");
	  printf("---------------------------------------------------------------\n");
	  printf("\n>>>lire_limite : Printing relative to Dirichlet\n");
	  imprime_Climp(*diric);
	  printf(">>>lire_limite : Printing relative to Flux\n");
	  imprime_Clim(*flux);
	  printf(">>>lire_limite : Printing relative to exchange condition\n");
	  imprime_Clim(*echang);
	}
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_limite_all(struct Contact *rescon,struct Clim *rayinf,
			  struct Maillage maillnodes,struct MaillageBord maillnodebord,
			  struct MaillageCL maillnodeus)
{
  int i,j,k,nn;
  int i1,i2,i3,i4,id,ok=1,ii,nb,nr,n;
  int yenari=0, yenarc=0, no_rc_cst=0, no_ri_cst=0;
  int *tab;
  double val;

  fseek(fdata,0,SEEK_SET);


  if (syrglob_nparts==1 ||syrglob_rang==0){
    printf("\n");
    if (SYRTHES_LANG == FR)  
      printf("\n *** LECTURE DES CONDITIONS AUX LIMITES DANS LE FICHIER DE DONNEES (contact, rayonnement infini)\n");
    else if (SYRTHES_LANG == EN)
      printf("\n *** READING OF THE BOUNDARY CONDITIONS IN THE DATA FILE (contact, infinite radiation)\n");
  }

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);

	  if (strstr(motcle,"CLIM_")) 
	    {
	      extr_motcle(motcle2,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle2,"RES_CONTACT"))
		{
		  if (strstr(motcle ,"FCT") || strstr(motcle,"PROG")) /* valeurs a lire que si ce n'est */
		    {yenarc=1; no_rc_cst=1;}                          /* ni une fonct ni un prog        */
		  else
		    {lire_rescon(rescon,maillnodeus,ch,id); yenarc=1;}
		}              
	      else if (!strcmp(motcle2,"RAY_INFINI"))  
		{
		  if (strstr(motcle ,"FCT") || strstr(motcle,"PROG")) /* valeurs a lire que si ce n'est */
		    {yenari=1; no_ri_cst=1;}                          /* ni une fonct ni un prog        */
		  else
		    {lire_rayinf(rayinf,maillnodeus,ch,id); yenari=1;}    
		}
	    }
	}
    }
  
  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (yenarc==1 && no_rc_cst==1)
      if (SYRTHES_LANG == FR)
	printf("        --> Les resistances de contact sont definie par fonction ou programme \n");
      else if (SYRTHES_LANG == EN)
	printf("        --> Function or program is given for contact resitance\n");

    if (yenari==1 && no_ri_cst==1)
      if (SYRTHES_LANG == FR)
	printf("        --> Le rayonnement infini est defini par fonction ou programme\n");
      else if (SYRTHES_LANG == EN)
	printf("        --> Function or program is given for infinite radiation\n");
  }


  /* impression des conditions aux limites lues */
  if (affich.cond_clim)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n>>>lire_limite :Conditions aux limites sur la temperature\n");
	  printf("---------------------------------------------------------\n");
	  printf(">>>lire_limite : Impression du rayonnement infini\n");
	  imprime_Clim(*rayinf);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n>>>lire_limite :Boundary condition for the temperature variable\n");
	  printf("---------------------------------------------------------------\n");
	  printf(">>>lire_limite : Printing relative to inifinite radiation\n");
	  imprime_Clim(*rayinf);
	}
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_flux(struct Clim *flux,struct MaillageCL maillnodeus,
	       char *ch,int id,char *nomvar)
{
  int i,j,indic=0;
  int ii,nb,nr,n;
  int *tab;
  double val;

  indic=1;
  rep_ndbl(1,&val,&ii,ch+id);
  rep_listint(ilist,&nb,ch+id+ii);
  verif_liste_ref_0N("CLIM_.=   FLUX",nb,ilist);

  if (flux->nelem)
    for (n=0;n<nb;n++)
      {
	nr=ilist[n];
	for (i=0;i<flux->nelem;i++) 
	  if (maillnodeus.nrefe[flux->numf[i]]==nr) 
	    for (j=0;j<flux->ndmat;j++) flux->val1[j][i]=val;
      }
  
  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR) 
	printf("        --> %s - FLUX (%f) impose sur les references",nomvar,val);
      else if (SYRTHES_LANG == EN)
	printf("        --> %s - FLUX (%f) precribed on references\n",nomvar,val);
      for (n=0;n<nb;n++) printf(" %d",ilist[n]);
      printf("\n");
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
  void lire_dirichlet(struct Clim *diric,
		      struct Maillage maillnodes,struct MaillageBord maillnodebord,
		      char *ch,int id,char *nomvar)
{
  int i,j,indic=0;
  int ii,nb,nr,n;
  double val;

  indic=1;
  rep_ndbl(1,&val,&ii,ch+id);
  rep_listint(ilist,&nb,ch+id+ii);
  verif_liste_ref_0N("CLIM_.=   DIRICHLET",nb,ilist);

  if (diric->nelem)
    {
      for (n=0;n<nb;n++)
	{
	  nr=ilist[n];
	  for (i=0;i<diric->nelem;i++) 
	    if (maillnodebord.nrefe[diric->numf[i]]==nr)
	      {
		for (j=0;j<maillnodebord.ndmat;j++)
		  diric->val1[j][i]=val;
	      }
	}
    }

  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR)
	printf("        --> %s - DIRICHLET (%f)  impose sur les references ",nomvar,val);
      else if (SYRTHES_LANG == EN)
	printf("        --> %s - DIRICHLET (%f)  imposed on references ",nomvar,val);
      for (n=0;n<nb;n++) printf(" %d",ilist[n]);
      printf("\n");
    }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_echange(struct Clim *echang,struct MaillageCL maillnodeus,
		  char *ch,int id,char *nomvar)
{
  int i,j,indic=0;
  int ii,nb,nr,n;
  int *tab;
  double val;
  
  indic=1;
  rep_ndbl(2,dlist,&ii,ch+id);
  rep_listint(ilist,&nb,ch+id+ii);
  verif_liste_ref_0N("CLIM_.=   COEF_ECH",nb,ilist);

  if (echang->nelem)
    for (n=0;n<nb;n++)
      {
	nr=ilist[n];
	for (i=0;i<echang->nelem;i++) 
	  if (maillnodeus.nrefe[echang->numf[i]]==nr) 
	    for (j=0;j<echang->ndmat;j++) 
	      {echang->val1[j][i]=*dlist;echang->val2[j][i]=*(dlist+1);}
      }

  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR)
	printf("        --> %s - ECHANGE (%s=%f,h=%f) impose sur les references",nomvar,nomvar,dlist[0],dlist[1]);
      else if (SYRTHES_LANG == EN)
	printf("        --> %s - EXCHANGE (%s=%f,h=%f) imposed on references",nomvar,nomvar,dlist[0],dlist[1]);
      for (n=0;n<nb;n++) printf(" %d",ilist[n]);
      printf("\n");
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture les RC en cond ou en hmt                              |
  |======================================================================| */
void lire_rescon(struct Contact *rescon,struct MaillageCL maillnodeus,
		   char *ch,int id)
{
  int i,j,k,indic=0;
  int ii,nb,nr,n;
  int *tab;
  double val;

  indic=1;
  rep_ndbl(nbVar,dlist,&ii,ch+id);
  rep_listint(ilist,&nb,ch+id+ii);

  /* la liste a deja ete verifiee auparavant */
  if (rescon->nelem)
    {
      for (k=0;k<nb;k++)
	{
	  nr=ilist[k];
	  if (nr>0)  /* il faut sauter le "-1" en milieu de liste */
	    {
	      for (i=0;i<rescon->nelem;i++) 
		if (maillnodeus.nrefe[rescon->numf[i]]==nr) 
		  for (n=0;n<nbVar;n++)
		    for (j=0;j<maillnodeus.ndmat;j++) 
		      rescon->g[n][j][i]=dlist[n];
	    }
	}
    }

  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR){
	printf("        --> RESISTANCE DE CONTACT (%f",dlist[0]);
	for (i=1;i<nbVar;i++) printf(",%f",dlist[i]);
	printf(") imposee les references");
      }
      else if (SYRTHES_LANG == EN){
	printf("        --> CONTACT RESISTANCE (%f",dlist[0]);
	for (i=1;i<nbVar;i++) printf(",%f",dlist[i]);
	printf(") imposed on references");
      }
      for (n=0;n<nb;n++) printf(" %d",ilist[n]);
      printf("\n");
    }



}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_rayinf(struct Clim *rayinf,struct MaillageCL maillnodeus,
		 char *ch,int id)
{
  int i,j,indic=0;
  int ii,nb,nr,n;
  int *tab;
  double val;

  indic=1;
  rep_ndbl(2,dlist,&ii,ch+id);
  rep_listint(ilist,&nb,ch+id+ii);
  verif_liste_ref_0N("CLIM_.=   RAY_INFINI",nb,ilist);

  if (rayinf->nelem)
    {
      for (n=0;n<nb;n++)
	{	
	  nr=ilist[n];
	  for (i=0;i<rayinf->nelem;i++) 
	    {
	      if (maillnodeus.nrefe[rayinf->numf[i]]==nr) 
		for (j=0;j<rayinf->ndmat;j++) {
		  rayinf->val1[j][i]=*dlist;rayinf->val2[j][i]=*(dlist+1);
		}
	    }
	}
    }

  else if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR)
	printf("        --> RAYONNEMENT INFINI (T=%f, eps=%f) impose sur les references",dlist[0],dlist[1]);
      else if (SYRTHES_LANG == EN)
	printf("        --> INFINITE RADIATION (T=%f, eps=%f) imposed on references",dlist[0],dlist[1]);
      for (n=0;n<nb;n++) printf(" %d",ilist[n]);
      printf("\n");
    }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |       creation de "maillnodeus"                                      |
  |======================================================================| */

void xmaill(struct Maillage maillnodes,struct MaillageBord maillnodebord, 
	    struct MaillageCL *maillnodeus,
	    struct SDparall sdparall) 
{
  int cou,flu,ray;
  int nfcou=0,nfflu=0,nfray=0;
  int i,j,n2,netot;
  int *ne0,*ne1,*ne2;


  for (i=0;i<maillnodebord.nelem;i++)
    {
      if (maillnodebord.type[i][POSTYPFLUX])   nfflu++;

      if (maillnodebord.type[i][POSCOUPF])  nfcou++;
      if (maillnodebord.type[i][POSCOUPR])  nfray++;
    }
  
  
  maillnodeus->ndim=maillnodes.ndim;
  maillnodeus->ndiele=maillnodes.ndiele-1;
  maillnodeus->iaxisy=maillnodes.iaxisy;
  maillnodeus->nelem=nfflu;
  maillnodeus->ndmat=maillnodeus->ndim;
  
  if (maillnodeus->nelem) 
    {
      maillnodeus->node=(int**)malloc(maillnodeus->ndmat*sizeof(int*));
      for (i=0;i<maillnodeus->ndmat;i++) maillnodeus->node[i]=(int*)malloc(maillnodeus->nelem*sizeof(int)); 
      maillnodeus->nrefe=(int*)malloc(maillnodeus->nelem*sizeof(int));
      maillnodeus->type=(int**)malloc(maillnodeus->nelem*sizeof(int*));
      for (i=0;i<maillnodeus->nelem;i++) maillnodeus->type[i]=(int*)malloc(MAXPOS*sizeof(int));
      verif_alloue_int2d(maillnodeus->ndmat,"xmaill",maillnodeus->node);
      verif_alloue_int1d("xmaill",maillnodeus->nrefe);
      verif_alloue_int2d(maillnodeus->nelem,"xmaill",maillnodeus->type);
      perfo.mem_cond+=((maillnodeus->ndmat+2)*maillnodeus->nelem) * sizeof(int);
      if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;
      
      n2=0;
      
      if (maillnodes.ndim==2)
	{
	  for (i=0,ne0=maillnodebord.node[0],ne1=maillnodebord.node[1];i<maillnodebord.nelem; i++,ne0++,ne1++)
	    {
	      if (maillnodebord.type[i][0]>0) 
		{
		  maillnodeus->node[0][n2]=*ne0; maillnodeus->node[1][n2]=*ne1; 
		  maillnodeus->nrefe[n2]=maillnodebord.nrefe[i]; 
		  for (j=0;j<MAXPOS;j++) maillnodeus->type[n2][j]=maillnodebord.type[i][j]; 
		  n2++;
		}
	    }
	}
      else
	{
	  for (i=0,ne0=maillnodebord.node[0],ne1=maillnodebord.node[1],ne2=maillnodebord.node[2];
	       i<maillnodebord.nelem;i++,ne0++,ne1++,ne2++)
	    {
	      if (maillnodebord.type[i][0]>0)
		{
		  maillnodeus->node[0][n2]=*ne0; maillnodeus->node[1][n2]=*ne1; maillnodeus->node[2][n2]=*ne2; 
		  maillnodeus->nrefe[n2]=maillnodebord.nrefe[i]; 
		  for (j=0;j<MAXPOS;j++) maillnodeus->type[n2][j]=maillnodebord.type[i][j]; 
		  n2++;
		}
	    }
	}
			 
    }	 	 
	
#ifdef _SYRTHES_MPI_
  netot=somme_int_parall(maillnodeus->nelem);
#else
  netot=maillnodeus->nelem;
#endif

  if (sdparall.nparts==1 || sdparall.rang==0)
    if (SYRTHES_LANG == FR)
      printf("\n *** XMAILL : Nombre d'elements de surface homologues a des flux  pour la variable : %6d\n",netot);
    else if (SYRTHES_LANG == EN)
      printf("\n *** XMAILL : Number of boundary element similar to flux for variable number : %6d\n",netot);
  
  
/*   if (affich.cond_creemaill) */
/*     { */
/*       if (SYRTHES_LANG == FR) */
/* 	printf(">>>\n xmaill : Impression de maillnodeus pour la variable &d\n",numvar); */
/*       else if (SYRTHES_LANG == EN) */
/* 	printf(">>>\n xmaill : Printing of maillnodeus for variable %d\n",numvar); */
/*       imprime_maillage_ref(maillnodeus); */
/*     } */
  
}

      
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
   | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */

void cree_liste_clim_cond(struct Maillage maillnodes,struct MaillageBord maillnodebord,  
			  struct MaillageCL maillnodeus,  
			  struct Clim *diric,struct Clim *flux,struct Clim *echang)
{
  int i,j,nr,ne,k,n,nb,netot;
  int ndir=0,nra=0;
  int nfflu=0,nfech=0,nfdir=0;
  int dir,ok;
  int *ne0,*ne1,*ne2;
  int *itrav1;


  diric->nbval=flux->nbval=1;
  echang->nbval=2;

  diric->npoin=diric->nelem=diric->ndmat=0; diric->nbarete=0;
  flux->npoin=flux->nelem=flux->ndmat=0;
  echang->npoin=echang->nelem=echang->ndmat=0; 

  /* allocation d'un tableau de travail */
  itrav1=(int*)malloc(maillnodes.npoin*sizeof(int));
  verif_alloue_int1d("cree_liste_clim",itrav1);
  for (i=0;i<maillnodes.npoin;i++) itrav1[i]=0;
  perfo.mem_cond+=maillnodes.npoin * sizeof(int);
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;


  /* compte des NOEUDS Dirichlet a partir des FACES Dirichlet */
  /* -------------------------------------------------------- */
  if (maillnodes.ndim==2)
    for (i=0,nfdir=0,ne0=maillnodebord.node[0],ne1=maillnodebord.node[1];i<maillnodebord.nelem;i++,ne0++,ne1++)
      {
	if (maillnodebord.type[i][POSDIRIC]==1)
	  {
	    itrav1[*ne0]=itrav1[*ne1]=1; 
	    nfdir++;
	  }
      }
  else
    for (i=0,nfdir=0,ne0=maillnodebord.node[0],ne1=maillnodebord.node[1],ne2=maillnodebord.node[2];
	 i<maillnodebord.nelem;i++,ne0++,ne1++,ne2++)
      {
	if (maillnodebord.type[i][POSDIRIC]==1)
	  {
	    itrav1[*ne0]=itrav1[*ne1]=itrav1[*ne2]=1; 
	    nfdir++; 
	  }
      }

  for (i=0,ndir=0;i<maillnodes.npoin;i++) {ndir+=itrav1[i];}

  diric->npoin=ndir;   
  diric->nelem=nfdir;  

  if (diric->nelem)
    {
      diric->nump=(int*)malloc(ndir*sizeof(int));verif_alloue_int1d("cree_liste_clim",diric->nump);
      perfo.mem_cond+=ndir * sizeof(int);

      diric->numf=(int*)malloc(nfdir*sizeof(int));
      verif_alloue_int1d("cree_liste_clim",diric->numf);
      diric->ndmat=maillnodebord.ndmat;
      perfo.mem_cond+=nfdir*diric->ndmat * sizeof(int);

      if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;


      for (i=0,nfdir=0;i<maillnodebord.nelem;i++)
	if (maillnodebord.type[i][POSDIRIC]==1) {diric->numf[nfdir]=i; nfdir++;}
      
      for (i=0,ndir=0;i<maillnodes.npoin;i++) if (itrav1[i]) {diric->nump[ndir]=i;ndir++;}

      /* rq: on garde la liste des noeuds, c'est utile dans omvdir */

      /* Liste des elements portant au moins 1 noeud Dirichlet */
      /* ----------------------------------------------------- */
      diric->arete=(int*)malloc(maillnodes.nbarete*sizeof(int));
      verif_alloue_int1d("cree_liste_clim",diric->arete);
      perfo.mem_cond+=maillnodes.nbarete * sizeof(int);
      if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;
      
      for (i=0,ne=0;i<maillnodes.nbarete;i++)
	/*????????  demader a isa  mais faux ???    if (itrav1[maillnodes.eltarete[0][i]] || itrav1[maillnodes.eltarete[1][i]]) */
	if (itrav1[maillnodes.arete[0][i]] || itrav1[maillnodes.arete[1][i]])
	  {diric->arete[ne]=i;ne++;}
      
      diric->nbarete=ne;
      diric->arete=(int*)realloc(diric->arete,diric->nbarete*sizeof(int));
      
      perfo.mem_cond-=(maillnodes.npoin+maillnodes.nbarete-diric->nbarete) * sizeof(int);
      
      diric->xdms=(double*)malloc(diric->nbarete*sizeof(double));
      verif_alloue_double1d("cree_liste_clim",diric->xdms); 
      perfo.mem_cond+=diric->nbarete*sizeof(double);
      
    }



  /* Traitement des CL de type flux et echange */
  /* ----------------------------------------- */

  for (i=0;i<maillnodes.npoin;itrav1[i]=0,i++);
  ne=maillnodeus.nelem;
 
  nfflu=nfech=0;

  for (i=0;i<maillnodeus.nelem;i++)
    {
      if (maillnodeus.type[i][POSFLUX])   nfflu++;
      if (maillnodeus.type[i][POSECH])    nfech++;
    }
  
  echang->npoin=0;    echang->nelem=nfech; 
  flux->npoin=0;      flux->nelem=nfflu;
	  
  if (flux->nelem)  {flux->numf=(int*)malloc(nfflu*sizeof(int));verif_alloue_int1d("cree_liste_clim",flux->numf);
                     flux->ndmat=maillnodeus.ndmat; perfo.mem_cond+=nfflu*sizeof(int);}
  if (echang->nelem)
    {echang->numf=(int*)malloc(nfech*sizeof(int));
      verif_alloue_int1d("cree_liste_clim",echang->numf);
      echang->ndmat=maillnodeus.ndmat; 
      perfo.mem_cond+=nfech*sizeof(int);}

  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  if (flux->nelem || echang->nelem)
    for (i=nfflu=nfech=0;i<maillnodeus.nelem;i++)
      {
	if (maillnodeus.type[i][POSFLUX])   {flux->numf[nfflu]=i;   nfflu++;}
	if (maillnodeus.type[i][POSECH])    {echang->numf[nfech]=i; nfech++;}
      }
  
  free(itrav1); 
  perfo.mem_cond-=maillnodes.npoin*sizeof(int);
  
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
   | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */

void cree_liste_clim_all(struct Maillage maillnodes,struct MaillageBord maillnodebord,  
			 struct MaillageCL maillnodeus,  
			 struct Contact *rescon,struct Clim *rayinf,
			 struct SDparall sdparall)
{
  int i,j,nr,ne,k,n,nb,netot,rc;
  int ndir=0,nra=0;
  int nfres=0,nfrai=0;
  int ok;
  int *ne0,*ne1,*ne2;
  int *itrav1;


  rescon->nelem=rescon->ndmat=0; 
  rayinf->npoin=rayinf->nelem=rayinf->ndmat=0;  


  /* allocation d'un tableau de travail */
  itrav1=(int*)malloc(maillnodes.npoin*sizeof(int));
  verif_alloue_int1d("cree_liste_clim",itrav1);
  for (i=0;i<maillnodes.npoin;i++) itrav1[i]=0;
  perfo.mem_cond+=maillnodes.npoin * sizeof(int);
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;


  /* Traitement des CL de type resistance de contact et rayonnement infini  */
  /* ---------------------------------------------------------------------- */

  for (i=0;i<maillnodes.npoin;itrav1[i]=0,i++);
  ne=maillnodeus.nelem;
  
  for (nfres=nfrai=i=0;i<maillnodeus.nelem;i++)
    {
      if (maillnodeus.type[i][POSRAYINF]) nfrai++;
      if (maillnodeus.type[i][POSRESCON]) nfres++;
    }
  
  rayinf->npoin=0;    rayinf->nelem=nfrai;
  rescon->nelem=nfres; 
  
  if (rescon->nelem){rescon->numf=(int*)malloc(nfres*sizeof(int));verif_alloue_int1d("cree_liste_clim",rescon->numf);
    rescon->ndmat=maillnodeus.ndmat; perfo.mem_cond+=nfres*sizeof(int);}
  
  if (rayinf->nelem){rayinf->numf=(int*)malloc(nfrai*sizeof(int));verif_alloue_int1d("cree_liste_clim",rayinf->numf);
    rayinf->ndmat=maillnodeus.ndmat; perfo.mem_cond+=nfrai*sizeof(int);}
  
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;
  
  for (i=nfrai=nfres=0;i<maillnodeus.nelem;i++)
    {
      if (maillnodeus.type[i][POSRAYINF]) {rayinf->numf[nfrai]=i; nfrai++;}
      if (maillnodeus.type[i][POSRESCON]) {rescon->numf[nfres]=i; nfres++;}
    }
  
  /* indicateur de presence de RC sur au moins 1 domaine */
  if (rescon->nelem)  rescon->existglob=1;  else rescon->existglob=0;
  
#ifdef _SYRTHES_MPI_
  if (syrglob_nparts>1) {
    rc=rescon->existglob;
    MPI_Allreduce(&rc,&(rescon->existglob),1,MPI_INT,MPI_SUM,sdparall.syrthes_comm_world);
  }
#endif


  free(itrav1); 
  perfo.mem_cond-=maillnodes.npoin*sizeof(int);
  
  
}


