# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

class CustomMessageBox(QtGui.QMessageBox):
# suppression dernier argument **kwargs sinon probleme TypeError sous cygwin
#    def __init__(self,*args, **kwargs):
#        QtGui.QMessageBox.__init__(self, *args, **kwargs)
    def __init__(self, *args):
        QtGui.QMessageBox.__init__(self, *args)
        self.setSizeGripEnabled(True)
        pass

    def event(self, e):
        result = QtGui.QMessageBox.event(self, e)

        self.setMinimumHeight(0)
        self.setMaximumHeight(16777215)
        self.setMinimumWidth(0)
        self.setMaximumWidth(16777215)
        self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

        textEdit = self.findChild(QtGui.QTextEdit)
        if textEdit != None :
            textEdit.setMinimumHeight(0)
            textEdit.setMaximumHeight(16777215)
            textEdit.setMinimumWidth(0)
            textEdit.setMaximumWidth(16777215)
            textEdit.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
            pass
        return result
