/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#include "journalDB.h"

#include "cb2bib_utilities.h"

#include <QFile>
#include <QObject>
#include <QTextStream>


journalDB::journalDB(const QString& dbfile)
{
    if (dbfile.isEmpty())
    {
        c2bUtils::warn(QObject::tr("No journal file especified"));
        return;
    }
    _nitems = 0;
    QFile file(dbfile);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        c2bUtils::warn(QObject::tr("Could not open journal file %1 for reading").arg(dbfile));
        return;
    }
    QTextStream stream(&file);
    stream.setCodec("UTF-8");
    stream.setAutoDetectUnicode(true);
    QString line;
    int line_number(0);
    while (!stream.atEnd())
    {
        line = stream.readLine();
        ++line_number;
        if (!(line.isEmpty() || line.startsWith('#')))
        {
            const QStringList spLine(line.split('|'));
            if (spLine.count() != 3)
            {
                c2bUtils::warn(QObject::tr("Syntax error in journal file at line %1").arg(line_number));
                continue;
            }
            _nitems++;
            JCode += spLine.at(0).toLower();
            JAbbrev += spLine.at(1);
            QString dum(spLine.at(1).toLower());
            dum.replace(c2bUtils::nonLetter, " "); // Keeps word structure
            c2bUtils::simplifyString(dum);
            JAbbrev_simp_w += dum;
            dum.remove(' '); // Removes whitespaces also
            JAbbrev_simp += dum;
            JExtended += spLine.at(2);
            dum = spLine.at(2).toLower();
            dum.remove(c2bUtils::nonLetter); // Removes whitespaces also
            JExtended_simp += dum;
        }
    }
    file.close();
}


/** \page journalproc Processing of journal names

     cb2Bib processes journal names according to its editable database,
     stored at <tt>abbreviations.txt</tt>. See \ref c2bconf_files and
     \ref c2bconf_bibtex.

     Journal names processing is performed in the following situations
     whenever a string is recognized as 'journal'. Additionally,
     it is also processed by pressing <b>Intro Key</b> at the journal
     edit line.

     <p>&nbsp;</p>

    - Retrieves Journal name, <b>abbreviated form</b>, if found.

    - If Journal name is not found in the database, returns input Journal name.

    - Search is case insensitive.

    - Warning: Journal codes can be duplicated. If duplicated, returns input
    Journal name.

*/
QString journalDB::retrieve(const QString& JQuery) const
{
    const QString query(JQuery.toLower().remove(c2bUtils::nonLetter)); // Removes whitespaces also
    int journal_found(0);
    int journal_found_at(-1);
    for (int i = 0; i < _nitems; i++)
    {
        if (JCode.at(i) == query)
        {
            journal_found++;
            journal_found_at = i;
        }
        if (JAbbrev_simp.at(i) == query)
            if (++journal_found == 1)
                return(JAbbrev.at(i));
        if (JExtended_simp.at(i) == query)
            if (++journal_found == 1)
                return(JAbbrev.at(i));
    }
    if (journal_found == 1 && journal_found_at != -1)
        return(JAbbrev.at(journal_found_at));
    else
        return(JQuery);
}

/** \page journalproc Processing of journal names
    <p>&nbsp;</p>

    - Retrieves Journal name, <b>full form</b>, if found.

    - If Journal name is not found in the database, returns input Journal name.

    - Search is case insensitive.

    - Warning: Journal codes can be duplicated. If duplicated, returns input
    Journal name.

*/
QString journalDB::retrieveFull(const QString& JQuery) const
{
    QString query(JQuery.toLower());
    query.remove(c2bUtils::nonLetter); // Removes whitespaces also
    int journal_found(0);
    int journal_found_at(-1);
    for (int i = 0; i < _nitems; i++)
    {
        if (JCode.at(i) == query)
        {
            journal_found++;
            journal_found_at = i;
        }
        if (JAbbrev_simp.at(i) == query)
            if (++journal_found == 1)
                return(JExtended.at(i));
        if (JExtended_simp.at(i) == query)
            if (++journal_found == 1)
                return(JExtended.at(i));
    }
    if (journal_found == 1 && journal_found_at != -1)
        return(JExtended.at(journal_found_at));
    else
        return(JQuery);
}
