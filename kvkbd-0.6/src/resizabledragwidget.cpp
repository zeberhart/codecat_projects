/*
 * This file is part of the Kvkbd project.
 * Copyright (C) 2007-2008 Todor Gyumyushev <yodor@developer.bg>
 * Copyright (C) 2008 Guillaume Martres <smarter@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "resizabledragwidget.h"

#include <QPoint>
#include <QPainter>

#include <QMouseEvent>


ResizableDragWidget::ResizableDragWidget(QWidget *parent, Qt::WindowFlags f)
        : DragWidget(parent, f)
{
    toResize = false;

}


ResizableDragWidget::~ResizableDragWidget()
{
}

void ResizableDragWidget::mousePressEvent(QMouseEvent * e)
{
    if (isLocked())
        return;

    QPoint pos = e->pos();
    if (pos.x() > width() - 20 && pos.x() < width() && pos.y() > height() - 20 && pos.y() < height()) {
        toResize = true;
        dragPoint = QPoint(width() - e->pos().x(), height() - e->pos().y());
    } else {
        DragWidget::mousePressEvent(e);
    }
}

void ResizableDragWidget::mouseMoveEvent(QMouseEvent * e)
{
    if (!toResize) {
        DragWidget::mouseMoveEvent(e);
        return;
    }

    setCursor(Qt::SizeFDiagCursor);
    QPoint curr(e->globalPos().x(), e->globalPos().y());
    QPoint pos = QWidget::pos();
    int nw = curr.x() - pos.x() + dragPoint.x();
    int nh = curr.y() - pos.y() + dragPoint.y();

    resize(nw, nh);

}

void ResizableDragWidget::mouseReleaseEvent(QMouseEvent * e)
{
    if (!toResize) {
        DragWidget::mouseReleaseEvent(e);
        return;
    }
    unsetCursor();
    toResize = false;
}

void ResizableDragWidget::paintEvent(QPaintEvent *)
{

    QPainter p(this);

    for (int a = 0;a < 20;a += 5) {

        p.setPen(QColor(170, 0, 0));
        p.drawLine(width() - 20 + a, height() - 2, width() - 2, height() - 20 + a);
        p.setPen(QColor(200, 0, 0));
        p.drawLine(width() - 19 + a, height() - 2, width() - 2, height() - 19 + a);
    }

}


