/*
     This file is part of GNUnet
     (C) 2013 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/identity/gnunet-identity-gtk.h
 * @author Christian Grothoff
 */
#ifndef GNUNET_CONVERSATION_GTK_H
#define GNUNET_CONVERSATION_GTK_H

 /** Get our configuration.
 *
 * @return configuration handle
 **/

extern struct GNUNET_CONFIGURATION_Handle * 
GIG_get_configuration ();


extern GObject *
GNUNET_CONVERSATION_GTK_get_main_window_object (const char *);


/**
 * log a message to gtk log textbuffer
 * @param Message to be logged
 */

extern void
GNUNET_CONVERSATION_GTK_log_message (const char *);

extern void
do_call(const char*);

#endif
