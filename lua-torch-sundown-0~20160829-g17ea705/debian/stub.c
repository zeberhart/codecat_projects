/* This is just a stub, created for making dh_lua happy.
   dh_lua requires luaopen_libsundown in libsundown.so, however
   this library is interfaced to lua with FFI, hence there is no
   such function provided by upstream.
 */
#include <lua.h>
#include <stdio.h>

int
luaopen_libsundown (lua_State *L)
{
  printf("Note, the function luaopen_libsundown is a stub.\n\
Shared library libsundown.so should be used via FFI.\n");
  return 0;
}
