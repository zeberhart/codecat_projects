#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------
#
#                         SYRTHES version 4.X.X.
#                         -------------------
#
#     This file is part of the SYRTHES Kernel, element of the
#     thermal code SYRTHES.
#
#     Copyright (C) 2009 EDF S.A., France
#
#     contact: syrthes-support@edf.fr
#
#
#     The SYRTHES Kernel is free software; you can redistribute it
#     and/or modify it under the terms of the GNU General Public License
#     as published by the Free Software Foundation; either version 2 of
#     the License, or (at your option) any later version.
#
#     The SYRTHES Kernel is distributed in the hope that it will be
#     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#
#     You should have received a copy of the GNU General Public License;
#     if not, write to the
#     Free Software Foundation, Inc.,
#     51 Franklin St, Fifth Floor,
#     Boston, MA  02110-1301  USA
#
#-----------------------------------------------------------------------
#
# To install syrthes, please lunch the commande
#
#    		./syrthes_install.py
#
# this command will install syrthes to the directory
#
#     syrthes4.x.y-N/arch/$NAME_ARCH
# with $NAME_ARCH is your architecture name (uname -a)
#
# this directory contains
#    |- bin      (binaries and profile)
#    |- include  (uic and .h of kernel)
#    |- lib      (internal librairie of syrthes)
#    |- share    (user.c, malefile, syrthes.py, example-data, ...)
#
#-------------------------------------------------------------------------------
# Library modules import
# 
#-------------------------------------------------------------------------------
import sys
import os, re, os.path, shutil
import time, getpass
import platform
from config_env import Setupsyrthesprofile
from config_makeFileIN import Setupmakefilein
#
# verifing python version
if sys.version_info[:2] < (2,3):
    sys.stderr.write("This script needs Python 2.3 at least\n")
    
#
if platform.system == 'Windows':
    sys.stderr.write("This script only works on Unix-like platforms\n")


#-------------------------------------------------------------------------------
# Class definition bellecouleur
#
# Color is beautiful
#-------------------------------------------------------------------------------
class bellecouleur:
#
    ROUGE="\033[31m"
    VERT="\033[32m"
    BLEU="\033[34m"
    GRAS="\033[1m"
    NORMAL="\033[0m"


#-------------------------------------------------------------------------------
# Class definition Setup
# 
#-------------------------------------------------------------------------------
class Setup(Setupmakefilein, Setupsyrthesprofile):
    #
    #----------------------------------------------------------------------------    
    # fonction __init__   
    #----------------------------------------------------------------------------    
    def __init__(self):
        #
        # Init
        #
        Setupsyrthesprofile.__init__(self)
        Setupmakefilein.__init__(self)

        resume = open('resume', mode='w')
        startTime = time.asctime()
        resume.write('Start time: '+startTime+'\n')
        name = ' '.join(os.uname())
        resume.write('Computer name: '+name+'\n')
        user = getpass.getuser()
        resume.write('User name: '+user+'\n')       

        # builds the Lib for SYRTHES       
        self.installFunctions={'blas'        : self.installBLAS,
			       'hdf5'        : self.installHDF,
			       'med'         :  self.installMED,
			       'mpi'         : self.installMPI,
			       'metis'       : self.installMETIS,
			       'scotch'      : self.installSCOTCH,
			       'ple'         : self.installple,
			       'profile'     : self.installprofile,
			       'syrthesseq'  : self.installsyrthesseq,
			       'syrthesconvert': self.installconvert,
			       'syrthesensight': self.installensight,
			       'syrthesmed'  : self.installmed,
			       'syrthesppfunc' : self.installppfunc,
			       'syrthesmpi'  : self.installsyrthesmpi,
			       'syrthespp'   : self.installpp,
			       'syrthespost' : self.installpost,
			       'syrthescfd'  : self.installsyrthescfd}
	# builds the SYRTHES Lib

        # Locate syrthes-install dir-name, extern-libraries/src and extern-libraries/opt 	   
        self.installPath    = os.path.dirname(os.getcwd())+'/'
	self.syrthesDir     = os.path.dirname(os.path.dirname(os.getcwd()))
        self.destPath = self.syrthesDir+'/arch/'

        self.libSourceDir=self.syrthesDir+'/extern-libraries/src'
        self.libDestOpt=self.syrthesDir+'/extern-libraries/opt'
        self.build = self.syrthesDir+'/build'
	
	print ('Creating Destination Directories ')

        if os.path.isdir(self.build): 
            shutil.rmtree(self.build)
	    shutil.os.mkdir(self.build )

        # Creating 'arch' Directories
        if (not os.path.isdir(self.destPath)): 
           shutil.os.mkdir(self.destPath)
	
	self.readSetup()

	print '------------------------------'
	for lib in ['blas','hdf5','med','mpi','metis','scotch']:
		if self.dicolib[lib]['USE'].upper()=='YES':
			if self.dicolib[lib].has_key('PATH'):
				print 'You have chosen to use your own '+lib+' extern libraries'
			elif os.path.isdir(self.libDestOpt) :
              			print 'Extern libraries directory is already existing, continue.'
       			else:
               			shutil.os.mkdir(self.libDestOpt )

		        
	print "-------------------------------------------"
	print "-- Creating destination directory structure:"
	print "-- arch - %s - bin\n --        %s - include\n --        %s - lib\n --        %s - share"%(self.arch, ' '*len(self.arch),' '*len(self.arch),' '*len(self.arch))
	print "-------------------------------------------"

        # Parallele compilation with make -jN
        if self.dicospeci['specific_makej'].has_key('PATH'):
            self.makeopt=self.dicospeci['specific_makej']['PATH']
            print " "
	    print "-------------------------------------------"
	    print "-- Parallele compilation with make"+self.makeopt
	    print "-------------------------------------------"
            print " "
        else:
            self.makeopt=" "

        archPath = self.destPath+self.arch
        if os.path.isdir(archPath) :
               shutil.rmtree(archPath)
               
        os.system('mkdir %s'%archPath )   
        os.chdir(archPath)
        os.system('mkdir bin include lib share')
        os.system('mkdir share/syrthes')
        os.system('mkdir share/syrthes/doc')
        
        # Begin installing libraries
        # Install libraries when  self.dicolib[libraryName]['INSTALL'].upper()=='YES'
	self.install = {}
	listToInstall = self.listLib + self.listSat
	
	for library in listToInstall:
            if (self.dicolib[library].has_key('INSTALL') and self.dicolib[library]['INSTALL'].upper()=='YES'):
                apply(self.installFunctions[library])
            else:
                print '- %syou ve chosen not to install "%s" %s'%(bellecouleur.BLEU,library,bellecouleur.NORMAL)

        # creating profile file syrthes.profile and Makefile, before installing syrthes...
        self.installprofile()
	
        # installing syrthes executable...
        for library in self.listSYR :
            if (self.dicolib[library].has_key('INSTALL') and self.dicolib[library]['INSTALL'].upper()=='YES'):
                apply(self.installFunctions[library])
            else:
                print '- %syou ve chosen not to install "%s" %s'%(bellecouleur.BLEU,library,bellecouleur.NORMAL)
		
        # copying files to destination directories
        self.copyFiles()
        # copying headers in the include folder
        self.copyInclude()
        self.cleaningFiles()

        # installing GUI        
	if (self.dicolib['gui'].has_key('INSTALL') and self.dicolib['gui']['INSTALL'].upper()=='YES'):
            self.installGUI()
        else:
            print '- %syou ve chosen not to install "%s" %s'%(bellecouleur.BLEU,'gui',bellecouleur.NORMAL)	
	
        # make a report of the installation
        os.chdir(self.installPath+'/syrthes-install')
        self.report()            
        endTime = time.asctime()        
        resume.write('End time: '+endTime+'\n')
        resume.write('\n')	
        resume.close()
	 
        # Delete file 
        try:
           os.remove('ERREUR.log')
	except:	
	   print "NO file ERREUR.log" 
	
	try:
	   os.remove(tmp)
        except:
           pass
        ft = open('tmp','w')
        ft.write('                                       \n')
        ft.write(' **************************************\n')
        ft.write(' *         Environment settings       *\n')
        ft.write(' **************************************\n')
        ft.write('                                       \n')
        ft.close()
	os.system("env >> tmp")
        os.system("cat resume tmp > tmp2; mv tmp2 resume; rm tmp")	       

        os.system("grep -i erreur resume > ERREUR.log")
        os.system("grep -i error  resume >> ERREUR.log")
        os.system("grep -i erreur ../*.log >> ERREUR.log")
        os.system("grep -i error  ../*.log >> ERREUR.log")

        vide=open("ERREUR.log",'r')
        lecture = vide.readlines()
        N_lignes = len(lecture)
	vide.close()
	
        # End Message	
        if N_lignes  == 0 :	
	   print ""
	   print "----------------------------------------------------------"
           print "Before using SYRTHES, each user must"+bellecouleur.BLEU+" source"+bellecouleur.NORMAL
	   print "the"+bellecouleur.BLEU+" "+self.syrthesDir+"/arch/"+self.arch+"/bin/syrthes.profile"+bellecouleur.NORMAL+" file before using the code."
	   print ""
	   print "The easiest way is to put the following lines in each of the users"
           print bellecouleur.BLEU+" '.profile'"+bellecouleur.NORMAL+" or "+bellecouleur.BLEU+"'.bashrc'"+bellecouleur.NORMAL+" (depending on the shell)"
           print ""
           print "syrthesprofile="+self.syrthesDir+"/arch/"+self.arch+"/bin/syrthes.profile"
           print "#(adjust path to your system)"
           print "if [ -f $syrthesprofile ] ; then " 
           print "  . $syrthesprofile"
           print "fi"
           print ""
	   print "---------------------------------------------------------"
           print ""	
           print bellecouleur.ROUGE+'Thank you for choosing SYRTHES'+bellecouleur.NORMAL       
        else :
	   print ""
	   print "---------------------------------------------------------"
           print "Before using SYRTHES, each user must"+bellecouleur.BLEU+" source"+bellecouleur.NORMAL
	   print "the"+bellecouleur.BLEU+" "+self.syrthesDir+"/arch/"+self.arch+"/bin/syrthes.profile"+bellecouleur.NORMAL+" file before using the code."
	   print ""
	   print "The easiest way is to put the following lines in each of the users"
           print bellecouleur.BLEU+" '.profile'"+bellecouleur.NORMAL+" or "+bellecouleur.BLEU+"'.bashrc'"+bellecouleur.NORMAL+" (depending on the shell)"
           print ""
           print "syrthesprofile="+self.syrthesDir+"/arch/"+self.arch+"/bin/syrthes.profile"
           print "#(adjust path to your system)"
           print "if [ -f $syrthesprofile ] ; then " 
           print "  . $syrthesprofile"
           print "fi"
           print ""
	   print "---------------------------------------------------------"
	   print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
           print bellecouleur.ROUGE+"Warning : installation of some composants failed"+bellecouleur.NORMAL
	   print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
 	   print "Check log files for more information...."
           print "" 


             


    

    #----------------------------------------------------------------------------    
    # Fonction readSetup
    # read the setup fine to locate installation options		
    # This methode create 3 attributs
    #     self.arch = architecture name (Linux_x86_64, ...)
    #     self.compC = compiler namePath : gcc ou /home/..../gcc
    #     self.wrapperc = compiler namePath : mpicc 
    #     self.dicolib = dictionnay of lib installing and using options
    #                  = {'med' : {'USE'='YES', 'INSTALL'='YES'   },
    #                     'hdf' : {'USE'='YES', 'INSTALL'='NO'(Default), PATH='/../syrthes4.0.0-6/extern-libraries/opt/hdf5-1.6.7/arch/Linux_x86_64'},
    #                      ...}
    #
    #----------------------------------------------------------------------------
    def readSetup(self):
        #
        # Init
        #
	print "Reading setup file"
        self.dicolib = {}
        self.dicospeci = {} 
         
        self.libraries = []
	# Graphical user Interface
	self.listGui = ['gui']
        # Archtecture and C compilateur
        self.listDef = ['specific_inc','specific_lib','specific_debug','specific_option', 'specific_makej']
        # Lib for SYRTHES BLAS, METIS, SCOTCH, MPI, MED, HDF
        self.listLib = ['blas','hdf5','med','mpi','metis','scotch']
        # Lib for coupling with Code_Saturne
        self.listSat = ['ple']	
	# Lib SYRTHES sequential, tool,user functions, parallele, preprocessor, post, syrthescfd
        self.listSYR = ['syrthesconvert','syrthesensight','syrthesmed', 'syrthesppfunc', 'syrthespp', 'syrthespost', 'syrthesseq', 'syrthesmpi', 'syrthescfd']

        list_lib = self.listGui + self.listLib + self.listSat + self.listSYR
        for ele in list_lib:
            self.dicolib[ele] = {}
        list_lib = self.listDef    
        for ele in list_lib:    
            self.dicospeci[ele] = {}
        #
        try:
            setupFile = file('setup.ini', mode='r')
        except IOError:
            print 'Error : opening setup file'
            sys.exit(1)
        #
        print '\n--------------------------------------------------'        
        print '%s          Installation of SYRTHES %s'%(bellecouleur.BLEU,bellecouleur.NORMAL)
        print '--------------------------------------------------'
        #
        self.comp = 'gcc'
        self.compcx = 'g++'
        self.wrapperc = 'mpicc'
        
        lines = setupFile.readlines()
        for line in lines:
	## Comment in file "setup"
	    if (line[0] == '#' or len(line[0])==0) :
	        pass
            else:
		# splitlines necessary to get rid of carriage return on IRIX64
		line = line.splitlines()

                listopt = line[0].split("=")
                list = line[0].split()
                list2 = line[0].split(None,1)

                # options specific case: there may be several on line
                if listopt[0] in ['specific_inc', 'specific_lib', 'specific_debug', 'specific_option', 'specific_makej'] :
                    # Specifique INCLUDE for C compiler in file "setup" 
                    # rajouter une cle dicolib avec Use = YESn INSTALL=NO, PATH=list(1)
                    self.dicospeci[listopt[0]]['PATH'] = listopt[1]
                    self.dicospeci[listopt[0]]['USE'] = 'YES'
		    
		    # Screen out Begin 
		    print "     Specific          : %s    %s  %s                  "     %(bellecouleur.VERT,listopt[1],bellecouleur.NORMAL)
                    #-------------------------------------------------------------------------------
                    # Screen out End
		    
		# Architecture Name in file "setup"
		elif list[0]=='nom_arch' :
		    try:
			self.arch = list[1]
		    except:
			arch = os.uname()[0]
			if arch=='Linux' :
			    if os.uname()[4]== 'x86_64' :
				arch = arch+'_'+'x86_64'
			    elif os.uname()[4]== 'ia64' :
				arch = arch+'_'+'IA64'
                            else :
                                arch = arch+'_'+os.uname()[4]    
			self.arch = arch
			
		    # Screen out Begin    
		    print "     Architecture Name : %s    %s  %s                  "     %(bellecouleur.VERT,self.arch,bellecouleur.NORMAL)
                    #-------------------------------------------------------------------------------
 		    # Screen out End
		
		# C compiler in file "setup"            
		elif list[0]=='compC' :
                    if len(list)!=1:
                        name = list[1]
		    else:
                        name = 'gcc'
		    self.comp = name
		    
		    # Screen out Begin                        
		    print "     C compiler        : %s   %s  %s                   "     %(bellecouleur.VERT,self.comp,bellecouleur.NORMAL)
                    #-------------------------------------------------------------------------------
 		    # Screen out End

		# C++ compiler in file "setup"            
		elif list[0]=='compCX' :
                    if len(list)!=1:
                        name = list[1]
		    else:
                        name = 'g++'
		    self.compcx = name
		    
		    # Screen out Begin                         
		    print "     C++ compiler      : %s   %s  %s                   "     %(bellecouleur.VERT,self.comp,bellecouleur.NORMAL)
                    #-------------------------------------------------------------------------------
 		    # Screen out End
		    
		# C++ compiler in file "setup"            
		elif list[0]=='wrapperC' :
                    if len(list)!=1:
                        name = list[1]
		    else:
                        name = 'mpicc'
		    self.wrapperc = name
		    
		    # Screen out Begin                         
		    print "     C wrapper         : %s   %s  %s                   "     %(bellecouleur.VERT,self.comp,bellecouleur.NORMAL)
                    #-------------------------------------------------------------------------------
 		    # Screen out End
		    
		# Extern Lib in file "setup"
                # find keys in file
		else :
                    name =  list[0]
                    list = list[1:]
                    for l in list:
                       [key,val] = l.split('=')
                       self.dicolib[name][key] = val
		        
        # Screen out Begin	     
	print "-----------------------------------------------------------"    
	print "   externe libraries :                                   -"
	print "-----------------------------------------------------------"    
        for key in self.listLib:
	    print " %s %s %s:"%(bellecouleur.VERT,key, bellecouleur.NORMAL)
	    for k in self.dicolib[key]:
	        print "    %s %s %s : %s" %(k, bellecouleur.VERT, bellecouleur.NORMAL, self.dicolib[key][k])
        # Screen out End




    #----------------------------------------------------------------------------    
    # Install of METIS
    # - unzip    
    # Warning for  install metis-5.0.2.tar.gz, it's necessary :
    # - make all 
    #----------------------------------------------------------------------------
    def installMETIS(self):
        #
        #
	nameLib, buildPath, optPath, srcPath = self.unzipLib('metis')
	
	self.printResume('metis')
        
        if (os.path.isdir(optPath+'/arch/'+self.arch)) :
            shutil.rmtree(optPath+'/arch/'+self.arch)

	error = os.system("make config prefix="+optPath+'/arch/'+self.arch+" && make "+self.makeopt+" && make install 2>&1 | tee "+nameLib+".log")


        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)

        # Clean the lib
        os.system('make clean')

 	# cleaning source directory
	shutil.rmtree(self.libSourceDir+'/TMP/'+nameLib)
        
        print 'metisINSTALL', error
        self.dicolib['metis']['PATH'] = optPath+'/arch/'+self.arch
        self.dicolib['metis']['INSTALL'] = 'OK'
        os.chdir(self.installPath)




    #---------------------------------------------------------------------------- 
    # Install of SCOTCH
    # - unzip     
    # - Use Make.inc/Makefile.inc.x86-64_pc_linux2
    # - make scotch
    # - make install
    #----------------------------------------------------------------------------
    def installSCOTCH(self):
        #
        #
	nameLib, buildPath, optPath, srcPath = self.unzipLib('scotch')
	
	self.printResume('scotch')

        os.chdir(srcPath+nameLib+'/src')

        shutil.copy('Make.inc/Makefile.inc.x86-64_pc_linux2','Makefile.inc')
        
        # Pour scotch6   
        #modif=file("./Makefile.inc","r").read().replace("LDFLAGS		= -lz -lm -lrt","LDFLAGS		= -lz -lm -lrt -lpthread")
        #file("./Makefile.inc","w").write(modif)

        if os.path.exists(optPath) == False:
            os.mkdir(optPath, 0755)
            os.mkdir(optPath+'/arch', 0755)
            os.mkdir(optPath+'/arch/'+self.arch, 0755)
        elif os.path.exists(optPath+'/arch/'+self.arch) == False:
            os.mkdir(optPath+'/arch/'+self.arch, 0755)
        elif os.path.exists(optPath+'/arch/'+self.arch) == True:
            shutil.rmtree(optPath+'/arch/'+self.arch)
            os.mkdir(optPath+'/arch/'+self.arch, 0755)

	error = os.system("make scotch && make "+self.makeopt+" install prefix="+optPath+'/arch/'+self.arch+" 2>&1 | tee "+nameLib+".log")

        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)

        # Clean the lib
        os.system('make clean')

	# cleaning source directory
	shutil.rmtree(self.libSourceDir+'/TMP/'+nameLib)
        
        print 'scotchINSTALL', error
        self.dicolib['scotch']['PATH'] = optPath+'/arch/'+self.arch
        self.dicolib['scotch']['INSTALL'] = 'OK'
        os.chdir(self.installPath)




    #----------------------------------------------------------------------------    
    # Install of  OPENMPI
    # - unzip
    # - make dir 'openx.y.z.built'
    # - Since openx.y.z.built dir, commande : configure --prefix + option 
    #----------------------------------------------------------------------------     
    def installMPI(self):
        #
        #
        nameLib, buildPath, optPath, srcPath = self.unzipLib('mpi')
	self.printResume('mpi')
	
	print "     "
	print "     "
	print "     "
	print " - debug--------------------------------------------------------------     "			
	print buildPath+'/configure --prefix='+optPath+'/arch/'+self.arch+' CC=\"'+self.comp+'\"'+' CXX=\"'+self.compcx+'\"'
	print " - debug--------------------------------------------------------------     "	
	print "     "
	print "     "
	print "     "	
		
	error = os.system(srcPath+nameLib+'/configure --prefix='+optPath+'/arch/'+self.arch +
                          ' CC=\"'+self.comp+'\"'+' CXX=\"'+self.compcx+'\"')
			  						  
        if error != 0 :
            print "Error during configure of "+nameLib
            sys.exit(1)

        error = os.system('make '+self.makeopt+' all')
        error = os.system('make install')

        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)
                
        os.system('make clean')
	 	
	# cleaning source directory
	shutil.rmtree(self.libSourceDir+'/TMP/'+nameLib)

	print 'MPIINSTALL', error

        self.dicolib['mpi']['PATH'] = optPath+'/arch/'+self.arch
        self.dicolib['mpi']['INSTALL'] = 'OK'
        os.chdir(self.installPath)




    #----------------------------------------------------------------------------    
    # Install of HDF5
    # - unzip
    # - make dir 'hdf5x.y.z.built'
    # - since hdf5x.y.z.built dir, commande : configure --prefix + option 
    # - since hdf5x.y.z.built dir, commande : make
    # - since hdf5x.y.z.built dir, commande : make install     
    #----------------------------------------------------------------------------
    def installHDF(self):
        #
        #
	nameLib, buildPath, optPath, srcPath = self.unzipLib('hdf')
	self.printResume('hdf')
        
        error = os.system(srcPath+nameLib+'/configure --prefix='+optPath+'/arch/'+self.arch+' CC=\"'+self.comp+'\"')	
	
	     		    
        error = os.system('make')
        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)
        error = os.system('make '+self.makeopt+' install')
        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)
	os.system('make clean')
        	
	# cleaning source directory
	shutil.rmtree(self.libSourceDir+'/TMP/'+nameLib)	
	
	print 'HDF5INSTALL', error

        self.dicolib['hdf5']['PATH'] = optPath+'/arch/'+self.arch
        self.dicolib['hdf5']['INSTALL'] = 'OK'
        os.chdir(self.installPath)
	



    #----------------------------------------------------------------------------    
    # Install of MED (*** need  Lib  HDF5 ***)
    # - unzip
    # - make dir 'medx.y.z.built'
    # - since medx.y.z.built dir, commande : configure --prefix + option 
    # - since medx.y.z.built dir, commande : make
    # - since medx.y.z.built dir, commande : make install      
    #----------------------------------------------------------------------------
    def installMED(self):
        #MED Installing needs HDF5INSTALL
        if self.dicolib['hdf5'].has_key('PATH'):
            hdfPath = self.dicolib['hdf5']['PATH']
        else:
            self.installHDF()
            self.dicolib['hdf5']['INSTALL']='NO'
            hdfPath = self.dicolib['hdf5']['PATH']

        withHdf = ''
	if self.dicolib['hdf5'].has_key('USE') and self.dicolib['hdf5']['USE'].upper()=='YES':
            withHdf = ' --with-hdf5='+hdfPath  
	#
	nameLib, buildPath, optPath, srcPath = self.unzipLib('med')
	self.printResume('med')
        	
        ## configure commande See 
	error = os.system(srcPath+nameLib+'/configure --prefix='+optPath+'/arch/'+self.arch+withHdf+' CC=\"'+self.comp+'\"')	
		
	error = os.system('make '+self.makeopt+' install')
        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)
                
        os.system('make clean')
        print 'MEDINSTALL', error
        self.dicolib['med']['PATH'] = optPath+'/arch/'+self.arch
        self.dicolib['med']['INSTALL'] = 'OK'

        os.chdir(self.installPath)
        



    #----------------------------------------------------------------------------    
    # Install of BLAS
    #    
    #----------------------------------------------------------------------------
    def installBLAS(self):
        #
        #
	nameLib, buildPath, optPath, srcPath = self.unzipLib('blas')
	self.printResume('blas')
	
        error = os.system(srcPath+'/configure --prefix='+optPath+'/arch/'+self.arch+' -b 32')
        if error != 0 :
            print "Error during configure of "+nameLib
            sys.exit(1)

        error = os.system('make install')
        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)

        print 'blasINSTALL', error

        self.dicolib['blas']['PATH'] = optPath+'/arch/'+self.arch
        self.dicolib['blas']['INSTALL'] = 'OK'
        os.chdir(self.installPath)
	



    #----------------------------------------------------------------------------    
    # Definition of variables for sythes.profile
    #    
    #----------------------------------------------------------------------------
    def installprofile(self):
        #
        os.chdir(self.installPath)
        fileProfileName = '/syrthes.profile'
        destPath = self.destPath+self.arch+'/bin'
	#
        nameLib = 'profile' 
        self.printResume(nameLib)
	
	Setupsyrthesprofile.configuration(self,self)	
        shutil.move(self.installPath+fileProfileName+'_'+self.arch, destPath+fileProfileName)	
        os.chdir(destPath)
        os.system('chmod +x '+destPath+fileProfileName)
        print "source "+destPath+fileProfileName	    	    
	os.environ['SYRTHES4_HOME']=self.syrthesDir
		
        os.chdir(self.installPath)		
        print "Configure file Makefile.in for installation"
        Setupmakefilein.configuration_make(self,self)
        #	
        os.chdir(self.installPath+'/syrthes-install')
	   	



    #----------------------------------------------------------------------------    
    # Install of SYRTHES sequential library
    #    
    #----------------------------------------------------------------------------
    def installsyrthesseq(self):
        nameLib = 'syrthesseq'
        print '\nInstallation of  '+nameLib+'  on architecture '+self.arch+'\n'
	#	
        destPath=self.syrthesDir+'/arch/'+self.arch+'/lib'

        try:
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     
	     os.system("echo '*                 Installation syrthesseq         *' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     	     
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.chdir(self.installPath)	   	     	         
	     os.system("cat "+nameLib+".log >>"+self.installPath+"/syrthes-install/resume")
        except:
	     print "Error during make of "+nameLib
	     
        #destPath
        print("make syrthesseq  NOM_ARCH="+self.arch+" SYRTHES4_HOME='"+self.syrthesDir+"' 2>&1 | tee "+nameLib+".log")
        error = os.system("make "+self.makeopt+" syrthesseq  NOM_ARCH="+self.arch+" SYRTHES4_HOME="+self.syrthesDir+" 2>&1 | tee "+nameLib+".log")
        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)
	
        print 'syrthesseqINSTALL', error
        os.system('mv %s/* %s'%(self.build,destPath))
        
        os.chdir(self.installPath)




    #----------------------------------------------------------------------------    
    # Install of SYRTHES convert
    #    
    #----------------------------------------------------------------------------
    def installconvert(self):
        #
        nameLib = 'syrthesconvert'
        #
        print '\n %s Installation of convert2syrthes4 on architecture %s %s'%(bellecouleur.VERT,self.arch,bellecouleur.NORMAL)
	#
        
        try:
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     
	     os.system("echo '*                 Installation convert              *' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     	     
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.chdir(self.installPath)	         
	     os.system("cat "+nameLib+".log >>"+self.installPath+"/syrthes-install/resume")
        except:
	     print "Error during make of "+nameLib
	     
        error = os.system("make "+self.makeopt+" syrthesconvert NOM_ARCH="+self.arch+" SYRTHES4_HOME="+self.syrthesDir+" 2>&1 | tee "+nameLib+".log")
        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)
	
        print 'syrthesconvert INSTALL', error
                
        os.chdir(self.installPath)
	



    #----------------------------------------------------------------------------    
    # Install of SYRTHES ensight
    #    
    #----------------------------------------------------------------------------
    def installensight(self):
        #
        nameLib = 'syrthesensight'
        #
        print '\n %s Installation of syrthes4ensight on architecture %s %s'%(bellecouleur.VERT,self.arch,bellecouleur.NORMAL)
	#
        
        try:
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     
	     os.system("echo '*                 Installation ensight            *' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     	     
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.chdir(self.installPath)	         
	     os.system("cat "+nameLib+".log >>"+self.installPath+"/syrthes-install/resume")
        except:
	     print "Error during make of "+nameLib
	     
        error = os.system("make "+self.makeopt+" syrthesensight NOM_ARCH="+self.arch+" SYRTHES4_HOME="+self.syrthesDir+" 2>&1 | tee "+nameLib+".log")
        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)
	
        print 'syrthesensight INSTALL', error
                
        os.chdir(self.installPath)
	



    #----------------------------------------------------------------------------    
    # Install of SYRTHES syrthes4med
    #    
    #----------------------------------------------------------------------------
    def installmed(self):
        #
        nameLib = 'syrthesmed'
        #
        print '\n %s Installation of syrthes4med3 on architecture %s %s'%(bellecouleur.VERT,self.arch,bellecouleur.NORMAL)
	#
        
        try:
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     
	     os.system("echo '*                 Installation med                *' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     	     
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.chdir(self.installPath)	     	     
	     os.system("cat "+nameLib+".log >>"+self.installPath+"/syrthes-install/resume")
        except:
	     print "Error during make of "+nameLib
	     
        error = os.system("make "+self.makeopt+" syrthesmed NOM_ARCH="+self.arch+" SYRTHES4_HOME="+self.syrthesDir+" 2>&1 | tee "+nameLib+".log")
        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)
	
        print 'syrthesmed INSTALL', error
                
        os.chdir(self.installPath)
	



    #----------------------------------------------------------------------------    
    # Install of SYRTHES user functions interpretor syrthes-ppfunc
    #    
    #----------------------------------------------------------------------------
    def installppfunc(self):
        #
        nameLib = 'syrthesppfunc'
        print '\n %s Installation of ppfunc on architecture %s %s'%(bellecouleur.VERT,self.arch,bellecouleur.NORMAL)
	#
        
        try:
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     
	     os.system("echo '*                 Installation   ppfunc           *' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     	     
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.chdir(self.installPath)	     	     
	     os.system("cat "+nameLib+".log >>"+self.installPath+"/syrthes-install/resume")
        except:
	     print "Error during make of "+nameLib
	     
        error = os.system("make "+self.makeopt+" ppfunc NOM_ARCH="+self.arch+" SYRTHES4_HOME="+self.syrthesDir+" 2>&1 | tee "+nameLib+".log")
        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)
	
        print 'syrthesppfuncINSTALL', error
               
        os.chdir(self.installPath)




    #----------------------------------------------------------------------------    
    # Install of SYRTHES MPI library
    #    
    #----------------------------------------------------------------------------
    def installsyrthesmpi(self):
        #
        print '\n %s Installation of syrthesmpi on architecture %s %s'%(bellecouleur.VERT,self.arch,bellecouleur.NORMAL)
	#
        nameLib = 'syrthesmpi'
        destPath=self.installPath+'/../arch/'+self.arch+'/lib'

        try:
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     
	     os.system("echo '*                 Installation syrthesmpi         *' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     	     
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.chdir(self.installPath)	     
	     os.system("make "+self.makeopt+" syrthesmpi NOM_ARCH="+self.arch+" SYRTHES4_HOME="+self.syrthesDir+" 2>&1 | tee "+nameLib+".log")	     
	     os.system("cat "+nameLib+".log >>"+self.installPath+"/syrthes-install/resume")
        except:
	     print "Error during make of "+nameLib
	     
        os.chdir(self.installPath)

        os.system('mv %s/* %s'%(self.build,destPath))




    #----------------------------------------------------------------------------    
    # Install of SYRTHES preprocessor syrthes-pp
    # (only required for MPI computations)
    #    
    #----------------------------------------------------------------------------
    def installpp(self):
        #
        #
        nameLib = 'pp'
        print '\n %s Installation of syrthes-pp on architecture %s %s'%(bellecouleur.VERT,self.arch,bellecouleur.NORMAL)
	#
        destPath=self.installPath+'/../arch/'+self.arch+'/lib'

        try:
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     
	     os.system("echo '*                 Installation   pp               *' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     	     
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.chdir(self.installPath)
	     os.system("cat "+nameLib+".log >>"+self.installPath+"/syrthes-install/resume") 	                 
        except:
	     print "Error during make of "+nameLib
             sys.exit(1)

        error = os.system("make "+self.makeopt+" pp NOM_ARCH="+self.arch+" SYRTHES4_HOME="+self.syrthesDir+" 2>&1 | tee "+nameLib+".log")

        if error != 0 :
            print "Error during make install of "+nameLib
            sys.exit(1)
	
        print 'syrthesppINSTALL', error
        os.system('mv %s/* %s'%(self.build,destPath))
	



    #----------------------------------------------------------------------------    
    # Install of SYRTHES postprocessor syrthes-post
    #    
    #----------------------------------------------------------------------------
    def installpost(self):
        #
        print '\n %s Installation of syrthes-post on architecture %s %s'%(bellecouleur.VERT,self.arch,bellecouleur.NORMAL)
	#
        nameLib = 'post' 
        destPath=self.installPath+'/../arch/'+self.arch+'/lib'

        try:
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     
	     os.system("echo '*                 Installation   post             *' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     	     
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.chdir(self.installPath)
	     os.system("make "+self.makeopt+" post NOM_ARCH="+self.arch+" SYRTHES4_HOME="+self.syrthesDir+" 2>&1 | tee "+nameLib+".log")
	     os.system("cat "+nameLib+".log >>"+self.installPath+"/syrthes-install/resume")
        except:
	     print "Error during make of "+nameLib
	     
        os.chdir(self.installPath)
        os.system('mv %s/* %s'%(self.build,destPath))




    #----------------------------------------------------------------------------    
    # Definition of variables for ple
    #    
    #----------------------------------------------------------------------------
    def installple(self):
        #    
        print '\n %s Installation of ple on architecture %s %s'%(bellecouleur.VERT,self.arch,bellecouleur.NORMAL)
	#
        nameLib = 'ple'

        os.system("echo '						    ' >> "+self.installPath+"/syrthes-install/resume")
        os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	
        os.system("echo '*		   Installation ple                *' >> "+self.installPath+"/syrthes-install/resume")
        os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")		
        os.system("echo '						    ' >> "+self.installPath+"/syrthes-install/resume")




    #----------------------------------------------------------------------------    
    # Install of SYRTHES CFD library
    #    
    #----------------------------------------------------------------------------
    def installsyrthescfd(self):
        #
        print '\n %s Installation of syrthescfd on architecture %s %s'%(bellecouleur.VERT,self.arch,bellecouleur.NORMAL)
	#
        nameLib = 'syrthescfd'

        try:
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     
	     os.system("echo '*                 Installation syrthescfd         *' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '***************************************************' >> "+self.installPath+"/syrthes-install/resume")	     	     
	     os.system("echo '                                                   ' >> "+self.installPath+"/syrthes-install/resume")
	     os.chdir(self.installPath)	     
	     os.system("make syrthescfd NOM_ARCH="+self.arch+" SYRTHES4_HOME="+self.syrthesDir+" 2>&1 | tee "+nameLib+".log")	     
	     os.system("cat "+nameLib+".log >>"+self.installPath+"/syrthes-install/resume")
        except:
	     print "Error during make of "+nameLib
	     
        os.chdir(self.installPath+'/syrthes-install')




    #----------------------------------------------------------------------------    
    #  Fonction report   
    #----------------------------------------------------------------------------
    def report(self):
        #
        # setup file update
        #
        print 'function report'
	#
        setupFile = file('setup', mode='w')
        
	# Report for definition of architecture
        setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('# Architecture Name\n')
        setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('#nom_arch  Linux_x86_64    <- for specific architecture\n')	
        setupFile.write('#nom_arch                  <- leave empty for automatic default\n')
        setupFile.write('#                             value (based on uname)\n')
        setupFile.write('nom_arch '+self.arch+'\n#\n')
	setupFile.write('#\n')

        # Report for definition of C compiler
	setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('# C compiler                                             \n')
        setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('#compC /home/toto/gcc-x.y.z/gcc <- for specific the  \n')
        setupFile.write('#                                  compiler\n')	
        setupFile.write('#compC  gcc                     <- leave gcc for \n')
        setupFile.write('#                                  automatic default\n')		
        setupFile.write('compC'+'   '+self.comp+"\n")
	setupFile.write('#\n')     	
	setupFile.write('#wrapperC  mpicc                 <- for specific C wrapper without path\n')
	setupFile.write('#                                   (only needed for MPI installation)\n')
	setupFile.write('#                                   leave mpicc for automatic default\n')
        setupFile.write('wrapperC'+'  '+self.wrapperc+"\n")
	setupFile.write('#\n')     	
        setupFile.write('# Specific options for compiler  \n')  
        setupFile.write('# specific_inc= specific_inc= -I /myinclude/inc -I /myinclude2/inc\n')
        setupFile.write('# specific_lib= -L /mylib/lib1 -L /mylib/lib2 \n')	
        setupFile.write('# specific_debug= -g\n')
        setupFile.write('# specific_option= -D _SYRTHES_MPI_IO_\n')
        setupFile.write('# specific_makej= -j8\n')
		        
        liblist=['specific_inc','specific_lib','specific_debug','specific_option', 'specific_makej']
	for lib in liblist:
		if  self.dicospeci[lib].has_key('PATH'):	
		  setupFile.write(lib+'  '+self.dicospeci[lib]['PATH']+"\n")                 
		else:
		  setupFile.write('# \n')			     
	      
        setupFile.write('#\n')
	setupFile.write('#\n')
	setupFile.write('#\n')
	
	# Report for Graphical user Interface Intallation
	setupFile.write('#--------------------------------------------------------\n')
	setupFile.write('# Graphical user Interface\n')
	setupFile.write('#--------------------------------------------------------\n')
        lib='gui'	
        setupFile.write(lib+'    '+'INSTALL='+self.dicolib[lib]['INSTALL']+"\n")
	setupFile.write('#\n')
	        
	# Report for builds the SYRTHES sequential library
	setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('# SYRTHES4 - kernel - sequential version\n')
        setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('# Answer by yes or no\n#\n')
	lib='syrthesppfunc'
       	if  self.dicolib[lib].has_key('PATH'):	
            setupFile.write(lib+' '+'INSTALL='+self.dicolib[lib]['INSTALL']+"\n")
	lib='syrthesseq'
       	if  self.dicolib[lib].has_key('PATH'):	
            setupFile.write(lib+'    '+'INSTALL='+self.dicolib[lib]['INSTALL']+"\n")
	setupFile.write('#\n')
	setupFile.write('#\n')
	
	# Report for builds the SYRTHES tools
	setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('# SYRTHES4 - tools for meshes and results conversions\n')
        setupFile.write('#--------------------------------------------------------\n')
	setupFile.write('# Choose your type of installation and answer by yes or no\n')
	setupFile.write('#\n')
	setupFile.write('# for MED format conversion : choose to istall or the path to the libraries\n')
        lib='hdf5'
	if self.dicolib[lib].has_key('PATH') :
		setupFile.write('#\n#\n')
		setupFile.write(lib+'   USE=yes  PATH='+self.dicolib[lib]['PATH']+"\n")
		setupFile.write('#\n#\n')
	lib='med'
	if self.dicolib[lib].has_key('PATH') :
		setupFile.write('#\n#\n')
		setupFile.write(lib+'   USE=yes  PATH='+self.dicolib[lib]['PATH']+"\n")
		setupFile.write('#\n#\n')

		
	lib='syrthesconvert'
        setupFile.write(lib+'    '+'INSTALL='+self.dicolib[lib]['INSTALL']+"\n")
	setupFile.write('#\n')

	lib='syrthesensight'
        setupFile.write(lib+'    '+'INSTALL='+self.dicolib[lib]['INSTALL']+"\n")
	setupFile.write('#\n')

	lib='syrthesmed'
        setupFile.write(lib+'    '+'INSTALL='+self.dicolib[lib]['INSTALL']+"\n")
	setupFile.write('#\n')

	# Report for builds the SYRTHES parall version
	setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('# SYRTHES4 - parall version (optional)\n')
        setupFile.write('#--------------------------------------------------------\n')

        for lib in ['mpi', 'metis', 'scotch']:
            if self.dicolib[lib].has_key('PATH') :
                setupFile.write('#\n#\n')
                setupFile.write(lib+'   USE=yes  PATH='+self.dicolib[lib]['PATH']+"\n")
                setupFile.write('#\n#\n')
	
	lib='syrthespp'
        setupFile.write(lib+'    '+'INSTALL='+self.dicolib[lib]['INSTALL']+"\n")
	setupFile.write('#\n')
	lib='syrthesmpi'
        setupFile.write(lib+'    '+'INSTALL='+self.dicolib[lib]['INSTALL']+"\n")
	setupFile.write('#\n')
	lib='syrthespost'
        setupFile.write(lib+'    '+'INSTALL='+self.dicolib[lib]['INSTALL']+"\n")
	setupFile.write('#\n')
        
	# Report for builds the SYRTHES optional libraiires
	setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('# Optional extern libraries\n')
        setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('#\n')
	setupFile.write('# BLAS Linear algebra library\n')
	setupFile.write('blas  USE=no\n')
	setupFile.write('#blas USE=yes INSTALL=yes\n')
	setupFile.write('#blas USE=yes PATH=/home/...\n')
	setupFile.write('#\n')
		
	
	# Report for builds the SYRTHES preprocessor syrthes-pp
	setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('# SYRTHES for coupling with Code-Saturne (optional)\n')
        setupFile.write('#--------------------------------------------------------\n')
        setupFile.write('# Warning : MPI installation must be defined before\n')
	setupFile.write('#\n')
	lib='ple'
	if self.dicolib[lib].has_key('PATH') :
		setupFile.write('#\n#\n')
		setupFile.write(lib+'   USE=yes  PATH='+self.dicolib[lib]['PATH']+"\n")
		setupFile.write('#\n#\n')
	
	lib='syrthescfd'
        setupFile.write(lib+'    '+'INSTALL='+self.dicolib[lib]['INSTALL']+"\n")
	setupFile.write('#\n')

        setupFile.close()
	


	
    #----------------------------------------------------------------------------
    # Function to find a lib partial name (extension : tar.gz) in a path
    #----------------------------------------------------------------------------
    def findLib(self,srcPath,lib):
        fileList=os.listdir(srcPath)
        nameLib = [f for f in fileList if f.find(lib) > -1][0].split('.tar')[0]
        return nameLib



	
    #----------------------------------------------------------------------------    
    # Function Copy of all headers in the include folder
    #----------------------------------------------------------------------------     
    def copyInclude(self):
                     
            files = os.listdir(self.installPath+'/syrthes-kernel/include/') 
            for name in files :
                 shutil.copy(self.installPath+'/syrthes-kernel/include/'+name,self.destPath+self.arch+'/include' )
	    
            files = os.listdir(self.installPath+'/syrthes-kernel/lib_material_syrthes/') 
            for name in files : 
                 shutil.copy(self.installPath+'/syrthes-kernel/lib_material_syrthes/'+name,self.destPath+self.arch+'/include' )



            
    #----------------------------------------------------------------------------
    # Function Cleaning of folders and temp directory.
    #----------------------------------------------------------------------------    
    def cleaningFiles(self):
	# cleaning .o files in arch/.../lib
	files = os.listdir(self.destPath+self.arch+'/lib/')
        for name in files:
            basename, extension = os.path.splitext(name)
            if extension=='.o':
               os.remove(self.destPath+self.arch+'/lib/'+name)
        # cleaning directories
	if os.path.isdir(self.syrthesDir+'/build') :
            shutil.rmtree(self.syrthesDir+'/build')
	if os.path.isdir(self.libSourceDir+'/TMP') :
            shutil.rmtree(self.libSourceDir+'/TMP')	
        



    #----------------------------------------------------------------------------
    # Function Copy of all working files (includes, share)
    #----------------------------------------------------------------------------    
    def copyFiles(self):
        # copy syrthes4_create_case file        
        shutil.copy(self.installPath+'/data/syrthes4_create_case'     , self.destPath+self.arch+'/bin')
        shutil.copy(self.installPath+'/data/syrthes.py'               , self.destPath+self.arch+'/share/syrthes')
        shutil.copy(self.installPath+'/data/Makefile'                 , self.destPath+self.arch+'/share/syrthes')
        shutil.copy(self.installPath+'/data/syrthes_data.syd_example' , self.destPath+self.arch+'/share/syrthes')
        shutil.copy(self.installPath+'/syrthes-kernel/src/user.c'     , self.destPath+self.arch+'/share/syrthes')
        shutil.copy(self.installPath+'/syrthes-kernel/src/user_cond.c', self.destPath+self.arch+'/share/syrthes')
        shutil.copy(self.installPath+'/syrthes-kernel/src/user_ray.c' , self.destPath+self.arch+'/share/syrthes')
        shutil.copy(self.installPath+'/syrthes-kernel/src/user_hmt.c' , self.destPath+self.arch+'/share/syrthes')
        os.system('cp '+self.installPath+'/doc/*.pdf  '+self.destPath+self.arch+'/share/syrthes/doc')
#        shutil.copy(self.installPath+'/doc/*.pdf', self.destPath+self.arch+'/share/syrthes/doc')
        shutil.copy(self.installPath+'/Makefile2.in'                  , self.destPath+self.arch+'/share/syrthes/Makefile.in')
        shutil.copytree(self.installPath+'/tests'                     , self.destPath+self.arch+'/share/syrthes/tests')
#
        shutil.copy(self.installPath+'/data/Makefile_mylibmat'        , self.destPath+self.arch+'/share/syrthes')
        shutil.copytree(self.installPath+'/syrthes-kernel/mylibmat_src' , self.destPath+self.arch+'/share/syrthes/mylibmat_src')
        shutil.copy(self.installPath+'/data/syrthes4_create_mylibmat'     , self.destPath+self.arch+'/bin')
        shutil.copy(self.installPath+'/syrthes-gui/src/22x22/syr_hmt_material.txt'     , self.destPath+self.arch+'/include')
        pass
	
    #----------------------------------------------------------------------------	
    # Function to find and unzip librairie
    #----------------------------------------------------------------------------    
    def unzipLib(self,myLibName):
	nameLib = self.findLib(self.libSourceDir,myLibName)
        print '\nInstallation of %s %s %s on architecture %s\n'%(bellecouleur.VERT,nameLib,bellecouleur.NORMAL, self.arch)
        buildPath =self.libSourceDir+'/TMP/'+nameLib+'.build/'+self.arch
        optPath = self.libDestOpt+'/'+nameLib
	
	# Unzip
	print bellecouleur.BLEU +"Unzip "+ nameLib + bellecouleur.NORMAL
        os.chdir(self.libSourceDir+'/')
        if os.path.isdir('TMP') :
            shutil.rmtree('TMP')
        os.system('mkdir TMP')
        os.system("tar -zxf %s/%s.tar.gz -C TMP"%(self.libSourceDir,nameLib))
	os.chdir('./TMP')
	            
	srcPath=self.libSourceDir+'/TMP/'
	if os.path.isdir(srcPath+nameLib+'.build') :
            shutil.rmtree(srcPath+nameLib+'.build')
	
        os.makedirs(buildPath)
        os.chdir(buildPath)
        files = os.listdir(srcPath+nameLib) 
        for file in files :
            try:
                shutil.copytree(srcPath+nameLib+'/'+file,buildPath+'/'+file)
            except:
                shutil.copy(srcPath+nameLib+'/'+file,buildPath+'/'+file)
	return nameLib, buildPath, optPath, srcPath




    #----------------------------------------------------------------------------
    # print function for Libraries
    #----------------------------------------------------------------------------    
    def printResume(self,myLibName):
       	try:
	     toWrite  = "                                                     \n"
             toWrite += "***************************************************  \n"
             toWrite += "*                 Installation %s              *  \n"%myLibName.upper()
             toWrite += "***************************************************  \n"
             toWrite += "                                                     \n"
             print toWrite 

	     os.system("echo '                                                 ' >> "+self.installPath+"/syrthes-install/resume")
	     os.system("echo '*************************************************' >> "+self.installPath+"/syrthes-install/resume")	     
	     os.system("echo '*                 Installation %s            *' >> "%myLibName.upper()+self.installPath+"/syrthes-install/resume")
	     os.system("echo '*************************************************' >> "+self.installPath+"/syrthes-install/resume")	     	     
	     os.system("echo '                                                 ' >> "+self.installPath+"/syrthes-install/resume")
             #os.system("make all NOM_ARCH="+self.arch+" 2>&1 | tee "+nameLib+".log")
	     os.system("cat "+myLibName+".log >>"+self.installPath+"/syrthes-install/resume")
 	except:
	     print "Error during make of "+myLibName




    #----------------------------------------------------------------------------
    # Function copy and install GUI    
    #----------------------------------------------------------------------------
    def installGUI(self):
    
        # Copy Graphical user Interface    
        if "x86_64" in os.uname()[4]: # <-> self.arch = "Linux_x86_64" or "Linux_IA64"
            try:
                 shutil.copytree(self.installPath+'/syrthes-gui/exe.linux-x86_64', self.destPath+self.arch+'/lib/syrthes-GUI_exe')
            except:
                 print " "
                 print "--> No such file or directory "+self.installPath+"/syrthes-gui/exe.linux-x86_64"
                 print " "
        else:
            if "CYGWIN" in os.uname()[0]:
                try:
                     shutil.copytree(self.installPath+'/syrthes-gui/exe.WIN32', self.destPath+self.arch+'/lib/syrthes-GUI_exe')
                except:
                     print " "
                     print "--> No such file or directory "+self.installPath+"/syrthes-gui/exe.WIN32"
                     print " "
            else:
                try:
                     shutil.copytree(self.installPath+'/syrthes-gui/exe.linux-x86_32', self.destPath+self.arch+'/lib/syrthes-GUI_exe')
                except:
                     print " "
                     print "--> No such file or directory "+self.installPath+"/syrthes-gui/exe.linux-x86_32"
                     print " "


        # Create a shortcut to syrthes-GUI program
	os.chdir(self.destPath+self.arch+'/bin')
	f = open("syrthes.gui",'w')
	f.write('#/bin.sh\n')
	f.write('# GUI Launcher\n')
        f.write(self.destPath+self.arch+'/lib/syrthes-GUI_exe/SyrthesMain $1 $2 $3 $4 $5')
	f.close()
	os.system('chmod +x syrthes.gui')


	
	

    
        

        

#-------------------------------------------------------------------------------
# 
# Progam principal
# 
#  The install script uses the "setup" file to determine which library to
#  install or to use. For each element, there are three options:
#
#  - to not use the element (for optional libraries like MPI)
#     In this case, specify "no" in the "Usage" and "Install" columns. The other
#     elements will be installed in accordance. The columns "Path" and "LibName"
#     are not used.
#
#  - to use a pre-installed library
#     In this case, specify "yes" in the Usage column and "no" in the Install
#     column. The "Path" column should contain the location of the library
#     (up to the name of the library itself). The column "LibName" is not used.
#
#  - to install and use a library
#     In this case, specify "yes" in the "Usage" and "Install" columns. The
#     script will read the "LibName" column and look for the library in the
#     default directory 
#
#
#-------------------------------------------------------------------------------
if __name__ == '__main__':
       main= Setup()
