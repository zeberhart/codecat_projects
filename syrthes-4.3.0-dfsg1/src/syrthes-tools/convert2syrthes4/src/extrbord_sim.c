/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

#include "convert2syrthes.h"

int somfac[4][3];

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Extraction du maillage de bord                                       |
  |======================================================================| */

int extrbord2(struct Maillage maillnodes,struct MaillageBord *maillnodebord,
	       int **nrefac)
{
  int i,j;
  int is1,is2,is3,isad,js1,js2,iso1,iso2,jso1,jso2;
  int nmemax,nelep1,ip,ip1,ipmax,ipmin;
  int nel,neli,nelj,ifac,ifaci,ifacj,ifauxi,ifauxj;
  int nfacbo,nr;
  int *iadr,*itrav;
  int nbface=3,**nvoisin;

  nvoisin=(int**)malloc(nbface*sizeof(int*));
  for (i=0; i<nbface; i++) {
    nvoisin[i]=(int*)malloc(maillnodes.nelem*sizeof(int));
  if (!nvoisin[i]) {
    printf("\n %%%% ERROR extrbord : Memory allocation error\n");
    return 1;}
  }

  nmemax =  maillnodes.nelem * 3 + 2 * maillnodes.npoin;
  nelep1 =  maillnodes.nelem + 1;
  ipmin  =  2 * maillnodes.npoin+1;
  ip     =  2 * maillnodes.npoin;
  

  somfac[0][0] = 0; somfac[0][1] = 1;
  somfac[1][0] = 1; somfac[1][1] = 2;
  somfac[2][0] = 2; somfac[2][1] = 0;


  iadr  = (int *)malloc(nmemax * sizeof(int));
  itrav = (int *)malloc(nmemax * sizeof(int));
  if (!iadr || !itrav) {
    printf("\n %%%% ERROR extrbord : Memory allocation error\n");
    return 1;}



  for (i=0; i < nmemax ; i++) *(iadr+i) = -1 ;
  for (i=0; i < nmemax ; i++) *(itrav+i) = 0 ;
  for (j=0; j < nbface; j++)
    for (i=0; i < maillnodes.nelem ; i++) nvoisin[j][i]=-1;


  for ( ifac=0;ifac<3;ifac++)
    for (i=0;i<maillnodes.nelem;i++)
      {
	is1 = maillnodes.node[somfac[ifac][0]][i];
	is2 = maillnodes.node[somfac[ifac][1]][i];
	
	isad = is1+is2;
	
	if(iadr[isad] == -1)
	  {
	    iadr[isad] =i+ifac*nelep1 ;
	    itrav[isad]=isad;
	  }
	else 
	  {
	    ip++; iadr[ip]=i+ifac*nelep1; itrav[ip]=itrav[isad];
	    itrav[isad] = ip;
	  }
      }

  ipmax = ip ;

  for ( i=ipmax;i>=ipmin;i--)
    {
      ifauxi  = iadr[i];
      ifaci   = ifauxi/nelep1;
      neli    = ifauxi-ifaci*nelep1;

      if (nvoisin[ifaci][neli] != -1) continue;

      is1 = maillnodes.node[somfac[ifaci][0]][neli];
      is2 = maillnodes.node[somfac[ifaci][1]][neli];

      if (is1<is2){iso1=is1; iso2=is2;}
      else {iso1=is2; iso2=is1;}

      ip1 = i;
      while (ip1 >= ipmin)
	{
	  ip1     = itrav[ip1];
	  ifauxj  = iadr[ip1];
	  ifacj   = ifauxj/nelep1;
	  nelj    = ifauxj-ifacj*nelep1;

	  js1 = maillnodes.node[somfac[ifacj][0]][nelj];
	  js2 = maillnodes.node[somfac[ifacj][1]][nelj];

	  if (js1<js2){jso1=js1; jso2=js2;}
	  else {jso1=js2; jso2=js1;}

	  if (iso1==jso1 && iso2==jso2)
	    {
	      nvoisin[ifaci][neli] = nelj;
	      nvoisin[ifacj][nelj] = neli;
	      continue ;
	    }
	}

    }

  free(iadr);
  free(itrav);


  maillnodebord->nelem=0;
  nfacbo=0;

  for (ifac=0;ifac<nbface;ifac++)
    for (nel=0;nel<maillnodes.nelem;nel++)
      if ( nvoisin[ifac][nel] == -1 ) nfacbo++;
  
  maillnodebord->nelem=nfacbo;
  maillnodebord->ndim=2;
  maillnodebord->ndmat=2;
  maillnodebord->ndiele=1;
  maillnodebord->nrefe=(int*)malloc(maillnodebord->nelem*sizeof(int));
  if (!maillnodebord->nrefe) {
    printf("\n %%%% ERROR extrbord : Memory allocation error\n");
    return 1;}

  maillnodebord->node=(int**)malloc((maillnodebord->ndmat+1)*sizeof(int*));
  for (i=0;i<maillnodebord->ndmat+1;i++){
    maillnodebord->node[i]=(int*)malloc(maillnodebord->nelem*sizeof(int));
    if (!maillnodebord->node[i]) {
      printf("\n %%%% ERROR extrbord : Memory allocation error\n");
      return 1;}
  }

  nfacbo=0;
  for (ifac=0;ifac<nbface;ifac++)
    for (i=0;i<maillnodes.nelem;i++)
      if ( nvoisin[ifac][i] == -1 )
	{
	  is1 = maillnodes.node[somfac[ifac][0]][i]; 
	  is2 = maillnodes.node[somfac[ifac][1]][i]; 
	  maillnodebord->node[0][nfacbo]=is1;
	  maillnodebord->node[1][nfacbo]=is2;
	  maillnodebord->node[2][nfacbo]=i;
	  nr=nrefac[ifac][i]; 
	  maillnodebord->nrefe[nfacbo]=nr;
	  nfacbo++;
	}

  printf("\n *** EXTRBORD2 : element number of the boundary mesh : %d",maillnodebord->nelem);


  for (i=0; i<nbface; i++) free(nvoisin[i]);
  free(nvoisin);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Extraction du maillage de bord                                       |
  |======================================================================| */

int extrbord3(struct Maillage maillnodes,struct MaillageBord *maillnodebord,
	       int **nrefac)
{
  int i,j;
  int is1,is2,is3,is4,is5,is6,isad,js1,js2,js3;
  int iso1,iso2,iso3,jso1,jso2,jso3;
  int nmemax,nelep1,ip,ip1,nr;
  int ipmax,ipmin;
  int nel,neli,nelj;
  int ifac,ifaci,ifacj;
  int ifauxi,ifauxj;
  int nfacbo;
  int *iadr,*itrav;
  int nbface=4,**nvoisin;

  nvoisin=(int**)malloc(nbface*sizeof(int*));
  for (i=0; i<nbface; i++) {
    nvoisin[i]=(int*)malloc(maillnodes.nelem*sizeof(int));
    if (!nvoisin[i]) {
      printf("\n %%%% ERROR extrbord : Memory allocation error\n");
      return 1;}
  }

  nmemax = maillnodes.nelem * 4 + 3 *maillnodes.npoin;
  nelep1 = maillnodes.nelem + 1;
  ipmin  = 3*maillnodes.npoin+1;
  ip     = 3*maillnodes.npoin;
  

  somfac[0][0]=0; somfac[0][1]=1; somfac[0][2]=2;
  somfac[1][0]=0; somfac[1][1]=3; somfac[1][2]=1;
  somfac[2][0]=0; somfac[2][1]=2; somfac[2][2]=3;
  somfac[3][0]=1; somfac[3][1]=3; somfac[3][2]=2;


  iadr  = (int *)malloc(nmemax * sizeof(int));
  itrav = (int *)malloc(nmemax * sizeof(int));
  if (!iadr || !itrav) {
    printf("\n %%%% ERROR extrbord : Memory allocation error\n");
    return 1;}

  for (i=0; i < nmemax ; i++) *(iadr+i) = -1 ;
  for (i=0; i < nmemax ; i++) *(itrav+i) = 0 ;
  for (j=0; j < nbface; j++)
    for (i=0; i < maillnodes.nelem ; i++) nvoisin[j][i]=-1;


  for ( ifac=0;ifac<4;ifac++)
    {
    
      for ( i=0;i<maillnodes.nelem;i++)
	{
	  is1 = maillnodes.node[somfac[ifac][0]][i]; 
	  is2 = maillnodes.node[somfac[ifac][1]][i]; 
	  is3 = maillnodes.node[somfac[ifac][2]][i]; 

	  isad = is1+is2+is3;

	  if(iadr[isad] == -1)
	    {
	      iadr[isad] = i + ifac*nelep1;
	      itrav[isad]= isad;
	    }
	  else 
	    {
	      ip++;  iadr[ip]=i+ifac*nelep1; itrav[ip]=itrav[isad];
	      itrav[isad] = ip;
	    }
	}
    }

  ipmax = ip ;

  for ( i=ipmax;i>=ipmin;i--)
    {
      ifauxi  = iadr[i];
      ifaci   = ifauxi/nelep1;
      neli    = ifauxi-ifaci*nelep1;


      if (nvoisin[ifaci][neli] != -1) continue;

      is1 = maillnodes.node[somfac[ifaci][0]][neli];
      is2 = maillnodes.node[somfac[ifaci][1]][neli];
      is3 = maillnodes.node[somfac[ifaci][2]][neli];

      if ( is1 <= is2 && is1 <= is3) 
	{
	  if (is2 <= is3) { iso1=is1; iso2=is2; iso3=is3;}
	  else            { iso1=is1; iso2=is3; iso3=is2;}
	}
      else if ( is2 <= is1 && is2 <= is3) 
	{
	  if (is1 <= is3) { iso1=is2; iso2=is1; iso3=is3;}
	  else            { iso1=is2; iso2=is3; iso3=is1;}
	}
      else
	{
	  if (is1 <= is2) { iso1=is3; iso2=is1; iso3=is2;}
	  else            { iso1=is3; iso2=is2; iso3=is1;}
	}

      ip1 = i;
      while ( ip1 >= ipmin )
	{
	  ip1     = itrav[ip1];
	  ifauxj  = iadr[ip1];
	  ifacj   = ifauxj/nelep1;
	  nelj    = ifauxj-ifacj*nelep1;

	  js1 = maillnodes.node[somfac[ifacj][0]][nelj];
	  js2 = maillnodes.node[somfac[ifacj][1]][nelj];
	  js3 = maillnodes.node[somfac[ifacj][2]][nelj];

	  if ( js1 <= js2 && js1 <= js3) 
	    {
	      if (js2 <= js3) { jso1=js1; jso2=js2; jso3=js3;}
	      else            { jso1=js1; jso2=js3; jso3=js2;}
	    }
	  else if ( js2 <= js1 && js2 <= js3) 
	    {
	      if (js1 <= js3) { jso1=js2; jso2=js1; jso3=js3;}
	      else            { jso1=js2; jso2=js3; jso3=js1;}
	    }
	  else
	    {
	      if (js1 <= js2) { jso1=js3; jso2=js1; jso3=js2;}
	      else            { jso1=js3; jso2=js2; jso3=js1;}
	    }

	  if ( iso1 == jso1 &&  iso2 == jso2 &&  iso3 == jso3)
	    {
	      nvoisin[ifaci][neli] = nelj;
	      nvoisin[ifacj][nelj] = neli;
	      continue ;
	    }
	}

    }

  free(iadr);
  free(itrav);

  nfacbo=0;

  for (ifac=0;ifac<nbface;ifac++)
    for (nel=0;nel<maillnodes.nelem;nel++)
      if ( nvoisin[ifac][nel] == -1 ) nfacbo++;

  maillnodebord->nelem=nfacbo;
  maillnodebord->ndim=3;
  maillnodebord->ndmat=3;
  maillnodebord->ndiele=2;
  maillnodebord->nrefe=(int*)malloc(maillnodebord->nelem*sizeof(int));
  if (!maillnodebord->nrefe) {
    printf("\n %%%% ERROR extrbord : Memory allocation error\n");
    return 1;}
  maillnodebord->node=(int**)malloc((maillnodebord->ndmat+1)*sizeof(int*));
  for (i=0;i<maillnodebord->ndmat+1;i++){
    maillnodebord->node[i]=(int*)malloc(maillnodebord->nelem*sizeof(int));
    if (!maillnodebord->node[i]) {
      printf("\n %%%% ERROR extrbord : Memory allocation error\n");
      return 1;}
  }

  nfacbo=0;
  for (ifac=0;ifac<nbface;ifac++)
    for (i=0;i<maillnodes.nelem;i++)
      {
      if ( nvoisin[ifac][i] == -1 )
	{
	  is1 = maillnodes.node[somfac[ifac][0]][i]; 
	  is2 = maillnodes.node[somfac[ifac][1]][i]; 
	  is3 = maillnodes.node[somfac[ifac][2]][i];
	  maillnodebord->node[0][nfacbo]=is1;
	  maillnodebord->node[1][nfacbo]=is2;
	  maillnodebord->node[2][nfacbo]=is3;
	  maillnodebord->node[3][nfacbo]=i;
	  nr=nrefac[ifac][i]; maillnodebord->nrefe[nfacbo]=nr;
	  nfacbo++;
	}
      }

  for (i=0; i<nbface; i++) free(nvoisin[i]);
  free(nvoisin);

  printf("\n *** EXTRBORD3 : nombre d'elements du maillage de bord : %d",maillnodebord->nelem);
/*    imprime_connectivite(*maillnodebord); */

}
