#! /usr/bin/env pike
// -*- pike -*- $Id: 96c507affd91fb5f55a8260f74427048afba4c57 $
#pike __REAL_VERSION__

constant description = "CFLAGS for C module compilation";

int main(int argc, array(string) argv) {
  write("%s\n", master()->cflags||"");
}
