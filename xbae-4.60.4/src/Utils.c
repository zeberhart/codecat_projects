/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 * Copyright(c) 1995-99 Andrew Lister
 * Copyright � 1999, 2000, 2001, 2002, 2003, 2004 by the LessTif Developers.
 *
 *                        All rights reserved
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * MatrixWidget Author: Andrew Wason, Bellcore, aw@bae.bellcore.com
 *
 * $Id: Utils.c,v 1.123 2006/05/16 19:59:53 tobiasoed Exp $
 */

/*
 * Utils.c created by Andrew Lister (7 August, 1995)
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <stdlib.h>
#include <assert.h>

#include <Xm/Xm.h>
#include <Xm/ScrollBar.h>

#include <Xbae/MatrixP.h>
#include <Xbae/Macros.h>
#include <Xbae/Utils.h>
#include <Xbae/Actions.h>

#include <XbaeDebug.h>

static int xbaeRowClip(XbaeMatrixWidget mw, int row)
{
        int clip;

        if (row == -1) {
                clip = CLIP_COLUMN_LABELS;
        } else if (IS_LEADING_FIXED_ROW(mw, row)) {
                clip = CLIP_FIXED_ROWS;
        } else if (IS_TRAILING_FIXED_ROW(mw, row)) {
                clip = CLIP_TRAILING_FIXED_ROWS;
        } else {
                clip = CLIP_VISIBLE_HEIGHT;
        }
        
        return clip;
}

static int xbaeColumnClip(XbaeMatrixWidget mw, int column)
{
        int clip;

        if (column == -1) {
                clip = CLIP_ROW_LABELS;
        } else if (IS_LEADING_FIXED_COLUMN(mw, column)) {
                clip = CLIP_FIXED_COLUMNS;
        } else if (IS_TRAILING_FIXED_COLUMN(mw, column)) {
                clip = CLIP_TRAILING_FIXED_COLUMNS;
        } else {
                clip = CLIP_VISIBLE_WIDTH;
        }

        return clip;
}

static int xbaeCellClip(XbaeMatrixWidget mw, int row, int column)
{
        return xbaeRowClip(mw, row) | xbaeColumnClip(mw, column);
}

/*
 * Returns the widget to which a cell belongs, i.e. the matrix, or one
 * or one of the extra clips which handle the fixed row/col cells/labels
 */
static Widget xbaeGetCellClip(XbaeMatrixWidget mw, int row, int column)
{
        int clip = xbaeCellClip(mw, row, column);
        Widget w;

        switch (clip) {
        case CLIP_VISIBLE_WIDTH | CLIP_VISIBLE_HEIGHT:
                /* not fixed at all - on center clip */
                w = CenterClip(mw);
                break;

        case CLIP_FIXED_COLUMNS | CLIP_VISIBLE_HEIGHT:
                /* fixed col only - on left clip */
                w = LeftClip(mw);
                break;

        case CLIP_TRAILING_FIXED_COLUMNS | CLIP_VISIBLE_HEIGHT:
                /* fixed trailing col only - on right clip */
                w = RightClip(mw);
                break;

        case CLIP_ROW_LABELS | CLIP_VISIBLE_HEIGHT:
                /* non fixed row labels - on row label clip */
                w = RowLabelClip(mw);
                break;

        case CLIP_FIXED_ROWS | CLIP_VISIBLE_WIDTH:
                /* fixed row only - on top clip */
                w = TopClip(mw);
                break;

        case CLIP_TRAILING_FIXED_ROWS | CLIP_VISIBLE_WIDTH:
                /* fixed trailing row only - on bottom clip */
                w = BottomClip(mw);
                break;

        case CLIP_COLUMN_LABELS | CLIP_VISIBLE_WIDTH:
                /* non fixed column labels - on column label clip */
                w = ColumnLabelClip(mw);
                break;

        default:
                /* total fixed cell/labels - on parent matrix window */
                w = (Widget) mw;
                break;
        }
        assert(w);
        return w;
}

/*
 * Cache the virtual position of each row/column
 */

static void 
xbaeGetPosition(int n, Boolean size_in_pixels, short *sizes, int *positions, int font_size, int border_size) 
{
        int i, pos;
        
        if (size_in_pixels) {
                for (i = 0, pos = 0; i < n; i++) {
                        positions[i] = pos;
                        pos += sizes[i];
                }
        } else {
                for (i = 0, pos = 0; i < n; i++) {
                        positions[i] = pos;
                        if (sizes[i] != 0) {
                                pos += sizes[i] * font_size + 2 * border_size;
                        }
                }
        }
        /* Tobias: The positions arrays are one element longer than
         * the number of columns/rows in the matrix. We need to initialze 
         * the last element so wo can calculate sizes safely from the 
         * the difference of any two positions
         */
        positions[n] = pos;
}

void xbaeGetColumnPositions(XbaeMatrixWidget mw)
{
        xbaeGetPosition(mw->matrix.columns, 
                        mw->matrix.column_width_in_pixels,
                        mw->matrix.column_widths, 
                        mw->matrix.column_positions,
                        CELL_FONT_WIDTH(mw),
                        CELL_BORDER_WIDTH(mw));
}

void xbaeGetRowPositions(XbaeMatrixWidget mw)
{
        xbaeGetPosition(mw->matrix.rows, 
                        mw->matrix.row_height_in_pixels,
                        mw->matrix.row_heights, 
                        mw->matrix.row_positions,
                        TEXT_HEIGHT(mw),
                        CELL_BORDER_HEIGHT(mw));
}

static int
xbaeCheckPosition(int n, Boolean size_in_pixels, short *sizes, int *positions, int font_size, int border_size, int j)
{
        int i, pos;

        if (size_in_pixels == True) {
                for (i = 0, pos = 0; i < n; pos += sizes[i], i++) {
                        assert(positions[i] == pos);
                }
        } else {
                for (i = 0, pos = 0; i < n; pos += sizes[i] * font_size + 2 * border_size, i++) {
                        assert(positions[i] == pos);
                }
        }
        assert(positions[n] == pos);

        assert(j >= 0 && j <= n);

        return positions[j];
}

int xbaeCheckColumnPosition(XbaeMatrixWidget mw, int column)
{
        return xbaeCheckPosition(mw->matrix.columns, 
                                 mw->matrix.column_width_in_pixels,
                                 mw->matrix.column_widths, 
                                 mw->matrix.column_positions,
                                 CELL_FONT_WIDTH(mw),
                                 CELL_BORDER_WIDTH(mw),
                                 column);
}

int xbaeCheckRowPosition(XbaeMatrixWidget mw, int row)
{
        return xbaeCheckPosition(mw->matrix.rows, 
                                 mw->matrix.row_height_in_pixels,
                                 mw->matrix.row_heights, 
                                 mw->matrix.row_positions,
                                 TEXT_HEIGHT(mw),
                                 CELL_BORDER_HEIGHT(mw),
                                 row);
}

/*
 * Find where a virtual coordinate falls by (binary) searching the positions array
 */
static int findPosition(int *positions, int start, int end, int pos)
{
        int middle;

        /* Tobias: Neither of the conditions should ever be true. If they are there is a bug somewhere 
         * up the call stack. So far I found three. The rest of xbae tries to fix problems instead of failing
         * so we do the same here to keep up with the debugging fun.
         */
        if (pos < positions[start]) {
                DEBUGOUT(_XbaeDebug
                         (__FILE__, NULL, "pos[start=%d]=%d pos[end=%d]=%d pos=%d\n", start,
                          positions[start], end, positions[end], pos));
                return start;
        } else if (pos > positions[end] - 1) {
                DEBUGOUT(_XbaeDebug
                         (__FILE__, NULL, "pos[start=%d]=%d pos[end=%d]=%d pos=%d\n", start,
                          positions[start], end, positions[end], pos));
                return end - 1;
        }

        for (;;) {
                middle = (start + end) / 2;
                if (positions[middle] > pos) {
                        end = middle;
                } else if (positions[middle + 1] - 1 < pos) {
                        start = middle;
                } else {
                        break;
                }
        }

        return middle;
}

static int xbaeXtoCol(XbaeMatrixWidget mw, int x)
{
        return findPosition(mw->matrix.column_positions, 0, mw->matrix.columns, x);
}

static int xbaeYtoRow(XbaeMatrixWidget mw, int y)
{
        return findPosition(mw->matrix.row_positions, 0, mw->matrix.rows, y);
}

/*
 * Return the top and bottom-most visible non-fixed row
 */
int xbaeTopRow(XbaeMatrixWidget mw)
{
        return xbaeYtoRow(mw, FIXED_ROW_HEIGHT(mw) + VERT_ORIGIN(mw));
}

int xbaeBottomRow(XbaeMatrixWidget mw)
{
        return xbaeYtoRow(mw, FIXED_ROW_HEIGHT(mw) + VERT_ORIGIN(mw) + VISIBLE_NON_FIXED_HEIGHT(mw) - 1);
}

void xbaeGetVisibleRows(XbaeMatrixWidget mw, int *top_row, int *bottom_row)
{
        *top_row = xbaeTopRow(mw);
        *bottom_row = xbaeBottomRow(mw);
}

/*
 * Return the left and right-most visible non-fixed column
 */
int xbaeLeftColumn(XbaeMatrixWidget mw)
{
        return xbaeXtoCol(mw, FIXED_COLUMN_WIDTH(mw) + HORIZ_ORIGIN(mw));
}

int xbaeRightColumn(XbaeMatrixWidget mw)
{
        return xbaeXtoCol(mw, FIXED_COLUMN_WIDTH(mw) + HORIZ_ORIGIN(mw) + VISIBLE_NON_FIXED_WIDTH(mw) - 1);
}

void xbaeGetVisibleColumns(XbaeMatrixWidget mw, int *left_column, int *right_column)
{
        *left_column = xbaeLeftColumn(mw);
        *right_column = xbaeRightColumn(mw);
}

/*
 * Return the top and bottom row and left and right column of
 * the visible non-fixed cells
 */
void
xbaeGetVisibleCells(XbaeMatrixWidget mw, int *top_row, int *bottom_row, int *left_column,
                    int *right_column)
{
        xbaeGetVisibleRows(mw, top_row, bottom_row);
        xbaeGetVisibleColumns(mw, left_column, right_column);
}

/*
 * Try to make a row/column top/left row/column. The row/column is relative to 
 * fixed_rows/columns - so 0 would be the first non-fixed row/column.
 * If we can't make it the top row/left column, make it as close as possible.
 */
int
xbaeCalculateVertOrigin(XbaeMatrixWidget mw, int top_row)
{
        if (NON_FIXED_HEIGHT(mw) < VISIBLE_NON_FIXED_HEIGHT(mw)) {
                return 0;
        } else if (ROW_POSITION(mw, TRAILING_ROW_ORIGIN(mw)) - 
                   ROW_POSITION(mw, mw->matrix.fixed_rows + top_row) < VISIBLE_NON_FIXED_HEIGHT(mw)) {
               return NON_FIXED_HEIGHT(mw) - VISIBLE_NON_FIXED_HEIGHT(mw);
        } else {
                return ROW_POSITION(mw, mw->matrix.fixed_rows + top_row) -
                       FIXED_ROW_HEIGHT(mw);
        }
}

int
xbaeCalculateHorizOrigin(XbaeMatrixWidget mw, int left_column) {
        if (NON_FIXED_WIDTH(mw) < VISIBLE_NON_FIXED_WIDTH(mw)) {
                return 0;
        } else if (COLUMN_POSITION(mw, TRAILING_COLUMN_ORIGIN(mw)) - 
                   COLUMN_POSITION(mw, mw->matrix.fixed_columns + left_column) < VISIBLE_NON_FIXED_WIDTH(mw)) {
               return NON_FIXED_WIDTH(mw) - VISIBLE_NON_FIXED_WIDTH(mw);
        } else {
                return COLUMN_POSITION(mw, mw->matrix.fixed_columns + left_column) -
                       FIXED_COLUMN_WIDTH(mw);
        }
}

/*
 * Return True if a row is visible on the screen (not scrolled totally off)
 */
Boolean xbaeIsRowVisible(XbaeMatrixWidget mw, int row)
{
        /*
         * If we are not in a fixed row or trailing fixed row,
         * see if we are on the screen vertically
         * (fixed rows are always on the screen)
         */
        if (!IS_FIXED_ROW(mw, row)) {
                /* SGO: used same method as IsColumnVisible */
                int y;
                y = ROW_POSITION(mw, row) - 
                    ROW_POSITION(mw, mw->matrix.fixed_rows) - VERT_ORIGIN(mw);

                if (y + ROW_HEIGHT(mw, row) > 0 && y < VISIBLE_NON_FIXED_HEIGHT(mw))
                        return True;
        } else
                return True;

        return False;
}

/*
 * Return True if a column is visible on the screen (not scrolled totally off)
 */
Boolean xbaeIsColumnVisible(XbaeMatrixWidget mw, int column)
{
        /*
         * If we are not in a fixed column, see if we are on the screen
         * horizontally (fixed columns are always on the screen)
         */
        if (!IS_FIXED_COLUMN(mw, column)) {
                int x;

                /*
                 * Calculate the x position of the column relative to the clip
                 */
                x = COLUMN_POSITION(mw, column) - 
                    COLUMN_POSITION(mw, mw->matrix.fixed_columns) - HORIZ_ORIGIN(mw);

                /*
                 * Check if we are visible horizontally
                 */
                if (x + COLUMN_WIDTH(mw, column) > 0 && x < VISIBLE_NON_FIXED_WIDTH(mw))
                        return True;
        } else
                return True;

        return False;
}

/*
 * Return True if a cell is visible on the screen (not scrolled totally off)
 */
Boolean xbaeIsCellVisible(XbaeMatrixWidget mw, int row, int column)
{
        return xbaeIsRowVisible(mw, row) && xbaeIsColumnVisible(mw, column);
}

/*
 * Scroll a row so it is visible on the screen.
 */
void xbaeMakeRowVisible(XbaeMatrixWidget mw, int row)
{
        int value, slider_size, increment, page_increment, y, vert_value;

        /*
         * If we are in a fixed column, we are already visible.
         */
        if (IS_FIXED_ROW(mw, row))
                return;

        /*
         * Calculate the y position of this row
         */
        y = ROW_POSITION(mw, row) - ROW_POSITION(mw, mw->matrix.fixed_rows);

        /*
         * Figure out the new value of the VSB to scroll this cell
         * onto the screen. If the whole cell won't fit, scroll so its
         * top edge is visible.
         */
        if (y < VERT_ORIGIN(mw) || ROW_HEIGHT(mw, row) >= VISIBLE_NON_FIXED_HEIGHT(mw)) {
                vert_value = y;
        } else if (y + ROW_HEIGHT(mw, row) > VISIBLE_NON_FIXED_HEIGHT(mw) + VERT_ORIGIN(mw)) {
                vert_value = y + ROW_HEIGHT(mw, row) - VISIBLE_NON_FIXED_HEIGHT(mw);
        } else {
                vert_value = VERT_ORIGIN(mw);
        }

        /*
         * Give the VSB the new value and pass a flag to make it
         * call our scroll callbacks
         */
        if (vert_value != VERT_ORIGIN(mw)) {
                XmScrollBarGetValues(VertScrollChild(mw), &value, &slider_size, &increment,
                                     &page_increment);
                XmScrollBarSetValues(VertScrollChild(mw), vert_value, slider_size, increment,
                                     page_increment, True);
        }
}

/*
 * Scroll a column so it is visible on the screen.
 */
void xbaeMakeColumnVisible(XbaeMatrixWidget mw, int column)
{
        int value, slider_size, increment, page_increment, x, horiz_value;

        /*
         * If we are in a fixed column, we are already visible.
         */
        if (IS_FIXED_COLUMN(mw, column))
                return;

        /*
         * Calculate the x position of this column
         */
        x = COLUMN_POSITION(mw, column) - COLUMN_POSITION(mw, mw->matrix.fixed_columns);

        /*
         * Figure out the new value of the HSB to scroll this cell
         * onto the screen. If the whole cell won't fit, scroll so its
         * left edge is visible.
         */
        if (x < HORIZ_ORIGIN(mw) || COLUMN_WIDTH(mw, column) >= VISIBLE_NON_FIXED_WIDTH(mw)) {
                horiz_value = x;
        } else if (x + COLUMN_WIDTH(mw, column) > VISIBLE_NON_FIXED_WIDTH(mw) + HORIZ_ORIGIN(mw)) {
                horiz_value = x + COLUMN_WIDTH(mw, column) - VISIBLE_NON_FIXED_WIDTH(mw);
        } else {
                horiz_value = HORIZ_ORIGIN(mw);
        }

        /*
         * Give the HSB the new value and pass a flag to make it
         * call our scroll callbacks
         */
        if (horiz_value != HORIZ_ORIGIN(mw)) {
                XmScrollBarGetValues(HorizScrollChild(mw), &value, &slider_size, &increment,
                                     &page_increment);
                XmScrollBarSetValues(HorizScrollChild(mw), horiz_value, slider_size, increment,
                                     page_increment, True);
        }
}

/*
 * Scrolls a fixed or non-fixed cell so it is visible on the screen.
 */
void xbaeMakeCellVisible(XbaeMatrixWidget mw, int row, int column)
{
        if (!xbaeIsRowVisible(mw, row))
                xbaeMakeRowVisible(mw, row);

        if (!xbaeIsColumnVisible(mw, column))
                xbaeMakeColumnVisible(mw, column);
}

void xbaeComputeSize(XbaeMatrixWidget mw, Boolean compute_width, Boolean compute_height)
{
        int full_width = TOTAL_WIDTH(mw) + ROW_LABEL_WIDTH(mw) + 2 * mw->manager.shadow_thickness;
        int full_height = TOTAL_HEIGHT(mw) + COLUMN_LABEL_HEIGHT(mw) + 2 * mw->manager.shadow_thickness;
        int width, height;

        /*
         * Calculate our width.
         * If visible_columns is set, then base it on that.
         * Otherwise, if the compute_width flag is set, then we are full width.
         * Otherwise we keep whatever width we are.
         */

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget) mw,
                "xbaeComputeSize compute_width = %s   compute_height = %s\n"
                "                visible_columns = %d visible_rows = %d\n"
                "                width = %d           height =%d\n"
                "                full_width = %d      full_height = %d\n",
                compute_width ? "True" : "False",
                compute_height ? "True" : "False",
                mw->matrix.visible_columns,
                mw->matrix.visible_rows,
                mw->core.width, 
                mw->core.height,
                full_width,
                full_height));

        if (mw->matrix.visible_columns) {
                width = ROW_LABEL_WIDTH(mw) + 2 * mw->manager.shadow_thickness 
                    + FIXED_COLUMN_WIDTH(mw) + TRAILING_FIXED_COLUMN_WIDTH(mw)
                    + NON_FIXED_WIDTH(mw) * mw->matrix.visible_columns 
                     / (mw->matrix.columns - mw->matrix.fixed_columns - mw->matrix.trailing_fixed_columns);
        } else if (compute_width) {
                width = full_width;
        } else {
                width = mw->core.width;
        }

        /*
         * Calculate our height.
         * If visible_rows is set, then base it on that.
         * Otherwise, if the compute_height flag is set, then we are full height.
         * Otherwise we keep whatever height we are.
         */
        if (mw->matrix.visible_rows) {
                height = COLUMN_LABEL_HEIGHT(mw) + 2 * mw->manager.shadow_thickness 
                    + FIXED_ROW_HEIGHT(mw) + TRAILING_FIXED_ROW_HEIGHT(mw)
                    + NON_FIXED_HEIGHT(mw) * mw->matrix.visible_rows 
                     / (mw->matrix.rows - mw->matrix.fixed_rows - mw->matrix.trailing_fixed_rows);
        } else if (compute_height) {
                height = full_height;
        } else {
                height = mw->core.height;
        }

        /*
         * If we are allowed to modify our height and we need to display a hsb 
         * include it's size in the computation
         */
        if (   (compute_height || mw->matrix.visible_rows)
            && (   (mw->matrix.hsb_display_policy == XmDISPLAY_STATIC)
                || (mw->matrix.hsb_display_policy == XmDISPLAY_AS_NEEDED && width < full_width)))
                height += HORIZ_SB_HEIGHT(mw);

        /*
         * If we are allowed to modify our width and we need to display a vsb 
         * include it's size in the computation
         */
        if (   (compute_width || mw->matrix.visible_columns)
            && (   (mw->matrix.vsb_display_policy == XmDISPLAY_STATIC)
                || (mw->matrix.vsb_display_policy == XmDISPLAY_AS_NEEDED && height < full_height)))
                width += VERT_SB_WIDTH(mw);

        /*
         * Store our calculated size.
         */
        mw->core.width = width;
        mw->core.height = height;

        /*
         * Save our calculated size for use in our query_geometry method.
         * This is the size we really want to be (not necessarily the size
         * we will end up being).
         */
        mw->matrix.desired_width = width;
        mw->matrix.desired_height = height;

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget) mw,
                            "xbaeComputeSize -> w %d h %d\n", width, height));
}

/*
 * Return the length of the longest line in string
 */
static int xbaeMaxLen(String string) {
        char *nl;
        int max_len = 0;

        /*
         * Get the length of the longest line terminated by \n
         */
        while ((nl = strchr(string, '\n')) != NULL) {
                int len = nl - string;
                if (len > max_len) {
                    max_len = len;
                }
                string = nl + 1;
        }

        /* 
         * If the last line wasn't terminated with a \n take it into account
         */
        if (*string != '\0') {
                int len = strlen(string);
                if (len > max_len) {
                        max_len = len;
                }
        }

        return max_len;
}

/*
 * Return the number of lines in string
 */
static int xbaeCountLines(String string) {
        char *nl;
        int n_lines = 0;
        
        /*
         * Count the number of lines terminated by \n
         */
        while ((nl = strchr(string, '\n')) != NULL) {
                n_lines++;
                string = nl + 1;
        }

        /*
         * If the last line wasn't terminated by a \n take it into account
         */
        if (*string != '\0') {
                n_lines++;
        }
        
        return n_lines;
}

/*
 * Return the length of the longest line of all row_labels
 */
int xbaeCalculateLabelMaxLength(XbaeMatrixWidget mw, String *labels, XmString *xmlabels, int n_labels)
{
        int i;
        int max_len = 0;

        /*
         * Determine the length of the longest row label
         */
        if (labels || xmlabels) {
                for (i = 0; i < n_labels; i++) {
                        int len = 0;
                        if (xmlabels && xmlabels[i]) {
#if XmVERSION >= 2
                                int width = XmStringWidth((mw->matrix.render_table)
                                                           ? mw->matrix.render_table 
                                                           : (mw->matrix.label_font_list)
                                                              ? mw->matrix.label_font_list 
                                                              : mw->matrix.font_list,
                                                          xmlabels[i]);
#else
                                int width = XmStringWidth((mw->matrix.label_font_list)
                                                            ? mw->matrix.label_font_list 
                                                            : mw->matrix.font_list,
                                                          xmlabels[i]);
#endif
                                len = width / LABEL_FONT_WIDTH(mw) + ((width % LABEL_FONT_WIDTH(mw)) > 0);
                        } else if (labels && labels[i]) {
                                len = xbaeMaxLen(labels[i]);
                        }
                        if (len > max_len) {
                                max_len = len;
                        }
                }
        }
        return max_len;
}

/*
 * Return the maximum number of lines in labels
 */
int xbaeCalculateLabelMaxLines(String *labels, XmString *xmlabels, int n_labels)
{
        int i;
        int max_lines = 0;
        if (labels || xmlabels) {
                for(i = 0; i < n_labels; i++) {
                        int n_lines = 0;
                        if (xmlabels && xmlabels[i]) {
                                n_lines = XmStringLineCount(xmlabels[i]);
                        } else if (labels && labels[i]) {
                                n_lines = xbaeCountLines(labels[i]);
                        }
                        if (n_lines > max_lines) {
                                max_lines = n_lines;
                        }
                }
        }
        return max_lines;
}

/*
 * Convert a matrix window x position to a column.
 * Return the clip region if x falls in a column or a row label. If so adjust x so that it's
 * relative to the column/label. If not return 0 and leave x alone
 */
int xbaeMatrixXtoColumn(XbaeMatrixWidget mw, int *x, int *column)
{
        if (*x >= VERT_SB_OFFSET(mw) && *x < VERT_SB_OFFSET(mw) + ROW_LABEL_WIDTH(mw)) {
                /* In the row labels */
                *column = -1;
                *x -= VERT_SB_OFFSET(mw);
                return CLIP_ROW_LABELS;
        } else if (*x >= FIXED_COLUMN_POSITION(mw)
                   && *x < FIXED_COLUMN_POSITION(mw) + VISIBLE_FIXED_COLUMN_WIDTH(mw)) {
                /* In the fixed columns */
                *x -= FIXED_COLUMN_POSITION(mw);
                if(*x >= COLUMN_POSITION(mw, mw->matrix.fixed_columns)) {
                        /* We are in the horizontal fill */
                        *column = mw->matrix.fixed_columns - 1;
                } else {
                        /* Get the column it corresponds to */
                        *column = xbaeXtoCol(mw, *x);
                }
                *x -= COLUMN_POSITION(mw, *column);
                return CLIP_FIXED_COLUMNS;
        } else if (*x >= TRAILING_FIXED_COLUMN_POSITION(mw)
                   && *x < TRAILING_FIXED_COLUMN_POSITION(mw) + VISIBLE_TRAILING_FIXED_COLUMN_WIDTH(mw)) {
                /* In the trailing fixed columns */
                *x -= TRAILING_FIXED_COLUMN_POSITION(mw) - COLUMN_POSITION(mw, TRAILING_COLUMN_ORIGIN(mw));
                if(*x >= COLUMN_POSITION(mw, mw->matrix.columns)) {
                        /* We are in the horizontal fill */
                        *column = mw->matrix.columns - 1;
                } else {
                        /* Get the column it corresponds to */
                        *column = xbaeXtoCol(mw, *x);
                }
                *x -= COLUMN_POSITION(mw, *column);
                return CLIP_TRAILING_FIXED_COLUMNS;
        } else if (*x >= NON_FIXED_COLUMN_POSITION(mw)
                   && *x < NON_FIXED_COLUMN_POSITION(mw) + VISIBLE_NON_FIXED_WIDTH(mw)) {
                /* In the non fixed columns */
                *x -= NON_FIXED_COLUMN_POSITION(mw) - HORIZ_ORIGIN(mw) - FIXED_COLUMN_WIDTH(mw);
                if(*x >= COLUMN_POSITION(mw, TRAILING_COLUMN_ORIGIN(mw))) {
                        /* We are in the horizontal fill */
                        *column = TRAILING_COLUMN_ORIGIN(mw) - 1;
                } else {
                        /* Get the column it corresponds to */
                        *column = xbaeXtoCol(mw, *x);
                }
                *x -= COLUMN_POSITION(mw, *column);
                return CLIP_VISIBLE_WIDTH;
        }

        *column = -2;
        return 0;
}

/*
 * Convert a matrix window y position to a row.
 * Return the clip region if y falls in a row or a column label. If so adjust y so that it's 
 * relative to the row/label. If not return 0 and leave y alone
 */
int xbaeMatrixYtoRow(XbaeMatrixWidget mw, int *y, int *row)
{
        if (*y >= HORIZ_SB_OFFSET(mw) && *y < HORIZ_SB_OFFSET(mw) + COLUMN_LABEL_HEIGHT(mw)) {
                /* In the column labels */
                *row = -1;
                *y -= HORIZ_SB_OFFSET(mw);
                return CLIP_COLUMN_LABELS;
        } else if (*y >= FIXED_ROW_POSITION(mw) && *y < FIXED_ROW_POSITION(mw) + VISIBLE_FIXED_ROW_HEIGHT(mw)) {
                /* In the fixed rows */
                *y -= FIXED_ROW_POSITION(mw);
                if (*y >= ROW_POSITION(mw, mw->matrix.fixed_rows)) {
                        /* We are in the vertical fill */
                        *row = mw->matrix.fixed_rows - 1;
                } else {
                        /* Get the row it corresponds to */
                        *row = xbaeYtoRow(mw, *y);
                }
                *y -= ROW_POSITION(mw, *row);
                return CLIP_FIXED_ROWS;
        } else if (*y >= TRAILING_FIXED_ROW_POSITION(mw)
                   && *y < TRAILING_FIXED_ROW_POSITION(mw) + VISIBLE_TRAILING_FIXED_ROW_HEIGHT(mw)) {
                /* In the trailing fixed rows */
                *y -= TRAILING_FIXED_ROW_POSITION(mw) - ROW_POSITION(mw, TRAILING_ROW_ORIGIN(mw));
                if (*y >= ROW_POSITION(mw, mw->matrix.rows)) {
                        /* We are in the vertical fill */
                        *row = mw->matrix.rows - 1;
                } else {
                        /* Get the row it corresponds to */
                        *row = xbaeYtoRow(mw, *y);
                }
                *y -= ROW_POSITION(mw, *row);
                return CLIP_TRAILING_FIXED_ROWS;
         } else if (*y >= NON_FIXED_ROW_POSITION(mw)
                   && *y < NON_FIXED_ROW_POSITION(mw) + VISIBLE_NON_FIXED_HEIGHT(mw)) {
                /* In the non fixed rows */
                *y -= NON_FIXED_ROW_POSITION(mw) - VERT_ORIGIN(mw) - FIXED_ROW_HEIGHT(mw);
                if (*y >= ROW_POSITION(mw, TRAILING_ROW_ORIGIN(mw))) {
                        /* We are in the vertical fill */
                        *row = TRAILING_ROW_ORIGIN(mw) - 1;
                } else {
                        /* Get the row it corresponds to */
                        *row = xbaeYtoRow(mw, *y);
                }
                *y -= ROW_POSITION(mw, *row);
                return CLIP_VISIBLE_HEIGHT;
        }

        *row = -2;
        return 0;
}

/*
 * Convert a matrix window x, y to a row, column pair.
 * Return the clip region if x, y falls in a cell or a label. If so adjust x 
 * and y so they are relative to the cell/label. If not return 0 and leave
 * x and y alone.
 */

/* ARGSUSED */
int xbaeMatrixXYToRowCol(XbaeMatrixWidget mw, int *x, int *y, int *row, int *column)
{
        int ret_x = *x;
        int ret_y = *y;

        int row_region = xbaeMatrixYtoRow(mw, &ret_y, row);
        int column_region = xbaeMatrixXtoColumn(mw, &ret_x, column);

        if (row_region == 0 || column_region == 0 
            || (row_region == CLIP_COLUMN_LABELS && column_region == CLIP_ROW_LABELS)) {
                /* Not a cell nor a label */
                *row = -1;
                *column = -1;
                return 0;
        }

        *x = ret_x;
        *y = ret_y;

        return row_region | column_region;
}

int xbaeEventToRowColumn(Widget w, XEvent * event, int *row, int *column, int *x, int *y)
{
        XbaeMatrixWidget mw;

        switch (event->type) {
        case KeyPress:
        case KeyRelease:
                *x = 0;
                *y = 0;
                break;
        case ButtonPress:
        case ButtonRelease:
                *x = event->xbutton.x;
                *y = event->xbutton.y;
                break;
        case MotionNotify:
                *x = event->xmotion.x;
                *y = event->xmotion.y;
                break;
        default:
                return 0;
        }

        if (XtIsSubclass(w, xbaeMatrixWidgetClass)) {
                /* The event occured in the matrix widget */
                
                mw = (XbaeMatrixWidget) w;

                return xbaeMatrixXYToRowCol(mw, x, y, row, column);
        } else {
                /* 
                 * The event didn't occur in a matrix widget. Find an ancestor that is the child
                 + of a matrix widget
                 */

                while (XtParent(w) != NULL && !XtIsSubclass(XtParent(w), xbaeMatrixWidgetClass)) {
                        *x += w->core.x;
                        *y += w->core.y;
                        w = XtParent(w);
                }

                mw = (XbaeMatrixWidget) XtParent(w);

                if (mw != NULL) {
                        XtVaGetValues(w, XmNattachRow, row, XmNattachColumn, column, NULL);
                        
                        return (*row == -1 || *column ==-1) ? 0 : xbaeCellClip(mw, *row, *column);
                }
        }
        
        return 0;
}

/*
 * Convert the coordinates in an event to coordinates relative to the matrix window
 */
XbaeMatrixWidget xbaeEventToMatrixXY(Widget w, XEvent * event, int *x, int *y)
{
        XbaeMatrixWidget mw;
        *x = 0;
        *y = 0;

        if (XtIsSubclass(w, xbaeMatrixWidgetClass)) {
                mw = (XbaeMatrixWidget) w;
        } else if (XtParent(w)
                   && XtIsSubclass(XtParent(w), xbaeMatrixWidgetClass)
                   && w == TextField((XbaeMatrixWidget) XtParent(w))) {
                int current_row, current_column;                
                mw = (XbaeMatrixWidget) XtParent(w);
                
                XtVaGetValues(TextField(mw), XmNattachRow, &current_row, XmNattachColumn, &current_column, NULL);
                
                *x = xbaeColumnToMatrixX(mw, current_column);
                *y = xbaeRowToMatrixY(mw, current_row);
        } else {
                do {
                        *x += w->core.x;
                        *y += w->core.y;
                        w = XtParent(w);
                } while (w && !XtIsSubclass(w, xbaeMatrixWidgetClass));

                mw = (XbaeMatrixWidget) w;
        }

        switch (event->type) {
        case KeyPress:
        case KeyRelease:
                break;
        case ButtonPress:
        case ButtonRelease:
                *x += event->xbutton.x;
                *y += event->xbutton.y;
                break;
        case MotionNotify:
                *x += event->xmotion.x;
                *y += event->xmotion.y;
                break;
        default:
                return NULL;
        }

        return mw;
}

/*
 * Find the matrix widget by walking the Widget tree 
 */
XbaeMatrixWidget xbaeEventToMatrixWidget(Widget w, XEvent *event) {
        while (w != NULL && !XtIsSubclass(w, xbaeMatrixWidgetClass)) {
            w = XtParent(w);
        }
        
        return (XbaeMatrixWidget) w;
}

/*
 * Convert a row/column cell position to the x/y of its upper left corner
 * wrt the matrix window 
 */
int xbaeColumnToMatrixX(XbaeMatrixWidget mw, int column)
{
        int x;

        if (column == -1) {
                x = VERT_SB_OFFSET(mw);
        } else if (IS_LEADING_FIXED_COLUMN(mw, column)) {
                x = FIXED_COLUMN_POSITION(mw) + COLUMN_POSITION(mw, column);
        } else if (IS_TRAILING_FIXED_COLUMN(mw, column)) {
                x = TRAILING_FIXED_COLUMN_POSITION(mw) + COLUMN_POSITION(mw, column) - COLUMN_POSITION(mw, TRAILING_COLUMN_ORIGIN(mw));
        } else {
                x = NON_FIXED_COLUMN_POSITION(mw) + COLUMN_POSITION(mw, column) - COLUMN_POSITION(mw, mw->matrix.fixed_columns) - HORIZ_ORIGIN(mw);
        }
        
        return x;
}

int xbaeRowToMatrixY(XbaeMatrixWidget mw, int row)
{
        int y;

        if (row == -1) {
                y = HORIZ_SB_OFFSET(mw);
        } else if (IS_LEADING_FIXED_ROW(mw, row)) {
                y = FIXED_ROW_POSITION(mw) + ROW_POSITION(mw, row);
        } else if (IS_TRAILING_FIXED_ROW(mw, row)) {
                y = TRAILING_FIXED_ROW_POSITION(mw) + ROW_POSITION(mw, row) - ROW_POSITION(mw, TRAILING_ROW_ORIGIN(mw));
        } else {
                y = NON_FIXED_ROW_POSITION(mw) + ROW_POSITION(mw, row) - ROW_POSITION(mw, mw->matrix.fixed_rows) - VERT_ORIGIN(mw);
        }
        
        return y;
}

/*
 * Convert a row/column cell position to the x/y of its upper left corner
 * wrt the window it will be drawn in (either the matrix window for
 * totally fixed cells/labels, or a clip window for non-fixed).
 */
Widget xbaeRowColToClipXY(XbaeMatrixWidget mw, int row, int column, int *x, int *y)
{
        Widget w = xbaeGetCellClip(mw, row, column);

        *x = xbaeColumnToMatrixX(mw, column);
        *y = xbaeRowToMatrixY(mw, row);

        if (w != (Widget) mw) {
                *x -= w->core.x;
                *y -= w->core.y;
        }
        
        return w;
}

void xbaePositionWidgetOverCell(XbaeMatrixWidget mw, Widget w, int row, int column)
{
        Widget new_parent = xbaeGetCellClip(mw, row, column);

        int x = xbaeColumnToMatrixX(mw, column) + mw->matrix.cell_shadow_thickness;
        int y = xbaeRowToMatrixY(mw, row) + mw->matrix.cell_shadow_thickness;
        int width = COLUMN_WIDTH(mw, column) - 2 * mw->matrix.cell_shadow_thickness;
        int height = ROW_HEIGHT(mw, row) - 2 * mw->matrix.cell_shadow_thickness;

        if (IS_FILL_COLUMN(mw, column) && mw->matrix.horz_fill) {
                width += EMPTY_WIDTH(mw);
        }

        if (IS_FILL_ROW(mw, row) && mw->matrix.vert_fill) {
                height += EMPTY_HEIGHT(mw);
        }
        
        #if 1
        /* This works, but the TextField flickers when scrolling (offset by labelsize + fixed) */
        XtConfigureWidget(w,
           /* position */ x, y,
           /* size     */ width, height,
           /* bw       */ XtBorderWidth(w));
        #else 
        /* The TextField doesn't flicker but toggle button cell widgets don't work anymore */
        XtResizeWidget(w,
            /* size     */ width, height, 
            /* bw       */ XtBorderWidth(w));
        #endif
        
        XtVaSetValues(w, XmNattachRow, row, XmNattachColumn, column, NULL);
        
        if (XtWindow(new_parent)) {
                if (new_parent != (Widget) mw) {
                        /* The widget is drawn in one of the clip windows */
                        x -= new_parent->core.x;
                        y -= new_parent->core.y;
                }
                XReparentWindow(XtDisplay(mw), XtWindow(w), XtWindow(new_parent), x, y);
        }
}

void xbaePositionCellWidget(XbaeMatrixWidget mw, int row, int column)
{
        Widget cellWidget = mw->matrix.per_cell ? mw->matrix.per_cell[row][column].widget : NULL;
        
        if (cellWidget && XtIsRealized(cellWidget) && XtIsManaged(cellWidget)) {
                xbaePositionWidgetOverCell(mw, cellWidget, row, column);
        }
}

void xbaePositionTextField(XbaeMatrixWidget mw)
{
        int current_row, current_column;
        XtVaGetValues(TextField(mw), XmNattachRow, &current_row, XmNattachColumn, &current_column, NULL);

        xbaePositionWidgetOverCell(mw, TextField(mw), current_row, current_column);

        /*
         * The TextField is now visible
         */
        mw->matrix.text_field_is_mapped = True;
}

void xbaeHideCellWidget(XbaeMatrixWidget mw, Widget w)
{
        Dimension width = XtWidth(w);
        Dimension height = XtHeight(w);
        Dimension bw = XtBorderWidth(w);

        XtVaSetValues(w, XmNattachRow, -1, XmNattachColumn, -1, NULL);

        XtConfigureWidget(w,
                         /* position */	-1 - width - bw, -1 - height - bw,
                         /* size */	width, height,
                         /* bw */	bw);
}

void xbaeHideTextField(XbaeMatrixWidget mw)
{
        /*
         * Let Xt believe the TextField is still visible so we can traverse to it
         */
        XtConfigureWidget(TextField(mw),
           /* position */ 0, 0,
           /* size     */ 1, 1,
           /* bw       */ XtBorderWidth(TextField(mw)));
        
        /*
         * But have X not show the window
         */
        if (XtIsRealized(TextField(mw)))
                XReparentWindow(XtDisplay(mw), XtWindow(TextField(mw)), XtWindow(mw), -1, -1);
        
        /*
         * The TextField is now hidden
         */
        mw->matrix.text_field_is_mapped = False;
}

void xbaeSetInitialFocus(XbaeMatrixWidget mw)
{
        int row = xbaeTopRow(mw);
        int column = xbaeLeftColumn(mw);
        Widget widget;
        
        if (mw->matrix.per_cell && mw->matrix.per_cell[row][column].widget) {
                widget = mw->matrix.per_cell[row][column].widget;
        } else {
                widget = TextField(mw);
        }

        if (widget != mw->manager.initial_focus) {
                XtVaSetValues((Widget) mw, 
                              XmNinitialFocus, widget,
                              NULL);
        }
}

void xbaeSaneRectangle(XbaeMatrixWidget mw, XRectangle *rect_p, int rs, int cs, int re, int ce){

        int x1,x2,y1,y2;

        x1 = xbaeColumnToMatrixX(mw, cs);
        if (!IS_FIXED_COLUMN(mw, cs)) {
                if(x1 < NON_FIXED_COLUMN_POSITION(mw)) {
                        x1 = NON_FIXED_COLUMN_POSITION(mw);
                } else if(x1 >= TRAILING_FIXED_COLUMN_POSITION(mw)) {
                        x1 = TRAILING_FIXED_COLUMN_POSITION(mw) - 1;
                }
        }

        x2 = xbaeColumnToMatrixX(mw, ce) + ((ce == -1) ? ROW_LABEL_WIDTH(mw) : COLUMN_WIDTH(mw, ce)) - 1;
        if (!IS_FIXED_COLUMN(mw, ce)) {
                if(x2 < NON_FIXED_COLUMN_POSITION(mw)) {
                        x2 = NON_FIXED_COLUMN_POSITION(mw);
                } else if(x2 >= TRAILING_FIXED_COLUMN_POSITION(mw)) {
                        x2 = TRAILING_FIXED_COLUMN_POSITION(mw) - 1;
                }
        }

        y1 = xbaeRowToMatrixY(mw, rs);
        if (!IS_FIXED_ROW(mw, rs)) {
                if(y1 < NON_FIXED_ROW_POSITION(mw)) {
                        y1 = NON_FIXED_ROW_POSITION(mw);
                } else if(y1 >= TRAILING_FIXED_ROW_POSITION(mw)) {
                        y1 = TRAILING_FIXED_ROW_POSITION(mw) - 1;
                }
        }

        y2 = xbaeRowToMatrixY(mw, re) + ((re == -1) ? COLUMN_LABEL_HEIGHT(mw) : ROW_HEIGHT(mw, re)) - 1;
        if (!IS_FIXED_ROW(mw, re)) {
                if(y2 < NON_FIXED_ROW_POSITION(mw)) {
                        y2 = NON_FIXED_ROW_POSITION(mw);
                } else if(y2 >= TRAILING_FIXED_ROW_POSITION(mw)) {
                        y2 = TRAILING_FIXED_ROW_POSITION(mw) - 1;
                }
        }
        
        rect_p->x = x1;
        rect_p->y = y1;
        rect_p->width = x2 - x1 + 1;
        rect_p->height = y2 - y1 + 1;
}

/*
 * Below are some functions to deal with a multi-threaded environment.
 * Use these instead of the Xt stuff, to ensure that we can still cope
 * with X11r5 where this stuff didn't exist.
 */
#ifdef  XtSpecificationRelease
#if     XtSpecificationRelease > 5
#define R6plus
#endif
#endif

void xbaeObjectLock(Widget w)
{
#ifdef  R6plus
        if (XmIsGadget(w))
                XtAppLock(XtWidgetToApplicationContext(XtParent(w)));
        else
                XtAppLock(XtWidgetToApplicationContext(w));
#endif
}

void xbaeObjectUnlock(Widget w)
{
#ifdef  R6plus
        if (XmIsGadget(w))
                XtAppUnlock(XtWidgetToApplicationContext(XtParent(w)));
        else
                XtAppUnlock(XtWidgetToApplicationContext(w));
#endif
}

void xbaeScrollRows(XbaeMatrixWidget mw, int step)
{
        int value, slider_size, increment, page_increment, limit;
        XtVaGetValues(VertScrollChild(mw), (step < 0 ? XmNminimum : XmNmaximum), &limit, NULL);
        XmScrollBarGetValues(VertScrollChild(mw), &value, &slider_size, &increment,
                             &page_increment);
        if (step < 0) {
                XmScrollBarSetValues(VertScrollChild(mw),
                                     ((value + step < limit) ? limit : value + step), slider_size,
                                     increment, page_increment, True);
        } else {
                limit -= slider_size;
                XmScrollBarSetValues(VertScrollChild(mw),
                                     ((value + step > limit) ? limit : value + step), slider_size,
                                     increment, page_increment, True);
        }
}

void xbaeScrollColumns(XbaeMatrixWidget mw, int step)
{
        int value, slider_size, increment, page_increment, limit;
        XtVaGetValues(HorizScrollChild(mw), (step < 0 ? XmNminimum : XmNmaximum), &limit, NULL);
        XmScrollBarGetValues(HorizScrollChild(mw), &value, &slider_size, &increment,
                             &page_increment);
        if (step < 0) {
                XmScrollBarSetValues(HorizScrollChild(mw),
                                     ((value + step < limit) ? limit : value + step), slider_size,
                                     increment, page_increment, True);
        } else {
                limit -= slider_size;
                XmScrollBarSetValues(HorizScrollChild(mw),
                                     ((value + step > limit) ? limit : value + step), slider_size,
                                     increment, page_increment, True);
        }
}
