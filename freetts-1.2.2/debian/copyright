This work was packaged for Debian by:

    Bdale Garbee <bdale@gag.com> on Sat, 03 Apr 2010 09:57:48 -0600

It was downloaded from:

    http://freetts.sourceforge.net

Upstream Author(s):

    FreeTTS was built by the Speech Integration Group of Sun Microsystems 
    Laboratories, who may be contacted via the FreeTTS Forums:

	http://sourceforge.net/projects/freetts/forums

	* Willie Walker, Manager and Principal Investigator
	* Paul Lamere, Staff Engineer
	* Philip Kwok, Member of Technical Staff


    FreeTTS is based on CMU's Flite, written by:

	* Alan Black
	* Kevin Lenzo

    Kevin and Alan generated the data used by FreeTTS.  In addition, Kevin 
    is the voice behind the diphone voices (kevin 8k, kevin 16k), and Alan 
    is the voice behind the speaking clock.

    Support for MBROLA voice output was contributed by Marc Schröder, 
    text-to-speech Researcher in the Language Technology Lab at DFKI, 
    Saarbrücken, Germany.

    Support for importing FestVox voices into FreeTTS, and support for 
    dynamically discovering and loading voices was developed by David Vos, 
    a Sun Microsystems Laboratories student intern.

Copyright:

    Portions Copyright 2001-2004 Sun Microsystems, Inc.  

    Portions Copyright 1999-2001 Language Technologies Institute,
    Carnegie Mellon University.  

License:

    Permission is hereby granted, free of charge, to use and distribute
    this software and its documentation without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of this work, and to
    permit persons to whom this work is furnished to do so, subject to
    the following conditions:
    
     1. The code must retain the above copyright notice, this list of
        conditions and the following disclaimer.
     2. Any modifications must be clearly marked as such.
     3. Original authors' names are not deleted.
     4. The authors' names are not used to endorse or promote products
        derived from this software without specific prior written
        permission.
    
    SUN MICROSYSTEMS, INC., CARNEGIE MELLON UNIVERSITY AND THE
    CONTRIBUTORS TO THIS WORK DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
    SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
    FITNESS, IN NO EVENT SHALL SUN MICROSYSTEMS, INC., CARNEGIE MELLON
    UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
    CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
    USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

The Debian packaging is:

    Copyright (C) 2010 Bdale Garbee <bdale@gag.com>

and is licensed under the GPL version 3,
see "/usr/share/common-licenses/GPL-3".

