/* $Id: 8f0192b37cd36abbb44383a0b38941dd12439f87 $ */
#ifdef E2BIG
add_integer_constant("E2BIG", E2BIG, 0);
#else /* !E2BIG */
#ifdef WSAE2BIG
add_integer_constant("E2BIG", WSAE2BIG, 0);
#endif /* WSAE2BIG */
#endif /* E2BIG */
#ifdef WSAE2BIG
add_integer_constant("WSAE2BIG", WSAE2BIG, 0);
#endif /* WSAE2BIG */
#ifdef EACCES
add_integer_constant("EACCES", EACCES, 0);
#else /* !EACCES */
#ifdef WSAEACCES
add_integer_constant("EACCES", WSAEACCES, 0);
#endif /* WSAEACCES */
#endif /* EACCES */
#ifdef WSAEACCES
add_integer_constant("WSAEACCES", WSAEACCES, 0);
#endif /* WSAEACCES */
#ifdef EADDRINUSE
add_integer_constant("EADDRINUSE", EADDRINUSE, 0);
#else /* !EADDRINUSE */
#ifdef WSAEADDRINUSE
add_integer_constant("EADDRINUSE", WSAEADDRINUSE, 0);
#endif /* WSAEADDRINUSE */
#endif /* EADDRINUSE */
#ifdef WSAEADDRINUSE
add_integer_constant("WSAEADDRINUSE", WSAEADDRINUSE, 0);
#endif /* WSAEADDRINUSE */
#ifdef EADDRNOTAVAIL
add_integer_constant("EADDRNOTAVAIL", EADDRNOTAVAIL, 0);
#else /* !EADDRNOTAVAIL */
#ifdef WSAEADDRNOTAVAIL
add_integer_constant("EADDRNOTAVAIL", WSAEADDRNOTAVAIL, 0);
#endif /* WSAEADDRNOTAVAIL */
#endif /* EADDRNOTAVAIL */
#ifdef WSAEADDRNOTAVAIL
add_integer_constant("WSAEADDRNOTAVAIL", WSAEADDRNOTAVAIL, 0);
#endif /* WSAEADDRNOTAVAIL */
#ifdef EADV
add_integer_constant("EADV", EADV, 0);
#else /* !EADV */
#ifdef WSAEADV
add_integer_constant("EADV", WSAEADV, 0);
#endif /* WSAEADV */
#endif /* EADV */
#ifdef WSAEADV
add_integer_constant("WSAEADV", WSAEADV, 0);
#endif /* WSAEADV */
#ifdef EAFNOSUPPORT
add_integer_constant("EAFNOSUPPORT", EAFNOSUPPORT, 0);
#else /* !EAFNOSUPPORT */
#ifdef WSAEAFNOSUPPORT
add_integer_constant("EAFNOSUPPORT", WSAEAFNOSUPPORT, 0);
#endif /* WSAEAFNOSUPPORT */
#endif /* EAFNOSUPPORT */
#ifdef WSAEAFNOSUPPORT
add_integer_constant("WSAEAFNOSUPPORT", WSAEAFNOSUPPORT, 0);
#endif /* WSAEAFNOSUPPORT */
#ifdef EAGAIN
add_integer_constant("EAGAIN", EAGAIN, 0);
#else /* !EAGAIN */
#ifdef WSAEAGAIN
add_integer_constant("EAGAIN", WSAEAGAIN, 0);
#endif /* WSAEAGAIN */
#endif /* EAGAIN */
#ifdef WSAEAGAIN
add_integer_constant("WSAEAGAIN", WSAEAGAIN, 0);
#endif /* WSAEAGAIN */
#ifdef EALREADY
add_integer_constant("EALREADY", EALREADY, 0);
#else /* !EALREADY */
#ifdef WSAEALREADY
add_integer_constant("EALREADY", WSAEALREADY, 0);
#endif /* WSAEALREADY */
#endif /* EALREADY */
#ifdef WSAEALREADY
add_integer_constant("WSAEALREADY", WSAEALREADY, 0);
#endif /* WSAEALREADY */
#ifdef EBADE
add_integer_constant("EBADE", EBADE, 0);
#else /* !EBADE */
#ifdef WSAEBADE
add_integer_constant("EBADE", WSAEBADE, 0);
#endif /* WSAEBADE */
#endif /* EBADE */
#ifdef WSAEBADE
add_integer_constant("WSAEBADE", WSAEBADE, 0);
#endif /* WSAEBADE */
#ifdef EBADF
add_integer_constant("EBADF", EBADF, 0);
#else /* !EBADF */
#ifdef WSAEBADF
add_integer_constant("EBADF", WSAEBADF, 0);
#endif /* WSAEBADF */
#endif /* EBADF */
#ifdef WSAEBADF
add_integer_constant("WSAEBADF", WSAEBADF, 0);
#endif /* WSAEBADF */
#ifdef EBADFD
add_integer_constant("EBADFD", EBADFD, 0);
#else /* !EBADFD */
#ifdef WSAEBADFD
add_integer_constant("EBADFD", WSAEBADFD, 0);
#endif /* WSAEBADFD */
#endif /* EBADFD */
#ifdef WSAEBADFD
add_integer_constant("WSAEBADFD", WSAEBADFD, 0);
#endif /* WSAEBADFD */
#ifdef EBADMSG
add_integer_constant("EBADMSG", EBADMSG, 0);
#else /* !EBADMSG */
#ifdef WSAEBADMSG
add_integer_constant("EBADMSG", WSAEBADMSG, 0);
#endif /* WSAEBADMSG */
#endif /* EBADMSG */
#ifdef WSAEBADMSG
add_integer_constant("WSAEBADMSG", WSAEBADMSG, 0);
#endif /* WSAEBADMSG */
#ifdef EBADR
add_integer_constant("EBADR", EBADR, 0);
#else /* !EBADR */
#ifdef WSAEBADR
add_integer_constant("EBADR", WSAEBADR, 0);
#endif /* WSAEBADR */
#endif /* EBADR */
#ifdef WSAEBADR
add_integer_constant("WSAEBADR", WSAEBADR, 0);
#endif /* WSAEBADR */
#ifdef EBADRPC
add_integer_constant("EBADRPC", EBADRPC, 0);
#else /* !EBADRPC */
#ifdef WSAEBADRPC
add_integer_constant("EBADRPC", WSAEBADRPC, 0);
#endif /* WSAEBADRPC */
#endif /* EBADRPC */
#ifdef WSAEBADRPC
add_integer_constant("WSAEBADRPC", WSAEBADRPC, 0);
#endif /* WSAEBADRPC */
#ifdef EBADRQC
add_integer_constant("EBADRQC", EBADRQC, 0);
#else /* !EBADRQC */
#ifdef WSAEBADRQC
add_integer_constant("EBADRQC", WSAEBADRQC, 0);
#endif /* WSAEBADRQC */
#endif /* EBADRQC */
#ifdef WSAEBADRQC
add_integer_constant("WSAEBADRQC", WSAEBADRQC, 0);
#endif /* WSAEBADRQC */
#ifdef EBADSLT
add_integer_constant("EBADSLT", EBADSLT, 0);
#else /* !EBADSLT */
#ifdef WSAEBADSLT
add_integer_constant("EBADSLT", WSAEBADSLT, 0);
#endif /* WSAEBADSLT */
#endif /* EBADSLT */
#ifdef WSAEBADSLT
add_integer_constant("WSAEBADSLT", WSAEBADSLT, 0);
#endif /* WSAEBADSLT */
#ifdef EBFONT
add_integer_constant("EBFONT", EBFONT, 0);
#else /* !EBFONT */
#ifdef WSAEBFONT
add_integer_constant("EBFONT", WSAEBFONT, 0);
#endif /* WSAEBFONT */
#endif /* EBFONT */
#ifdef WSAEBFONT
add_integer_constant("WSAEBFONT", WSAEBFONT, 0);
#endif /* WSAEBFONT */
#ifdef EBUFSIZE
add_integer_constant("EBUFSIZE", EBUFSIZE, 0);
#else /* !EBUFSIZE */
#ifdef WSAEBUFSIZE
add_integer_constant("EBUFSIZE", WSAEBUFSIZE, 0);
#endif /* WSAEBUFSIZE */
#endif /* EBUFSIZE */
#ifdef WSAEBUFSIZE
add_integer_constant("WSAEBUFSIZE", WSAEBUFSIZE, 0);
#endif /* WSAEBUFSIZE */
#ifdef EBUSY
add_integer_constant("EBUSY", EBUSY, 0);
#else /* !EBUSY */
#ifdef WSAEBUSY
add_integer_constant("EBUSY", WSAEBUSY, 0);
#endif /* WSAEBUSY */
#endif /* EBUSY */
#ifdef WSAEBUSY
add_integer_constant("WSAEBUSY", WSAEBUSY, 0);
#endif /* WSAEBUSY */
#ifdef ECANCELED
add_integer_constant("ECANCELED", ECANCELED, 0);
#else /* !ECANCELED */
#ifdef WSAECANCELED
add_integer_constant("ECANCELED", WSAECANCELED, 0);
#endif /* WSAECANCELED */
#endif /* ECANCELED */
#ifdef WSAECANCELED
add_integer_constant("WSAECANCELED", WSAECANCELED, 0);
#endif /* WSAECANCELED */
#ifdef ECANTEXTENT
add_integer_constant("ECANTEXTENT", ECANTEXTENT, 0);
#else /* !ECANTEXTENT */
#ifdef WSAECANTEXTENT
add_integer_constant("ECANTEXTENT", WSAECANTEXTENT, 0);
#endif /* WSAECANTEXTENT */
#endif /* ECANTEXTENT */
#ifdef WSAECANTEXTENT
add_integer_constant("WSAECANTEXTENT", WSAECANTEXTENT, 0);
#endif /* WSAECANTEXTENT */
#ifdef ECHILD
add_integer_constant("ECHILD", ECHILD, 0);
#else /* !ECHILD */
#ifdef WSAECHILD
add_integer_constant("ECHILD", WSAECHILD, 0);
#endif /* WSAECHILD */
#endif /* ECHILD */
#ifdef WSAECHILD
add_integer_constant("WSAECHILD", WSAECHILD, 0);
#endif /* WSAECHILD */
#ifdef ECHRNG
add_integer_constant("ECHRNG", ECHRNG, 0);
#else /* !ECHRNG */
#ifdef WSAECHRNG
add_integer_constant("ECHRNG", WSAECHRNG, 0);
#endif /* WSAECHRNG */
#endif /* ECHRNG */
#ifdef WSAECHRNG
add_integer_constant("WSAECHRNG", WSAECHRNG, 0);
#endif /* WSAECHRNG */
#ifdef ECLOCKCPU
add_integer_constant("ECLOCKCPU", ECLOCKCPU, 0);
#else /* !ECLOCKCPU */
#ifdef WSAECLOCKCPU
add_integer_constant("ECLOCKCPU", WSAECLOCKCPU, 0);
#endif /* WSAECLOCKCPU */
#endif /* ECLOCKCPU */
#ifdef WSAECLOCKCPU
add_integer_constant("WSAECLOCKCPU", WSAECLOCKCPU, 0);
#endif /* WSAECLOCKCPU */
#ifdef ECLONEME
add_integer_constant("ECLONEME", ECLONEME, 0);
#else /* !ECLONEME */
#ifdef WSAECLONEME
add_integer_constant("ECLONEME", WSAECLONEME, 0);
#endif /* WSAECLONEME */
#endif /* ECLONEME */
#ifdef WSAECLONEME
add_integer_constant("WSAECLONEME", WSAECLONEME, 0);
#endif /* WSAECLONEME */
#ifdef ECOMM
add_integer_constant("ECOMM", ECOMM, 0);
#else /* !ECOMM */
#ifdef WSAECOMM
add_integer_constant("ECOMM", WSAECOMM, 0);
#endif /* WSAECOMM */
#endif /* ECOMM */
#ifdef WSAECOMM
add_integer_constant("WSAECOMM", WSAECOMM, 0);
#endif /* WSAECOMM */
#ifdef ECONNABORTED
add_integer_constant("ECONNABORTED", ECONNABORTED, 0);
#else /* !ECONNABORTED */
#ifdef WSAECONNABORTED
add_integer_constant("ECONNABORTED", WSAECONNABORTED, 0);
#endif /* WSAECONNABORTED */
#endif /* ECONNABORTED */
#ifdef WSAECONNABORTED
add_integer_constant("WSAECONNABORTED", WSAECONNABORTED, 0);
#endif /* WSAECONNABORTED */
#ifdef ECONNREFUSED
add_integer_constant("ECONNREFUSED", ECONNREFUSED, 0);
#else /* !ECONNREFUSED */
#ifdef WSAECONNREFUSED
add_integer_constant("ECONNREFUSED", WSAECONNREFUSED, 0);
#endif /* WSAECONNREFUSED */
#endif /* ECONNREFUSED */
#ifdef WSAECONNREFUSED
add_integer_constant("WSAECONNREFUSED", WSAECONNREFUSED, 0);
#endif /* WSAECONNREFUSED */
#ifdef ECONNRESET
add_integer_constant("ECONNRESET", ECONNRESET, 0);
#else /* !ECONNRESET */
#ifdef WSAECONNRESET
add_integer_constant("ECONNRESET", WSAECONNRESET, 0);
#endif /* WSAECONNRESET */
#endif /* ECONNRESET */
#ifdef WSAECONNRESET
add_integer_constant("WSAECONNRESET", WSAECONNRESET, 0);
#endif /* WSAECONNRESET */
#ifdef ECONTROLLER
add_integer_constant("ECONTROLLER", ECONTROLLER, 0);
#else /* !ECONTROLLER */
#ifdef WSAECONTROLLER
add_integer_constant("ECONTROLLER", WSAECONTROLLER, 0);
#endif /* WSAECONTROLLER */
#endif /* ECONTROLLER */
#ifdef WSAECONTROLLER
add_integer_constant("WSAECONTROLLER", WSAECONTROLLER, 0);
#endif /* WSAECONTROLLER */
#ifdef ECORRUPT
add_integer_constant("ECORRUPT", ECORRUPT, 0);
#else /* !ECORRUPT */
#ifdef WSAECORRUPT
add_integer_constant("ECORRUPT", WSAECORRUPT, 0);
#endif /* WSAECORRUPT */
#endif /* ECORRUPT */
#ifdef WSAECORRUPT
add_integer_constant("WSAECORRUPT", WSAECORRUPT, 0);
#endif /* WSAECORRUPT */
#ifdef EDEADLK
add_integer_constant("EDEADLK", EDEADLK, 0);
#else /* !EDEADLK */
#ifdef WSAEDEADLK
add_integer_constant("EDEADLK", WSAEDEADLK, 0);
#endif /* WSAEDEADLK */
#endif /* EDEADLK */
#ifdef WSAEDEADLK
add_integer_constant("WSAEDEADLK", WSAEDEADLK, 0);
#endif /* WSAEDEADLK */
#ifdef EDEADLOCK
add_integer_constant("EDEADLOCK", EDEADLOCK, 0);
#else /* !EDEADLOCK */
#ifdef WSAEDEADLOCK
add_integer_constant("EDEADLOCK", WSAEDEADLOCK, 0);
#endif /* WSAEDEADLOCK */
#endif /* EDEADLOCK */
#ifdef WSAEDEADLOCK
add_integer_constant("WSAEDEADLOCK", WSAEDEADLOCK, 0);
#endif /* WSAEDEADLOCK */
#ifdef EDESTADDREQ
add_integer_constant("EDESTADDREQ", EDESTADDREQ, 0);
#else /* !EDESTADDREQ */
#ifdef WSAEDESTADDREQ
add_integer_constant("EDESTADDREQ", WSAEDESTADDREQ, 0);
#endif /* WSAEDESTADDREQ */
#endif /* EDESTADDREQ */
#ifdef WSAEDESTADDREQ
add_integer_constant("WSAEDESTADDREQ", WSAEDESTADDREQ, 0);
#endif /* WSAEDESTADDREQ */
#ifdef EDESTADDRREQ
add_integer_constant("EDESTADDRREQ", EDESTADDRREQ, 0);
#else /* !EDESTADDRREQ */
#ifdef WSAEDESTADDRREQ
add_integer_constant("EDESTADDRREQ", WSAEDESTADDRREQ, 0);
#endif /* WSAEDESTADDRREQ */
#endif /* EDESTADDRREQ */
#ifdef WSAEDESTADDRREQ
add_integer_constant("WSAEDESTADDRREQ", WSAEDESTADDRREQ, 0);
#endif /* WSAEDESTADDRREQ */
#ifdef EDESTROYED
add_integer_constant("EDESTROYED", EDESTROYED, 0);
#else /* !EDESTROYED */
#ifdef WSAEDESTROYED
add_integer_constant("EDESTROYED", WSAEDESTROYED, 0);
#endif /* WSAEDESTROYED */
#endif /* EDESTROYED */
#ifdef WSAEDESTROYED
add_integer_constant("WSAEDESTROYED", WSAEDESTROYED, 0);
#endif /* WSAEDESTROYED */
#ifdef EDIRCORRUPTED
add_integer_constant("EDIRCORRUPTED", EDIRCORRUPTED, 0);
#else /* !EDIRCORRUPTED */
#ifdef WSAEDIRCORRUPTED
add_integer_constant("EDIRCORRUPTED", WSAEDIRCORRUPTED, 0);
#endif /* WSAEDIRCORRUPTED */
#endif /* EDIRCORRUPTED */
#ifdef WSAEDIRCORRUPTED
add_integer_constant("WSAEDIRCORRUPTED", WSAEDIRCORRUPTED, 0);
#endif /* WSAEDIRCORRUPTED */
#ifdef EDIRTY
add_integer_constant("EDIRTY", EDIRTY, 0);
#else /* !EDIRTY */
#ifdef WSAEDIRTY
add_integer_constant("EDIRTY", WSAEDIRTY, 0);
#endif /* WSAEDIRTY */
#endif /* EDIRTY */
#ifdef WSAEDIRTY
add_integer_constant("WSAEDIRTY", WSAEDIRTY, 0);
#endif /* WSAEDIRTY */
#ifdef EDISJOINT
add_integer_constant("EDISJOINT", EDISJOINT, 0);
#else /* !EDISJOINT */
#ifdef WSAEDISJOINT
add_integer_constant("EDISJOINT", WSAEDISJOINT, 0);
#endif /* WSAEDISJOINT */
#endif /* EDISJOINT */
#ifdef WSAEDISJOINT
add_integer_constant("WSAEDISJOINT", WSAEDISJOINT, 0);
#endif /* WSAEDISJOINT */
#ifdef EDIST
add_integer_constant("EDIST", EDIST, 0);
#else /* !EDIST */
#ifdef WSAEDIST
add_integer_constant("EDIST", WSAEDIST, 0);
#endif /* WSAEDIST */
#endif /* EDIST */
#ifdef WSAEDIST
add_integer_constant("WSAEDIST", WSAEDIST, 0);
#endif /* WSAEDIST */
#ifdef EDOM
add_integer_constant("EDOM", EDOM, 0);
#else /* !EDOM */
#ifdef WSAEDOM
add_integer_constant("EDOM", WSAEDOM, 0);
#endif /* WSAEDOM */
#endif /* EDOM */
#ifdef WSAEDOM
add_integer_constant("WSAEDOM", WSAEDOM, 0);
#endif /* WSAEDOM */
#ifdef EDOTDOT
add_integer_constant("EDOTDOT", EDOTDOT, 0);
#else /* !EDOTDOT */
#ifdef WSAEDOTDOT
add_integer_constant("EDOTDOT", WSAEDOTDOT, 0);
#endif /* WSAEDOTDOT */
#endif /* EDOTDOT */
#ifdef WSAEDOTDOT
add_integer_constant("WSAEDOTDOT", WSAEDOTDOT, 0);
#endif /* WSAEDOTDOT */
#ifdef EDQUOT
add_integer_constant("EDQUOT", EDQUOT, 0);
#else /* !EDQUOT */
#ifdef WSAEDQUOT
add_integer_constant("EDQUOT", WSAEDQUOT, 0);
#endif /* WSAEDQUOT */
#endif /* EDQUOT */
#ifdef WSAEDQUOT
add_integer_constant("WSAEDQUOT", WSAEDQUOT, 0);
#endif /* WSAEDQUOT */
#ifdef EDUPPKG
add_integer_constant("EDUPPKG", EDUPPKG, 0);
#else /* !EDUPPKG */
#ifdef WSAEDUPPKG
add_integer_constant("EDUPPKG", WSAEDUPPKG, 0);
#endif /* WSAEDUPPKG */
#endif /* EDUPPKG */
#ifdef WSAEDUPPKG
add_integer_constant("WSAEDUPPKG", WSAEDUPPKG, 0);
#endif /* WSAEDUPPKG */
#ifdef EEMPTY
add_integer_constant("EEMPTY", EEMPTY, 0);
#else /* !EEMPTY */
#ifdef WSAEEMPTY
add_integer_constant("EEMPTY", WSAEEMPTY, 0);
#endif /* WSAEEMPTY */
#endif /* EEMPTY */
#ifdef WSAEEMPTY
add_integer_constant("WSAEEMPTY", WSAEEMPTY, 0);
#endif /* WSAEEMPTY */
#ifdef EENDOFMINOR
add_integer_constant("EENDOFMINOR", EENDOFMINOR, 0);
#else /* !EENDOFMINOR */
#ifdef WSAEENDOFMINOR
add_integer_constant("EENDOFMINOR", WSAEENDOFMINOR, 0);
#endif /* WSAEENDOFMINOR */
#endif /* EENDOFMINOR */
#ifdef WSAEENDOFMINOR
add_integer_constant("WSAEENDOFMINOR", WSAEENDOFMINOR, 0);
#endif /* WSAEENDOFMINOR */
#ifdef EENQUEUED
add_integer_constant("EENQUEUED", EENQUEUED, 0);
#else /* !EENQUEUED */
#ifdef WSAEENQUEUED
add_integer_constant("EENQUEUED", WSAEENQUEUED, 0);
#endif /* WSAEENQUEUED */
#endif /* EENQUEUED */
#ifdef WSAEENQUEUED
add_integer_constant("WSAEENQUEUED", WSAEENQUEUED, 0);
#endif /* WSAEENQUEUED */
#ifdef EEXIST
add_integer_constant("EEXIST", EEXIST, 0);
#else /* !EEXIST */
#ifdef WSAEEXIST
add_integer_constant("EEXIST", WSAEEXIST, 0);
#endif /* WSAEEXIST */
#endif /* EEXIST */
#ifdef WSAEEXIST
add_integer_constant("WSAEEXIST", WSAEEXIST, 0);
#endif /* WSAEEXIST */
#ifdef EFAIL
add_integer_constant("EFAIL", EFAIL, 0);
#else /* !EFAIL */
#ifdef WSAEFAIL
add_integer_constant("EFAIL", WSAEFAIL, 0);
#endif /* WSAEFAIL */
#endif /* EFAIL */
#ifdef WSAEFAIL
add_integer_constant("WSAEFAIL", WSAEFAIL, 0);
#endif /* WSAEFAIL */
#ifdef EFAULT
add_integer_constant("EFAULT", EFAULT, 0);
#else /* !EFAULT */
#ifdef WSAEFAULT
add_integer_constant("EFAULT", WSAEFAULT, 0);
#endif /* WSAEFAULT */
#endif /* EFAULT */
#ifdef WSAEFAULT
add_integer_constant("WSAEFAULT", WSAEFAULT, 0);
#endif /* WSAEFAULT */
#ifdef EFBIG
add_integer_constant("EFBIG", EFBIG, 0);
#else /* !EFBIG */
#ifdef WSAEFBIG
add_integer_constant("EFBIG", WSAEFBIG, 0);
#endif /* WSAEFBIG */
#endif /* EFBIG */
#ifdef WSAEFBIG
add_integer_constant("WSAEFBIG", WSAEFBIG, 0);
#endif /* WSAEFBIG */
#ifdef EFORMAT
add_integer_constant("EFORMAT", EFORMAT, 0);
#else /* !EFORMAT */
#ifdef WSAEFORMAT
add_integer_constant("EFORMAT", WSAEFORMAT, 0);
#endif /* WSAEFORMAT */
#endif /* EFORMAT */
#ifdef WSAEFORMAT
add_integer_constant("WSAEFORMAT", WSAEFORMAT, 0);
#endif /* WSAEFORMAT */
#ifdef EFTYPE
add_integer_constant("EFTYPE", EFTYPE, 0);
#else /* !EFTYPE */
#ifdef WSAEFTYPE
add_integer_constant("EFTYPE", WSAEFTYPE, 0);
#endif /* WSAEFTYPE */
#endif /* EFTYPE */
#ifdef WSAEFTYPE
add_integer_constant("WSAEFTYPE", WSAEFTYPE, 0);
#endif /* WSAEFTYPE */
#ifdef EGROUPLOOP
add_integer_constant("EGROUPLOOP", EGROUPLOOP, 0);
#else /* !EGROUPLOOP */
#ifdef WSAEGROUPLOOP
add_integer_constant("EGROUPLOOP", WSAEGROUPLOOP, 0);
#endif /* WSAEGROUPLOOP */
#endif /* EGROUPLOOP */
#ifdef WSAEGROUPLOOP
add_integer_constant("WSAEGROUPLOOP", WSAEGROUPLOOP, 0);
#endif /* WSAEGROUPLOOP */
#ifdef EHOSTDOWN
add_integer_constant("EHOSTDOWN", EHOSTDOWN, 0);
#else /* !EHOSTDOWN */
#ifdef WSAEHOSTDOWN
add_integer_constant("EHOSTDOWN", WSAEHOSTDOWN, 0);
#endif /* WSAEHOSTDOWN */
#endif /* EHOSTDOWN */
#ifdef WSAEHOSTDOWN
add_integer_constant("WSAEHOSTDOWN", WSAEHOSTDOWN, 0);
#endif /* WSAEHOSTDOWN */
#ifdef EHOSTUNREACH
add_integer_constant("EHOSTUNREACH", EHOSTUNREACH, 0);
#else /* !EHOSTUNREACH */
#ifdef WSAEHOSTUNREACH
add_integer_constant("EHOSTUNREACH", WSAEHOSTUNREACH, 0);
#endif /* WSAEHOSTUNREACH */
#endif /* EHOSTUNREACH */
#ifdef WSAEHOSTUNREACH
add_integer_constant("WSAEHOSTUNREACH", WSAEHOSTUNREACH, 0);
#endif /* WSAEHOSTUNREACH */
#ifdef EIDRM
add_integer_constant("EIDRM", EIDRM, 0);
#else /* !EIDRM */
#ifdef WSAEIDRM
add_integer_constant("EIDRM", WSAEIDRM, 0);
#endif /* WSAEIDRM */
#endif /* EIDRM */
#ifdef WSAEIDRM
add_integer_constant("WSAEIDRM", WSAEIDRM, 0);
#endif /* WSAEIDRM */
#ifdef EILSEQ
add_integer_constant("EILSEQ", EILSEQ, 0);
#else /* !EILSEQ */
#ifdef WSAEILSEQ
add_integer_constant("EILSEQ", WSAEILSEQ, 0);
#endif /* WSAEILSEQ */
#endif /* EILSEQ */
#ifdef WSAEILSEQ
add_integer_constant("WSAEILSEQ", WSAEILSEQ, 0);
#endif /* WSAEILSEQ */
#ifdef EINIT
add_integer_constant("EINIT", EINIT, 0);
#else /* !EINIT */
#ifdef WSAEINIT
add_integer_constant("EINIT", WSAEINIT, 0);
#endif /* WSAEINIT */
#endif /* EINIT */
#ifdef WSAEINIT
add_integer_constant("WSAEINIT", WSAEINIT, 0);
#endif /* WSAEINIT */
#ifdef EINPROG
add_integer_constant("EINPROG", EINPROG, 0);
#else /* !EINPROG */
#ifdef WSAEINPROG
add_integer_constant("EINPROG", WSAEINPROG, 0);
#endif /* WSAEINPROG */
#endif /* EINPROG */
#ifdef WSAEINPROG
add_integer_constant("WSAEINPROG", WSAEINPROG, 0);
#endif /* WSAEINPROG */
#ifdef EINPROGRESS
add_integer_constant("EINPROGRESS", EINPROGRESS, 0);
#else /* !EINPROGRESS */
#ifdef WSAEINPROGRESS
add_integer_constant("EINPROGRESS", WSAEINPROGRESS, 0);
#endif /* WSAEINPROGRESS */
#endif /* EINPROGRESS */
#ifdef WSAEINPROGRESS
add_integer_constant("WSAEINPROGRESS", WSAEINPROGRESS, 0);
#endif /* WSAEINPROGRESS */
#ifdef EINTR
add_integer_constant("EINTR", EINTR, 0);
#else /* !EINTR */
#ifdef WSAEINTR
add_integer_constant("EINTR", WSAEINTR, 0);
#endif /* WSAEINTR */
#endif /* EINTR */
#ifdef WSAEINTR
add_integer_constant("WSAEINTR", WSAEINTR, 0);
#endif /* WSAEINTR */
#ifdef EINVAL
add_integer_constant("EINVAL", EINVAL, 0);
#else /* !EINVAL */
#ifdef WSAEINVAL
add_integer_constant("EINVAL", WSAEINVAL, 0);
#endif /* WSAEINVAL */
#endif /* EINVAL */
#ifdef WSAEINVAL
add_integer_constant("WSAEINVAL", WSAEINVAL, 0);
#endif /* WSAEINVAL */
#ifdef EINVALMODE
add_integer_constant("EINVALMODE", EINVALMODE, 0);
#else /* !EINVALMODE */
#ifdef WSAEINVALMODE
add_integer_constant("EINVALMODE", WSAEINVALMODE, 0);
#endif /* WSAEINVALMODE */
#endif /* EINVALMODE */
#ifdef WSAEINVALMODE
add_integer_constant("WSAEINVALMODE", WSAEINVALMODE, 0);
#endif /* WSAEINVALMODE */
#ifdef EINVALSTATE
add_integer_constant("EINVALSTATE", EINVALSTATE, 0);
#else /* !EINVALSTATE */
#ifdef WSAEINVALSTATE
add_integer_constant("EINVALSTATE", WSAEINVALSTATE, 0);
#endif /* WSAEINVALSTATE */
#endif /* EINVALSTATE */
#ifdef WSAEINVALSTATE
add_integer_constant("WSAEINVALSTATE", WSAEINVALSTATE, 0);
#endif /* WSAEINVALSTATE */
#ifdef EINVALTIME
add_integer_constant("EINVALTIME", EINVALTIME, 0);
#else /* !EINVALTIME */
#ifdef WSAEINVALTIME
add_integer_constant("EINVALTIME", WSAEINVALTIME, 0);
#endif /* WSAEINVALTIME */
#endif /* EINVALTIME */
#ifdef WSAEINVALTIME
add_integer_constant("WSAEINVALTIME", WSAEINVALTIME, 0);
#endif /* WSAEINVALTIME */
#ifdef EIO
add_integer_constant("EIO", EIO, 0);
#else /* !EIO */
#ifdef WSAEIO
add_integer_constant("EIO", WSAEIO, 0);
#endif /* WSAEIO */
#endif /* EIO */
#ifdef WSAEIO
add_integer_constant("WSAEIO", WSAEIO, 0);
#endif /* WSAEIO */
#ifdef EIORESID
add_integer_constant("EIORESID", EIORESID, 0);
#else /* !EIORESID */
#ifdef WSAEIORESID
add_integer_constant("EIORESID", WSAEIORESID, 0);
#endif /* WSAEIORESID */
#endif /* EIORESID */
#ifdef WSAEIORESID
add_integer_constant("WSAEIORESID", WSAEIORESID, 0);
#endif /* WSAEIORESID */
#ifdef EISCONN
add_integer_constant("EISCONN", EISCONN, 0);
#else /* !EISCONN */
#ifdef WSAEISCONN
add_integer_constant("EISCONN", WSAEISCONN, 0);
#endif /* WSAEISCONN */
#endif /* EISCONN */
#ifdef WSAEISCONN
add_integer_constant("WSAEISCONN", WSAEISCONN, 0);
#endif /* WSAEISCONN */
#ifdef EISDIR
add_integer_constant("EISDIR", EISDIR, 0);
#else /* !EISDIR */
#ifdef WSAEISDIR
add_integer_constant("EISDIR", WSAEISDIR, 0);
#endif /* WSAEISDIR */
#endif /* EISDIR */
#ifdef WSAEISDIR
add_integer_constant("WSAEISDIR", WSAEISDIR, 0);
#endif /* WSAEISDIR */
#ifdef EISNAM
add_integer_constant("EISNAM", EISNAM, 0);
#else /* !EISNAM */
#ifdef WSAEISNAM
add_integer_constant("EISNAM", WSAEISNAM, 0);
#endif /* WSAEISNAM */
#endif /* EISNAM */
#ifdef WSAEISNAM
add_integer_constant("WSAEISNAM", WSAEISNAM, 0);
#endif /* WSAEISNAM */
#ifdef EJOINED
add_integer_constant("EJOINED", EJOINED, 0);
#else /* !EJOINED */
#ifdef WSAEJOINED
add_integer_constant("EJOINED", WSAEJOINED, 0);
#endif /* WSAEJOINED */
#endif /* EJOINED */
#ifdef WSAEJOINED
add_integer_constant("WSAEJOINED", WSAEJOINED, 0);
#endif /* WSAEJOINED */
#ifdef EJUSTRETURN
add_integer_constant("EJUSTRETURN", EJUSTRETURN, 0);
#else /* !EJUSTRETURN */
#ifdef WSAEJUSTRETURN
add_integer_constant("EJUSTRETURN", WSAEJUSTRETURN, 0);
#endif /* WSAEJUSTRETURN */
#endif /* EJUSTRETURN */
#ifdef WSAEJUSTRETURN
add_integer_constant("WSAEJUSTRETURN", WSAEJUSTRETURN, 0);
#endif /* WSAEJUSTRETURN */
#ifdef EL2HLT
add_integer_constant("EL2HLT", EL2HLT, 0);
#else /* !EL2HLT */
#ifdef WSAEL2HLT
add_integer_constant("EL2HLT", WSAEL2HLT, 0);
#endif /* WSAEL2HLT */
#endif /* EL2HLT */
#ifdef WSAEL2HLT
add_integer_constant("WSAEL2HLT", WSAEL2HLT, 0);
#endif /* WSAEL2HLT */
#ifdef EL2NSYNC
add_integer_constant("EL2NSYNC", EL2NSYNC, 0);
#else /* !EL2NSYNC */
#ifdef WSAEL2NSYNC
add_integer_constant("EL2NSYNC", WSAEL2NSYNC, 0);
#endif /* WSAEL2NSYNC */
#endif /* EL2NSYNC */
#ifdef WSAEL2NSYNC
add_integer_constant("WSAEL2NSYNC", WSAEL2NSYNC, 0);
#endif /* WSAEL2NSYNC */
#ifdef EL3HLT
add_integer_constant("EL3HLT", EL3HLT, 0);
#else /* !EL3HLT */
#ifdef WSAEL3HLT
add_integer_constant("EL3HLT", WSAEL3HLT, 0);
#endif /* WSAEL3HLT */
#endif /* EL3HLT */
#ifdef WSAEL3HLT
add_integer_constant("WSAEL3HLT", WSAEL3HLT, 0);
#endif /* WSAEL3HLT */
#ifdef EL3RST
add_integer_constant("EL3RST", EL3RST, 0);
#else /* !EL3RST */
#ifdef WSAEL3RST
add_integer_constant("EL3RST", WSAEL3RST, 0);
#endif /* WSAEL3RST */
#endif /* EL3RST */
#ifdef WSAEL3RST
add_integer_constant("WSAEL3RST", WSAEL3RST, 0);
#endif /* WSAEL3RST */
#ifdef ELIBACC
add_integer_constant("ELIBACC", ELIBACC, 0);
#else /* !ELIBACC */
#ifdef WSAELIBACC
add_integer_constant("ELIBACC", WSAELIBACC, 0);
#endif /* WSAELIBACC */
#endif /* ELIBACC */
#ifdef WSAELIBACC
add_integer_constant("WSAELIBACC", WSAELIBACC, 0);
#endif /* WSAELIBACC */
#ifdef ELIBBAD
add_integer_constant("ELIBBAD", ELIBBAD, 0);
#else /* !ELIBBAD */
#ifdef WSAELIBBAD
add_integer_constant("ELIBBAD", WSAELIBBAD, 0);
#endif /* WSAELIBBAD */
#endif /* ELIBBAD */
#ifdef WSAELIBBAD
add_integer_constant("WSAELIBBAD", WSAELIBBAD, 0);
#endif /* WSAELIBBAD */
#ifdef ELIBEXEC
add_integer_constant("ELIBEXEC", ELIBEXEC, 0);
#else /* !ELIBEXEC */
#ifdef WSAELIBEXEC
add_integer_constant("ELIBEXEC", WSAELIBEXEC, 0);
#endif /* WSAELIBEXEC */
#endif /* ELIBEXEC */
#ifdef WSAELIBEXEC
add_integer_constant("WSAELIBEXEC", WSAELIBEXEC, 0);
#endif /* WSAELIBEXEC */
#ifdef ELIBMAX
add_integer_constant("ELIBMAX", ELIBMAX, 0);
#else /* !ELIBMAX */
#ifdef WSAELIBMAX
add_integer_constant("ELIBMAX", WSAELIBMAX, 0);
#endif /* WSAELIBMAX */
#endif /* ELIBMAX */
#ifdef WSAELIBMAX
add_integer_constant("WSAELIBMAX", WSAELIBMAX, 0);
#endif /* WSAELIBMAX */
#ifdef ELIBSCN
add_integer_constant("ELIBSCN", ELIBSCN, 0);
#else /* !ELIBSCN */
#ifdef WSAELIBSCN
add_integer_constant("ELIBSCN", WSAELIBSCN, 0);
#endif /* WSAELIBSCN */
#endif /* ELIBSCN */
#ifdef WSAELIBSCN
add_integer_constant("WSAELIBSCN", WSAELIBSCN, 0);
#endif /* WSAELIBSCN */
#ifdef ELNRNG
add_integer_constant("ELNRNG", ELNRNG, 0);
#else /* !ELNRNG */
#ifdef WSAELNRNG
add_integer_constant("ELNRNG", WSAELNRNG, 0);
#endif /* WSAELNRNG */
#endif /* ELNRNG */
#ifdef WSAELNRNG
add_integer_constant("WSAELNRNG", WSAELNRNG, 0);
#endif /* WSAELNRNG */
#ifdef ELOGINLIM
add_integer_constant("ELOGINLIM", ELOGINLIM, 0);
#else /* !ELOGINLIM */
#ifdef WSAELOGINLIM
add_integer_constant("ELOGINLIM", WSAELOGINLIM, 0);
#endif /* WSAELOGINLIM */
#endif /* ELOGINLIM */
#ifdef WSAELOGINLIM
add_integer_constant("WSAELOGINLIM", WSAELOGINLIM, 0);
#endif /* WSAELOGINLIM */
#ifdef ELOOP
add_integer_constant("ELOOP", ELOOP, 0);
#else /* !ELOOP */
#ifdef WSAELOOP
add_integer_constant("ELOOP", WSAELOOP, 0);
#endif /* WSAELOOP */
#endif /* ELOOP */
#ifdef WSAELOOP
add_integer_constant("WSAELOOP", WSAELOOP, 0);
#endif /* WSAELOOP */
#ifdef EMEDIA
add_integer_constant("EMEDIA", EMEDIA, 0);
#else /* !EMEDIA */
#ifdef WSAEMEDIA
add_integer_constant("EMEDIA", WSAEMEDIA, 0);
#endif /* WSAEMEDIA */
#endif /* EMEDIA */
#ifdef WSAEMEDIA
add_integer_constant("WSAEMEDIA", WSAEMEDIA, 0);
#endif /* WSAEMEDIA */
#ifdef EMEDIUMTYPE
add_integer_constant("EMEDIUMTYPE", EMEDIUMTYPE, 0);
#else /* !EMEDIUMTYPE */
#ifdef WSAEMEDIUMTYPE
add_integer_constant("EMEDIUMTYPE", WSAEMEDIUMTYPE, 0);
#endif /* WSAEMEDIUMTYPE */
#endif /* EMEDIUMTYPE */
#ifdef WSAEMEDIUMTYPE
add_integer_constant("WSAEMEDIUMTYPE", WSAEMEDIUMTYPE, 0);
#endif /* WSAEMEDIUMTYPE */
#ifdef EMFILE
add_integer_constant("EMFILE", EMFILE, 0);
#else /* !EMFILE */
#ifdef WSAEMFILE
add_integer_constant("EMFILE", WSAEMFILE, 0);
#endif /* WSAEMFILE */
#endif /* EMFILE */
#ifdef WSAEMFILE
add_integer_constant("WSAEMFILE", WSAEMFILE, 0);
#endif /* WSAEMFILE */
#ifdef EMLINK
add_integer_constant("EMLINK", EMLINK, 0);
#else /* !EMLINK */
#ifdef WSAEMLINK
add_integer_constant("EMLINK", WSAEMLINK, 0);
#endif /* WSAEMLINK */
#endif /* EMLINK */
#ifdef WSAEMLINK
add_integer_constant("WSAEMLINK", WSAEMLINK, 0);
#endif /* WSAEMLINK */
#ifdef EMSGSIZE
add_integer_constant("EMSGSIZE", EMSGSIZE, 0);
#else /* !EMSGSIZE */
#ifdef WSAEMSGSIZE
add_integer_constant("EMSGSIZE", WSAEMSGSIZE, 0);
#endif /* WSAEMSGSIZE */
#endif /* EMSGSIZE */
#ifdef WSAEMSGSIZE
add_integer_constant("WSAEMSGSIZE", WSAEMSGSIZE, 0);
#endif /* WSAEMSGSIZE */
#ifdef EMTIMERS
add_integer_constant("EMTIMERS", EMTIMERS, 0);
#else /* !EMTIMERS */
#ifdef WSAEMTIMERS
add_integer_constant("EMTIMERS", WSAEMTIMERS, 0);
#endif /* WSAEMTIMERS */
#endif /* EMTIMERS */
#ifdef WSAEMTIMERS
add_integer_constant("WSAEMTIMERS", WSAEMTIMERS, 0);
#endif /* WSAEMTIMERS */
#ifdef EMULTIHOP
add_integer_constant("EMULTIHOP", EMULTIHOP, 0);
#else /* !EMULTIHOP */
#ifdef WSAEMULTIHOP
add_integer_constant("EMULTIHOP", WSAEMULTIHOP, 0);
#endif /* WSAEMULTIHOP */
#endif /* EMULTIHOP */
#ifdef WSAEMULTIHOP
add_integer_constant("WSAEMULTIHOP", WSAEMULTIHOP, 0);
#endif /* WSAEMULTIHOP */
#ifdef EMUSTRUN
add_integer_constant("EMUSTRUN", EMUSTRUN, 0);
#else /* !EMUSTRUN */
#ifdef WSAEMUSTRUN
add_integer_constant("EMUSTRUN", WSAEMUSTRUN, 0);
#endif /* WSAEMUSTRUN */
#endif /* EMUSTRUN */
#ifdef WSAEMUSTRUN
add_integer_constant("WSAEMUSTRUN", WSAEMUSTRUN, 0);
#endif /* WSAEMUSTRUN */
#ifdef ENAMETOOLONG
add_integer_constant("ENAMETOOLONG", ENAMETOOLONG, 0);
#else /* !ENAMETOOLONG */
#ifdef WSAENAMETOOLONG
add_integer_constant("ENAMETOOLONG", WSAENAMETOOLONG, 0);
#endif /* WSAENAMETOOLONG */
#endif /* ENAMETOOLONG */
#ifdef WSAENAMETOOLONG
add_integer_constant("WSAENAMETOOLONG", WSAENAMETOOLONG, 0);
#endif /* WSAENAMETOOLONG */
#ifdef ENAVAIL
add_integer_constant("ENAVAIL", ENAVAIL, 0);
#else /* !ENAVAIL */
#ifdef WSAENAVAIL
add_integer_constant("ENAVAIL", WSAENAVAIL, 0);
#endif /* WSAENAVAIL */
#endif /* ENAVAIL */
#ifdef WSAENAVAIL
add_integer_constant("WSAENAVAIL", WSAENAVAIL, 0);
#endif /* WSAENAVAIL */
#ifdef ENETDOWN
add_integer_constant("ENETDOWN", ENETDOWN, 0);
#else /* !ENETDOWN */
#ifdef WSAENETDOWN
add_integer_constant("ENETDOWN", WSAENETDOWN, 0);
#endif /* WSAENETDOWN */
#endif /* ENETDOWN */
#ifdef WSAENETDOWN
add_integer_constant("WSAENETDOWN", WSAENETDOWN, 0);
#endif /* WSAENETDOWN */
#ifdef ENETRESET
add_integer_constant("ENETRESET", ENETRESET, 0);
#else /* !ENETRESET */
#ifdef WSAENETRESET
add_integer_constant("ENETRESET", WSAENETRESET, 0);
#endif /* WSAENETRESET */
#endif /* ENETRESET */
#ifdef WSAENETRESET
add_integer_constant("WSAENETRESET", WSAENETRESET, 0);
#endif /* WSAENETRESET */
#ifdef ENETUNREACH
add_integer_constant("ENETUNREACH", ENETUNREACH, 0);
#else /* !ENETUNREACH */
#ifdef WSAENETUNREACH
add_integer_constant("ENETUNREACH", WSAENETUNREACH, 0);
#endif /* WSAENETUNREACH */
#endif /* ENETUNREACH */
#ifdef WSAENETUNREACH
add_integer_constant("WSAENETUNREACH", WSAENETUNREACH, 0);
#endif /* WSAENETUNREACH */
#ifdef ENFILE
add_integer_constant("ENFILE", ENFILE, 0);
#else /* !ENFILE */
#ifdef WSAENFILE
add_integer_constant("ENFILE", WSAENFILE, 0);
#endif /* WSAENFILE */
#endif /* ENFILE */
#ifdef WSAENFILE
add_integer_constant("WSAENFILE", WSAENFILE, 0);
#endif /* WSAENFILE */
#ifdef ENFSREMOTE
add_integer_constant("ENFSREMOTE", ENFSREMOTE, 0);
#else /* !ENFSREMOTE */
#ifdef WSAENFSREMOTE
add_integer_constant("ENFSREMOTE", WSAENFSREMOTE, 0);
#endif /* WSAENFSREMOTE */
#endif /* ENFSREMOTE */
#ifdef WSAENFSREMOTE
add_integer_constant("WSAENFSREMOTE", WSAENFSREMOTE, 0);
#endif /* WSAENFSREMOTE */
#ifdef ENOANO
add_integer_constant("ENOANO", ENOANO, 0);
#else /* !ENOANO */
#ifdef WSAENOANO
add_integer_constant("ENOANO", WSAENOANO, 0);
#endif /* WSAENOANO */
#endif /* ENOANO */
#ifdef WSAENOANO
add_integer_constant("WSAENOANO", WSAENOANO, 0);
#endif /* WSAENOANO */
#ifdef ENOATTACH
add_integer_constant("ENOATTACH", ENOATTACH, 0);
#else /* !ENOATTACH */
#ifdef WSAENOATTACH
add_integer_constant("ENOATTACH", WSAENOATTACH, 0);
#endif /* WSAENOATTACH */
#endif /* ENOATTACH */
#ifdef WSAENOATTACH
add_integer_constant("WSAENOATTACH", WSAENOATTACH, 0);
#endif /* WSAENOATTACH */
#ifdef ENOATTR
add_integer_constant("ENOATTR", ENOATTR, 0);
#else /* !ENOATTR */
#ifdef WSAENOATTR
add_integer_constant("ENOATTR", WSAENOATTR, 0);
#endif /* WSAENOATTR */
#endif /* ENOATTR */
#ifdef WSAENOATTR
add_integer_constant("WSAENOATTR", WSAENOATTR, 0);
#endif /* WSAENOATTR */
#ifdef ENOBUFS
add_integer_constant("ENOBUFS", ENOBUFS, 0);
#else /* !ENOBUFS */
#ifdef WSAENOBUFS
add_integer_constant("ENOBUFS", WSAENOBUFS, 0);
#endif /* WSAENOBUFS */
#endif /* ENOBUFS */
#ifdef WSAENOBUFS
add_integer_constant("WSAENOBUFS", WSAENOBUFS, 0);
#endif /* WSAENOBUFS */
#ifdef ENOCONNECT
add_integer_constant("ENOCONNECT", ENOCONNECT, 0);
#else /* !ENOCONNECT */
#ifdef WSAENOCONNECT
add_integer_constant("ENOCONNECT", WSAENOCONNECT, 0);
#endif /* WSAENOCONNECT */
#endif /* ENOCONNECT */
#ifdef WSAENOCONNECT
add_integer_constant("WSAENOCONNECT", WSAENOCONNECT, 0);
#endif /* WSAENOCONNECT */
#ifdef ENOCSI
add_integer_constant("ENOCSI", ENOCSI, 0);
#else /* !ENOCSI */
#ifdef WSAENOCSI
add_integer_constant("ENOCSI", WSAENOCSI, 0);
#endif /* WSAENOCSI */
#endif /* ENOCSI */
#ifdef WSAENOCSI
add_integer_constant("WSAENOCSI", WSAENOCSI, 0);
#endif /* WSAENOCSI */
#ifdef ENODATA
add_integer_constant("ENODATA", ENODATA, 0);
#else /* !ENODATA */
#ifdef WSAENODATA
add_integer_constant("ENODATA", WSAENODATA, 0);
#endif /* WSAENODATA */
#endif /* ENODATA */
#ifdef WSAENODATA
add_integer_constant("WSAENODATA", WSAENODATA, 0);
#endif /* WSAENODATA */
#ifdef ENODEV
add_integer_constant("ENODEV", ENODEV, 0);
#else /* !ENODEV */
#ifdef WSAENODEV
add_integer_constant("ENODEV", WSAENODEV, 0);
#endif /* WSAENODEV */
#endif /* ENODEV */
#ifdef WSAENODEV
add_integer_constant("WSAENODEV", WSAENODEV, 0);
#endif /* WSAENODEV */
#ifdef ENOENT
add_integer_constant("ENOENT", ENOENT, 0);
#else /* !ENOENT */
#ifdef WSAENOENT
add_integer_constant("ENOENT", WSAENOENT, 0);
#endif /* WSAENOENT */
#endif /* ENOENT */
#ifdef WSAENOENT
add_integer_constant("WSAENOENT", WSAENOENT, 0);
#endif /* WSAENOENT */
#ifdef ENOEXEC
add_integer_constant("ENOEXEC", ENOEXEC, 0);
#else /* !ENOEXEC */
#ifdef WSAENOEXEC
add_integer_constant("ENOEXEC", WSAENOEXEC, 0);
#endif /* WSAENOEXEC */
#endif /* ENOEXEC */
#ifdef WSAENOEXEC
add_integer_constant("WSAENOEXEC", WSAENOEXEC, 0);
#endif /* WSAENOEXEC */
#ifdef ENOEXIST
add_integer_constant("ENOEXIST", ENOEXIST, 0);
#else /* !ENOEXIST */
#ifdef WSAENOEXIST
add_integer_constant("ENOEXIST", WSAENOEXIST, 0);
#endif /* WSAENOEXIST */
#endif /* ENOEXIST */
#ifdef WSAENOEXIST
add_integer_constant("WSAENOEXIST", WSAENOEXIST, 0);
#endif /* WSAENOEXIST */
#ifdef ENOINTRGROUP
add_integer_constant("ENOINTRGROUP", ENOINTRGROUP, 0);
#else /* !ENOINTRGROUP */
#ifdef WSAENOINTRGROUP
add_integer_constant("ENOINTRGROUP", WSAENOINTRGROUP, 0);
#endif /* WSAENOINTRGROUP */
#endif /* ENOINTRGROUP */
#ifdef WSAENOINTRGROUP
add_integer_constant("WSAENOINTRGROUP", WSAENOINTRGROUP, 0);
#endif /* WSAENOINTRGROUP */
#ifdef ENOIOCTLCMD
add_integer_constant("ENOIOCTLCMD", ENOIOCTLCMD, 0);
#else /* !ENOIOCTLCMD */
#ifdef WSAENOIOCTLCMD
add_integer_constant("ENOIOCTLCMD", WSAENOIOCTLCMD, 0);
#endif /* WSAENOIOCTLCMD */
#endif /* ENOIOCTLCMD */
#ifdef WSAENOIOCTLCMD
add_integer_constant("WSAENOIOCTLCMD", WSAENOIOCTLCMD, 0);
#endif /* WSAENOIOCTLCMD */
#ifdef ENOLCK
add_integer_constant("ENOLCK", ENOLCK, 0);
#else /* !ENOLCK */
#ifdef WSAENOLCK
add_integer_constant("ENOLCK", WSAENOLCK, 0);
#endif /* WSAENOLCK */
#endif /* ENOLCK */
#ifdef WSAENOLCK
add_integer_constant("WSAENOLCK", WSAENOLCK, 0);
#endif /* WSAENOLCK */
#ifdef ENOLIMFILE
add_integer_constant("ENOLIMFILE", ENOLIMFILE, 0);
#else /* !ENOLIMFILE */
#ifdef WSAENOLIMFILE
add_integer_constant("ENOLIMFILE", WSAENOLIMFILE, 0);
#endif /* WSAENOLIMFILE */
#endif /* ENOLIMFILE */
#ifdef WSAENOLIMFILE
add_integer_constant("WSAENOLIMFILE", WSAENOLIMFILE, 0);
#endif /* WSAENOLIMFILE */
#ifdef ENOLINK
add_integer_constant("ENOLINK", ENOLINK, 0);
#else /* !ENOLINK */
#ifdef WSAENOLINK
add_integer_constant("ENOLINK", WSAENOLINK, 0);
#endif /* WSAENOLINK */
#endif /* ENOLINK */
#ifdef WSAENOLINK
add_integer_constant("WSAENOLINK", WSAENOLINK, 0);
#endif /* WSAENOLINK */
#ifdef ENOLOGIN
add_integer_constant("ENOLOGIN", ENOLOGIN, 0);
#else /* !ENOLOGIN */
#ifdef WSAENOLOGIN
add_integer_constant("ENOLOGIN", WSAENOLOGIN, 0);
#endif /* WSAENOLOGIN */
#endif /* ENOLOGIN */
#ifdef WSAENOLOGIN
add_integer_constant("WSAENOLOGIN", WSAENOLOGIN, 0);
#endif /* WSAENOLOGIN */
#ifdef ENOMEDIUM
add_integer_constant("ENOMEDIUM", ENOMEDIUM, 0);
#else /* !ENOMEDIUM */
#ifdef WSAENOMEDIUM
add_integer_constant("ENOMEDIUM", WSAENOMEDIUM, 0);
#endif /* WSAENOMEDIUM */
#endif /* ENOMEDIUM */
#ifdef WSAENOMEDIUM
add_integer_constant("WSAENOMEDIUM", WSAENOMEDIUM, 0);
#endif /* WSAENOMEDIUM */
#ifdef ENOMEM
add_integer_constant("ENOMEM", ENOMEM, 0);
#else /* !ENOMEM */
#ifdef WSAENOMEM
add_integer_constant("ENOMEM", WSAENOMEM, 0);
#endif /* WSAENOMEM */
#endif /* ENOMEM */
#ifdef WSAENOMEM
add_integer_constant("WSAENOMEM", WSAENOMEM, 0);
#endif /* WSAENOMEM */
#ifdef ENOMSG
add_integer_constant("ENOMSG", ENOMSG, 0);
#else /* !ENOMSG */
#ifdef WSAENOMSG
add_integer_constant("ENOMSG", WSAENOMSG, 0);
#endif /* WSAENOMSG */
#endif /* ENOMSG */
#ifdef WSAENOMSG
add_integer_constant("WSAENOMSG", WSAENOMSG, 0);
#endif /* WSAENOMSG */
#ifdef ENONET
add_integer_constant("ENONET", ENONET, 0);
#else /* !ENONET */
#ifdef WSAENONET
add_integer_constant("ENONET", WSAENONET, 0);
#endif /* WSAENONET */
#endif /* ENONET */
#ifdef WSAENONET
add_integer_constant("WSAENONET", WSAENONET, 0);
#endif /* WSAENONET */
#ifdef ENOPKG
add_integer_constant("ENOPKG", ENOPKG, 0);
#else /* !ENOPKG */
#ifdef WSAENOPKG
add_integer_constant("ENOPKG", WSAENOPKG, 0);
#endif /* WSAENOPKG */
#endif /* ENOPKG */
#ifdef WSAENOPKG
add_integer_constant("WSAENOPKG", WSAENOPKG, 0);
#endif /* WSAENOPKG */
#ifdef ENOPROC
add_integer_constant("ENOPROC", ENOPROC, 0);
#else /* !ENOPROC */
#ifdef WSAENOPROC
add_integer_constant("ENOPROC", WSAENOPROC, 0);
#endif /* WSAENOPROC */
#endif /* ENOPROC */
#ifdef WSAENOPROC
add_integer_constant("WSAENOPROC", WSAENOPROC, 0);
#endif /* WSAENOPROC */
#ifdef ENOPROTOOPT
add_integer_constant("ENOPROTOOPT", ENOPROTOOPT, 0);
#else /* !ENOPROTOOPT */
#ifdef WSAENOPROTOOPT
add_integer_constant("ENOPROTOOPT", WSAENOPROTOOPT, 0);
#endif /* WSAENOPROTOOPT */
#endif /* ENOPROTOOPT */
#ifdef WSAENOPROTOOPT
add_integer_constant("WSAENOPROTOOPT", WSAENOPROTOOPT, 0);
#endif /* WSAENOPROTOOPT */
#ifdef ENOSPC
add_integer_constant("ENOSPC", ENOSPC, 0);
#else /* !ENOSPC */
#ifdef WSAENOSPC
add_integer_constant("ENOSPC", WSAENOSPC, 0);
#endif /* WSAENOSPC */
#endif /* ENOSPC */
#ifdef WSAENOSPC
add_integer_constant("WSAENOSPC", WSAENOSPC, 0);
#endif /* WSAENOSPC */
#ifdef ENOSR
add_integer_constant("ENOSR", ENOSR, 0);
#else /* !ENOSR */
#ifdef WSAENOSR
add_integer_constant("ENOSR", WSAENOSR, 0);
#endif /* WSAENOSR */
#endif /* ENOSR */
#ifdef WSAENOSR
add_integer_constant("WSAENOSR", WSAENOSR, 0);
#endif /* WSAENOSR */
#ifdef ENOSTR
add_integer_constant("ENOSTR", ENOSTR, 0);
#else /* !ENOSTR */
#ifdef WSAENOSTR
add_integer_constant("ENOSTR", WSAENOSTR, 0);
#endif /* WSAENOSTR */
#endif /* ENOSTR */
#ifdef WSAENOSTR
add_integer_constant("WSAENOSTR", WSAENOSTR, 0);
#endif /* WSAENOSTR */
#ifdef ENOSYM
add_integer_constant("ENOSYM", ENOSYM, 0);
#else /* !ENOSYM */
#ifdef WSAENOSYM
add_integer_constant("ENOSYM", WSAENOSYM, 0);
#endif /* WSAENOSYM */
#endif /* ENOSYM */
#ifdef WSAENOSYM
add_integer_constant("WSAENOSYM", WSAENOSYM, 0);
#endif /* WSAENOSYM */
#ifdef ENOSYS
add_integer_constant("ENOSYS", ENOSYS, 0);
#else /* !ENOSYS */
#ifdef WSAENOSYS
add_integer_constant("ENOSYS", WSAENOSYS, 0);
#endif /* WSAENOSYS */
#endif /* ENOSYS */
#ifdef WSAENOSYS
add_integer_constant("WSAENOSYS", WSAENOSYS, 0);
#endif /* WSAENOSYS */
#ifdef ENOTBLK
add_integer_constant("ENOTBLK", ENOTBLK, 0);
#else /* !ENOTBLK */
#ifdef WSAENOTBLK
add_integer_constant("ENOTBLK", WSAENOTBLK, 0);
#endif /* WSAENOTBLK */
#endif /* ENOTBLK */
#ifdef WSAENOTBLK
add_integer_constant("WSAENOTBLK", WSAENOTBLK, 0);
#endif /* WSAENOTBLK */
#ifdef ENOTCONN
add_integer_constant("ENOTCONN", ENOTCONN, 0);
#else /* !ENOTCONN */
#ifdef WSAENOTCONN
add_integer_constant("ENOTCONN", WSAENOTCONN, 0);
#endif /* WSAENOTCONN */
#endif /* ENOTCONN */
#ifdef WSAENOTCONN
add_integer_constant("WSAENOTCONN", WSAENOTCONN, 0);
#endif /* WSAENOTCONN */
#ifdef ENOTCONTROLLER
add_integer_constant("ENOTCONTROLLER", ENOTCONTROLLER, 0);
#else /* !ENOTCONTROLLER */
#ifdef WSAENOTCONTROLLER
add_integer_constant("ENOTCONTROLLER", WSAENOTCONTROLLER, 0);
#endif /* WSAENOTCONTROLLER */
#endif /* ENOTCONTROLLER */
#ifdef WSAENOTCONTROLLER
add_integer_constant("WSAENOTCONTROLLER", WSAENOTCONTROLLER, 0);
#endif /* WSAENOTCONTROLLER */
#ifdef ENOTDIR
add_integer_constant("ENOTDIR", ENOTDIR, 0);
#else /* !ENOTDIR */
#ifdef WSAENOTDIR
add_integer_constant("ENOTDIR", WSAENOTDIR, 0);
#endif /* WSAENOTDIR */
#endif /* ENOTDIR */
#ifdef WSAENOTDIR
add_integer_constant("WSAENOTDIR", WSAENOTDIR, 0);
#endif /* WSAENOTDIR */
#ifdef ENOTEMPTY
add_integer_constant("ENOTEMPTY", ENOTEMPTY, 0);
#else /* !ENOTEMPTY */
#ifdef WSAENOTEMPTY
add_integer_constant("ENOTEMPTY", WSAENOTEMPTY, 0);
#endif /* WSAENOTEMPTY */
#endif /* ENOTEMPTY */
#ifdef WSAENOTEMPTY
add_integer_constant("WSAENOTEMPTY", WSAENOTEMPTY, 0);
#endif /* WSAENOTEMPTY */
#ifdef ENOTENQUEUED
add_integer_constant("ENOTENQUEUED", ENOTENQUEUED, 0);
#else /* !ENOTENQUEUED */
#ifdef WSAENOTENQUEUED
add_integer_constant("ENOTENQUEUED", WSAENOTENQUEUED, 0);
#endif /* WSAENOTENQUEUED */
#endif /* ENOTENQUEUED */
#ifdef WSAENOTENQUEUED
add_integer_constant("WSAENOTENQUEUED", WSAENOTENQUEUED, 0);
#endif /* WSAENOTENQUEUED */
#ifdef ENOTJOINED
add_integer_constant("ENOTJOINED", ENOTJOINED, 0);
#else /* !ENOTJOINED */
#ifdef WSAENOTJOINED
add_integer_constant("ENOTJOINED", WSAENOTJOINED, 0);
#endif /* WSAENOTJOINED */
#endif /* ENOTJOINED */
#ifdef WSAENOTJOINED
add_integer_constant("WSAENOTJOINED", WSAENOTJOINED, 0);
#endif /* WSAENOTJOINED */
#ifdef ENOTNAM
add_integer_constant("ENOTNAM", ENOTNAM, 0);
#else /* !ENOTNAM */
#ifdef WSAENOTNAM
add_integer_constant("ENOTNAM", WSAENOTNAM, 0);
#endif /* WSAENOTNAM */
#endif /* ENOTNAM */
#ifdef WSAENOTNAM
add_integer_constant("WSAENOTNAM", WSAENOTNAM, 0);
#endif /* WSAENOTNAM */
#ifdef ENOTREADY
add_integer_constant("ENOTREADY", ENOTREADY, 0);
#else /* !ENOTREADY */
#ifdef WSAENOTREADY
add_integer_constant("ENOTREADY", WSAENOTREADY, 0);
#endif /* WSAENOTREADY */
#endif /* ENOTREADY */
#ifdef WSAENOTREADY
add_integer_constant("WSAENOTREADY", WSAENOTREADY, 0);
#endif /* WSAENOTREADY */
#ifdef ENOTRUST
add_integer_constant("ENOTRUST", ENOTRUST, 0);
#else /* !ENOTRUST */
#ifdef WSAENOTRUST
add_integer_constant("ENOTRUST", WSAENOTRUST, 0);
#endif /* WSAENOTRUST */
#endif /* ENOTRUST */
#ifdef WSAENOTRUST
add_integer_constant("WSAENOTRUST", WSAENOTRUST, 0);
#endif /* WSAENOTRUST */
#ifdef ENOTSOCK
add_integer_constant("ENOTSOCK", ENOTSOCK, 0);
#else /* !ENOTSOCK */
#ifdef WSAENOTSOCK
add_integer_constant("ENOTSOCK", WSAENOTSOCK, 0);
#endif /* WSAENOTSOCK */
#endif /* ENOTSOCK */
#ifdef WSAENOTSOCK
add_integer_constant("WSAENOTSOCK", WSAENOTSOCK, 0);
#endif /* WSAENOTSOCK */
#ifdef ENOTSTOPPED
add_integer_constant("ENOTSTOPPED", ENOTSTOPPED, 0);
#else /* !ENOTSTOPPED */
#ifdef WSAENOTSTOPPED
add_integer_constant("ENOTSTOPPED", WSAENOTSTOPPED, 0);
#endif /* WSAENOTSTOPPED */
#endif /* ENOTSTOPPED */
#ifdef WSAENOTSTOPPED
add_integer_constant("WSAENOTSTOPPED", WSAENOTSTOPPED, 0);
#endif /* WSAENOTSTOPPED */
#ifdef ENOTSUP
add_integer_constant("ENOTSUP", ENOTSUP, 0);
#else /* !ENOTSUP */
#ifdef WSAENOTSUP
add_integer_constant("ENOTSUP", WSAENOTSUP, 0);
#endif /* WSAENOTSUP */
#endif /* ENOTSUP */
#ifdef WSAENOTSUP
add_integer_constant("WSAENOTSUP", WSAENOTSUP, 0);
#endif /* WSAENOTSUP */
#ifdef ENOTTY
add_integer_constant("ENOTTY", ENOTTY, 0);
#else /* !ENOTTY */
#ifdef WSAENOTTY
add_integer_constant("ENOTTY", WSAENOTTY, 0);
#endif /* WSAENOTTY */
#endif /* ENOTTY */
#ifdef WSAENOTTY
add_integer_constant("WSAENOTTY", WSAENOTTY, 0);
#endif /* WSAENOTTY */
#ifdef ENOTUNIQ
add_integer_constant("ENOTUNIQ", ENOTUNIQ, 0);
#else /* !ENOTUNIQ */
#ifdef WSAENOTUNIQ
add_integer_constant("ENOTUNIQ", WSAENOTUNIQ, 0);
#endif /* WSAENOTUNIQ */
#endif /* ENOTUNIQ */
#ifdef WSAENOTUNIQ
add_integer_constant("WSAENOTUNIQ", WSAENOTUNIQ, 0);
#endif /* WSAENOTUNIQ */
#ifdef ENXIO
add_integer_constant("ENXIO", ENXIO, 0);
#else /* !ENXIO */
#ifdef WSAENXIO
add_integer_constant("ENXIO", WSAENXIO, 0);
#endif /* WSAENXIO */
#endif /* ENXIO */
#ifdef WSAENXIO
add_integer_constant("WSAENXIO", WSAENXIO, 0);
#endif /* WSAENXIO */
#ifdef EOPNOTSUPP
add_integer_constant("EOPNOTSUPP", EOPNOTSUPP, 0);
#else /* !EOPNOTSUPP */
#ifdef WSAEOPNOTSUPP
add_integer_constant("EOPNOTSUPP", WSAEOPNOTSUPP, 0);
#endif /* WSAEOPNOTSUPP */
#endif /* EOPNOTSUPP */
#ifdef WSAEOPNOTSUPP
add_integer_constant("WSAEOPNOTSUPP", WSAEOPNOTSUPP, 0);
#endif /* WSAEOPNOTSUPP */
#ifdef EOVERFLOW
add_integer_constant("EOVERFLOW", EOVERFLOW, 0);
#else /* !EOVERFLOW */
#ifdef WSAEOVERFLOW
add_integer_constant("EOVERFLOW", WSAEOVERFLOW, 0);
#endif /* WSAEOVERFLOW */
#endif /* EOVERFLOW */
#ifdef WSAEOVERFLOW
add_integer_constant("WSAEOVERFLOW", WSAEOVERFLOW, 0);
#endif /* WSAEOVERFLOW */
#ifdef EPERM
add_integer_constant("EPERM", EPERM, 0);
#else /* !EPERM */
#ifdef WSAEPERM
add_integer_constant("EPERM", WSAEPERM, 0);
#endif /* WSAEPERM */
#endif /* EPERM */
#ifdef WSAEPERM
add_integer_constant("WSAEPERM", WSAEPERM, 0);
#endif /* WSAEPERM */
#ifdef EPFNOSUPPORT
add_integer_constant("EPFNOSUPPORT", EPFNOSUPPORT, 0);
#else /* !EPFNOSUPPORT */
#ifdef WSAEPFNOSUPPORT
add_integer_constant("EPFNOSUPPORT", WSAEPFNOSUPPORT, 0);
#endif /* WSAEPFNOSUPPORT */
#endif /* EPFNOSUPPORT */
#ifdef WSAEPFNOSUPPORT
add_integer_constant("WSAEPFNOSUPPORT", WSAEPFNOSUPPORT, 0);
#endif /* WSAEPFNOSUPPORT */
#ifdef EPIPE
add_integer_constant("EPIPE", EPIPE, 0);
#else /* !EPIPE */
#ifdef WSAEPIPE
add_integer_constant("EPIPE", WSAEPIPE, 0);
#endif /* WSAEPIPE */
#endif /* EPIPE */
#ifdef WSAEPIPE
add_integer_constant("WSAEPIPE", WSAEPIPE, 0);
#endif /* WSAEPIPE */
#ifdef EPROCLIM
add_integer_constant("EPROCLIM", EPROCLIM, 0);
#else /* !EPROCLIM */
#ifdef WSAEPROCLIM
add_integer_constant("EPROCLIM", WSAEPROCLIM, 0);
#endif /* WSAEPROCLIM */
#endif /* EPROCLIM */
#ifdef WSAEPROCLIM
add_integer_constant("WSAEPROCLIM", WSAEPROCLIM, 0);
#endif /* WSAEPROCLIM */
#ifdef EPROCUNAVAIL
add_integer_constant("EPROCUNAVAIL", EPROCUNAVAIL, 0);
#else /* !EPROCUNAVAIL */
#ifdef WSAEPROCUNAVAIL
add_integer_constant("EPROCUNAVAIL", WSAEPROCUNAVAIL, 0);
#endif /* WSAEPROCUNAVAIL */
#endif /* EPROCUNAVAIL */
#ifdef WSAEPROCUNAVAIL
add_integer_constant("WSAEPROCUNAVAIL", WSAEPROCUNAVAIL, 0);
#endif /* WSAEPROCUNAVAIL */
#ifdef EPROGMISMATCH
add_integer_constant("EPROGMISMATCH", EPROGMISMATCH, 0);
#else /* !EPROGMISMATCH */
#ifdef WSAEPROGMISMATCH
add_integer_constant("EPROGMISMATCH", WSAEPROGMISMATCH, 0);
#endif /* WSAEPROGMISMATCH */
#endif /* EPROGMISMATCH */
#ifdef WSAEPROGMISMATCH
add_integer_constant("WSAEPROGMISMATCH", WSAEPROGMISMATCH, 0);
#endif /* WSAEPROGMISMATCH */
#ifdef EPROGUNAVAIL
add_integer_constant("EPROGUNAVAIL", EPROGUNAVAIL, 0);
#else /* !EPROGUNAVAIL */
#ifdef WSAEPROGUNAVAIL
add_integer_constant("EPROGUNAVAIL", WSAEPROGUNAVAIL, 0);
#endif /* WSAEPROGUNAVAIL */
#endif /* EPROGUNAVAIL */
#ifdef WSAEPROGUNAVAIL
add_integer_constant("WSAEPROGUNAVAIL", WSAEPROGUNAVAIL, 0);
#endif /* WSAEPROGUNAVAIL */
#ifdef EPROTO
add_integer_constant("EPROTO", EPROTO, 0);
#else /* !EPROTO */
#ifdef WSAEPROTO
add_integer_constant("EPROTO", WSAEPROTO, 0);
#endif /* WSAEPROTO */
#endif /* EPROTO */
#ifdef WSAEPROTO
add_integer_constant("WSAEPROTO", WSAEPROTO, 0);
#endif /* WSAEPROTO */
#ifdef EPROTONOSUPPORT
add_integer_constant("EPROTONOSUPPORT", EPROTONOSUPPORT, 0);
#else /* !EPROTONOSUPPORT */
#ifdef WSAEPROTONOSUPPORT
add_integer_constant("EPROTONOSUPPORT", WSAEPROTONOSUPPORT, 0);
#endif /* WSAEPROTONOSUPPORT */
#endif /* EPROTONOSUPPORT */
#ifdef WSAEPROTONOSUPPORT
add_integer_constant("WSAEPROTONOSUPPORT", WSAEPROTONOSUPPORT, 0);
#endif /* WSAEPROTONOSUPPORT */
#ifdef EPROTOTYPE
add_integer_constant("EPROTOTYPE", EPROTOTYPE, 0);
#else /* !EPROTOTYPE */
#ifdef WSAEPROTOTYPE
add_integer_constant("EPROTOTYPE", WSAEPROTOTYPE, 0);
#endif /* WSAEPROTOTYPE */
#endif /* EPROTOTYPE */
#ifdef WSAEPROTOTYPE
add_integer_constant("WSAEPROTOTYPE", WSAEPROTOTYPE, 0);
#endif /* WSAEPROTOTYPE */
#ifdef ERANGE
add_integer_constant("ERANGE", ERANGE, 0);
#else /* !ERANGE */
#ifdef WSAERANGE
add_integer_constant("ERANGE", WSAERANGE, 0);
#endif /* WSAERANGE */
#endif /* ERANGE */
#ifdef WSAERANGE
add_integer_constant("WSAERANGE", WSAERANGE, 0);
#endif /* WSAERANGE */
#ifdef ERELOCATED
add_integer_constant("ERELOCATED", ERELOCATED, 0);
#else /* !ERELOCATED */
#ifdef WSAERELOCATED
add_integer_constant("ERELOCATED", WSAERELOCATED, 0);
#endif /* WSAERELOCATED */
#endif /* ERELOCATED */
#ifdef WSAERELOCATED
add_integer_constant("WSAERELOCATED", WSAERELOCATED, 0);
#endif /* WSAERELOCATED */
#ifdef EREFUSED
add_integer_constant("EREFUSED", EREFUSED, 0);
#else /* !EREFUSED */
#ifdef WSAEREFUSED
add_integer_constant("EREFUSED", WSAEREFUSED, 0);
#endif /* WSAEREFUSED */
#endif /* EREFUSED */
#ifdef WSAEREFUSED
add_integer_constant("WSAEREFUSED", WSAEREFUSED, 0);
#endif /* WSAEREFUSED */
#ifdef EREMCHG
add_integer_constant("EREMCHG", EREMCHG, 0);
#else /* !EREMCHG */
#ifdef WSAEREMCHG
add_integer_constant("EREMCHG", WSAEREMCHG, 0);
#endif /* WSAEREMCHG */
#endif /* EREMCHG */
#ifdef WSAEREMCHG
add_integer_constant("WSAEREMCHG", WSAEREMCHG, 0);
#endif /* WSAEREMCHG */
#ifdef EREMDEV
add_integer_constant("EREMDEV", EREMDEV, 0);
#else /* !EREMDEV */
#ifdef WSAEREMDEV
add_integer_constant("EREMDEV", WSAEREMDEV, 0);
#endif /* WSAEREMDEV */
#endif /* EREMDEV */
#ifdef WSAEREMDEV
add_integer_constant("WSAEREMDEV", WSAEREMDEV, 0);
#endif /* WSAEREMDEV */
#ifdef EREMOTE
add_integer_constant("EREMOTE", EREMOTE, 0);
#else /* !EREMOTE */
#ifdef WSAEREMOTE
add_integer_constant("EREMOTE", WSAEREMOTE, 0);
#endif /* WSAEREMOTE */
#endif /* EREMOTE */
#ifdef WSAEREMOTE
add_integer_constant("WSAEREMOTE", WSAEREMOTE, 0);
#endif /* WSAEREMOTE */
#ifdef EREMOTEIO
add_integer_constant("EREMOTEIO", EREMOTEIO, 0);
#else /* !EREMOTEIO */
#ifdef WSAEREMOTEIO
add_integer_constant("EREMOTEIO", WSAEREMOTEIO, 0);
#endif /* WSAEREMOTEIO */
#endif /* EREMOTEIO */
#ifdef WSAEREMOTEIO
add_integer_constant("WSAEREMOTEIO", WSAEREMOTEIO, 0);
#endif /* WSAEREMOTEIO */
#ifdef EREMOTERELEASE
add_integer_constant("EREMOTERELEASE", EREMOTERELEASE, 0);
#else /* !EREMOTERELEASE */
#ifdef WSAEREMOTERELEASE
add_integer_constant("EREMOTERELEASE", WSAEREMOTERELEASE, 0);
#endif /* WSAEREMOTERELEASE */
#endif /* EREMOTERELEASE */
#ifdef WSAEREMOTERELEASE
add_integer_constant("WSAEREMOTERELEASE", WSAEREMOTERELEASE, 0);
#endif /* WSAEREMOTERELEASE */
#ifdef ERESTART
add_integer_constant("ERESTART", ERESTART, 0);
#else /* !ERESTART */
#ifdef WSAERESTART
add_integer_constant("ERESTART", WSAERESTART, 0);
#endif /* WSAERESTART */
#endif /* ERESTART */
#ifdef WSAERESTART
add_integer_constant("WSAERESTART", WSAERESTART, 0);
#endif /* WSAERESTART */
#ifdef ERESTARTNOHAND
add_integer_constant("ERESTARTNOHAND", ERESTARTNOHAND, 0);
#else /* !ERESTARTNOHAND */
#ifdef WSAERESTARTNOHAND
add_integer_constant("ERESTARTNOHAND", WSAERESTARTNOHAND, 0);
#endif /* WSAERESTARTNOHAND */
#endif /* ERESTARTNOHAND */
#ifdef WSAERESTARTNOHAND
add_integer_constant("WSAERESTARTNOHAND", WSAERESTARTNOHAND, 0);
#endif /* WSAERESTARTNOHAND */
#ifdef ERESTARTNOINTR
add_integer_constant("ERESTARTNOINTR", ERESTARTNOINTR, 0);
#else /* !ERESTARTNOINTR */
#ifdef WSAERESTARTNOINTR
add_integer_constant("ERESTARTNOINTR", WSAERESTARTNOINTR, 0);
#endif /* WSAERESTARTNOINTR */
#endif /* ERESTARTNOINTR */
#ifdef WSAERESTARTNOINTR
add_integer_constant("WSAERESTARTNOINTR", WSAERESTARTNOINTR, 0);
#endif /* WSAERESTARTNOINTR */
#ifdef ERESTARTSYS
add_integer_constant("ERESTARTSYS", ERESTARTSYS, 0);
#else /* !ERESTARTSYS */
#ifdef WSAERESTARTSYS
add_integer_constant("ERESTARTSYS", WSAERESTARTSYS, 0);
#endif /* WSAERESTARTSYS */
#endif /* ERESTARTSYS */
#ifdef WSAERESTARTSYS
add_integer_constant("WSAERESTARTSYS", WSAERESTARTSYS, 0);
#endif /* WSAERESTARTSYS */
#ifdef EROFS
add_integer_constant("EROFS", EROFS, 0);
#else /* !EROFS */
#ifdef WSAEROFS
add_integer_constant("EROFS", WSAEROFS, 0);
#endif /* WSAEROFS */
#endif /* EROFS */
#ifdef WSAEROFS
add_integer_constant("WSAEROFS", WSAEROFS, 0);
#endif /* WSAEROFS */
#ifdef ERPCMISMATCH
add_integer_constant("ERPCMISMATCH", ERPCMISMATCH, 0);
#else /* !ERPCMISMATCH */
#ifdef WSAERPCMISMATCH
add_integer_constant("ERPCMISMATCH", WSAERPCMISMATCH, 0);
#endif /* WSAERPCMISMATCH */
#endif /* ERPCMISMATCH */
#ifdef WSAERPCMISMATCH
add_integer_constant("WSAERPCMISMATCH", WSAERPCMISMATCH, 0);
#endif /* WSAERPCMISMATCH */
#ifdef ERREMOTE
add_integer_constant("ERREMOTE", ERREMOTE, 0);
#else /* !ERREMOTE */
#ifdef WSAERREMOTE
add_integer_constant("ERREMOTE", WSAERREMOTE, 0);
#endif /* WSAERREMOTE */
#endif /* ERREMOTE */
#ifdef WSAERREMOTE
add_integer_constant("WSAERREMOTE", WSAERREMOTE, 0);
#endif /* WSAERREMOTE */
#ifdef ESAD
add_integer_constant("ESAD", ESAD, 0);
#else /* !ESAD */
#ifdef WSAESAD
add_integer_constant("ESAD", WSAESAD, 0);
#endif /* WSAESAD */
#endif /* ESAD */
#ifdef WSAESAD
add_integer_constant("WSAESAD", WSAESAD, 0);
#endif /* WSAESAD */
#ifdef ESHUTDOWN
add_integer_constant("ESHUTDOWN", ESHUTDOWN, 0);
#else /* !ESHUTDOWN */
#ifdef WSAESHUTDOWN
add_integer_constant("ESHUTDOWN", WSAESHUTDOWN, 0);
#endif /* WSAESHUTDOWN */
#endif /* ESHUTDOWN */
#ifdef WSAESHUTDOWN
add_integer_constant("WSAESHUTDOWN", WSAESHUTDOWN, 0);
#endif /* WSAESHUTDOWN */
#ifdef ESOCKTNOSUPPORT
add_integer_constant("ESOCKTNOSUPPORT", ESOCKTNOSUPPORT, 0);
#else /* !ESOCKTNOSUPPORT */
#ifdef WSAESOCKTNOSUPPORT
add_integer_constant("ESOCKTNOSUPPORT", WSAESOCKTNOSUPPORT, 0);
#endif /* WSAESOCKTNOSUPPORT */
#endif /* ESOCKTNOSUPPORT */
#ifdef WSAESOCKTNOSUPPORT
add_integer_constant("WSAESOCKTNOSUPPORT", WSAESOCKTNOSUPPORT, 0);
#endif /* WSAESOCKTNOSUPPORT */
#ifdef ESOFT
add_integer_constant("ESOFT", ESOFT, 0);
#else /* !ESOFT */
#ifdef WSAESOFT
add_integer_constant("ESOFT", WSAESOFT, 0);
#endif /* WSAESOFT */
#endif /* ESOFT */
#ifdef WSAESOFT
add_integer_constant("WSAESOFT", WSAESOFT, 0);
#endif /* WSAESOFT */
#ifdef ESPIPE
add_integer_constant("ESPIPE", ESPIPE, 0);
#else /* !ESPIPE */
#ifdef WSAESPIPE
add_integer_constant("ESPIPE", WSAESPIPE, 0);
#endif /* WSAESPIPE */
#endif /* ESPIPE */
#ifdef WSAESPIPE
add_integer_constant("WSAESPIPE", WSAESPIPE, 0);
#endif /* WSAESPIPE */
#ifdef ESRCH
add_integer_constant("ESRCH", ESRCH, 0);
#else /* !ESRCH */
#ifdef WSAESRCH
add_integer_constant("ESRCH", WSAESRCH, 0);
#endif /* WSAESRCH */
#endif /* ESRCH */
#ifdef WSAESRCH
add_integer_constant("WSAESRCH", WSAESRCH, 0);
#endif /* WSAESRCH */
#ifdef ESRMNT
add_integer_constant("ESRMNT", ESRMNT, 0);
#else /* !ESRMNT */
#ifdef WSAESRMNT
add_integer_constant("ESRMNT", WSAESRMNT, 0);
#endif /* WSAESRMNT */
#endif /* ESRMNT */
#ifdef WSAESRMNT
add_integer_constant("WSAESRMNT", WSAESRMNT, 0);
#endif /* WSAESRMNT */
#ifdef ESTALE
add_integer_constant("ESTALE", ESTALE, 0);
#else /* !ESTALE */
#ifdef WSAESTALE
add_integer_constant("ESTALE", WSAESTALE, 0);
#endif /* WSAESTALE */
#endif /* ESTALE */
#ifdef WSAESTALE
add_integer_constant("WSAESTALE", WSAESTALE, 0);
#endif /* WSAESTALE */
#ifdef ESTRPIPE
add_integer_constant("ESTRPIPE", ESTRPIPE, 0);
#else /* !ESTRPIPE */
#ifdef WSAESTRPIPE
add_integer_constant("ESTRPIPE", WSAESTRPIPE, 0);
#endif /* WSAESTRPIPE */
#endif /* ESTRPIPE */
#ifdef WSAESTRPIPE
add_integer_constant("WSAESTRPIPE", WSAESTRPIPE, 0);
#endif /* WSAESTRPIPE */
#ifdef ESUCCESS
add_integer_constant("ESUCCESS", ESUCCESS, 0);
#else /* !ESUCCESS */
#ifdef WSAESUCCESS
add_integer_constant("ESUCCESS", WSAESUCCESS, 0);
#endif /* WSAESUCCESS */
#endif /* ESUCCESS */
#ifdef WSAESUCCESS
add_integer_constant("WSAESUCCESS", WSAESUCCESS, 0);
#endif /* WSAESUCCESS */
#ifdef ETIME
add_integer_constant("ETIME", ETIME, 0);
#else /* !ETIME */
#ifdef WSAETIME
add_integer_constant("ETIME", WSAETIME, 0);
#endif /* WSAETIME */
#endif /* ETIME */
#ifdef WSAETIME
add_integer_constant("WSAETIME", WSAETIME, 0);
#endif /* WSAETIME */
#ifdef ETIMEDOUT
add_integer_constant("ETIMEDOUT", ETIMEDOUT, 0);
#else /* !ETIMEDOUT */
#ifdef WSAETIMEDOUT
add_integer_constant("ETIMEDOUT", WSAETIMEDOUT, 0);
#endif /* WSAETIMEDOUT */
#endif /* ETIMEDOUT */
#ifdef WSAETIMEDOUT
add_integer_constant("WSAETIMEDOUT", WSAETIMEDOUT, 0);
#endif /* WSAETIMEDOUT */
#ifdef ETOOMANYREFS
add_integer_constant("ETOOMANYREFS", ETOOMANYREFS, 0);
#else /* !ETOOMANYREFS */
#ifdef WSAETOOMANYREFS
add_integer_constant("ETOOMANYREFS", WSAETOOMANYREFS, 0);
#endif /* WSAETOOMANYREFS */
#endif /* ETOOMANYREFS */
#ifdef WSAETOOMANYREFS
add_integer_constant("WSAETOOMANYREFS", WSAETOOMANYREFS, 0);
#endif /* WSAETOOMANYREFS */
#ifdef ETXTBSY
add_integer_constant("ETXTBSY", ETXTBSY, 0);
#else /* !ETXTBSY */
#ifdef WSAETXTBSY
add_integer_constant("ETXTBSY", WSAETXTBSY, 0);
#endif /* WSAETXTBSY */
#endif /* ETXTBSY */
#ifdef WSAETXTBSY
add_integer_constant("WSAETXTBSY", WSAETXTBSY, 0);
#endif /* WSAETXTBSY */
#ifdef EUCLEAN
add_integer_constant("EUCLEAN", EUCLEAN, 0);
#else /* !EUCLEAN */
#ifdef WSAEUCLEAN
add_integer_constant("EUCLEAN", WSAEUCLEAN, 0);
#endif /* WSAEUCLEAN */
#endif /* EUCLEAN */
#ifdef WSAEUCLEAN
add_integer_constant("WSAEUCLEAN", WSAEUCLEAN, 0);
#endif /* WSAEUCLEAN */
#ifdef EUNATCH
add_integer_constant("EUNATCH", EUNATCH, 0);
#else /* !EUNATCH */
#ifdef WSAEUNATCH
add_integer_constant("EUNATCH", WSAEUNATCH, 0);
#endif /* WSAEUNATCH */
#endif /* EUNATCH */
#ifdef WSAEUNATCH
add_integer_constant("WSAEUNATCH", WSAEUNATCH, 0);
#endif /* WSAEUNATCH */
#ifdef EUSERS
add_integer_constant("EUSERS", EUSERS, 0);
#else /* !EUSERS */
#ifdef WSAEUSERS
add_integer_constant("EUSERS", WSAEUSERS, 0);
#endif /* WSAEUSERS */
#endif /* EUSERS */
#ifdef WSAEUSERS
add_integer_constant("WSAEUSERS", WSAEUSERS, 0);
#endif /* WSAEUSERS */
#ifdef EVERSION
add_integer_constant("EVERSION", EVERSION, 0);
#else /* !EVERSION */
#ifdef WSAEVERSION
add_integer_constant("EVERSION", WSAEVERSION, 0);
#endif /* WSAEVERSION */
#endif /* EVERSION */
#ifdef WSAEVERSION
add_integer_constant("WSAEVERSION", WSAEVERSION, 0);
#endif /* WSAEVERSION */
#ifdef EWOULDBLOCK
add_integer_constant("EWOULDBLOCK", EWOULDBLOCK, 0);
#else /* !EWOULDBLOCK */
#ifdef WSAEWOULDBLOCK
add_integer_constant("EWOULDBLOCK", WSAEWOULDBLOCK, 0);
#endif /* WSAEWOULDBLOCK */
#endif /* EWOULDBLOCK */
#ifdef WSAEWOULDBLOCK
add_integer_constant("WSAEWOULDBLOCK", WSAEWOULDBLOCK, 0);
#endif /* WSAEWOULDBLOCK */
#ifdef EWRPROTECT
add_integer_constant("EWRPROTECT", EWRPROTECT, 0);
#else /* !EWRPROTECT */
#ifdef WSAEWRPROTECT
add_integer_constant("EWRPROTECT", WSAEWRPROTECT, 0);
#endif /* WSAEWRPROTECT */
#endif /* EWRPROTECT */
#ifdef WSAEWRPROTECT
add_integer_constant("WSAEWRPROTECT", WSAEWRPROTECT, 0);
#endif /* WSAEWRPROTECT */
#ifdef EXDEV
add_integer_constant("EXDEV", EXDEV, 0);
#else /* !EXDEV */
#ifdef WSAEXDEV
add_integer_constant("EXDEV", WSAEXDEV, 0);
#endif /* WSAEXDEV */
#endif /* EXDEV */
#ifdef WSAEXDEV
add_integer_constant("WSAEXDEV", WSAEXDEV, 0);
#endif /* WSAEXDEV */
#ifdef EXFULL
add_integer_constant("EXFULL", EXFULL, 0);
#else /* !EXFULL */
#ifdef WSAEXFULL
add_integer_constant("EXFULL", WSAEXFULL, 0);
#endif /* WSAEXFULL */
#endif /* EXFULL */
#ifdef WSAEXFULL
add_integer_constant("WSAEXFULL", WSAEXFULL, 0);
#endif /* WSAEXFULL */
#ifdef LASTERRNO
add_integer_constant("LASTERRNO", LASTERRNO, 0);
#else /* !LASTERRNO */
#ifdef WSALASTERRNO
add_integer_constant("LASTERRNO", WSALASTERRNO, 0);
#endif /* WSALASTERRNO */
#endif /* LASTERRNO */
#ifdef WSALASTERRNO
add_integer_constant("WSALASTERRNO", WSALASTERRNO, 0);
#endif /* WSALASTERRNO */
