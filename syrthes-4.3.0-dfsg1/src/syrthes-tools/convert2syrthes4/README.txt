**************************************************************
  convert2syrthes : convert mesh files to SYRTHES mesh format
**************************************************************

convert2syrthes is a tool to convert mesh files from GAMBIT, GMSH or MED
to SYRTHES format.

Taking into account MED format is optional. If you want to convert MED files,
the MED libraries have to be installed independently and previously.



1. How to build convert2syrthes
===============================

If you want to convert also MED files : 
     - edit the Makefile and put the path for your own med libraries 
     - type : make MED=yes

If not :
     - type : make



2. How to use convert2syrthes
=============================

  convert2syrthes [-dim dimension]  -m mesh1.ext [-o mesh2.syr]
                  with  .ext =
                               .neu (GAMBIT format)
                               .msh (GMSH format)
                               .med (MED format)
     --> change initial mesh file format to SYRTHES 3.4 mesh format

  Additional options :
     -o mesh2.syr : to change the name of the output file
     -dim 2 or -dim 3 : needed for GMSH file format, ignored in the other cases


  Note :
     For MED format, convert2syrthes creates an additionnal ASCII file (..._desc)
     where one finds the links between group names (used in Salome)
     and familly (or references) numbers used in SYRTHES



  Examples :
     convert2syrthes -m square.med                         --> square.syr square.syr_desc
     convert2syrthes -m square.neu                         --> square.syr
     convert2syrthes -m square.msh -dim 2                  --> square.syr
     convert2syrthes -m square.med -o square2.syr          --> square2.syr square2.syr_desc
