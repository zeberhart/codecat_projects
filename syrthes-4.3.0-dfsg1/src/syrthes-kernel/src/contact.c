/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <math.h>

#include "syr_usertype.h"
# include "syr_bd.h"
# include "syr_const.h"
# include "syr_abs.h"
# include "syr_tree.h"
# include "syr_proto.h"
# include "syr_option.h"

extern struct Performances perfo;
extern struct Affichages affich;


extern char nomdata[CHLONG];
extern FILE *fdata;

extern int nbVar;

static char ch[CHLONG],motcle[CHLONG],formule[CHLONG],repc[CHLONG];
static double dlist[100];
static int ilist[100];

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_pairesrc(struct Contact *rescon,
		   struct Maillage maillnodes,struct MaillageBord maillnodebord,
		   struct SDparall sdparall)
{
  int i,j,k;
  int i1,i2,i3,i4,id,ii,nb,nr,n,it,ityp;
  int *itrav;
  double zero=0.,val;
  int *np1,*np2,nn1,nn2;

  rescon->npoin=0;

  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  ityp=-1;
	  if      (!strcmp(motcle,"CLIM_T") || !strcmp(motcle,"CLIM_HMT"))           ityp=0;
	  else if (!strcmp(motcle,"CLIM_T_FCT") || !strcmp(motcle,"CLIM_HMT_FCT"))   ityp=1;
	  else if (!strcmp(motcle,"CLIM_T_PROG") || !strcmp(motcle,"CLIM_HMT_PROG")) ityp=2;

	  if (ityp>-1) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"RES_CONTACT")) 
		{
		  switch(ityp){
		  case 0: rep_ndbl(nbVar,dlist,&ii,ch+id); id+=ii; break;
		  case 1: for (i=0;i<nbVar;i++) {ii=rep_ch(formule,ch+id); id+=ii;} break;
		  }
		  rep_listint(ilist,&nb,ch+id);
		  cree_liste_noeuds_periorc(maillnodebord,maillnodes,POSRESCON, 
					    ilist,nb,&nn1,&nn2,&np1,&np2);
		  cree_paires_noeuds_rc(maillnodes,nn1,nn2,np1,np2,rescon);
		  free(np1); free(np2);
		}
	    }
	  
	}
    }

/*   if (SYRTHES_LANG == FR) */
/*     { */
/*       printf("\n *** RESISTANCE DE CONTACT\n"); */
/*       printf("       - nombre final de noeuds RC  : %d\n",rescon->npoin); */
/*       printf("       - nombre final d'elements RC : %d\n",rescon->nelem); */
/*     } */
/*   else if (SYRTHES_LANG == EN) */
/*     { */
/*       printf("\n *** CONTACT RESISTANCE\n"); */
/*       printf("       - total number of CR : %d\n",rescon->npoin); */
/*       printf("       - total number of CR : %d\n",rescon->nelem); */
/*     } */

} 
  

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | cree_liste_rc                                                        |
  |         Recherche des faces resistance de contatc correspondantes    |
  |======================================================================| */

void cree_paires_noeuds_rc(struct Maillage maillnodes,
			   int nn1,int nn2,int *np1,int *np2,
			   struct Contact *rescon)

{
  int i,j,n,npl1,npl2,nb1,nb2,np,ng,num;
  int iminmin;
  int n1,n2,nr,l1,l2,err=0,ok,idebp,idebe;
  int nn,ng1,ng2;
  double x1,y1,z1,x2,y2,z2,x3,y3,z3,rien;
  double d1,dmin,dminmin;
  double zero=0.;



  /* ----------------------------------------------------------------------- */
  /* remplisssage de la liste des noeuds de bord avec resistance de contact  */
  /* et de leur correspondant rescon->numpc[]                                 */
  /* ----------------------------------------------------------------------- */
  if (rescon->npoin)
    {
      /* s'il y a deja des noeuds rescon, on rajoute les nouveaux */
      idebp=rescon->npoin;
      rescon->npoin+=nn1+nn2;
      rescon->nump=realloc(rescon->nump,rescon->npoin*sizeof(int)); verif_alloue_int1d("rescon",rescon->nump);
      rescon->numpc=realloc(rescon->numpc,rescon->npoin*sizeof(int)); verif_alloue_int1d("rescon",rescon->numpc);
    }
  else
    {
      rescon->npoin=nn1+nn2;
      rescon->nump=(int*)malloc(rescon->npoin*sizeof(int)); verif_alloue_int1d("rescon",rescon->nump);
      rescon->numpc=(int*)malloc(rescon->npoin*sizeof(int)); verif_alloue_int1d("rescon",rescon->numpc);
      idebp=0;
    }

  /* remplissage de la liste des noeuds de bord rc */
  for (i=n=0;i<nn1;i++) {rescon->nump[idebp+i]=np1[i];      rescon->numpc[idebp+i]=-1;}
  for (i=n=0;i<nn2;i++) {rescon->nump[idebp+nn1+i]=np2[i];  rescon->numpc[idebp+nn1+i]=-1;}


  /* mise en correspondance des noeuds de bord rc */
  if (maillnodes.ndim==2)
    {
      dminmin=0.; iminmin=0;
      for (i=0;i<nn1;i++)
	{
	  ng1=np1[i]; x1=maillnodes.coord[0][ng1]; y1=maillnodes.coord[1][ng1]; 
	  ok=0; dmin=1.e8;
	  for (j=0;j<nn2;j++)
	    {
	      ng2=np2[j]; 
	      x2=maillnodes.coord[0][ng2]; y2=maillnodes.coord[1][ng2]; 
	      d1=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
	      if (d1<dmin) {ok=j; dmin=d1;}
	    }
	  rescon->numpc[idebp+i]=np2[ok]; rescon->numpc[idebp+nn1+ok]=ng1; 
	  if (dmin>dminmin) {dminmin=dmin;iminmin=idebp+i;}
	}
    }
  else
    {
      dminmin=0.; iminmin=0;
      for (i=0;i<nn1;i++)
	{
	  ng1=np1[i]; x1=maillnodes.coord[0][ng1]; y1=maillnodes.coord[1][ng1]; 
	  z1=maillnodes.coord[2][ng1];
	  ok=0; dmin=1.e8;
	  for (j=0;j<nn2;j++)
	    {
	      ng2=np2[j]; 
	      x2=maillnodes.coord[0][ng2]; y2=maillnodes.coord[1][ng2]; z2=maillnodes.coord[2][ng2]; 
	      d1=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2);
	      if (d1<dmin) {ok=j; dmin=d1;}
	    }
	  rescon->numpc[idebp+i]=np2[ok]; rescon->numpc[idebp+nn1+ok]=ng1; 
	  if (dmin>dminmin) {dminmin=dmin;iminmin=idebp+i;}
	}
    }
  
  /* verification que tout c'est bien passe */
  ok=1;
  for (i=0;i<nn1;i++) {if (rescon->numpc[idebp+i]==-1) ok=0;}
  for (i=0;i<nn2;i++) {if (rescon->numpc[idebp+nn1+i]==-1) ok=0;}
  if (!ok)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n\n %%%% ERREUR  RESISTANCE DE CONTACT\n");
	  printf("       --> certaines paires de noeuds avec resistance sont ambigues\n");
	  printf("           il faut revoir le maillage\n");
	  printf("           Liste des noeuds concernes :\n");
	  for (i=0;i<nn1;i++) {if (rescon->numpc[idebp+i]==-1) printf(" %d",np1[i]+1);}
	  for (i=0;i<nn2;i++) {if (rescon->numpc[idebp+nn1+i]==-1) printf(" %d",np2[i]+1);}
	  printf("\n\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n\n %%%% ERROR CONTACT RESISTANCE\n");
	  printf("       --> some paire of nodes with contact resistance are ambiguous \n");
	  printf("           you have to modify your mesh\n");
	  printf("           List of concerned nodes :\n");
	  for (i=0;i<nn1;i++) {if (rescon->numpc[idebp+i]==-1) printf(" %d",np1[i]+1);}
	  for (i=0;i<nn2;i++) {if (rescon->numpc[idebp+nn1+i]==-1) printf(" %d",np2[i]+1);}
	  printf("\n\n");
	}
      syrthes_exit(1);
    }


  if (SYRTHES_LANG == FR)
    {
      printf("\n *** RESISTANCE DE CONTACT\n");
      printf("       - nombre de noeuds resistance de contact             : %d\n",rescon->npoin);
      printf("       - distance maximale de correspondance des noeuds RC  : %f\n",dminmin);
      printf("         atteinte pour le couple de noeuds                  : %d %d\n",rescon->nump[iminmin],
                                                                   rescon->numpc[iminmin]);
    }
  else if (SYRTHES_LANG == EN)
    {
      printf("\n *** CONTACT RESISTANCE\n");
      printf("       - number of CR nodes                             : %d\n",rescon->npoin);
      printf("       - maximum distance between correponding CR nodes : %f\n",dminmin);
      printf("         for the couple of nodes                        : %d %d\n",rescon->nump[iminmin],
                                                                   rescon->numpc[iminmin]);
    }

  if (affich.cond_rescon)
    {
      if (SYRTHES_LANG == FR)
	printf("\n--> Resist contact : %d noeuds - calcul des correspondances des noeuds \n",rescon->npoin);
      else if (SYRTHES_LANG == EN)
	printf("\n--> Periodicity : %d nodes - corresponding nodes calculation \n",rescon->npoin);
      for (i=0;i<rescon->npoin;i++)
	printf("i=%d num_glob=%d corr_glob=%d\n",i,rescon->nump[i],rescon->numpc[i]);
    }
      
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void alloue_rc(struct Contact *rescon)
{
  rescon->trav=(double*)malloc(rescon->npoin*sizeof(double));
  rescon->gassc=(double*)malloc(rescon->npoin*sizeof(double));
  rescon->gnonassc=(double*)malloc(rescon->npoin*sizeof(double));
  verif_alloue_double1d("inisol",rescon->trav);
  verif_alloue_double1d("inisol",rescon->gassc);
  verif_alloue_double1d("inisol",rescon->gnonassc);
} 
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Recuperation des variables pour les paires de RC pour programmation  |
  | de la valeurs de RC dans les fonctions utilisateur                   |
  | var1[i] = valleur de la variable au noeud i                          |
  | var2[i] = valeur de la meme variable mais au noeud de RC en face     |                           
  |======================================================================| */
void prepare_paires_rc(struct Maillage maillnodes,struct Contact rescon,
		       double *var1,double *var2,
		       struct SDparall sdparall)
{

  int i,j,nr,ne,nn;
  int num,numc,numloc;
  double t1,t2;
#ifdef _SYRTHES_MPI_
  MPI_Status status;
#endif
  

  azero1D(maillnodes.npoin,var2);  
  
  if (sdparall.nparts==1) /* en sequentiel */
    {
      for (i=0;i<rescon.npoin;i++) var2[i]=var1[rescon.nump[i]];  
    }
#ifdef _SYRTHES_MPI_
  else /* en parallele */
    {
      /* recuperation de la temperature des noeuds correspondants */
      for (nn=0;nn<sdparall.nparts;nn++)
	{
	  if (nn!=sdparall.rang)
	    {
	      for (i=0;i<sdparall.nbcommunrc[nn];i++) 
		rescon.trav[i]=var1[sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i]];
	      
	      MPI_Sendrecv_replace(rescon.trav,sdparall.nbcommunrc[nn],MPI_DOUBLE,nn,0,nn,0,
				   sdparall.syrthes_comm_world,&status);
	      
	      for (i=0;i<sdparall.nbcommunrc[nn];i++)
		var2[sdparall.tcommunrc[sdparall.adrcommun[nn]+2*i]]=rescon.trav[i];
	    }
	  else /* RC au sein de la meme partition */
	    {
	      for (i=0;i<sdparall.nbcommunrc[nn];i++)
		{
		  num=sdparall.tcommunrc[sdparall.adrcommun[nn]+2*i];
		  var2[num]=var1[num];
		}
	    }
	}
    }
#endif
}
