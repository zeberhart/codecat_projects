Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: munge
Upstream-Contact: Chris Dunlap <cdunlap@llnl.gov>
Source: https://dun.github.io/munge

Files: *
Copyright: 2007-2016 Lawrence Livermore National Security, LLC
           2002-2007 The Regents of the University of California
License: GPL-3+ and LGPL-3+

Files: debian/*
Copyright: 2006-2017 Gennaro Oliva
           2016 Karl Kornel
           2014-2015 Rémi Palancher
           2014 Ana Guerrero Lopez
           2006-2007 Dirk Eddelbuettel
License: GPL-2+

Files: */Makefile.in
       aclocal.m4
       config/libtool.m4
       config/lt~obsolete.m4
       config/ltoptions.m4
       config/ltsugar.m4
       config/ltversion.m4
Copyright: 1994-2013 Free Software Foundation, Inc
License: FSF-unlimited

Files: config/ltmain.sh
Copyright: 1996-2011 Free Software Foundation, Inc.
License: GPL-2+ with Libtool exception

Files: config/compile
       config/depcomp
       config/missing
       config/test-driver
Copyright: 1996-2013 Free Software Foundation, Inc
License: GPL-2+ with Autoconf exception

Files: src/libmissing/strlcat.c
       src/libmissing/strlcpy.c
Copyright: 1998 Todd C. Miller
License: ISC

Files: src/libmissing/getopt.h
       src/libmissing/getopt1.c
       src/libmissing/getopt.c
Copyright: 1987-2002 Free Software Foundation, Inc
License: LGPL-2.1+

Files: config/config.guess
       config/config.sub
Copyright: 1992-2014 Free Software Foundation, Inc
License: GPL-3+ with Autoconf exception

Files: config/install-sh
Copyright: 1994 X Consortium
License: MIT/X11

License: GPL-3+ and LGPL-3+
 This file is part of the MUNGE Uid 'N' Gid Emporium (MUNGE).
 For details, see <https://dun.github.io/munge/>.
 .
 MUNGE is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.  Additionally for the MUNGE library (libmunge), you
 can redistribute it and/or modify it under the terms of the GNU Lesser
 General Public License as published by the Free Software Foundation,
 either version 3 of the License, or (at your option) any later version.
 .
 MUNGE is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 and GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 and GNU Lesser General Public License along with MUNGE.  If not, see
 <http://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file `/usr/share/common-licenses/GPL-3' and the
 full text of the GNU Lesser General Public License version 3 can be found in
 the file `/usr/share/common-licenses/LGPL-3'

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
Comment:
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: FSF-unlimited
 This file is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY, to the extent permitted by law; without
 even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.

License: GPL-2+ with Libtool exception
 This is free software; see the source for copying conditions.  There is NO
 warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 .
 GNU Libtool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 As a special exception to the GNU General Public License,
 if you distribute this file as part of a program or library that
 is built using GNU Libtool, you may include this file under the
 same distribution terms that you use for the rest of that program.
 .
 GNU Libtool is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with GNU Libtool; see the file COPYING.  If not, a copy
 can be downloaded from http://www.gnu.org/licenses/gpl.html,
 or obtained by writing to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

License: GPL-2+ with Autoconf exception
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception to the GNU General Public License, if you
 distribute this file as part of a program that contains a
 configuration script generated by Autoconf, you may include it under
 the same distribution terms that you use for the rest of that program.

License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: LGPL-2.1+
 This file is part of the GNU C Library.
 .
 The GNU C Library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 The GNU C Library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
Comment:
 On Debian systems, the full text of the GNU Lesser General Public License
 version 2.1 can be found in the file `/usr/share/common-licenses/LGPL-2.1'.

License: GPL-3+ with Autoconf exception
 This file is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception to the GNU General Public License, if you
 distribute this file as part of a program that contains a
 configuration script generated by Autoconf, you may include it under
 the same distribution terms that you use for the rest of that
 program.  This Exception is an additional permission under section 7
 of the GNU General Public License, version 3 ("GPLv3").

License: MIT/X11
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.
 .
 FSF changes to this file are in the public domain.
