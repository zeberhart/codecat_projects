/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "syr_usertype.h"
#include "syr_option.h"
#include "syr_tree.h"
#include "syr_bd.h"
#include "syr_hmt_libmat.h"
#include "syr_const.h"
#include "syr_proto.h"
#include "syr_hmt_proto.h"

#ifdef _SYRTHES_MPI_
#include <mpi.h>
#endif

#ifdef _SYRTHES_CFD_
extern int nbCFD;
#endif
extern int lhumid;
extern int lray;

extern FILE *fflu;



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Surfacic balance                                              |
  |======================================================================| */
void bilsurf(struct Maillage maillnodes, struct MaillageCL maillnodeus,  
	     struct Climcfd scoupf,struct Clim flux,struct Clim echang,
	     struct Contact rescon,struct Couple scoupr,struct Clim rayinf,
	     struct HmtClimhhh hmtclimhhh, 
	     struct ConstPhyhmt constphyhmt,
	     struct Variable variable,struct Bilan bilansurf,double tempss)
{
  int i,n;
  double bil1,bil2,bil3,bil1tot,bil2tot,bil3tot;
  double bilt,bilv,bilas,bilttot,bilvtot,bilastot;
  double *tmps,*pv,*pt;

  if (syrglob_nparts==1 || syrglob_rang==0){
    if (SYRTHES_LANG == FR)
      printf("\n Bilans de Flux surfaciques (en W pour la temperature) \n");
    else if (SYRTHES_LANG == EN)
      printf("\n Surface Balance (in W for the temperature)\n");
  }

  tmps=variable.var[variable.adr_t];
  if (lhumid){
    pv=variable.var[variable.adr_pv];
    pt=variable.var[variable.adr_pt];
  }

  for (n=0;n<bilansurf.nb;n++)
    {
      bil1=bil2=bil3=0.;
      bilt=bilv=bilas=0;

      if (syrglob_nparts==1 || syrglob_rang==0) {
	printf(" SURF Time= %14.8e Balance %d",tempss,n+1);
	fprintf(fflu,"SURF Time= %14.8e Balance %3d", tempss,n+1);
      }


#ifdef _SYRTHES_CFD_
      if (scoupf.exist){
	bil3 +=cfd_calbilsurf(n,scoupf.nelem,scoupf.nume,scoupf.tfluid,scoupf.hfluid,
			      maillnodes,maillnodeus,bilansurf,tmps);
	bil3tot=somme_double_parall(bil3);
	if (syrglob_nparts==1 || syrglob_rang==0) {
	  printf(" * Convection= %12.5e",bil3tot);
	  fprintf(fflu," * Convection= %12.5e",bil3tot);
	}
      }
 #endif
      if (!lhumid){
	bil1 =calbilsurf(n,1,flux.nelem,flux.numf,flux.val1,flux.val1,
			 maillnodes,maillnodeus,bilansurf,tmps);
	bil1+=calbilsurf(n,2,echang.nelem,echang.numf,echang.val1,echang.val2,
			 maillnodes,maillnodeus,bilansurf,tmps);
	bil1+=calbilsurf(n,3,rayinf.nelem,rayinf.numf,rayinf.val1,rayinf.val2,
			 maillnodes,maillnodeus,bilansurf,tmps);
	bil1tot=somme_double_parall(bil1);
	if (syrglob_nparts==1 || syrglob_rang==0) {
	  printf(" * Bound_cond= %12.5e",bil1tot);
	  fprintf(fflu," * Bound_cond= %12.5e",bil1tot);
	}
      }
      else
	{
	  hmt_calbilsurf(n,hmtclimhhh,maillnodes,maillnodeus,
			 bilansurf,variable,constphyhmt,
			 &bilt,&bilv,&bilas);
	  bilttot=somme_double_parall(bilt);
	  bilvtot=somme_double_parall(bilv);
	  bilastot=somme_double_parall(bilas);
	  if (syrglob_nparts==1 || syrglob_rang==0) {
	    printf(" * Temp_Flux= %12.5e * Vapor_Flux= %12.5e * Dry_Air_Flux= %12.5e",bilttot,bilvtot,bilastot);
	    fprintf(fflu," * Temp_Flux= %12.5e * Vapor_Flux= %12.5e * Dry_Air_Flux= %12.5e",bilttot,bilvtot,bilastot);
	  }
	}

      if (lray){
	bil2 =calbilsurf(n,3,scoupr.nelem,scoupr.numf,scoupr.t,scoupr.h,
			 maillnodes,maillnodeus,bilansurf,tmps);
	bil2tot=somme_double_parall(bil2);
	if (syrglob_nparts==1 || syrglob_rang==0) {
	  printf(" * Rad= %12.5e *",bil2tot);
	  fprintf(fflu," * Rad= %12.5e",bil2tot);
	}
      }
      if (syrglob_nparts==1 || syrglob_rang==0) {
	printf("\n");    fprintf(fflu,"\n");
	fflush(stdout);  fflush(fflu);
      }
    }

}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Surfacic balance                                              |
  |======================================================================| */
double calbilsurf(int n,int type,int nelem,int *numf,double **v1,double **v2,
		  struct Maillage maillnodes,struct MaillageCL maillnodeus,
		  struct Bilan bilansurf,double *tmpsa)
{
  int i,j,nn,nf,nca,numref,ok,i1,i2,i3;
  double s,s12,s24,r1,r2,f1,f2,f3,phi,bil,hrayi[6];
  double spi3=Pi/3.;
  double s3=1./3.;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  s24=1./24.;
  s12=1./12.;

  bil=0;
  for (i=0;i<nelem;i++)
    {
      nf=numf[i];
      numref=maillnodeus.nrefe[nf];
      ok=0;
      if (bilansurf.ref[n][0]==-1) ok=1;
      else for (j=0;j<bilansurf.nbref[n];j++) if (numref==bilansurf.ref[n][j]) ok=1;
      if (ok)
	{
	  s=maillnodeus.volume[nf]; phi=0;
	  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
	    {
	      i1=maillnodeus.node[0][nf]; i2=maillnodeus.node[1][nf]; 
	      switch (type)
		{
		case 1:
		  f1=v1[0][i];
		  f2=v1[1][i];
		  break;
		case 2:
		  f1=v2[0][i]*(v1[0][i]-tmpsa[i1]);
		  f2=v2[1][i]*(v1[1][i]-tmpsa[i2]);
		  break;
		case 3:
		  for (j=0;j<2;j++)
		    {
		      nn=maillnodeus.node[j][nf];
		      hrayi[j] = v2[j][i]*sigma*(tmpsa[nn]+v1[j][i]+2*tkel)*
		                 ((tmpsa[nn]+tkel)*(tmpsa[nn]+tkel) + 
		                 (v1[j][i]+tkel)*(v1[j][i]+tkel) );
		    }
		  f1=hrayi[0]*(v1[0][i]-tmpsa[i1]);
		  f2=hrayi[1]*(v1[1][i]-tmpsa[i2]);
		  break;
		}
	      bil+=(f1+f2)*0.5*s;
	    }
	  else if (maillnodes.ndim==2 && maillnodes.iaxisy!=0)
	    {
	      r1=fabs(maillnodes.coord[nca][maillnodeus.node[0][nf]]);
	      r2=fabs(maillnodes.coord[nca][maillnodeus.node[1][nf]]);
	      i1=maillnodeus.node[0][nf]; i2=maillnodeus.node[1][nf]; 
	      switch (type)
		{
		case 1:
		  f1=v1[0][i];
		  f2=v1[1][i];
		  break;
		case 2:
		  f1=v2[0][i]*(v1[0][i]-tmpsa[i1]);
		  f2=v2[1][i]*(v1[1][i]-tmpsa[i2]);
		  break;
		case 3:
		  for (j=0;j<2;j++)
		    {
		      nn=maillnodeus.node[j][nf];
		      hrayi[j] = v2[j][i]*sigma*(tmpsa[nn]+v1[j][i]+2*tkel)*
		                 ((tmpsa[nn]+tkel)*(tmpsa[nn]+tkel) + 
		                 (v1[j][i]+tkel)*(v1[j][i]+tkel) );
		    }
		  f1=hrayi[0]*(v1[0][i]-tmpsa[i1]);
		  f2=hrayi[1]*(v1[1][i]-tmpsa[i2]);
		  break;
		}
	      phi=f1*(2*r1+r2) + f2*(r1+2*r2);
	      bil+=phi*spi3*s;
	    }
	  else
	    {
	      i1=maillnodeus.node[0][nf]; i2=maillnodeus.node[1][nf]; 
	      i3=maillnodeus.node[2][nf];
	      switch (type)
		{
		case 1:
		  f1=v1[0][i];
		  f2=v1[1][i];
		  f3=v1[2][i];
		  break;
		case 2:
		  f1=v2[0][i]*(v1[0][i]-tmpsa[i1]);
		  f2=v2[1][i]*(v1[1][i]-tmpsa[i2]);
		  f3=v2[2][i]*(v1[2][i]-tmpsa[i3]);
		  break;
		case 3:
		  for (j=0;j<3;j++)
		    {
		      nn=maillnodeus.node[j][nf];
		      hrayi[j] = v2[j][i]*sigma*(tmpsa[nn]+v1[j][i]+2*tkel)*
		                 ((tmpsa[nn]+tkel)*(tmpsa[nn]+tkel) + 
		                 (v1[j][i]+tkel)*(v1[j][i]+tkel) );
		    }
		  f1=hrayi[0]*(v1[0][i]-tmpsa[i1]);
		  f2=hrayi[1]*(v1[1][i]-tmpsa[i2]);
		  f3=hrayi[2]*(v1[2][i]-tmpsa[i3]);
		  break;
		}
	      phi=f1+f2+f3;
	      bil+=phi*s3*s;
	    }
	}
    }
  
  return bil;
}


#ifdef _SYRTHES_CFD_
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Bilan surfacique                                              |
  |======================================================================| */
double cfd_calbilsurf(int n,int *nelem,int **numf,double **tf,double **hf,
		      struct Maillage maillnodes,struct MaillageCL maillnodeus,
		      struct Bilan bilansurf,double *tmpsa)
{
  int i,j,nn,nf,nca,numref,ok,i1,i2,i3,nb;
  double s,r1,r2,f1,f2,f3,phi,bil;
  double spi3=Pi/3.;
  double s3=1./3.;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;

  bil=0;

  for (nb=0;nb<nbCFD;nb++)
    for (i=0;i<nelem[nb];i++)
    {
      nf=numf[nb][i];
      numref=maillnodeus.nrefe[nf];
      ok=0;
      if (bilansurf.ref[n][0]==-1) ok=1;
      else for (j=0;j<bilansurf.nbref[n];j++) if (numref==bilansurf.ref[n][j]) ok=1;
      if (ok)
	{
	  s=maillnodeus.volume[nf]; phi=0;
	  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
	    {
	      i1=maillnodeus.node[0][nf]; i2=maillnodeus.node[1][nf]; 
	      f1=hf[nb][i]*(tf[nb][i]-tmpsa[i1]);
	      f2=hf[nb][i]*(tf[nb][i]-tmpsa[i2]);
	      bil+=(f1+f2)*0.5*s;
	    }
	  else if (maillnodes.ndim==2 && maillnodes.iaxisy!=0)
	    {
	      r1=fabs(maillnodes.coord[nca][maillnodeus.node[0][nf]]);
	      r2=fabs(maillnodes.coord[nca][maillnodeus.node[1][nf]]);
	      i1=maillnodeus.node[0][nf]; i2=maillnodeus.node[1][nf]; 
	      f1=hf[nb][i]*(tf[nb][i]-tmpsa[i1]);
	      f2=hf[nb][i]*(tf[nb][i]-tmpsa[i2]);
	      phi=f1*(2*r1+r2) + f2*(r1+2*r2);
	      bil+=phi*spi3*s;
	    }
	  else
	    {
	      i1=maillnodeus.node[0][nf]; i2=maillnodeus.node[1][nf]; 
	      i3=maillnodeus.node[2][nf];
	      f1=hf[nb][i]*(tf[nb][i]-tmpsa[i1]);
	      f2=hf[nb][i]*(tf[nb][i]-tmpsa[i2]);
	      f3=hf[nb][i]*(tf[nb][i]-tmpsa[i3]);
	      phi=f1+f2+f3;
	      bil+=phi*s3*s;
	    }
	}
    }
  
  return bil;
}
#endif


#ifdef _SYRTHES_CFD_
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Surface balance (contribution of CFD codes)                   |
  |======================================================================| */
double cfd_bilsurf_conserv(int nelem,int *numf,double *tf,double *hf,
			   struct Maillage maillnodes,struct MaillageCL maillnodeus,
			   double *tmps)
{
  int i,j,nn,nf,nca,numref,ok,i1,i2,i3,nb;
  double s,r1,r2,f1,f2,f3,phi,bil,biltot;
  double spi3=Pi/3.;
  double s3=1./3.;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;

  bil=0;


  for (i=0;i<nelem;i++)
    {
      nf=numf[i];
      numref=maillnodeus.nrefe[nf];
      if (maillnodeus.type[nf][POSCOUPF])
	{
	  s=maillnodeus.volume[nf]; phi=0;
	  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
	    {
	      i1=maillnodeus.node[0][nf]; i2=maillnodeus.node[1][nf]; 
	      f1=hf[i]*(tf[i]-tmps[i1]);
	      f2=hf[i]*(tf[i]-tmps[i2]);
	      bil+=(f1+f2)*0.5*s;
	    }
	  else if (maillnodes.ndim==2 && maillnodes.iaxisy!=0)
	    {
	      r1=fabs(maillnodes.coord[nca][maillnodeus.node[0][nf]]);
	      r2=fabs(maillnodes.coord[nca][maillnodeus.node[1][nf]]);
	      i1=maillnodeus.node[0][nf]; i2=maillnodeus.node[1][nf]; 
	      f1=hf[i]*(tf[i]-tmps[i1]);
	      f2=hf[i]*(tf[i]-tmps[i2]);
	      phi=f1*(2*r1+r2) + f2*(r1+2*r2);
	      bil+=phi*spi3*s;
	    }
	  else
	    {
	      i1=maillnodeus.node[0][nf]; i2=maillnodeus.node[1][nf]; 
	      i3=maillnodeus.node[2][nf];
	      f1=hf[i]*(tf[i]-tmps[i1]);
	      f2=hf[i]*(tf[i]-tmps[i2]);
	      f3=hf[i]*(tf[i]-tmps[i3]);
	      phi=f1+f2+f3;
	      bil+=phi*s3*s;
	    }
	}
    }
  
  biltot=bil;
  biltot=somme_double_parall(bil);

  return biltot;
}
#endif
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Bilan surfacique                                              |
  |======================================================================| */
double hmt_calbilsurf(int n,struct HmtClimhhh hmtclimhhh,
		      struct Maillage maillnodes,struct MaillageCL maillnodeus,
		      struct Bilan bilansurf,struct Variable variable,
		      struct ConstPhyhmt constphyhmt,
		      double *bilt,double *bilv,double *bilas)
{
  int i,j,nn,nf,nca,numref,ok,i0,i1,i2,nb;
  double s,r0,r1,f0,f1,f2,phi,b0,b1,b2;
  double spi3=Pi/3.;
  double s3=1./3.;
  double omegamv0,omegamv1,omegamv2;
  double phiv0,phiv1,phiv2,gamaasd0,gamaasd1,gamaasd2;
  double *t,*pv,*pt;
  double pp0,pp1,pp2,tt0,tt1,tt2;

  // hmtclimhhh.t_ext(noeud)(elt)  pv_ext    pt_ext
  // hmtclimhhh.t_h(noeud)(elt)    pv_h      pt_h;

  t=variable.var[variable.adr_t];
  pv=variable.var[variable.adr_pv];
  pt=variable.var[variable.adr_pt];

  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  
  *bilt=*bilv=*bilas=0;

  for (i=0;i<hmtclimhhh.nelem;i++)
    {
      nf=hmtclimhhh.numf[i];
      numref=maillnodeus.nrefe[nf];
      ok=0;
      for (j=0;j<bilansurf.nbref[n];j++) if (numref==bilansurf.ref[n][j]) ok=1;
      if (ok)
	{
	  s=maillnodeus.volume[nf]; phi=0;
	  if (maillnodes.ndim==2)
	    {
	      i0=maillnodeus.node[0][nf]; i1=maillnodeus.node[1][nf]; 
	      /* ht(Text-T) */
	      f0=hmtclimhhh.t_h[0][i]*(hmtclimhhh.t_ext[0][i]-t[i0]);
	      f1=hmtclimhhh.t_h[1][i]*(hmtclimhhh.t_ext[1][i]-t[i1]);
	      /* calcul de phiv (flux de vapeur) */
	      pp0=hmtclimhhh.pt_ext[0][i]; pp1=hmtclimhhh.pt_ext[1][i];
	      if (pt[i0]>hmtclimhhh.pt_ext[0][i]) pp0=pt[i0];
	      if (pt[i1]>hmtclimhhh.pt_ext[1][i]) pp1=pt[i1];

	      omegamv0=constphyhmt.xmv*pv[i0]/
		(constphyhmt.xmas*pp0 + (constphyhmt.xmv-constphyhmt.xmas)*pv[i0]);
	      omegamv1=constphyhmt.xmv*pv[i1]/
		(constphyhmt.xmas*pp1 + (constphyhmt.xmv-constphyhmt.xmas)*pv[i1]);

	      phiv0 = hmtclimhhh.pv_h[0][i]*(hmtclimhhh.pv_ext[0][i]/hmtclimhhh.pt_ext[0][i] - pv[i0]/pt[i0]) 
		* hmtclimhhh.pt_ext[0][i] + omegamv0*hmtclimhhh.pt_h[0][i]*(hmtclimhhh.pt_ext[0][i]-pt[i0]) ;
	      phiv1 = hmtclimhhh.pv_h[1][i]*(hmtclimhhh.pv_ext[1][i]/hmtclimhhh.pt_ext[1][i] - pv[i1]/pt[i1]) 
		* hmtclimhhh.pt_ext[1][i] + omegamv1*hmtclimhhh.pt_h[1][i]*(hmtclimhhh.pt_ext[1][i]-pt[i1]) ;
	      tt0=hmtclimhhh.t_ext[0][i]+tkel; tt1=hmtclimhhh.t_ext[1][i]+tkel; 
	      if (phiv0<0) tt0=t[i0]+tkel;                 /* on prend L(Tparoi)  si flux de vapeur sortant */ 
	      if (phiv1<0) tt1=t[i1]+tkel;                 /* on prend L(Tparoi)  si flux de vapeur sortant */ 

	      /* flux de chaleur : h DeltaT + L(t)phiv*/
	      if (maillnodes.iaxisy==0)
		*bilt += (f0+fphyhmt_fxl(constphyhmt,tt0)*phiv0 + f1+fphyhmt_fxl(constphyhmt,tt1)*phiv1)*0.5*s;
	      else {
		r0=fabs(maillnodes.coord[nca][i0]);
		r1=fabs(maillnodes.coord[nca][i1]);
		phi=(f0+fphyhmt_fxl(constphyhmt,tt0)*phiv0)*(2*r0+r1) + (f1+fphyhmt_fxl(constphyhmt,tt1)*phiv1)*(r0+2*r1);
		*bilt+=phi*spi3*s;
	      }
	      /* flux de vapeur */
	      if (maillnodes.iaxisy==0)
		{
		  *bilv +=  (  hmtclimhhh.pv_h[0][i]*(hmtclimhhh.pv_ext[0][i]-pv[i0])
			     + hmtclimhhh.pv_h[1][i]*(hmtclimhhh.pv_ext[1][i]-pv[i1]) )*0.5*s;
		  /* remplacer ci-dessous pour les CL completes */
		  /* *bilv += (phiv0 + phiv1)*0.5*s; */
		}
	      else 
		{
		  phi=   (hmtclimhhh.pv_h[0][i]*(hmtclimhhh.pv_ext[0][i]-pv[i0]))*(2*r0+r1) 
		       + (hmtclimhhh.pv_h[1][i]*(hmtclimhhh.pv_ext[1][i]-pv[i1]))*(r0+2*r1);
		  /* remplacer ci-dessous pour les CL completes */
		  /* phi=phiv0*(2*r0+r1) + phiv1*(r0+2*r1); */
		  *bilv+=phi*spi3*s;
		}		
	      /* flux d'air sec */
	      b0=hmtclimhhh.pt_h[0][i]*(hmtclimhhh.pt_ext[0][i]-pt[i0]);
	      b1=hmtclimhhh.pt_h[1][i]*(hmtclimhhh.pt_ext[1][i]-pt[i1]);
	      
	      /* remplacer ci-dessous pour les CL completes */
	      /* has0=constphyhmt.xmas/constphyhmt.xmv * hmtclimhhh.pv_h[0][i]; */
	      /* has1=constphyhmt.xmas/constphyhmt.xmv * hmtclimhhh.pv_h[1][i]; */
	      /* b0=has0*hmtclimhhh.pv_h[0][i]/pt[i0]*(pv[i0]-hmtclimhhh.pv_h[0][i])  */
	      /* 	+ (1-omegamv0)*hmtclimhhh.pt_h[0][i]*(hmtclimhhh.pt_ext[0][i]-pt[i0]); */
	      /* b1=has1*hmtclimhhh.pv_h[1][i]/pt[i1]*(pv[i1]-hmtclimhhh.pv_h[1][i])  */
	      /* 	+ (1-omegamv1)*hmtclimhhh.pt_h[1][i]*(hmtclimhhh.pt_ext[1][i]-pt[i1]); */

	      if (maillnodes.iaxisy==0)
		*bilas+= (b0+b1)*0.5*s;
	      else{
		phi=b0*(2*r0+r1) + b1*(r0+2*r1);
		*bilas+=phi*spi3*s;
	      }
	    }
	  else
	    {
	      i0=maillnodeus.node[0][nf]; i1=maillnodeus.node[1][nf]; 
	      i2=maillnodeus.node[2][nf];
	      /* ht(Text-T) */
	      f0=hmtclimhhh.t_h[0][i]*(hmtclimhhh.t_ext[0][i]-t[i0]);
	      f1=hmtclimhhh.t_h[1][i]*(hmtclimhhh.t_ext[1][i]-t[i1]);
	      f2=hmtclimhhh.t_h[2][i]*(hmtclimhhh.t_ext[2][i]-t[i2]);
	      /* calcul de phiv */
	      pp0=hmtclimhhh.pt_ext[0][i]; pp1=hmtclimhhh.pt_ext[1][i]; pp2=hmtclimhhh.pt_ext[2][i];
	      if (pt[i0]>hmtclimhhh.pt_ext[0][i]) pp0=pt[i0];
	      if (pt[i1]>hmtclimhhh.pt_ext[1][i]) pp1=pt[i1];
	      if (pt[i1]>hmtclimhhh.pt_ext[2][i]) pp2=pt[i2];

	      omegamv0=constphyhmt.xmv*pv[i0]/
		(constphyhmt.xmas*pp0 + (constphyhmt.xmv-constphyhmt.xmas)*pv[i0]);
	      omegamv1=constphyhmt.xmv*pv[i1]/
		(constphyhmt.xmas*pp1 + (constphyhmt.xmv-constphyhmt.xmas)*pv[i1]);
	      omegamv2=constphyhmt.xmv*pv[i2]/
		(constphyhmt.xmas*pp2 + (constphyhmt.xmv-constphyhmt.xmas)*pv[i2]);

	      phiv0 = hmtclimhhh.pv_h[0][i]*(hmtclimhhh.pv_ext[0][i]/hmtclimhhh.pt_ext[0][i] - pv[i0]/pt[i0]) 
		* hmtclimhhh.pt_ext[0][i] + omegamv0*hmtclimhhh.pt_h[0][i]*(hmtclimhhh.pt_ext[0][i]-pt[i0]) ;
	      phiv1 = hmtclimhhh.pv_h[1][i]*(hmtclimhhh.pv_ext[1][i]/hmtclimhhh.pt_ext[1][i] - pv[i1]/pt[i1]) 
		* hmtclimhhh.pt_ext[1][i] + omegamv1*hmtclimhhh.pt_h[1][i]*(hmtclimhhh.pt_ext[1][i]-pt[i1]) ;
	      phiv2 = hmtclimhhh.pv_h[2][i]*(hmtclimhhh.pv_ext[2][i]/hmtclimhhh.pt_ext[2][i] - pv[i2]/pt[i2]) 
		* hmtclimhhh.pt_ext[2][i] + omegamv2*hmtclimhhh.pt_h[2][i]*(hmtclimhhh.pt_ext[2][i]-pt[i2]) ;
	      tt0=hmtclimhhh.t_ext[0][i]+tkel; tt1=hmtclimhhh.t_ext[1][i]+tkel; tt2=hmtclimhhh.t_ext[2][i]+tkel; 
	      if (phiv0<0) tt0=t[i0]+tkel;                 /* on prend L(Tparoi)  si flux de vapeur sortant */ 
	      if (phiv1<0) tt1=t[i1]+tkel;                 /* on prend L(Tparoi)  si flux de vapeur sortant */ 
	      if (phiv2<0) tt2=t[i2]+tkel;                 /* on prend L(Tparoi)  si flux de vapeur sortant */ 

	      /* flux de chaleur */
              *bilt += (f0+fphyhmt_fxl(constphyhmt,tt0)*phiv0 + 
			f1+fphyhmt_fxl(constphyhmt,tt1)*phiv1 +
			f2+fphyhmt_fxl(constphyhmt,tt2)*phiv2 ) *s3*s;

	      /* flux de vapeur */
	      *bilv +=  (  hmtclimhhh.pv_h[0][i]*(hmtclimhhh.pv_ext[0][i]-pv[i0])
			 + hmtclimhhh.pv_h[1][i]*(hmtclimhhh.pv_ext[1][i]-pv[i1]) 
		         + hmtclimhhh.pv_h[2][i]*(hmtclimhhh.pv_ext[2][i]-pv[i2]) )*s3*s;
	      /* remplacer ci-dessous pour les CL completes */
	      /* *bilv += (phiv0 + phiv1 + phiv2)*s3*s; */

	      /* flux d'air sec */
	      b0=hmtclimhhh.pt_h[0][i]*(hmtclimhhh.pt_ext[0][i]-pt[i0]);
	      b1=hmtclimhhh.pv_h[1][i]*(hmtclimhhh.pt_ext[1][i]-pt[i1]);
	      b2=hmtclimhhh.pv_h[2][i]*(hmtclimhhh.pt_ext[2][i]-pt[i2]);
	      
	      /* remplacer ci-dessous pour les CL completes */
	      /* has0=constphyhmt.xmas/constphyhmt.xmv * hmtclimhhh.pv_h[0][i]; */
	      /* has1=constphyhmt.xmas/constphyhmt.xmv * hmtclimhhh.pv_h[1][i]; */
	      /* has2=constphyhmt.xmas/constphyhmt.xmv * hmtclimhhh.pv_h[2][i]; */
	      /* b0=has0*hmtclimhhh.pv_h[0][i]/pt[i0]*(pv[i0]-hmtclimhhh.pv_h[0][i])  */
	      /* 	+ (1-omegamv0)*hmtclimhhh.pt_h[0][i]*(hmtclimhhh.pt_ext[0][i]-pt[i0]); */
	      /* b1=has1*hmtclimhhh.pv_h[1][i]/pt[i1]*(pv[i1]-hmtclimhhh.pv_h[1][i])  */
	      /* 	+ (1-omegamv1)*hmtclimhhh.pt_h[1][i]*(hmtclimhhh.pt_ext[1][i]-pt[i1]); */
	      /* b2=has2*hmtclimhhh.pv_h[2][i]/pt[i2]*(pv[i2]-hmtclimhhh.pv_h[2][i])  */
	      /* 	+ (2-omegamv2)*hmtclimhhh.pt_h[2][i]*(hmtclimhhh.pt_ext[2][i]-pt[i2]); */

	      *bilas+= (b0+b1+b2)*s3*s;
	    }
	}
    }
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Volumetric balance                                            |
  |======================================================================| */
void bilvol(struct Maillage maillnodes,struct Climcfd scouvf,struct Cvol fluxvol,
	    struct Variable variable,struct Bilan bilanvol,double tempss,
	    struct Humid humid,
	    double **var_ele,
	    struct ConstPhyhmt constphyhmt,struct ConstMateriaux *constmateriaux)
{
  int n;
  double bilT,bilcfd,bilMasseEauLiq,bilMasseEauVap,bilMasseAir;
  double bilTtot,bilcfdtot,bilMasseEauLiqtot,bilMasseEauVaptot,bilMasseAirtot;
  int j,ok,nf;
  double *t,*pv,*pt;


  if (syrglob_nparts==1 || syrglob_rang==0){
    if (SYRTHES_LANG == FR)
      printf("\n Bilans de Flux volumiques (en W pour la temperature)\n");
    else if (SYRTHES_LANG == EN)
      printf("\n Volume Heat Balance (in W for the temperature)\n");
  }

  t=variable.var[variable.adr_t];
  if (lhumid){
    pv=variable.var[variable.adr_pv];
    pt=variable.var[variable.adr_pt];
  }

  for (n=0;n<bilanvol.nb;n++)
    {
      
      if (syrglob_nparts==1 || syrglob_rang==0){
	fprintf(fflu,"VOL  Time= %14.8e Balance %3d",tempss,n+1);
	printf(" VOL Time= %14.8e Balance %3d",tempss,n+1);
      }

      bilT=bilTtot=0;
      bilcfd=bilcfdtot=0;
      bilMasseEauLiq=bilMasseEauLiqtot=0;
      bilMasseEauVap=bilMasseEauVaptot=0;
      bilMasseAir=bilMasseAirtot=0;

      bilT = calbilvol(n,fluxvol.nelem,fluxvol.nume,fluxvol.val1,fluxvol.val2,
		       maillnodes,bilanvol,t);
      bilTtot=somme_double_parall(bilT);
      if (syrglob_nparts==1 || syrglob_rang==0) {
	fprintf(fflu," * Volume_Flux= %14.7e",bilTtot);
	printf(" * Volume_Flux= %14.7e",bilTtot);
      }


#ifdef _SYRTHES_CFD_
      if (scouvf.exist)
	bilcfd += cfd_calbilvol(n,scouvf.nelem,scouvf.nume,scouvf.tfluid,scouvf.hfluid,
				maillnodes,bilanvol,t);
      bilcfdtot=somme_double_parall(bilcfd);
      if (syrglob_nparts==1 || syrglob_rang==0) {
	fprintf(fflu," * cfd_volume_flux= %14.7e ",bilcfdtot);
	printf(" * cfd_volume_flux= %14.7e ",bilcfdtot);
      }
#endif

      if (humid.actif) {
	hmt_calbilvol(n,bilanvol,maillnodes,var_ele,
		      humid,constphyhmt,constmateriaux,
		      &bilMasseEauLiq,&bilMasseEauVap,&bilMasseAir);
	bilMasseEauLiqtot=somme_double_parall(bilMasseEauLiq);
	bilMasseEauVaptot=somme_double_parall(bilMasseEauVap);
	bilMasseAirtot=somme_double_parall(bilMasseAir);
	if (syrglob_nparts==1 || syrglob_rang==0) {
	  fprintf(fflu," * Liq_Water_Mass= %14.7e   * Vap_Water_Mass= %14.7e * Tot_Water_Mass= %14.7e * Air_Mass= %14.7e",
		  bilMasseEauLiqtot,bilMasseEauVaptot,bilMasseEauLiqtot+bilMasseEauVaptot,bilMasseAirtot);
	  printf(" * Liq_Water_Mass= %14.7e   * Vap_Water_Mass= %14.7e * Tot_Water_Mass= %14.7e * Air_Mass= %14.7e",
		  bilMasseEauLiqtot,bilMasseEauVaptot,bilMasseEauLiqtot+bilMasseEauVaptot,bilMasseAirtot);
	}
     }
      if (syrglob_nparts==1 || syrglob_rang==0){
	printf("\n");   fprintf(fflu,"\n");
	fflush(stdout); fflush(fflu); 
      }
 
    }
}


#ifdef _SYRTHES_CFD_
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Surfacic balance (contribution of CFD codes)                  |
  |======================================================================| */
double cfd_calbilvol(int n,int *nelem,int **nume,double **tf,double **hf,
		     struct Maillage maillnodes,
		     struct Bilan bilanvol,double *ta)
{
  int i,j,ne,nca,numref,ok,nb;
  double s,r1,r2,r3,bil;
  double s12f2pi;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  s12f2pi=2.*Pi/3.;

  bil=0;

  for (nb=0;nb<nbCFD;nb++)
    for (i=0;i<nelem[nb];i++)
      {
	ne=nume[nb][i];
	numref=maillnodes.nrefe[ne];
	ok=0;
	if (bilanvol.ref[n][0]==-1) ok=1;
	else for (j=0;j<bilanvol.nbref[n];j++) if (numref==bilanvol.ref[n][j]) ok=1;
	if (ok)
	  {
	    if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
	      {
		bil+=tf[nb][i]*hf[nb][i]*maillnodes.volume[ne];
	      }
	    else if (maillnodes.ndim==2)
	      {
		r1=fabs(maillnodes.coord[nca][maillnodes.node[0][ne]]);
		r2=fabs(maillnodes.coord[nca][maillnodes.node[1][ne]]);
		r3=fabs(maillnodes.coord[nca][maillnodes.node[2][ne]]);
		bil+=tf[nb][i]*hf[nb][i]*(r1+r2+r3)*maillnodes.volume[ne]*s12f2pi;
	      }
	    else if (maillnodes.ndim==3)
	      {
		bil+=tf[nb][i]*hf[nb][i]*maillnodes.volume[ne];
	      }
	  }
      }
  return bil;
}
#endif





/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Calcul des masses d'eau et d'air contenu dans l'echantillon          |
  | pour le bilan numero n                                               |
  |======================================================================| */
void hmt_calbilvol(int n,struct Bilan bilanvol,
		   struct Maillage maillnodes,
		   double **var_ele,
		   struct Humid humid,
		   struct ConstPhyhmt constphyhmt,struct ConstMateriaux *constmateriaux,
		   double *masseEauLiq, double *masseEauVap, double *masseAir)
{


  int i;
  int nmat,num,numref,ok,j;
  double *te,*pve,*pte;
  double t,psat,tauv,eps;
  double bil_masse_eau_liq,biltot_masse_eau_liq;
  double bil_masse_eau_vap,biltot_masse_eau_vap;
  double bil_masse_air,biltot_masse_air;
  double resu[2];

  te=var_ele[0];
  pve=var_ele[1];
  pte=var_ele[2];


  /* Calcul de la masse d'eau totale dans l'echantillon */
  /* -------------------------------------------------- */
  bil_masse_eau_liq=bil_masse_eau_vap=0.;

  for (i=0;i<maillnodes.nelem;i++)
    {
      numref=maillnodes.nrefe[i];
      ok=0;
      for (j=0;j<bilanvol.nbref[n];j++) if (numref==bilanvol.ref[n][j]) ok=1;
      if (ok)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t); 
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);
	  
	  bil_masse_eau_liq += tauv*maillnodes.volume[i];
	  bil_masse_eau_vap += eps*pve[i]/(constphyhmt.Rv*t)*maillnodes.volume[i];
	}
    }

  *masseEauLiq = bil_masse_eau_liq;
  *masseEauVap = bil_masse_eau_vap;


  /* Calcul de la masse d'eau air dans l'echantillon */
  /* -------------------------------------------------- */
  bil_masse_air=0.;
  for (i=0;i<maillnodes.nelem;i++)
    {
      numref=maillnodes.nrefe[i];
      ok=0;
      for (j=0;j<bilanvol.nbref[n];j++) if (numref==bilanvol.ref[n][j]) ok=1;
      if (ok)
	{
	  nmat=humid.mat[i];
	  t=te[i]+tkel;
	  psat=fphyhmt_fpsat(t);
	  tauv=fmat_ftauv[nmat](constphyhmt,constmateriaux[nmat],pve[i],psat,t); 
	  eps=fphyhmt_feps(constphyhmt,constmateriaux[nmat],tauv);

	  bil_masse_air+=eps*(pte[i]-pve[i])/(constphyhmt.Ras*t)*maillnodes.volume[i];
	}
    }

  *masseAir=bil_masse_air;
 
 }



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Volumetric balance                                            |
  |======================================================================| */
double calbilvol(int n,int nelem,int *nume,double *v1,double *v2,
		  struct Maillage maillnodes,
		  struct Bilan bilanvol,double *ta)
{
  int i,j,nf,nca,numref,ok,nrefe;
  double bil,s12f2pi,r1,r2,r3;
  double x,y,z,tmoy;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  s12f2pi=2.*Pi/12.;

  bil=0;
  for (i=0;i<nelem;i++)
    {
      nf=nume[i];
      numref=maillnodes.nrefe[nf];
      ok=0;
      if (bilanvol.ref[n][0]==-1) ok=1;
      else for (j=0;j<bilanvol.nbref[n];j++) if (numref==bilanvol.ref[n][j]) ok=1;
      if (ok)
	{
	  data_element_moy(nf,maillnodes,ta,&nrefe,&x,&y,&z,&tmoy);
  
	  if (maillnodes.ndim==2 && maillnodes.iaxisy==0)
	    {
	      bil+= (v1[i]*tmoy+v2[i]) *maillnodes.volume[nf];
	    }
	  else if (maillnodes.ndim==2)
	    {
	      r1=fabs(maillnodes.coord[nca][maillnodes.node[0][nf]]);
	      r2=fabs(maillnodes.coord[nca][maillnodes.node[1][nf]]);
	      r3=fabs(maillnodes.coord[nca][maillnodes.node[2][nf]]);
	      bil+= ((v1[i]*ta[maillnodes.node[0][nf]]+v2[i]) *(2*r1+r2+r3)+
		      (v1[i]*ta[maillnodes.node[1][nf]]+v2[i])*(r1+2*r2+r3)+
		     (v1[i]*ta[maillnodes.node[2][nf]]+v2[i])*(r1+r2+2*r3))*
		      maillnodes.volume[nf]*s12f2pi;

	    }
	  else if (maillnodes.ndim==3)
	    {
	      bil+= (v1[i]*tmoy+v2[i]) *maillnodes.volume[nf];
	    }
	}
    }
  
  return bil;

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Bilan solaire                                                 |
  |======================================================================| */
void bilsolaire(struct Maillage maillnodray,
		struct Soleil soleil,struct Bilan bilansolaire,
		struct Bande bandespec,double *fdfnp1,double tempss)
{
  int i,j,n,nn,nb,ok,numref;
  double *bilf,*bilr,*bilftot,*bilrtot;

  if (SYRTHES_LANG == FR)
    printf("\nBilans de Flux solaires (W) :\n");
  else if (SYRTHES_LANG == EN)
    printf("\nSolar Flux Balance (W) :\n");


  bilf=(double*)malloc(bandespec.nb*sizeof(double));
  bilr=(double*)malloc(bandespec.nb*sizeof(double));
  verif_alloue_double1d("bilsolaire",bilf);
  verif_alloue_double1d("bilsolaire",bilr);

  printf("---------------------------------------------------------------------------------\n");
  for (nn=0;nn<bilansolaire.nb;nn++)
    {
      for (nb=0;nb<bandespec.nb;nb++) bilf[nb]=bilr[nb]=0.;

      for (i=0;i<maillnodray.nelem;i++)
	{
	  numref=maillnodray.nrefe[i];
	  ok=0;
	  for (j=0;j<bilansolaire.nbref[nn];j++) if (numref==bilansolaire.ref[nn][j]) ok=1;
	  if (ok) 
	    for (nb=0;nb<bandespec.nb;nb++)
	      {
		bilf[nb]+=soleil.diffus[nb][i]*fdfnp1[i];
		bilr[nb]+=soleil.direct[nb][i]*maillnodray.volume[i];
	      }
	}

      if (syrglob_npartsray==1){
	bilftot=bilf;
	bilrtot=bilr;
      }

#ifdef _SYRTHES_MPI_
      else
	{
	  bilftot=(double*)malloc(bandespec.nb*sizeof(double));
	  bilrtot=(double*)malloc(bandespec.nb*sizeof(double));
	  for (nb=1;nb<bandespec.nb;nb++) {
	    MPI_Allreduce(bilf+nb,bilftot+nb,1,MPI_DOUBLE,MPI_SUM,syrglob_comm_world);
	    MPI_Allreduce(bilr+nb,bilrtot+nb,1,MPI_DOUBLE,MPI_SUM,syrglob_comm_world);
	  }
	}
#endif

      if (syrglob_nparts==1 || syrglob_rang==0)
	{
	  if (SYRTHES_LANG == FR)
	    {
	      for (nb=0;nb<bandespec.nb;nb++) {
		printf("Time= %14.8e Bilan %d  bande %d : direct=%12.5e diffu=%12.5e Total=%12.5e \n",
		       tempss,nn+1,nb+1,bilrtot[nb],bilftot[nb],bilftot[nb]+bilrtot[nb]);
		fprintf(fflu,"SOL  Time= %14.8e Bilan= %3d  bande= %d  direct= %12.5e diffu= %12.5e Total= %12.5e\n",
			tempss,nn+1,nb+1,bilrtot[nb],bilftot[nb],bilftot[nb]+bilrtot[nb]);
		fflush(fflu);
	      }
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      for (nb=0;nb<bandespec.nb;nb++) {
		printf("Time= %14.8e Balance %d  Spectral range %d : direct=%12.5e diffu=%12.5e Total=%12.5e\n",
		       tempss,nn+1,nb+1,bilrtot[nb],bilftot[nb],bilftot[nb]+bilrtot[nb]);
		fprintf(fflu,"SOL  Time= %14.8e Balance %d  Spectral_range %d  direct= %12.5e diffu= %12.5e Total= %12.5e\n",
			tempss,nn+1,nb+1,bilrtot[nb],bilftot[nb],bilftot[nb]+bilrtot[nb]);
		fflush(fflu);
	      }
	    }
	}
    }
  
  printf("---------------------------------------------------------------------------------\n");
  printf("\n");

  free(bilf);  free(bilr);  
  if (syrglob_npartsray>1){free(bilftot);  free(bilrtot);}
}

