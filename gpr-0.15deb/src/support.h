/*
 * GPR
 * Copyright (C) 2000 CompuMetric Labs, Inc.
 *
 * For more information contact:
 *        Thomas Hubbell
 *        CompuMetric Labs, Inc.
 *        5920 Grelot Road, Suite C-2
 *        Mobile, AL 36609
 * 
 * Voice: (334) 342-2220
 *   Fax: (334) 343-2261
 * Email: thubbell@compumetric.com
 *   Web: http://www.compumetric.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *    
 *        File: Support.h
 * 
 * Description: Support functions needed by gpr.
 */

#ifdef USE_GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#endif

/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (PACKAGE, String)
#  define Q_(String) g_strip_context ((String), gettext (String))
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define Q_(String) g_strip_context ((String), (String))
#  define N_(String) (String)
#endif

/*
 * Public Functions.
 */

/*
 * This function returns a widget in a component created by Glade.
 * Call it with the toplevel widget in the component (i.e. a window/dialog),
 * or alternatively any widget in the component, and the name of the widget
 * you want returned.
 */
GtkWidget *lookup_widget(GtkWidget * widget, const gchar * widget_name);

/* get_widget() is deprecated. Use lookup_widget instead. */
#define get_widget lookup_widget


/*
 * Private Functions.
 */

/* This is used to create the pixmaps in the interface. */
GtkWidget *create_pixmap(GtkWidget * widget, const gchar * filename,
			 gboolean gnome_pixmap);

/* This creates a pixmap from an xpm struct. */
GtkWidget *create_pixmap_from_data
(
 GtkWidget * widget, 
 gchar ** xpm_data,
 gboolean gnome_pixmap
);

//GdkImlibImage *create_image(const gchar * filename);
