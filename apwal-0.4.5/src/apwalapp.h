/* apwalapp.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#ifndef APWALAPP__H
#define APWALAPP__H

#include "common.h"

// ----------------------------------------------------------------------------
typedef struct apwalapp_t
{
  GtkWidget *window;
  GtkWidget   *notebook;
  //            *page 'Editor'
  GtkWidget       *property_frame;
  GtkWidget       *editor_frame;
  //            *page 'Icon selection'
  GtkWidget       *iconsel_frame;
  //            *page 'Icon selection preference'
  GtkWidget       *iconsel_pref_frame;
  //            *page 'About Apwal'
  GtkWidget       *apwal_pref_frame;
  GtkWidget       *about_frame;

  GtkWidget *window2;
  GtkWidget   *notebook2;
  //            *page 'Icon selection'
  //            *page 'Icon selection preference'
  GtkWidget   *w2_btn_apply;
  GtkWidget   *w2_btn_close;
  GtkWidget   *w2_btn_cancel;
  GtkWidget   *w2_btn_ok;

  
  GtkTooltips  *tips;

  struct apwal_pref_t     *apwal_pref;
  struct editor_t         *editor;
  struct property_t       *prop;
  struct icon_selection_t *iconsel;
  struct iconsel_pref_t   *iconsel_pref;
  struct filesel_t        *filesel;
  struct about_t          *about;
  struct splash_t         *splash;

  struct app_list_t       *apps;                    // list of apps
  struct app_t            *selected_app;            // app currently selected

} apwalapp_t;
// ----------------------------------------------------------------------------

apwalapp_t *apwalapp_new(void);
void apwalapp_selected_app_modified(apwalapp_t *apwal);
void apwalapp_selected_icon_modified(apwalapp_t *apwal);

void apwalapp_goto_iconsel(apwalapp_t *apwal);
void apwalapp_goto_editor(apwalapp_t *apwal);
void apwal_iconsel_win2tab(apwalapp_t *apwal);
void apwal_iconsel_tab2win(apwalapp_t *apwal);
void apwal_iconsel_set_modal(apwalapp_t *apwal);
void apwal_iconsel_set_not_modal(apwalapp_t *apwal);

#endif /*APWALAPP__H*/
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
