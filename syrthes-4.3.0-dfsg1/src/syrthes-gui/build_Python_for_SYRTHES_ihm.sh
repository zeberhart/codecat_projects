#!/bin/bash
#This file is for people installing SYRTHES IHM, 
#You need first to install Python and different packages.
#To simplify the installation, 
#included  are the packages sources, (alternatively you may download them from the web)
#The ones included should work together and be compatible.
#Note also that according the computer, compiling qt can be quite long (several hours).

# To help, here is the shell which does the installation automatically.
# To use it, create a directory (with the name you want)
# In that directory create two sub-directories 
#     opt
#     src

# put in src the uncompressed sources of Python and all the external librairies
# ie the files  
#      Python-2.6.4.tgz,
#      sip-4.10.tar.gz,
#      qt-evrywhere-opensource-src-4.6.2.tar.gz,
#      PyQt-x11-gpl-4.7.tar.gz,
#      PyQwt-5.2.0.tar.gz,
#      cx_Freeze-4.1.2.tar.gz
#
# Use unzip on all packages, so you should have :
#      Python-2.6.4,
#      sip-4.10,
#      qt-everywhere-opensource-src-4.6.2,
#      PyQt-x11-gpl-4.7,
#      PyQwt-5.2.0,
#      cx_Freeze-4.1.2
#
# The present shell should be at the same level as directories src et opt. 
# So you should have the following organisation. 
# |
# |_opt______Python-2.6.4
# |      |___qt
# |_src__|___cx_Freeze-4.1.2
# |    __|___PyQt-x11-gpl-4.7
# |      |___PyQwt-5.2.0
# |      |___Python-2.6.4
# |      |___qt-everywhere-opensource-src-4.6.2
# |      |___sip-4.10
# |_Python_Install_script.sh
#
#
#----------------------------------------------------------
# run the shell below named build_Python_for_SYRTHES_ihm.sh
# whose commands are below:
# You will have to answer some questions (licence of qt) - answer yes
# If you plan to use a more recent python, then replace in the following commands
# Python-2.6.4 by Python-2.6.x
#
# Normally, this long installation of Python and packages has to be done only once !
# and is not intented to be done by end-user, 
# but only by experienced people installing the package.
# First a try with the pre-compiled version of SYRTHES GUI should be tempted.
#
# Once this has been done 
# To build the stand alone SYRTHES 
# ihm (with no packages necessary or conflict with another Python version)
# go in the directory syrthes-gui/Sources_syrthes-GUI_1.0.0
#
# using the python just installed, type the command :
# python setup_cxfl.py build
#
# 
# To test, if the SYRTHES ihm starts, type : 
# ./SyrthesMain
#-----------------------------------------------------------------------------------
# 
# Compilation de Python-2.6.4
ici=`pwd`
echo "Compilation de Python-2.6.4"
echo "Compilation de Python-2.6.4">compil.log

cd $ici/src/Python-2.6.4
./configure --prefix=$ici/opt/Python-2.6.4 2>&1| tee -a $ici/compil.log
make 2>&1| tee -a $ici/compil.log
make install 2>&1| tee -a $ici/compil.log

# Compilation de sip-4.10

echo "Compilation de sip-4.10"
echo "Compilation de sip-4.10">>compil.log

cd  ../sip-4.10
../../opt/Python-2.6.4/bin/python configure.py 2>&1| tee -a $ici/compil.log
make 2>&1| tee -a $ici/compil.log
make install 2>&1| tee -a $ici/compil.log

# Compilation de Qt

echo "Compilation de Qt"
echo "Compilation de Qt">>compil.log

cd ../qt-everywhere-opensource-src-4.6.2
./configure --prefix=../../opt/qt 2>&1| tee -a $ici/compil.log
make 2>&1| tee -a $ici/compil.log
make install 2>&1| tee -a $ici/compil.log

# Compilation de PyQt-x11-gpl-4.7

echo "Compilation de PyQt-x11-gpl-4.7"
echo "Compilation de PyQt-x11-gpl-4.7">>compil.log

cd ../PyQt-x11-gpl-4.7
../../opt/Python-2.6.4/bin/python configure.py -q ../../opt/qt/bin/qmake 2>&1| tee -a $ici/compil.log
make 2>&1| tee -a $ici/compil.log
make install 2>&1| tee -a $ici/compil.log

# Compilation de PyQwt-5.2.0

echo "Compilation de PyQwt-5.2.0"
echo "Compilation de PyQwt-5.2.0">>compil.log

cd ../PyQwt-5.2.0/configure
../../../opt/Python-2.6.4/bin/python configure.py -Q ../qwt-5.2 2>&1| tee -a $ici/compil.log
make 2>&1| tee -a $ici/compil.log
make install 2>&1| tee -a $ici/compil.log

# Compilation de cx_Freeze

echo "Compilation de cx_Freeze"
echo "Compilation de cx_Freeze">>compil.log

cd ../../cx_Freeze-4.1.2
../../opt/Python-2.6.4/bin/python setup.py build 2>&1| tee -a $ici/compil.log
../../opt/Python-2.6.4/bin/python setup.py install 2>&1| tee -a $ici/compil.log

#------------------------------------------------------------------
#Hope it will work for you
#Christophe Peniguel