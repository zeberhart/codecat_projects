/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "syr_usertype.h"
#include "syr_tree.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_parall.h"
#include "syr_proto.h"
#include "syr_hmt_proto.h"
#include "syr_const.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
#endif

extern struct Performances perfo;
extern struct Affichages affich;
extern char nomdata[CHLONG];
extern FILE *fdata;

extern int nbVar;

static char ch[CHLONG],motcle[CHLONG],motcle2[CHLONG],repc[CHLONG],formule[CHLONG];
static double dlist[100];
static int ilist[100];

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Typage des faces de bord                                             |
  | Premiere lecture des CL pour identification des types de facettes    |
  |                                                                      |
  | on initialise le type=[1,0,1,0,1 0...MAXPOS] ==> elt ech+rayinf      |
  | (type[0]>0 ==> elt de type flux                                      |
  |======================================================================| */
void hmt_decode_clim(struct MaillageBord *maillnodebord)
{
  int n,i,j,nr,numvar;
  int n0,n1,n2;
  int n0tot,n1tot,n2tot;

  int i1,i2,i3,i4,ii,ii2,id,nb,ok,ityp;
  double val;

  int nbc=3;     /* 0=hhh 1=contact  2=?? */
  int **ref;    


  /* Allocations */
  /* ----------- */

  ref=(int**)malloc(nbc*sizeof(int*));
  for (i=0;i<nbc;i++)                    
    {
      ref[i]=(int*)malloc(MAX_REF*sizeof(int));
      for (j=0;j<MAX_REF;j++) ref[i][j]=0;
    }


  /* 1ere passe : decodage des references pour les CL */
  /* ================================================ */
  
  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);

	  if (!strncmp(motcle,"CLIM_HMT",8)) 
	    {
	      ityp=0;
	      if (strstr(motcle,"FCT")) ityp=1;
	      else if (strstr(motcle,"PROG")) ityp=2;

	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
	      if (!strcmp(motcle,"HHH"))
		{  
		  if (nbVar==2)
		    {
		      switch(ityp){
		      case 0: rep_ndbl(5,dlist,&ii,ch+id); id+=ii; break;
		      case 1: for (i=0;i<5;i++){ii=rep_ch(formule,ch+id);id+=ii;} break;
		      }
		    }
		  else
		    {
		      switch(ityp){
		      case 0: rep_ndbl(7,dlist,&ii,ch+id); id+=ii; break;
		      case 1: for (i=0;i<7;i++){ii=rep_ch(formule,ch+id);id+=ii;} break;
		      }
		    }
		  rep_listint(ilist,&nb,ch+id);
		  verif_liste_ref_0N("CLIM_HMT   HHH",nb,ilist);
		  for (n=0;n<nb;n++)  ref[0][ilist[n]]=1;
		}
	      
	    }

	}
    }




 /* 0=hhh 1=contact 2=??  */

  /* compte des facettes de chaque type de CL */
  /* ---------------------------------------- */
  /* on repere aussi les elts de type flux : type[POSTYPFLUX]>0 si l'elt est de type flux */

  n0=n1=n2=0;
  for (i=0;i<maillnodebord->nelem;i++)
    {
      nr=maillnodebord->nrefe[i];
      
      if (ref[0][nr]) {maillnodebord->type[i][POSHMTHHH]=maillnodebord->type[i][POSTYPFLUX]=1;  n0++;}
    }
  
  n0tot=n0;     
  
  if (syrglob_nparts>1)
    {
      n0tot=somme_int_parall(n0);
    }
  
  if ((syrglob_nparts==1 || syrglob_rang==0))
    {
      if (SYRTHES_LANG == FR){
	printf("\n *** hmt_decode_clim : conditions aux limites\n");
	printf("           -----------------------------------------------\n");
	printf("           |  Conditions aux limites    |  Nbre de faces |\n");
	printf("           -----------------------------|-----------------\n");
	printf("           |       HMT HHH              | %10d     |\n",n0tot);
	printf("           -----------------------------------------------\n");
      }
      else if (SYRTHES_LANG == EN){
	printf("\n *** hmt_decode_clim : boundary conditions\n");
	printf("           -----------------------------------------------\n");
	printf("           |    Boundary conditions     |  Nbre of faces |\n");
	printf("           -----------------------------|-----------------\n");
	printf("           |       HMT HHH              | %10d     |\n",n0tot);
	printf("           -----------------------------------------------\n");
      }
    }
  
  /* liberation memoire */
  for (i=0;i<nbc;i++) free(ref[i]);
  free (ref);
      
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void hmt_lire_limite(struct HmtClimhhh *hmtclimhhh,
		     struct Maillage maillnodes,struct MaillageBord maillnodebord,
		     struct MaillageCL maillnodeus)
{
  int i,j,k,nn,indic=0;
  int i1,i2,i3,i4,id,ok=1,ii,nb,nr,n;
  int *tab;
  double val;

  fseek(fdata,0,SEEK_SET);

  printf("\n");


  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (SYRTHES_LANG == FR)  
      printf("\n *** LECTURE DES CONDITIONS AUX LIMITES HMT DANS LE FICHIER DE DONNEES\n");
    else if (SYRTHES_LANG == EN)
      printf("\n *** READING OF THE HMT BOUNDARY CONDITIONS IN THE DATA FILE\n");
  }

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);

	  if (!strcmp(motcle,"CLIM_HMT")) 
	    {
	      indic=1;
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"HHH")	&&
		  !strstr(motcle ,"FCT")  && !strstr(motcle,"PROG")) /* lire que si ce n'est ni une fct ni un prog */ 
		hmt_lire_hhh(hmtclimhhh,maillnodeus,ch,id); /* ??????????????? quid de maillnodeus ???? */
	    }


	} /* fin si CLIM_HMT */
    }


  if (!indic && syrglob_rang==0) 
    if (SYRTHES_LANG == FR)
      printf("        --> Aucune condition limite HMT n'est presente dans le fichier de donnees\n");
    else if (SYRTHES_LANG == EN)
      printf("        --> No HMT boundary condition is present in the data file\n");
  

  if (affich.cond_clim)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n>>>hmt_lire_limite :Conditions aux limites ");
	  printf("---------------------------------------------------------\n");
	  printf(">>>hmt_lire_limite : Impression des CL HHH\n");
	  printf("   Non disponible a ce jour\n");

	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n>>>hmt_lire_limite :Boundary condition for the temperature variable\n");
	  printf("---------------------------------------------------------------\n");
	  printf(">>>hmt_lire_limite : Printing relative to inifinite radiation\n");
	  printf("   Not avalaible yet\n");
	}
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void hmt_lire_hhh(struct HmtClimhhh *hmtclimhhh,struct MaillageCL maillnodeus,
		  char *ch,int id)
{
  int i,j,indic=0;
  int ii,nb,nr,n;
  int *tab;
  double val;

  indic=1;
  if (nbVar==3)
    rep_ndbl(7,dlist,&ii,ch+id);
  else
    rep_ndbl(5,dlist,&ii,ch+id);

  rep_listint(ilist,&nb,ch+id+ii);
  verif_liste_ref_0N("CLIM_HMT=   HHH",nb,ilist);
  
  if (hmtclimhhh->nelem)
    for (n=0;n<nb;n++)
      {
	nr=ilist[n];
	for (i=0;i<hmtclimhhh->nelem;i++) 
	  if (maillnodeus.nrefe[hmtclimhhh->numf[i]]==nr) 
	    for (j=0;j<hmtclimhhh->ndmat;j++) 
	      {
		hmtclimhhh->t_ext [j][i]=dlist[0];
		hmtclimhhh->t_h   [j][i]=dlist[1];
		hmtclimhhh->pv_ext[j][i]=dlist[2];
		hmtclimhhh->pv_h  [j][i]=dlist[3];
		if (nbVar==3){
		  hmtclimhhh->pt_ext[j][i]=dlist[4];
		  hmtclimhhh->pt_h  [j][i]=dlist[5];
		}	    
	      }
      }
  
  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR) 
	printf("        --> HMT - HHH (T=%f,h=%e)  (PV=%f,h=%e) (PT=%f,h=%e)\n                       impose sur les references : ",
	       dlist[0],dlist[1],dlist[2],dlist[3],dlist[4],dlist[5]);
      else if (SYRTHES_LANG == EN)
	printf("        --> HMT - HHH (T=%f,h=%e)  (PV=%f,h=%e) (PT=%f,h=%e)\n                       precribed on references : ",
	       dlist[0],dlist[1],dlist[2],dlist[3],dlist[4],dlist[5]);
      for (n=0;n<nb;n++) printf(" %d",ilist[n]);
      printf("\n");
    }
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */

void hmt_cree_liste_clim(struct MaillageCL maillnodeus,struct HmtClimhhh *hmtclimhhh)
{
  int i,j,nr,ne,k,n,nb,netot,rc;
  int dir,ok;
  int *ne0,*ne1,*ne2;
  int nfhhh=0;


  hmtclimhhh->npoin=hmtclimhhh->nelem=hmtclimhhh->ndmat=0; 


  /* Traitement des CL de type hhh */
  /* ----------------------------- */

 
  for (nfhhh=i=0;i<maillnodeus.nelem;i++)
    if (maillnodeus.type[i][POSHMTHHH]) nfhhh++;
	
  
  hmtclimhhh->npoin = hmtclimhhh->nelem = nfhhh; 

  if (hmtclimhhh->nelem)
    {
      hmtclimhhh->numf=(int*)malloc(nfhhh*sizeof(int));
      verif_alloue_int1d("hmt_cree_liste_clim",hmtclimhhh->numf);
      hmtclimhhh->ndmat=maillnodeus.ndmat; 
      perfo.mem_cond+=nfhhh*sizeof(int);
    }

  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  if (hmtclimhhh->nelem)
    for (i=nfhhh=0;i<maillnodeus.nelem;i++)
      if (maillnodeus.type[i][POSHMTHHH])    {hmtclimhhh ->numf[nfhhh]=i; nfhhh++;}

  
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Allocation des structures de donnees                                 |
  |======================================================================| */
void hmt_alloue_val_clim(struct HmtClimhhh *hmtclimhhh)
{
  int i,j;
  double *p,*p1,*p2;


  if (hmtclimhhh->nelem>0) 
    {
      /* sur T */
      hmtclimhhh->t_ext=(double**)malloc(hmtclimhhh->ndmat*sizeof(double*));
      for (i=0;i<hmtclimhhh->ndmat;i++) hmtclimhhh->t_ext[i]=(double*)malloc(hmtclimhhh->nelem*sizeof(double));

      hmtclimhhh->t_h=(double**)malloc(hmtclimhhh->ndmat*sizeof(double*));
      for (i=0;i<hmtclimhhh->ndmat;i++) hmtclimhhh->t_h[i]=(double*)malloc(hmtclimhhh->nelem*sizeof(double));

      verif_alloue_double2d(hmtclimhhh->ndmat,"alloue",hmtclimhhh->t_ext);
      verif_alloue_double2d(hmtclimhhh->ndmat,"alloue",hmtclimhhh->t_h);

      for (j=0;j<hmtclimhhh->ndmat;j++) 
	for (i=0;i<hmtclimhhh->nelem;i++) hmtclimhhh->t_ext[j][i]=hmtclimhhh->t_h[j][i]=0;
      perfo.mem_cond+=(hmtclimhhh->ndmat*hmtclimhhh->nelem) * 2 * sizeof(double);



      /* sur PV */
      hmtclimhhh->pv_ext=(double**)malloc(hmtclimhhh->ndmat*sizeof(double*));
      for (i=0;i<hmtclimhhh->ndmat;i++) hmtclimhhh->pv_ext[i]=(double*)malloc(hmtclimhhh->nelem*sizeof(double));

      hmtclimhhh->pv_h=(double**)malloc(hmtclimhhh->ndmat*sizeof(double*));
      for (i=0;i<hmtclimhhh->ndmat;i++) hmtclimhhh->pv_h[i]=(double*)malloc(hmtclimhhh->nelem*sizeof(double));

      verif_alloue_double2d(hmtclimhhh->ndmat,"alloue",hmtclimhhh->pv_ext);
      verif_alloue_double2d(hmtclimhhh->ndmat,"alloue",hmtclimhhh->pv_h);

      for (j=0;j<hmtclimhhh->ndmat;j++) 
	for (i=0;i<hmtclimhhh->nelem;i++) hmtclimhhh->pv_ext[j][i]=hmtclimhhh->pv_h[j][i]=0;
      perfo.mem_cond+=(hmtclimhhh->ndmat*hmtclimhhh->nelem) * 2 * sizeof(double);


      /* sur PT */
      if (nbVar==3){
	hmtclimhhh->pt_ext=(double**)malloc(hmtclimhhh->ndmat*sizeof(double*));
	for (i=0;i<hmtclimhhh->ndmat;i++) hmtclimhhh->pt_ext[i]=(double*)malloc(hmtclimhhh->nelem*sizeof(double));
	
	hmtclimhhh->pt_h=(double**)malloc(hmtclimhhh->ndmat*sizeof(double*));
	for (i=0;i<hmtclimhhh->ndmat;i++) hmtclimhhh->pt_h[i]=(double*)malloc(hmtclimhhh->nelem*sizeof(double));
	
	verif_alloue_double2d(hmtclimhhh->ndmat,"alloue",hmtclimhhh->pt_ext);
	verif_alloue_double2d(hmtclimhhh->ndmat,"alloue",hmtclimhhh->pt_h);
	
	for (j=0;j<hmtclimhhh->ndmat;j++) 
	  for (i=0;i<hmtclimhhh->nelem;i++) hmtclimhhh->pt_ext[j][i]=hmtclimhhh->pt_h[j][i]=0;
	perfo.mem_cond+=(hmtclimhhh->ndmat*hmtclimhhh->nelem) * 2 * sizeof(double);
      }

    }
  
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;
}

