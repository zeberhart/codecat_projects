#ifndef lint
static const char sccsid[] = "@(#)parse.c 2.6 02/03/11 falk" ;
static const char rcsid[] = "$Id: parse.c,v 1.4 2003/12/22 06:03:14 efalk Exp $" ;
#endif

#include <stdio.h>
#include <stdlib.h>	/* defines getenv(3) */
#include <pwd.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/param.h>

#include "sortmail.h"
#include "utils.h"

extern	int	errno ;

#ifdef	DEBUG
#define	register
#endif

static	char	spooldir[] = SPOOLDIR ;

static	void	includerc();
static	void	read_initfile() ;
static	void	parse_var() ;
static	void	add_include() ;
static	void	add_exclude() ;
static	void	add_bouncecheck() ;
static	void	add_header() ;
#ifdef	COMMENT
static	void	expand_variables() ;
#endif	/* COMMENT */

#define	NN(str)	((str)==NULL ? "-none-" : (str))



void
read_initfiles()
{
	struct passwd	*passwd ;
	char		*ptr ;

	/* first, try to set up some of the defaults */

	if( user == NULL ) {
	  passwd = getpwuid(getuid()) ;
	  if( (user = strdup(passwd->pw_name)) == NULL )
	    die(4,"out of memory") ;
	  setenv("user",user,1) ;
	}

	if( home == NULL ) {
	  if( (passwd = getpwnam(user)) != NULL )
	    home = strdup(passwd->pw_dir) ;
	  else if( (home = getenv("HOME")) == NULL )
	    die(6,"can't determine %s's directory",user) ;
	}

	if( mailbox == NULL ) {
	  mailbox = (char *) malloc(sizeof(spooldir) + strlen(user)) ;
	  strcpy(mailbox, spooldir) ;
	  strcat(mailbox, user) ;
	  setenv("mailbox",mailbox,1) ;
	}

	if( mailrc == NULL ) {
	  mailrc = ".mailrc" ;
	  setenv("mailrc",mailrc,1) ;
	}

	if( sortmailrc == NULL ) {
	  sortmailrc = ".sortmailrc" ;
	  setenv("sortmailrc",sortmailrc,1) ;
	}

	if( folder == NULL ) {
	  folder = FOLDER ;
	  setenv("folder",folder,1) ;
	}

	read_initfile("/usr/lib/sortmailrc", False) ;
	read_initfile("/usr/local/lib/sortmailrc", False) ;
	read_initfile("/etc/sortmailrc", False) ;
	read_initfile("/usr/etc/sortmailrc", False) ;
	read_initfile("/usr/local/etc/sortmailrc", False) ;
	read_initfile(mailrc, False) ;
	read_initfile(sortmailrc, False) ;


	/* parse the last of the variables */

	vacation = getenv("vacation") ;
	from = getenv("from") ;
	reject = getenv("reject") ;
	folder = getenv("folder") ;
	deflt = getenv("default") ;
	if( (ptr = getenv("delay")) != NULL )
	  delayTime = atoi(ptr) ;
	if( (ptr = getenv("limit")) != NULL )
	  sizeLimit = getLimit(ptr) ;
	if( (ptr = getenv("timeout")) != NULL )
	  timeout = atoi(ptr) ;
	if( (ptr = getenv("maxlines")) != NULL )
	  maxlines = atoi(ptr) ;
	if( (tmpdir = getenv("TMPDIR")) == NULL || strlen(tmpdir) > 100 )
	  tmpdir = "/tmp" ;
	if( (sendmail = getenv("sendmail")) == NULL )
	  sendmail = SENDMAIL ;

	if( verbose >= 3 ) {
	  logFile("$USER = %s\n", NN(user)) ;
	  logFile("$HOME = %s\n", NN(home)) ;
	  logFile("mailbox = %s\n", NN(mailbox)) ;
	  logFile("mailrc = %s\n", NN(mailrc)) ;
	  logFile("sortmailrc = %s\n", NN(sortmailrc)) ;
	  logFile("deflt = %s\n", NN(deflt)) ;
	  logFile("from = %s\n", NN(from)) ;
	  logFile("vacation = %s\n", NN(vacation)) ;
	  logFile("folder = %s\n", NN(folder)) ;
	  logFile("verbose = %d\n", verbose) ;
	}
}


static	void
read_initfile( char *filename, bool complain )
{
register FILE	*ifile ;
	Ictx	in ;
	char	line[MAXPATHLEN] ;

	if( *filename == '~' )
	{
	  filename_expand(line, filename) ;
	  filename = line ;
	}
	else if( *filename != '/' &&	/* relative path, relative to home */
		strncmp(filename,"./",2) != 0 )
	{
	  strcpy(line,home) ;
	  strcat(line,"/") ;
	  strcat(line,filename) ;
	  filename = line ;
	}

	if( (ifile = fopen(filename, "r")) != NULL )
	{
	  logFilev(2, "read %s\n", filename) ;
	  in.ptr = "" ;
	  in.file = ifile ;
	  in.lineno = in.lineno0 = 0 ;
	  advanceLine(&in) ;
	  while( *in.ptr != EOF )
	  {
	    if( *in.ptr == '/' ||	/* pattern */
		*in.ptr == '[' ||	/* ip pattern */
		*in.ptr == '(' )	/* expression */
	      ExpressionAdd(&in) ;
	    else
	    {
	      if( strmatch(in.ptr,"set") )		/* variable */
		parse_var(in.ptr, in.lineno) ;
	      else if( strmatch(in.ptr, "includelist"))	/* include list */
		add_include(in.ptr+11, in.lineno) ;
	      else if( strmatch(in.ptr, "excludelist"))	/* exclude list */
		add_exclude(in.ptr+11, in.lineno) ;
	      else if( strmatch(in.ptr, "header") )	/* append header */
		add_header(in.ptr+6,False, in.lineno) ;
	      else if( strmatch(in.ptr, "replace") )	/* replace header */
		add_header(in.ptr+7,True, in.lineno) ;
	      else if( strmatch(in.ptr, "bouncecheck") ) /* bounce DB check */
		add_bouncecheck(in.ptr+11, in.lineno) ;
	      else if( strmatch(in.ptr, "includerc") ) /* bounce DB check */
		includerc(in.ptr+9, in.lineno) ;
	      /* else ignore it */
	      advanceLine(&in) ;
	    }
	  }
	  fclose(ifile) ;
	}
	else if( complain ) {
	  logFile("cannot open file %s: %s\n", filename, strerror(errno)) ;
	}
}



static	void
parse_var( register char *ptr, int lineno)
{
	char	*ptr2, *ptr3 ;

	ptr += 3 ;	/* skip "set" */
	if( !isspace(*ptr) )
	  return ;		/* something was wrong */

	ptr = firstNonBlank(ptr) ;
	if( !isalnum(*ptr) )
	  return ;

	/* Expand the entire input line */

	if( (ptr2 = variable_expand(ptr, False)) == NULL ) {
	  logFile("cannot expand %s\n", ptr) ;
	  return ;
	}
	ptr = ptr2 ;

	/* tighten it up by removing any spaces surrounding the '=' */

	/* skip past variable name */
	for(ptr2=ptr;	isalnum(*ptr2) || *ptr2 == '_'; ++ptr2) ;

	if( (ptr3 = strchr(ptr2,'=')) == NULL )
	  strcpy(ptr2,"=") ;
	else
	{
	  if( ptr3 > ptr2 )
	    strcpy(ptr2,ptr3) ;
	  ++ptr2 ;
	  ptr3 = firstNonBlank(ptr2) ;
	  if( ptr3 > ptr2 )
	    strcpy(ptr2,ptr3) ;
	}

	putenv(ptr) ;			/* and set it */


	/* Update variables that can't wait.  */

	if( strmatch(ptr,"user") )
	  user = ptr+4+1 ;
	else if( strmatch(ptr,"HOME") )
	  home = ptr+4+1 ;
	else if( strmatch(ptr,"mailrc") )
	  mailrc = ptr+6+1 ;
	else if( strmatch(ptr,"sortmailrc") )
	  sortmailrc = ptr+10+1 ;
	else if( strmatch(ptr,"verbose") )
	  verbose = atoi(ptr+7+1) ;
	else if( strmatch(ptr,"logfile") )
	{
	  logfilename = ptr+7+1 ;
	  if( logfile != stderr )
	    fclose(logfile) ;
	  if( (logfile = pathOpen(logfilename, "a")) == NULL )
	    logfile = stderr ;
	}
}


static	void
includerc( register char *ptr, int lineno)
{
	char	*p2 ;

	ptr = firstNonBlank(ptr) ;
	removeNL(ptr) ;

	if( (p2 = variable_expand(ptr, False)) == NULL ) {
	  logFile("cannot expand %s\n", ptr) ;
	  return ;
	}
	read_initfile(p2, True) ;
	free(p2) ;
}


	/* append a command to the linked list */

void
append_cmd(Cmd *cmd)
{
	cmd->next = NULL ;
	if( cmds == NULL )
	  cmds = cmd ;
	else
	  lastcmd->next = cmd ;
	lastcmd = cmd ;
}





	/* Add an include/exclude filename to the list */

static	void
make_include( register char *ptr, cmdType type, int lineno)
{
	IncludeInfo *info ;

	info = MALLOC(IncludeInfo,1) ;
	if( info == NULL ) {
	  fprintf(logfile, "out of memory\n") ;
	  return ;
	}

	ptr = firstNonBlank(ptr) ;
	if( (info->filename=variable_expand(ptr, False)) == NULL ) {
	  fprintf(logfile, "cannot expand %s\n", ptr) ;
	  return ;
	}
	info->c.type = type ;
	info->c.lineno = lineno ;
	append_cmd((Cmd *)info) ;
}



	/* Add an include filename to the list */

static	void
add_include( char *ptr, int lineno)
{
	make_include(ptr, INCLUDE, lineno) ;
}

	/* Add an exclude filename to the list */

static	void
add_exclude( char *ptr, int lineno)
{
	make_include(ptr, EXCLUDE, lineno) ;
}

	/* Add a bouncecheck filename to the list */

static	void
add_bouncecheck( char *ptr, int lineno )
{
	make_include(ptr, BCHECK, lineno) ;
}



	/* Add a header line to the list */
	/* TODO: defer variable expansion? */

static	void
add_header( register char *ptr, bool replace, int lineno)
{
	HeaderInfo *info ;
	char	*p2 ;
	char	*line ;

	ptr = firstNonBlank(ptr) ;

	if( (line=variable_expand(ptr, False)) == NULL ) {
	  fprintf(logfile, "cannot expand %s\n", ptr) ;
	  return ;
	}

	if( (p2 = strchr(line,':')) == NULL ) {
	  fprintf(logfile, "invalid header line \"%s\"\n", line) ;
	  free(line) ;
	  return ;
	}

	info = MALLOC(HeaderInfo,1) ;
	if( info == NULL ) {
	  fprintf(logfile, "out of memory\n") ;
	  free(line) ;
	  return ;
	}

	append_cmd((Cmd *)info) ;
	info->c.type = replace ? REPLACE : HEADER ;
	info->c.lineno = lineno ;
	info->header = line ;
	info->len = p2 - line + 1 ;
	info->done = 0 ;
	headers = 1 ;
}


	/* advance one character */

void
advance( Ictx *in, int n)
{
	while( --n >= 0 )
	{
	  ++in->ptr ;
	  if( *in->ptr == '\0' )
	    advanceLine(in) ;
	}
}

	/* advance to next token */

void
advanceNext(Ictx *in)
{
	while( *in->ptr == '\0' || isspace(*in->ptr) )
	  if( *in->ptr == '\0' )
	    advanceLine(in) ;
	  else
	    ++in->ptr ;
}


	/* advance to next line */

void
advanceLine(Ictx *in)
{
	if( fgets(in->line, MAXLINE, in->file ) == NULL )
	{
	  in->ptr[0] = EOF ;
	  in->ptr[1] = '\0' ;
	  return ;
	}
#ifdef	COMMENT
	expand_variables(in) ;
#endif	/* COMMENT */
	++in->lineno ;
	in->ptr = in->line ;
	in->ptr = firstNonBlank(in->ptr) ;
	removeNL(in->ptr) ;
}



#ifdef	COMMENT
	/* copy input line, expanding variables according to
	 * traditional quoting rules.  '$$' => '$', thus deferring
	 * variables with $$name.
	 *
	 * Use a primitive state machine.
	 */

static	void
expand_variables(Ictx *in)
{
	char	*tmp ;

	tmp = variable_expand(in->line, False) ;
	strcpy(in->line, tmp) ;
	free(tmp) ;
}
#endif	/* COMMENT */
