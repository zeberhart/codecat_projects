#ifndef __RESOURCE_region_H__
#define __RESOURCE_region_H__

#include <gio/gio.h>

extern GResource *region_get_resource (void);
#endif
