#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Pour l'IHM Syrthes, remplacement de la chaine de carcateres VERSION_SYRTHES par la version de Syrthes
# Le ./version_syrthes/version_syrthes doit contenir la version de Syrthes

import os

def main():

   # lecture du fichier ./version_syrthes/version_syrthes
   # qui doit contenir la version SYRTHES en premiere ligne
   fich_version  = os.path.join('.', 'version_syrthes', 'version_syrthes')
   f = open(fich_version,"r")
   flines = f.read()
   list_lines = flines.split("\n") 
   version_syrthes_value = list_lines[0]
   f.close()
   # recherche dans les repertoires install, install/22x22, install/ficwhatsthis
   # des fichiers html py et licence pour remplacer la chaine de caracteres
   # VERSION_SYRTHES par la version precedemment lue
   dir1 = os.path.join('.', 'install')
   dir2 = os.path.join(dir1, '22x22')
   dir3 = os.path.join(dir1, 'ficwhatsthis')
   for directory in [dir1,dir2,dir3]:
      list_files=os.listdir(directory)
      for filename in list_files:
         # consideration des fichiers .html, .py et license
         if filename.endswith(".html") or filename.endswith(".py") or filename=="license"  :
            filename_l = os.path.join(directory,filename)
            if os.path.isfile(filename_l):
               modif=file(filename_l,"r").read().replace("VERSION_SYRTHES",version_syrthes_value)
               file(filename_l,"w").write(modif)


if __name__ == '__main__':
    main()

