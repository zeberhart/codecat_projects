/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <math.h>

#include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_option.h"
# include "syr_const.h"
# include "syr_bd.h"
# include "syr_parall.h"
# include "syr_proto.h"

#ifdef _SYRTHES_MPI_
# include <mpi.h>
MPI_Status status;
#endif

extern struct Affichages affich;
extern struct Performances perfo;

double epsgcs_rad=1.e-6;
int optim=0;

int ndecoup_max=0;
extern FILE  *fchror;
extern char nomresucr[CHLONG];

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | radiation                                                            |
  |                                                                      |
  |======================================================================| */
void radiation(struct PasDeTemps *pasdetemps,struct GestionFichiers gestionfichiers,
	       struct Maillage maillnodes,struct Maillage maillnodray,
	       struct MaillageCL maillnodeus,
	       double *tmps,double *tmpsa,struct Couple scoupr,struct Couple rcoups,
	       struct FacForme ***listefdf,double *diagfdf,double *tmpray,
	       double **radios,double **firay,double *trayeq,double *erayeq,
	       struct ProphyRay phyray,
	       double **epropr,struct Clim fimpray,
	       struct PropInfini *propinf,struct MatriceRay matrray,
	       struct Soleil *soleil,struct Bilan bilansolaire,
	       struct Lieu lieu,struct Meteo meteo,struct Myfile myfile,
	       struct node *arbre_solaire,double size_min_solaire,double *dim_boite_solaire,
	       struct Compconnexe volconnexe,struct Horizon horiz,struct Vitre vitre,
	       struct Travail trav,
	       struct SDparall sdparall,struct SDparall_ray sdparall_ray)
{
  int i;
  double tcpu1,tcpu2;
  double *trav1;
  char chi[5],chn[20];


  pasdetemps->nbparay++;
  
  /* isa ?????????????????????????? */
/*   trav1=trav.tab[0]; */
/*   for (i=0;i<maillnodes.npoin;i++)  *(trav1+i)=*(tmps[ADR_T]+i) */
/*     +0.5*pasdetemps->rdtts/pasdetemps->rdttsprec*(*(tmps[ADR_T]+i)-*(tmpsa[ADR_T]+i)); */
/*   if (sdparall_ray.npartsray>1) */
/*     transSR_parall(maillnodes.ndim,maillnodeus.node,rcoups,trav1,tmpray,sdparall_ray); */
/*   else */
/*     transSR(maillnodes.ndim,maillnodeus.node,rcoups,trav1,tmpray); */


  /* le transfert se fait avec tous les proc */
  /* --------------------------------------- */
  if (sdparall_ray.nparts>1)
    transSR_parall(maillnodes.ndim,maillnodeus.node,rcoups,tmps,tmpray,sdparall_ray);
  else
    transSR(maillnodes.ndim,maillnodeus.node,rcoups,tmps,tmpray);
  

  /* la resolution ne se fait que sur les noeuds de ray */
  /* -------------------------------------------------- */
  if (sdparall_ray.rangray>-1)
    {
      
      user_ray(maillnodray,tmpray,phyray,propinf,fimpray,vitre,*pasdetemps);
      verif_ray(maillnodray,phyray);
  
  
      /* prise en compte du rayonnement solaire */
      if (soleil->actif) resol_solaire(soleil,&lieu,arbre_solaire,maillnodray,phyray,
				       *pasdetemps,vitre, meteo,myfile,*propinf,
				       size_min_solaire,dim_boite_solaire,sdparall);
      
      
      
      /* resolution du rayonnement */
      tcpu1=cpusyrt();
      resray(pasdetemps->ntsyr,maillnodray,listefdf,diagfdf,tmpray,
	     radios,firay,trayeq,erayeq,phyray,epropr,
	     fimpray,*propinf,
	     matrray,*soleil,bilansolaire,
	     volconnexe,horiz,vitre,trav.tab[0],sdparall_ray);

      /* bilans sur les flux soliares */
      if (soleil->actif && bilansolaire.nb) 
	bilsolaire(maillnodray,*soleil,bilansolaire,phyray.bandespec,propinf->fdfnp1,pasdetemps->tempss);

      tcpu2=cpusyrt();
      perfo.cpu_1iter_ray=tcpu2-tcpu1;
      perfo.cpu_iter_ray+=perfo.cpu_1iter_ray;
      
      
    }

  /* le transfert se fait avec tous les proc */
  /* --------------------------------------- */
  if (sdparall_ray.nparts>1)
    transRS_parall(maillnodeus.ndmat,scoupr,trayeq,erayeq,sdparall_ray);
  else
    transRS(maillnodeus.ndmat,scoupr,trayeq,erayeq);


}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | resray                                                               |
  |                                                                      |
  |======================================================================| */
void resray(int ntsyr,struct Maillage maillnodray,
	    struct FacForme ***listefdf,double *diagfdf,double *tmpray,
	    double **radios,double **firay,double *trayeq,double *erayeq,
	    struct ProphyRay phyray,
	    double **epropr,struct Clim fimpray,
	    struct PropInfini propinf,struct MatriceRay matrray,
	    struct Soleil soleil,struct Bilan bilansolaire,
	    struct Compconnexe volconnexe,struct Horizon horiz,struct Vitre vitre,
	    double *trav0,struct SDparall_ray sdparall_ray)
{

  int numbs,n,ngfac,i,j,niter,nel,nume,iv;
  double v,*r;
  static int lprem=1;
  double c2=1.4388e-2,x1,x2,w1,w2,xx;
 
  for (i=0;i<maillnodray.nelem;i++) *(tmpray+i) += tkel;

  if (lprem)
    {
      lprem=0;
      if (phyray.bandespec.nb==1)
	for (i=0;i<maillnodray.nelem;i++)
	  radios[0][i]=sigma*phyray.emissi[0][i]*pow(tmpray[i],4);
      else
	for (numbs=0;numbs<phyray.bandespec.nb;numbs++)
	  {
	    x1=c2/phyray.bandespec.borneinf[numbs]; x2=c2/phyray.bandespec.bornesup[numbs];
	    for (i=0;i<maillnodray.nelem;i++)
	      {
		v=x1/tmpray[i]; w1=wiebel(v);
		v=x2/tmpray[i]; w2=wiebel(v);
		radios[numbs][i]=sigma*phyray.emissi[numbs][i]*pow(tmpray[i],4)*(w2-w1);
	      }
	  }
    }

  /* second membre */
  /* ------------- */
  for (numbs=0;numbs<phyray.bandespec.nb;numbs++)
    {
      smbray(numbs,maillnodray.nelem,maillnodray.volume,tmpray,propinf,phyray,
	     epropr,radios,horiz,vitre,soleil);
      
      for (i=0;i<fimpray.nelem;i++)
	epropr[numbs][fimpray.numf[i]]=fimpray.val1[numbs][i]*maillnodray.volume[fimpray.numf[i]];
    }

  /* resolution du systeme */
  /* --------------------- */
  if (vitre.actif)
    {
      if (SYRTHES_LANG == FR)
	printf("Traitement des vitres non disponible\n"); 
      else if (SYRTHES_LANG == EN)
	printf("Windows handling not yet completly implemented\n"); 
      syrthes_exit(1);

/*       for (j=0;j<phyray.bandespec.nb;j++) */
/* 	{ */
/* 	  rrayrcglob(maillnodray,listefdf,diagfdf, */
/* 		     phyray.reflec[j],phyray.transm[j], */
/* 		     epropr[j],vitre, */
/* 		     x,b,xm1,gd,res,z,di,resm1,&niter); */
/* 	  if (niter) for (i=0;i<maillnodray.nelem;i++) radios[j][i]=x[i]; */
/* 	} */
    }
  else
    {
      for (j=0;j<phyray.bandespec.nb;j++)
	{
	  if (syrglob_nparts==1 || syrglob_rang==0)
	    if (SYRTHES_LANG == FR) printf(" Bande spectrale %d\n",j+1);
	    else if (SYRTHES_LANG == EN) printf(" Spectral band%d\n",j+1);

	  for (i=0;i<fimpray.nelem;i++) 
	    {phyray.reflec[j][fimpray.numf[i]]=1.;
	      phyray.absorb[j][fimpray.numf[i]]=0.;}

	  if (sdparall_ray.npartsray>1)
	    rrayrc_parall(maillnodray.nelem,listefdf,diagfdf,
			  maillnodray.volume,phyray.reflec[j],epropr[j],
			  matrray,&niter,trav0,sdparall_ray);
	  else
	    rrayrc(maillnodray.nelem,listefdf[0],diagfdf,
		   maillnodray.volume,phyray.reflec[j],epropr[j],
		   matrray,&niter);

	  if (niter) for (i=0;i<maillnodray.nelem;i++) radios[j][i]=matrray.x[i];
	}
    }


  if (affich.ray_resray) 
    { 
      if (SYRTHES_LANG == FR)
	printf("Fin de rrayrc : impression de la radiosite pour la bande 0\n");
      else if (SYRTHES_LANG == EN)
	printf("End of rrayrc : printing for radiosity of band 0\n");
      for (i=0;i<maillnodray.nelem;i++) printf("radios[%d]=%25.18e\n",i,radios[0][i]);
    }
  
  /* 3- preparation des donnees equivalentes */
  /* --------------------------------------- */
  if (sdparall_ray.npartsray>1)
    /* b et di et xm1 sont des vect auxiliaires pour rrayrc : */
    /* on s'en sert ici comme tableaux de travail ! */
    fi2teq_parall(maillnodray.nelem,tmpray,listefdf,diagfdf,
		  maillnodray.volume,phyray,radios,
		  firay,trayeq,erayeq,propinf,fimpray,soleil,horiz,vitre,
		  sdparall_ray,trav0,matrray.di,matrray.xm1);
  else
    /* b et di sont des vect auxiliaires pour rrayrc : on s'en sert ici comme tableaux de travail */
    fi2teq(maillnodray.nelem,tmpray,listefdf[0],diagfdf,
	   maillnodray.volume,phyray,radios,
	   firay,trayeq,erayeq,propinf,fimpray,soleil,horiz,vitre);
 
  
  for (i=0;i<maillnodray.nelem;i++)
    {*(tmpray+i) -= tkel; *(trayeq+i) -= tkel;}
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | smbray                                                               |
  |                                                                      |
  |======================================================================| */
void smbray(int numbs,int nelray,
	    double *sufray,double *tmpray,struct PropInfini propinf,
	    struct ProphyRay phyray,double **epropr,double **radios,
	    struct Horizon horiz,struct Vitre vitre,struct Soleil soleil)
{
  int iv,n,i,ng,ngvois,j,nel,nume;
  double c2,x1,x2,v,w1,w2,xj,*e,*r,x;
  double *fdfnp1;

  fdfnp1=propinf.fdfnp1;

  c2=1.4388e-2;
  if (propinf.actif||horiz.actif||soleil.actif||vitre.actif) 
    {
      e=(double*)malloc(nelray*sizeof(double)); verif_alloue_double1d("smbray",e);
      r=(double*)malloc(nelray*sizeof(double)); verif_alloue_double1d("smbray",r);
      for (i=0;i<nelray;i++) {e[i]=0;r[i]=0;}
    }


  /* 1- cas d'une seule bande spectrale*/
  if (phyray.bandespec.nb==1)
    {
      for (i=0;i<nelray;i++)
	epropr[0][i]=sigma*phyray.emissi[0][i]*pow(tmpray[i],4)*sufray[i];


      if (propinf.actif)
	{
	  xj=sigma * pow(propinf.temp+tkel,4);
	  for (i=0;i<nelray;i++) e[i]+=fdfnp1[i]*xj;
	}
      if (horiz.actif)
	{
	  xj=sigma * horiz.emissi*pow(horiz.temp+tkel,4);
	  for (i=0;i<nelray;i++) e[i]+=horiz.fdf[i]*xj;
	}

      if (soleil.actif) 
	for (i=0;i<nelray;i++) e[i]+= fdfnp1[i]*soleil.diffus[0][i];
    }

  /* 2- cas de bandes spectrales multiples*/
  else
    {
      x1=c2/phyray.bandespec.borneinf[numbs]; x2=c2/phyray.bandespec.bornesup[numbs];
      for (i=0;i<nelray;i++)
	{
          v=x1/tmpray[i]; w1=wiebel(v);
          v=x2/tmpray[i]; w2=wiebel(v);
	  epropr[numbs][i]=sigma*phyray.emissi[numbs][i]*pow(tmpray[i],4)*(w2-w1)*sufray[i];
	}
      if (propinf.actif)
	{
	  v=x1/(propinf.temp+tkel);  w1=wiebel(v);
	  v=x2/(propinf.temp+tkel);  w2=wiebel(v);
	  xj=sigma*pow(propinf.temp+tkel,4)*(w2-w1);
	  for (i=0;i<nelray;i++) e[i]+=fdfnp1[i]*xj;  
	}
      if (horiz.actif)
	{
	  v=x1/(horiz.temp+tkel);  w1=wiebel(v);
	  v=x2/(horiz.temp+tkel);  w2=wiebel(v);
	  xj=sigma*horiz.emissi*pow(horiz.temp+tkel,4)*(w2-w1);
	  for (i=0;i<nelray;i++)  e[i]+=horiz.fdf[i]*xj;  
	}

      if (soleil.actif) 
	for (i=0;i<nelray;i++) e[i]+= fdfnp1[i]*soleil.diffus[numbs][i];
    }


  if (propinf.actif||horiz.actif||soleil.actif)
    for (i=0;i<nelray;i++)
      epropr[numbs][i]+=phyray.reflec[numbs][i]*e[i];
  
  if (soleil.actif)
    for (i=0;i<nelray;i++) epropr[numbs][i]+=soleil.reflec[numbs][i]*soleil.direct[numbs][i]*sufray[i];

  
  if (vitre.actif)
    {
      for (i=0;i<nelray;i++)
	if ((iv=vitre.voisin[i])>-1) 
	  epropr[numbs][i]+=phyray.transm[numbs][i]*e[iv];
    }
  
  /*   printf("SMBRAY second membre epropr\n");
       for (i=0;i<nelray;i++) printf(" i=%d epropr/S=%f\n",i,epropr[0][i]/sufray[i]); */

  if (propinf.actif||horiz.actif||soleil.actif||vitre.actif) {free(e); free(r);}

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | wiebel                                                               |
  |                                                                      |
  |======================================================================| */
double wiebel(double v)
{
  int m;
  double w,v2,v4;
  const double pi4=.153989717364e+00; 
  const double z0=.333333343267e+00; 
  const double z1=.125000000000e+00; 
  const double z2=.166666675359e-01; 
  const double z4=.198412701138e-03; 
  const double z6=.367430925508e-05; 
  const double z8=.751563220547e-07; 
  
  if (v>=2.)
    {
      w=0; 
      for (m=1;m<6;m++) w+=exp(-m*v)/(m*m*m*m) * (((m*v+3)*m*v+6)*m*v+6);
      w=w * pi4;
    }
  else
    {
      v2=v*v;
      v4=v2*v2;
      w=z0 - z1*v + z2*v2 - z4*v2*v2 + z6*v4*v2 - z8*v4*v4;
      w=1. - pi4*v2*v*w;
    }
  return w;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | gausei                                                               |
  |                                                                      |
  |======================================================================| */
void rrayrc(int nelray,struct FacForme **listefdf,double *diagfdf,
	    double *sufray,
	    double *rho,double *epr,
	    struct MatriceRay matrray,int *niter)
{
  int n,ngfac;
  double aa,bb,cc,d,ee,alfa,aalfa,tau,alfam1,alftau;
  double aux1,aux2,aux,denom;
  int i,j,nitsmo;
  double x0,resnor,prsca1;
  double epsis,zero,epssmo;
  double *xx;
  struct FacForme *pff;
  double *x,*b,*xm1,*gd,*res,*z,*di,*resm1;
 

  x     = matrray.x;
  b     = matrray.b;
  xm1   = matrray.xm1;
  gd    = matrray.gd;
  res   = matrray.res;
  z     = matrray.z;
  di    = matrray.di;
  resm1 = matrray.resm1;
  
  zero=0.;
  nitsmo=100;
  epssmo=1.e-12;
  n=0;

  /* 1- initialisation des vecteurs auxiliaires */
  for (i=0;i<nelray;i++)
    {
      xm1[i]=x[i]=epr[i];
      b[i]=epr[i];
      di[i]=sufray[i]-rho[i]*diagfdf[i];
    }

  /* norme du second membre */
  for (i=0,prsca1=0,xx=x;i<nelray;i++,xx++) prsca1+= *xx * *xx;
  x0=sqrt(prsca1);

  if (x0< 1.e-20 ) x0=1.e-6;
  epsis=1.e-4 * x0;

  for (i=0;i<nelray;i++)
    {
      res[i]=0.;
      pff=listefdf[i];
      while(pff) {res[i]-=pff->fdf*rho[i]*x[pff->numenface]; pff=pff->suivant;}
      res[i] += di[i]*x[i]-b[i];
    }


  for (i=0,prsca1=0,xx=res;i<nelray;i++,xx++) prsca1+= *xx * *xx;
  resnor=sqrt(prsca1);
/*   printf(" >>>> ray=resnor_ini seq : %25.18e \n",resnor); */

  if (SYRTHES_LANG == FR)
    printf(" *** RRAYRC : ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
  else if (SYRTHES_LANG == EN)
    printf(" *** RRAYRC : ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");


  if ( resnor<=epsis && resnor<=epsgcs_rad*sqrt((double)(nelray)))
    {
      printf("                 %4d         %12.5e                %12.5e\n",
	     n,resnor/x0,resnor/sqrt((double)(nelray)));
      *niter=n;
    }

  else
    {
      for (i=0;i<nelray;i++) resm1[i]=res[i];
      n=0;

      do
	{
	  n++;
	  for (i=0;i<nelray;i++) gd[i]=res[i]/di[i];
	  for (i=0;i<nelray;i++)
	    {
	      z[i]=0.;
	      pff=listefdf[i];
	      while(pff) {z[i]-=pff->fdf*rho[i]*gd[pff->numenface]; pff=pff->suivant;}
	      z[i] += di[i]*gd[i];
	    }
	  aa=0.;bb=0.;cc=0.;d =0.;ee=0.;
	  for (i=0;i<nelray;i++)
	    {
	      aa += z[i]*res[i];
	      bb += z[i]*resm1[i];
	      aux=res[i]-resm1[i];
	      cc += res[i]*aux;
	      d  += resm1[i]*aux;
	      ee +=z[i]*z[i];
	    }

	  denom=(cc-d )*ee-(aa-bb)*(aa-bb);
	  if(fabs(denom)< 1.e-20) alfa=1.;
	  else alfa=((aa-bb)*bb-d *ee)/denom;
	  aalfa=fabs(alfa);
	  if (aalfa<1e-20 || fabs(aalfa-1.)< 1.e-20) {alfa=1.; tau=aa/ee;}
	  else tau=aa/ee + (1.-alfa)/alfa * bb/ee;
	  alfam1 =1.-alfa;
	  alftau =-alfa*tau;
	  for (i=0;i<nelray;i++)
	    {
	      aux1    =res[i];
	      aux2    =x[i];
	      res[i]  =alfa*aux1+alfam1*resm1[i]+alftau*z[i];
	      resm1[i]=aux1;
	      x[i]    =alfa*aux2+alfam1*xm1[i]+alftau*gd[i];
	      xm1[i]  =aux2;
	    }

	  for (i=0,prsca1=0,xx=res;i<nelray;i++,xx++) prsca1+= *xx * *xx;
	  resnor=sqrt(prsca1);
/* 	  printf("rrayrc_seq : it=%d   resnor=%25.18e \n",n,resnor); */

	  if (n%10==0)
	    printf("                 %4d         %12.5e        %12.5e\n",
		   n,resnor/x0,resnor/sqrt((double)(nelray)));

	}
      while(!( (resnor<=epsis &&  resnor<=epsgcs_rad*sqrt((double)(nelray))) || n>=nitsmo));

      if (n%10!=0)
	printf("                 %4d         %12.5e        %12.5e\n",
	       n,resnor/x0,resnor/sqrt((double)(nelray)));
      *niter=n;
    }
/*   for (i=0;i<nelray;i++)  */
/*     printf("fin de rrayrc proc=%d temray[%d]=%f\n",syrglob_rang,i,matrray.x[i]); */
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void rrayrcglob(struct Maillage maillnodray,
		struct FacForme **listefdf,double *diagfdf,
		double *rho,double *tr,double *epr,struct Vitre vitre,
		struct MatriceRay matrray,int *niter)
{
  int n,ni,ngfac,ng,ngj,nelem,nv,nvloc,nvvol;
  double aa,bb,cc,d,ee,alfa,aalfa,tau,alfam1,alftau,rr;
  double aux1,aux2,aux,denom;
  int i,j,nitsmo;
  double x0,resnor,prsca1;
  double epsis,zero,epssmo;
  double *xx;
  struct FacForme *pff;
  double *x,*b,*xm1,*gd,*res,*z,*di,*resm1;
 

  x     = matrray.x;
  b     = matrray.b;
  xm1   = matrray.xm1;
  gd    = matrray.gd;
  res   = matrray.res;
  z     = matrray.z;
  di    = matrray.di;
  resm1 = matrray.resm1;

  zero=0.;
  nitsmo=100;
  epssmo=1.e-12;
  n=0;

  double **fdf; /* pour que ca compile !!!!!!! */

  /* 1- initialisation des vecteurs auxiliaires */
  for (i=0;i<maillnodray.nelem;i++)
    {
      xm1[i]=x[i]=epr[i]/maillnodray.volume[i];
      b[i]=epr[i];
      di[i]=maillnodray.volume[i]-rho[i]*diagfdf[i];
    }


  /* norme du second membre */
  for (i=0,prsca1=0,xx=x;i<maillnodray.nelem;i++,xx++) prsca1+= *xx * *xx;
  x0=sqrt(prsca1);

  if (x0< 1.e-20 ) x0=1.e-6;
  epsis=1.e-4 * x0;

  for (i=0;i<maillnodray.nelem;i++)
    {
      res[i]=0.;  
      pff=listefdf[i];
      while(pff) 
	{
	  res[i]-=pff->fdf * rho[i]*x[pff->numenface];  
	  pff=pff->suivant;
	}
      res[i] += di[i]*x[i]-b[i];
    }

/*   for (i=0;i<maillnodray.nelem;i++) */
/*     if (vitre.voisin[i]>-1) */
/*       { */
/* 	nv=vitre.voisin[i]; */
/* 	nvvol=vitre.voisvol[i]; */
/* 	nvloc=vitre.voisloc[i]; */
/* 	nelem=volconnexe.nelem[nvvol]; */
/* 	rr=0; */
/* 	for (j=0;j<nvloc;j++) */
/* 	  rr+=fdf[nvvol][j*nelem-j*(j+1)/2+nvloc]*x[volconnexe.elem[nvvol][j]]; */
/* 	for (j=nvloc+1;j<nelem;j++) */
/* 	  rr+=fdf[nvvol][nvloc*nelem-(nvloc+1)*nvloc/2+j]*x[volconnexe.elem[nvvol][j]]; */
/* 	res[i]-=tr[i]*rr; */
/*       } */




  for (i=0,prsca1=0,xx=res;i<maillnodray.nelem;i++,xx++) prsca1+= *xx * *xx;
  resnor=sqrt(prsca1);

  if ( resnor<=epsis && resnor<=epsgcs_rad*sqrt((double)(maillnodray.nelem)))
    {
      if (SYRTHES_LANG == FR)
	printf(" *** RRAYRC G : ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
      else if (SYRTHES_LANG == EN)
	printf(" *** RRAYRC G : ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");
      printf("                 %4d         %12.5e                %12.5e\n",
	     n,resnor/x0,resnor/sqrt((double)(maillnodray.nelem)));
      *niter=n;
    }

  else
    {
      if (SYRTHES_LANG == FR)
	printf(" *** RRAYRC G : ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
      else if (SYRTHES_LANG == EN)
	printf(" *** RRAYRC G : ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");
      
      for (i=0;i<maillnodray.nelem;i++) resm1[i]=res[i];
      ni=0;

/*       do */
/* 	{ */
/* 	  ni++; */
/* 	  for (i=0;i<nelray;i++) gd[i]=res[i]/di[i]; */

/* 	  for (n=0;n<volconnexe.nbcomp;n++) */
/* 	    for (i=0;i<volconnexe.nelem[n];i++) */
/* 	      { */
/* 		nelem=volconnexe.nelem[n]; */
/* 		ng=volconnexe.elem[n][i]; */
/* 		z[ng]=0.; */
/* 	      for (j=0;j<i;j++) */
/* 		{ */
/* 		  ngj=volconnexe.elem[n][j]; */
/* 		  z[ng] -= fdf[n][j*nelem-(j+1)*j/2+i]*rho[ng]*gd[ngj]; */
/* 		} */
/* 	      for (j=i+1;j<nelem;j++) */
/* 		{ */
/* 		  ngj=volconnexe.elem[n][j]; */
/* 		  z[ng] -= fdf[n][i*nelem-(i+1)*i/2+j]*rho[ng]*gd[ngj]; */
/* 		} */
/* 	      z[ng] += di[ng]*gd[ng]; */
/* 	      } */

/* 	  for (i=0;i<nelray;i++) */
/* 	    if (vitre.voisin[i]>-1) */
/* 	      { */
/* 		nv=vitre.voisin[i]; */
/* 		nvvol=vitre.voisvol[i]; */
/* 		nvloc=vitre.voisloc[i]; */
/* 		nelem=volconnexe.nelem[nvvol]; */
/* 		rr=0; */
/* 		for (j=0;j<nvloc;j++) */
/* 		  rr+=fdf[nvvol][j*nelem-j*(j+1)/2+nvloc]*gd[volconnexe.elem[nvvol][j]]; */
/* 		for (j=nvloc+1;j<nelem;j++) */
/* 		  rr+=fdf[nvvol][nvloc*nelem-(nvloc+1)*nvloc/2+j]*gd[volconnexe.elem[nvvol][j]]; */
/* 		z[i]-=tr[i]*rr; */
/* 	      } */

/* 	  aa=0.;bb=0.;cc=0.;d =0.;ee=0.; */
/* 	  for (i=0;i<nelray;i++) */
/* 	    { */
/* 	      aa += z[i]*res[i]; */
/* 	      bb += z[i]*resm1[i]; */
/* 	      aux=res[i]-resm1[i]; */
/* 	      cc += res[i]*aux; */
/* 	      d  += resm1[i]*aux; */
/* 	      ee +=z[i]*z[i]; */
/* 	    } */
/* 	  denom=(cc-d )*ee-(aa-bb)*(aa-bb); */
/* 	  if(fabs(denom)< 1.e-20) alfa=1.; */
/* 	  else alfa=((aa-bb)*bb-d *ee)/denom; */
/* 	  aalfa=fabs(alfa); */
/* 	  if (aalfa<1e-20 || fabs(aalfa-1.)< 1.e-20) {alfa=1.; tau=aa/ee;} */
/* 	  else tau=aa/ee + (1.-alfa)/alfa * bb/ee; */
/* 	  alfam1 =1.-alfa; */
/* 	  alftau =-alfa*tau; */
/* 	  for (i=0;i<nelray;i++) */
/* 	    { */
/* 	      aux1    =res[i]; */
/* 	      aux2    =x[i]; */
/* 	      res[i]  =alfa*aux1+alfam1*resm1[i]+alftau*z[i]; */
/* 	      resm1[i]=aux1; */
/* 	      x[i]    =alfa*aux2+alfam1*xm1[i]+alftau*gd[i]; */
/* 	      xm1[i]  =aux2; */
/* 	    } */

/* 	  for (i=0,prsca1=0,xx=res;i<maillnodray.nelem;i++,xx++) prsca1+= *xx * *xx; */
/* 	  resnor=sqrt(prsca1); */

/* 	  printf("                 %4d         %12.5e        %12.5e\n", */
/* 		 ni,resnor/x0,resnor/sqrt((double)(maillnodray.nelem))); */

/* 	} */
/*       while(!( (resnor<=epsis &&  resnor<=epsgcs_rad*sqrt((double)(maillnodray.nelem))) || ni>=nitsmo)); */
      
      if (n%10==0) printf("                 %4d         %12.5e        %12.5e\n",
			  ni,resnor/x0,resnor/sqrt((double)(maillnodray.nelem)));
      *niter=ni;
    }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | fi2teq                                                               |
  |                                                                      |
  |======================================================================| */
void fi2teq(int nelray,double *tmpray,
	    struct FacForme **listefdf,double *diagfdf,
	    double *sufray,
	    struct ProphyRay phyray,
	    double **radios,double **firay,
	    double *trayeq,double *erayeq,
	    struct PropInfini propinf,struct Clim fimpray,
	    struct Soleil soleil,
	    struct Horizon horiz,
	    struct Vitre vitre)
{
  int nb,n,m,i,j,ngfac,nel,nume,iv,jv,numbs;
  double eps;
  double w1,w2,tfac,xj1,xj[100],xh1,xh[100];
  double ee,c2,x1,x2,x,v,v2,v4,rr,**e,**ev,xx,f;
  struct FacForme *pff;
  double *fdfnp1;

  fdfnp1=propinf.fdfnp1;

  if (phyray.bandespec.nb>100)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("fi2teq : le nombre de bandes spectrales ne peut etre superieur a 100\n");
	  printf("         dans le cas improbable ou cela pose pb contacter CP/IR \n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("fi2teq : the number of spectral band cannot be above 100\n");
	  printf("         in the very unlikely case it induces a pb contact  CP/IR \n");
	}
      syrthes_exit(1);
    }

  eps=1e-6;
  c2=1.4388e-2;
  for (i=0;i<nelray;i++) trayeq[i]=erayeq[i]=0.;

  e=(double**)malloc(phyray.bandespec.nb*sizeof(double*));
  for (n=0;n<phyray.bandespec.nb;n++) e[n]=(double*)malloc(nelray*sizeof(double));
  verif_alloue_double2d(phyray.bandespec.nb,"fi2teq",e);
  for (i=0;i<nelray;i++) for (n=0;n<phyray.bandespec.nb;n++) {e[n][i]=0;}


  if (phyray.bandespec.nb==1)
    {
      if (propinf.actif) xj[0]=sigma*pow(propinf.temp+tkel,4);
      if (horiz.actif)   xh[0]=sigma*horiz.emissi*pow(horiz.temp+tkel,4);
      for (i=0;i<nelray;i++) erayeq[i]=phyray.emissi[0][i];
    }
  else
    for (n=0;n<phyray.bandespec.nb;n++) 
      {
	x1=c2/phyray.bandespec.borneinf[n]; x2=c2/phyray.bandespec.bornesup[n];
	if (propinf.actif)
	  {
	    v=x1/(propinf.temp+tkel);  w1=wiebel(v);
	    v=x2/(propinf.temp+tkel);  w2=wiebel(v);
	    xj[n]= sigma * pow((propinf.temp+tkel),4) * (w2-w1);
	  }
	if (horiz.actif)
	  {
	    v=x1/(horiz.temp+tkel);  w1=wiebel(v);
	    v=x2/(horiz.temp+tkel);  w2=wiebel(v);
	    xh[n]= sigma*horiz.emissi*pow((horiz.temp+tkel),4) * (w2-w1);
	  }
	
	for (i=0;i<nelray;i++)
	  {
	    v=x1/tmpray[i]; w1=wiebel(v);
	    v=x2/tmpray[i]; w2=wiebel(v);
	    erayeq[i] += (w2-w1)*phyray.emissi[n][i];
	  }
      }   
  
  /* ----------------------------------------- */
  /*
  if (propinf.actif)
    for (nb=0;nb<bandespec.nb;nb++) 
      for (n=0;n<volconnexe.nbcomp;n++)
	for (i=0;i<volconnexe.nelem[n];i++)
	  e[nb][volconnexe.elem[n][i]]+=fdfnp1[n][i]*xj[nb];  
  
  
  if (horiz.actif)
    for (nb=0;nb<bandespec.nb;nb++) 
      for (n=0;n<volconnexe.nbcomp;n++)
	for (i=0;i<volconnexe.nelem[n];i++)
	  e[nb][volconnexe.elem[n][i]]+=horiz.fdf[n][i]*xh[nb];  
  
  if (soleil.actif) 
    {
      for (nb=0;nb<bandespec.nb;nb++) 
	for (n=0;n<volconnexe.nbcomp;n++)
	  for (i=0;i<volconnexe.nelem[n];i++)
	    e[nb][volconnexe.elem[n][i]]+= fdfnp1[n][i]*soleil.diffus[nb][volconnexe.elem[n][i]];

      for (nb=0;nb<bandespec.nb;nb++) 
	for (i=0;i<nelray;i++) 
	  e[nb][i]+=soleil.direct[nb][i]*sufray[i]; 
    }



  for (numbs=0;numbs<bandespec.nb;numbs++) 
    for (n=0;n<volconnexe.nbcomp;n++)
      for (i=0;i<volconnexe.nelem[n];i++)
	{
	  nel=volconnexe.nelem[n];  nume=volconnexe.elem[n][i];
	  x=0.;
	  for (j=0;j<i+1;j++)
	    x+= fdf[n][j*nel-j*(j+1)/2+i]*radios[numbs][volconnexe.elem[n][j]];
	  for (j=i+1;j<nel;j++)
	    x+= fdf[n][i*nel-(i+1)*i/2+j]*radios[numbs][volconnexe.elem[n][j]];
	  e[numbs][volconnexe.elem[n][i]]+=x;
	} 


  for (i=0;i<nelray;i++)
    {
      for (ee=0.,n=0;n<bandespec.nb;n++) ee+=e[n][i];
      trayeq[i]=pow(ee/(sufray[i]*sigma),0.25);
    }
    */



  /* ----------------------------------------- */
  if (phyray.bandespec.nb==1)
    {
      for (i=0;i<nelray;i++)
	{
	  rr=0;
	  pff=listefdf[i];
	  while(pff) {
	    rr+=pff->fdf*radios[0][pff->numenface]; 
	    pff=pff->suivant;
	  }
	  rr+=diagfdf[i]*radios[0][i];
	  if (propinf.actif) rr += fdfnp1[i]*xj[0];
	  if (horiz.actif)   rr += horiz.fdf[i]*xh[0];
	  if (soleil.actif)  rr += fdfnp1[i]*soleil.diffus[0][i]
	    +soleil.direct[0][i]*sufray[i];
	  trayeq[i]=pow(rr/(sufray[i]*sigma),0.25);
	}
    }
  else
    {
      for (i=0;i<nelray;i++)
	{
	  rr=0.;
	  for (nb=0;nb<phyray.bandespec.nb;nb++) 
	    {
	      xx=0;
	      pff=listefdf[i];
	      while(pff) {xx+=pff->fdf*radios[nb][pff->numenface]; pff=pff->suivant;}
	      xx+=diagfdf[i]*radios[nb][i];
	      if (propinf.actif) xx += fdfnp1[i]*xj[nb];
	      if (horiz.actif)   xx += horiz.fdf[i]*xh[nb];
	      if (soleil.actif)  xx += fdfnp1[i]*soleil.diffus[nb][i]
		+soleil.direct[nb][i]*sufray[i];
	      rr+=xx*phyray.emissi[nb][i];
	    }
	  trayeq[i]=pow(rr/(sufray[i]*erayeq[i]*sigma),0.25);
	}
    }
    


  /* 4.- calcul du flux de rayonnemnent (pour le post-processing) */
  for (nb=0;nb<phyray.bandespec.nb;nb++) 
    {
      for (i=0;i<nelray;i++)
	{
	  rr=0;
	  pff=listefdf[i];
	  while(pff) {rr+=pff->fdf*radios[nb][pff->numenface]; pff=pff->suivant;}
	  rr+=diagfdf[i]*radios[nb][i];
	  if (propinf.actif) rr += fdfnp1[i]*xj[nb];
	  if (horiz.actif)   rr += horiz.fdf[i]*xj[nb];
	  if (soleil.actif)  rr += fdfnp1[i]*soleil.diffus[nb][i]+soleil.direct[nb][i]*sufray[i];
	  if (phyray.bandespec.nb==1)
	    firay[nb][i]= phyray.emissi[nb][i]*(sigma*pow(tmpray[i],4)-rr/sufray[i]);
	  else
	    {
	      v=x1/tmpray[i]; w1=wiebel(v);
	      v=x2/tmpray[i]; w2=wiebel(v);
	      firay[nb][i]= phyray.emissi[nb][i]*(sigma*(w2-w1)*pow(tmpray[i],4)-rr/sufray[i]);
	    }
	}
    }
  
  /*
    if (vitre.actif)
    if (bandespec.nb==1) 
    firay[0][i]= emissi[0][i]*sigma*pow(tmpray[i],4) - absorb[0][i]*ev[0][i];
    else
    for (nb=0;nb<bandespec.nb;nb++) 
    for (i=0;i<nelray;i++)
    { 
    v=x1/tmpray[ig]; w1=wiebel(v);
    v=x2/tmpray[ig]; w2=wiebel(v);
    firay[nb][i]= emissi[nb][i]*sigma*pow(tmpray[i],4) - absorb[n][i]*ev[n][i];
    }
    else
    if (bandespec.nb==1)
    firay[0][i]= emissi[0][i]*sigma*pow(tmpray[i],4) - absorb[0][i]*e[0][i];
    else
    for (nb=0;nb<bandespec.nb;nb++) 
    for (i=0;i<nelray;i++)
    { 
    v=x1/tmpray[ig]; w1=wiebel(v);
    v=x2/tmpray[ig]; w2=wiebel(v);
    firay[nb][i]= emissi[nb][i]*sigma*pow(tmpray[i],4) - absorb[nb][i]*e[nb][i];
    }
    */



  /* 5-mise a jour des facettes avec flux imposee */
  for (n=0;n<phyray.bandespec.nb;n++) 
    for (i=0;i<fimpray.nelem;i++)
      {
	ngfac=fimpray.numf[i];
	firay[n][ngfac]=fimpray.val1[n][i];
      }
  
  /* 6-calcul de la temperature de la facette necessaire */
  for (i=0;i<fimpray.nelem;i++)
    {
      tfac=0.;
      ngfac=fimpray.numf[i];
      for (n=0;n<phyray.bandespec.nb;n++) 
	tfac += (1.-phyray.emissi[n][ngfac])/phyray.emissi[n][ngfac]*fimpray.val1[n][i] 
	  + radios[n][i];
      tmpray[ngfac]=pow(tfac/sigma,0.25);
    }
  
  
  if (affich.ray_fi2teq)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** fi2teq : calcul des emissivites equivalentes ");
	  printf(" des temperatures de rayonnement equivalentes\n");
	  printf("             facette    emissivite equi   temp equi (degres c)\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** fi2teq : calculation of equivalent emissivity ");
	  printf(" and of equivalent radiation temperature\n");
	  printf("             face    emissivity equi   temp equi (degres c)\n");
	}
      for (i=0;i<nelray;i++) 
	printf("              %6d      %25.18e            %25.18e\n",
	       i,erayeq[i],trayeq[i]-tkel);
    }

  /* penser a faire un free pour e et ev ?????????????????????? */

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | fi2teq_parall  - version parallele                                   |
  |                                                                      |
  |======================================================================| */
void fi2teq_parall(int nelray,double *tmpray,
		   struct FacForme ***listefdf,double *diagfdf,
		   double *sufray,
		   struct ProphyRay phyray,
		   double **radios,double **firay,
		   double *trayeq,double *erayeq,
		   struct PropInfini propinf,struct Clim fimpray,
		   struct Soleil soleil,
		   struct Horizon horiz,
		   struct Vitre vitre,
		   struct SDparall_ray sdparall_ray,
		   double *trav0,double *trav1,double *trav2)
{
  int nb,n,m,i,j,ngfac,nel,nume,iv,jv,numbs;
  double eps;
  double w1,w2,tfac,xj1,xj[100],xh1,xh[100];
  double ee,c2,x1,x2,x,v,v2,v4,**e,**ev,f;
  double *fdfnp1;

  fdfnp1=propinf.fdfnp1;

  if (phyray.bandespec.nb>100)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("fi2teq : le nombre de bandes spectrales ne peut etre superieur a 100\n");
	  printf("         dans le cas improbable ou cela pose pb contacter CP/IR \n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("fi2teq : the number of spectral band cannot be above 100\n");
	  printf("         in the very unlikely case it induces a pb contact  CP/IR \n");
	}
      syrthes_exit(1);
    }

  eps=1e-6;
  c2=1.4388e-2;
  for (i=0;i<nelray;i++) trayeq[i]=erayeq[i]=0.;

  e=(double**)malloc(phyray.bandespec.nb*sizeof(double*));
  for (n=0;n<phyray.bandespec.nb;n++) e[n]=(double*)malloc(nelray*sizeof(double));
  verif_alloue_double2d(phyray.bandespec.nb,"fi2teq",e);
  for (i=0;i<nelray;i++) for (n=0;n<phyray.bandespec.nb;n++) {e[n][i]=0;}


  if (phyray.bandespec.nb==1)
    {
      if (propinf.actif) xj[0]=sigma*pow(propinf.temp+tkel,4);
      if (horiz.actif)   xh[0]=sigma*horiz.emissi*pow(horiz.temp+tkel,4);
      for (i=0;i<nelray;i++) erayeq[i]=phyray.emissi[0][i];
    }
  else
    for (n=0;n<phyray.bandespec.nb;n++) 
      {
	x1=c2/phyray.bandespec.borneinf[n]; x2=c2/phyray.bandespec.bornesup[n];
	if (propinf.actif)
	  {
	    v=x1/(propinf.temp+tkel);  w1=wiebel(v);
	    v=x2/(propinf.temp+tkel);  w2=wiebel(v);
	    xj[n]= sigma * pow((propinf.temp+tkel),4) * (w2-w1);
	  }
	if (horiz.actif)
	  {
	    v=x1/(horiz.temp+tkel);  w1=wiebel(v);
	    v=x2/(horiz.temp+tkel);  w2=wiebel(v);
	    xh[n]= sigma*horiz.emissi*pow((horiz.temp+tkel),4) * (w2-w1);
	  }
	
	for (i=0;i<nelray;i++)
	  {
	    v=x1/tmpray[i]; w1=wiebel(v);
	    v=x2/tmpray[i]; w2=wiebel(v);
	    erayeq[i] += (w2-w1)*phyray.emissi[n][i];
	  }
      }   
  
  /* ----------------------------------------- */
  /*
  if (propinf.actif)
    for (nb=0;nb<bandespec.nb;nb++) 
      for (n=0;n<volconnexe.nbcomp;n++)
	for (i=0;i<volconnexe.nelem[n];i++)
	  e[nb][volconnexe.elem[n][i]]+=fdfnp1[n][i]*xj[nb];  
  
  
  if (horiz.actif)
    for (nb=0;nb<bandespec.nb;nb++) 
      for (n=0;n<volconnexe.nbcomp;n++)
	for (i=0;i<volconnexe.nelem[n];i++)
	  e[nb][volconnexe.elem[n][i]]+=horiz.fdf[n][i]*xh[nb];  
  
  if (soleil.actif) 
    {
      for (nb=0;nb<bandespec.nb;nb++) 
	for (n=0;n<volconnexe.nbcomp;n++)
	  for (i=0;i<volconnexe.nelem[n];i++)
	    e[nb][volconnexe.elem[n][i]]+= fdfnp1[n][i]*soleil.diffus[nb][volconnexe.elem[n][i]];

      for (nb=0;nb<bandespec.nb;nb++) 
	for (i=0;i<nelray;i++) 
	  e[nb][i]+=soleil.direct[nb][i]*sufray[i]; 
    }



  for (numbs=0;numbs<bandespec.nb;numbs++) 
    for (n=0;n<volconnexe.nbcomp;n++)
      for (i=0;i<volconnexe.nelem[n];i++)
	{
	  nel=volconnexe.nelem[n];  nume=volconnexe.elem[n][i];
	  x=0.;
	  for (j=0;j<i+1;j++)
	    x+= fdf[n][j*nel-j*(j+1)/2+i]*radios[numbs][volconnexe.elem[n][j]];
	  for (j=i+1;j<nel;j++)
	    x+= fdf[n][i*nel-(i+1)*i/2+j]*radios[numbs][volconnexe.elem[n][j]];
	  e[numbs][volconnexe.elem[n][i]]+=x;
	} 


  for (i=0;i<nelray;i++)
    {
      for (ee=0.,n=0;n<bandespec.nb;n++) ee+=e[n][i];
      trayeq[i]=pow(ee/(sufray[i]*sigma),0.25);
    }
    */



  /* ----------------------------------------- */
  if (phyray.bandespec.nb==1)
    {

      fdf_Y_parall(trayeq,radios[0],listefdf,diagfdf,trav0,sdparall_ray);

      for (i=0;i<nelray;i++)
	{
	  trayeq[i] += diagfdf[i]*radios[0][i];
	  if (propinf.actif) trayeq[i] += fdfnp1[i]*xj[0];
	  if (horiz.actif)   trayeq[i] += horiz.fdf[i]*xh[0];
	  if (soleil.actif)  trayeq[i] += fdfnp1[i]*soleil.diffus[0][i]
	                                  +soleil.direct[0][i]*sufray[i];
	  trayeq[i]=pow(trayeq[i]/(sufray[i]*sigma),0.25);
	}
    }
  else
    {
      for (i=0;i<nelray;i++) trav2[i]=0;

      for (nb=0;nb<phyray.bandespec.nb;nb++) 
	{
	  fdf_Y_parall(trav1,radios[nb],listefdf,diagfdf,trav0,sdparall_ray);

	  for (i=0;i<nelray;i++)
	    {
	      trayeq[i] += diagfdf[i]*radios[nb][i];
	      if (propinf.actif) trav1[i] += fdfnp1[i]*xj[nb];
	      if (horiz.actif)   trav1[i] += horiz.fdf[i]*xh[nb];
	      if (soleil.actif)  trav1[i] += fdfnp1[i]*soleil.diffus[nb][i]
		+soleil.direct[nb][i]*sufray[i];
 	      trav2[i]+=trav1[i]*phyray.emissi[nb][i];
	    }
	}

      for (i=0;i<nelray;i++) 
	trayeq[i]=pow(trav2[i]/(sufray[i]*erayeq[i]*sigma),0.25); 

    }
    


  /* 4.- calcul du flux de rayonnemnent (pour le post-processing) */
  for (nb=0;nb<phyray.bandespec.nb;nb++) 
    {
      fdf_Y_parall(trav1,radios[nb],listefdf,diagfdf,trav0,sdparall_ray);

      for (i=0;i<nelray;i++)
	{
	  if (propinf.actif) trav1[i] += fdfnp1[i]*xj[nb];
	  if (horiz.actif)   trav1[i] += horiz.fdf[i]*xj[nb];
	  if (soleil.actif)  trav1[i] += fdfnp1[i]*soleil.diffus[nb][i]+soleil.direct[nb][i]*sufray[i];
	  if (phyray.bandespec.nb==1)
	    firay[nb][i]= phyray.emissi[nb][i]*(sigma*pow(tmpray[i],4)-trav1[i]/sufray[i]);
	  else
	    {
	      v=x1/tmpray[i]; w1=wiebel(v);
	      v=x2/tmpray[i]; w2=wiebel(v);
	      firay[nb][i]= phyray.emissi[nb][i]*(sigma*(w2-w1)*pow(tmpray[i],4)-trav1[i]/sufray[i]);
	    }
	}
    }
  
  /*
    if (vitre.actif)
    if (bandespec.nb==1) 
    firay[0][i]= emissi[0][i]*sigma*pow(tmpray[i],4) - absorb[0][i]*ev[0][i];
    else
    for (nb=0;nb<bandespec.nb;nb++) 
    for (i=0;i<nelray;i++)
    { 
    v=x1/tmpray[ig]; w1=wiebel(v);
    v=x2/tmpray[ig]; w2=wiebel(v);
    firay[nb][i]= emissi[nb][i]*sigma*pow(tmpray[i],4) - absorb[n][i]*ev[n][i];
    }
    else
    if (bandespec.nb==1)
    firay[0][i]= emissi[0][i]*sigma*pow(tmpray[i],4) - absorb[0][i]*e[0][i];
    else
    for (nb=0;nb<bandespec.nb;nb++) 
    for (i=0;i<nelray;i++)
    { 
    v=x1/tmpray[ig]; w1=wiebel(v);
    v=x2/tmpray[ig]; w2=wiebel(v);
    firay[nb][i]= emissi[nb][i]*sigma*pow(tmpray[i],4) - absorb[nb][i]*e[nb][i];
    }
    */



  /* 5-mise a jour des facettes avec flux imposee */
  for (n=0;n<phyray.bandespec.nb;n++) 
    for (i=0;i<fimpray.nelem;i++)
      {
	ngfac=fimpray.numf[i];
	firay[n][ngfac]=fimpray.val1[n][i];
      }
  
  /* 6-calcul de la temperature de la facette necessaire */
  for (i=0;i<fimpray.nelem;i++)
    {
      tfac=0.;
      ngfac=fimpray.numf[i];
      for (n=0;n<phyray.bandespec.nb;n++) 
	tfac += (1.-phyray.emissi[n][ngfac])/phyray.emissi[n][ngfac]*fimpray.val1[n][i] 
	  + radios[n][i];
      tmpray[ngfac]=pow(tfac/sigma,0.25);
    }
  
  
  if (affich.ray_fi2teq)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** fi2teq rang %d : calcul des emissivites equivalentes ",syrglob_rang);
	  printf(" des temperatures de rayonnement equivalentes\n");
	  printf("             facette  emissivite equi      temp equivalente (degres c)\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** fi2teq rang %d : calculation of equivalent emissivity ",syrglob_rang);
	  printf(" and equivalent radiation temperature\n");
	  printf("             face  emissivity equi      equivalente temp (degres c)\n");
	}
      for (i=0;i<nelray;i++) 
	printf("              %6d       %25.18e               %25.18e\n",
	       i,erayeq[i],trayeq[i]-tkel);
    }

  /* penser a faire un free pour e et ev ?????????????????????? */

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | gausei                                                               |
  |                                                                      |
  |======================================================================| */
void rrayrc_parall(int nelray,struct FacForme ***listefdf,double *diagfdf,
		   double *sufray,
		   double *rho,double *epr,
		   struct MatriceRay matrray,int *niter,
		   double *trav0,struct SDparall_ray sdparall_ray)
{
  int n,ngfac;
  double aa,bb,cc,d,ee,alfa,aalfa,tau,alfam1,alftau;
  double aux1,aux2,aux,denom;
  int i,j,nitsmo;
  double x0,resnor,prsca1;
  double epsis,zero,epssmo;
  struct FacForme *pff;
  double *x,*b,*xm1,*gd,*res,*z,*di,*resm1;
 

  x     = matrray.x;
  b     = matrray.b;
  xm1   = matrray.xm1;
  gd    = matrray.gd;
  res   = matrray.res;
  z     = matrray.z;
  di    = matrray.di;
  resm1 = matrray.resm1;
 

  zero=0.;
  nitsmo=100;
  epssmo=1.e-12;
  n=0;

  /* 1- initialisation des vecteurs auxiliaires */
  for (i=0;i<nelray;i++)
    {
      xm1[i]=x[i]=epr[i]; 
      b[i]=epr[i];
      di[i]=sufray[i]-rho[i]*diagfdf[i];
    }

  /* norme du second membre */
  prosca_ray_parall(&prsca1,x,x,sdparall_ray);
  x0=sqrt(prsca1);
  
  if (x0< 1.e-20 ) x0=1.e-6;
  epsis=1.e-4 * x0;


  fdf_Y_parall(res,x,listefdf,diagfdf,trav0,sdparall_ray);

  for (i=0;i<nelray;i++)
    res[i] = -rho[i]*res[i] + di[i]*x[i] - b[i];

  prosca_ray_parall(&prsca1,res,res,sdparall_ray);
  resnor=sqrt(prsca1);
  /*   printf(" >>>> ray=resnor_ini par : %25.18e \n",resnor); */



  if (sdparall_ray.rangray==0)
    {
      if (SYRTHES_LANG == FR)
	printf(" *** RRAYRC : ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
      else if (SYRTHES_LANG == EN)
	printf(" *** RRAYRC : ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");
    }

  if ( resnor<=epsis && resnor<=epsgcs_rad*sqrt((double)(sdparall_ray.nelraytot)) )
    {
      if (sdparall_ray.rangray==0)
	printf("                 %4d         %12.5e                %12.5e\n",
	       n,resnor/x0,resnor/sqrt((double)(sdparall_ray.nelraytot)));
      *niter=n;
    }
  
  else
    {
      for (i=0;i<nelray;i++) resm1[i]=res[i];
      n=0;
      
      do
	{
	  n++;
	  for (i=0;i<nelray;i++) gd[i]=res[i]/di[i];
	  
	  fdf_Y_parall(z,gd,listefdf,diagfdf,trav0,sdparall_ray);
	  
	  for (i=0;i<nelray;i++)
	    {z[i] = -rho[i]*z[i] + di[i]*gd[i];trav0[i]=res[i]-resm1[i];}

	  aa=0.;bb=0.;cc=0.;d =0.;ee=0.;
	  prosca_ray_parall(&aa,z,res,sdparall_ray);
	  prosca_ray_parall(&bb,z,resm1,sdparall_ray);
	  prosca_ray_parall(&cc,res,trav0,sdparall_ray);
	  prosca_ray_parall(&d,resm1,trav0,sdparall_ray);
	  prosca_ray_parall(&ee,z,z,sdparall_ray);

	  denom=(cc-d )*ee-(aa-bb)*(aa-bb);
	  if(fabs(denom)< 1.e-20) alfa=1.;
	  else alfa=((aa-bb)*bb-d *ee)/denom;
	  aalfa=fabs(alfa);
	  if (aalfa<1e-20 || fabs(aalfa-1.)< 1.e-20) {alfa=1.; tau=aa/ee;}
	  else tau=aa/ee + (1.-alfa)/alfa * bb/ee;
	  alfam1 =1.-alfa;
	  alftau =-alfa*tau;
	  for (i=0;i<nelray;i++)
	    {
	      aux1    =res[i];
	      aux2    =x[i];
	      res[i]  =alfa*aux1+alfam1*resm1[i]+alftau*z[i];
	      resm1[i]=aux1;
	      x[i]    =alfa*aux2+alfam1*xm1[i]+alftau*gd[i];
	      xm1[i]  =aux2;
	    }

	  prosca_ray_parall(&prsca1,res,res,sdparall_ray);
	  resnor=sqrt(prsca1);

/* 	  if (sdparall_ray.rangray==0) printf("rrayrc_parall : it=%d   resnor=%25.18e \n",n,resnor); */

	  if (sdparall_ray.rangray==0 && n%10==0)
	    printf("                 %4d         %12.5e        %12.5e\n",
		   n,resnor/x0,resnor/sqrt((double)(sdparall_ray.nelraytot)));

	}
      while(!( (resnor<=epsis &&  resnor<=epsgcs_rad*sqrt((double)(sdparall_ray.nelraytot))) || n>=nitsmo)); 


      if (sdparall_ray.rangray==0 && n%10!=0)
	printf("                 %4d         %12.5e        %12.5e\n",
	       n,resnor/x0,resnor/sqrt((double)(sdparall_ray.nelraytot)));
      *niter=n;
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Produit scalaire pour le rayonnement en parallele                    |
  |                                                                      |
  |======================================================================|*/
void prosca_ray_parall(double *resu,double *a, double *b,
		       struct SDparall_ray sdparall_ray)
{
#ifdef _SYRTHES_MPI_
  int i;
  double resuloc;

  /* produit scalaire sur le proc courant */
  for (resuloc=0.,i=0;i<sdparall_ray.nelrayloc[sdparall_ray.rangray];i++) resuloc+=a[i]*b[i];

  MPI_Allreduce(&resuloc,resu,1,MPI_DOUBLE,MPI_SUM,sdparall_ray.syrthes_comm_ray);

#endif
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | fdf_Y_parall                                                         |
  |         Calcul matrice vecteur   FDF.J en parallele (rayonnement)    |
  |        RES = fdf . Y   (sauf la diagonale)                           |
  |======================================================================|*/
void fdf_Y_parall(double *res,double *y,
		  struct FacForme ***listefdf,double *diagfdf,
		  double *trav0,struct SDparall_ray sdparall_ray )
{
#ifdef _SYRTHES_MPI_
  int i,n,ideb,ifin,nelemloc;
  struct FacForme *pff;

  ideb=sdparall_ray.ieledeb[sdparall_ray.rangray];
  ifin=sdparall_ray.ieledeb[sdparall_ray.rangray+1];
  nelemloc=sdparall_ray.nelrayloc[sdparall_ray.rangray];

 

  /* multiplication en local */
  /* ----------------------- */
  for (i=0;i<nelemloc;i++) res[i]=0.;

  for (i=0;i<nelemloc;i++) 
    if (listefdf[sdparall_ray.rangray][i])
      {
	pff=listefdf[sdparall_ray.rangray][i];
	while(pff) {
	  res[i] += pff->fdf * y[pff->numenface];
	  pff=pff->suivant;}
      }

  /* on complete avec ce qui provient des autres processeurs */
  /* ------------------------------------------------------- */
  for (n=0;n<sdparall_ray.npartsray;n++)
    {
      if (n!=sdparall_ray.rangray && sdparall_ray.fdfproc[n]) /* si ce n'est pas le proc courant  et qu'il y a */
 	{                                                  /* des fdf a echanger avec ce proc               */
	  for (i=0;i<nelemloc;i++) trav0[i]=y[i];
	  for (i=nelemloc;i<sdparall_ray.nelrayloc_max;i++) trav0[i]=0.;

	  MPI_Sendrecv_replace(trav0,sdparall_ray.nelrayloc_max,MPI_DOUBLE,n,0,n,0,sdparall_ray.syrthes_comm_ray,&status);

	  /* on fait la multiplication avec ce que l'on a recu */
	  for (i=0;i<nelemloc;i++) 
	    if (listefdf[n][i])
	      {
		pff=listefdf[n][i];
		while(pff) {res[i] += pff->fdf * trav0[pff->numenface]; pff=pff->suivant;}
	      }

	}
    }
#endif
}
