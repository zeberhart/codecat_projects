/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: 4fadc58de91981c59dfb36f62ba1863fe05a058a $
*/

/* Define if you have FUSE */
#undef HAVE_LIBFUSE
