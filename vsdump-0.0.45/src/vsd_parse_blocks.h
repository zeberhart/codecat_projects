typedef struct{
	guint32	len;
	guint8	type;
	guint8	idx;
} VsdBlockHdr;

typedef struct{
	guint8	idx;
	char		*formula;
} VsdBlock;

VsdBlock*	vsd_parse_blocks(GByteArray *stream, int *offset, int version);
