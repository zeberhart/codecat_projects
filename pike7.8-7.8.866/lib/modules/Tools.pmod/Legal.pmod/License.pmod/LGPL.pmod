#pike __REAL_VERSION__

// $Id: 848983c4fec48424237622a637d46ce37a8a6a6e $

inherit .GPL;

protected constant name = "GNU Lesser General Public License 2.1";
protected constant text = #string "lgpl.txt";
