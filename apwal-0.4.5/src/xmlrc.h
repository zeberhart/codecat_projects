/* xmlrc.h
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#ifndef XMLRC__H
#define XMLRC__H

#include "common.h"

void xmlrc_load_from_file(app_list_t **apps, iconsel_pref_t **iconsel_pref,
                          apwal_pref_t **apwal_pref);
void xmlrc_save_to_file(app_list_t  *apps, iconsel_pref_t *iconsel_pref,
                        apwal_pref_t *apwal_pref);

gboolean xmlrc_resource_file_exist(void);
void     xmlrc_resource_file_create(void);

void     xmlrc_set_resource_file(char *config_file);

#endif /*XMLRC__H*/
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
