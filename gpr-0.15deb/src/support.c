/*
 * GPR
 *
 * Copyright (C) 2006 A. Mennucci
 *
 * Copyright (C) 2000 CompuMetric Labs, Inc.
 *
 * For more information contact:
 *        Thomas Hubbell
 *        CompuMetric Labs, Inc.
 *        5920 Grelot Road, Suite C-2
 *        Mobile, AL 36609
 * 
 * Voice: (334) 342-2220
 *   Fax: (334) 343-2261
 * Email: thubbell@compumetric.com
 *   Web: http://www.compumetric.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *    
 *        File: Support.c
 * 
 * Description: Support functions needed by gpr.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gtk/gtk.h>

#include "support.h"

GtkWidget *lookup_widget_DEPRECATED(GtkWidget * widget, const gchar * widget_name)
{
	GtkWidget *parent, *found_widget;

	for (;;) {
		if (GTK_IS_MENU(widget))
			parent = gtk_menu_get_attach_widget(GTK_MENU(widget));
		else
			parent = widget->parent;
		if (parent == NULL)
			break;
		widget = parent;
	}

	found_widget =
	    (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), widget_name);
	if (!found_widget)
		g_warning(_("Widget not found: %s"), widget_name);
	return found_widget;
}

/* This is a dummy pixmap we use when a pixmap can't be found. */
static char *dummy_pixmap_xpm[] = {
/* columns rows colors chars-per-pixel */
	"1 1 1 1",
	"  c None",
/* pixels */
	" ",
	" "
};







GtkWidget *create_pixmap_from_data
(
 GtkWidget * widget,
 gchar ** xpm_data,
 gboolean gnome_pixmap
) {

        GdkColormap *colormap;
        GdkPixmap *gdkpixmap;
        GdkBitmap *mask;
        GtkWidget *pixmap;

        g_assert (!gnome_pixmap);      

        colormap = gtk_widget_get_colormap(widget);
        gdkpixmap =
            gdk_pixmap_colormap_create_from_xpm_d(NULL, colormap, &mask, NULL,
                                                  xpm_data);
        if (gdkpixmap == NULL)
                g_error(_("Couldn't create pixmap from data."));
        pixmap = gtk_pixmap_new(gdkpixmap, mask);
        gdk_pixmap_unref(gdkpixmap);
        gdk_bitmap_unref(mask);
        return pixmap;
}
