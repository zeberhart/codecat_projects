/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * vsdump: test program to dump and parse content of vsd file
 *
 * Copyright (C) 2006-2007	Valek Filippov (frob@df.ru)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 3 or later of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 */
#include <gsf/gsf-input.h>
#include "vsdump.h"
#include "vsd_utils.h"
#include "vsd_dump_stream_c.h"

int
vsd_dump_stream_c(GByteArray *decomp, char *dirname, int *numfrgn)
{
		guint j;
		char *tmpstr;
		FILE *fimage;

/* I'm lazy to take Foreign Format from ForeignType chunk... */
        if (0x89 ==decomp->data[0] && 0x89 ==decomp->data[1] && 0x89 ==decomp->data[2] && 0x89 ==decomp->data[3]){
            tmpstr = g_strdup_printf ("file%05d.png", *numfrgn);
        }else{
            if (0x42 ==decomp->data[0] && 0x4d==decomp->data[1]){
                tmpstr = g_strdup_printf ("file%05d.bmp", *numfrgn);
            }else{
                if (0x20 ==decomp->data[0x2c] && 0x45 ==decomp->data[0x2d] && 0x4d ==decomp->data[0x2e] && 0x46 ==decomp->data[0x2f]){
                    tmpstr = g_strdup_printf ("file%05d.emf", *numfrgn);
                }else{
                    if (0x4a ==decomp->data[0xa] && 0x46 ==decomp->data[0xb] && 0x49 ==decomp->data[0xc] && 0x46 ==decomp->data[0xd]){
                        tmpstr = g_strdup_printf ("file%05d.jpg", *numfrgn);
                    }else{
                        if (0x47 ==decomp->data[4] && 0x49 ==decomp->data[4] && 0x46 ==decomp->data[6] && 0x38 ==decomp->data[7]){
                        tmpstr = g_strdup_printf ("file%05d.gif", *numfrgn);
                        }else{
                            tmpstr = g_strdup_printf ("file%05d.wmf", *numfrgn);
                        }
                    }
                }
            }
		} 
 					
 		fimage = vsd_openfd (dirname, tmpstr);

/* 'Magic numbers'. 4 -- skip header (length of decompressed stream dword) */
		for(j = 4; j < decomp->len; j++){
			fputc(decomp->data[j],fimage);
		}
		(*numfrgn)++;
		fclose(fimage);
		
		return 0;
}
