/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#include "example.h"

/* dummy function #1 */
double example_function1(double *array, int array_size) {
  int i;
  double sum = 0;
  for (i = 0; i < array_size; i++) {
    array[i] = array[i] * (i + 1);
    sum += array[i];
  }
  return sum;
}

/* dummy function #2 */
int example_function2(int *array, int array_size) {
  int i;
  int sum = 0;
  for (i = 0; i < array_size; i++) {
    array[i] = array[i] * (i + 1);
    sum += array[i];
  }
  return sum;
}
