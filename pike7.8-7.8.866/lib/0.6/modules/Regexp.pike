// $Id: 500e1e57ba1aceace666d309e03a051cf6020b31 $

#pike 7.0

inherit Regexp;

// Hide replace().
private string replace(string in, string|function(string:string) transform)
{
  return ::replace(in, transform);
  replace;	// Disable warning for unused symbol...
}

protected string _sprintf()
{
  return "Regexp 0.6";
}
