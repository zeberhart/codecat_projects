/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <ctype.h>

#include "syr_usertype.h"
#include "syr_proto.h"


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |     Manipulation de chaines                                          |
  |======================================================================| */
void extr_motcle_(char* mc,char *ch,int *i1,int *i2)
{
  int i=0,j=0;
  char *c;

  c=ch;
  while(!strncmp(c," ",1) && i<strlen(ch)-1) {c++;i++;}
  if (i==strlen(ch)-1) 
    {
      *i1=*i2=0;
    }
  else
    {
      j=i;
      while(strncmp(c,"=",1) && j<strlen(ch)-1) {c++;j++;}
      if (j==strlen(ch)-1) 
	{
	  if (SYRTHES_LANG == FR)
	    {
	      printf("Erreur sur la chaine %s\n",ch);
	      printf("On attend un symbole '='\n");
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("Error on the string %s\n",ch);
	      printf("The following symbol is expected '='\n");
	    }
	  syrthes_exit(1);
	}
      else
	{
	  strncpy(mc,ch+i,j-i);
	  strcpy(mc+j-i,"\0");
	  /*	  *i1=i; *i2=j+1; */
	  *i1=i; *i2=j;
	}
    }
  
  
  /*  int i;
      
      i=0; *i1=*i2=-1;
      while (*i2==-1)
      {
      if (ch[i]=='\'')
      if (*i1==-1) *i1=i;
      else *i2=i;
      i++;
      }
      strncpy(mc,ch+*i1+1,*i2-*i1-1);
      strcpy(mc+*i2-*i1-1,"\0");
  */
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |      Extraction de mots-cles                                         |
  |======================================================================| */
void extr_motcle(char* mc,char *ch,int *i1,int *i2)
{
  int i=0,j=0;
  char *c;

  c=ch;
  while(!strncmp(c," ",1) && i<strlen(ch)-1) {c++;i++;}
  if (i==strlen(ch)-1) 
    {
      *i1=*i2=0;
    }
  else
    {
      j=i;
      while(strncmp(c," ",1) && j<strlen(ch)-1) {c++;j++;}
      strncpy(mc,ch+i,j-i);
      strcpy(mc+j-i,"\0");
      /*      *i1=i; *i2=j+1;*/
      *i1=i; *i2=j;
    }
  
} 
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | lecture des mot-cle : lecture d'une reponse de type "chaine"         |
  | obligatoirement non vide et sans espaces                             |
  |======================================================================| */
int rep_ch(char*ch1,char *ch)
{
  int i=0,j=0;
  char c;

  c=ch[0]; i=j=0;
  while( c == ' ' && i<strlen(ch)-1) {i++; c=ch[i];}
  if (i==strlen(ch)-1) {
    return 0;
  }
  else {
    j=i;
    c=ch[j];
    while( c != ' ' && iscntrl(c)==0 && j<strlen(ch)-1) {j++;c=ch[j];}
    strncpy(ch1,ch+i,j-i);
    strcpy(ch1+j-i,"\0");
    return j;
  }
}
  
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2012 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | lecture des mot-cle : lecture d'une reponse de type "chaine"         |
  | obligatoirement non vide, mais presences d'espaces possible          |
  |======================================================================| */
int rep_chw(char*ch1,char *ch)
{
  int i=0,j=0;
  char c;

  c=ch[0]; i=j=0;
  while( c == ' ' && i<strlen(ch)-1) {i++; c=ch[i];}
  if (i==strlen(ch)-1) {
    return 0;
  }
  else {
    j=i;
    c=ch[j];
    while( c != '\0' && iscntrl(c)==0 && j<strlen(ch)-1) {j++;c=ch[j];}
    strncpy(ch1,ch+i,j-i);
    strcpy(ch1+j-i,"\0");
    return j;
  }
}
  
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
int rep_int(char *ch)
{
  int i;
  
  sscanf(ch,"%d",&i);
  return(i);
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
double rep_dbl(char *ch)
{
  double d;
  
  sscanf(ch,"%lf",&d);
  return(d);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void rep_listint(int *ilist,int *nb,char *ch)
{
  int i=0;
  char c;
  
  *nb=0;
  c=ch[0];
  while(iscntrl(c)==0 && i<strlen(ch)-1)
    {
      while( c == ' ') {i++;c=ch[i];}
      if (iscntrl(c)==0 && i<strlen(ch)-1)
	{
	  sscanf(ch+i,"%d",ilist+*nb); 
	  (*nb)++;
	  while( c != ' ') {i++;c=ch[i];}
	}
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void rep_listdble(double *dlist,int *nb,char *ch)
{
  int i=0;
  char c;
  
  *nb=0;
  c=ch[0];
  while(iscntrl(c)==0 && i<strlen(ch)-1)
    {
      while( c == ' ') {i++;c=ch[i];}
      if (iscntrl(c)==0 && i<strlen(ch)-1)
	{
	  sscanf(ch+i,"%le",dlist+*nb); 
	  (*nb)++;
	  while( c != ' ') {i++;c=ch[i];}
	}
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void rep_ndbl(int nb,double *dlist,int *ifin,char *ch)
{
  int i=0,n=0;
  char *c;
  
  c=ch;
  while(n<nb)
    {
      while(!strncmp(c," ",1)) {c++;i++;}
      sscanf(c,"%lf",dlist+n); 
      n++;
      while(strncmp(c," ",1)&&i<strlen(ch)) {c++;i++;}
    }
  *ifin=i;
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void rep_nint(int nb,int *ilist,int *ifin,char *ch)
{
  int i=0,n=0;
  char *c;
  
  c=ch;
  while(n<nb)
    {
      while(!strncmp(c," ",1)) {c++;i++;}
      sscanf(c,"%d",ilist+n); 
      n++;
      while(strncmp(c," ",1)&&i<strlen(ch)) {c++;i++;}
    }
  *ifin=i;
}






