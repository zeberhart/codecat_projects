/*
     This file is part of GNUnet
     (C) 2005, 2006, 2010 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/fs/gnunet-fs-gtk_main-window-view-toggles.c
 * @author Christian Grothoff
 * @brief This file contains callbacks for the 'view' menu that toggle views.
 */
#include "gnunet-fs-gtk_common.h"
#include "gnunet-fs-gtk.h"


/**
 * Toggle the visibility of a widget based on the checkeness
 * of a menu item.
 *
 * @param toggled_widget name of widget to toggle
 * @param toggle_menu name of menu entry
 */
static void
toggle_view (const char *toggled_widget,
	     const char *toggle_menu)
{
  GtkCheckMenuItem *mi;
  GtkWidget *widget;

  widget = GTK_WIDGET (GNUNET_FS_GTK_get_main_window_object (toggled_widget));
  mi = GTK_CHECK_MENU_ITEM (GNUNET_FS_GTK_get_main_window_object (toggle_menu));
  if (gtk_check_menu_item_get_active (mi))
    gtk_widget_show (widget);
  else
    gtk_widget_hide (widget);
}


/**
 * If nothing in the "GNUNET_GTK_main_window_extras_vbox" is visible,
 * hide the entire vbox (and show it if anything is visible).
 */
static void
check_extras ()
{
  GtkCheckMenuItem *m1;
  GtkCheckMenuItem *m2;
  GtkWidget *wbox;

  m1 = GTK_CHECK_MENU_ITEM (GNUNET_FS_GTK_get_main_window_object ("GNUNET_GTK_main_menu_view_metadata"));
  m2 = GTK_CHECK_MENU_ITEM (GNUNET_FS_GTK_get_main_window_object ("GNUNET_GTK_main_menu_view_preview"));
  wbox = GTK_WIDGET (GNUNET_FS_GTK_get_main_window_object ("GNUNET_GTK_main_window_extras_vbox"));
  if ( (gtk_check_menu_item_get_active (m1)) ||
       (gtk_check_menu_item_get_active (m2)) )
    gtk_widget_show (wbox);
  else
    gtk_widget_hide (wbox);
}


/**
 * Preview view is toggled.
 *
 * @param dummy widget triggering the event
 * @param data main window builder (unused)
 */
void
GNUNET_GTK_main_menu_view_preview_toggled_cb (GtkWidget * dummy,
					      gpointer data)
{
  toggle_view ("GNUNET_GTK_main_window_preview_image",
               "GNUNET_GTK_main_menu_view_preview");
  check_extras ();
}


/**
 * Metadata view is toggled.
 *
 * @param dummy widget triggering the event
 * @param data main window builder (unused)
 */
void
GNUNET_GTK_main_menu_view_metadata_toggled_cb (GtkWidget * dummy,
					       gpointer data)
{
  toggle_view ("GNUNET_GTK_main_window_metadata_treeview",
               "GNUNET_GTK_main_menu_view_metadata");
  check_extras ();

}


/**
 * Preview view is toggled.
 *
 * @param dummy widget triggering the event
 * @param data main window builder (unused)
 */
void
GNUNET_GTK_main_menu_view_search_toggled_cb (GtkWidget * dummy,
					     gpointer data)
{
  toggle_view ("main_window_search_hbox",
               "GNUNET_GTK_main_menu_search_preview");
}

/* end of gnunet-fs-gtk_main-window-view-toggles.c */
