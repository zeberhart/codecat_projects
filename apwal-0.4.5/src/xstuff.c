/* xstuff.c
 * Copyright (C) 2002-2004 Pascal Eberhard <pascal.ebo@netcourrier.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if 0 /* DEPRECATED - REPLACE BY gdk_screen_width & height */

#include <X11/Xlib.h>   /* X[Open|Close]Display, XDefaultScreenOfDisplay */
#include <gtk/gtk.h>

// ----------------------------------------------------------------------------
void get_resolution(int *width, int *height)
{
  Display *display;
  Screen  *screen;

  g_assert(width != NULL && height != NULL);

  display = XOpenDisplay("");
  screen  = XDefaultScreenOfDisplay(display);
  *width  = screen->width;
  *height = screen->height;
  XCloseDisplay(display);
}

#endif

// ----------------------------------------------------------------------------
