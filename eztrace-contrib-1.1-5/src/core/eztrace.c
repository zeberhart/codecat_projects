/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include "pptrace.h"
#include "eztrace.h"

extern int pptrace_debug_level;

void set_launcher_env() {
  setenv("TESTLAUNCHER", "1", 1);
}

void unset_launcher_env() {
  setenv("TESTLAUNCHER", "0", 1);
}

int file_exists(const char *pathname) {
  if (access(pathname, F_OK) != -1) {
    return 1; // file exists
  }
  return 0;
}

int str_ends_with(char *str, char *end) {
  size_t str_len = strlen(str);
  size_t end_len = strlen(end);
  char *str_end = str + str_len - end_len;
  return strcmp(str_end, end) == 0;
}

void usage(const char *prog_name) {
  printf("Usage: %s [OPTION] program [arg1 arg2 ...]\n", prog_name);
  printf("\t-t \"plugin1 plugin2 ... pluginN\" Select a list of plugins\n");
  printf("\t-o <directory>			 Select the output directory\n");
  printf("\t-l <directory>			 Select a plugin directory\n");
  printf("\t-f				 Enable EZTRACE_FLUSH\n");
  printf("\t-d                               Debug mode\n");
  printf("\t-?  -h                           Display this help and exit\n");
  /* todo: add a verbose mode */
}

static char* get_env_str(const char* env_name, const char* suffix) {
  char* tmp = getenv(env_name);
  int string_length = 1;
  char* res = NULL;

  if ((tmp == NULL || tmp[0] == '\0') && suffix == NULL)
    return NULL;
  if (tmp && !suffix)
    return strdup(tmp);
  if (!tmp && suffix)
    return strdup(suffix);
  string_length += strlen(tmp) + strlen(suffix);
  res = calloc(string_length, sizeof(char));
  sprintf(res, "%s%s", tmp, suffix);
  return res;
}

/* add to_add to an already allocated string */
static char* add_to_str(char* dest_str, const char* to_add) {
  int len = strlen(dest_str) + strlen(to_add) + 1;
  dest_str = realloc(dest_str, len);
  strcat(dest_str, to_add);
  return dest_str;
}

int main(int argc, char **argv) {
  int debug = 0;
  int i, j = 0;
  int test = 0;
  char* libdir=NULL;
  char *ezt_library_path = NULL;
  int nb_opts = 0; // options
  char *prog_name = NULL; // prog_name
  void *bin = NULL;
  int nb_modules = 0;

  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i], "-d")) {
      debug = 1;
      nb_opts++;
    } else if (!strcmp(argv[i], "-p")) {
      test = 1;
      nb_opts++;
    } else if (!strcmp(argv[i], "-t")) {
      setenv("EZTRACE_TRACE", argv[i + 1], 1);
      i++;
      nb_opts += 2;
    } else if (!strcmp(argv[i], "-o")) {
      setenv("EZTRACE_TRACE_DIR", argv[i + 1], 1);
      i++;
      nb_opts += 2;
    } else if (!strcmp(argv[i], "-l")) {
      setenv("EZTRACE_LIBRARY_PATH", argv[i + 1], 1);
      i++;
      nb_opts += 2;
    } else if (!strcmp(argv[i], "-f")) {
      setenv("EZTRACE_FLUSH", "1", 1);
      nb_opts++;
    } else if (!strcmp(argv[i], "-?") || !strcmp(argv[i], "-h")) {
      usage(argv[0]);
      return EXIT_SUCCESS;
    } else if (argv[i][0] == '-' && argv[i][1] == 'v') {
      nb_opts++;
      for (j = 1; argv[i][j] == 'v'; j++)
	pptrace_debug_level++;
    } else {
      /* Unknown parameter name. It's probably the program name. We can stop
       * parsing the parameter list.
       */
      break;
    }
  }

  // list of enabled module
  char *ezt_trace = get_env_str("EZTRACE_TRACE", "");

  if(!test) {
    int len = strlen(EZTRACE_LIB_DIR);
    libdir=malloc(sizeof(char)*(len+2));
    snprintf(libdir, len+2, ":%s", EZTRACE_LIB_DIR);
  }

  // user libs env variable
  ezt_library_path = get_env_str("EZTRACE_LIBRARY_PATH", libdir);

  prog_name = argv[nb_opts + 1];
  if (prog_name == NULL) {
    usage(argv[0]);
    return EXIT_SUCCESS;
  }

  // init binary & catch all modules
  bin = pptrace_prepare_binary(prog_name);
  if (!bin) {
    fprintf(stderr, "Unable to load binary %s\n", prog_name);
    return EXIT_FAILURE;
  }

  // modules in ezt_trace
  if (ezt_trace != NULL) {
    char *module = strtok(ezt_trace, " ");
    while (module != NULL && ++nb_modules)
      module = strtok(NULL, " ");
    free(ezt_trace);
    if(nb_modules == 0) {
      unsetenv("EZTRACE_TRACE");
      ezt_trace=NULL;
    }
  }


  char *modules[nb_modules];
  if (nb_modules != 0) {
    // we have to get EZTRACE_TRACE again because we strtok-ed it
    ezt_trace = get_env_str("EZTRACE_TRACE", "");
    char *module = strtok(ezt_trace, " ");
    i = 0;
    while (module != NULL) {
      modules[i++] = module;
      module = strtok(NULL, " ");
    }
  }

  // dirs in ezt_library_path
  int nb_dirs = 0;
  if (ezt_library_path != NULL) {
    char *dir = strtok(ezt_library_path, ":");
    while (dir != NULL && ++nb_dirs)
      dir = strtok(NULL, ":");
    free(ezt_library_path);
  }

  char *dirs[nb_dirs];
  if (nb_dirs != 0) {
    // we have to get EZTRACE_LIBRARY_PATH again because we strtok-ed it
    ezt_library_path = get_env_str("EZTRACE_LIBRARY_PATH",libdir);
    char *dir = strtok(ezt_library_path, ":");
    i = 0;
    while (dir != NULL) {
      dirs[i++] = dir;
      dir = strtok(NULL, ":");
    }
  }

  // libeztrace should always be preloaded!
  char *files = NULL;
  if (test) {
    fprintf(stderr, "Eztrace test Mode\n");
    files = strdup(EZTRACE_ABS_TOP_BUILDDIR);
    files = add_to_str(files, "src/core/.libs/libeztrace-autostart.so");
  } else {
    files = strdup(EZTRACE_LIB_DIR);
    files = add_to_str(files, "libeztrace-autostart.so");
  }
  if (pptrace_add_preload(bin, files)) {
    fprintf(stderr, "Unable to add %s to LD_PRELOAD\n", files);
    return EXIT_FAILURE;
  }
  set_launcher_env();
  if (ezt_trace != NULL) {
    for (i = 0; i < nb_modules; i++) {
      int module_found=0;
      for (j = 0; j < nb_dirs; j++) {
	char *pathname = strdup(dirs[j]);
	pathname = add_to_str(pathname, "/libeztrace-autostart-");
	pathname = add_to_str(pathname, modules[i]);
	pathname = add_to_str(pathname, ".so");

	if (file_exists(pathname)) {
	  module_found=1;
	  if (pptrace_load_module(bin, pathname)) {
	    fprintf(stderr, "Unable to add %s to LD_PRELOAD\n",
		    pathname);
	    return EXIT_FAILURE;
	  }
	  files = add_to_str(files, ":");
	  files = add_to_str(files, pathname);
	}
	free(pathname);
      }
      if(!module_found) {
	fprintf(stderr,"[EZTrace] cannot find module %s\n", modules[i]);
      }
    }
    free(ezt_trace);
  } else {
    for (i = 0; i < nb_dirs; i++) {
      char *pathname = dirs[i];
      struct dirent *dirent;
      DIR *d = opendir(pathname);
      while ((dirent = readdir(d))) {
	if (!strncmp(dirent->d_name, "libeztrace-autostart-", 21)
	    && str_ends_with(dirent->d_name, ".so")) {
	  char *file = strdup(pathname);
	  file = add_to_str(file, "/");
	  file = add_to_str(file, dirent->d_name);
	  if (pptrace_load_module(bin, file)) {
	    fprintf(stderr, "Unable to add %s to LD_PRELOAD\n", file);
	    return EXIT_FAILURE;
	  }
	  files = add_to_str(files, ":");
	  files = add_to_str(files, file);
	  free(file);
	}
      }
      closedir(d);
    }
  }
  free(ezt_library_path);
  unset_launcher_env();
  // run
  if (debug) {
    // TODO: make it configurable
    char *debugger[5];
    debugger[0] = "gdb";
    debugger[1] = "-quiet";
    debugger[2] = "{name}";
    debugger[3] = "{pid}";
    debugger[4] = NULL;
    pptrace_add_debugger(bin, debugger);
  }
  if (pptrace_run(bin, argv + nb_opts + 1, environ)) {
    fprintf(stderr, "Unable to run the target\n");
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
