/***************************************************************************
 *   Copyright (C) 2004-2015 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 1.9.2. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 *
 *   Improvements and modifications:
 *   July 2009 - Added <<author_all_abbreviated>>, (C) 2009 by Dayu Huang
 ***************************************************************************/
#include "c2bIdLineEdit.h"

#include "c2bUtils.h"

#include <cb2bib_parameters.h>

#include <QAction>
#include <QContextMenuEvent>
#include <QMenu>


c2bIdLineEdit::c2bIdLineEdit(QWidget* parentw) : c2bLineEdit(parentw)
{
    a_author_all_abbreviated = new QAction(this);
    a_author_all_initials = new QAction(this);
    a_author_first = new QAction(this);
    a_citeid = new QAction(this);
    a_journal_initials = new QAction(this);
    a_pages_first = new QAction(this);
    a_ppages_first = new QAction(this);
    a_title = new QAction(this);
    a_title_underscored = new QAction(this);
    a_volume = new QAction(this);
    a_year_abbreviated = new QAction(this);
    a_year_full = new QAction(this);

    a_reset = new QAction(this);

    c2bUtils::addSeparator(this);
    addAction(a_author_all_abbreviated);
    addAction(a_author_all_initials);
    addAction(a_author_first);
    c2bUtils::addSeparator(this);
    addAction(a_citeid);
    c2bUtils::addSeparator(this);
    addAction(a_journal_initials);
    c2bUtils::addSeparator(this);
    addAction(a_pages_first);
    addAction(a_ppages_first);
    c2bUtils::addSeparator(this);
    addAction(a_title);
    addAction(a_title_underscored);
    c2bUtils::addSeparator(this);
    addAction(a_volume);
    c2bUtils::addSeparator(this);
    addAction(a_year_abbreviated);
    addAction(a_year_full);
    c2bUtils::addSeparator(this);
    addAction(a_reset);

    a_author_all_abbreviated->setText(tr("Insert <<author_all_abbreviated>>"));
    a_author_all_initials->setText(tr("Insert <<author_all_initials>>"));
    a_author_first->setText(tr("Insert <<author_first>>"));
    a_citeid->setText(tr("Insert <<citeid>>"));
    a_journal_initials->setText(tr("Insert <<journal_initials>>"));
    a_pages_first->setText(tr("Insert <<pages_first>>"));
    a_ppages_first->setText(tr("Insert <<ppages_first>>"));
    a_title->setText(tr("Insert <<title>>"));
    a_title_underscored->setText(tr("Insert <<title_underscored>>"));
    a_volume->setText(tr("Insert <<volume>>"));
    a_year_abbreviated->setText(tr("Insert <<year_abbreviated>>"));
    a_year_full->setText(tr("Insert <<year_full>>"));

    a_reset->setText(tr("Reset ID Pattern"));

    connect(a_author_all_abbreviated, SIGNAL(triggered()), this, SLOT(author_all_abbreviated()));
    connect(a_author_all_initials, SIGNAL(triggered()), this, SLOT(author_all_initials()));
    connect(a_author_first, SIGNAL(triggered()), this, SLOT(author_first()));
    connect(a_citeid, SIGNAL(triggered()), this, SLOT(citeid()));
    connect(a_journal_initials, SIGNAL(triggered()), this, SLOT(journal_initials()));
    connect(a_pages_first, SIGNAL(triggered()), this, SLOT(pages_first()));
    connect(a_ppages_first, SIGNAL(triggered()), this, SLOT(ppages_first()));
    connect(a_title, SIGNAL(triggered()), this, SLOT(title()));
    connect(a_title_underscored, SIGNAL(triggered()), this, SLOT(title_underscored()));
    connect(a_volume, SIGNAL(triggered()), this, SLOT(volume()));
    connect(a_year_abbreviated, SIGNAL(triggered()), this, SLOT(year_abbreviated()));
    connect(a_year_full, SIGNAL(triggered()), this, SLOT(year_full()));

    connect(a_reset, SIGNAL(triggered()), this, SLOT(reset()));

    // Hide citeid as default: CiteID line edit
    a_citeid->setVisible(false);
}

c2bIdLineEdit::~c2bIdLineEdit()
{}


void c2bIdLineEdit::setDocumentIDEdit()
{
    a_citeid->setVisible(true);
}

void c2bIdLineEdit::author_all_abbreviated()
{
    insert("<<author_all_abbreviated>>");
}

void c2bIdLineEdit::author_all_initials()
{
    insert("<<author_all_initials>>");
}

void c2bIdLineEdit::author_first()
{
    insert("<<author_first>>");
}

void c2bIdLineEdit::citeid()
{
    insert("<<citeid>>");
}

void c2bIdLineEdit::journal_initials()
{
    insert("<<journal_initials>>");
}

void c2bIdLineEdit::pages_first()
{
    insert("<<pages_first>>");
}

void c2bIdLineEdit::ppages_first()
{
    insert("<<ppages_first>>");
}

void c2bIdLineEdit::title()
{
    insert("<<title_25>>");
}

void c2bIdLineEdit::title_underscored()
{
    insert("<<title_underscored_25>>");
}

void c2bIdLineEdit::volume()
{
    insert("<<volume>>");
}

void c2bIdLineEdit::year_abbreviated()
{
    insert("<<year_abbreviated>>");
}

void c2bIdLineEdit::year_full()
{
    insert("<<year_full>>");
}

void c2bIdLineEdit::reset()
{
    if (a_citeid->isVisible())
        setText(C2B_DOCUMENT_ID_PATTERN);
    else
        setText(C2B_CITE_ID_PATTERN);
}
