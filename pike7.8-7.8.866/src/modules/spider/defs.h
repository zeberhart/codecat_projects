/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: f5bc9a92f7c603108e6419d700273283f113c732 $
*/

extern void f_discdate(INT32 argc);
extern void f_stardate (INT32 args);
