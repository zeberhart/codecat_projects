/*
     This file is part of GNUnet
     (C) 2005, 2006, 2010, 2012 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/fs/gnunet-fs-gtk_anonymity-widgets.h
 * @author Christian Grothoff
 * @brief operations to manage user's anonymity level selections
 */
#ifndef GNUNET_FS_GTK_ANONYMITY_SPIN_BUTTONS_H
#define GNUNET_FS_GTK_ANONYMITY_SPIN_BUTTONS_H

#include "gnunet-fs-gtk_common.h"


/**
 * Set the anonymity level displayed by a combo box.
 *
 * @param builder the builder of the combo box
 * @param combo_name name of the combo box
 * @param sel_level desired anonymity level
 * @return TRUE on success, FALSE on failure
 */
gboolean
GNUNET_GTK_select_anonymity_level (GtkBuilder * builder, gchar * combo_name,
                                   guint sel_level);


/**
 * Set the anonymity level displayed by a combo box.
 *
 * @param combo the combo box
 * @param sel_level desired anonymity level
 * @return TRUE on success, FALSE on failure
 */
gboolean
GNUNET_GTK_select_anonymity_combo_level (GtkComboBox *combo, guint sel_level);

#endif
