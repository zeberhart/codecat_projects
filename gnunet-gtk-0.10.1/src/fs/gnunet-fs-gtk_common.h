/*
     This file is part of GNUnet.
     (C) 2010 Christian Grothoff (and other contributing authors)

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA.
*/

/**
 * @file src/fs/gnunet-fs-gtk_common.h
 * @brief Common includes for all gnunet-gtk source files
 * @author Christian Grothoff
 */
#ifndef GNUNET_FS_GTK_COMMON_H
#define GNUNET_FS_GTK_COMMON_H

#include "gnunet_gtk.h"
#include <gnunet/gnunet_fs_service.h>
#include <extractor.h>


/**
 * Obtain pixbuf from thumbnail data in meta data.
 *
 * @param meta input meta data
 * @return NULL on error, otherwise the embedded thumbnail
 */
GdkPixbuf *
GNUNET_FS_GTK_get_thumbnail_from_meta_data (const struct
                                            GNUNET_CONTAINER_MetaData *meta);


/**
 * mmap the given file and run the #GNUNET_FS_directory_list_contents
 * function on it.
 *
 * @param filename name with the directory
 * @param dep function to call on each entry
 * @param dep_cls closure for @a dep
 * @return #GNUNET_OK on success
 */
int
GNUNET_FS_GTK_mmap_and_scan (const char *filename,
                             GNUNET_FS_DirectoryEntryProcessor dep,
                             void *dep_cls);


/**
 * Add meta data to list store.
 *
 * @param cls closure (the GtkListStore)
 * @param plugin_name name of the plugin that produced this value;
 *        special values can be used (i.e. '\<zlib\>' for zlib being
 *        used in the main libextractor library and yielding
 *        meta data).
 * @param type libextractor-type describing the meta data
 * @param format basic format information about data
 * @param data_mime_type mime-type of data (not of the original file);
 *        can be NULL (if mime-type is not known)
 * @param data actual meta-data found
 * @param data_len number of bytes in @a data
 * @return 0 to continue (always)
 */
int
GNUNET_FS_GTK_add_meta_data_to_list_store (void *cls, const char *plugin_name,
                                           enum EXTRACTOR_MetaType type,
                                           enum EXTRACTOR_MetaFormat format,
                                           const char *data_mime_type,
                                           const char *data, size_t data_len);


/**
 * Converts metadata specified by @a data of size @a data_len
 * and saved in format @a format to UTF-8 encoded string.
 * Works only for C-string and UTF8 metadata formats
 * (returns NULL for everything else).
 * Verifies UTF-8 strings.
 *
 * @param format format of the @a data
 * @param data data to convert
 * @param data_len length of the @a data buffer (in bytes)
 * @return NULL if can't be converted, allocated string otherwise,
 *         freeable with GNUNET_free().
 */
char *
GNUNET_FS_GTK_dubious_meta_to_utf8 (enum EXTRACTOR_MetaFormat format,
                                    const char *data, size_t data_len);



/**
 * Obtain the string we will use to describe a search result from
 * the respective meta data.
 *
 * @param meta meta data to inspect
 * @param is_a_dup is set to #GNUNET_YES if the result is a dup, and there was
 *        no description to be found. #GNUNET_NO otherwise.
 * @return description of the result in utf-8, never NULL
 */
char *
GNUNET_FS_GTK_get_description_from_metadata (const struct GNUNET_CONTAINER_MetaData *meta,
					     int *is_a_dup);


/**
 * A URI was selected (or pasted into the application).  Run
 * the appropriate action.
 *
 * @param uri the URI
 * @param anonymity_level anonymity level to use
 */
void
GNUNET_FS_GTK_handle_uri (const struct GNUNET_FS_Uri *uri,
			  guint anonymity_level);


/**
 * Converts a GtkTreeRowReference to a GtkTreeIter.
 *
 * @param rr row reference
 * @param iter pointer to an iter structure to fill
 * @return #GNUNET_OK if iter was filled, #GNUNET_SYSERR otherwise
 */
int
GNUNET_GTK_get_iter_from_reference (GtkTreeRowReference *rr,
				    GtkTreeIter *iter);


/**
 * Creates a GtkTreeRowReference from a GtkTreeIter.
 *
 * @param model a model to reference
 * @param iter an iter that points to a row in the model
 * @return newly created reference or NULL in case of error
 */
GtkTreeRowReference *
GNUNET_GTK_get_reference_from_iter (GtkTreeModel *model,
				    GtkTreeIter *iter);


/**
 * Fills "next_iter" with iterator for an item that comes next in the tree
 * after "iter".
 * Next item is, in order of precedence:
 * 1) First child of "iter", if "iter" has children and "allow_children"
 *   is enabled.
 * 2) Next sibling of "iter", unless "iter" is the last sibling.
 * If none of those are present, function recursively checks parents of
 *   "iter" until it finds next item or runs out of parents.
 *
 * @param model a model to reference
 * @param iter an iter that points to current row in the model
 * @param allow_children whether child of "iter" is considered to be next.
 * @param next_iter will be filled with the next row in the model on success
 * @return TRUE if next_iter is set to a valid iter,
 *         FALSE if ran out of parents
 */
gboolean
GNUNET_GTK_tree_model_get_next_flat_iter (GtkTreeModel *model,
					  GtkTreeIter *iter,
					  gboolean allow_children,
					  GtkTreeIter *next_iter);


#endif
/* end of gnunet-fs-gtk-common.h */
