#ifndef PRINTER_OPTIONS_H
#define PRINTER_OPTIONS_H
#include <glib.h>
#include <glib-object.h>
#include <ppd.h>

G_BEGIN_DECLS

struct PrinterOption {
	struct PrinterOption *next;
	PpdOption *option;
	int selected_index;
	int count;
	struct PrinterOptionChoice {
		const PpdChoice *choice;
		struct PrinterConstraint *constraints;
		int forbidden;
	} choices[]; // needs C99
};
struct _PrinterOptions {
	GObject parent;
	PpdFile *ppd;
	struct PrinterOption *options, *installable;
};

#define TYPE_PRINTER_OPTIONS (printer_options_get_type())
#define PRINTER_OPTIONS(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_PRINTER_OPTIONS, PrinterOptions))
#define PRINTER_OPTIONS_CLASS(klass) (G_TYPE_CHECK_CLASS(CAST((klass),TYPE_PRINTER_OPTIONS, PrinterOptionsClass))

typedef struct _PrinterOptions PrinterOptions;
typedef struct _PrinterOptionsClass PrinterOptionsClass;

GType printer_options_get_type (void) G_GNUC_CONST;
PrinterOptions *printer_options_new(const char*);
PpdFile *printer_options_get_handle(PrinterOptions *);
void printer_options_select(PrinterOptions *, const char *, const char *);
char *printer_options_concat(PrinterOptions *, char *, const char *, const char *);
char *printer_option_select(struct PrinterOption *, int);

G_END_DECLS

#endif
