/*
|| This file is part of Pike. For copyright information see COPYRIGHT.
|| Pike is distributed under GPL, LGPL and MPL. See the file COPYING
|| for more information.
|| $Id: b9893246f12f0003a2c0d0b2e29a8c33831a01e8 $
*/

#ifndef GMP_MACHINE_H
#define GMP_MACHINE_H

@TOP@
@BOTTOM@

/* Define this if you have -lz */
#undef HAVE_LIBZ

#endif
