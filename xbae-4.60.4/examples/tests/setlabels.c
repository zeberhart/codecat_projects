
#include <stdlib.h>
#include <stdio.h>

#include <X11/Intrinsic.h>
#include <Xm/Xm.h>

#include <Xbae/Matrix.h> /* for version info */
#include <Xm/Form.h>
#include <Xm/Text.h>

char *column_labels[] = {"a", "b", "c\nd"};
char *row_labels[] = {"r1", "r2", "r3"};

void
doit(XtPointer data)
{
    Widget widget = (Widget) data;

    static int r = 0;
    static int c = 0;

    static Boolean columns = True;
    static Boolean set = True;
    
    if (columns) {
        if (set) {
            printf("Setting column label %d %s\n", c, column_labels[c]);
            XbaeMatrixSetColumnLabel(widget, c, column_labels[c]);
        } else {
            printf("Unsetting column label %d\n", c);
            XbaeMatrixSetColumnLabel(widget, c, NULL);
        }
        c++;
    } else {
        if (set) {
            printf("Setting row label %d %s\n", r, row_labels[r]);
            XbaeMatrixSetRowLabel(widget, r, row_labels[r]);
        } else {
            printf("Unsetting row label %d\n", r);
            XbaeMatrixSetRowLabel(widget, r, NULL);
        }
        r++;
    }
    
    if (c == XtNumber(row_labels)) {
        c = 0;
        columns = False;
    }
    
    if (r == XtNumber(row_labels)) {
        r = 0;
        columns = True;
        set = !set;
    }
    
    XtAppAddTimeOut(
	    XtWidgetToApplicationContext(widget), 500, (XtTimerCallbackProc)doit,
	    (XtPointer)widget);
}           

int
main (int argc, char **argv)
{

    Widget toplevel, form, widget;

    char *rows[3][3] = {{"a","b","c"},{"d","e","f"},{"g","h","i"}};
    char **cells[3];
    cells[0] = &rows[0][0];
    cells[1] = &rows[1][0];
    cells[2] = &rows[2][0];
    
    /* Print run-time version of libXbae to stdout */
    printf("Using Xbae %s\n", XbaeGetVersionTxt());
    printf("This example was built with %s\n", XbaeVersionTxt);

    /* Print compile-time version of libXm to stdout */
    printf("Xbae was built with %s\n", XbaeGetXmVersionTxt());
    printf("and is running with %s\n", XmVERSION_STRING);

    toplevel = XtInitialize (argv[0], "Test", NULL, 0, &argc, argv);
    form = XtVaCreateManagedWidget("form", xmFormWidgetClass, toplevel,
                                   XmNwidth, 	500,
                                   XmNheight,	400,
                                   NULL);

    widget = XtVaCreateManagedWidget ("test_xbae",
                           xbaeMatrixWidgetClass,
                           form,
                           XmNtopAttachment,    XmATTACH_FORM,
                           XmNleftAttachment,   XmATTACH_FORM,
                           XmNbottomAttachment, XmATTACH_FORM,
                           XmNrightAttachment,  XmATTACH_FORM,
                           XmNtopOffset, 10,
                           XmNbottomOffset, 10,
                           XmNleftOffset, 10,
                           XmNrightOffset, 10,
                           XmNrows, XtNumber(row_labels),
                           XmNcolumns, XtNumber(column_labels),
                           XmNcells, cells,
                           NULL);

    printf("nrows=%d ncols=%d\n",XbaeMatrixNumRows(widget), XbaeMatrixNumColumns(widget));
    
    doit(widget);
                           
    XtRealizeWidget (toplevel);
    XtMainLoop ();

    return (EXIT_SUCCESS);
}
