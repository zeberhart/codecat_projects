Description:
 GAP assumes packages doc are built inside the GAP root.  This patch convert
 relative reference to absolute reference, based on $gaproot/pkg/gapdoc/doc.
 Note that gapdoc is irrelevant here, except that it is always available.
Origin: Debian
Author: Bill Allombert <ballombe@debian.org>
Last-Update: 2014-02-20
Index: gap-4r8p3/etc/convert.pl
===================================================================
--- gap-4r8p3.orig/etc/convert.pl	2016-03-19 23:17:51.000000000 +0100
+++ gap-4r8p3/etc/convert.pl	2016-04-06 17:58:46.961078342 +0200
@@ -141,6 +141,9 @@
 
 use Getopt::Std;
 
+$gaproot="/usr/share/gap/";
+#Added by Bill Allombert to absorb ../../.. from caller. Any pkg/*/* would do.
+$gappkg="$gaproot/pkg/GAPDoc/doc/";
 
 #
 # Global variables 
@@ -462,7 +465,7 @@
 sub hreftype {
     my ($book, $bdir) = @_;
     if ( !(exists $convertbooks{$book}) ) {
-      my @ls = `ls ${odir}$bdir`;
+      my @ls = `ls $gappkg$bdir`;
       $convertbooks{$book} 
           = (grep { m/^CHAP...[.]htm$/ } @ls) ?
                 1 :             # .htm files have shape CHAP<MMM>.htm
@@ -2168,11 +2171,11 @@
   if (/\\UseReferences\{\/([^}]*)}/) {
       getlabs "/$1/";
   } elsif (/\\UseReferences\{([^}]*)}/) {
-      getlabs "$dir$1/";
+      getlabs "$gappkg$1/";
   } elsif (/\\UseGapDocReferences\{\/([^}]*)}/) {
       getlabs "/$1/";
   } elsif (/\\UseGapDocReferences\{([^}]*)}/) {
-      getlabs "$dir$1/";
+      getlabs "$gappkg$1/";
 #      ($gapdocbook = $1) =~ s?.*/([^/]*)/doc?$1?;
 #      $gapdocbooks{$gapdocbook} = 1;
 #      print STDERR "GapDoc books: ", keys(%gapdocbooks), "\n";
Index: gap-4r8p3/doc/gapmacro.tex
===================================================================
--- gap-4r8p3.orig/doc/gapmacro.tex	2016-03-19 23:17:51.000000000 +0100
+++ gap-4r8p3/doc/gapmacro.tex	2016-04-06 17:02:51.956652586 +0200
@@ -891,11 +891,15 @@
 %%
 %F  \UseReferences{<book-path>}	. . . use references from book in <book-path>
 %%
+
+%% /usr/share/gap/pkg/GAPDoc/doc/:
+%% Added by Bill Allombert to absorb ../../.. from caller. Any pkg/*/* would do.
+
 \outer\def\UseReferences#1{\begingroup\def\makelabel##1##2{\m@kelabel{##1}{##2}{}}\undoquotes
-    \inputaux\labelin{#1/manual.lab}\endgroup
+    \inputaux\labelin{/usr/share/gap/pkg/GAPDoc/doc/#1/manual.lab}\endgroup
 }
 \outer\def\UseGapDocReferences#1{\begingroup\def\makelabel##1##2##3{\m@kelabel{##1}{##2}{##3}}\undoquotes
-    \inputaux\labelin{#1/manual.lab}\endgroup
+    \inputaux\labelin{/usr/share/gap/pkg/GAPDoc/doc/#1/manual.lab}\endgroup
 }
 \outer\def\GAPDocLabFile#1{\def\makelabel##1##2##3{\m@kelabel{##1}{##2}{##3}}}
 %
