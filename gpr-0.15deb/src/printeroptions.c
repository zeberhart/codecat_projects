/* Copyright 2011 Bernhard R. Link
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 */

#include "config.h"
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <glib-object.h>
#include <ppd.h>
#include "printeroptions.h"

struct PrinterConstraint {
	struct PrinterConstraint *next;
	struct PrinterOption *option;
	struct PrinterOptionChoice *choice;
};

struct _PrinterOptionsClass {
	GObjectClass parent_class;
};

static void printer_options_finalize(GObject *);
static void printer_options_class_init(PrinterOptionsClass *);

static void printer_options_init(PrinterOptions *i) {
}

G_DEFINE_TYPE_WITH_CODE(PrinterOptions, printer_options, G_TYPE_OBJECT,)

static void option_free(struct PrinterOption *o) {
	int i;

	if (o == NULL)
		return;
	for (i = 0 ; i < o->count ; i++) {
		struct PrinterOptionChoice *h = &o->choices[i];

		while (h->constraints != NULL) {
			struct PrinterConstraint *c = h->constraints;
			h->constraints = c->next;

			free(c);
		}
	}
	free(o->choices);
	free(o);
}

static void printer_options_finalize(GObject *o) {
	PrinterOptions *po = (PrinterOptions*)o;
	struct PrinterOption *option;

	option = po->options;
	while (option != NULL) {
		struct PrinterOption *h = option;
		option = option->next;
		option_free(h);
	}
	option = po->installable;
	while (option != NULL) {
		struct PrinterOption *h = option;
		option = option->next;
		option_free(h);
	}
	ppd_file_free(po->ppd);


	G_OBJECT_CLASS (printer_options_parent_class)->finalize (o);
}

static void printer_options_class_init(PrinterOptionsClass *class) {
	GObjectClass *o = (GObjectClass*) class;
	o->finalize = printer_options_finalize;
}

static struct PrinterOption *option_new(PpdOption *option) {
	struct PrinterOption *r;
	GSList *sl;
	size_t i, count = g_slist_length(option->choices);

	r = g_malloc0(sizeof(struct PrinterOption) +
			(count + 1) * sizeof(struct PrinterOptionChoice));
	r->count = count;
	r->option = option;
	r->selected_index = -1;
	i = 0;
	for (sl = option->choices ; sl != NULL ; sl = g_slist_next(sl)) {
		PpdChoice *choice = sl->data;

		r->choices[i].choice = choice;
		if (choice->marked)
			r->selected_index = i;
		i++;
	}
	r->choices[count].choice = NULL;
	g_assert (count == i);
	return r;
}

static void group_add(struct PrinterOption ***next_p, PpdGroup *group) {
	GSList *gl, *ol;

	for (ol = group->options ; ol != NULL ; ol = g_slist_next(ol)) {
		PpdOption *option = ol->data;
		(**next_p) = option_new(option);
		*next_p = &(**next_p)->next;
	}
	for (gl = group->subgroups ; gl != NULL ; gl = g_slist_next(gl)) {
		PpdGroup *subgroup = gl->data;
		group_add(next_p, subgroup);
	}
}

static struct PrinterOption *option_find(PrinterOptions *po, const char *key) {
	struct PrinterOption *p;

	for (p = po->options ; p != NULL ; p = p->next) {
	       if (strcmp(p->option->keyword->str, key) == 0)
		       return p;
	}
	for (p = po->installable ; p != NULL ; p = p->next) {
	       if (strcmp(p->option->keyword->str, key) == 0)
		       return p;
	}
	return NULL;
}

static struct PrinterOptionChoice *option_find_choice(struct PrinterOption *o, const char *choice) {
	struct PrinterOptionChoice *c;

	for (c = o->choices ; c->choice != NULL ; c++) {
		if (strcmp(c->choice->choice->str, choice) == 0)
			return c;
	}
	return NULL;
}

static void addconstraint(PrinterOptions *po, const char *key1, const char *choice1, const char *key2,  const char *choice2) {
	struct PrinterOption *o1, *o2;
	struct PrinterOptionChoice *c1, *c2;
	struct PrinterConstraint *t;

	o1 = option_find(po, key1);
	o2 = option_find(po, key2);
	if (o1 == NULL || o2 == NULL || o1 == o2)
		return;
	c1 = option_find_choice(o1, choice1);
	c2 = option_find_choice(o2, choice2);
	if (c1 == NULL || c2 == NULL)
		return;
	t = g_malloc0(sizeof(struct PrinterConstraint));
	t->next = c1->constraints;
	t->option = o2;
	t->choice = c2;
	c1->constraints = t;
	t = g_malloc0(sizeof(struct PrinterConstraint));
	t->next = c2->constraints;
	t->option = o1;
	t->choice = c1;
	c2->constraints = t;
	if (o1->selected_index == c1 - o1->choices)
		c2->forbidden++;
	if (o2->selected_index == c2 - o2->choices)
		c1->forbidden++;
}

PrinterOptions* printer_options_new(const char *filename) {
	PpdFile *ppd;
	PrinterOptions *r;
	GSList *gl, *cl;
	struct PrinterOption **next_option, **next_installable;

  	ppd = ppd_file_new(filename);
	if (ppd == NULL)
		return NULL;
	r = g_object_new(TYPE_PRINTER_OPTIONS, NULL);
	r->ppd = ppd;
	r->options = NULL;
	r->installable = NULL;
	ppd_mark_defaults(r->ppd);
	g_object_ref(G_OBJECT(r));

	next_option = &r->options;
	next_installable = &r->installable;

	for (gl = r->ppd->groups ; gl != NULL ; gl = g_slist_next(gl)) {
		PpdGroup *group = gl->data;
		const char *name = group->text->str;

		if (name != NULL) {
			if (strcasecmp(name, "Options Installed") == 0)
				group_add(&next_installable, group);
			else if (strcasecmp(name, "InstallableOptions") == 0)
				group_add(&next_installable, group);
			else if (strcasecmp(name, "Installable Options") == 0)
				group_add(&next_installable, group);
			else if (strcasecmp(name, "Installed Options") == 0)
				group_add(&next_installable, group);
			else
				group_add(&next_option, group);
		} else
				group_add(&next_installable, group);
	}
	for (cl = r->ppd->consts ; cl != NULL ; cl = g_slist_next(cl)) {
		PpdConstraint *c = cl->data;

		if (c->option1 == NULL || c->choice1 == NULL ||
				c->option2 == NULL || c->choice2 == NULL)
			continue;
		addconstraint(r, c->option1->str, c->choice1->str,
				c->option2->str, c->choice2->str);
	}
	return r;
}

PpdFile *printer_options_get_handle(PrinterOptions *po) {
	return po->ppd;
}

void printer_options_select(PrinterOptions *po, const char *key, const char *value) {
	PpdOption *o;
	PpdChoice *c;
	struct PrinterOption *p;

	o = ppd_find_option_by_keyword(po->ppd, key);
	if (o == NULL)
		return;
	p = po->options;
	while (p != NULL && p->option != o)
		p = p->next;
	if (p == NULL) {
		p = po->installable;
		while (p != NULL && p->option != o)
			p = p->next;
		if (p == NULL)
			return;
	}
	c = ppd_find_choice(o, value);
	if (c == NULL)
		return;
	p->selected_index = g_slist_index(o->choices, c);
}

char *printer_options_concat(PrinterOptions *po, char *s, const char *prefix, const char *postfix) {
	struct PrinterOption *o;

	for (o = po->options ; o != NULL ; o = o->next) {
		if (o->selected_index < 0)
			continue;

		s = g_strconcat(s, prefix,
				o->option->keyword->str,
				":",
				o->choices[o->selected_index].choice->choice->str,
				postfix,
				(char*)NULL);
	}
	return s;
}

char *printer_option_select(struct PrinterOption *o, int s) {
	struct PrinterConstraint *t;
	struct PrinterOptionChoice *c;
	char *message = NULL;

	if (o->selected_index >= 0) {
		c = &o->choices[o->selected_index];
		for (t = c->constraints ; t != NULL ; t = t->next ) {
			t->choice->forbidden--;
		}
	}

	if (s < 0 || s >= o->count)
		o->selected_index = -1;
	else {
		o->selected_index = s;
		c = &o->choices[o->selected_index];
		for (t = c->constraints ; t != NULL ; t = t->next ) {
			struct PrinterOption *co = t->option;
			struct PrinterOptionChoice *cc = t->choice;
			cc->forbidden++;
			if (co->selected_index == cc - co->choices) {
				char *n;
				n = g_strconcat(message?message:"",
						"Option \"",
						o->option->text->str,
						"\":\"",
						c->choice->text->str,
						"\"\n conflicts with option\n\"",
						co->option->text->str,
						"\":\"",
						cc->choice->text->str,
						"\"\n", NULL);
				g_free(message);
				message = n;
			}
		}
	}
	return message;
}
