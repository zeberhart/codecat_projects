extern	unsigned long	cr3tab[] ;

#define UPDC32(b, c) (cr3tab[((int)c ^ b) & 0xff] ^ ((c >> 8) & 0x00FFFFFF))

/* To generate a CRC32:
 *	u_long	crc = 0xFFFFFFFF ;
 *	for each byte in input:
 *	  crc = UPDC32(b, crc) ;
 *	crc = ~crc ;
 *	transmit LSB first
 *
 * To verify a CRC:
 *	u_long	crc = 0xFFFFFFFF ;
 *	for each byte in input, and four CRC bytes:
 *	  crc = UPDC32(b, crc) ;
 *	verify crc == 0xDEBB20E3
 */
