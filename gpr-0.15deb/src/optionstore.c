/* Copyright 2011 Bernhard R. Link
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 */

#include "config.h"
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gtk/gtktreemodel.h>
#include <gtk/gtkcombobox.h>
#include <gtk/gtksignal.h>
#include "printeroptions.h"
#include "optionstore.h"

struct _OptionStore {
	GObject parent;
	PrinterOptions *options;
	struct PrinterOption *option;
	void (*constraint_callback)(char *);
};
struct _OptionStoreClass {
	GObjectClass parent_class;
};

static void option_store_finalize(GObject *);
static void option_store_class_init(OptionStoreClass *);

static GtkTreeModelFlags option_store_get_flags(GtkTreeModel *m) {
	return GTK_TREE_MODEL_LIST_ONLY | GTK_TREE_MODEL_ITERS_PERSIST;
}

static int option_store_get_n_columns(GtkTreeModel *m) {
	return 1;
}
static GType option_store_get_column_type(GtkTreeModel *m, int index) {
	return G_TYPE_STRING;
}
static gboolean option_store_get_iter(GtkTreeModel *tm, GtkTreeIter *iter, GtkTreePath *path) {
	OptionStore *os = (OptionStore*)tm;
	int i = gtk_tree_path_get_indices (path)[0];

	iter->stamp = 0;
	iter->user_data = NULL;
	if (os->option == NULL)
		return FALSE;
	iter->stamp = 17;
	iter->user_data = GINT_TO_POINTER(i+1);
	return TRUE;
}

static GtkTreePath *option_store_get_path(GtkTreeModel *tm, GtkTreeIter *iter) {
	OptionStore *os = (OptionStore*)tm;
	int i;

	if (iter->stamp != 17 || os->option == NULL)
		return NULL;
	i = GPOINTER_TO_INT(iter->user_data);
	if (i <= 0)
		return NULL;
	return gtk_tree_path_new_from_indices (i-1, -1);
}

static void option_store_get_value(GtkTreeModel *tm, GtkTreeIter *iter, int column, GValue *v) {
	OptionStore *os = (OptionStore*)tm;
	PpdChoice *choice_ptr;
	int i;

	g_value_init(v, G_TYPE_STRING);
	if (column != 0)
		return;
	if (iter->stamp != 17 || os->option == NULL)
		return;
	i = GPOINTER_TO_INT(iter->user_data) - 1;
	if (i < 0 || i >= os->option->count)
		return;
	choice_ptr = g_slist_nth_data(os->option->option->choices, i);
	g_value_set_string(v, choice_ptr->text->str);
}

static gboolean option_store_iter_next(GtkTreeModel *tm, GtkTreeIter *iter) {
	OptionStore *os = (OptionStore*)tm;
	int i;

	if (os->option == NULL || iter->stamp != 17)
		return FALSE;
	i = GPOINTER_TO_INT(iter->user_data);
	if (i <= 0 || i > os->option->count) {
		iter->stamp = 0;
		iter->user_data = GINT_TO_POINTER(0);
		return FALSE;
	}
	iter->user_data = GINT_TO_POINTER(i+1);
	return TRUE;
}

static gboolean option_store_iter_children(GtkTreeModel *tm, GtkTreeIter *iter, GtkTreeIter *parent) {
	OptionStore *os = (OptionStore*)tm;

	if (parent != NULL)
		return FALSE;
	if (os->option == NULL)
		return FALSE;
	iter->stamp = 17;
	iter->user_data = GINT_TO_POINTER(1);
	return FALSE;
}

static gboolean option_store_iter_has_child(GtkTreeModel *tm, GtkTreeIter *iter) {
	return FALSE;
}
static int option_store_iter_n_children(GtkTreeModel *tm, GtkTreeIter *iter) {
	OptionStore *os = (OptionStore*)tm;

	if (iter != NULL || os->option == NULL )
		return 0;
	else
		return os->option->count;
}
static int option_store_iter_nth_child(GtkTreeModel *tm, GtkTreeIter *iter, GtkTreeIter *parent, int n) {
	OptionStore *os = (OptionStore*)tm;

	if (parent != NULL || os->option == NULL)
		return FALSE;
	if (n < 0 || n >= os->option->count)
		return FALSE;
	iter->user_data = GINT_TO_POINTER(n+1);
	iter->stamp = 17;
	return TRUE;
}
static int option_store_iter_parent(GtkTreeModel *tm, GtkTreeIter *iter, GtkTreeIter *child) {
	return FALSE;
}

static void option_store_tree_model_init(GtkTreeModelIface *iface) {
	iface->get_flags = option_store_get_flags;
	iface->get_n_columns = option_store_get_n_columns;
	iface->get_column_type = option_store_get_column_type;
	iface->get_iter = option_store_get_iter;
	iface->get_path = option_store_get_path;
	iface->get_value = option_store_get_value;
	iface->iter_next = option_store_iter_next;
	iface->iter_children = option_store_iter_children;
	iface->iter_has_child = option_store_iter_has_child;
	iface->iter_n_children = option_store_iter_n_children;
	iface->iter_nth_child = option_store_iter_nth_child;
	iface->iter_parent = option_store_iter_parent;
}

static void option_store_init(OptionStore *store) {
}

G_DEFINE_TYPE_WITH_CODE(OptionStore, option_store, G_TYPE_OBJECT,
			G_IMPLEMENT_INTERFACE(GTK_TYPE_TREE_MODEL,
				option_store_tree_model_init))

static void option_store_finalize(GObject *o) {
	g_object_unref(((OptionStore*)o)->options);
	G_OBJECT_CLASS (option_store_parent_class)->finalize (o);
}

static void option_store_class_init(OptionStoreClass *class) {
	GObjectClass *o = (GObjectClass*) class;
	o->finalize = option_store_finalize;
}

GtkTreeModel * option_store_new(PrinterOptions *po, struct PrinterOption *o, void (*callback)(char *)) {
	OptionStore *os = g_object_new(TYPE_OPTION_STORE, NULL);

	os->options = g_object_ref(po);
	os->option = o;
	os->constraint_callback = callback;
	return GTK_TREE_MODEL(os);
}

int option_store_get_default(OptionStore *os) {
	return 0;
}

static void change_selection(GtkWidget *widget, void *privdata) {
	GtkComboBox *combobox = GTK_COMBO_BOX(widget);
	OptionStore *os = OPTION_STORE(gtk_combo_box_get_model(combobox));
	int i = gtk_combo_box_get_active(combobox);
	struct PrinterOption *o = os->option;
	char *message;

	message = printer_option_select(o, i);
	if (message != NULL) {
		if (os->constraint_callback != NULL)
			os->constraint_callback(message);
		else
			g_free(message);
	}
}

void combo_box_mode_set(GtkWidget *combobox, PrinterOptions *options, struct PrinterOption *option, void (*callback)(char *)) {
	GtkComboBox *cb = GTK_COMBO_BOX(combobox);
  	GtkTreeModel *m = option_store_new(options, option, callback);

	gtk_combo_box_set_model(cb, m);
	gtk_combo_box_set_active(cb,
		  OPTION_STORE(m)->option->selected_index);
	gtk_signal_connect(GTK_OBJECT(combobox), "changed",
		GTK_SIGNAL_FUNC(change_selection), NULL);
}
