#include <regex.h>

typedef struct tTab {
  char *title;
  char **patterns;
  regex_t **regexp;
  char **strings;
} tTab, *ptrTab;

/*-------------------------------------------------------------------
  Parse the xml file containing tab information.  Use this to build 
  a list of tTab structs that we'll use for matching later on.
-------------------------------------------------------------------*/
ptrTab * tab_parse_xml_file (char *filename);

/*-------------------------------------------------------------------
  Match the given description string to the list of strings and
  regexp patterns.  If there's a match, return the tab's title
  string.
-------------------------------------------------------------------*/
char * tab_match (char *desc, ptrTab *tablist);

/*-------------------------------------------------------------------
  Free a list of tTab structures, then free the list itself.
-------------------------------------------------------------------*/
void tab_free_all (ptrTab * tabs);

/*-------------------------------------------------------------------
  Print out data from a tTab struct to stdout.
-------------------------------------------------------------------*/
void tab_print (ptrTab tab);
