#line 213 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_UNDEFINED, "push UNDEFINED", I_UPDATE_SP)




OPCODE0(F_CONST0, "push 0", I_UPDATE_SP)



OPCODE0(F_CONST1, "push 1", I_UPDATE_SP)




OPCODE0(F_MARK_AND_CONST0, "mark & 0", I_UPDATE_SP|I_UPDATE_M_SP)




OPCODE0(F_MARK_AND_CONST1, "mark & 1", I_UPDATE_SP|I_UPDATE_M_SP)




OPCODE0(F_CONST_1, "push -1", I_UPDATE_SP)



OPCODE0(F_BIGNUM, "push 0x7fffffff", I_UPDATE_SP)



OPCODE1(F_NUMBER, "push int", I_UPDATE_SP)





OPCODE2(F_NUMBER64, "push 64-bit int", I_UPDATE_SP)
#line 262 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_NEG_NUMBER, "push -int", I_UPDATE_SP)



OPCODE1(F_CONSTANT, "constant", I_UPDATE_SP)
#line 275 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE2(F_REARRANGE, "rearrange", 0)
#line 284 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_TAIL(F_MARK_AND_STRING, "mark & string", I_UPDATE_SP|I_UPDATE_M_SP)OPCODE1(F_STRING, "string", I_UPDATE_SP)
#line 297 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_ARROW_STRING, "->string", I_UPDATE_SP)







OPCODE1(F_LOOKUP_LFUN, "->lfun", 0)
#line 338 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_LFUN, "local function", I_UPDATE_SP)





OPCODE2(F_TRAMPOLINE, "trampoline", I_UPDATE_SP)
#line 367 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_TAIL(F_MARK_AND_GLOBAL, "mark & global", I_UPDATE_SP|I_UPDATE_M_SP)OPCODE1(F_GLOBAL, "global", I_UPDATE_SP)
#line 377 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE2_TAIL(F_MARK_AND_EXTERNAL, "mark & external", I_UPDATE_SP|I_UPDATE_M_SP)OPCODE2(F_EXTERNAL, "external", I_UPDATE_SP)
#line 409 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE2(F_EXTERNAL_LVALUE, "& external", I_UPDATE_SP)
#line 431 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_MARK_AND_LOCAL, "mark & local", I_UPDATE_SP|I_UPDATE_M_SP)





OPCODE1(F_LOCAL, "local", I_UPDATE_SP)




OPCODE2(F_2_LOCALS, "2 locals", I_UPDATE_SP)






OPCODE2(F_LOCAL_2_LOCAL, "local = local", 0)



OPCODE2(F_LOCAL_2_GLOBAL, "global = local", 0)





OPCODE2(F_GLOBAL_2_LOCAL, "local = global", 0)





OPCODE1(F_LOCAL_LVALUE, "& local", I_UPDATE_SP)






OPCODE2(F_LEXICAL_LOCAL, "lexical local", I_UPDATE_SP)
#line 483 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE2(F_LEXICAL_LOCAL_LVALUE, "&lexical local", I_UPDATE_SP)
#line 496 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_ARRAY_LVALUE, "[ lvalues ]", I_UPDATE_SP)
#line 506 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_CLEAR_2_LOCAL, "clear 2 local", 0)
#line 516 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_CLEAR_4_LOCAL, "clear 4 local", 0)
#line 527 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_CLEAR_LOCAL, "clear local", 0)






OPCODE1(F_INC_LOCAL, "++local", I_UPDATE_SP)
#line 551 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_POST_INC_LOCAL, "local++", I_UPDATE_SP)
#line 570 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_INC_LOCAL_AND_POP, "++local and pop", 0)
#line 587 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_DEC_LOCAL, "--local", I_UPDATE_SP)
#line 604 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_POST_DEC_LOCAL, "local--", I_UPDATE_SP)
#line 623 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_DEC_LOCAL_AND_POP, "--local and pop", 0)
#line 644 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_LTOSVAL, "lvalue to svalue", I_UPDATE_SP)
#line 675 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_LTOSVAL2_AND_FREE, "ltosval2 and free", I_UPDATE_SP)
#line 700 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_LTOSVAL3_AND_FREE, "ltosval3 and free", I_UPDATE_SP)
#line 733 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_LTOSVAL_AND_FREE, "ltosval and free", I_UPDATE_SP)
#line 753 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_ADD_TO, "+=", I_UPDATE_SP)
#line 822 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_ADD_TO_AND_POP, "+= and pop", I_UPDATE_SP)
#line 889 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_GLOBAL_LVALUE, "& global", I_UPDATE_SP)




OPCODE0(F_INC, "++x", I_UPDATE_SP)
#line 914 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_DEC, "--x", I_UPDATE_SP)
#line 934 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_DEC_AND_POP, "x-- and pop", I_UPDATE_SP)
#line 953 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_INC_AND_POP, "x++ and pop", I_UPDATE_SP)
#line 972 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_POST_INC, "x++", I_UPDATE_SP)
#line 995 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_POST_DEC, "x--", I_UPDATE_SP)
#line 1018 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_ASSIGN_LOCAL, "assign local", 0)



OPCODE0(F_ASSIGN, "assign", I_UPDATE_SP)







OPCODE2(F_APPLY_ASSIGN_LOCAL_AND_POP, "apply, assign local and pop", I_UPDATE_SP|I_UPDATE_M_SP)







OPCODE2(F_APPLY_ASSIGN_LOCAL, "apply, assign local", I_UPDATE_ALL)





OPCODE0(F_ASSIGN_AND_POP, "assign and pop", I_UPDATE_SP)




OPCODE1(F_ASSIGN_LOCAL_AND_POP, "assign local and pop", I_UPDATE_SP)





OPCODE1(F_ASSIGN_GLOBAL, "assign global", 0)





OPCODE1(F_ASSIGN_GLOBAL_AND_POP, "assign global and pop", I_UPDATE_SP)
#line 1071 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_POP_VALUE, "pop", I_UPDATE_SP)



OPCODE1(F_POP_N_ELEMS, "pop_n_elems", I_UPDATE_SP)



OPCODE0_TAIL(F_MARK2, "mark mark", I_UPDATE_M_SP)OPCODE0_TAIL(F_SYNCH_MARK, "synch mark", I_UPDATE_M_SP)OPCODE0(F_MARK, "mark", I_UPDATE_M_SP)
#line 1092 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_MARK_X, "mark Pike_sp-X", I_UPDATE_M_SP)



OPCODE0(F_POP_MARK, "pop mark", I_UPDATE_M_SP)



OPCODE0(F_POP_TO_MARK, "pop to mark", I_UPDATE_SP|I_UPDATE_M_SP)






OPCODE0_TAIL(F_CLEANUP_SYNCH_MARK, "cleanup synch mark", I_UPDATE_SP|I_UPDATE_M_SP)OPCODE0(F_POP_SYNCH_MARK, "pop synch mark", I_UPDATE_SP|I_UPDATE_M_SP)
#line 1127 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_CLEAR_STRING_SUBTYPE, "clear string subtype", 0)




OPCODE0_BRANCH(F_BRANCH, "branch", 0)



OPCODE2_BRANCH(F_BRANCH_IF_NOT_LOCAL_ARROW, "branch if !local->x", 0)OPCODE0_TAILBRANCH(F_BRANCH_WHEN_ZERO, "branch if zero", I_UPDATE_SP)
#line 1161 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_BRANCH(F_BRANCH_WHEN_NON_ZERO, "branch if not zero", I_UPDATE_SP)
#line 1172 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_BRANCH(F_BRANCH_IF_TYPE_IS_NOT, "branch if type is !=", I_UPDATE_SP)
#line 1200 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_BRANCH(F_BRANCH_IF_LOCAL, "branch if local", 0)
#line 1210 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_BRANCH(F_BRANCH_IF_NOT_LOCAL, "branch if !local", 0)
#line 1231 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_BRANCH(F_BRANCH_WHEN_EQ, "branch if ==", I_UPDATE_SP)

OPCODE0_BRANCH(F_BRANCH_WHEN_NE, "branch if !=", I_UPDATE_SP)

OPCODE0_BRANCH(F_BRANCH_WHEN_LT, "branch if <", I_UPDATE_SP)

OPCODE0_BRANCH(F_BRANCH_WHEN_LE, "branch if <=", I_UPDATE_SP)

OPCODE0_BRANCH(F_BRANCH_WHEN_GT, "branch if >", I_UPDATE_SP)

OPCODE0_BRANCH(F_BRANCH_WHEN_GE, "branch if >=", I_UPDATE_SP)

OPCODE0_BRANCH(F_BRANCH_AND_POP_WHEN_ZERO, "branch & pop if zero", 0)
#line 1249 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_BRANCH(F_BRANCH_AND_POP_WHEN_NON_ZERO, "branch & pop if !zero", 0)
#line 1260 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_BRANCH(F_LAND, "&&", I_UPDATE_SP)
#line 1273 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_BRANCH(F_LOR, "||", I_UPDATE_SP)
#line 1284 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_BRANCH(F_EQ_OR, "==||", I_UPDATE_SP)
#line 1297 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_BRANCH(F_EQ_AND, "==&&", I_UPDATE_SP)
#line 1317 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_PTRJUMP(F_CATCH, "catch", I_UPDATE_ALL)
#line 1434 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_ESCAPE_CATCH, "escape catch", 0)



OPCODE0(F_EXIT_CATCH, "exit catch", I_UPDATE_SP)




OPCODE1_JUMP(F_SWITCH, "switch", I_UPDATE_ALL)
#line 1458 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_JUMP(F_SWITCH_ON_INDEX, "switch on index", I_UPDATE_ALL)
#line 1477 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE2_JUMP(F_SWITCH_ON_LOCAL, "switch on local", 0)
#line 1530 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_BRANCH(F_INC_LOOP, "++Loop", 0)

OPCODE0_BRANCH(F_DEC_LOOP, "--Loop", 0)

OPCODE0_BRANCH(F_INC_NEQ_LOOP, "++Loop!=", 0)

OPCODE0_BRANCH(F_DEC_NEQ_LOOP, "--Loop!=", 0)
#line 1546 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_BRANCH(F_LOOP, "loop", I_UPDATE_SP)
#line 1559 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_BRANCH(F_FOREACH, "foreach", 0)
#line 1580 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_MAKE_ITERATOR, "get_iterator", 0)




OPCODE0_BRANCH(F_FOREACH_START, "foreach start", 0)
#line 1599 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_BRANCH(F_FOREACH_LOOP, "foreach loop", 0)
#line 1614 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_RETURN(F_RETURN_LOCAL, "return local", I_UPDATE_SP|I_UPDATE_FP)
#line 1633 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_RETURN(F_RETURN_IF_TRUE, "return if true", I_UPDATE_SP|I_UPDATE_FP)





OPCODE0_RETURN(F_RETURN_1, "return 1", I_UPDATE_SP|I_UPDATE_FP)




OPCODE0_RETURN(F_RETURN_0, "return 0", I_UPDATE_SP|I_UPDATE_FP)




OPCODE0_RETURN(F_RETURN, "return", I_UPDATE_FP)



OPCODE0_RETURN(F_DUMB_RETURN, "dumb return", I_UPDATE_FP)



OPCODE0(F_NEGATE, "unary minus", 0)
#line 1681 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_ALIAS(F_COMPL, "~", 0, o_compl)

OPCODE0(F_NOT, "!", 0)
#line 1712 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_ALIAS(F_LSH, "<<", I_UPDATE_SP, o_lsh)

OPCODE0_ALIAS(F_RSH, ">>", I_UPDATE_SP, o_rsh)
#line 1722 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_EQ, "==", I_UPDATE_SP)

OPCODE0(F_NE, "!=", I_UPDATE_SP)

OPCODE0(F_GT, ">", I_UPDATE_SP)

OPCODE0(F_GE, ">=", I_UPDATE_SP)

OPCODE0(F_LT, "<", I_UPDATE_SP)

OPCODE0(F_LE, "<=", I_UPDATE_SP)


OPCODE0(F_ADD, "+", I_UPDATE_SP)




OPCODE0(F_ADD_INTS, "int+int", I_UPDATE_SP)
#line 1752 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_ADD_FLOATS, "float+float", I_UPDATE_SP)
#line 1764 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_ALIAS(F_SUBTRACT, "-", I_UPDATE_SP, o_subtract)

OPCODE0_ALIAS(F_AND, "&", I_UPDATE_SP, o_and)

OPCODE0_ALIAS(F_OR, "|", I_UPDATE_SP, o_or)

OPCODE0_ALIAS(F_XOR, "^", I_UPDATE_SP, o_xor)

OPCODE0_ALIAS(F_MULTIPLY, "*", I_UPDATE_SP, o_multiply)

OPCODE0_ALIAS(F_DIVIDE, "/", I_UPDATE_SP, o_divide)

OPCODE0_ALIAS(F_MOD, "%", I_UPDATE_SP, o_mod)

OPCODE1(F_ADD_INT, "add integer", 0)
#line 1787 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_ADD_NEG_INT, "add -integer", 0)
#line 1802 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_PUSH_ARRAY, "@", I_UPDATE_SP)
#line 1833 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_APPEND_ARRAY, "append array", I_UPDATE_SP|I_UPDATE_M_SP)



OPCODE2(F_LOCAL_LOCAL_INDEX, "local[local]", I_UPDATE_SP)







OPCODE1(F_LOCAL_INDEX, "local index", 0)
#line 1855 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE2(F_GLOBAL_LOCAL_INDEX, "global[local]", I_UPDATE_SP)
#line 1867 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE2(F_LOCAL_ARROW, "local->x", I_UPDATE_SP)
#line 1877 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_ARROW, "->x", 0)
#line 1889 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_STRING_INDEX, "string index", 0)
#line 1901 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_POS_INT_INDEX, "int index", 0)





OPCODE1(F_NEG_INT_INDEX, "-int index", 0)





OPCODE0(F_INDEX, "index", I_UPDATE_SP)



OPCODE2(F_MAGIC_INDEX, "::`[]", I_UPDATE_SP)



OPCODE2(F_MAGIC_SET_INDEX, "::`[]=", I_UPDATE_SP)



OPCODE2(F_MAGIC_INDICES, "::_indices", I_UPDATE_SP)



OPCODE2(F_MAGIC_VALUES, "::_values", I_UPDATE_SP)



OPCODE0_ALIAS(F_CAST, "cast", I_UPDATE_SP, f_cast)

OPCODE0_ALIAS(F_CAST_TO_INT, "cast_to_int", 0, o_cast_to_int)

OPCODE0_ALIAS(F_CAST_TO_STRING, "cast_to_string", 0, o_cast_to_string)

OPCODE0(F_SOFT_CAST, "soft cast", I_UPDATE_SP)
#line 1959 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_ALIAS(F_RANGE, "range", I_UPDATE_SP, o_range2)

OPCODE0(F_COPY_VALUE, "copy_value", 0)







OPCODE0(F_INDIRECT, "indirect", I_UPDATE_SP)
#line 1991 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_SIZEOF, "sizeof", 0)





OPCODE1(F_SIZEOF_LOCAL, "sizeof local", I_UPDATE_SP)



OPCODE2_ALIAS(F_SSCANF, "sscanf", I_UPDATE_SP, o_sscanf)
#line 2097 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_JUMP(F_CALL_LFUN, "call lfun", I_UPDATE_ALL)OPCODE1_JUMP(F_CALL_LFUN_AND_POP, "call lfun" " & pop", I_UPDATE_ALL)OPCODE1_RETURN(F_CALL_LFUN_AND_RETURN, "call lfun" " & return", I_UPDATE_ALL)OPCODE1_JUMP(F_MARK_CALL_LFUN, "mark, " "call lfun", I_UPDATE_ALL)OPCODE1_JUMP(F_MARK_CALL_LFUN_AND_POP, "mark, " "call lfun" " & pop", I_UPDATE_ALL)OPCODE1_RETURN(F_MARK_CALL_LFUN_AND_RETURN, "mark, " "call lfun" " & return", I_UPDATE_ALL)



OPCODE1_JUMP(F_APPLY, "apply", I_UPDATE_ALL)OPCODE1_JUMP(F_APPLY_AND_POP, "apply" " & pop", I_UPDATE_ALL)OPCODE1_RETURN(F_APPLY_AND_RETURN, "apply" " & return", I_UPDATE_ALL)OPCODE1_JUMP(F_MARK_APPLY, "mark, " "apply", I_UPDATE_ALL)OPCODE1_JUMP(F_MARK_APPLY_AND_POP, "mark, " "apply" " & pop", I_UPDATE_ALL)OPCODE1_RETURN(F_MARK_APPLY_AND_RETURN, "mark, " "apply" " & return", I_UPDATE_ALL)


OPCODE0_JUMP(F_CALL_FUNCTION, "call function", I_UPDATE_ALL)OPCODE0_JUMP(F_CALL_FUNCTION_AND_POP, "call function" " & pop", I_UPDATE_ALL)OPCODE0_RETURN(F_CALL_FUNCTION_AND_RETURN, "call function" " & return", I_UPDATE_ALL)

OPCODE1_JUMP(F_CALL_OTHER, "call other", I_UPDATE_ALL)
#line 2164 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_JUMP(F_CALL_OTHER_AND_POP, "call other & pop", I_UPDATE_ALL)
#line 2225 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_JUMP(F_CALL_OTHER_AND_RETURN, "call other & return", I_UPDATE_ALL)
#line 2337 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_CALL_BUILTIN, "call builtin", I_UPDATE_ALL)




OPCODE1(F_CALL_BUILTIN_AND_POP, "call builtin & pop", I_UPDATE_ALL)





OPCODE1_RETURN(F_CALL_BUILTIN_AND_RETURN, "call builtin & return", I_UPDATE_ALL)






OPCODE1(F_MARK_CALL_BUILTIN, "mark, call builtin", I_UPDATE_ALL)




OPCODE1(F_MARK_CALL_BUILTIN_AND_POP, "mark, call builtin & pop", 0)





OPCODE1_RETURN(F_MARK_CALL_BUILTIN_AND_RETURN, "mark, call builtin & return", I_UPDATE_ALL)






OPCODE1(F_CALL_BUILTIN1, "call builtin 1", I_UPDATE_ALL)




OPCODE1(F_CALL_BUILTIN1_AND_POP, "call builtin1 & pop", I_UPDATE_ALL)





OPCODE1(F_LTOSVAL_CALL_BUILTIN_AND_ASSIGN, "ltosval, call builtin & assign", I_UPDATE_ALL)
#line 2432 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_LTOSVAL_CALL_BUILTIN_AND_ASSIGN_POP, "ltosval, call builtin, assign & pop", I_UPDATE_ALL)
#line 2560 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1_PTRJUMP(F_COND_RECUR, "recur if not overloaded", I_UPDATE_ALL)OPCODE0_TAILPTRJUMP(F_RECUR, "recur", I_UPDATE_ALL)
#line 2618 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0_PTRJUMP(F_RECUR_AND_POP, "recur & pop", I_UPDATE_ALL)






OPCODE0_PTRJUMP(F_TAIL_RECUR, "tail recursion", I_UPDATE_ALL)
#line 2670 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE1(F_THIS_OBJECT, "this_object", I_UPDATE_SP)
#line 2688 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_ZERO_TYPE, "zero_type", 0)
#line 2706 "/Users/hww3/pikebuild/src/interpret_functions.h"
OPCODE0(F_SWAP, "swap", 0)



OPCODE0(F_DUP, "dup", I_UPDATE_SP)



OPCODE2(F_THIS, "this", I_UPDATE_SP)
