/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "syr_usertype.h"
#include "syr_abs.h"  
#include "syr_bd.h" 
#include "syr_tree.h" 
#include "syr_option.h"
#include "syr_parall.h"
#include "syr_proto.h"
#include "syr_const.h"

/* #include "mpi.h" */

extern struct Performances perfo;
extern struct Affichages affich;

extern char nomdata[CHLONG],nomgeomr[CHLONG];
extern FILE *fdata;
extern  double c2,tsoleil;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */

void lire_donnees_ray(struct Maillage maillnodes,struct Maillage *maillnodray,
		      struct GestionFichiers *gestionfichiers,
		      struct Bande *bandespec,struct PropInfini *propinf,
		      struct Mask *mask,struct Soleil *soleil,
		      struct Horizon *horiz,struct Vitre *vitre,
		      struct SDparall_ray *sdparall_ray)
{
  int l;


  /* ?????????????? */
/*   switch (verif_type_maillage(nomgeomr)) */
/*     { */
/*     case 1 : lire_simail_ray (maillnodray); break; */
/*     case 2 : lire_ideas_ray  (maillnodray); break; */
/*     case 3 : lire_syrthes_ray(maillnodray); break; */
/*     } */


  /* verification des mots-cles */
  verif_RAYT_data();

  verif_type_maillage(nomgeomr);
  lire_syrthes_ray(maillnodray);
  maillnodray->iaxisy=maillnodes.iaxisy;
  
  lire_ray_mc(gestionfichiers,propinf,bandespec,soleil,vitre);
  
  extraitmask(maillnodray,mask,horiz);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture de syrthes.env                                        |
  |======================================================================| */
void lire_ray_mc(struct GestionFichiers *gestionfichiers,struct PropInfini *propinf,
		 struct Bande *bandespec,struct Soleil *soleil,
		 struct Vitre *vitre)
{
  char ch[CHLONG],*motcle,*repc;
  int i1,i2,ok=1,ii,n;
  double *dlist;

  /* initialisations */
  soleil->actif=0;
  ndecoup_max=0;
  propinf->actif=0;
  bandespec->nb=1;
  vitre->actif=0; vitre->prop_glob=1;

  gestionfichiers->stock_corr_sr=0;  gestionfichiers->lec_corr_sr=0; /* option supprimee */

  gestionfichiers->stock_fdf=1;      gestionfichiers->lec_fdf=0;

  gestionfichiers->resu_r=0;

  motcle=(char*)malloc(CHLONG*sizeof(char));   verif_alloue_char("lire_ray_mc",motcle);
  repc=(char*)malloc(CHLONG*sizeof(char));     verif_alloue_char("lire_ray_mc",repc);
  dlist=(double*)malloc(20*sizeof(double)); verif_alloue_double1d("lire_ray_mc",dlist);


  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  strcpy(repc,"");
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"ECRITURES OPTIONNELLES RAYONNEMENT")) 
	    {
	      rep_ch(repc,ch+i2+1);
	      if (!strcmp(repc,"OUI")) gestionfichiers->resu_r=1;
	      else if (!strcmp(repc,"NON")) gestionfichiers->resu_r=0;
	      else ok=0;
	    }
	  else if (!strcmp(motcle,"PRISE EN COMPTE DU RAYONNEMENT SOLAIRE")) 
	    {
	      rep_ch(repc,ch+i2+1);
	      if (!strcmp(repc,"OUI")) soleil->actif=1;
	      else if (!strcmp(repc,"NON")) soleil->actif=0;
	      else ok=0;
	    }
	  else if (!strcmp(motcle,"NOMBRE DE REDECOUPAGES POUR CALCUL DES FACTEURS DE FORME")) 
	    {
	      ndecoup_max=rep_int(ch+i2+1);
	    }
	  else if (!strcmp(motcle,"NOMBRE DE BANDES SPECTRALES POUR LE RAYONNEMENT")) 
	    {
	      bandespec->nb=rep_int(ch+i2+1);
	    }
	  else if (!strcmp(motcle,"DOMAINE DE RAYONNEMENT CONFINE OUVERT SUR L EXTERIEUR")) 
	    {
	      rep_ch(repc,ch+i2+1);
	      if (!strcmp(repc,"OUI")) propinf->actif=1;
	      else if (!strcmp(repc,"NON")) propinf->actif=0;
	      else ok=0;
	    }
	  else if (!strcmp(motcle,"LECTURE DES FACTEURS DE FORME SUR FICHIER")) 
	    {
	      rep_ch(repc,ch+i2+1);
	      if (!strcmp(repc,"OUI")) {gestionfichiers->lec_fdf=1; gestionfichiers->stock_fdf=0;}
	      else if (!strcmp(repc,"NON")) gestionfichiers->lec_fdf=0;
	      else ok=0;
	    }
/* 	  else if (!strcmp(motcle,"LECTURE DES CORRESPONDANTS POUR RAYONNEMENT"))  */
/* 	    { */
/* 	      rep_ch(repc,ch+i2+1); */
/* 	      if (!strcmp(repc,"OUI")) {gestionfichiers->lec_corr_sr=1; gestionfichiers->stock_corr_sr=0;} */
/* 	      else if (!strcmp(repc,"NON")) gestionfichiers->lec_corr_sr=0; */
/* 	      else ok=0; */
/* 	    } */
	  else if (!strcmp(motcle,"PROPRIETES PHYSIQUES GLOBALES POUR LES VITRAGES")) 
	    {
	      rep_ch(repc,ch+i2+1);
	      if (!strcmp(repc,"OUI")) vitre->prop_glob=1;
	      else if (!strcmp(repc,"NON")) vitre->prop_glob=0;
	      else ok=0;
	    }
	}
    }
  free(motcle); free(dlist); free(repc);


  if (syrglob_nparts==1 || syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      {
	printf("\n *** RAYONNEMENT : RECAPITULATIF DES OPTIONS DU CALCUL\n");
	if (gestionfichiers->resu_r==1)	printf("      - Ecriture des fichiers optionnels : OUI\n");
	else	                        printf("      - Ecriture des fichiers optionnels : NON\n");

	printf("      - Nombre de bandes spectrales : %d\n",bandespec->nb);
	if (propinf->actif) printf("      - le domaine de rayonnement est ouvert\n");
	else printf("      - le domaine de rayonnement est ferme\n");
	if (gestionfichiers->stock_fdf) printf("      - les facteurs de forme seront stockes sur fichier\n");
	else printf("      - les facteurs de forme ne seront pas stockes sur fichier\n");
	if (gestionfichiers->lec_fdf) printf("      - les facteurs de forme seront lus sur fichier\n");
	else printf("      - les facteurs de forme seront calcules\n");
	if (soleil->actif) printf("      - le rayonnement solaire est pris en compte\n");
	else printf("      - il n'y a pas de rayonnement solaire\n");
      }  
    else if (SYRTHES_LANG == EN)
      {
	printf("\n *** RADIATION : SUMMARY OF THE CALCULATION OPTIONS\n");
	if (gestionfichiers->resu_r==1)	  printf("      - Optional files output : YES\n");
	else 	                          printf("      - Optional files output : NO\n");
	printf("      - Number of splitting for the view factors calculations : %d\n",ndecoup_max);
	printf("      - Number of spectral bands : %d\n",bandespec->nb);
	if (propinf->actif) printf("      - The radiation is done with an open enclosure\n");
	else printf("      - The radiation domain is a closed space\n");
	if (gestionfichiers->stock_fdf) printf("      - View factors will be stored on a file\n");
	else printf("      - View factors are not stored on a file\n");
	if (gestionfichiers->lec_fdf) printf("      - View facyors will be read on a file\n");
	else printf("      - View factors will be calculated \n");
	if (soleil->actif) printf("      - Solar radiation is accounted for\n");
	else printf("      - Solar radiation is not accounted for\n");
      }  
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_divers_ray(struct Maillage maillnodray,
		     struct SymPer *raysymper,struct Compconnexe *volconnexe,
		     struct Bande bandespec,
		     struct PropInfini *propinf,
		     struct Mask *mask,struct Soleil *soleil,struct Lieu *lieu,
		     struct Bilan *bilansolaire,struct Horizon *horiz)
{
  int i,j,k,nn,*p,err=0;
  char ch[CHLONG],*motcle,*repc;
  int i1,i2,i3,i4,id,ok=1,ii,nb,nr,n;
  int *ilist;
  double val,*dlist,ff,w1,w2;
  char *nommoisFR[12]={"janvier","fevrier","mars","avril","mai","juin",
		       "juillet","aout","septembre","octobre","novembre","decembre"}; 
  char *nommoisGB[12]={"january","february","marsh","april","mai","june",
		       "july","august","september","october","november","december"}; 


  repc=(char*)malloc(CHLONG*sizeof(char));     verif_alloue_char("lire_divers_ray",repc);
  motcle=(char*)malloc(CHLONG*sizeof(char));   verif_alloue_char("lire_divers_ray",motcle);
  ilist=(int*)malloc(100*sizeof(int));      verif_alloue_int1d("lire_divers_ray",ilist);
  dlist=(double*)malloc(10*sizeof(double)); verif_alloue_double1d("lire_divers_ray",dlist);


  raysymper->nbsym=0;
  raysymper->nbper=0;
  raysymper->nbsection=0;
  volconnexe->nb=0;
  propinf->nb=0;

  if  (bandespec.nb==1) {bandespec.borneinf[0]=1.e-10; bandespec.bornesup[0]=10;}
  else for (i=0;i<bandespec.nb;i++) {bandespec.borneinf[i]=bandespec.bornesup[i]=-1;}

  if (mask->nelem)
    {
      mask->opacite=(double*)malloc(mask->nelem*sizeof(double)); 
      verif_alloue_double1d("lire_divers_ray",mask->opacite);
      for (i=0;i<mask->nelem;i++) mask->opacite[i]=1.;
      perfo.mem_ray+=mask->nelem*sizeof(double);
      if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
    }

  lieu->latituded=44; lieu->latitudem=35;
  lieu->longituded=4; lieu->longitudem=43;
  lieu->fuseau=0;  lieu->decalage=-1;
  lieu->heure=0;  lieu->minute=0; lieu->seconde=0;
  lieu->mois=1; lieu->jour=1; lieu->day=(int)((lieu->mois-1)*30.5+lieu->jour);
  lieu->xc=lieu->yc=lieu->zc=0; lieu->gama=0;
  soleil->A=0.88; soleil->B=0.26;
  bilansolaire->nb=0;
  soleil->anglect_actif=0; soleil->anglect=90.; soleil->azict=0.; 
  soleil->fluxct=(double*)malloc(bandespec.nb*sizeof(double));
  verif_alloue_double1d("lire_divers_ray",soleil->fluxct);
  perfo.mem_ray+=mask->nelem*sizeof(double);
  soleil->autorepartct=0;
  horiz->temp=20.; horiz->emissi=1.;
  lieu->vertical=1;lieu->hauteur=1;


  /* il y a besoin d'une premiere passe en cas solaire avec source constante :  */
  /* il faut savoir si la source est repartie automatiquement sur les bandes ou */
  /* si l'utilisateur fournit les flux su rchacune des bandes                   */

  fseek(fdata,0,SEEK_SET);
  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"SOLAIRE")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
	      if (!strcmp(motcle,"SOURCE_REPART_AUTO_SUR_LES_BANDES")) 
		{
		  rep_ch(repc,ch+id+1);
		  if (!strcmp(repc,"OUI")) soleil->autorepartct=1;
		}
	    }
	}
    }


  /* lecture normale du fichier de donnees */

  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"RAYT")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"SYMETRIE_2D")) 
		{
		  rep_ndbl(3,dlist,&ii,ch+id);
		  for (i=0;i<3;i++) raysymper->sym[raysymper->nbsym][i]=dlist[i];
		  (raysymper->nbsym)++;
		}
	      else if (!strcmp(motcle,"SYMETRIE_3D")) 
		{
		  rep_ndbl(4,dlist,&ii,ch+id);
		  for (i=0;i<4;i++) raysymper->sym[raysymper->nbsym][i]=dlist[i];
		  (raysymper->nbsym)++;
		}
	      else if (!strcmp(motcle,"PERIODICITE_2D")) 
		{
		  rep_ndbl(3,dlist,&ii,ch+id);
		  for (i=0;i<3;i++) raysymper->per[raysymper->nbsym][i]=dlist[i];
		  (raysymper->nbsection)++;
		  raysymper->nbper=(int)(360./dlist[2]+0.1);
		}
	      else if (!strcmp(motcle,"PERIODICITE_3D")) 
		{
		  rep_ndbl(7,dlist,&ii,ch+id);
		  for (i=0;i<7;i++) raysymper->per[raysymper->nbsym][i]=dlist[i];
		  (raysymper->nbsection)++;
		  raysymper->nbper=(int)(360./dlist[6]+0.1);
		}
	      else if (!strcmp(motcle,"VOLUME_CONNEXE")) 
		{
		  if (maillnodray.ndim==2)
		    {
		      rep_ndbl(2,dlist,&ii,ch+id);
		      volconnexe->x[volconnexe->nb]=dlist[0];
		      volconnexe->y[volconnexe->nb]=dlist[1];
		      volconnexe->z[volconnexe->nb]=0.;
		    }
		  else
		    {
		      rep_ndbl(3,dlist,&ii,ch+id);
		      volconnexe->x[volconnexe->nb]=dlist[0];
		      volconnexe->y[volconnexe->nb]=dlist[1];
		      volconnexe->z[volconnexe->nb]=dlist[2];
		    }
		  (volconnexe->nb)++;
		}
	      else if (!strcmp(motcle,"BANDES_SPECTRALES")) 
		{
		  rep_ndbl(3,dlist,&ii,ch+id);
		  if ((int)(*dlist)>bandespec.nb) 
		    {
		      if (SYRTHES_LANG == FR)
			{
			  printf("\n\n Erreur dans le fichier de donnees pour le rayonnement\n");
			  printf(" On definit la bande spectrale %d alors que le calcul n'est\n",(int)(*dlist));
			  printf(" demande que sur %d bande(s)\n",bandespec.nb);
			  printf(" cf 'NOMBRE DE BANDES SPECTRALES POUR LE RAYONNEMENT='\n\n");
			}
		      else if (SYRTHES_LANG == EN)
			{
			  printf("\n\n Error on the data file for the radiation\n");
			  printf(" one defines the spectral band %d but the calculation is asked\n",(int)(*dlist));
			  printf(" on  %d band(s)\n",bandespec.nb);
			  printf(" cf 'NOMBRE DE BANDES SPECTRALES POUR LE RAYONNEMENT='\n\n");
			}

		      syrthes_exit(1);
		    }
		  bandespec.borneinf[(int)(*dlist)-1]=dlist[1];
		  bandespec.bornesup[(int)(*dlist)-1]=dlist[2];
		}

	      else if (!strcmp(motcle,"TEMPERATURE_INFINI")) 
		{
		  rep_ndbl(1,&val,&ii,ch+id);
		  propinf->temp=val;
		}
	      
	      else if (!strcmp(motcle,"OMBRAGE")) 
		{
		  rep_ndbl(1,&val,&ii,ch+id);
		  rep_listint(ilist,&nb,ch+id+ii);
		  for (n=0;n<nb;n++)
		    {
		      nr=ilist[n];
		      if (nr==-1)
			for (i=0;i<mask->nelem;i++) mask->opacite[i]=val;
		      else
			for (i=0;i<mask->nelem;i++) 
			  if (mask->ref[i]==nr) mask->opacite[i]=val;
		    }
		}

	    }

	  else if (!strcmp(motcle,"SOLAIRE")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
	      if (!strcmp(motcle,"LATITUDE_DU_LIEU")) 
		{
		  rep_nint(2,ilist,&ii,ch+id);
		  lieu->latituded=ilist[0]; lieu->latitudem=ilist[1];
		}
	      else if (!strcmp(motcle,"LONGITUDE_DU_LIEU")) 
		{
		  rep_nint(2,ilist,&ii,ch+id);
		  lieu->longituded=ilist[0]; lieu->longitudem=ilist[1];
		}
	      else if (!strcmp(motcle,"MOIS_JOUR_HEURE_MINUTE")) 
		{
		  rep_nint(4,ilist,&ii,ch+id);
		  lieu->mois_i=lieu->mois=ilist[0];   
		  lieu->jour_i=lieu->jour=ilist[1];
		  lieu->day_i=lieu->day=(int)((lieu->mois-1)*30.5+lieu->jour);
		  lieu->heure_i=lieu->heure=ilist[2];  
		  lieu->minute_i=lieu->minute=ilist[3];
		}
	      else if (!strcmp(motcle,"COEFFICIENTS_DE_CLARTE_DU_CIEL")) 
		{
		  rep_ndbl(2,dlist,&ii,ch+id);
		  soleil->A=dlist[0];   soleil->B=dlist[1];
		}
	      else if (!strcmp(motcle,"2D_VERTICAL_:_ANGLE_PAR_RAPPORT_AU_NORD")) 
		{
		  rep_ndbl(1,dlist,&ii,ch+id);
		  lieu->gama=dlist[0]*Pi/180.; 
		}
	      else if (!strcmp(motcle,"2D_HORIZONTAL_:_HAUTEUR_DE_LA_COUPE")) 
		{
		  rep_ndbl(1,dlist,&ii,ch+id);
		  lieu->hauteur=dlist[0]; 
		}
	      else if (!strcmp(motcle,"2D_:_HORIZONTAL_OU_VERTICAL")) 
		{
		  rep_ch(repc,ch+id+1);
		  lieu->vertical=1;
		  if (!strcmp(repc,"HORIZONTAL")) lieu->vertical=0;
		}
	      else if (!strcmp(motcle,"CENTRE_DU_DOMAINE_DE_CALCUL")) 
		{
		  rep_ndbl(3,dlist,&ii,ch+id);
		  lieu->xc=dlist[0]; lieu->yc=dlist[1]; lieu->zc=dlist[2]; 
		}
	      else if (!strcmp(motcle,"BILAN")) 
		{	    
		  rep_listint(ilist,&nb,ch+id);
		  bilansolaire->nbref[bilansolaire->nb]=nb;
		  bilansolaire->ref[bilansolaire->nb]=(int*)malloc(nb*sizeof(int));
		  verif_alloue_int1d("lire_divers_ray",bilansolaire->ref[bilansolaire->nb]);
		  for (i=0;i<nb;i++) bilansolaire->ref[bilansolaire->nb][i]=ilist[i];
		  bilansolaire->nb++;	  
		}
	      else if (!strcmp(motcle,"SOURCE_AZIMUT_H"))
		{                                        
		  rep_ndbl(2,dlist,&ii,ch+id);
		  if (dlist[1]>0. && dlist[2]>=0.)
		    {
		      soleil->anglect=dlist[1]*Pi/180.;   soleil->azict=dlist[0]*Pi/180.; 
		      soleil->anglect_actif=1;
		    }
		  else
		    {
		      soleil->anglect_actif=0;
		      if (syrglob_nparts==1 ||syrglob_rang==0)
			{
			  if (SYRTHES_LANG == FR){
			    printf("\nL'azimut et la hauteur de la source ne sont pas corrects\n");
			    printf("Se reporter au mot-cle SOLAIRE=  SOURCE_AZIMUT_H \n\n\n");
			  }
			  else if (SYRTHES_LANG == EN){
			    printf("\nAzimut and height of the source are not valid\n");
			    printf("See key-word SOLAIRE=  SOURCE_AZIMUT_H \n\n\n");
			  }
			}
		      err=1;
		    }
		}
	      else if (!strcmp(motcle,"SOURCE_FLUX_CONSTANT_PAR_BANDE")) /* ???????? ca ne va pas car ce depend de */ 
		{                                                        /* de l'ordre des mots-cles (avec le precedent) */
		  if (soleil->autorepartct){
		    rep_ndbl(1,dlist,&ii,ch+id);
		    soleil->fluxct[0]=dlist[0];
		  }
		  else{
		    rep_ndbl(bandespec.nb,dlist,&ii,ch+id);
		    for (i=0;i<bandespec.nb;i++) soleil->fluxct[i]=dlist[i];
		  }
		}
	      else if (!strcmp(motcle,"TEMPERATURE_ET_EMISSIVITE_DE_LA_TERRE_A_L_HORIZON")) 
		{
		  rep_ndbl(2,dlist,&ii,ch+id);
		  horiz->temp=dlist[0]; horiz->emissi=dlist[1];
		}

	    }

	}
    }


  /* Controles d'erreurs */
  /* ------------------- */
  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR)
	{
	  if (bandespec.nb>1)
	    for (i=0;i<bandespec.nb;i++)
	      if (bandespec.borneinf[i]<0 || bandespec.bornesup[i]<0)
		{
		  printf("\nLes bornes de la bande spectrale %i ne sont pas definies\n",i+1);
		  printf("Se reporter au mot-cle RAYT=  BANDES_SPECTRALES \n\n\n");
		  err=1;
		}
	  
	  if (soleil->actif && (raysymper->nbsym || raysymper->nbper))
	    {
	      printf("\nLe calcul du rayonnement solaire est actif.\n");
	      printf("Il est incoherent de vouloir prendre en compte une symetrie ou une periodicite\n");
	      printf("Le rayonnement solaire est par essence tridimensionnel et la totalite\n");
	      printf("du domaine de calcul doit etre maillee.\n\n\n");
	      err=1;
	    }
	}
      else if (SYRTHES_LANG == EN)
	{
	  if (bandespec.nb>1)
	    for (i=0;i<bandespec.nb;i++)
	      if (bandespec.borneinf[i]<0 || bandespec.bornesup[i]<0)
		{
		  printf("\n Spectral band range  %i is not defined\n",i+1);
		  printf("See the key-word :  RAYT=  BANDES_SPECTRALES \n\n\n");
		  err=1;
		}
	  
	  if (soleil->actif && (raysymper->nbsym || raysymper->nbper))
	    {
	      printf("\n Solar radiation calculation is activated.\n");
	      printf(" It is not coherent to take into account periodicity or symetry into account\n");
	      printf(" Solar radiation is fundamentally  tridimensionnal and the complete\n");
	      printf(" domain must be meshed.\n\n\n");
	      err=1;
	    }
	}   
    }
  if (err) syrthes_exit(1);

  /* Impression des informations */
  /*---------------------------- */
  if (syrglob_nparts==1 ||syrglob_rang==0)
    {

      if (!lieu->vertical)
	{
	  if (SYRTHES_LANG == FR)
	    {
	      printf("\n $$$ ATTENTION\n");
	      printf("     ---------\n");
	      printf("        Vous avez choisi de faire un calcul avec rayonnement solaire\n");
	      printf("        dans un plan 2D HORIZONTAL\n");
	      printf("        On rappelle que dans ce cas l'approximation 2D n'est pas justifiee\n");
	      printf("        et induit une erreur sur le calcul des flux radiatifs solaires\n");
	      printf("        puisque l'effet du sol ne peut etre pris en compte.\n\n");
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("\n $$$ WARNING\n");
	      printf("     ---------\n");
	      printf("        You have chosen to do a calculation including solar radiation\n");
	      printf("        in a 2D HORIZONTAL plane\n");
	      printf("        On reminds that in that case the 2D approximation is not justified\n");
	      printf("        and introduces an error on the solar radiation flux\n");
	      printf("        since the ground effect cannot be taken into account.\n\n");
	    }
	}
      
      if (soleil->anglect_actif && soleil->autorepartct)
	{
	  if (SYRTHES_LANG == FR)
	    {
	      printf("\n *** REPARTITION DU FLUX SOLAIRE CONSTANT SUR LES BANDES SPECTRALES :\n");
	      printf("     Flux initial %f\n",soleil->fluxct[0]);
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("\n *** DISTRIBUTION OF CONSTANT SOLAR FLUX ON THE SPECTRAL BANDS :\n");
	      printf("     Initial Flux %f\n",soleil->fluxct[0]);
	    }
	  ff=soleil->fluxct[0];
	  for (nn=0;nn<bandespec.nb;nn++)
	    {
	      w1=wiebel(c2/(bandespec.borneinf[nn]*tsoleil));
	      w2=wiebel(c2/(bandespec.bornesup[nn]*tsoleil));
	      soleil->fluxct[nn]=ff*(w2-w1);
	      if (SYRTHES_LANG == FR)
		printf("      - Bande %d : %f\n",nn+1,soleil->fluxct[nn]);
	      else if (SYRTHES_LANG == EN)
		printf("      - Band %d : %f\n",nn+1,soleil->fluxct[nn]);
	    }
	}
      

      if (soleil->anglect_actif && !soleil->autorepartct)
	{
	  if (SYRTHES_LANG == FR)
	    printf("\n *** SOLAIRE : SOURCE CONSTANTE SUR LES BANDES SPECTRALES :\n");
	  
	  else if (SYRTHES_LANG == EN)
	    printf("\n *** SOLAR : CONSTANT SOURCE ON THE SPECTRAL BANDS :\n");
	  
	  for (nn=0;nn<bandespec.nb;nn++)
	    {
	      if (SYRTHES_LANG == FR)      printf("      - Bande %d : %f\n",nn+1,soleil->fluxct[nn]);
	      else if (SYRTHES_LANG == EN) printf("      - Band %d : %f\n",nn+1,soleil->fluxct[nn]);
	    }
	}
      
      if (soleil->actif && !soleil->anglect_actif) 
	{
	  if (SYRTHES_LANG == FR)
	    {
	      printf("\n *** CONDITIONS POUR LA PRISE EN COMPTE DU RAYONNEMENT SOLAIRE\n");
	      printf("      - Lieu :\n");
	      printf("           . latitude  : %d degres  %d minutes\n",lieu->latituded,lieu->latitudem);
	      printf("           . longitude : %d degres  %d minutes\n",lieu->longituded,lieu->longitudem);
	      printf("      - Debut de la simulation : le %d %s a %d h %d\n",
		     lieu->jour,nommoisFR[lieu->mois-1],lieu->heure,lieu->minute);
	      printf("      - Coefficients de clarte du ciel : %f %f\n",soleil->A,soleil->B);
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("\n *** CONDITIONS FOR TAKING INTO ACCOUNT SOLAR RADIATION\n");
	      printf("      - Location :\n");
	      printf("           . latitude  : %d degres  %d minutes\n",lieu->latituded,lieu->latitudem);
	      printf("           . longitude : %d degres  %d minutes\n",lieu->longituded,lieu->longitudem);
	      printf("      - Start of simulation : the %d %s a %d h %d\n",
		     lieu->jour,nommoisGB[lieu->mois-1],lieu->heure,lieu->minute);
	      printf("      - Coefficients of sky clearness : %f %f\n",soleil->A,soleil->B);
	    }
	}
    }

  free(motcle); free(ilist); free(dlist);


}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_limite_ray(struct Maillage maillnodray,struct ProphyRay phyray,
		     struct Vitre *vitre,struct Clim *fimpray,
		     double *tmpray)
{
  int i,j,k;
  char ch[CHLONG],*motcle;
  int i1,i2,i3,i4,id,ok=1,ii,nb,nr,n;
  int *ilist;
  double val,*dlist;


  fseek(fdata,0,SEEK_SET);

  motcle=(char*)malloc(CHLONG*sizeof(char));    verif_alloue_char("lire_limite_ray",motcle);
  ilist=(int*)malloc(100*sizeof(int));       verif_alloue_int1d("lire_limite_ray",ilist);
  dlist=(double*)malloc(10*sizeof(double));  verif_alloue_double1d("lire_limite_ray",dlist);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"RAYT")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"ETR")) /* emissivite, transmittivite, reflectivite */ 
		{
		  rep_ndbl(4,dlist,&ii,ch+id);
		  rep_listint(ilist,&nb,ch+id+ii);
		  for (n=0;n<nb;n++)
		    {
		      nr=ilist[n];
		      if (nr==-1)
			for (i=0;i<maillnodray.nelem;i++) {
			  phyray.emissi[(int)(*dlist)-1][i]=phyray.absorb[(int)(*dlist)-1][i]=*(dlist+1);
			  phyray.transm[(int)(*dlist)-1][i]=*(dlist+2);
			  phyray.reflec[(int)(*dlist)-1][i]=*(dlist+3);
			}
		      else
			for (i=0;i<maillnodray.nelem;i++) 
			  if (maillnodray.nrefe[i]==nr) {
			    phyray.emissi[(int)(*dlist)-1][i]=phyray.absorb[(int)(*dlist)-1][i]=*(dlist+1);
			    phyray.transm[(int)(*dlist)-1][i]=*(dlist+2);
			    phyray.reflec[(int)(*dlist)-1][i]=*(dlist+3);
			  }
		    }
		}
	      
	      else if (!strcmp(motcle,"INDICE_DE_REFRACTION_PAR_BANDE")) 
		{
		  rep_ndbl(2,dlist,&ii,ch+id);
		  rep_listint(ilist,&nb,ch+id+ii);
		  for (n=0;n<nb;n++)
		    {
		      nr=ilist[n];
		      if (nr==-1)
			for (i=0;i<maillnodray.nelem;i++) vitre->refrac[(int)(*dlist)-1][i]=*(dlist+1);
		      else
			for (i=0;i<maillnodray.nelem;i++) 
			  if (maillnodray.nrefe[i]==nr) vitre->refrac[(int)(*dlist)-1][i]=*(dlist+1);
		    }
		}
	    }
	  
	  else if (!strcmp(motcle,"CLIM_RAYT")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"TEMPERATURE_IMPOSEE")) 
		{
		  rep_ndbl(1,&val,&ii,ch+id);
		  rep_listint(ilist,&nb,ch+id+ii);
		  for (n=0;n<nb;n++)
		    {
		      nr=ilist[n];
		      if (nr==-1)
			for (i=0;i<maillnodray.nelem;i++) tmpray[i]=val;
		      else
			for (i=0;i<maillnodray.nelem;i++) 
			  if (maillnodray.nrefe[i]==nr) tmpray[i]=val;
		    }
		}

			           
	      else if (!strcmp(motcle,"FLUX_IMPOSE_PAR_BANDE")) 
		{
		  rep_ndbl(2,dlist,&ii,ch+id);
		  rep_listint(ilist,&nb,ch+id+ii);
		  for (n=0;n<nb;n++)
		    {
		      nr=ilist[n];
		      if (nr==-1)
			  for (i=0;i<fimpray->nelem;i++) 
			   {fimpray->val1[(int)(*dlist)][i]=*(dlist+1);}
		      else
			for (i=0;i<fimpray->nelem;i++) 
			  if (maillnodray.nrefe[fimpray->numf[i]]==nr) 
			    {fimpray->val1[(int)(*dlist)][i]=*(dlist+1);}
		    }
		}


	    } /* fin CLIM_RAYT */
	}
    }


  free(motcle); free(ilist); free(dlist);


}
